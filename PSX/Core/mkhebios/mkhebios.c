/***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/***************************************************************************/

typedef unsigned char  uint8;
typedef unsigned short uint16;
typedef unsigned int   uint32;

/***************************************************************************/

uint16 get16lsb(const uint8 *p) {
  return (((uint16)(p[0])) <<  0) |
         (((uint16)(p[1])) <<  8);
}

uint32 get32lsb(const uint8 *p) {
  return (((uint32)(p[0])) <<  0) |
         (((uint32)(p[1])) <<  8) |
         (((uint32)(p[2])) << 16) |
         (((uint32)(p[3])) << 24);
}

void put16lsb(uint8 *p, uint16 n) {
  p[0] = n >> 0;
  p[1] = n >> 8;
}

void put32lsb(uint8 *p, uint32 n) {
  p[0] = n >>  0;
  p[1] = n >>  8;
  p[2] = n >> 16;
  p[3] = n >> 24;
}

/***************************************************************************/

struct MODULE {
  uint8 name[11];
  uint16 code;
  void *ext_data;
  uint32 size;
  uint32 start;
};

void init_mod_fields(
  struct MODULE *mod,
  const char *name,
  uint16 code,
  uint32 start,
  uint32 size
) {
  memset(mod, 0, sizeof(struct MODULE));
  strncpy(mod->name, name, 10);
  mod->code  = code;
  mod->start = start;
  mod->size  = size;
}

/***************************************************************************/

int read_script_file(const char *filename, int (*linehandler)(int, const char*)) {
  int r = 0;
  char s[1000];
  int l = 0;
  int linenum = 1;
  FILE *f;
  printf("[reading script: '%s']\n", filename); fflush(stdout);
  f = fopen(filename, "rb");
  if(!f) {
    perror(filename);
    return 1;
  }
  for(;;) {
    int c = fgetc(f);
    if(c == 10 || c == EOF) {
      while(l && s[l-1] == 32) l--;
      s[l] = 0;
      if(s[0]) {
        if(linehandler(linenum, s)) {
          printf("error in %s line %d\n", filename, linenum);
          r = 1;
          break;
        }
      }
      l = 0;
      if(c == EOF) { r = 0; break; }
      linenum++;
      continue;
    }
    if(c < 32) c = 32;
    if(l || c > 32) {
      s[l] = c;
      if(l < (sizeof(s) - 1)) l++;
    }
  }
  fclose(f);
  return r;
}

/***************************************************************************/
/*
** Find the ROMDIR offset within the ROM.
** Returns 0 if not found (it should never be 0 in real circumstances).
*/
uint32 find_romdir(uint8 *bios, uint32 bios_size) {
  uint32 u;
  bios_size &= ~0xF;
  for(u = 0; u < bios_size; u += 0x10) {
    if(!memcmp(bios + u, "RESET\0\0\0\0\0", 10)) return u;
  }
  return 0;
}

/***************************************************************************/
/*
** Returns true on success
*/
int find_romdir_entry(
  uint8 *bios, uint32 bios_size,
  uint32 romdir_start,
  const uint8 *name,
  uint16 *out_code,
  uint32 *out_start,
  uint32 *out_size
) {
  uint32 ofs = 0;
  bios_size &= ~0xF;
  romdir_start &= ~0xF;
  for(; romdir_start < bios_size; romdir_start += 0x10) {
    uint8 n[11];
    uint16 c;
    uint32 s;
    // exhausted romdir?
    if(!bios[romdir_start]) return 0;
    memcpy(n, bios + romdir_start, 10);
    n[10] = 0;
    c = get16lsb(bios + romdir_start + 0xA);
    s = get32lsb(bios + romdir_start + 0xC);
    if(!strcmp(n, name)) {
      if(out_code ) *out_code  = c;
      if(out_start) *out_start = ofs;
      if(out_size ) *out_size  = s;
      return 1;
    }
    // round up to the nearest paragraph and advance offset
    // (confirmed the actual BIOS does this too)
    s += 0xF; s &= ~0xF;
    ofs += s;
  }
  return 0;
}

/***************************************************************************/

uint8 master_bios[0x400000];

struct MODULE *master_module_free_list = NULL;
int master_module_free_n = 0;
struct MODULE *master_module_pin_list = NULL;
int master_module_pin_n = 0;

struct MODULE *master_find_module(const char *name) {
  int n;
  for(n = 0; n < master_module_pin_n; n++) {
    if(!strcasecmp(master_module_pin_list[n].name, name)) return master_module_pin_list + n;
  }
  for(n = 0; n < master_module_free_n; n++) {
    if(!strcasecmp(master_module_free_list[n].name, name)) return master_module_free_list + n;
  }
  return NULL;
}

char *master_iopbtconf = NULL;

void master_init(void) {
  memset(master_bios, 0, sizeof(master_bios));
  master_iopbtconf = NULL;
  master_module_free_list = NULL;
  master_module_free_n = 0;
  master_module_pin_list = NULL;
  master_module_pin_n = 0;
}

void master_iopbtconf_append(const char *line) {
  char newline[1000];
  int l;
  char *p;

  strncpy(newline, line, sizeof(newline));
  newline[sizeof(newline)-1]=0;
  p = strrchr(newline,',');
  if(p) *p = 0;

  if(!master_iopbtconf) {
    master_iopbtconf = malloc(1);
    if(!master_iopbtconf) abort();
    *master_iopbtconf = 0;
  }
  l = strlen(master_iopbtconf) + strlen(newline) + 1;
  master_iopbtconf = realloc(master_iopbtconf, l + 1);
  if(!master_iopbtconf) abort();
  p = master_iopbtconf; p += strlen(p);
  strcpy(p, newline); p += strlen(p);
  p[0] = 10;
  p[1] = 0;
}

void master_module_free_add(struct MODULE *mod) {
  master_module_free_list = realloc(
    master_module_free_list,
    sizeof(struct MODULE) * (master_module_free_n + 1)
  );
  if(!master_module_free_list) abort();
  memcpy(master_module_free_list + master_module_free_n, mod, sizeof(struct MODULE));
  master_module_free_n++;
}

void master_module_pin_add(struct MODULE *mod) {
  master_module_pin_list = realloc(
    master_module_pin_list,
    sizeof(struct MODULE) * (master_module_pin_n + 1)
  );
  if(!master_module_pin_list) abort();
  memcpy(master_module_pin_list + master_module_pin_n, mod, sizeof(struct MODULE));
  master_module_pin_n++;
}

/***************************************************************************/
/*
** Returns nonzero on error
*/
int master_bios_modinfo(
  const char *name,
  struct MODULE *out_mod
) {
  uint32 rbase;
  int success;
  uint16 code;
  uint32 start;
  uint32 size;

  rbase = find_romdir(master_bios, sizeof(master_bios));
  if(!rbase) return 1;
  success = find_romdir_entry(
    master_bios, sizeof(master_bios),
    rbase,
    name,
    &code,
    &start,
    &size
  );
  if(!success) return 1;
  if(out_mod) init_mod_fields(out_mod, name, code, start, size);
  return 0;
}

/***************************************************************************/
/*
** ROM usage map (granularity 16 bytes)
*/
#define USAGE_ENTRIES (sizeof(master_bios)/0x10)
struct MODULE *rom_usage_map[USAGE_ENTRIES];

void rom_usage_clear(void) {
  memset(rom_usage_map, 0, sizeof(rom_usage_map));
}

void rom_usage_set(uint32 ofs, uint32 size, struct MODULE *m) {
  uint32 slop;
//printf("rom_usage_set(%X,%X,%X)\n",ofs,size,(uint32)m);
  slop = ofs & 0xF;
  size += slop;
  ofs -= slop;
  slop = (0x10 - (size & 0xF)) & 0xF;
  size += slop;
  ofs /= 0x10;
  size /= 0x10;
  if(ofs >= USAGE_ENTRIES) return;
  if(size > USAGE_ENTRIES) size = USAGE_ENTRIES;
  if((ofs + size) > USAGE_ENTRIES) size = USAGE_ENTRIES - ofs;
  while(size--) rom_usage_map[ofs++] = m;
}

uint32 rom_usage_get_free_space(uint32 ofs) {
  uint32 i;
  ofs /= 0x10;
  i = ofs;
  for(; i < USAGE_ENTRIES; i++) if(rom_usage_map[i]) break;
  i -= ofs;
  i *= 0x10;
  return i;
}

struct MODULE *rom_usage_get_module(uint32 ofs) {
  ofs /= 0x10;
  if(ofs >= USAGE_ENTRIES) return NULL;
  return rom_usage_map[ofs];
}

void rom_usage_set_all_pinned(void) {
  int n;
  if(!master_module_pin_list) return;
  for(n = 0; n < master_module_pin_n; n++) {
    rom_usage_set(
      master_module_pin_list[n].start,
      master_module_pin_list[n].size,
      master_module_pin_list + n
    );
  }
}

/***************************************************************************/

uint8 *romdir = NULL;
uint32 romdir_size = 0;
uint16 romdir_code = 0;

uint32 attempt_romdir_line = 0;
uint32 attempt_romdir_ofs  = 0;

int any_unhandled_pinned_modules(void) {
  int n;
  for(n = 0; n < master_module_pin_n; n++) {
    if(master_module_pin_list[n].start >= attempt_romdir_ofs) return 1;
  }
  return 0;
}

uint32 closest_unhandled_pinned_module(void) {
  uint32 lowest = 0xFFFFFFFF;
  int n;
  for(n = 0; n < master_module_pin_n; n++) {
    uint32 s = master_module_pin_list[n].start;
    if(s >= attempt_romdir_ofs) {
      if(s < lowest) lowest = s;
    }
  }
  return lowest;
}

uint32 space_in_romdir(void) { return romdir_size - attempt_romdir_line; }

int line_from_arbitrary(const char *name, uint16 code, uint32 size) {
  uint8 *p = romdir + attempt_romdir_line;
  if(space_in_romdir() < 0x10) return 1;
  memset  (p, 0, 0x10);
  strcpy  (p, name);
  put16lsb(p + 0xA, code);
  put32lsb(p + 0xC, size);
  attempt_romdir_line += 0x10;
//printf("new line name '%s' code %X size %X\n",name,code,size);
  return 0;
}

int line_from_module(struct MODULE *m) { return line_from_arbitrary(m->name, m->code, m->size); }
int line_from_blank(uint32 size) { return line_from_arbitrary("-", 0, size); }
int line_from_eod(void) { return line_from_arbitrary("", 0, 0); }

int handle_pinned(void) {
//printf("handle_pinned\n");
  for(;;) {
    struct MODULE *m = rom_usage_get_module(attempt_romdir_ofs);
    if(!m) break;
    if(line_from_module(m)) return 1;
    attempt_romdir_ofs += (m->size + 0xF) & (~0xF);
  }
  return 0;
}

int seek_to_free(uint32 s) {
//printf("seek_to_free(%X)\n",s);
  for(;;) {
    uint32 cf;
    if(handle_pinned()) return 1;
    cf = rom_usage_get_free_space(attempt_romdir_ofs);
//printf("  cf=%X\n",cf);
    if(!cf) return 1;
    if(cf >= s) return 0;
    if(line_from_blank(cf)) return 1;
    attempt_romdir_ofs += cf;
  }
  return 0;
}

/*
** Returns nonzero on error
*/
int attempt_rebuild_romdir(void) {
  int n;

  if(!romdir) return 1;
  romdir_size &= ~0xF;
  if(!romdir_size) return 1;

  memset(romdir, 0, romdir_size);

//printf("attempt rebuild romdir_size=0x%X\n",romdir_size);

  attempt_romdir_line = 0;
  attempt_romdir_ofs = 0;

  rom_usage_clear();
  rom_usage_set_all_pinned();

  if(seek_to_free(romdir_size)) return 1;
//printf("a l=%X ofs=%X\n",attempt_romdir_line,attempt_romdir_ofs);

  if(line_from_arbitrary("ROMDIR", romdir_code, romdir_size)) return 1;
  attempt_romdir_ofs += romdir_size;
//printf("b l=%X ofs=%X\n",attempt_romdir_line,attempt_romdir_ofs);

  for(n = 0; n < master_module_free_n; n++) {
    uint32 size;
//printf("n%02d a l=%X ofs=%X\n",n,attempt_romdir_line,attempt_romdir_ofs);
    size = master_module_free_list[n].size;
//printf("n%02d b l=%X ofs=%X\n",n,attempt_romdir_line,attempt_romdir_ofs);
    if(seek_to_free(size)) return 1;
//printf("n%02d c l=%X ofs=%X\n",n,attempt_romdir_line,attempt_romdir_ofs);
    if(line_from_module(master_module_free_list + n)) return 1;
//printf("n%02d d l=%X ofs=%X\n",n,attempt_romdir_line,attempt_romdir_ofs);
    attempt_romdir_ofs += (size + 0xF) & (~0xF);
//printf("n%02d e l=%X ofs=%X\n",n,attempt_romdir_line,attempt_romdir_ofs);
  }
//printf("z l=%X ofs=%X\n",attempt_romdir_line,attempt_romdir_ofs);

  // handle any higher-up pinned modules if necessary
  for(;;) {
    uint32 c, blank;
    handle_pinned();
    if(!(any_unhandled_pinned_modules())) break;
    c = closest_unhandled_pinned_module();
    blank = c - attempt_romdir_ofs;
    if(line_from_blank(blank)) return 1;
    attempt_romdir_ofs += blank;
  }

  if(line_from_eod()) return 1;

  return 0;
}

/***************************************************************************/

int find_romdir_code(void) {
  struct MODULE mod;
  printf("romdir code: "); fflush(stdout);
  if(master_bios_modinfo("ROMDIR", &mod)) {
    printf("not found\n");
    return 1;
  }
  romdir_code = mod.code;
  printf("[0x%04X]\n", romdir_code);
  return 0;
}

/***************************************************************************/
/*
** Returns nonzero on error
** modname can also be of the form "modname,hexcode"
*/
int irx(const char *modname) {
  FILE *f;
  uint32 code = 0xFFFFFFFF;
  char n[1000];
  int i;
  char *t;
  struct MODULE mod;
  i = 0;
  memset(&mod, 0, sizeof(struct MODULE));
  for(;;) {
    char c = *modname++;
    if(!c) break;
    if(isspace(c)) continue;
    n[i] = toupper(c);
    if(i < (sizeof(n) - 1)) i++;
  }
  n[i] = 0;
  t = strchr(n, ',');
  if(t) {
    *t = 0;
    t++;
    code = strtoul(t, NULL, 16);
    code &= 0xFFFF;
  }
  // force name to get chopped to 10 bytes
  n[10] = 0;

  printf("irx '%s'", n);
  if(code != 0xFFFFFFFF) printf("[0x%04X]", code);
  printf(": ");
  fflush(stdout);

  goto try_file;

try_file:
  memset(&mod, 0, sizeof(struct MODULE));
  { char s[1000];
    strcpy(s,"module/");
    strcat(s,n);
    f = fopen(s, "rb");
  }
  if(!f) goto file_fail;
  strcpy(mod.name, n);
  fseek(f, 0, SEEK_END);
  mod.size = ftell(f);
  fseek(f, 0, SEEK_SET);
  mod.ext_data = malloc(mod.size);
  if(!mod.ext_data) abort();
  if(fread(mod.ext_data, 1, mod.size, f) != mod.size) {
    free(mod.ext_data);
    fclose(f);
    printf("error reading from external file\n");
    return 1;
  }
  fclose(f);
  printf("file (%d bytes)", mod.size);
  goto file_ok;

try_bios:
  if(master_bios_modinfo(n, &mod)) goto bios_fail;
  mod.ext_data = malloc(mod.size);
  if(!mod.ext_data) abort();
  memcpy(mod.ext_data, master_bios + mod.start, mod.size);
  printf("BIOS 0x%08X (%d bytes)", mod.start, mod.size);
  goto bios_ok;

file_fail: goto try_bios;
bios_fail: goto giveup;

file_ok: goto codecheck;
bios_ok: goto codecheck;

giveup:
  printf("not found in BIOS or external file!\n");
  return 1;

codecheck:
  printf(" ");
  // if we're using an override code, great
  if(code != 0xFFFFFFFF) {
    mod.code = code;
    printf("[code override=%04X]", mod.code);
  // otherwise, try finding the BIOS code
  } else {
    struct MODULE tmpmod;
    if(master_bios_modinfo(n, &tmpmod)) {
      free(mod.ext_data);
      printf("code needed!\n");
      return 1;
    }
    mod.code = tmpmod.code;
    printf("[code=%04X]", mod.code);
  }

  printf(" ");
  if(mod.size < 4 || memcmp(mod.ext_data, "\x7F" "ELF", 4)) {
    free(mod.ext_data);
    printf("module is not ELF!\n");
    return 1;
  }
  printf("ok\n");

  master_module_free_add(&mod);

  return 0;
}

/***************************************************************************/

int pin(const char *modname) {
  char n[11];
  int i, l;
  struct MODULE mod;
  strncpy(n, modname, 10);
  n[10] = 0;
  l = strlen(n);
  for(i = 0; i < l; i++) { n[i] = toupper(n[i]); }

  printf("pin '%s': ", n); fflush(stdout);

  if(master_bios_modinfo(n, &mod)) {
    printf("not found\n");
    return 1;
  }
  printf("ok [0x%04X] 0x%08X (%d bytes)\n", mod.code, mod.start, mod.size);

  mod.ext_data = malloc(mod.size);
  if(!mod.ext_data) abort();
  memcpy(mod.ext_data, master_bios + mod.start, mod.size);

  master_module_pin_add(&mod);

  return 0;
}

/***************************************************************************/

int mkhebios_script_pin(const char *args) {
  return pin(args);
}

/***************************************************************************/

int mkhebios_script_load(const char *args) {
  FILE *f;
  int r;
  printf("load '%s': ", args); fflush(stdout);
  f = fopen(args, "rb");
  if(!f) { printf("%s\n", strerror(errno)); return 1; }
  r = fread(master_bios, 1, sizeof(master_bios), f);
  fclose(f);
  if(r != sizeof(master_bios)) {
    printf("incorrect size\n"); return 1;
  }
  printf("ok\n");
  printf("  [l] "); if(find_romdir_code()) return 1;
  printf("  [l] "); if(pin("reset")) return 1;
  return 0;
}

/***************************************************************************/

int mkhebios_script_save(const char *args) {
  FILE *f;
  printf("save '%s': ", args); fflush(stdout);
  f = fopen(args, "wb");
  if(!f) { printf("%s\n", strerror(errno)); return 1; }
  fwrite(master_bios, 1, sizeof(master_bios) / 8, f);
  fclose(f);
  printf("ok\n");
  return 0;
}

/***************************************************************************/

int mkhebios_iopbtconf_line(int linenum, const char *line) {
  int r;
  if(!line[0]) return 0;
  if(line[0] == '#') return 0;
  master_iopbtconf_append(line);
  if(!isalpha(line[0])) return 0;
  printf("  ");
  r = irx(line);
  if(r) return 1;
  return 0;
}

/***************************************************************************/

int mkhebios_script_iopbtconf(const char *args) {
  struct MODULE mod;

  printf("iopbtconf '%s': ", args);
  if(master_bios_modinfo("IOPBTCONF", &mod)) {
    printf("could not find BIOS IOPBTCONF data!\n");
    return 1;
  }
  printf("[code=0x%04X] processing...\n", mod.code);

  if(read_script_file(args, mkhebios_iopbtconf_line)) return 1;

  mod.size = strlen(master_iopbtconf);
  mod.ext_data = malloc(mod.size);
  if(!mod.ext_data) abort();
  memcpy(mod.ext_data, master_iopbtconf, mod.size);
  master_module_free_add(&mod);

  return 0;
}

/***************************************************************************/

int find_export(uint8 *stuff, uint32 size, char *export, uint32 *version) {
  uint32 i = 0;
  for(;; i += 4) {
    uint32 q;
    if((i + 4) > size) return 0;
    q = get32lsb(stuff + i);
    if(q == 0x41C00000) {
      i += 8;
      if((i + 4) > size) return 0;
      *version = get32lsb(stuff + i);
      i += 4;
      if((i + 8) > size) return 0;
      memcpy(export, stuff + i, 8);
      return 1;
    }
  }
  return 0;
}

/***************************************************************************/

void dump_romdir(uint8 *p) {
  uint32 start = 0;
  uint32 size;
  uint16 code;
  printf("---begin romdir dump---\n");
  for(;;) {
    char n[11];
    int i;
    memcpy(n, p, 10);
    n[10] = 0;
    if(!n[0]) break;
    for(i = 0; i < 10; i++) if(!n[i]) n[i] = 32;
    code = get16lsb(p + 0xA);
    size = get32lsb(p + 0xC);
    printf("0x%06X: %s [0x%04X] (0x%06X bytes)", start, n, code, size);
    memcpy(n, p, 10);
    n[10] = 0;
    { struct MODULE *pmod = master_find_module(n);
//      if(!pmod) { printf("couldn't find a named module '%s'\n", n); exit(1); }
      if(pmod) {
        uint8 *stuff = pmod->ext_data;
//printf("pmod");
        if(stuff) {
          char export[9];
          uint32 version = 0;
//printf("stuff");
          memset(export, 0, sizeof(export));
          if(find_export(stuff, size, export, &version)) {
            printf(" export: %-8s v%X", export, version);
          }
        }
      }
    }
    printf("\n");
    size += 0xF; size &= ~0xF; start += size;
    p += 0x10;
  }
  printf("0x%06X:\n", start);
  printf("---end romdir dump---\n");
}

/***************************************************************************/

uint32 fill_word = 0;

void rebuild_master_bios(void) {
  uint8 *p = romdir;
  uint32 start = 0;
  uint32 size;
  uint16 code;

  memset(master_bios, 0, sizeof(master_bios));
  { int i;
    for(i=0;i<sizeof(master_bios);i+=4){
      *((uint32*)(((uint8*)master_bios)+i)) = fill_word;
    }
  }

  for(;;) {
    uint8 *srcdata = NULL;
    char n[11];
//    int i;
    memcpy(n, p, 10);
    n[10] = 0;
    if(!n[0]) break;
    code = get16lsb(p + 0xA);
    size = get32lsb(p + 0xC);
//printf("named module %s\n",n);fflush(stdout);
    if(!strcmp(n, "-")) {
      srcdata = NULL;
    } else if(!strcmp(n, "ROMDIR")) {
      srcdata = romdir;
    } else {
      struct MODULE *pmod = master_find_module(n);
      if(!pmod) { printf("couldn't find a named module '%s'\n", n); exit(1); }
//printf("pmod         = %08X\n",(uint32)pmod          );
//printf("pmod extdata = %08X\n",(uint32)(pmod->ext_data));
      srcdata = pmod->ext_data;
    }
    if(start >= sizeof(master_bios)) { printf("bios too big\n"); exit(1); }
    if((start + size) >= sizeof(master_bios)) { printf("bios too big\n"); exit(1); }
    if(srcdata) memcpy(master_bios + start, srcdata, size);
//printf("named module %s done\n",n);fflush(stdout);
    size += 0xF; size &= ~0xF; start += size;
    p += 0x10;
  }
}

/***************************************************************************/

int mkhebios_script_rebuild(const char *args) {
  uint32 romdir_size_min;
  uint32 romdir_size_max;

  printf("rebuild: "); fflush(stdout);

  romdir_size_min = 0x10 * (master_module_free_n + master_module_pin_n);
  romdir_size_max = 500000 & (~0xF);

  for(
    romdir_size = romdir_size_min;
    romdir_size <= romdir_size_max;
    romdir_size += 0x10
  ) {
    if(romdir) { free(romdir); romdir = NULL; }
    romdir = malloc(romdir_size);
    if(!romdir) abort();
    if(!attempt_rebuild_romdir()) break;
    if(romdir) { free(romdir); romdir = NULL; }
  }

  if(!romdir) {
    printf("unsuccessful\n");
    return 1;
  }

  printf("romdir size=0x%X\n", romdir_size);

  rebuild_master_bios();

  dump_romdir(romdir);

  printf("ok\n");
  return 0;
}

/***************************************************************************/

int mkhebios_script_patch(const char *args) {
  uint32 patchstart = 0;
  uint8 patchfrom[1000];
  uint8 patchto[1000];
  uint32 patchfromny = 0;
  uint32 patchtony = 0;
  uint32 patchlen = 0;
  char n[1000];
  int i = 0;
  char *t = n;
  printf("patch '%s': ", args); fflush(stdout);
  for(;;) {
    char c = *args++;
    if(!c) break;
    if(isspace(c)) continue;
    n[i] = toupper(c);
    if(i < (sizeof(n) - 1)) i++;
  }
  n[i] = 0;
  t = n;
  for(;;) {
    int c = *t++;
    if(c == ':') break;
         if(c >= 'A' && c <= 'F') { c -= 'A'; c += 10; }
    else if(c >= 'a' && c <= 'f') { c -= 'a'; c += 10; }
    else if(c >= '0' && c <= '9') { c -= '0'; c +=  0; }
    else { printf("error on line\n"); return 1; }
    patchstart <<= 4;
    patchstart += c;
  }
//  printf("patchstart %X\n",patchstart);

  for(;;) {
    int c = *t++;
    if(c == ':') break;
         if(c >= 'A' && c <= 'F') { c -= 'A'; c += 10; }
    else if(c >= 'a' && c <= 'f') { c -= 'a'; c += 10; }
    else if(c >= '0' && c <= '9') { c -= '0'; c +=  0; }
    else { printf("error on line\n"); return 1; }
    patchfrom[patchfromny/2] <<= 4;
    patchfrom[patchfromny/2] += c;
    patchfromny++;
  }

  for(;;) {
    int c = *t++;
    if(c == 0) break;
         if(c >= 'A' && c <= 'F') { c -= 'A'; c += 10; }
    else if(c >= 'a' && c <= 'f') { c -= 'a'; c += 10; }
    else if(c >= '0' && c <= '9') { c -= '0'; c +=  0; }
    else { printf("error on line\n"); return 1; }
    patchto[patchtony/2] <<= 4;
    patchto[patchtony/2] += c;
    patchtony++;
  }

  if(patchfromny != patchtony) { printf("patch from/to lengths must match\n"); return 1; }
  patchlen = patchfromny / 2;
  if(!patchlen) { printf("nothing to patch\n"); return 1; }

  if(patchstart >= sizeof(master_bios)) { printf("patch out of range\n"); return 1; }
  if((patchstart + patchlen) > sizeof(master_bios)) { printf("patch out of range\n"); return 1; }

  if(memcmp(master_bios + patchstart, patchfrom, patchlen)) {
    printf("original pattern does not match\n");
    return 1;
  }

  memcpy(master_bios + patchstart, patchto, patchlen);

  printf("ok (0x%X)\n", patchlen);

  return 0;
}

/***************************************************************************/

int mkhebios_script_asc(const char *args) {
  uint32 ascstart = 0;
  printf("asc '%s': ", args); fflush(stdout);

  for(;;) {
    int c = *args++;
    if(!c) { printf("error on line\n"); return 1; }
    if(isspace(c)) continue;
    if(c == ':') break;
         if(c >= 'A' && c <= 'F') { c -= 'A'; c += 10; }
    else if(c >= 'a' && c <= 'f') { c -= 'a'; c += 10; }
    else if(c >= '0' && c <= '9') { c -= '0'; c +=  0; }
    else { printf("error on line\n"); return 1; }
    ascstart <<= 4;
    ascstart += c;
  }

  for(;;) {
    int c = *args++;
    if(!c) { printf("error on line\n"); return 1; }
    if(isspace(c)) continue;
    if(c == '\"') break;
  }

  for(;;) {
    int c = *args++;
    if(!c) { printf("error on line\n"); return 1; }
    if(c == '\"') break;
    master_bios[ascstart % sizeof(master_bios)] = c;
    ascstart++;
  }
  master_bios[ascstart % sizeof(master_bios)] = 0;

  printf("ok\n");
  return 0;
}

/***************************************************************************/

int mkhebios_script_deadbeef(const char *args) {
  fill_word = 0xDEADBEEF;
  return 0;
}

/***************************************************************************/
/*
** Returns nonzero on error
*/
int mkhebios_script_line(int linenum, const char *line) {
  if(line[0] == '#') return 0;

  { int cmdlen = 0;
    int ofs_to_args = 0;
    while(line[cmdlen] && isalpha(line[cmdlen])) cmdlen++;
    ofs_to_args = cmdlen;
    while(line[ofs_to_args] && isspace(line[ofs_to_args])) ofs_to_args++;

    switch(cmdlen) {
    case 3:
      if(!memcmp(line, "pin", 3)) return mkhebios_script_pin(line + ofs_to_args);
      if(!memcmp(line, "asc", 3)) return mkhebios_script_asc(line + ofs_to_args);
      break;
    case 4:
      if(!memcmp(line, "load", 4)) return mkhebios_script_load(line + ofs_to_args);
      if(!memcmp(line, "save", 4)) return mkhebios_script_save(line + ofs_to_args);
      break;
    case 5:
      if(!memcmp(line, "patch", 5)) return mkhebios_script_patch(line + ofs_to_args);
      break;
    case 7:
      if(!memcmp(line, "rebuild", 7)) return mkhebios_script_rebuild(line + ofs_to_args);
      break;
    case 8:
      if(!memcmp(line, "deadbeef", 8)) return mkhebios_script_deadbeef(line + ofs_to_args);
      break;
    case 9:
      if(!memcmp(line, "iopbtconf", 9)) return mkhebios_script_iopbtconf(line + ofs_to_args);
      break;
    }

    printf("unknown command '");
    while(cmdlen--) { printf("%c", *line); line++; }
    printf("'\n");
  }
  return 1;
}

/***************************************************************************/

int main(int argc, char **argv) {
  if(argc != 2) {
    fprintf(stderr, "usage: %s scriptfile\n", argv[0]);
    return 1;
  }
  master_init();
  return read_script_file(argv[1], mkhebios_script_line);
}

/***************************************************************************/
