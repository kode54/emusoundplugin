#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define uint8 unsigned char
#define uint16 unsigned short
#define uint32 unsigned int

uint32 get32lsb(const uint8 *p) {
  return (((uint32)(p[0])) <<  0) |
         (((uint32)(p[1])) <<  8) |
         (((uint32)(p[2])) << 16) |
         (((uint32)(p[3])) << 24);
}

int find_export(uint8 *stuff, uint32 size, char *export, uint32 *version) {
  uint32 i = 0;
  for(;; i += 4) {
    uint32 q;
    if((i + 4) > size) return 0;
    q = get32lsb(stuff + i);
    if(q == 0x41C00000) {
      i += 8;
      if((i + 4) > size) return 0;
      *version = get32lsb(stuff + i);
      i += 4;
      if((i + 8) > size) return 0;
      memcpy(export, stuff + i, 8);
      return 1;
    }
  }
  return 0;
}

int main(int argc, char **argv) {
  unsigned o;
  unsigned dir_ok;
  FILE *f;

  if(argc != 2) {
    printf("usage: %s image\n", argv[0]);
    return 1;
  }

  f = fopen(argv[1], "rb");
  if(!f) { perror(argv[1]); return 1; }

  dir_ok = 0;
  o = 0;
  for(;;) {
    char buf[0x10];
    char s[100];
    unsigned n;
    unsigned l;
    unsigned ver;
    int r;
    memset(buf, 0, sizeof(buf));
    r = fread(buf, 1, 0x10, f);
    if(r != 0x10) break;

    if(!dir_ok) {
      if(memcmp(buf, "RESET\0\0\0\0\0", 10)) continue;
      dir_ok = 1;
    }

    if(!buf[0]) break;
    memcpy(s, buf, 10);
    s[10] = 0;
    n =
      ((((unsigned)((unsigned char)(buf[0xA]))) & 0xFF) <<  0) |
      ((((unsigned)((unsigned char)(buf[0xB]))) & 0xFF) <<  8);
    l =
      ((((unsigned)((unsigned char)(buf[0xC]))) & 0xFF) <<  0) |
      ((((unsigned)((unsigned char)(buf[0xD]))) & 0xFF) <<  8) |
      ((((unsigned)((unsigned char)(buf[0xE]))) & 0xFF) << 16) |
      ((((unsigned)((unsigned char)(buf[0xF]))) & 0xFF) << 24);
    printf("0x%06X: %-10s (0x%04X) (0x%06X bytes)",
      o, s, n, l
    );

    { int templ = ftell(f);
      char *thebuf = malloc(l);
      int r;
      if(!thebuf) abort();
      fseek(f,o,SEEK_SET);
      r = fread(thebuf, 1, l, f);
      if(r == l) {
        memset(s, 0, sizeof(s));
        ver = 0;
        if(find_export(thebuf, l, s, &ver)) {
          printf(" export: %-8s v%X", s, ver);
        }
      }
      free(thebuf);
      fseek(f,templ,SEEK_SET);
    }
    printf("\n");

    o += ((l + 0xF) & (~0xF));
  }


  fclose(f);

  return 0;
}
