#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned char stuff[4096*1024];

int main(int argc, char **argv) {
  unsigned o;
  unsigned dir_ok;
  FILE *f;

  if(argc != 2) {
    fprintf(stderr, "usage: %s image\n", argv[0]);
    return 1;
  }

  f = fopen(argv[1], "rb");
  if(!f) { perror(argv[1]); return 1; }

  fread(stuff,1,sizeof(stuff),f);
  fseek(f,0,SEEK_SET);

  dir_ok = 0;
  o = 0xBFC00000;
  for(;;) {
    char buf[0x10];
    char s[100];
    unsigned n;
    unsigned l;
    int r;
    memset(buf, 0, sizeof(buf));
    r = fread(buf, 1, 0x10, f);
    if(r != 0x10) break;

    if(!dir_ok) {
      if(memcmp(buf, "RESET\0\0\0\0\0", 10)) continue;
      dir_ok = 1;
    }

    if(!buf[0]) break;
    memcpy(s, buf, 10);
    s[10] = 0;
    n =
      ((((unsigned)((unsigned char)(buf[0xA]))) & 0xFF) <<  0) |
      ((((unsigned)((unsigned char)(buf[0xB]))) & 0xFF) <<  8);
    l =
      ((((unsigned)((unsigned char)(buf[0xC]))) & 0xFF) <<  0) |
      ((((unsigned)((unsigned char)(buf[0xD]))) & 0xFF) <<  8) |
      ((((unsigned)((unsigned char)(buf[0xE]))) & 0xFF) << 16) |
      ((((unsigned)((unsigned char)(buf[0xF]))) & 0xFF) << 24);
    fprintf(stdout, "0x%08X: %s (0x%X) (0x%X bytes)\n",
      o, s, n, l
    );
    { FILE*g=fopen(s,"wb");
      if(g){
        fwrite(stuff+(o%sizeof(stuff)),1,l,g);
        fclose(g);
      }
    }
    o += ((l + 0xF) & (~0xF));
  }


  fclose(f);

  return 0;
}
