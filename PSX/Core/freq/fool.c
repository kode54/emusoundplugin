#include <stdio.h>
#include <math.h>

void makefilter(int ishigh, double mCurRate, double frequency, double dB_boost) {
  double a0,a1,a2,b0,b1,b2;
  char *pref = ishigh? "h":"l";

  /* Compute coefficents of the biquand IIR filter */
  double A = exp(log(10.0) * dB_boost / 40.0);
  double omega = 2.0 * (3.14159265358979323846264338) * frequency / mCurRate;
  double sn = sin(omega);
  double cs = cos(omega);
  double shape = 1.0;
  double beta = sqrt((A * A + 1.0) / shape - (pow((A - 1.0), 2.0)));

if(!ishigh) {
// low shelf
b0 =    A*( (A+1) - (A-1)*cs + beta*sn );
b1 =2.0*A*( (A-1) - (A+1)*cs );
b2 =    A*( (A+1) - (A-1)*cs - beta*sn );
a0 =        (A+1) + (A-1)*cs + beta*sn;
a1 = -2.0*( (A-1) + (A+1)*cs );
a2 =        (A+1) + (A-1)*cs - beta*sn;
} else {
// high shelf
b0 =         (A+1) - (A-1)*cs + beta*sn;
b1 =   2.0*( (A-1) - (A+1)*cs );
b2 =         (A+1) - (A-1)*cs - beta*sn;
a0 =     A*( (A+1) + (A-1)*cs + beta*sn );
a1 =-2.0*A*( (A-1) + (A+1)*cs );
a2 =     A*( (A+1) + (A-1)*cs - beta*sn );
}

a1/=a0;a2/=a0;b0/=a0;b1/=a0;b2/=a0;

printf("// %s sample=%f freq=%f db=%f\n",
ishigh? "HIGH SHELF":"LOW SHELF",
mCurRate,frequency,dB_boost);
printf("%sa0=%.20f;\n",pref,b0);
printf("%sa1=%.20f;\n",pref,b1);
printf("%sa2=%.20f;\n",pref,b2);
printf("%sb1=%.20f;\n",pref,a1);
printf("%sb2=%.20f;\n",pref,a2);

}

int main(void) {

  double lowf = 150.0;
  double lowboost = 4.0;

  double highf = 6000.0;
  double highboost = -5.0;

  printf("if(nSampleRate==48000) {\n");
  makefilter(0,48000.0,lowf,lowboost);
  makefilter(1,48000.0,highf,highboost);
  printf("} else {\n");
  makefilter(0,44100.0,lowf,lowboost);
  makefilter(1,44100.0,highf,highboost);
  printf("}\n");

  return 0;
}
