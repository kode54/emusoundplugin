#include <stdio.h>
double coefs[8]={
       (-0.056169185359),
       ( 0.068746930633),
       ( 0.284067907143),
       ( 0.476052988879),
       ( 0.476052988879),
       ( 0.284067907143),
       ( 0.068746930633),
       (-0.056169185359)
};
int main(void) {

  int i;
  double sum=0.0;
  for(i=0;i<4;i++)sum+=coefs[i];
  sum*=2.0;
printf("sum=%.30f\n",sum);
  for(i=0;i<8;i++)coefs[i]/=sum;
  for(i=0;i<8;i++){
  printf("%.30f\n",coefs[i]);
  }

  return 0;
}
