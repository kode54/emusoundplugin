#include <stdio.h>
#include <math.h>

void main(void) {
double a0,a1,a2,b0,b1,b2;
  double frequency = 150.0;
  double dB_boost = 4.0;
  double mCurRate = 44100.0;

  /* Compute coefficents of the biquand IIR filter */
  double omega = 2.0 * 3.141592653589 * frequency / mCurRate;
  double sn = sin(omega);
  double cs = cos(omega);
  double a = exp(log(10.0) * dB_boost / 40.0);
  double shape = 1.0;           /*Low Shelf filter's shape, if this is too large
                            or too small it will result an unstable filter */
  double beta = sqrt((a * a + 1.0) / shape - (pow((a - 1.0), 2.0)));
  /*  Coefficients  */
  b0 = a * ((a + 1) - (a - 1) * cs + beta * sn);
  b1 = 2 * a * ((a - 1) - (a + 1) * cs);
  b2 = a * ((a + 1) - (a - 1) * cs - beta * sn);
  a0 = ((a + 1) + (a - 1) * cs + beta * sn);
  a1 = -2 * ((a - 1) + (a + 1) * cs);
  a2 = (a + 1) + (a - 1) * cs - beta * sn;

a1/=a0;
a2/=a0;
b0/=a0;
b1/=a0;
b2/=a0;

printf("// for sample=%f freq=%f db=%f\n",mCurRate,frequency,dB_boost);
printf("a0=%.20f;\n",b0);
printf("a1=%.20f;\n",b1);
printf("a2=%.20f;\n",b2);
//printf("b0=%.20f;\n",a0);
printf("b1=%.20f;\n",a1);
printf("b2=%.20f;\n",a2);

}
