/*
** PSF emu core testing utility
** Usage: test in.psf out.raw
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "zlib/zlib.h"
#include "psxcore/psx.h"

static uint32 get32lsb(uint8 *src) {
  return
    ((((uint32)(src[0])) & 0xFF) <<  0) |
    ((((uint32)(src[1])) & 0xFF) <<  8) |
    ((((uint32)(src[2])) & 0xFF) << 16) |
    ((((uint32)(src[3])) & 0xFF) << 24);
}

/***************************************************************************/
/*
** Read the EXE from a PSF file
**
** Returns the error message, or NULL on success
*/
const char *psf_read_exe(
  const char *filename,
  unsigned char *exebuffer,
  unsigned exebuffersize
) {
  int r;
  FILE *f;
  uint8 *zexebuf;
  uLong destlen;
  uint32 reserved_size;
  uint32 exe_size;
  uint32 exe_crc;
  uint32 calc_zexe_crc;
  uint32 fl;
  uint8 hdr[0x10];

  f=fopen(filename,"rb");
  if(!f) return "Unable to open the file";
  fseek(f,0,SEEK_END);fl=ftell(f);fseek(f,0,SEEK_SET);
  /*
  ** Idiot check for insanely large or small files
  */
  if(fl >= 0x10000000) {
    fclose(f);
    return "PSF too large - likely corrupt";
  }
  if(fl < 0x10) {
    fclose(f);
    return "PSF too small - likely corrupt";
  }
  fread(hdr, 1, 0x10,f);
  if(memcmp(hdr, "PSF\x01", 4)) {
    fclose(f);
    return "Invalid PSF format";
  }
  reserved_size = get32lsb(hdr +  4);
  exe_size      = get32lsb(hdr +  8);
  exe_crc       = get32lsb(hdr + 12);
  printf("rsvd=%08X exe=%08X execrc=%08X\n", reserved_size, exe_size, exe_crc);
  /*
  ** Consistency check on section lengths
  */
  if(
    (reserved_size > fl) ||
    (exe_size > fl) ||
    ((16+reserved_size+exe_size) > fl)
  ) {
    fclose(f);
    return "PSF header is inconsistent";
  }

  zexebuf=malloc(exe_size);
  if(!zexebuf) {
    fclose(f);
    return "Out of memory reading the file";
  }
  fseek(f, 16+reserved_size, SEEK_SET);
  r = fread(zexebuf, 1, exe_size, f);
  if(r != exe_size) {
    printf("read(%d) returned %d\n",exe_size, r);
    return "Unexpected EOF";
  }
  fclose(f);

  calc_zexe_crc = crc32(crc32(0L, Z_NULL, 0), zexebuf, exe_size);
  printf("calculated crc = %08X\n", calc_zexe_crc);

  if(exe_crc != calc_zexe_crc) {
    free(zexebuf);
    return "CRC failure - executable data is corrupt";
  }

  destlen = exebuffersize;

  if(uncompress(exebuffer, &destlen, zexebuf, exe_size) != Z_OK) {
    free(zexebuf);
    return "Decompression failed";
  }

  /*
  ** Okay, if the decompression worked, then the file is PROBABLY not corrupt.
  ** Hooray for that.
  */

  free(zexebuf);

  return NULL;
}

int main(int argc, char **argv) {
  const char *complaint = NULL;

  unsigned char *program = NULL;
  void *psx_state = NULL;
  void *samplebuffer = NULL;
  uint32 textsize;
  FILE *outfile;
  int r;
  uint32 nsamples;
  int samples_per_second = 44100;
  int seconds = 60;

  if(argc != 3){
    printf("usage: %s in.psf out.raw\n", argv[0]);
    return 1;
  }

  program = malloc(0x200000);
  if(!program) abort();

  printf("(reading psf)\n");

  complaint = psf_read_exe(argv[1], program, 0x200000);
  if(complaint) {
    printf("%s\n", complaint);
    return 1;
  }

  textsize = get32lsb(program+0x1C);
  if(textsize > 0x200000 || (textsize + 0x800) > 0x200000) {
    printf("PSX EXE too large\n");
    return 1;
  }

  printf("(initializing emulation)\n");

  psx_init();

  printf("(setting up emulation: %s)\n", psx_getversion());

  psx_state = malloc(psx_get_state_size(1));
  if(!psx_state) abort();

  psx_clear_state(psx_state, 1);

  psx_upload_psxexe(psx_state, program, 0x800 + textsize);

  outfile = fopen(argv[2], "wb");
  if(!outfile) { printf("couldn't open %s\n", argv[2]); return 1; }

  printf("(playing to %s)\n", argv[2]);

  samplebuffer = malloc(2 * 2 * samples_per_second);

//  printf("odometer=%d\n",(int)psx_get_odometer(psx_state));

  for(; seconds; seconds--) {
    nsamples = samples_per_second;
    r = psx_execute(
      psx_state,
      0x7FFFFFFF,
      samplebuffer,
      &nsamples,
      0
    );
    if(r < 0) {
      printf("emulation error\n");
//      printf("odometer=%d\n",(int)psx_get_odometer(psx_state));

      return 1;
    }
    fwrite(samplebuffer, 1, 2 * 2 * samples_per_second, outfile);
  }

  fclose(outfile);

  return 0;
}
