/////////////////////////////////////////////////////////////////////////////

#include <kernel.h>
#include <errno.h>

/////////////////////////////////////////////////////////////////////////////

#include "heio.h"
#include "hefile.h"

/////////////////////////////////////////////////////////////////////////////

static char devicename[] = "hefile";
static char devicedesc[] = "Virtual PSF2 device";

/////////////////////////////////////////////////////////////////////////////

volatile int hefile_debugmode = 0;

#define MAXFILES (32)

struct FDTABLE_ENTRY {
  int kernelfd;
  int emufd;
};

struct FDTABLE_ENTRY hefile_fdtable[MAXFILES];

//
// Initialize table
//
static void fdtable_init(void) {
  int i;
  for(i = 0; i < MAXFILES; i++) {
    hefile_fdtable[i].emufd = -1;
  }
}

//
// Returns index, or negative on error
//
int hefile_fdtable_find_free_index(void) {
  int i;
  for(i = 0; i < MAXFILES; i++) {
    if(hefile_fdtable[i].emufd < 0) return i;
  }
  return -1;
}

//
// Returns index, or negative on error
//
int hefile_fdtable_lookup_index(int kernelfd) {
  int i;
  for(i = 0; i < MAXFILES; i++) {
    if(hefile_fdtable[i].emufd < 0) continue;
    if(hefile_fdtable[i].kernelfd == kernelfd) return i;
  }
  return -1;
}

/////////////////////////////////////////////////////////////////////////////
//
// generic I/O handler for emu calls
//
int hefile_io(int kernelfd, int type, char *ptr, int arg1, int arg2) {
  int oldstat;
  int r = -(EIO);
  int index, emufd;
  CpuSuspendIntr(&oldstat);
  // Prepare based on the command type
  switch(type) {
  case HEIO_OPEN:
    // Find a free index in the fdtable
    index = hefile_fdtable_find_free_index();
    if(index < 0) { r = -(ENOMEM); goto done; }
    emufd = 0;
    break;
  default:
    // Try to find kernelfd in the table
    index = hefile_fdtable_lookup_index(kernelfd);
    if(index < 0) { r = -(EBADF); goto done; }
    emufd = hefile_fdtable[index].emufd;
    break;
  }
  // Try the command
  r = heio(type, emufd, ptr, arg1, arg2);
  // Do some extra stuff depending on the command type
  switch(type) {
  case HEIO_OPEN:
    // If it failed, return failure
    if(r < 0) goto done;
    // Otherwise store the new entry
    hefile_fdtable[index].kernelfd = kernelfd;
    hefile_fdtable[index].emufd = r;
    r = kernelfd;
    break;
  case HEIO_CLOSE:
    // In all cases: remove the mapping from the fdtable
    hefile_fdtable[index].emufd = -1;
    // Return zero for success
    r = 0;
    break;
  }
done:
  CpuResumeIntr(oldstat);
  return r;
}

/////////////////////////////////////////////////////////////////////////////
//
// ioman handlers
//
int hefile_ioinit(void) { return 0; }
int hefile_ioquit(void) { return 0; }

int hefile_open(int kernelfd, char *filename, int mode) {
  if(hefile_debugmode) {
    Kprintf("[open(\"hefile:%s\", %d)]\n", filename, mode);
  }
  if(
    (filename[0] == 'd') &&
    (filename[1] == 'e') &&
    (filename[2] == 'b') &&
    (filename[3] == 'u') &&
    (filename[4] == 'g') &&
    (filename[5] == 'o')
  ) {
    if(filename[6] == 'n' && filename[7] == 0) {
      hefile_debugmode = 1;
      return -2;
    } else if(filename[6] == 'f' && filename[7] == 'f' && filename[8] == 0) {
      hefile_debugmode = 0;
      return -2;
    }
  }
  return hefile_io(kernelfd, HEIO_OPEN, filename, mode, 0);
}

int hefile_close(int kernelfd) {
  return hefile_io(kernelfd, HEIO_CLOSE, NULL, 0, 0);
}

int hefile_read(int kernelfd, char *buffer, int size) {
  return hefile_io(kernelfd, HEIO_READ, buffer, size, 0);
}

int hefile_write(int kernelfd, char *buffer, int size) {
  return hefile_io(kernelfd, HEIO_WRITE, buffer, size, 0);
}

int hefile_lseek(int kernelfd, int offset, int whence) {
  return hefile_io(kernelfd, HEIO_LSEEK, NULL, offset, whence);
}

int hefile_dummy(void) {
  return -(EIO);
}

/////////////////////////////////////////////////////////////////////////////
//
// Structure to pass to AddDrv
//
struct DEVICEINFO {
  char *name;
  unsigned flag1;
  unsigned flag2;
  char *description;
  void **function_table;
};

/////////////////////////////////////////////////////////////////////////////
//
// Hopefully these will link to ioman
//
int DelDrv(char *name);
int AddDrv(struct DEVICEINFO *device);

/////////////////////////////////////////////////////////////////////////////
//
// Function tables for hefile
//
void *hefile_function_table[17] = {
  hefile_ioinit,
  hefile_ioquit,
  hefile_dummy,
  hefile_open,
  hefile_close,
  hefile_read,
  hefile_write,
  hefile_lseek,
  hefile_dummy,
  hefile_dummy,
  hefile_dummy,
  hefile_dummy,
  hefile_dummy,
  hefile_dummy,
  hefile_dummy,
  hefile_dummy,
  hefile_dummy
};

/////////////////////////////////////////////////////////////////////////////
//
// hefile device info struct
//
static struct DEVICEINFO deviceinfo = {
  devicename,
  0x10,
  1,
  devicedesc,
  hefile_function_table
};

/////////////////////////////////////////////////////////////////////////////
//
// returns negative on error
//
int hefile_init(void) {
  int r;
  fdtable_init();
  DelDrv(devicename);
  r = AddDrv(&deviceinfo);
  if(r < 0) {
    Kprintf("hefile: error: device registration failed (%d)\n", r);
    return -1;
  }
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
