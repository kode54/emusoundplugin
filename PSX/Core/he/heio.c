/////////////////////////////////////////////////////////////////////////////

#include "heio.h"

/////////////////////////////////////////////////////////////////////////////

#define TRIGGER_ADDRESS (0xBFC17120)

struct EMUCALL {
  int type;
  int emufd;
  void *address;
  int arg1;
  int arg2;
};

int heio(int type, int emufd, void *address, int arg1, int arg2) {
  volatile struct EMUCALL cmd;
  cmd.type = type;
  cmd.emufd = emufd;
  cmd.address = address;
  cmd.arg1 = arg1;
  cmd.arg2 = arg2;
  (*((volatile unsigned*)(TRIGGER_ADDRESS))) =
    ((unsigned)(&cmd));
  return cmd.type;
}

/////////////////////////////////////////////////////////////////////////////
