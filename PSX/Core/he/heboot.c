/////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <kernel.h>

#include "heboot.h"
#include "heio.h"

/////////////////////////////////////////////////////////////////////////////

static char startup[] = "hefile:/psf2.irx";

extern char *he_banner;

/////////////////////////////////////////////////////////////////////////////

void heboot_loader(void) {
  int r, module_result;
  //
  // Print banner
  //
  printf("Highly Experimental PS2 environment %s\n", he_banner);
  //
  // Run the PSF2 startup file
  //
  r = LoadStartModule(startup, 1, "", &module_result);
  if(r < 0) {
    printf("heboot: unable to run %s: ", startup);
    switch(r) {
    case KE_LINKERR:        printf("link error\n"); break;
    case KE_ILLEGAL_OBJECT: printf("illegal object\n"); break;
    case KE_NOFILE:         printf("file not found\n"); break;
    case KE_FILEERR:        printf("error while reading file\n"); break;
    case KE_NO_MEMORY:      printf("out of memory\n"); break;
    default:                printf("(code %d)\n", r); break;
    }
  }
  //
  // Quit
  //
  printf("heboot: shutting down\n");
  heio(HEIO_QUIT, 0, NULL, 0, 0);
}

/////////////////////////////////////////////////////////////////////////////

void heboot(void) {
  struct ThreadParam param;
  int r, tid;
  param.attr         = TH_C;
  param.entry        = heboot_loader;
  param.initPriority = 32;
  param.stackSize    = 0x4000;
  param.option       = 0;
  tid = CreateThread(&param);
  if(tid < 0) {
    Kprintf("heboot: error creating thread (%d)\n", tid);
    return;
  }

  r = StartThread(tid, 0);
  if(r < 0) {
    Kprintf("heboot: error starting thread (%d)\n", r);
    return;
  }
}

/////////////////////////////////////////////////////////////////////////////

