/////////////////////////////////////////////////////////////////////////////

#include "heio.h"
#include "hetty.h"

/////////////////////////////////////////////////////////////////////////////
//
// tty write code
//
static int hetty_iocallback(int fd, char *buf, int len) {
  return heio(HEIO_TTYWRITE, 0, buf, len, 0);
}

/////////////////////////////////////////////////////////////////////////////
//
// try to hack the tty write function from a production copy of ioman
// returns < 0 on error
//
int hetty_init(void) {
  unsigned *start = (unsigned*)0x800;
  unsigned *end   = (unsigned*)hetty_init;
  unsigned ascptr;
  for(; start < end; start++) {
    if(*start == 0x797474) break; // "tty"
  }
  if(start >= end) return -1;
  ascptr = (unsigned)start;
  for(; start < end; start++) {
    if(*start == ascptr) break; // ptr to device name "tty"
  }
  if(start >= end) return -1;
  // advance by 4 and get the function table address
  start += 4;
  start = (unsigned*)(*start);
  // add 6 to get to the write spot
  start += 6;
  // overwrite the pointer
  *start = (unsigned)hetty_iocallback;
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
