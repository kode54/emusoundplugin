#ifndef __HEIO_H__
#define __HEIO_H__

/////////////////////////////////////////////////////////////////////////////

#define HEIO_TTYWRITE (0)
#define HEIO_QUIT     (1)
#define HEIO_OPEN     (3)
#define HEIO_CLOSE    (4)
#define HEIO_READ     (5)
#define HEIO_WRITE    (6)
#define HEIO_LSEEK    (7)

int heio(int type, int emufd, void *address, int arg1, int arg2);

/////////////////////////////////////////////////////////////////////////////

#endif
