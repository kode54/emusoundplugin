// PSFAudit.cpp: implementation of the CPSFAudit class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PSFAudit.h"

#include "../Core/psx.h"
#include "../Core/iop.h"
#include "../Core/r3000.h"
#include "../Core/spu.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPSFAudit::CPSFAudit()
{
  m_bOpen = FALSE;
}

CPSFAudit::~CPSFAudit()
{
  Close();
}

void CPSFAudit::Open()
{
  //
  // Kill old thread, if there was one
  //
  Stop();
  //
  // Allocate an audit state if it doesn't already exist
  //
  m_aState.SetSize(psx_get_state_size(1));
  psx_clear_state(m_aState.GetData(), 1);
  //
  // And an audit map
  //
  m_aMap.SetSize(0x200000);
  memset(m_aMap.GetData(), 0, 0x200000);
  //
  // Register the audit map and set default compatibility
  //
  iop_register_map_for_auditing(psx_get_iop_state(m_aState.GetData()), m_aMap.GetData());
  iop_set_compat(psx_get_iop_state(m_aState.GetData()), IOP_COMPAT_HARSH);
  /*
  ** Reset live audit info
  */
  m_dLiveSeconds = 0.0;
  m_dLiveUsedSeconds = 0.0;
  m_nLiveBytes = 0;
  m_bErrorOccurred = FALSE;
  m_bOpen = TRUE;
}

void CPSFAudit::Stop()
{
  m_dwThreadKillFlag = 1;
  m_csThread.Lock();
  m_csThread.Unlock();
}

void CPSFAudit::Start()
{
  Stop();
  m_evThreadStarted.ResetEvent();
  m_dwThreadKillFlag = 0;
  AfxBeginThread(CPSFAudit__Thread, (LPVOID)this);
  m_evThreadStarted.Lock();
}

double CPSFAudit::GetSeconds()
{
  m_csLiveInfo.Lock();
  double r = m_dLiveSeconds;
  m_csLiveInfo.Unlock();
  return r;
}

double CPSFAudit::GetUsedSeconds()
{
  m_csLiveInfo.Lock();
  double r = m_dLiveUsedSeconds;
  m_csLiveInfo.Unlock();
  return r;
}

int CPSFAudit::GetBytes() {
  m_csLiveInfo.Lock();
  int r = m_nLiveBytes;
  m_csLiveInfo.Unlock();
  return r;
}

void CPSFAudit::Close()
{
  Stop();
  m_aState.SetSize(0);
  m_aMap.SetSize(0);
  m_bOpen = FALSE;
}

////////////////////////////////////////////////////////////////////////////////
/*
** Auditing thread
*/
static UINT AFX_CDECL CPSFAudit__Thread(LPVOID lpParam) {
  CPSFAudit *me = (CPSFAudit*)lpParam;
  ASSERT(me);
  ASSERT(me->m_bOpen);
  /*
  ** Signal thread start
  */
  me->m_csThread.Lock();
  me->m_evThreadStarted.SetEvent();
  /*
  ** Audit loop
  */
  INT64 my_odometer = 0;
  while(!me->m_dwThreadKillFlag) {
    /*
    ** Execute
    */
    signed short samplebuffer[2*4410];
    uint32 uSoundSamples = 4410;
    int r = psx_execute(me->m_aState.GetData(), 0x7FFFFFFF, samplebuffer, &uSoundSamples, 0);
    int nBytes = iop_get_bytes_used_in_audit(psx_get_iop_state(me->m_aState.GetData()));
    /*
    ** Update live info
    */
    me->m_csLiveInfo.Lock();
    if(r < 0) {
      me->m_bErrorOccurred = TRUE;
      me->m_strAuditError.Format(L"Emulation error (PC=%08X)",
        r3000_getreg(iop_get_r3000_state(psx_get_iop_state(me->m_aState.GetData())), R3000_REG_PC)
      );
    } else {
      my_odometer += r;
    }
    me->m_dLiveSeconds = (((double)my_odometer) / 33868800.0);
    if(nBytes > me->m_nLiveBytes) {
      me->m_nLiveBytes = nBytes;
      me->m_dLiveUsedSeconds = me->m_dLiveSeconds;
    }
    me->m_csLiveInfo.Unlock();
  }
  /*
  ** Signal thread end; return
  */
  me->m_csThread.Unlock();
  return 0;
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSFAudit::UploadProgram(LPVOID lpSrc, DWORD dwSize)
{
  ASSERT(m_bOpen);
  ASSERT(lpSrc);

//TRACE("uploading program %08X '%s' size %08X\n",lpSrc,lpSrc,dwSize);
//{FILE*f=fopen("testing","wb");ASSERT(f);fwrite(lpSrc,1,dwSize,f);fclose(f);}

  if(!dwSize) {
    m_strLastError = L"Can't audit empty program";
    return FALSE;
  }
  if(psx_upload_psxexe(m_aState.GetData(), lpSrc, dwSize)) {
    m_strLastError = L"psx_upload_program failed";
    return FALSE;
  }
  return TRUE;
}

void CPSFAudit::Upload(DWORD dwDestAddress, LPVOID lpSrc, DWORD dwSize)
{
  ASSERT(m_bOpen);
  ASSERT(lpSrc);
  if(!dwSize) return;
  iop_upload_to_ram(psx_get_iop_state(m_aState.GetData()), dwDestAddress, lpSrc, dwSize);
}

void CPSFAudit::SetPC(DWORD dwValue)
{
  ASSERT(m_bOpen);
  r3000_setreg(iop_get_r3000_state(psx_get_iop_state(m_aState.GetData())), R3000_REG_PC, dwValue);
}

void CPSFAudit::SetSP(DWORD dwValue)
{
  ASSERT(m_bOpen);
  r3000_setreg(iop_get_r3000_state(psx_get_iop_state(m_aState.GetData())), R3000_REG_GEN+29, dwValue);
}

////////////////////////////////////////////////////////////////////////////////

void CPSFAudit::GrowBy(int nBytes)
{
  ASSERT(m_bOpen);
  LPBYTE lpMap = m_aMap.GetData();
  int i, nGrowValue;
  /* Forwards */
  nGrowValue = 0;
  for(i = 0; i <= 0x1FFFFF; i++) {
    if(lpMap[i] == IOP_AUDIT_READ) {
      nGrowValue = nBytes;
    } else {
      if(nGrowValue) {
        lpMap[i] = IOP_AUDIT_READ;
        nGrowValue--;
      }
    }
  }
  /* Backwards */
  nGrowValue = 0;
  for(i = 0x1FFFFF; i >= 0; i--) {
    if(lpMap[i] == IOP_AUDIT_READ) {
      nGrowValue = nBytes;
    } else {
      if(nGrowValue) {
        lpMap[i] = IOP_AUDIT_READ;
        nGrowValue--;
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSFAudit::IsWordUsed(DWORD dwAddress)
{
  ASSERT(m_bOpen);
  dwAddress &= 0x1FFFFC;
  LPBYTE lpMap = m_aMap.GetData();
  if(lpMap[dwAddress + 0] == IOP_AUDIT_READ) return TRUE;
  if(lpMap[dwAddress + 1] == IOP_AUDIT_READ) return TRUE;
  if(lpMap[dwAddress + 2] == IOP_AUDIT_READ) return TRUE;
  if(lpMap[dwAddress + 3] == IOP_AUDIT_READ) return TRUE;
  return FALSE;
}

////////////////////////////////////////////////////////////////////////////////

void CPSFAudit::SetCompat(int nCompat)
{
  ASSERT(m_bOpen);
  iop_set_compat(psx_get_iop_state(m_aState.GetData()), nCompat);
}

////////////////////////////////////////////////////////////////////////////////

void CPSFAudit::ForceWordUsed(DWORD dwAddress)
{
  ASSERT(m_bOpen);
  dwAddress &= 0x1FFFFC;
  LPBYTE lpMap = m_aMap.GetData();
  lpMap[dwAddress + 0] = IOP_AUDIT_READ;
  lpMap[dwAddress + 1] = IOP_AUDIT_READ;
  lpMap[dwAddress + 2] = IOP_AUDIT_READ;
  lpMap[dwAddress + 3] = IOP_AUDIT_READ;
}

BOOL CPSFAudit::DidAuditErrorOccur()
{
  m_csLiveInfo.Lock();
  BOOL b = m_bErrorOccurred;
  m_csLiveInfo.Unlock();
  return b;
}

