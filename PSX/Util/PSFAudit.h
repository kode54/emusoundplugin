#pragma once

class CPSFAudit  
{
public:
	BOOL DidAuditErrorOccur();
	void ForceWordUsed(DWORD dwAddress);
	void SetCompat(int nCompat);
	BOOL IsWordUsed(DWORD dwAddress);
	void GrowBy(int nBytes);
	BOOL UploadProgram(LPVOID lpSrc, DWORD dwSize);
	void Upload(DWORD dwDestAddress, LPVOID lpSrc, DWORD dwSize);
    void SetPC(DWORD dwValue);
    void SetSP(DWORD dwValue);
	void Open();
	void Close();
	void Start();
	void Stop();
	double GetSeconds();
	double GetUsedSeconds();
	int GetBytes();
	CPSFAudit();
	virtual ~CPSFAudit();

    CStringW GetLastError () const { return m_strLastError; }
    CStringW GetAuditError() const { return m_strAuditError; }

private:
	BOOL m_bOpen;
	CStringW m_strLastError;
	CStringW m_strAuditError;
	BOOL m_bErrorOccurred;
	DWORD m_dwThreadKillFlag;

    friend UINT AFX_CDECL CPSFAudit__Thread(LPVOID lpParam);

    CCriticalSection m_csLiveInfo;
	double m_dLiveSeconds;
	double m_dLiveUsedSeconds;
	int m_nLiveBytes;
	CEvent m_evThreadStarted;
	CCriticalSection m_csThread;
//	LPBYTE m_lpMap;
//  void *m_pState;

    CByteArray m_aState;
    CByteArray m_aMap;
};
