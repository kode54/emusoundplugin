#pragma once

#include "../../PSF/Util/PSF.h"

#include "../../MFCUtil/File.h"

class CPSXEXE  
{
public:
	void SetRegionData(CByteArray &data);
	void GetRegionData(CByteArray &data);
	void SuperimposeFrom(CPSXEXE *pSrcEXE);
	BOOL Loaded();
	void SetSP(DWORD dwValue);
	void SetPC(DWORD dwValue);
	DWORD GetSP();
	DWORD GetPC();
	void CopyFrom(CPSXEXE *pSrcEXE);
	LPBYTE GetEXEBuffer();
	LPBYTE GetTextBuffer();
	DWORD GetEXESize();
	DWORD GetTextSize();
	DWORD GetTextStart();
	BOOL WriteToPSF(CPSF &psf, UINT uMethod);
	BOOL NoEXE();
	void WriteAddressRange(DWORD dwDest, LPVOID lpSrc, DWORD dwSize);
	void ReadAddressRange(LPVOID lpDest, DWORD dwSrc, DWORD dwSize);
	BOOL ReadFromPSF(CPSF &psf, BOOL bStrict);
	BOOL ReadFromEXEFile(File f, BOOL bStrict);
	BOOL WriteToEXEFile(File f);
    CStringW GetLastError() { return m_strLastError; }
	void ReleaseData();
	CPSXEXE();
	virtual ~CPSXEXE();

private:
	void ScrubFormat();
	BOOL VerifyFormat();
	CByteArray m_aData;
	void GrowEXEEnd(DWORD dwEnd);
	void GrowEXEStart(DWORD dwStart);
	void SetTextSize(DWORD dwTextSize);
	void SetTextStart(DWORD dwTextStart);
	DWORD GetSafeTextStart();
	DWORD GetSafeTextSize();
	CStringW m_strLastError;

    BOOL FileError(File &f, CStringW message = L"");

};
