// PSXEXE.cpp: implementation of the CPSXEXE class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "PSXEXE.h"

#include "../../MFCUtil/Endian.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPSXEXE::CPSXEXE()
{
  m_aData.SetSize(0);
}

CPSXEXE::~CPSXEXE()
{
}

BOOL CPSXEXE::FileError(File &f, CStringW message) {
    if(message.IsEmpty()) {
        m_strLastError = f.GetLastError();
    } else {
        m_strLastError = message;
    }
    f.Close();
    m_aData.SetSize(0);
    return FALSE;
}

BOOL CPSXEXE::ReadFromEXEFile(File f, BOOL bStrict) {
    m_aData.SetSize(0);

    if(!f.Open(File::READ)) return FileError(f);

    DWORD l = (DWORD)f.Length();
    if(l < 0x800) return FileError(f, L"PS-X EXE file is too small");
    if(l > 0x1F0800) {
        if(bStrict) {
            return FileError(f, L"PS-X EXE file is too large");
        } else {
            l = 0x1F0800;
        }
    }
    //
    // Truly pedantic check: File size must be 2048-byte aligned
    //
//    if(bStrict && (l & 0x7FF)) {
//      m_strLastError.Format(L"PS-X EXE (0x%X) is not a multiple of 2048 bytes", l);
//      return FileError(f, m_strLastError);
//    }
    m_aData.SetSize(l);
    if(f.Read(m_aData.GetData(), l) != l) return FileError(f);
    f.Close();

    //
    // ScrubFormat if not Strict
    //
    if(!bStrict) ScrubFormat();

    //
    // Verify the format of the file
    //
    if(!VerifyFormat()) {
        m_aData.SetSize(0);
        return FALSE;
    }

    //
    // Success
    //
    return TRUE;
}

BOOL CPSXEXE::ReadFromPSF(CPSF &psf, BOOL bStrict)
{
    //
    // Version check - technically what we are reading is not a PSXEXE
    // unless the version is 0x01
    //
    int v = psf.GetVersion();
    if(v < 0) {
        m_strLastError = L"CPSXEXE::ReadFromPSF - Unknown version (program error)";
        return FALSE;
    }
    if(v != 0x01) {
        m_strLastError = L"Improper version - not a PSF1 file";
        return FALSE;
    }

    DWORD dwSize = 0x1F0800;
    m_aData.SetSize(0);
    m_aData.SetSize(dwSize);
    BOOL b = psf.GetProgramData(m_aData.GetData(), &dwSize);
    if(!b) {
        //
        // TODO: maybe handle size errors more elegantly.
        //
        // However, the PSF spec says the uncompressed EXE can't be any
        // bigger than the buffer we already made, so even with the current
        // handling, we're still playing by the rules. it's all good.
        //
        m_strLastError = psf.GetLastError();
        m_aData.SetSize(0);
        return FALSE;
    }

    //
    // Truly pedantic check: EXE size must be 2048-byte aligned
    //
//  if(bStrict && (dwSize & 0x7FF)) {
//    m_strLastError.Format(L"PS-X EXE (0x%X) is not a multiple of 2048 bytes", dwSize);
//    m_aData.SetSize(0);
//    return FALSE;
//  }

    m_aData.SetSize(dwSize);

    //
    // ScrubFormat if not Strict
    //
    if(!bStrict) ScrubFormat();

    //
    // Verify the format of the file
    //
    if(!VerifyFormat()) {
        m_aData.SetSize(0);
        return FALSE;
    }

    return TRUE;
}

void CPSXEXE::ReleaseData()
{
    m_aData.SetSize(0);
}

void CPSXEXE::ReadAddressRange(LPVOID lpDest, DWORD dwSrc, DWORD dwSize)
{
  if(NoEXE()) {
    memset(lpDest, 0, dwSize);
    return;
  }
  DWORD dwTextStart = GetSafeTextStart();
  DWORD dwTextSize  = GetSafeTextSize();
  while(dwSize) {
    DWORD dwAdvance = 0;
    dwSrc &= 0x1FFFFF;
    DWORD dwCurrentOffsetInEXE = (dwSrc - dwTextStart) & 0x1FFFFF;
    /*
    ** If inside the EXE, copy to the end of it
    */
    if(dwCurrentOffsetInEXE < dwTextSize) {
      dwAdvance = dwTextSize - dwCurrentOffsetInEXE;
      if(dwAdvance > dwSize) { dwAdvance = dwSize; }
      memcpy(lpDest, m_aData.GetData() + 0x800 + dwCurrentOffsetInEXE, dwAdvance);
    } else {
      dwAdvance = 0x200000 - dwCurrentOffsetInEXE;
      if(dwAdvance > dwSize) { dwAdvance = dwSize; }
      memset(lpDest, 0, dwAdvance);
    }
    lpDest = (((LPBYTE)lpDest) + dwAdvance);
    dwSrc += dwAdvance;
    dwSize -= dwAdvance;
  }
}

DWORD CPSXEXE::GetSafeTextSize()
{
  if(NoEXE()) return 0;
  DWORD d = Util::Get32LSB(m_aData.GetData() + 0x1C);
  if(d > (DWORD)(m_aData.GetSize() - 0x800)) {
    d = (DWORD)(m_aData.GetSize() - 0x800);
  }
  return d;
}

void CPSXEXE::WriteAddressRange(DWORD dwDest, LPVOID lpSrc, DWORD dwSize)
{
  if(NoEXE()) return;
  while(dwSize) {
    dwDest &= 0x1FFFFF;
    DWORD dwAdvance = 0x200000 - dwDest;
    if(dwAdvance > dwSize) { dwAdvance = dwSize; }
    GrowEXEStart(dwDest);
    GrowEXEEnd(dwDest + dwAdvance);
    DWORD dwTextStart = GetSafeTextStart();
    DWORD dwTextSize  = GetSafeTextSize();
    if(dwTextStart > dwDest) {
      DWORD dwDefecit = dwTextStart - dwDest;
      if(dwDefecit > dwAdvance) { dwDefecit = dwAdvance; }
      dwDest += dwDefecit;
      lpSrc = (((LPBYTE)lpSrc) + dwDefecit);
      dwSize -= dwDefecit;
      dwAdvance -= dwDefecit;
    }
    DWORD dwOffsetInData = dwDest - dwTextStart;
    if(dwOffsetInData < dwTextSize) {
      if(dwAdvance) {
        DWORD dwMaxAdvance = dwTextSize - dwOffsetInData;
        if(dwAdvance > dwMaxAdvance) { dwAdvance = dwMaxAdvance; }
      }
      if(dwAdvance) {
        memcpy(m_aData.GetData() + 0x800 + dwOffsetInData, lpSrc, dwAdvance);
      }
    }
    dwDest += dwAdvance;
    lpSrc = (((LPBYTE)lpSrc) + dwAdvance);
    dwSize -= dwAdvance;
  }
}

DWORD CPSXEXE::GetSafeTextStart()
{
  if(NoEXE()) return 0;
  return Util::Get32LSB(m_aData.GetData() + 0x18) & 0x1FFFFF;
}

BOOL CPSXEXE::NoEXE()
{
  return (m_aData.GetSize() < 0x800);
}

void CPSXEXE::SetTextStart(DWORD dwTextStart)
{
  if(NoEXE()) return;
  Util::Put32LSB(m_aData.GetData() + 0x18,
    0x80000000 | (dwTextStart & 0x001FFFFF)
  );
}

void CPSXEXE::SetTextSize(DWORD dwTextSize)
{
  if(NoEXE()) return;
  Util::Put32LSB(m_aData.GetData() + 0x1C, dwTextSize);
}

void CPSXEXE::GrowEXEStart(DWORD dwStart)
{
  if(NoEXE()) return;
  dwStart &= 0x1FFFFF;
  if(dwStart < 0x10000) dwStart = 0x10000;
  DWORD dwTextStart = GetSafeTextStart();
  DWORD dwTextSize  = GetSafeTextSize();
  if(dwStart >= dwTextStart) return;
  DWORD dwGrowBy = dwTextStart - dwStart;
  m_aData.SetSize(0x800 + dwTextSize + dwGrowBy);
  memmove(m_aData.GetData() + 0x800 + dwGrowBy, m_aData.GetData() + 0x800, dwTextSize);
  memset(m_aData.GetData() + 0x800, 0, dwGrowBy);
  SetTextStart(dwStart);
  SetTextSize(dwTextSize + dwGrowBy);
}

void CPSXEXE::GrowEXEEnd(DWORD dwEnd)
{
  if(NoEXE()) return;
  dwEnd &= 0x1FFFFF;
  DWORD dwTextStart = GetSafeTextStart();
  DWORD dwTextSize  = GetSafeTextSize();
  if(dwEnd <= (dwTextStart + dwTextSize)) return;
  DWORD dwNewTextSize = dwEnd - dwTextStart;
  m_aData.SetSize(0x800 + dwNewTextSize);
  memset(m_aData.GetData() + 0x800 + dwTextSize, 0, dwNewTextSize - dwTextSize);
  SetTextSize(dwNewTextSize);
}

BOOL CPSXEXE::WriteToEXEFile(File f)
{
    if(NoEXE()) {
        m_strLastError = L"Tried to write nonexistent EXE data";
        return FALSE;
    }

    if(!f.Open(File::WRITE)) {
        m_strLastError = f.GetLastError();
        return FALSE;
    }

    if(f.Write(m_aData.GetData(), m_aData.GetSize()) != m_aData.GetSize()) {
        m_strLastError = f.GetLastError();
        return FALSE;
    }

    f.Close();

    return TRUE;
}

BOOL CPSXEXE::WriteToPSF(CPSF &psf, UINT uMethod)
{
    if(NoEXE()) {
        m_strLastError = L"Tried to write nonexistent EXE data";
        return FALSE;
    }

    //
    // Version check - version MUST be initialized to 0x01 first
    //
    int v = psf.GetVersion();
    if(v < 0) {
        m_strLastError = L"CPSXEXE::WriteToPSF - Unknown version (program error)";
        return FALSE;
    }
    if(v != 0x01) {
        m_strLastError = L"Improper version - not a PSF1 file";
        return FALSE;
    }

    BOOL b = psf.SetProgramData(m_aData.GetData(), m_aData.GetSize(), uMethod);

    //
    // should have worked with no retries since all buffer sizes are
    // taken care of by MaxCompressedSize
    //
    // so we can just error out if the SetProgramData call failed
    //
    if(!b) {
        m_strLastError = psf.GetLastError();
        return FALSE;
    }

    return TRUE;
}

DWORD CPSXEXE::GetTextStart()
{
  if(NoEXE()) return 0;
  return Util::Get32LSB(m_aData.GetData() + 0x18);
}

DWORD CPSXEXE::GetTextSize()
{
  if(NoEXE()) return 0;
  return GetSafeTextSize();
}

DWORD CPSXEXE::GetEXESize()
{
  if(NoEXE()) return 0;
  return GetSafeTextSize() + 0x800;
}

LPBYTE CPSXEXE::GetTextBuffer()
{
  if(NoEXE()) return NULL;
  return m_aData.GetData() + 0x800;
}

LPBYTE CPSXEXE::GetEXEBuffer()
{
  if(NoEXE()) return NULL;
  return m_aData.GetData();
}

void CPSXEXE::CopyFrom(CPSXEXE *pSrcEXE)
{
  ASSERT(pSrcEXE);
  DWORD dwSize = pSrcEXE->m_aData.GetSize();
  m_aData.SetSize(dwSize);
  if(dwSize) memcpy(m_aData.GetData(), pSrcEXE->m_aData.GetData(), dwSize);
}

DWORD CPSXEXE::GetPC()
{
  if(NoEXE()) return 0;
  return Util::Get32LSB(m_aData.GetData() + 0x10);
}

DWORD CPSXEXE::GetSP()
{
  if(NoEXE()) return 0;
  return Util::Get32LSB(m_aData.GetData() + 0x30);
}

void CPSXEXE::SetPC(DWORD dwValue)
{
  if(NoEXE()) return;
  Util::Put32LSB(m_aData.GetData() + 0x10, dwValue);
}

void CPSXEXE::SetSP(DWORD dwValue)
{
  if(NoEXE()) return;
  Util::Put32LSB(m_aData.GetData() + 0x30, dwValue);
}

BOOL CPSXEXE::Loaded()
{
  return (!(NoEXE()));
}

void CPSXEXE::SuperimposeFrom(CPSXEXE *pSrcEXE)
{
  if(!pSrcEXE) return;
  if(pSrcEXE->NoEXE()) return;
  if(NoEXE()) {
    CopyFrom(pSrcEXE);
    return;
  }
  WriteAddressRange(
    pSrcEXE->GetTextStart(),
    pSrcEXE->GetTextBuffer(),
    pSrcEXE->GetTextSize()
  );
}

BOOL CPSXEXE::VerifyFormat()
{
    if(m_aData.GetSize() < 0x800) {
        m_strLastError = L"PS-X EXE data is too small";
        return FALSE;
    }
    if(m_aData.GetSize() > 0x1F0800) {
        m_strLastError = L"PS-X EXE data is too large";
        return FALSE;
    }
    if(memcmp(m_aData.GetData(), "PS-X EXE", 8)) {
        m_strLastError = L"Not a valid PS-X EXE";
        return FALSE;
    }

    DWORD dwHeaderTextSize = Util::Get32LSB(m_aData.GetData() + 0x1C);
    if(dwHeaderTextSize != (m_aData.GetSize() - 0x800)) {
        m_strLastError.Format(
            L"Text section size in header (0x%X) does not match actual size (0x%X)",
            dwHeaderTextSize,
            (m_aData.GetSize() - 0x800)
        );
        return FALSE;
    }

    // TODO: other checks?

    return TRUE;
}

void CPSXEXE::ScrubFormat()
{
  int s = m_aData.GetSize();
  if(s < 0) s = 0;
  if(s < 0x800) {
    m_aData.SetSize(0x800);
    memset(m_aData.GetData() + s, 0, 0x800 - s);
  } else if(s > 0x1F0800) {
    m_aData.SetSize(0x1F0800);
  }
  memcpy(m_aData.GetData(), "PS-X EXE", 8);
  Util::Put32LSB(m_aData.GetData() + 0x1C, m_aData.GetSize() - 0x800);
}

void CPSXEXE::GetRegionData(CByteArray &data)
{
  int s = m_aData.GetSize() - 0x4C;
  if(s <= 0) return;
  if(s > 0x7B4) s = 0x7B4;
  data.SetSize(s);
  memcpy(data.GetData(), m_aData.GetData() + 0x4C, s);
}

void CPSXEXE::SetRegionData(CByteArray &data)
{
  if(m_aData.GetSize() < 0x800) m_aData.SetSize(0x800);
  int s = data.GetSize();
  if(s > 0x7B4) s = 0x7B4;
  if(s <= 0) return;
  memcpy(m_aData.GetData() + 0x4C, data.GetData(), s);
}
