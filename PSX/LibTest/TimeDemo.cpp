//////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LibTest.h"

#include "TimeDemo.h"

//////////////////////////////////////////////////////////////////////////////

void TimeDemo(LPCTSTR filename, BOOL seekonly) {

  CPSF psf;

  BOOL b = psf.ReadFromFile(filename, CPSF::PSF_FILE_ALL, TRUE);
  if(!b) { printf("%s\n", psf.GetLastError()); return; }

  CPSXEXE exe;
  b = exe.ReadFromPSF(psf, TRUE);
  if(!b) { printf("%s\n", exe.GetLastError()); return; }

  void *emu_state = malloc(emu_get_state_size(1));
  emu_clear_state(emu_state, 1);
  emu_upload_program(emu_state, exe.GetEXEBuffer(), exe.GetEXESize());
//  iop_set_compat(emu_get_iop_state(emu_state), IOP_COMPAT_FRIENDLY);

#define SAMPLES (4410)

  printf("starting timedemo of %s\n", filename);

  DWORD time_start = ::GetTickCount();
  signed short samplebuffer[2*SAMPLES];
  int i;
  for(i = 0; i < 10*30; i++) {
    unsigned samples_left = 4410;
    signed short *buf = seekonly ? NULL : samplebuffer;
    while(samples_left) {
      unsigned runsamples = samples_left;
      int r = emu_execute(emu_state, 0x7FFFFFFF, buf, &runsamples, 0);
      samples_left -= runsamples;
      if(r < 0) { free(emu_state); return; }
    }
  }
  DWORD time_end = ::GetTickCount();

  printf("ok\n");

  DWORD time_elapse = time_end - time_start;
  printf("%d ticks\n", time_elapse);

}

//////////////////////////////////////////////////////////////////////////////
