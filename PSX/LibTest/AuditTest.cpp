//////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LibTest.h"

#include "AuditTest.h"

//////////////////////////////////////////////////////////////////////////////

void AuditTest(LPCTSTR filename) {

  CPSF psf;

  BOOL b = psf.ReadFromFile(filename, CPSF::PSF_FILE_ALL, TRUE);
  if(!b) { printf("%s\n", psf.GetLastError()); return; }

  CPSXEXE exe;
  b = exe.ReadFromPSF(psf, TRUE);
  if(!b) { printf("%s\n", exe.GetLastError()); return; }


  CByteArray state_noaudit;
  CByteArray state_audit;

  CByteArray map_audit;

  map_audit.SetSize(0x200000);
  memset(map_audit.GetData(), 0, 0x200000);

  state_noaudit.SetSize(emu_get_state_size(1));
  state_audit  .SetSize(emu_get_state_size(1));


  LPVOID sp_noaudit = state_noaudit.GetData();
  LPVOID sp_audit   = state_audit  .GetData();
  emu_clear_state(sp_noaudit, 1);
  emu_clear_state(sp_audit  , 1);
  LPVOID r3k_noaudit = iop_get_r3000_state(emu_get_iop_state(sp_noaudit));
  LPVOID r3k_audit   = iop_get_r3000_state(emu_get_iop_state(sp_audit  ));

  emu_upload_program(sp_noaudit, exe.GetEXEBuffer(), exe.GetEXESize());
  emu_upload_program(sp_audit  , exe.GetEXEBuffer(), exe.GetEXESize());

  iop_set_compat(emu_get_iop_state(sp_noaudit), IOP_COMPAT_HARSH);
  iop_set_compat(emu_get_iop_state(sp_audit  ), IOP_COMPAT_HARSH);

  //
  // only set auditing up on one state, not the other
  //
  iop_register_map_for_auditing(
    emu_get_iop_state(sp_audit),
    map_audit.GetData()
  );

  //
  // now we execute both and see if they synchronize okay
  //
  printf("executing...\n");fflush(stdout);
  int c = 0;
  int sec = 0;
  for(;;) {
    uint32 samples;
    int r_noaudit, r_audit;
    samples = 1;
    r_noaudit = emu_execute(sp_noaudit, 1, NULL, &samples, 0);
    samples = 1;
    r_audit   = emu_execute(sp_audit  , 1, NULL, &samples, 0);
    if(r_noaudit < 0) { printf("noaudit: emulation error\n"); break; }
    if(r_audit   < 0) { printf("audit: emulation error\n"); break; }

    // compare
    int i;
    for(i = 1; i < 32; i++) {
      DWORD reg_noaudit = r3000_getreg(r3k_noaudit, R3000_REG_GEN+i);
      DWORD reg_audit   = r3000_getreg(r3k_audit  , R3000_REG_GEN+i);
      if(reg_noaudit != reg_audit) {
        printf("r%d mismatch (noaudit=%08X, audit=%08X)\n", i, reg_noaudit, reg_audit);
        goto thehalt;
      }
    }

    c++;
    if(c>=3386880){
      c=0;sec++;
      printf("%d.%d seconds\n", sec/10,sec%10);fflush(stdout);
    }
  }

thehalt:
  printf("halted at noauditPC=%08X, auditPC=%08X\n",
    r3000_getreg(r3k_noaudit, R3000_REG_PC),
    r3000_getreg(r3k_audit  , R3000_REG_PC)
  );
  fflush(stdout);
}

//////////////////////////////////////////////////////////////////////////////
