// LibTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "LibTest.h"

#include "AuditTest.h"
#include "TimeDemo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		nRetCode = 1;
	}

  printf("initializing emu library\n");
  emu_init();

  //printf("running audit test\n");
  //AuditTest("c:\\corlett\\Projects\\psf\\testfiles\\dragongod_benchmark.psf");

  printf("running timedemo test\n");
//  TimeDemo("c:\\corlett\\Projects\\psf\\testfiles\\dragongod_benchmark.psf", FALSE);
  TimeDemo("c:\\corlett\\Projects\\psf\\testfiles\\earthpainting_benchmark.psf", FALSE);

  printf("done\n");
  char s[100];
  fgets(s, sizeof(s)-1, stdin);

	return nRetCode;
}
