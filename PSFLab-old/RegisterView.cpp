// RegisterView.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "RegisterView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegisterView

IMPLEMENT_DYNCREATE(CRegisterView, CFancyTextView)

CRegisterView::CRegisterView()
{
}

CRegisterView::~CRegisterView()
{
}


BEGIN_MESSAGE_MAP(CRegisterView, CFancyTextView)
	//{{AFX_MSG_MAP(CRegisterView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegisterView drawing

/////////////////////////////////////////////////////////////////////////////
// CRegisterView diagnostics

#ifdef _DEBUG
void CRegisterView::AssertValid() const
{
	CFancyTextView::AssertValid();
}

void CRegisterView::Dump(CDumpContext& dc) const
{
	CFancyTextView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRegisterView message handlers

#define MYLINES (12)

int CRegisterView::GetLineCount()
{
  return MYLINES;
}

CString CRegisterView::GetLineText(CDWordArray &craFG, CDWordArray &craBG, int nLineIndex)
{
  static const int regmatrix[MYLINES+1][4] = {
    { PSFWORKAREA_REG_CPU_PC    , PSFWORKAREA_REG_CPU_GEN+ 1, PSFWORKAREA_REG_CPU_GEN+ 2, PSFWORKAREA_REG_CPU_GEN+ 3 },
    { PSFWORKAREA_REG_CPU_GEN+ 4, PSFWORKAREA_REG_CPU_GEN+ 5, PSFWORKAREA_REG_CPU_GEN+ 6, PSFWORKAREA_REG_CPU_GEN+ 7 },
    { PSFWORKAREA_REG_CPU_GEN+ 8, PSFWORKAREA_REG_CPU_GEN+ 9, PSFWORKAREA_REG_CPU_GEN+10, PSFWORKAREA_REG_CPU_GEN+11 },
    { PSFWORKAREA_REG_CPU_GEN+12, PSFWORKAREA_REG_CPU_GEN+13, PSFWORKAREA_REG_CPU_GEN+14, PSFWORKAREA_REG_CPU_GEN+15 },
    { PSFWORKAREA_REG_CPU_GEN+16, PSFWORKAREA_REG_CPU_GEN+17, PSFWORKAREA_REG_CPU_GEN+18, PSFWORKAREA_REG_CPU_GEN+19 },
    { PSFWORKAREA_REG_CPU_GEN+20, PSFWORKAREA_REG_CPU_GEN+21, PSFWORKAREA_REG_CPU_GEN+22, PSFWORKAREA_REG_CPU_GEN+23 },
    { PSFWORKAREA_REG_CPU_GEN+24, PSFWORKAREA_REG_CPU_GEN+25, PSFWORKAREA_REG_CPU_GEN+26, PSFWORKAREA_REG_CPU_GEN+27 },
    { PSFWORKAREA_REG_CPU_GEN+28, PSFWORKAREA_REG_CPU_GEN+29, PSFWORKAREA_REG_CPU_GEN+30, PSFWORKAREA_REG_CPU_GEN+31 },
    { PSFWORKAREA_REG_CPU_HI    , PSFWORKAREA_REG_CPU_LO    , -1                        , -1                         },
    { PSFWORKAREA_REG_CPU_C0 +12, PSFWORKAREA_REG_CPU_C0 +13, PSFWORKAREA_REG_CPU_C0 +14, -1                         },
    { PSFWORKAREA_REG_CPU_CI    , -1, -1, -1 },
    { -1, -1, -1, -1 },
    { -1, -1, -1, -1 }
  };
  static const LPCTSTR regname[MYLINES+1][4] = {
    { _T("pc"), _T("at"), _T("v0"), _T("v1") },
    { _T("a0"), _T("a1"), _T("a2"), _T("a3") },
    { _T("t0"), _T("t1"), _T("t2"), _T("t3") },
    { _T("t4"), _T("t5"), _T("t6"), _T("t7") },
    { _T("s0"), _T("s1"), _T("s2"), _T("s3") },
    { _T("s4"), _T("s5"), _T("s6"), _T("s7") },
    { _T("t8"), _T("t9"), _T("k0"), _T("k1") },
    { _T("gp"), _T("sp"), _T("fp"), _T("ra") },
    { _T("hi"), _T("lo"), _T(""), _T("") },
    { _T("status"), _T("cause"), _T("epc"), _T("") },
    { _T("cache_isolate"), _T(""), _T(""), _T("") },
    { _T(""), _T(""), _T(""), _T("") },
    { _T(""), _T(""), _T(""), _T("") }
  };
  CPSFWorkArea *wa = GetWorkArea();
  ASSERT(wa);
  TCHAR s[100];
  LPTSTR p = s;
  int i;
  if(nLineIndex < 0) nLineIndex = MYLINES;
  if(nLineIndex > MYLINES) nLineIndex = MYLINES;

  if(nLineIndex == (MYLINES-1)) {
    int nVersion = wa->GetVersion();
    *p = 0;
    switch(nVersion) {
    case 1:
      lstrcpy(p, _T("emu mode: PS1"));
      break;
    case 2:
      lstrcpy(p, _T("emu mode: PS2 IOP"));
      break;
    }
    goto colorization;
  }

  for(i = 0; i < 4; i++) {
    int x = regmatrix[nLineIndex][i];
    if(x < 0) break;
    if(i) *p++ = _T(' ');
    wsprintf(p, _T("%s%c%08X"),
      regname[nLineIndex][i],
      wa->RegisterChangedRecently(regmatrix[nLineIndex][i]) ? _T('^') : _T('='),
      wa->GetRegister(regmatrix[nLineIndex][i])
    );
    while(*p) p++;
  }
  *p = 0;

colorization:

  Colorize(s, craFG, craBG);
  return ((LPCTSTR)s);
}

CPSFWorkArea* CRegisterView::GetWorkArea()
{
  CDocument *pDoc = GetDocument();
  ASSERT(pDoc->IsKindOf(RUNTIME_CLASS(CPSFWorkArea)));
  return (CPSFWorkArea*)pDoc;
}

int CRegisterView::GetMaxLineCharCount()
{
  return 48;
}

COLORREF CRegisterView::GetBlankColor()
{
  return theApp.GetColor(PSFLAB_COLOR_REG_BG);
}

COLORREF CRegisterView::GetDefaultFGColor()
{
  return theApp.GetColor(PSFLAB_COLOR_REG_FG);
}

void CRegisterView::Colorize(LPTSTR s, CDWordArray &craFG, CDWordArray &craBG)
{
  int nHiRemain = 0;
  int i, l;
  l = lstrlen(s);
  craFG.SetSize(l+1);
  craBG.SetSize(l+1);
  COLORREF bgOff    = GetBlankColor();
  COLORREF bgOn     = theApp.GetColor(PSFLAB_COLOR_REG_BG_MODIFIED);
  COLORREF fgOn     = theApp.GetColor(PSFLAB_COLOR_REG_FG_MODIFIED);
  COLORREF fgNormal = GetDefaultFGColor();
  COLORREF fgLow    = theApp.GetColor(PSFLAB_COLOR_REG_FG_PUNCTUATION);
  for(i = 0; i < l; i++) {
    if(nHiRemain) {
      craBG[i] = bgOn;
      craFG[i] = fgOn;
      nHiRemain--;
    } else {
      craBG[i] = bgOff;
      craFG[i] = fgNormal;
    }
    if(s[i] == _T('^')) {
      s[i] = _T('=');
      craFG[i] = fgLow;
      nHiRemain = 8;
    } else if(s[i] == _T('=')) {
      craFG[i] = fgLow;
      nHiRemain = 0;
    } else {
//      craFG[i] = fgNormal;
    }
  }
  craFG[l] = fgNormal;
  craBG[l] = bgOff;
}
