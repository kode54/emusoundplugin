// MIPSQuickAssembler.h: interface for the CMIPSQuickAssembler class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MIPSQUICKASSEMBLER_H__908FEE0E_FFBB_48F1_9AB3_AA113AE1D640__INCLUDED_)
#define AFX_MIPSQUICKASSEMBLER_H__908FEE0E_FFBB_48F1_9AB3_AA113AE1D640__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMIPSQuickAssembler  
{
public:
	CString Assemble(DWORD dwPC, LPCTSTR lpszText, DWORD &dwIns);
	CMIPSQuickAssembler();
	virtual ~CMIPSQuickAssembler();

private:
	void ReplaceInsField(DWORD &dwIns, int nBitPosition, int nBits, DWORD dwValue);
	CString AssemblePattern(LPCSTR lpszPattern, DWORD dwPC, LPCTSTR lpszText, DWORD &dwIns);
	CString AssemblePatterns(LPCSTR lpszPatterns, DWORD dwPC, LPCTSTR lpszText, DWORD &dwIns, DWORD dwInsTemplate);
	BOOL AlphaCompare(LPCTSTR lpszText, LPCTSTR lpszPattern);
	LPCTSTR Scan(LPCTSTR lpszText, DWORD &dwToken, DWORD &dwSubToken);
};

#endif // !defined(AFX_MIPSQUICKASSEMBLER_H__908FEE0E_FFBB_48F1_9AB3_AA113AE1D640__INCLUDED_)
