// FindHistory.h: interface for the CFindHistory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FINDHISTORY_H__7D86370C_E915_433F_A67E_C77F55A33D71__INCLUDED_)
#define AFX_FINDHISTORY_H__7D86370C_E915_433F_A67E_C77F55A33D71__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CFindHistory  
{
public:
	CFindHistory();
	virtual ~CFindHistory();
  void SetMatchSpaces(BOOL bMatchSpaces);
  void SetMatchCase(BOOL bMatchCase);
  void SetBackwards(BOOL bBackwards);
  BOOL IsMatchSpaces() const;
  BOOL IsMatchCase() const;
  BOOL IsBackwards() const;
  void Clear();
  void AddString(LPCTSTR lpszText);
  const CStringArray* GetStringArray() const;
  CString GetMostRecentString() const;
private:
  CStringArray m_sa;
  BOOL m_bMatchSpaces;
  BOOL m_bMatchCase;
  BOOL m_bBackwards;
};

#endif // !defined(AFX_FINDHISTORY_H__7D86370C_E915_433F_A67E_C77F55A33D71__INCLUDED_)
