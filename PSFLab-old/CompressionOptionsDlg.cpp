// CompressionOptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "CompressionOptionsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCompressionOptionsDlg dialog


CCompressionOptionsDlg::CCompressionOptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCompressionOptionsDlg::IDD, pParent)
{
  m_uMethod = 0;

	//{{AFX_DATA_INIT(CCompressionOptionsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCompressionOptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCompressionOptionsDlg)
	DDX_Control(pDX, IDC_METHODLIST, m_cMethodList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCompressionOptionsDlg, CDialog)
	//{{AFX_MSG_MAP(CCompressionOptionsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCompressionOptionsDlg message handlers

BOOL CCompressionOptionsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  UINT uMethodCount   = CPSFCompressor::GetMethodCount();
  UINT uDefaultMethod = CPSFCompressor::GetDefaultMethod();
  UINT uSafeMethod    = CPSFCompressor::GetSafeMethod();
  for(UINT u = 0; u < uMethodCount; u++) {
    CString s = CPSFCompressor::GetMethodVersion(u);
    if(u == uDefaultMethod) s += _T(" (default)");
    if(u == uSafeMethod   ) s += _T(" (safe)");
    m_cMethodList.AddString(s);
  }

  m_cMethodList.SetCurSel(m_uMethod);
  //m_cSmallerOnly.SetCheck(m_bSmallerOnly ? BST_CHECKED : BST_UNCHECKED);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCompressionOptionsDlg::OnOK() 
{
  int s = m_cMethodList.GetCurSel();
  if(s == LB_ERR) return;
  if(s < 0) return;
  if(s >= CPSFCompressor::GetMethodCount()) return;
  m_uMethod = s;

	CDialog::OnOK();
}
