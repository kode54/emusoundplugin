#if !defined(AFX_CALLSTACKVIEW_H__C3D5107E_A713_4E21_BF6B_BAA6FE6DAE98__INCLUDED_)
#define AFX_CALLSTACKVIEW_H__C3D5107E_A713_4E21_BF6B_BAA6FE6DAE98__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CallStackView.h : header file
//

#include "FancyTextView.h"
#include "PSFWorkArea.h"

/////////////////////////////////////////////////////////////////////////////
// CCallStackView view

class CCallStackView : public CFancyTextView
{
protected:
	CCallStackView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCallStackView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCallStackView)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCallStackView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	virtual int GetMaxLineCharCount();
	virtual CString GetLineText(CDWordArray& craFG, CDWordArray& craBG, int nLineIndex);
	virtual COLORREF GetDefaultFGColor();
	virtual COLORREF GetBlankColor();
	virtual int GetLineCount();
	//{{AFX_MSG(CCallStackView)
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnUpdateEditJump(CCmdUI* pCmdUI);
	afx_msg void OnEditJump();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	LPCTSTR GetExceptionName(DWORD dwCause) const;
	void Colorize(LPTSTR s, CDWordArray &craFG, CDWordArray &craBG);
	CPSFWorkArea* GetWorkArea();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALLSTACKVIEW_H__C3D5107E_A713_4E21_BF6B_BAA6FE6DAE98__INCLUDED_)
