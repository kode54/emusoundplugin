// OptimizeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "OptimizeDlg.h"
#include "EmuSettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptimizeDlg dialog


COptimizeDlg::COptimizeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptimizeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptimizeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

  m_bParanoid = TRUE;
  m_nParanoidBytes = 1024;
}

void COptimizeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptimizeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptimizeDlg, CDialog)
	//{{AFX_MSG_MAP(COptimizeDlg)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_APPLYCHANGES, OnApplychanges)
	ON_BN_CLICKED(IDC_EMUSETTINGS, OnEmusettings)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptimizeDlg message handlers

BOOL COptimizeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  CWnd *pWnd;

  theApp.GetAuditOptions(
    m_bParanoid,
    m_nParanoidBytes
  );

  SetDlgItemText(IDOK, _T("Begin Audit"));
  pWnd = GetDlgItem(IDC_APPLYCHANGES);
  if(pWnd) pWnd->EnableWindow(FALSE);

  m_bAuditing = FALSE;

  ASSERT(m_pWorkArea);
  m_audit.Open();

  SPSFEmuSettings es;
  theApp.GetEmuSettings(PSF_EMUSETTINGS_AUDIT, es);
  m_audit.SetCompat(es.bFriendly ? IOP_COMPAT_FRIENDLY : IOP_COMPAT_HARSH);

  m_pWorkArea->AuditUpload(m_audit);

  CheckDlgButton(IDC_PARANOID, m_bParanoid?BST_CHECKED:BST_UNCHECKED);
  SetDlgItemText(IDC_PARANOID_BYTES, Util::NumToString(m_nParanoidBytes, FALSE));

  UpdateStatistics();

  SetTimer(10, 500, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COptimizeDlg::OnDestroy() 
{
  KillTimer(10);
  m_audit.Close();

	CDialog::OnDestroy();
}

void COptimizeDlg::OnTimer(UINT nIDEvent) 
{
	CDialog::OnTimer(nIDEvent);
  if(nIDEvent != 10) return;

  UpdateStatistics();
}

void COptimizeDlg::UpdateStatistics()
{
  double dSeconds = m_audit.GetSeconds();
  int nBytes = m_audit.GetBytes();
  CString s = 
    _T("Duration: ") + Util::NumToString(dSeconds, TRUE) + _T("\n") +
    _T("Bytes used: ") + Util::NumToString(nBytes, FALSE);
  SetDlgItemText(IDC_STATISTICS, s);
}

void COptimizeDlg::OnOK() 
{
  if(m_bAuditing) {
    m_audit.Stop();
    m_bAuditing = FALSE;
    SetDlgItemText(IDOK, _T("Continue Audit"));
  } else {
    GetDlgItem(IDC_EMUSETTINGS)->EnableWindow(FALSE);
    m_audit.Start();
    m_bAuditing = TRUE;
    SetDlgItemText(IDOK, _T("Stop Audit"));
  }
  GetDlgItem(IDC_APPLYCHANGES)->EnableWindow(TRUE);
//	CDialog::OnOK();
}

void COptimizeDlg::OnApplychanges() 
{
  SyncVariablesToControls();
  m_bAuditing = FALSE;
  m_audit.Stop();
  if(m_bParanoid) {
    int nBytes = m_nParanoidBytes;
    if(nBytes < 0) nBytes = 0;
    m_audit.GrowBy(nBytes);
  }
  m_pWorkArea->AuditCommit(m_audit);
  theApp.SetAuditOptions(
    m_bParanoid,
    m_nParanoidBytes
  );
  CDialog::OnOK();
}

void COptimizeDlg::SyncVariablesToControls()
{
  m_bParanoid = IsDlgButtonChecked(IDC_PARANOID);
  CString s;
  double d;
  GetDlgItemText(IDC_PARANOID_BYTES, s);
  if(Util::StringToNum(s, d, FALSE)) {
    int nBytes = (int)d;
    if(nBytes < 0) nBytes = 0;
    if(nBytes > 0x200000) nBytes = 0x200000;
    m_nParanoidBytes = nBytes;
  }
}

void COptimizeDlg::OnEmusettings() 
{
  CEmuSettingsDlg dlg;
  dlg.m_nWhichSettingToStart = PSF_EMUSETTINGS_AUDIT;
  dlg.DoModal();
}
