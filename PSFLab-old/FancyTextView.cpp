// FancyTextView.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "FancyTextView.h"

#include "FindDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define FANCY_TIMER_DRAGSEL 1001

#define FONT_WIDTH_REFERENCE_STRING ((LPCTSTR)(_T("0")))

/////////////////////////////////////////////////////////////////////////////
// CFancyTextView

IMPLEMENT_DYNCREATE(CFancyTextView, CView)

CFancyTextView::CFancyTextView()
{
  m_bFocused = FALSE;
  m_pCacheBitmap = NULL;
  m_bCursorHidden = FALSE;
  m_bShowInactiveSelection = FALSE;
  m_pFont = NULL;
	ResetView();
}

CFancyTextView::~CFancyTextView()
{
  if(m_pCacheBitmap) {
    m_pCacheBitmap->DeleteObject();
    delete m_pCacheBitmap;
  }
  if(m_pFont) {
    m_pFont->DeleteObject();
    delete m_pFont;
  }
}


BEGIN_MESSAGE_MAP(CFancyTextView, CView)
	//{{AFX_MSG_MAP(CFancyTextView)
	ON_WM_CREATE()
	ON_WM_SETCURSOR()
	ON_WM_SIZE()
	ON_WM_KILLFOCUS()
	ON_WM_SETFOCUS()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_TIMER()
	ON_WM_KEYDOWN()
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_FIND, OnUpdateEditFind)
	ON_COMMAND(ID_EDIT_FIND, OnEditFind)
	ON_WM_MOUSEWHEEL()
	ON_WM_RBUTTONDOWN()
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFancyTextView drawing

void CFancyTextView::OnDraw(CDC* pDC)
{
  ASSERT(GetDocument());

	CRect rcClient;
	GetClientRect(rcClient);

	int nLineCount = GetLineCount();
	int nLineHeight = GetLineHeight();

	CDC cacheDC;
	VERIFY(cacheDC.CreateCompatibleDC(pDC));
	if(!m_pCacheBitmap) {
		m_pCacheBitmap = new CBitmap;
		VERIFY(m_pCacheBitmap->CreateCompatibleBitmap(pDC, rcClient.Width(), nLineHeight));
	}
	CBitmap *pOldBitmap = cacheDC.SelectObject(m_pCacheBitmap);

	CRect rcLine;
	rcLine = rcClient;
	rcLine.bottom = rcLine.top + nLineHeight;

	CRect rcCacheLine(0, 0, rcLine.Width(), nLineHeight);

	int nCurrentLine = m_ptDisplay.y;
	while (rcLine.top < rcClient.bottom) {
		if (nCurrentLine < nLineCount) {
			DrawSingleLine(&cacheDC, rcCacheLine, nCurrentLine);
		} else {
			DrawSingleLine(&cacheDC, rcCacheLine, -1);
		}

		VERIFY(pDC->BitBlt(rcLine.left, rcLine.top, rcLine.Width(), rcLine.Height(), &cacheDC, 0, 0, SRCCOPY));

		nCurrentLine ++;
		rcLine.OffsetRect(0, nLineHeight);
	}

	cacheDC.SelectObject(pOldBitmap);
	cacheDC.DeleteDC();
}

/////////////////////////////////////////////////////////////////////////////
// CFancyTextView diagnostics

#ifdef _DEBUG
void CFancyTextView::AssertValid() const
{
	CView::AssertValid();
}

void CFancyTextView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFancyTextView message handlers

BOOL CFancyTextView::PreCreateWindow(CREATESTRUCT& cs) 
{
	CWnd *pParentWnd = CWnd::FromHandlePermanent(cs.hwndParent);
	if (pParentWnd == NULL || ! pParentWnd->IsKindOf(RUNTIME_CLASS(CSplitterWnd)))
	{
		//	View must always create its own scrollbars,
		//	if only it's not used within splitter
		cs.style |= (WS_HSCROLL | WS_VSCROLL);
	}
	cs.lpszClass = AfxRegisterWndClass(CS_DBLCLKS);
	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////

void CFancyTextView::DrawSingleLine(CDC *pdc, const CRect &rc, int nLineIndex)
{
if(nLineIndex<-1){nLineIndex=-1;}
if(nLineIndex>=GetLineCount()){nLineIndex=-1;}

	ASSERT(nLineIndex >= -1 && nLineIndex < GetLineCount());

  COLORREF crBlank = GetBlankColor();

  //	Draw line beyond the text
  if(nLineIndex == -1) {
    pdc->FillSolidRect(rc, crBlank);
    return;
  }

  CString strText;
  CDWordArray craFG;
  CDWordArray craBG;

  strText = GetLineText(craFG, craBG, nLineIndex);

  if(!craFG.GetSize()) craFG.Add(GetDefaultFGColor());
  if(!craBG.GetSize()) craBG.Add(crBlank);

  int nCharCount = strText.GetLength();

//TRACE("drawing line %d\n",nLineIndex);

	//	Draw the line text
	CPoint ptOrigin(rc.left, rc.top);

  int nCharWidth = GetCharWidth();
  CFont *pFont = GetFont();

  ptOrigin.x -= nCharWidth * m_ptDisplay.x;

  COLORREF crFG, crBG;

  TCHAR strTemp[2];
  strTemp[1] = 0;
  int i;
  for (i = 0; i < nCharCount; i++) {
    strTemp[0] = strText[i];
    if(i < craFG.GetSize()) { crFG = craFG[i]; }
    if(i < craBG.GetSize()) { crBG = craBG[i]; }
    if((m_bFocused || m_bShowInactiveSelection) && IsInsideSelection(CPoint(i, nLineIndex))) {
      pdc->SetBkColor(SelColor(crBG));
      pdc->SetTextColor(SelColor(crFG));
    } else {
      pdc->SetBkColor(crBG);
      pdc->SetTextColor(crFG);
    }
    pdc->SelectObject(pFont);

    CRect chRect = rc;
    if(chRect.left < ptOrigin.x) chRect.left = ptOrigin.x;
    if(chRect.right > (ptOrigin.x + nCharWidth)) chRect.right = (ptOrigin.x + nCharWidth);

    if(chRect.right > chRect.left) {
	    CSize szCharExt = pdc->GetTextExtent((LPCTSTR)strTemp, 1);
      int nExtent = szCharExt.cx;
      pdc->ExtTextOut(ptOrigin.x+((nCharWidth-nExtent)/2), ptOrigin.y, ETO_CLIPPED|ETO_OPAQUE, &chRect, strTemp, 1, NULL);
    }

    ptOrigin.x += nCharWidth;
	}

  crBG = craBG[craBG.GetSize() - 1];

	// Draw whitespace
	CRect frect = rc;
	if (ptOrigin.x > frect.left) frect.left = ptOrigin.x;
	if (frect.right > frect.left) {
		if ((m_bFocused || m_bShowInactiveSelection) && IsInsideSelection(CPoint(nCharCount, nLineIndex))) {
			pdc->FillSolidRect(frect, SelColor(crBG));
		} else {
			pdc->FillSolidRect(frect, crBG);
    }
	}
}

int CFancyTextView::GetLineCount() {
  return 10;
}

CString CFancyTextView::GetLineText(CDWordArray& craFG, CDWordArray& craBG, int nLineIndex) {
  craFG.SetSize(0);
  craBG.SetSize(0);
  CString f;
  f.Format(_T("unsubclassed line %d"), nLineIndex);
  return f;
//  return _T("test line");
}

COLORREF CFancyTextView::GetBlankColor() {
  return RGB(0, 0, 0);
}

COLORREF CFancyTextView::SelColor(COLORREF cr)
{
// boring way
//  return RGB(
//    (GetRValue(cr)^0xFF),
//    (GetGValue(cr)^0xFF),
//    (GetBValue(cr)^0xFF)
//  );
// interesting way!!!
  double R,G,B,Y,U,V;
  R = GetRValue(cr);
  G = GetGValue(cr);
  B = GetBValue(cr);
  Y = 0.299*R + 0.587*G + 0.114*B;
  U = (B-Y)*0.565;
  V = (R-Y)*0.713;

  Y=255.0-Y;
  U *= 0.5;
  V *= 0.5;

  R = Y + 1.403*V;
  G = Y - 0.344*U - 0.714*V;
  B = Y + 1.770*U;
  int r=(int)R,g=(int)G,b=(int)B;
  if(r>255)r=255;if(g>255)g=255;if(b>255)b=255;
  if(r<0)r=0;if(g<0)g=0;if(b<0)b=0;
  return RGB(r,g,b);
}

void CFancyTextView::ResetView()
{
	m_ptDisplay.x = 0;
	m_ptDisplay.y = 0;
	m_nLineHeight = -1;
	m_nCharWidth = -1;
  m_nScreenLines = -1;
  m_nScreenChars = -1;
	m_ptAnchor.x = 0;
	m_ptAnchor.y = 0;
	m_ptCursor.x = 0;
	m_ptCursor.y = 0;
  m_bDragSelection = FALSE;
  m_bVertScrollBarLocked = FALSE;
  m_bHorzScrollBarLocked = FALSE;
	if (::IsWindow(m_hWnd)) UpdateCaret();
}

int CFancyTextView::GetLineHeight()
{
	if (m_nLineHeight == -1)
		CalcLineCharDim();
	return m_nLineHeight;
}

int CFancyTextView::GetCharWidth()
{
	if (m_nCharWidth == -1)
		CalcLineCharDim();
	return m_nCharWidth;
}

BOOL CFancyTextView::IsInsideSelection(CPoint ptTextPos) {
  CPoint ptDrawSelStart;
  CPoint ptDrawSelEnd;
  GetSelection(ptDrawSelStart, ptDrawSelEnd);
  EnforcePointOrder(ptDrawSelStart, ptDrawSelEnd);
	if(ptTextPos.y < ptDrawSelStart.y) return FALSE;
	if(ptTextPos.y > ptDrawSelEnd.y) return FALSE;
	if(ptTextPos.y < ptDrawSelEnd.y && ptTextPos.y > ptDrawSelStart.y) return TRUE;
	if(ptDrawSelStart.y < ptDrawSelEnd.y) {
		if(ptTextPos.y == ptDrawSelEnd.y) {
			return (ptTextPos.x < ptDrawSelEnd.x);
    }
		ASSERT(ptTextPos.y == ptDrawSelStart.y);
		return (ptTextPos.x >= ptDrawSelStart.x);
	}
	ASSERT(ptDrawSelStart.y == ptDrawSelEnd.y);
	return ((ptTextPos.x >= ptDrawSelStart.x) && (ptTextPos.x < ptDrawSelEnd.x));
}

void CFancyTextView::CalcLineCharDim()
{
	CDC *pdc = GetDC();
	CFont *pOldFont = pdc->SelectObject(GetFont());
	CSize szCharExt = pdc->GetTextExtent(FONT_WIDTH_REFERENCE_STRING, 1);
	m_nLineHeight = szCharExt.cy;
	if(m_nLineHeight < 1) m_nLineHeight = 1;
	m_nCharWidth = szCharExt.cx;
	if(m_nCharWidth < 1) m_nCharWidth = 1;
	pdc->SelectObject(pOldFont);
	ReleaseDC(pdc);
}

void CFancyTextView::UpdateCaret() {
  if(
    (m_bFocused) &&
    (!m_bCursorHidden) &&
    (m_ptCursor.x >= m_ptDisplay.x)
  ) {
    CreateSolidCaret(2, GetLineHeight());
    SetCaretPos(TextToClient(m_ptCursor));
    ShowCaret();
  } else {
    HideCaret();
  }
}

CPoint CFancyTextView::TextToClient(const CPoint &pt)
{
	CPoint ptR;
	ptR.y = (pt.y - m_ptDisplay.y) * GetLineHeight();
	ptR.x = (pt.x - m_ptDisplay.x) * GetCharWidth();
	return ptR;
}

int CFancyTextView::OnCreate(LPCREATESTRUCT lpCreateStruct) {
  if(CView::OnCreate(lpCreateStruct) == -1) return -1;

  memcpy(&m_lfBaseFont, theApp.GetLogFont(), sizeof(m_lfBaseFont));
/*
	memset(&m_lfBaseFont, 0, sizeof(m_lfBaseFont));
	lstrcpy(m_lfBaseFont.lfFaceName, _T("FixedSys"));
	m_lfBaseFont.lfHeight = 0;
	m_lfBaseFont.lfWeight = FW_NORMAL;
	m_lfBaseFont.lfItalic = FALSE;
	m_lfBaseFont.lfCharSet = DEFAULT_CHARSET;
	m_lfBaseFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	m_lfBaseFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	m_lfBaseFont.lfQuality = DEFAULT_QUALITY;
	m_lfBaseFont.lfPitchAndFamily = DEFAULT_PITCH;
*/

//	ASSERT(m_hAccel == NULL);
//	m_hAccel = ::LoadAccelerators(GetResourceHandle(), MAKEINTRESOURCE(IDR_DEFAULT_ACCEL));
//	ASSERT(m_hAccel != NULL);
	return 0;
}

BOOL CFancyTextView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	if (nHitTest == HTCLIENT) {
//		CPoint pt;
//		::GetCursorPos(&pt);
//		ScreenToClient(&pt);
		::SetCursor(::LoadCursor(NULL, MAKEINTRESOURCE(IDC_IBEAM)));
		return TRUE;
	}
	return CView::OnSetCursor(pWnd, nHitTest, message);
}

void CFancyTextView::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);
	if(m_pCacheBitmap) {
		m_pCacheBitmap->DeleteObject();
		delete m_pCacheBitmap;
		m_pCacheBitmap = NULL;
	}
	m_nScreenLines = -1;
	m_nScreenChars = -1;
	RecalcVertScrollBar();
	RecalcHorzScrollBar();
}

void CFancyTextView::RecalcVertScrollBar(BOOL bPositionOnly)
{
	SCROLLINFO si;
	si.cbSize = sizeof(si);
	if (bPositionOnly) {
		si.fMask = SIF_POS;
		si.nPos = m_ptDisplay.y;
	} else {
		if (GetScreenLines() >= GetLineCount() && m_ptDisplay.y > 0)
		{
			m_ptDisplay.y = 0;
			Invalidate();
			UpdateCaret();
		}
		si.fMask = SIF_DISABLENOSCROLL | SIF_PAGE | SIF_POS | SIF_RANGE;
		si.nMin = 0;
		si.nMax = GetLineCount() - 1;
		si.nPage = GetScreenLines();
		si.nPos = m_ptDisplay.y;
	}
	VERIFY(SetScrollInfo(SB_VERT, &si));
}

void CFancyTextView::RecalcHorzScrollBar(BOOL bPositionOnly)
{
	//	Again, we cannot use nPos because it's 16-bit
	SCROLLINFO si;
	si.cbSize = sizeof(si);
	if (bPositionOnly) {
		si.fMask = SIF_POS;
		si.nPos = m_ptDisplay.x;
	} else {
		if (GetScreenChars() >= GetMaxLineCharCount() && m_ptDisplay.x > 0)
		{
			m_ptDisplay.x = 0;
			Invalidate();
			UpdateCaret();
		}
		si.fMask = SIF_DISABLENOSCROLL | SIF_PAGE | SIF_POS | SIF_RANGE;
		si.nMin = 0;
		si.nMax = GetMaxLineCharCount() - 1;
		si.nPage = GetScreenChars();
		si.nPos = m_ptDisplay.x;
	}
	VERIFY(SetScrollInfo(SB_HORZ, &si));
}

CFont *CFancyTextView::GetFont() {
	if(!m_pFont) {
		m_pFont = new CFont;
		if(!m_pFont->CreateFontIndirect(&m_lfBaseFont)) {
			delete m_pFont;
			m_pFont = NULL;
			return CView::GetFont();
		}
	}
	return m_pFont;
}

void CFancyTextView::OnKillFocus(CWnd* pNewWnd) {
	m_bFocused = FALSE;
	UpdateCaret();
  InvalidateSelectedLines();
	if(m_bDragSelection) {
		ReleaseCapture();
		KillTimer(m_nDragSelTimer);
		m_bDragSelection = FALSE;
	}
	CView::OnKillFocus(pNewWnd);
}

void CFancyTextView::InvalidateLines(int nLine1, int nLine2)
{
	if(nLine2 == -1) {
		CRect rcInvalid;
		GetClientRect(&rcInvalid);
		rcInvalid.top = (nLine1 - m_ptDisplay.y) * GetLineHeight();
		InvalidateRect(&rcInvalid, FALSE);
	} else {
		if (nLine2 < nLine1)
		{
			int nTemp = nLine1;
			nLine1 = nLine2;
			nLine2 = nTemp;
		}
		CRect rcInvalid;
		GetClientRect(&rcInvalid);
		rcInvalid.top = (nLine1 - m_ptDisplay.y) * GetLineHeight();
		rcInvalid.bottom = (nLine2 - m_ptDisplay.y + 1) * GetLineHeight();
		InvalidateRect(&rcInvalid, FALSE);
	}
}

void CFancyTextView::OnSetFocus(CWnd* pOldWnd) 
{
//TRACE("CFancyTextView::OnSetFocus(%08X)\n",(DWORD)this);
//  SetActiveWindow();
//  SetForegroundWindow();
	CView::OnSetFocus(pOldWnd);
	m_bFocused = TRUE;
  InvalidateSelectedLines();
	UpdateCaret();
}

BOOL CFancyTextView::OnEraseBkgnd(CDC* pDC) 
{
	return TRUE;
}

CPoint CFancyTextView::ClientToText(const CPoint &point)
{
	CPoint ptR;
	ptR.x = m_ptDisplay.x + (point.x / GetCharWidth());
	ptR.y = m_ptDisplay.y + (point.y / GetLineHeight());
  return BoundsCheck(ptR);
}

void CFancyTextView::SetSelection(const CPoint &ptAnchor, const CPoint &ptCursor, int nScrollPercent)
{
  CPoint ptAnchorChecked, ptCursorChecked;
  ptAnchorChecked = BoundsCheck(ptAnchor);
  ptCursorChecked = BoundsCheck(ptCursor);
  EnsureVisible(ptCursorChecked, nScrollPercent);
  InvalidateSelectedLines();
  m_ptAnchor = ptAnchorChecked;
  m_ptCursor = ptCursorChecked;
  InvalidateSelectedLines();
  UpdateCaret();
}

void CFancyTextView::GetLogFont(LOGFONT &lf)
{
	lf = m_lfBaseFont;
}

void CFancyTextView::SetLogFont(const LOGFONT &lf)
{
	m_lfBaseFont = lf;
	m_nScreenLines = -1;
	m_nScreenChars = -1;
	m_nCharWidth = -1;
	m_nLineHeight = -1;
	if(m_pCacheBitmap) {
		m_pCacheBitmap->DeleteObject();
		delete m_pCacheBitmap;
		m_pCacheBitmap = NULL;
	}
	if(m_pFont) {
    m_pFont->DeleteObject();
    delete m_pFont;
    m_pFont = NULL;
	}
	if(::IsWindow(m_hWnd)) {
		RecalcVertScrollBar();
		RecalcHorzScrollBar();
		UpdateCaret();
		Invalidate();
	}
}

void CFancyTextView::ShowCursor()
{
	m_bCursorHidden = FALSE;
  UpdateCaret();
}

void CFancyTextView::HideCursor()
{
	m_bCursorHidden = TRUE;
  UpdateCaret();
}

void CFancyTextView::OnLButtonDown(UINT nFlags, CPoint point) 
{
//  SetFocus();
//TRACE("CFancyTextView::OnLButtonDown\n");
	CView::OnLButtonDown(nFlags, point);

	BOOL bShift = GetKeyState(VK_SHIFT) & 0x8000;

	CPoint ptStart, ptEnd;

	AdjustTextPoint(point);

	ptStart = m_ptAnchor;
	ptEnd   = ClientToText(point);

  if(!bShift) ptStart = ptEnd;

	SetSelection(ptStart, ptEnd);

	SetCapture();
	m_nDragSelTimer = SetTimer(FANCY_TIMER_DRAGSEL, 100, NULL);
	ASSERT(m_nDragSelTimer != 0);
	m_bDragSelection = TRUE;

}

void CFancyTextView::EnsureVisible(CPoint pt, int nScrollPercent)
{
	//	Scroll vertically
	int nLineCount = GetLineCount();
	int nNewTopLine = m_ptDisplay.y;
	if (pt.y >= nNewTopLine + GetScreenLines())
	{
		nNewTopLine = pt.y - GetScreenLines() + 1;
	}
	if (pt.y < nNewTopLine)
	{
		nNewTopLine = pt.y;
	}

	if (nNewTopLine < 0)
		nNewTopLine = 0;
	if (nNewTopLine >= nLineCount)
		nNewTopLine = nLineCount - 1;

  if(nScrollPercent >= 0) {
    nNewTopLine = pt.y - ((GetScreenLines() * nScrollPercent) / 100);
  }

	if (m_ptDisplay.y != nNewTopLine)
	{
		ScrollToLine(nNewTopLine);
//		UpdateSiblingScrollPos(TRUE);
	}

	//	Scroll horizontally
//	int nActualPos = CalculateActualOffset(pt.y, pt.x);
	int nActualPos = pt.x;
	int nNewOffset = m_ptDisplay.x;
	if (nActualPos > nNewOffset + GetScreenChars())
	{
		nNewOffset = nActualPos - GetScreenChars();
	}
	if (nActualPos < nNewOffset)
	{
		nNewOffset = nActualPos;
	}

	if (nNewOffset >= GetMaxLineCharCount())
		nNewOffset = GetMaxLineCharCount() - 1;
	if (nNewOffset < 0)
		nNewOffset = 0;

	if (m_ptDisplay.x != nNewOffset)
	{
		ScrollToChar(nNewOffset);
		UpdateCaret();
//		UpdateSiblingScrollPos(FALSE);
	}
}

CPoint CFancyTextView::CharToRight(CPoint pt) {
  pt.x++;
  return BoundsCheck(pt);
}

CPoint CFancyTextView::CharToLeft(CPoint pt) {
  pt.x--;
  return BoundsCheck(pt);
}

int CFancyTextView::GetScreenLines()
{
	if (m_nScreenLines == -1)
	{
		CRect rect;
		GetClientRect(&rect);
		m_nScreenLines = rect.Height() / GetLineHeight();
	}
	return m_nScreenLines;
}

int CFancyTextView::GetScreenChars()
{
	if (m_nScreenChars == -1)
	{
		CRect rect;
		GetClientRect(&rect);
		m_nScreenChars = rect.Width() / GetCharWidth();
	}
	return m_nScreenChars;
}

int CFancyTextView::GetMaxLineCharCount()
{
  return 80;
}

void CFancyTextView::ScrollToChar(int nNewOffsetChar, BOOL bNoSmoothScroll, BOOL bTrackScrollBar)
{
	//	For now, ignoring bNoSmoothScroll and m_bSmoothScroll
	if (m_ptDisplay.x != nNewOffsetChar) {
		int nScrollChars = m_ptDisplay.x - nNewOffsetChar;
		m_ptDisplay.x = nNewOffsetChar;
		CRect rcScroll;
		GetClientRect(&rcScroll);
//		rcScroll.left += GetSelectableMarginWidth();
		ScrollWindow(nScrollChars * GetCharWidth(), 0, &rcScroll, &rcScroll);
		UpdateWindow();
		if (bTrackScrollBar)
			RecalcHorzScrollBar(TRUE);
	}
}

void CFancyTextView::ScrollToLine(int nNewTopLine, BOOL bNoSmoothScroll, BOOL bTrackScrollBar)
{
	if (m_ptDisplay.y != nNewTopLine)
	{
		int nScrollLines = m_ptDisplay.y - nNewTopLine;
		m_ptDisplay.y = nNewTopLine;
		ScrollWindow(0, nScrollLines * GetLineHeight());
		UpdateWindow();
		if (bTrackScrollBar)
			RecalcVertScrollBar(TRUE);
	}
}

void CFancyTextView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CView::OnLButtonUp(nFlags, point);

	if (m_bDragSelection) {
		AdjustTextPoint(point);
		CPoint ptNewCursor = ClientToText(point);

		SetSelection(m_ptAnchor, ptNewCursor);

		ReleaseCapture();
		KillTimer(m_nDragSelTimer);
		m_bDragSelection = FALSE;
	}
}

void CFancyTextView::AdjustTextPoint(CPoint &point)
{
	point.x += GetCharWidth() / 2;	//todo
}

void CFancyTextView::OnMouseMove(UINT nFlags, CPoint point) 
{
	CView::OnMouseMove(nFlags, point);

	if (m_bDragSelection) {
		AdjustTextPoint(point);
		CPoint ptNewCursorPos = ClientToText(point);
		CPoint ptStart, ptEnd;
		ptStart = m_ptAnchor;
		ptEnd = ptNewCursorPos;
		SetSelection(ptStart, ptEnd);
	}
}

void CFancyTextView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CView::OnVScroll(nSBCode, nPos, pScrollBar);

	//	Note we cannot use nPos because of its 16-bit nature
	SCROLLINFO si;
	si.cbSize = sizeof(si);
	si.fMask = SIF_ALL;
	VERIFY(GetScrollInfo(SB_VERT, &si));

	int nPageLines = GetScreenLines();
	int nLineCount = GetLineCount();

	int nNewTopLine;
	switch (nSBCode) {
	case SB_TOP:
		nNewTopLine = 0;
		break;
	case SB_BOTTOM:
		nNewTopLine = nLineCount - nPageLines + 1;
		break;
	case SB_LINEUP:
		nNewTopLine = m_ptDisplay.y - 1;
		break;
	case SB_LINEDOWN:
		nNewTopLine = m_ptDisplay.y + 1;
		break;
	case SB_PAGEUP:
		nNewTopLine = m_ptDisplay.y - si.nPage + 1;
		break;
	case SB_PAGEDOWN:
		nNewTopLine = m_ptDisplay.y + si.nPage - 1;
		break;
	case SB_THUMBPOSITION:
	case SB_THUMBTRACK:
		nNewTopLine = si.nTrackPos;
		break;
	default:
		return;
	}

	if (nNewTopLine < 0)
		nNewTopLine = 0;
	if (nNewTopLine > nLineCount)
		nNewTopLine = nLineCount;
	ScrollToLine(nNewTopLine);
}

void CFancyTextView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CView::OnHScroll(nSBCode, nPos, pScrollBar);

	SCROLLINFO si;
	si.cbSize = sizeof(si);
	si.fMask = SIF_ALL;
	VERIFY(GetScrollInfo(SB_HORZ, &si));

	int nPageChars = GetScreenChars();
	int nMaxLineLength = GetMaxLineCharCount();

	int nNewOffset;
	switch (nSBCode)
	{
	case SB_LEFT:
		nNewOffset = 0;
		break;
	case SB_BOTTOM:
		nNewOffset = nMaxLineLength - nPageChars + 1;
		break;
	case SB_LINEUP:
		nNewOffset = m_ptDisplay.x - 1;
		break;
	case SB_LINEDOWN:
		nNewOffset = m_ptDisplay.x + 1;
		break;
	case SB_PAGEUP:
		nNewOffset = m_ptDisplay.x - si.nPage + 1;
		break;
	case SB_PAGEDOWN:
		nNewOffset = m_ptDisplay.x + si.nPage - 1;
		break;
	case SB_THUMBPOSITION:
	case SB_THUMBTRACK:
		nNewOffset = si.nTrackPos;
		break;
	default:
		return;
	}

	if (nNewOffset >= nMaxLineLength)
		nNewOffset = nMaxLineLength - 1;
	if (nNewOffset < 0)
		nNewOffset = 0;
	ScrollToChar(nNewOffset, TRUE);
	UpdateCaret();
}

void CFancyTextView::OnTimer(UINT nIDEvent) 
{
	CView::OnTimer(nIDEvent);

	if (nIDEvent == FANCY_TIMER_DRAGSEL)
	{
		ASSERT(m_bDragSelection);
		CPoint pt;
		::GetCursorPos(&pt);
		ScreenToClient(&pt);
		CRect rcClient;
		GetClientRect(&rcClient);

		BOOL bChanged = FALSE;

		//	Scroll vertically, if necessary
		int nNewTopLine = m_ptDisplay.y;
		int nLineCount = GetLineCount();
		if (pt.y < rcClient.top)
		{
			nNewTopLine --;
			if (pt.y < rcClient.top - GetLineHeight())
				nNewTopLine -= 2;
		}
		else
		if (pt.y >= rcClient.bottom)
		{
			nNewTopLine ++;
			if (pt.y >= rcClient.bottom + GetLineHeight())
				nNewTopLine += 2;
		}

		if (nNewTopLine < 0)
			nNewTopLine = 0;
		if (nNewTopLine >= nLineCount)
			nNewTopLine = nLineCount - 1;

		if (m_ptDisplay.y != nNewTopLine)
		{
			ScrollToLine(nNewTopLine);
			//UpdateSiblingScrollPos(TRUE);
			bChanged = TRUE;
		}

		//	Scroll horizontally, if necessary
		int nNewOffsetChar = m_ptDisplay.x;
		int nMaxLineLength = GetMaxLineCharCount();
		if (pt.x < rcClient.left)
			nNewOffsetChar --;
		else
		if (pt.x >= rcClient.right)
			nNewOffsetChar ++;

		if (nNewOffsetChar >= nMaxLineLength)
			nNewOffsetChar = nMaxLineLength - 1;
		if (nNewOffsetChar < 0)
			nNewOffsetChar = 0;

		if (m_ptDisplay.x != nNewOffsetChar)
		{
			ScrollToChar(nNewOffsetChar);
			UpdateCaret();
			//UpdateSiblingScrollPos(FALSE);
			bChanged = TRUE;
		}

		//	Fix changes
		if (bChanged)
		{
			AdjustTextPoint(pt);
			CPoint ptNewCursorPos = ClientToText(pt);
			SetSelection(m_ptAnchor, ptNewCursorPos);
		}
	}
}

void CFancyTextView::MoveCursor(CPoint point, BOOL bSelect)
{
  CPoint ptNewAnchor;
  CPoint ptNewCursor;
  ptNewAnchor = m_ptAnchor;
  ptNewCursor = point;
  if(!bSelect) ptNewAnchor = ptNewCursor;
	SetSelection(ptNewAnchor, ptNewCursor);
}

CPoint CFancyTextView::LineUp(CPoint pt)
{
  pt.y--;
  return BoundsCheck(pt);
}

CPoint CFancyTextView::LineDown(CPoint pt)
{
  pt.y++;
  return BoundsCheck(pt);
}

CPoint CFancyTextView::PageUp(CPoint pt)
{
  pt.y -= GetScreenLines();
  return BoundsCheck(pt);
}

CPoint CFancyTextView::PageDown(CPoint pt)
{
  pt.y += GetScreenLines();
  return BoundsCheck(pt);
}

CPoint CFancyTextView::BoundsCheck(CPoint pt)
{
  if(pt.y < 0) pt.y = 0;
  if(pt.y > GetLineCount()) pt.y = GetLineCount();
  int nCharCount = 1024;
  if(pt.x < 0) pt.x = 0;
  if(pt.x > nCharCount) pt.x = nCharCount;
  return pt;
}

void CFancyTextView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
//TRACE("CFancyTextView::OnKeyDown\n");

  BOOL bShift   = GetKeyState(VK_SHIFT  ) & 0x8000 ? 1 : 0;
  BOOL bControl = GetKeyState(VK_CONTROL) & 0x8000 ? 1 : 0;
  switch(nChar) {
  case VK_UP:
    MoveCursor(LineUp(m_ptCursor), bShift);
    break;
  case VK_DOWN:
    MoveCursor(LineDown(m_ptCursor), bShift);
    break;
  case VK_LEFT:
    MoveCursor(CharToLeft(m_ptCursor), bShift);
    break;
  case VK_RIGHT:
    MoveCursor(CharToRight(m_ptCursor), bShift);
    break;
  case VK_HOME:
    MoveCursor(bControl ? ToStart(m_ptCursor) : ToLineStart(m_ptCursor), bShift);
    break;
  case VK_END:
    MoveCursor(bControl ? ToEnd(m_ptCursor) : ToLineEnd(m_ptCursor), bShift);
    break;
  case VK_PRIOR:
    MoveCursor(bControl ? m_ptCursor : PageUp(m_ptCursor), bShift);
    break;
  case VK_NEXT:
    MoveCursor(bControl ? m_ptCursor : PageDown(m_ptCursor), bShift);
    break;
  }

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

CPoint CFancyTextView::ToLineStart(CPoint pt)
{
  pt.x = 0;
  return BoundsCheck(pt);
}

CPoint CFancyTextView::ToLineEnd(CPoint pt)
{
  CDWordArray craFG, craBG;
  pt.x = 0;
  if(pt.y >= 0 && pt.y < GetLineCount()) pt.x = GetLineText(craFG, craBG, pt.y).GetLength();
  return BoundsCheck(pt);
}

CPoint CFancyTextView::ToStart(CPoint pt)
{
  pt.x = 0;
  pt.y = 0;
  return BoundsCheck(pt);
}

CPoint CFancyTextView::ToEnd(CPoint pt)
{
  pt.x = 0;
  pt.y = GetLineCount();
  return BoundsCheck(pt);
}

void CFancyTextView::OnUpdateEditCopy(CCmdUI* pCmdUI) {
  if(!m_bFocused) return;
	pCmdUI->Enable(m_ptAnchor != m_ptCursor);
}

void CFancyTextView::OnEditCopy() 
{
  CString strText = GetSelectedText();

	HGLOBAL hGlobal;		// Global memory handle
	LPSTR lpszData;			// Pointer to clipboard data
	unsigned long nSize;	// Size of clipboard data

	// First, open the clipboard. OpenClipboard() takes one
	// parameter, the handle of the window that will temporarily
	// be it's owner. If NULL is passed, the current process
	// is assumed. After opening, empty the clipboard so we
	// can put our text on it.
	OpenClipboard();
	EmptyClipboard();

	// Get the size of the string in the buffer that was
	// passed into the function, so we know how much global
	// memory to allocate for the string.
	nSize = strText.GetLength();

	// Allocate the memory for the string.
	hGlobal = GlobalAlloc(GMEM_ZEROINIT, nSize+1);
	
	// If we got any error during the memory allocation,
	// we have been returned a NULL handle.
	if(!hGlobal) return;

	// Now we have a global memory handle to the text
	// stored on the clipboard. We have to lock this global
	// handle so that we have access to it.
	lpszData = (LPSTR)GlobalLock(hGlobal);

	// Now, copy the text from the buffer into the allocated
	// global memory pointer.
	for (UINT i = 0; i < nSize + 1; ++i)
		*(lpszData + i) = *(((LPCTSTR)strText) + i);

	// Now, simply unlock the global memory pointer,
	// set the clipboard data type and pointer,
	// and close the clipboard.
	GlobalUnlock(hGlobal);
	SetClipboardData(CF_TEXT, hGlobal);
	CloseClipboard();

}

void CFancyTextView::OnUpdateEditFind(CCmdUI* pCmdUI) {
  pCmdUI->Enable(m_ptAnchor.y == m_ptCursor.y);
}

void CFancyTextView::GetSelection(CPoint& ptAnchor, CPoint& ptCursor) const
{
  ptAnchor = m_ptAnchor;
  ptCursor = m_ptCursor;
}

CString CFancyTextView::GetTextRange(CPoint ptStart, CPoint ptEnd)
{
  CDWordArray craFG, craBG;
  CString strTemp;
  int nLine;

  for(nLine = ptStart.y; nLine <= ptEnd.y; nLine++) {
    CString strText = GetLineText(craFG, craBG, nLine);
    int nCharCount = strText.GetLength();
    int x0 = 0;
    int x1 = nCharCount;
    if(nLine == ptStart.y) x0 = ptStart.x;
    if(nLine == ptEnd.y) x1 = ptEnd.x;
    if(x0 > nCharCount) x0 = nCharCount;
    if(x0 < 0) x0 = 0;
    if(x1 > nCharCount) x1 = nCharCount;
    if(x1 < 0) x1 = 0;
    if(x1 < x0) x1 = x0;
    strTemp += strText.Mid(x0, x1 - x0);
    if(nLine < ptEnd.y) strTemp += _T("\r\n");
  }

  return strTemp;
}

COLORREF CFancyTextView::GetDefaultFGColor()
{
  return RGB(192,192,192);
}

void CFancyTextView::OnEditFind() 
{
  CFindDlg dlgFind;

  dlgFind.m_nDir = m_findhistory.IsBackwards() ? 0 : 1;

//  dlgFind.m_nDir = 0;

  dlgFind.m_saHistory = m_findhistory.GetStringArray();
  dlgFind.m_strFindWhat = GetSelectedText();
  dlgFind.m_bMatchCase = m_findhistory.IsMatchCase();
  dlgFind.m_bMatchSpaces = m_findhistory.IsMatchSpaces();
  if(dlgFind.m_strFindWhat.IsEmpty()) {
    dlgFind.m_strFindWhat = m_findhistory.GetMostRecentString();
  }

  if(dlgFind.DoModal() != IDOK) return;
  /*
  ** Input an empty string? just quit
  */
  if(dlgFind.m_strFindWhat.IsEmpty()) return;

  m_findhistory.AddString(dlgFind.m_strFindWhat);
  m_findhistory.SetBackwards(dlgFind.m_nDir == 0);
  m_findhistory.SetMatchCase(dlgFind.m_bMatchCase);
  m_findhistory.SetMatchSpaces(dlgFind.m_bMatchSpaces);

  Find(dlgFind.m_strFindWhat, dlgFind.m_nDir == 0, dlgFind.m_bMatchCase, dlgFind.m_bMatchSpaces);
}

void CFancyTextView::InvalidateSelectedLines()
{
//	if(m_ptCursor != m_ptAnchor) {
    if(m_ptCursor.y < m_ptAnchor.y) {
      InvalidateLines(m_ptCursor.y, m_ptAnchor.y);
    } else {
      InvalidateLines(m_ptAnchor.y, m_ptCursor.y);
    }
//  }
}

CString CFancyTextView::GetSelectedText()
{
  CPoint ptStart, ptEnd;
  GetSelection(ptStart, ptEnd);
  EnforcePointOrder(ptStart, ptEnd);
  return GetTextRange(ptStart, ptEnd);
}

void CFancyTextView::EnforcePointOrder(CPoint &ptStart, CPoint &ptEnd)
{
  if(
    (ptStart.y > ptEnd.y) ||
    ((ptStart.y == ptEnd.y) && (ptStart.x > ptEnd.x))
  ) {
    CPoint ptTemp;
    ptTemp  = ptStart;
    ptStart = ptEnd;
    ptEnd   = ptTemp;
  }
}

BOOL CFancyTextView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
  int nNewLine = m_ptDisplay.y - (zDelta / 40);
  int nLineCount = GetLineCount();
  if(nNewLine < 0) nNewLine = 0;
  if(nNewLine > nLineCount) nNewLine = nLineCount;
  ScrollToLine(nNewLine);
	return CView::OnMouseWheel(nFlags, zDelta, pt);
}

void CFancyTextView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	RecalcVertScrollBar();
	RecalcHorzScrollBar();
  Invalidate(FALSE);
}

void CFancyTextView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CView::OnRButtonDown(nFlags, point);
	CPoint ptStart, ptEnd;
  GetSelection(ptStart, ptEnd);
  if(ptStart != ptEnd) return;

	AdjustTextPoint(point);
	ptEnd   = ClientToText(point);
  ptStart = ptEnd;
	SetSelection(ptStart, ptEnd);
}

void CFancyTextView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
  CMenu Menu;
  if(Menu.LoadMenu(IDR_CONTEXT_COPYONLY)) {
    CMenu* pPopup = Menu.GetSubMenu(0);
    my_init_popup(this, pPopup, TRUE);
    pPopup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,this);
  }
}

int CFancyTextView::FindString(LPCTSTR lpszLine, LPCTSTR lpszPattern, BOOL bBackwards) const
{
  ASSERT(lpszLine);
  ASSERT(lpszPattern);
  int pl = lstrlen(lpszPattern);
  int ll = lstrlen(lpszLine);
  int i;
  if(bBackwards) {
    for(i = ll - 1; i >= 0; i--) {
      if(!memcmp(lpszLine + i, lpszPattern, sizeof(TCHAR) * pl)) return i;
    }
  } else {
    for(i = 0; i < ll; i++) {
      if(!memcmp(lpszLine + i, lpszPattern, sizeof(TCHAR) * pl)) return i;
    }
  }
  return -1;
}

int CFancyTextView::FindStringWithOptions(LPCTSTR lpszLine, LPCTSTR lpszPattern, BOOL bBackwards, BOOL bMatchCase, BOOL bMatchSpaces)
{
  ASSERT(lpszLine);
  ASSERT(lpszPattern);
  int pl = lstrlen(lpszPattern);
  int ll = lstrlen(lpszLine);
  int i, j;
  /*
  ** Allocate space for the filtered indexes/lines
  */
  int *index_l_filtered = new int[ll+1];
  LPTSTR lpszLineFiltered    = new TCHAR[ll+1];
  LPTSTR lpszPatternFiltered = new TCHAR[pl+1];
  /*
  ** Filter the pattern
  */
  for(i = 0, j = 0; j < pl; j++) {
    TCHAR p = lpszPattern[j];
    if(!bMatchCase) p = tolower(p);
    if(!bMatchSpaces && isspace(p)) continue;
    lpszPatternFiltered[i++] = p;
  }
  lpszPatternFiltered[i] = 0;
  /*
  ** Filter the line, copying the indexes too
  */
  for(i = 0, j = 0; j < ll; j++) {
    TCHAR p = lpszLine[j];
    if(!bMatchCase) p = tolower(p);
    if(!bMatchSpaces && isspace(p)) continue;
    index_l_filtered[i] = j;
    lpszLineFiltered[i++] = p;
  }
  lpszLineFiltered[i] = 0;

  int r_index = -1;
  if(lpszPatternFiltered[0] && lpszLineFiltered[0]) {
    i = FindString(lpszLineFiltered, lpszPatternFiltered, bBackwards);
    if(i >= 0) r_index = index_l_filtered[i];
  }

  delete[] lpszPatternFiltered;
  delete[] lpszLineFiltered;
  delete[] index_l_filtered;

  return r_index;
}

void CFancyTextView::Find(LPCTSTR lpszPattern, BOOL bBackwards, BOOL bMatchCase, BOOL bMatchSpaces)
{
  //AfxGetMainWnd()->MessageBox(_T("not implemented"));

  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  EnforcePointOrder(ptAnchor, ptCursor);

  int x, y, ytarget, lc, step;
  lc = GetLineCount();

  if(bBackwards) {
    x = ptAnchor.x;
    y = ptAnchor.y;
    step = -1;
    ytarget = 0;
  } else {
    x = ptCursor.x;
    y = ptCursor.y;
    step = 1;
    ytarget = lc;
  }
  if(x < 0) x = 0;
  if(y < 0) y = 0;
  if(y > lc) y = lc;

  for(; y != ytarget; y += step) {
    CPoint ptStart, ptEnd;
    ptStart.y = ptEnd.y = y;
    ptStart.x = 0;
    ptEnd.x = 0x7FFFFFFF;
    if(y == ptCursor.y) {
      if(bBackwards) {
        ptEnd.x = x;
      } else {
        ptStart.x = x;
      }
    }
    CString str = GetTextRange(ptStart, ptEnd);
    int nIndex = FindStringWithOptions(str, lpszPattern, bBackwards, bMatchCase, bMatchSpaces);
    if(nIndex >= 0) {
      ptStart.x += nIndex;
      SetSelection(ptStart, ptStart, 25);
      return;
    }
  }

  CString str;
  str.Format(_T("Cannot find the string '%s'."), lpszPattern);
  AfxGetMainWnd()->MessageBox(str, _T("Find"), MB_OK|MB_ICONEXCLAMATION);

}
