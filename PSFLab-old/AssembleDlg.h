#if !defined(AFX_ASSEMBLEDLG_H__9AA07CF0_ECB6_4E23_92A6_789189862D38__INCLUDED_)
#define AFX_ASSEMBLEDLG_H__9AA07CF0_ECB6_4E23_92A6_789189862D38__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AssembleDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAssembleDlg dialog

class CAssembleDlg : public CDialog
{
// Construction
public:
	CAssembleDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAssembleDlg)
	enum { IDD = IDD_ASSEMBLE };
	CString	m_strAddress;
	CString	m_strInstruction;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAssembleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAssembleDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ASSEMBLEDLG_H__9AA07CF0_ECB6_4E23_92A6_789189862D38__INCLUDED_)
