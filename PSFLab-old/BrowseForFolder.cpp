#include "stdafx.h"

#include <shlobj.h>

#include "BrowseForFolder.h"

static int CALLBACK BrowseCallbackProc(
  HWND hwnd,
  UINT uMsg,
  LPARAM lParam,
  LPARAM lpData
) {
  switch(uMsg) {
  case BFFM_INITIALIZED:
    SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
    break;
  case BFFM_SELCHANGED:
    break;
  default:
    break;
  }
  return 0;
}

BOOL BrowseForFolder(
  HWND    hwnd,
	LPCTSTR title,
  LPTSTR  pathbuffer
) {
  TCHAR pathdef[MAX_PATH + 1];
  lstrcpy(pathdef, pathbuffer);

  IMalloc* pMalloc;
  LPITEMIDLIST pidl;
  BROWSEINFO bi;
  BOOL bRet = FALSE;

  if (SHGetMalloc(&pMalloc) != NOERROR) return(FALSE);
  // fills the structure.
  bi.hwndOwner = hwnd;
  bi.pidlRoot = NULL;
  bi.pszDisplayName = pathbuffer;
  bi.lpszTitle = title;
  bi.ulFlags = 0;
  bi.lpfn = (BFFCALLBACK)BrowseCallbackProc;
  bi.lParam = (LPARAM)pathdef;

  // invokes the dialog.
  if((pidl = SHBrowseForFolder(&bi)) != NULL) {
    bRet = SHGetPathFromIDList(pidl, pathbuffer);
    pMalloc->Free(pidl);
  }

  // clean up.
  pMalloc->Release();

  return(bRet);
}
