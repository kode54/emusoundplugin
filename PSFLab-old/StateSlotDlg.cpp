// StateSlotDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "StateSlotDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStateSlotDlg dialog


CStateSlotDlg::CStateSlotDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStateSlotDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CStateSlotDlg)
	m_nSlot = -1;
	m_strInUse0 = _T("");
	m_strInUse1 = _T("");
	m_strInUse2 = _T("");
	m_strInUse3 = _T("");
	m_strInUse4 = _T("");
	m_strInUse5 = _T("");
	m_strInUse6 = _T("");
	m_strInUse7 = _T("");
	m_strInUse8 = _T("");
	m_strInUse9 = _T("");
	//}}AFX_DATA_INIT
}


void CStateSlotDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStateSlotDlg)
	DDX_Radio(pDX, IDC_RADIO1, m_nSlot);
	DDX_Text(pDX, IDC_INUSE0, m_strInUse0);
	DDX_Text(pDX, IDC_INUSE1, m_strInUse1);
	DDX_Text(pDX, IDC_INUSE2, m_strInUse2);
	DDX_Text(pDX, IDC_INUSE3, m_strInUse3);
	DDX_Text(pDX, IDC_INUSE4, m_strInUse4);
	DDX_Text(pDX, IDC_INUSE5, m_strInUse5);
	DDX_Text(pDX, IDC_INUSE6, m_strInUse6);
	DDX_Text(pDX, IDC_INUSE7, m_strInUse7);
	DDX_Text(pDX, IDC_INUSE8, m_strInUse8);
	DDX_Text(pDX, IDC_INUSE9, m_strInUse9);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CStateSlotDlg, CDialog)
	//{{AFX_MSG_MAP(CStateSlotDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStateSlotDlg message handlers
