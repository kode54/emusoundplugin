// CodeView.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "CodeView.h"

#include "GoToDlg.h"
#include "SelectRangeDlg.h"
#include "AssembleDlg.h"
#include "PatchDlg.h"
#include "FillDlg.h"

#include "MIPSQuickAssembler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCodeView

IMPLEMENT_DYNCREATE(CCodeView, CFancyTextView)

CCodeView::CCodeView()
{
}

CCodeView::~CCodeView()
{
}


BEGIN_MESSAGE_MAP(CCodeView, CFancyTextView)
	//{{AFX_MSG_MAP(CCodeView)
	ON_UPDATE_COMMAND_UI(ID_EDIT_JUMP, OnUpdateEditJump)
	ON_UPDATE_COMMAND_UI(ID_EDIT_GOTO, OnUpdateEditGoto)
	ON_UPDATE_COMMAND_UI(ID_EDIT_RETURN, OnUpdateEditReturn)
	ON_UPDATE_COMMAND_UI(ID_EDIT_EMPTY, OnUpdateEditEmpty)
	ON_COMMAND(ID_EDIT_GOTO, OnEditGoto)
	ON_UPDATE_COMMAND_UI(ID_EDIT_BREAKPOINT, OnUpdateEditBreakpoint)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PATCH, OnUpdateEditPatch)
	ON_UPDATE_COMMAND_UI(ID_EDIT_ASSEMBLE, OnUpdateEditAssemble)
	ON_UPDATE_COMMAND_UI(ID_EDIT_FILL, OnUpdateEditFill)
	ON_UPDATE_COMMAND_UI(ID_EDIT_DELETEPATCH, OnUpdateEditDeletepatch)
	ON_UPDATE_COMMAND_UI(ID_EDIT_SELECTRANGE, OnUpdateEditSelectrange)
	ON_COMMAND(ID_EDIT_SELECTRANGE, OnEditSelectrange)
	ON_COMMAND(ID_EDIT_ASSEMBLE, OnEditAssemble)
	ON_COMMAND(ID_EDIT_RETURN, OnEditReturn)
	ON_COMMAND(ID_EDIT_JUMP, OnEditJump)
	ON_COMMAND(ID_EDIT_EMPTY, OnEditEmpty)
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_EDIT_PATCH, OnEditPatch)
	ON_COMMAND(ID_EDIT_BREAKPOINT, OnEditBreakpoint)
	ON_COMMAND(ID_EDIT_DELETEPATCH, OnEditDeletepatch)
	ON_COMMAND(ID_EDIT_FILL, OnEditFill)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_EDIT_CURRENTLINE, OnEditCurrentline)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_RUNTOCURSOR, OnUpdateDebugRuntocursor)
	ON_COMMAND(ID_DEBUG_RUNTOCURSOR, OnDebugRuntocursor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCodeView drawing

/////////////////////////////////////////////////////////////////////////////
// CCodeView diagnostics

#ifdef _DEBUG
void CCodeView::AssertValid() const
{
	CFancyTextView::AssertValid();
}

void CCodeView::Dump(CDumpContext& dc) const
{
	CFancyTextView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCodeView message handlers

int CCodeView::GetLineCount()
{
  return ((2*1024*1024)+(512*1024))/4;
}

void CCodeView::OnUpdateEditJump(CCmdUI* pCmdUI) 
{
	// can jump if there's something good underneath the cursor
  CString strHex8;
  strHex8 = FindCompleteHex8AtCursor();
  if(strHex8.GetLength()) {
    pCmdUI->Enable(TRUE);
  } else {
	  pCmdUI->Enable(FALSE);
  }
}

void CCodeView::OnUpdateEditGoto(CCmdUI* pCmdUI) 
{
	// can always go to
	pCmdUI->Enable(TRUE);
}

void CCodeView::OnUpdateEditReturn(CCmdUI* pCmdUI) 
{
	// can return if there's anything on the nav stack
  if(NavStackSize()) {
    pCmdUI->Enable(TRUE);
  } else {
	  pCmdUI->Enable(FALSE);
  }
}

void CCodeView::OnUpdateEditEmpty(CCmdUI* pCmdUI) 
{
	// can empty nav stack if there's anything on it
  if(NavStackSize()) {
	  pCmdUI->Enable(TRUE);
  } else {
	  pCmdUI->Enable(FALSE);
  }
}

void CCodeView::OnEditGoto() 
{
  CGoToDlg dlgGoTo;
  dlgGoTo.m_dwAddress = GetCursorPC();
  if(dlgGoTo.DoModal() != IDOK) return;
  NavJump(dlgGoTo.m_dwAddress);
}

void CCodeView::OnUpdateEditBreakpoint(CCmdUI* pCmdUI) 
{
  // if running, disable
  if(GetWorkArea()->IsRunning()) { pCmdUI->Enable(FALSE); return; }
  // if selection spans more than one line, disable
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y != ptCursor.y) { pCmdUI->Enable(FALSE); return; }
  // if the cursor is not on a breakpointable line, disable
  if(ptCursor.y < 0 || ptCursor.y >= 0xA0000) { pCmdUI->Enable(FALSE); return; }
	// otherwise, enable
	pCmdUI->Enable(TRUE);
}

void CCodeView::OnUpdateEditPatch(CCmdUI* pCmdUI) 
{
  // if running, disable
  if(GetWorkArea()->IsRunning()) { pCmdUI->Enable(FALSE); return; }
  // if selection spans more than one line, disable
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y != ptCursor.y) { pCmdUI->Enable(FALSE); return; }
  // if the cursor is not on a patchable RAM line, disable
  if(ptCursor.y < (0x0000/4) || ptCursor.y >= (0x200000/4)) { pCmdUI->Enable(FALSE); return; }
	// otherwise, enable
	pCmdUI->Enable(TRUE);
}

void CCodeView::OnUpdateEditAssemble(CCmdUI* pCmdUI) 
{
  // if running, disable
  if(GetWorkArea()->IsRunning()) { pCmdUI->Enable(FALSE); return; }
  // if selection spans more than one line, disable
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y != ptCursor.y) { pCmdUI->Enable(FALSE); return; }
  // if the cursor is not on a patchable RAM line, disable
  if(ptCursor.y < (0x0000/4) || ptCursor.y >= (0x200000/4)) { pCmdUI->Enable(FALSE); return; }
	// otherwise, enable
	pCmdUI->Enable(TRUE);
}

void CCodeView::OnUpdateEditFill(CCmdUI* pCmdUI) 
{
  // if running, disable
  if(GetWorkArea()->IsRunning()) { pCmdUI->Enable(FALSE); return; }
	// enable it otherwise
	pCmdUI->Enable(TRUE);
}

void CCodeView::OnUpdateEditDeletepatch(CCmdUI* pCmdUI) 
{
  // if running, disable
  if(GetWorkArea()->IsRunning()) { pCmdUI->Enable(FALSE); return; }
  // enable it if multiple lines are selected
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y != ptCursor.y) { pCmdUI->Enable(TRUE); return; }
	// enable it if the current line is patched
  if(GetWorkArea()->IsWordPatched(GetCursorPC())) { pCmdUI->Enable(TRUE); return; }
  // otherwise don't
	pCmdUI->Enable(FALSE);
}

CString CCodeView::GetLineText(CDWordArray& craFG, CDWordArray& craBG, int nLineIndex)
{
  COLORREF crsetBG                = theApp.GetColor(PSFLAB_COLOR_CODE_BG);
  COLORREF crsetBGCurrentLine     = theApp.GetColor(PSFLAB_COLOR_CODE_BG_CURRENT_LINE);
  COLORREF crsetBGPatched         = theApp.GetColor(PSFLAB_COLOR_CODE_BG_PATCHED);
  COLORREF crsetBGBreakpoint      = theApp.GetColor(PSFLAB_COLOR_CODE_BG_BREAKPOINT);
  COLORREF crsetFG                = theApp.GetColor(PSFLAB_COLOR_CODE_FG);
  COLORREF crsetFGCurrentLine     = theApp.GetColor(PSFLAB_COLOR_CODE_FG_CURRENT_LINE);
  COLORREF crsetFGBreakpoint      = theApp.GetColor(PSFLAB_COLOR_CODE_FG_BREAKPOINT);
  COLORREF crsetFGCursorHighlight = theApp.GetColor(PSFLAB_COLOR_CODE_FG_CURSOR_HIGHLIGHT);

  craFG.SetSize(0);
  craBG.SetSize(0);
  CPSFWorkArea *workarea = GetWorkArea();
  DWORD dwAddress;
  DWORD dwPC = workarea->GetPC();
  if(nLineIndex <  0x00000) return _T("");
  if(nLineIndex >= 0xA0000) return _T("");
  if(nLineIndex < 0x80000) {
    dwAddress = (4 * nLineIndex)&0x1FFFFC;
    if((dwPC & 0x1FFFFFFF) < 0x1F000000) {
      dwAddress |= (dwPC & 0xFFE00000);
    } else {
      dwAddress |= 0x00000000;
    }
  } else {
    dwAddress = (4 * nLineIndex)&0x07FFFC;
    if((dwPC & 0x1FFFFFFF) >= 0x1FC00000) {
      dwAddress |= (dwPC & 0xFFF80000);
    } else {
      dwAddress |= 0xBFC00000;
    }
  }
  TCHAR szBuffer[60];
  craFG.SetSize(60);
  craBG.SetSize(60);
  wsprintf(szBuffer, "%08X: ", dwAddress);
  int i;
  for(i = 0; i < 10; i++) craFG[i] = crsetFG;
  Disassemble(szBuffer + 10, ((DWORD*)craFG.GetData()) + 10, dwAddress, workarea->PeekWord(dwAddress));
  COLORREF crBG_a = crsetBG;
  COLORREF crBG_b = crsetBG;
  COLORREF crBG_c = crsetBG;
  /*
  ** Only highlight the current PC if not running
  */
  if(!GetWorkArea()->IsRunning()) {
    if((dwAddress & 0x1FFFFFFC) == ((workarea->GetPC()) & 0x1FFFFFFC)) {
      crBG_a = crsetBGCurrentLine;
      crBG_b = crsetBGCurrentLine;
      crBG_c = crsetBGCurrentLine;
      for(i=0;i<8;i++) craFG[i] = crsetFGCurrentLine;
    }
  }
  if(workarea->IsWordPatched(dwAddress)) {
    crBG_b = crsetBGPatched;
  }
  if(workarea->IsBreakpointOnExecute(dwAddress)) {
    crBG_a = crsetBGBreakpoint;
    for(i=0;i<8;i++) craFG[i] = crsetFGBreakpoint;
  }
  for(i = 0; szBuffer[i]; i++) craBG[i] = crBG_b;
  craFG.SetSize(i + 1);
  craBG.SetSize(i + 1);
  craFG[i] = crBG_c;
  craBG[i] = crBG_c;
  for(i = 0; i < 8; i++) craBG[i] = crBG_a;
  for(i = 8; i < 10; i++) craBG[i] = crBG_c;
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(IsFocused() && ptAnchor == ptCursor && ptCursor.y == nLineIndex) {
    for(i = 0; i < 8; i++) { craFG[i] = crsetFGCursorHighlight; }
    LPCTSTR lpszHex8 = FindCompleteHex8InString(szBuffer, ptCursor.x);
    if(lpszHex8) {
      for(i = 0; i < 10; i++) {
        craFG[(lpszHex8 - szBuffer) + i] = crsetFGCursorHighlight;
      }
    }
  }

  return (CString)(szBuffer);
}

int CCodeView::GetMaxLineCharCount()
{
  return 36;
}

void CCodeView::OnUpdateEditSelectrange(CCmdUI* pCmdUI) 
{
	// always available
	pCmdUI->Enable(TRUE);
}

void CCodeView::OnEditSelectrange() 
{
  CSelectRangeDlg dlgSelectRange;
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y <= ptCursor.y) {
    dlgSelectRange.m_dwStartAddress = LineIndexToPC(ptAnchor.y);
    dlgSelectRange.m_dwEndAddress   = LineIndexToPC(ptCursor.y);
  } else {
    dlgSelectRange.m_dwStartAddress = LineIndexToPC(ptCursor.y);
    dlgSelectRange.m_dwEndAddress   = LineIndexToPC(ptAnchor.y);
  }
  if(dlgSelectRange.DoModal() != IDOK) return;
  ptAnchor.x = 0;
  ptCursor.x = 0;
  ptAnchor.y = PCToLineIndex(dlgSelectRange.m_dwStartAddress);
  ptCursor.y = PCToLineIndex(dlgSelectRange.m_dwEndAddress);
  if(ptAnchor.y > ptCursor.y) {
    int temp = ptCursor.y;
    ptAnchor.y = ptCursor.y;
    ptCursor.y = temp;
  }
  SetSelection(ptAnchor, ptCursor, 50);
}

/***************************************************************************/

#define INS_S (UINT32)((dwInstruction>>21)&0x1F)
#define INS_T (UINT32)((dwInstruction>>16)&0x1F)
#define INS_D (UINT32)((dwInstruction>>11)&0x1F)
#define INS_H (UINT32)((dwInstruction>>6)&0x1F)
#define INS_I (UINT32)(dwInstruction&0xFFFF)

#define SIGNED16(x) ((INT32)(((SHORT)(x))))

#define REL_I (dwPC+4+(((INT32)(SIGNED16(INS_I)))<<2))
#define ABS_I ((dwPC&0xF0000000)|((dwInstruction<<2)&0x0FFFFFFC))

int CCodeView::DH(LPTSTR lpszDest, LPCOLORREF lpcrFG, DWORD dwPC, DWORD dwInstruction, LPCTSTR lpszFormat)
{
  COLORREF crsetFG                = theApp.GetColor(PSFLAB_COLOR_CODE_FG);
  COLORREF crsetFGImmediate       = theApp.GetColor(PSFLAB_COLOR_CODE_FG_IMMEDIATE);
  COLORREF crsetFGAddress         = theApp.GetColor(PSFLAB_COLOR_CODE_FG_ADDRESS);
  COLORREF crsetFGPunctuation     = theApp.GetColor(PSFLAB_COLOR_CODE_FG_PUNCTUATION);
  COLORREF crsetFGUnknown         = theApp.GetColor(PSFLAB_COLOR_CODE_FG_UNKNOWN);
  static const LPCTSTR alpszRegName[32] = {
    _T("0") ,_T("at"),_T("v0"),_T("v1"),_T("a0"),_T("a1"),_T("a2"),_T("a3"),
    _T("t0"),_T("t1"),_T("t2"),_T("t3"),_T("t4"),_T("t5"),_T("t6"),_T("t7"),
    _T("s0"),_T("s1"),_T("s2"),_T("s3"),_T("s4"),_T("s5"),_T("s6"),_T("s7"),
    _T("t8"),_T("t9"),_T("k0"),_T("k1"),_T("gp"),_T("sp"),_T("fp"),_T("ra")
  };
  int nColumn = 0;
  int nColorColumn = 0;
  int j;
  #define DO_COLORS(n) { while(nColorColumn < nColumn) { lpcrFG[nColorColumn++] = (n); } }
  while(*lpszFormat) {
    switch(*lpszFormat) {
    case _T('S'): lpszDest[nColumn++] = _T('$'); lpszDest[nColumn++] = alpszRegName[INS_S][0]; if(INS_S) lpszDest[nColumn++] = alpszRegName[INS_S][1]; DO_COLORS(crsetFG); break;
    case _T('T'): lpszDest[nColumn++] = _T('$'); lpszDest[nColumn++] = alpszRegName[INS_T][0]; if(INS_T) lpszDest[nColumn++] = alpszRegName[INS_T][1]; DO_COLORS(crsetFG); break;
    case _T('D'): lpszDest[nColumn++] = _T('$'); lpszDest[nColumn++] = alpszRegName[INS_D][0]; if(INS_D) lpszDest[nColumn++] = alpszRegName[INS_D][1]; DO_COLORS(crsetFG); break;
    case _T('H'): wsprintf(lpszDest + nColumn, "%d", INS_H); while(lpszDest[nColumn]) nColumn++; DO_COLORS(crsetFGImmediate); break;
    case _T('I'): wsprintf(lpszDest + nColumn, "0x%04X", INS_I); while(lpszDest[nColumn]) nColumn++; DO_COLORS(crsetFGImmediate); break;
    case _T('B'): wsprintf(lpszDest + nColumn, "0x%08X", REL_I); while(lpszDest[nColumn]) nColumn++; DO_COLORS(crsetFGAddress); break;
    case _T('J'): wsprintf(lpszDest + nColumn, "0x%08X", ABS_I); while(lpszDest[nColumn]) nColumn++; DO_COLORS(crsetFGAddress); break;
    case _T('?'): wsprintf(lpszDest + nColumn, "<%08X>", dwInstruction); while(lpszDest[nColumn]) nColumn++; DO_COLORS(crsetFGUnknown); break;
    case _T('C'):
      lpszFormat++;
      j = 0;
      switch(*lpszFormat) {
      case _T('D'): j = INS_D; break;
      }
      switch(j) {
      case 12: wsprintf(lpszDest + nColumn, "C0_status"); break;
      case 13: wsprintf(lpszDest + nColumn, "C0_cause"); break;
      case 14: wsprintf(lpszDest + nColumn, "C0_epc"); break;
      case 15: wsprintf(lpszDest + nColumn, "C0_prid"); break;
      default: wsprintf(lpszDest + nColumn, "C0_%d", j);
      }
      while(lpszDest[nColumn]) nColumn++;
      DO_COLORS(crsetFG);
      break;
    case _T(' '): while(nColumn < 6) { lpszDest[nColumn++] = _T(' '); } DO_COLORS(crsetFGPunctuation); break;
    case _T(','): lpszDest[nColumn++] = _T(','); lpszDest[nColumn++] = _T(' '); DO_COLORS(crsetFGPunctuation); break;
    case _T('('):
    case _T(')'): lpszDest[nColumn++] = (TCHAR)(*lpszFormat); DO_COLORS(crsetFGPunctuation); break;
    default: lpszDest[nColumn++] = (TCHAR)(*lpszFormat); DO_COLORS(crsetFG); break;
    }
    lpszFormat++;
  }
  lpszDest[nColumn] = 0;
  return nColumn;
}

int CCodeView::Disassemble(LPTSTR lpszDest, LPCOLORREF lpcrFG, DWORD dwPC, DWORD dwInstruction)
{
  if(dwInstruction < 0x04000000) {
    switch(dwInstruction & 0x3F) {
    case 0x00:
      if(!dwInstruction) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "nop");
      if(INS_D==INS_T)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sll D,H");
      if(!INS_H)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sll D,T,H");
    case 0x02:
      if(INS_D==INS_T)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "srl D,H");
      if(!INS_H)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "srl D,T,H");
    case 0x03:
      if(INS_D==INS_T)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sra D,H");
      if(!INS_H)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sra D,T,H");
    case 0x04:
      if(INS_D==INS_T)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sllv D,S");
      if(!INS_S)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sllv D,T,S");
    case 0x06:
      if(INS_D==INS_T)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "srlv D,S");
      if(!INS_S)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "srlv D,T,S");
    case 0x07:
      if(INS_D==INS_T)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "srav D,S");
      if(!INS_S)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "srav D,T,S");
    case 0x08:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "jr S");
    case 0x09:
      if(INS_D==31)      return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "jalr S");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "jalr D,S");
    case 0x0C:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "syscall");
    case 0x10:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "mfhi D");
    case 0x11:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "mthi S");
    case 0x12:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "mflo D");
    case 0x13:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "mtlo S");
    case 0x18:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "mult S,T");
    case 0x19:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "multu S,T");
    case 0x1A:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "div S,T");
    case 0x1B:           return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "divu S,T");
    case 0x20:
      if(INS_D==INS_S)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "add D,T");
      if(!INS_T)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,S");
      if(!INS_S)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "add D,S,T");
    case 0x21:
      if(INS_D==INS_S)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "addu D,T");
      if(!INS_T)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,S");
      if(!INS_S)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "addu D,S,T");
    case 0x22:
      if(INS_D==INS_S)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sub D,T");
      if(!INS_T)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,S");
      if(!INS_S) {
        if(INS_D==INS_T) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "neg D");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "neg D,T");
      }
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sub D,S,T");
    case 0x23:
      if(INS_D==INS_S)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "subu D,T");
      if(!INS_T)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,S");
      if(!INS_S) {
        if(INS_D==INS_T) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "negu D");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "negu D,T");
      }
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "subu D,S,T");
    case 0x24:
      if(INS_D==INS_S)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "and D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "and D,S,T");
    case 0x25:
      if(INS_D==INS_S)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "or D,T");
      if(!INS_T)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,S");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "or D,S,T");
    case 0x26:
      if(INS_D==INS_S)   return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "xor D,T");
      if(!INS_T)         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move D,S");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "xor D,S,T");
    case 0x27:
      if(!INS_T) {
        if(INS_D==INS_S) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "not D");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "not D,S");
      } else if(!INS_S) {
        if(INS_D==INS_T) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "not D");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "not D,T");
      } else {
        if(INS_D==INS_S) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "nor D,T");
                         return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "nor D,S,T");
      }
    case 0x2A: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "slt D,S,T");
    case 0x2B: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sltu D,S,T");
    default: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "?");
    }
  } else {
    switch(dwInstruction >> 26) {
    case 0x01:
      switch(INS_T) {
      case 0x00: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "bltz S,B");
      case 0x01: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "bgez S,B");
      case 0x10: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "bltzal S,B");
      case 0x11: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "bgezal S,B");
      default: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "?");
      }
      break;
    case 0x02: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "j J");
    case 0x03: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "jal J");
    case 0x04:
      if(INS_S==INS_T) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "b B");
                       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "beq S,T,B");
    case 0x05: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "bne S,T,B");
    case 0x06:
      if(!INS_S)       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "b B");
                       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "blez S,B");
    case 0x07: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "bgtz S,B");
    case 0x08:
      if(INS_T==INS_S) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "addi T,I");
      if(!INS_S)       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "li T,I");
      if(!INS_I)       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move T,S");
                       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "addi T,S,I");
    case 0x09:
      if(INS_T==INS_S) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "addiu T,I");
      if(!INS_S)       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "li T,I");
      if(!INS_I)       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "move T,S");
                       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "addiu T,S,I");
    case 0x0A: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "slti T,S,I");
    case 0x0B: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sltiu T,S,I");
    case 0x0C:
      if(INS_T==INS_S) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "andi T,I");
                       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "andi T,S,I");
    case 0x0D:
      if(INS_T==INS_S) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "ori T,I");
                       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "ori T,S,I");
    case 0x0E:
      if(INS_T==INS_S) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "xori T,I");
                       return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "xori T,S,I");
    case 0x0F: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lui T,I");
    case 0x10:
      switch(INS_S) {
      case 0x00: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "mfc0 T,CD");
      case 0x04: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "mtc0 T,CD");
      case 0x10: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "rfe");
      default: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "?");
      }
      break;
    case 0x20:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lb T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lb T,I(S)");
    case 0x21:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lh T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lh T,I(S)");
    case 0x22:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lwl T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lwl T,I(S)");
    case 0x23:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lw T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lw T,I(S)");
    case 0x24:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lbu T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lbu T,I(S)");
    case 0x25:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lhu T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lhu T,I(S)");
    case 0x26:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lwr T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "lwr T,I(S)");
    case 0x28:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sb T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sb T,I(S)");
    case 0x29:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sh T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sh T,I(S)");
    case 0x2A:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "swl T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "swl T,I(S)");
    case 0x2B:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sw T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "sw T,I(S)");
    case 0x2E:
      if(!INS_I) return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "swr T,(S)");
                 return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "swr T,I(S)");
    default: return DH(lpszDest, lpcrFG, dwPC, dwInstruction, "?");
    }
  }
}

/***************************************************************************/

void CCodeView::OnEditAssemble() 
{
  /*
  ** This loops repeatedly until the dialog is canceled
  */
  for(;;) {
    CPoint ptAnchor, ptCursor;
    GetSelection(ptAnchor, ptCursor);
    DWORD dwPC = LineIndexToPC(ptCursor.y);
    CAssembleDlg dlgAssemble;
    /*
    ** Set the read-only address field
    */
    dlgAssemble.m_strAddress.Format(_T("0x%08X"), dwPC);
    /*
    ** As sample text, we will disassemble the instruction at this line
    */
    TCHAR szInstructionTemp[100];
    COLORREF acrFGTemp[100];
    Disassemble(szInstructionTemp, acrFGTemp, dwPC, GetWorkArea()->PeekWord(dwPC));
    /*
    ** Eat spaces from the disassembly string
    */
    TCHAR *t1 = szInstructionTemp;
    TCHAR *t2 = szInstructionTemp;
    BOOL canspace = FALSE;
    for(;;) {
      TCHAR c = *t2++;
      if(!c) break;
      if(isspace(c)) {
        if(!canspace) continue;
        canspace = FALSE;
      } else {
        canspace = TRUE;
      }
      *t1++ = c;
    }
    *t1++ = 0;
    dlgAssemble.m_strInstruction = (LPCTSTR)szInstructionTemp;
    DWORD dwIns = 0;
    /*
    ** Keep trying until we get a valid assembled instruction word
    */
    for(;;) {
      if(dlgAssemble.DoModal() != IDOK) return;
      CMIPSQuickAssembler qa;
      CString complaint = qa.Assemble(dwPC, dlgAssemble.m_strInstruction, dwIns);
      if(!complaint.IsEmpty()) {
        CString s;
        s.Format(_T("'%s' is not a valid instruction.\n\n%s"), (LPCTSTR)dlgAssemble.m_strInstruction, (LPCTSTR)complaint);
        MessageBox((LPCTSTR)s, _T("Assemble"), MB_OK|MB_ICONEXCLAMATION);
        continue;
      }
      break;
    }
    /*
    ** Perform the actual patch
    */
    GetWorkArea()->SetPatchWord(dwPC, dwIns);
    /*
    ** Advance to next line - intuitive behavior for the enter key
    */
    ptCursor.y++;
    ptCursor.x = 0;
    SetSelection(ptCursor, ptCursor);
    /*
    ** Have to do this in case the memory view changes, etc.
    */
    GetDocument()->UpdateAllViews(NULL);
    /*
    ** Do it all over again! (until canceled)
    */
  }
}

DWORD CCodeView::LineIndexToPC(int nLineIndex)
{
  if(nLineIndex < 0 || nLineIndex >= 0xA0000) return 0;
  if(nLineIndex < 0x80000) {
    return 0x80000000 + 4 * nLineIndex;
  } else {
    return 0xBFC00000 + 4 * (nLineIndex - 0x80000);
  }
}

BOOL CCodeView::IsAddressValid(DWORD dwAddress)
{
  dwAddress &= 0x1FFFFFFC;
  if(dwAddress < 0x200000) return TRUE;
  // _include_ the word after the end
  if(dwAddress >= 0x1FC00000 && dwAddress <= 0x1FE00000) return TRUE;
  return FALSE;
}

int CCodeView::PCToLineIndex(DWORD dwPC)
{
  dwPC &= 0x1FFFFFFC;
  if(dwPC <  0x1F000000) return ((dwPC & 0x1FFFFC) / 4);
  if(dwPC >= 0x1FC00000) return ((dwPC &  0x7FFFC) / 4) + 0x80000;
  return 0;
}

BOOL CCodeView::IsCompleteHex8(LPCTSTR lpszString)
{
  if(lpszString[0] != _T('0')) return FALSE;
  if(lpszString[1] != _T('x') && lpszString[1] != _T('X')) return FALSE;
  int i;
  for(i = 0; i < 8; i++) {
    TCHAR t = lpszString[2 + i];
    if(!t) return FALSE;
    if(!isxdigit(t)) return FALSE;
  }
  return TRUE;
}

CString CCodeView::FindCompleteHex8AtCursor()
{
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor != ptCursor) return _T("");
  CString line;
  CDWordArray craFG, craBG;
  line = GetLineText(craFG, craBG, ptCursor.y);
  return FindCompleteHex8InString(line, ptCursor.x);
}

LPCTSTR CCodeView::FindCompleteHex8InString(LPCTSTR lpszString, int nIndex)
{
  int nStrLen = lstrlen(lpszString);
  if(nIndex > nStrLen) return NULL;
  if(nIndex > 0) {
    // work backwards until the first non-x or xdigit
    while((--nIndex) > 0) {
      TCHAR t = lpszString[nIndex];
      if(!(isxdigit(t) || t == _T('x') || t == _T('X'))) { nIndex++; break; }
    }
  }
  if(nIndex >= nStrLen) return NULL;
  if(!IsCompleteHex8(lpszString+nIndex)) return NULL;
  return lpszString+nIndex;
}

COLORREF CCodeView::GetBlankColor()
{
  return theApp.GetColor(PSFLAB_COLOR_CODE_BG);
}

void CCodeView::NavPush()
{
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  iaNavStack.Add(ptCursor.x);
  iaNavStack.Add(ptCursor.y);
}

CPoint CCodeView::NavPop()
{
  CPoint pt;
  pt.x = 0;
  pt.y = 0;
  int nSize = iaNavStack.GetSize();
  if(nSize < 2) return pt;
  pt.x = iaNavStack[nSize - 2];
  pt.y = iaNavStack[nSize - 1];
  iaNavStack.SetSize(nSize - 2);
  return pt;
}

int CCodeView::NavStackSize()
{
  return iaNavStack.GetSize() / 2;
}

void CCodeView::OnEditReturn() 
{
	CPoint pt;
  if(!NavStackSize()) return;
  pt = NavPop();
  SetSelection(pt, pt, 25);
}

void CCodeView::OnEditJump() 
{
  CString strHex8 = FindCompleteHex8AtCursor();
  if(!strHex8.GetLength()) return;
  DWORD dwValue;
  if(!ParseNumber(strHex8, dwValue)) return;
  NavJump(dwValue);
}

void CCodeView::NavStackEmpty()
{
  iaNavStack.SetSize(0);
}

void CCodeView::OnEditEmpty() 
{
	NavStackEmpty();
}

void CCodeView::NavJump(DWORD dwAddress)
{
  if(!IsAddressValid(dwAddress)) return;
  NavPush();
  JumpTo(dwAddress);
}

DWORD CCodeView::GetCursorPC()
{
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  return LineIndexToPC(ptCursor.y);
}

void CCodeView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
  OnEditJump();
	CFancyTextView::OnLButtonDblClk(nFlags, point);
}

void CCodeView::OnEditPatch() 
{
  CPatchDlg dlgPatch;
  dlgPatch.m_dwAddress = GetCursorPC();
  dlgPatch.m_dwValue = GetWorkArea()->PeekWord(dlgPatch.m_dwAddress);
  if(dlgPatch.DoModal() != IDOK) return;
  GetWorkArea()->SetPatchWord(dlgPatch.m_dwAddress, dlgPatch.m_dwValue);
  GetDocument()->UpdateAllViews(NULL);
}

void CCodeView::OnEditBreakpoint() 
{
  DWORD dwPC = GetCursorPC();
  BOOL bBreak = GetWorkArea()->IsBreakpointOnExecute(dwPC);
  GetWorkArea()->SetBreakpointOnExecute(dwPC, !bBreak);
  GetDocument()->UpdateAllViews(NULL);
}

void CCodeView::OnEditDeletepatch() 
{
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  int nLine0, nLine1;
  if(ptAnchor.y < ptCursor.y) {
    nLine0 = ptAnchor.y;
    nLine1 = ptCursor.y;
  } else if(ptAnchor.y == ptCursor.y) {
    nLine0 = ptCursor.y;
    nLine1 = ptCursor.y + 1;
  } else {
    nLine0 = ptCursor.y;
    nLine1 = ptAnchor.y;
  }
  int nLine;
  for(nLine = nLine0; nLine < nLine1; nLine++) {
    if(nLine >= (0x10000/4) && nLine < (0x200000/4)) {
      DWORD dwAddress = LineIndexToPC(nLine);
      if(GetWorkArea()->IsWordPatched(dwAddress)) {
        GetWorkArea()->DeletePatchWord(dwAddress);
      }
    }
  }
  GetDocument()->UpdateAllViews(NULL);
}

void CCodeView::OnEditFill() 
{
  CFillDlg dlgFill;
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y <= ptCursor.y) {
    dlgFill.m_dwStartAddress = LineIndexToPC(ptAnchor.y);
    dlgFill.m_dwEndAddress   = LineIndexToPC(ptCursor.y);
  } else {
    dlgFill.m_dwStartAddress = LineIndexToPC(ptCursor.y);
    dlgFill.m_dwEndAddress   = LineIndexToPC(ptAnchor.y);
  }
  dlgFill.m_dwValue = 0;
  if(dlgFill.DoModal() != IDOK) return;
  DWORD d;
  dlgFill.m_dwStartAddress &= 0x1FFFFFFC;
  dlgFill.m_dwEndAddress   &= 0x1FFFFFFC;
  for(d = dlgFill.m_dwStartAddress; d < dlgFill.m_dwEndAddress; d++) {
    GetWorkArea()->SetPatchWord(d, dlgFill.m_dwValue);
  }
  GetDocument()->UpdateAllViews(NULL);
}

CPSFWorkArea* CCodeView::GetWorkArea()
{
  CDocument *pDoc = GetDocument();
  ASSERT(pDoc);
  ASSERT(pDoc->IsKindOf(RUNTIME_CLASS(CPSFWorkArea)));
  return (CPSFWorkArea*)pDoc;
}

void CCodeView::JumpTo(DWORD dwAddress)
{
  CPoint pt;
  pt.x = 0;
  pt.y = PCToLineIndex(dwAddress);
  SetSelection(pt, pt, 25);
}

void CCodeView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
  CMenu Menu;
  if(Menu.LoadMenu(IDR_CONTEXT_CODE)) {
    CMenu* pPopup = Menu.GetSubMenu(0);
    my_init_popup(this, pPopup, TRUE);
    pPopup->SetDefaultItem(ID_EDIT_JUMP);
    pPopup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,this);
  }
}

void CCodeView::OnEditCurrentline() 
{
  NavJump(GetWorkArea()->GetRegister(PSFWORKAREA_REG_CPU_PC));
}

void CCodeView::OnUpdateDebugRuntocursor(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(!GetWorkArea()->IsRunning());
}

void CCodeView::OnDebugRuntocursor() 
{
  GetWorkArea()->RunToAddress(GetCursorPC());
}
