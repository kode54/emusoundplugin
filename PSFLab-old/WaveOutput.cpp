// WaveOutput.cpp: implementation of the CWaveOutput class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "psflab.h"
#include "WaveOutput.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWaveOutput::CWaveOutput()
{
  m_bPlaying = FALSE;
  m_bPaused  = FALSE;
  m_dwWaveOutProcEnabled = 0;
}

CWaveOutput::~CWaveOutput()
{
  Close();
}

//////////////////////////////////////////////////////////////////////

void CALLBACK CWaveOutput__WaveOutProc(HWAVEOUT hWaveOut, UINT nMsg, DWORD dwInstance, DWORD dwParam1, DWORD dwParam2) {
  CWaveOutput *me = (CWaveOutput*)dwInstance;
  ASSERT(me);
  if(!(me->m_dwWaveOutProcEnabled)) return;
  if(nMsg != WOM_DONE) return;
  WAVEHDR *pWaveHeader = (WAVEHDR*)(dwParam1);
  ASSERT(pWaveHeader);
  ::PostThreadMessage(me->m_dwFillerThreadId, WM_APP, (WPARAM)pWaveHeader, (LPARAM)(pWaveHeader->lpData));
}

BOOL CWaveOutput::Open(
  LPVOID lpBuffer,
  int nBuffers,
  int nSamplesPerBuffer,
  int nSampleRate
)
{
  if(m_bPlaying) Close();

  m_dwWaveOutProcEnabled = 0;
  m_dwFillerThreadId = ::GetCurrentThreadId();
  m_dwWaveOutProcEnabled = 1;

  WAVEFORMATEX WaveFormat = {
    WAVE_FORMAT_PCM,
    2,
    nSampleRate,
    nSampleRate*4,
    4,
    16,
    0
  };

	DWORD	err;

  m_nBuffers = nBuffers;

	/* Open the default WAVE Out Device, specifying my callback */
  err = waveOutOpen(
    &m_hWaveOut,
    WAVE_MAPPER,
    &WaveFormat,
    (DWORD)CWaveOutput__WaveOutProc,
    (DWORD)this,
    CALLBACK_FUNCTION
  );

  if(err) {
    TRACE(_T("Unable to open wave output device\n"));
    return FALSE;
  }

  m_pWaveHeader = new WAVEHDR[nBuffers];
  ASSERT(m_pWaveHeader);
  memset(m_pWaveHeader, 0, sizeof(WAVEHDR) * nBuffers);
  /*
  ** Prepare headers and post prefill messages
  */
	for(int i = 0; i < nBuffers; i++) {
	  m_pWaveHeader[i].lpData = ((char*)(lpBuffer)) + i * 4 * nSamplesPerBuffer;
	  m_pWaveHeader[i].dwBufferLength = 4 * nSamplesPerBuffer;
    waveOutPrepareHeader(
      m_hWaveOut,
      m_pWaveHeader + i,
      sizeof(WAVEHDR)
    );
    ::PostThreadMessage(
      m_dwFillerThreadId, WM_APP,
      (WPARAM)(m_pWaveHeader + i),
      (LPARAM)(m_pWaveHeader[i].lpData)
    );
	}

  m_bPlaying = TRUE;
  m_bPaused = FALSE;
  return TRUE;
}

void CWaveOutput::BufferReady(WPARAM wBufferId)
{
  waveOutWrite(m_hWaveOut, (LPWAVEHDR)wBufferId, sizeof(WAVEHDR));
}

void CWaveOutput::Close()
{
  if(!m_bPlaying) return;
  m_dwWaveOutProcEnabled = 0;
  waveOutReset(m_hWaveOut);
  ASSERT(m_pWaveHeader);
	for(int i = 0; i < m_nBuffers; i++) {
	  waveOutUnprepareHeader(m_hWaveOut, m_pWaveHeader + i, sizeof(WAVEHDR));
	}
	waveOutClose(m_hWaveOut);
  delete[] m_pWaveHeader;
  m_bPlaying = FALSE;
  m_bPaused = FALSE;
}

void CWaveOutput::Pause(BOOL bPause)
{
  if(!m_bPlaying) return;
  if(bPause && !m_bPaused) {
    waveOutPause(m_hWaveOut);
  } else if(!bPause && m_bPaused) {
    waveOutRestart(m_hWaveOut);
  }
  m_bPaused = bPause;
}
