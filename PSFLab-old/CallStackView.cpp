// CallStackView.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "CallStackView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCallStackView

IMPLEMENT_DYNCREATE(CCallStackView, CFancyTextView)

CCallStackView::CCallStackView()
{
}

CCallStackView::~CCallStackView()
{
}


BEGIN_MESSAGE_MAP(CCallStackView, CFancyTextView)
	//{{AFX_MSG_MAP(CCallStackView)
	ON_WM_CONTEXTMENU()
	ON_UPDATE_COMMAND_UI(ID_EDIT_JUMP, OnUpdateEditJump)
	ON_COMMAND(ID_EDIT_JUMP, OnEditJump)
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCallStackView drawing

/////////////////////////////////////////////////////////////////////////////
// CCallStackView diagnostics

#ifdef _DEBUG
void CCallStackView::AssertValid() const
{
	CFancyTextView::AssertValid();
}

void CCallStackView::Dump(CDumpContext& dc) const
{
	CFancyTextView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCallStackView message handlers

int CCallStackView::GetLineCount()
{
  return GetWorkArea()->GetCallStack()->GetSize();
}

CPSFWorkArea* CCallStackView::GetWorkArea()
{
  CDocument *pDoc = GetDocument();
  ASSERT(pDoc);
  ASSERT(pDoc->IsKindOf(RUNTIME_CLASS(CPSFWorkArea)));
  return (CPSFWorkArea*)pDoc;
}

COLORREF CCallStackView::GetBlankColor()
{
  return theApp.GetColor(PSFLAB_COLOR_CALL_BG);
}

COLORREF CCallStackView::GetDefaultFGColor()
{
  return theApp.GetColor(PSFLAB_COLOR_CALL_FG);
}

CString CCallStackView::GetLineText(CDWordArray &craFG, CDWordArray &craBG, int nLineIndex)
{
  SCallStackEntry e;
  GetWorkArea()->GetCallStack()->GetEntry(nLineIndex, e);
  TCHAR s[100];
  if(e.bException) {
    wsprintf(s, _T("Exception: %s (epc=0x%08X,sp=0x%08X)"),
      GetExceptionName(e.dwCause),
      e.dwReturnPC,
      e.dwReturnSP
    );
  } else {
    wsprintf(s, _T("Call: 0x%08X(0x%08X,0x%08X,0x%08X,0x%08X) (ra=0x%08X,sp=0x%08X)"), 
      e.dwInvokedAddress,
      e.dwArg[0],
      e.dwArg[1],
      e.dwArg[2],
      e.dwArg[3],
      e.dwReturnPC,
      e.dwReturnSP
    );
  }
  Colorize(s, craFG, craBG);
  return (LPCTSTR)s;
}

void CCallStackView::Colorize(LPTSTR s, CDWordArray &craFG, CDWordArray &craBG)
{
  int l = lstrlen(s);
  int i;
  craFG.SetSize(l);
  craBG.SetSize(l);
  COLORREF crBG          = GetBlankColor();
  COLORREF crFGNormal    = GetDefaultFGColor();
  COLORREF crFGAddress   = theApp.GetColor(PSFLAB_COLOR_CALL_FG_ADDRESS);
  COLORREF crFGException = theApp.GetColor(PSFLAB_COLOR_CALL_FG_EXCEPTION);
  COLORREF crFGCause     = theApp.GetColor(PSFLAB_COLOR_CALL_FG_CAUSE);

  COLORREF crFG = crFGNormal;
  for(i = 0; i < l; i++) {
    craBG[i] = crBG;
    craFG[i] = crFGNormal;
  }
  if(s[0] == _T('E')) {
    for(i = 0; s[i] && i < 9; i++) craFG[i] = crFGException;
    for(i = 10; s[i] && (s[i] != _T('(')); i++) craFG[i] = crFGCause;
  } else if(s[0] == _T('C')) {
    for(i = 6; s[i] && i < 16; i++) craFG[i] = crFGAddress;
  }
}

LPCTSTR CCallStackView::GetExceptionName(DWORD dwCause) const
{
  switch((dwCause >> 2) & 0xF) {
  case 0x0: return _T("Interrupt");
  case 0x1: return _T("TLB protection fault");
  case 0x2: return _T("TLB miss (load)");
  case 0x3: return _T("TLB miss (store)");
  case 0x4: return _T("Address error (load)");
  case 0x5: return _T("Address error (store)");
  case 0x6: return _T("Bus error (instruction)");
  case 0x7: return _T("Bus error (data)");
  case 0x8: return _T("Syscall");
  case 0x9: return _T("Break");
  case 0xA: return _T("Illegal instruction");
  case 0xB: return _T("Coprocessor unusable");
  case 0xC: return _T("Overflow");
  case 0xD: return _T("0xD");
  case 0xE: return _T("0xE");
  case 0xF: return _T("0xF");
  }
  return _T("");
}

int CCallStackView::GetMaxLineCharCount()
{
  return 100;
}

void CCallStackView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
  CMenu Menu;
  if(Menu.LoadMenu(IDR_CONTEXT_CALLSTACK)) {
    CMenu* pPopup = Menu.GetSubMenu(0);
    my_init_popup(this, pPopup, TRUE);
    pPopup->SetDefaultItem(ID_EDIT_JUMP);
    pPopup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,this);
  }
}

void CCallStackView::OnUpdateEditJump(CCmdUI* pCmdUI) 
{
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y != ptCursor.y) { pCmdUI->Enable(FALSE); return; }
  if(ptCursor.y < 0 || ptCursor.y >= GetLineCount()) { pCmdUI->Enable(FALSE); return; }
  pCmdUI->Enable(TRUE);
}

void CCallStackView::OnEditJump() 
{
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y != ptCursor.y) return;
  if(ptCursor.y < 0 || ptCursor.y >= GetLineCount()) return;

  CPSFWorkArea *wa = GetWorkArea();

  /*
  ** If this is the top entry, jump to the current program counter
  */
  if(!ptCursor.y) {
    wa->CodeJumpTo(wa->GetPC());
  /*
  ** Otherwise, go one entry up and jump to its return address
  */
  } else {
    CCallStack *pStack = wa->GetCallStack();
    SCallStackEntry e;
    pStack->GetEntry(ptCursor.y - 1, e);
    DWORD dwAddress = e.dwReturnPC;
    if(!e.bException) dwAddress -= 8;
    wa->CodeJumpTo(dwAddress);
  }
}

void CCallStackView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
  OnEditJump();
	CFancyTextView::OnLButtonDblClk(nFlags, point);
}
