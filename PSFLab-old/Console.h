// Console.h: interface for the CConsole class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONSOLE_H__454B5D5C_1BE4_4D57_9901_5A693116A167__INCLUDED_)
#define AFX_CONSOLE_H__454B5D5C_1BE4_4D57_9901_5A693116A167__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CConsole  
{
public:
	CString GetLine(int nLine);
	int GetLineCount();
	void AddChar(TCHAR c);
	void Clear();
	CConsole();
	virtual ~CConsole();

private:
	CCriticalSection cs;
	CStringArray saData;
};

#endif // !defined(AFX_CONSOLE_H__454B5D5C_1BE4_4D57_9901_5A693116A167__INCLUDED_)
