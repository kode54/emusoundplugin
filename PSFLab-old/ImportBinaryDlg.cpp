// ImportBinaryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "ImportBinaryDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImportBinaryDlg dialog


CImportBinaryDlg::CImportBinaryDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImportBinaryDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CImportBinaryDlg)
	//}}AFX_DATA_INIT
}


void CImportBinaryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImportBinaryDlg)
	DDX_Control(pDX, IDC_OFFSET, m_cOffset);
	DDX_Control(pDX, IDC_LENGTH, m_cLength);
	DDX_Control(pDX, IDC_BEGINNING, m_cBeginning);
	DDX_Control(pDX, IDC_END, m_cEnd);
	DDX_Control(pDX, IDC_OFFSETEDIT, m_cOffsetEdit);
	DDX_Control(pDX, IDC_LENGTHEDIT, m_cLengthEdit);
	DDX_Control(pDX, IDC_DESTINATION, m_cDestination);
	DDX_Control(pDX, IDC_FILE, m_cFile);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CImportBinaryDlg, CDialog)
	//{{AFX_MSG_MAP(CImportBinaryDlg)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
	ON_BN_CLICKED(IDC_BEGINNING, OnBeginning)
	ON_BN_CLICKED(IDC_OFFSET, OnOffset)
	ON_BN_CLICKED(IDC_END, OnEnd)
	ON_BN_CLICKED(IDC_LENGTH, OnLength)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImportBinaryDlg message handlers

void CImportBinaryDlg::OnBrowse() 
{
  CString strAllFilter;
  strAllFilter.LoadString(IDS_FILTER_ALL);
  CString strFilterString = strAllFilter + _T("||");
  CFileDialog dlgFile(
    TRUE,
    _T(""),
    _T(""),
    OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
    (LPCTSTR)strFilterString
  );
  CString strTitle;
  strTitle.LoadString(IDS_IMPORT_BINARY);
  dlgFile.m_ofn.lpstrTitle = (LPCTSTR)strTitle;
  if(dlgFile.DoModal() != IDOK) return;
  CString strPathName = dlgFile.GetPathName();
  m_cFile.SetWindowText((LPCTSTR)strPathName);
}

void CImportBinaryDlg::OnOK() 
{
  CString s;
  m_cDestination.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwDestinationAddress, this, IDC_DESTINATION, _T("destination address"))) return;

  m_cFile.GetWindowText(m_strPathName);

  if(m_cOffset.GetCheck() == BST_CHECKED) {
    m_cOffsetEdit.GetWindowText(s);
    if(!ParseDlgValue(s, m_dwOffset, this, IDC_OFFSETEDIT, _T("offset"))) return;
  } else {
    m_dwOffset = 0;
  }

  if(m_cLength.GetCheck() == BST_CHECKED) {
    m_cLengthEdit.GetWindowText(s);
    if(!ParseDlgValue(s, m_dwLength, this, IDC_LENGTHEDIT, _T("length"))) return;
  } else {
    FILE *f = NULL;
    if(!m_strPathName.IsEmpty()) {
      f = fopen((LPCSTR)m_strPathName, "rb");
    }
    if(!f) {
      CString s;
      s.Format(_T("Unable to open '%s'.\n\nPlease enter a valid filename."), (LPCTSTR)m_strPathName);
      MessageBox((LPCTSTR)s, _T("Import Binary Data"), MB_OK|MB_ICONHAND);
      return;
    }
    fseek(f, 0, SEEK_END);
    m_dwLength = ftell(f);
    if(m_dwLength >= m_dwOffset) {
      m_dwLength -= m_dwOffset;
    } else {
      m_dwLength = 0;
    }
    fclose(f);
  }

	CDialog::OnOK();
}

BOOL CImportBinaryDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  OnBeginning();
  OnEnd();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CImportBinaryDlg::OnBeginning() 
{
  m_cBeginning.SetCheck(BST_CHECKED);
  m_cOffset.SetCheck(BST_UNCHECKED);
  m_cOffsetEdit.EnableWindow(FALSE);
}

void CImportBinaryDlg::OnOffset() 
{
  m_cOffset.SetCheck(BST_CHECKED);
	m_cBeginning.SetCheck(BST_UNCHECKED);
  m_cOffsetEdit.EnableWindow(TRUE);
  m_cOffsetEdit.SetFocus();
}

void CImportBinaryDlg::OnEnd() 
{
  m_cEnd.SetCheck(BST_CHECKED);
  m_cLength.SetCheck(BST_UNCHECKED);
  m_cLengthEdit.EnableWindow(FALSE);
}

void CImportBinaryDlg::OnLength() 
{
  m_cLength.SetCheck(BST_CHECKED);
	m_cEnd.SetCheck(BST_UNCHECKED);
  m_cLengthEdit.EnableWindow(TRUE);
  m_cLengthEdit.SetFocus();
}
