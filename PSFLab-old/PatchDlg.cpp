// PatchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "PatchDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPatchDlg dialog


CPatchDlg::CPatchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPatchDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPatchDlg)
	//}}AFX_DATA_INIT
}


void CPatchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPatchDlg)
	DDX_Control(pDX, IDC_VALUE, m_cValue);
	DDX_Control(pDX, IDC_ADDRESS, m_cAddress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPatchDlg, CDialog)
	//{{AFX_MSG_MAP(CPatchDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPatchDlg message handlers

void CPatchDlg::OnOK() 
{
  CString s;
  m_cAddress.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwAddress, this, IDC_ADDRESS, _T("address"))) return;
  m_cValue.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwValue, this, IDC_VALUE, _T("value"))) return;
	CDialog::OnOK();
}

BOOL CPatchDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString s;
  s.Format(_T("0x%08X"), m_dwAddress);
  m_cAddress.SetWindowText(s);
  s.Format(_T("0x%08X"), m_dwValue);
  m_cValue.SetWindowText(s);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
