#if !defined(AFX_EMUSETTINGSDLG_H__054460B8_380A_4428_BBBF_FE642DE17F75__INCLUDED_)
#define AFX_EMUSETTINGSDLG_H__054460B8_380A_4428_BBBF_FE642DE17F75__INCLUDED_

#include "PSFLab.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EmuSettingsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEmuSettingsDlg dialog

class CEmuSettingsDlg : public CDialog
{
// Construction
public:
	int m_nWhichSettingToStart;
	CEmuSettingsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEmuSettingsDlg)
	enum { IDD = IDD_EMUSETTINGS };
	CListBox	m_cSettingList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEmuSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEmuSettingsDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeSettingList();
	afx_msg void OnHarsh();
	afx_msg void OnFriendly();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void SetSettingType(int nWhich);
	int GetSettingType();
	void UpdateInterface();
	SPSFEmuSettings m_es[PSF_EMUSETTINGS_MAX];
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EMUSETTINGSDLG_H__054460B8_380A_4428_BBBF_FE642DE17F75__INCLUDED_)
