#if !defined(AFX_GOTODLG_H__10DDA8C6_75F9_4C6B_A68B_0F836E9C35A7__INCLUDED_)
#define AFX_GOTODLG_H__10DDA8C6_75F9_4C6B_A68B_0F836E9C35A7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GoToDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGoToDlg dialog

class CGoToDlg : public CDialog
{
// Construction
public:
	DWORD m_dwAddress;
	CGoToDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGoToDlg)
	enum { IDD = IDD_GOTO };
	CEdit	m_cAddress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGoToDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGoToDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GOTODLG_H__10DDA8C6_75F9_4C6B_A68B_0F836E9C35A7__INCLUDED_)
