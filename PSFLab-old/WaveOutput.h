// WaveOutput.h: interface for the CWaveOutput class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WAVEOUTPUT_H__07829C13_5B82_49F6_B7FC_68D613115B8A__INCLUDED_)
#define AFX_WAVEOUTPUT_H__07829C13_5B82_49F6_B7FC_68D613115B8A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Mmsystem.h>

class CWaveOutput  
{
public:
	void Pause(BOOL bPause = TRUE);
	CWaveOutput();
	virtual ~CWaveOutput();

	BOOL Open(LPVOID lpBuffer, int nBuffers, int nSamplesPerBuffer, int nSampleRate);
	void Close();
	void BufferReady(WPARAM wBufferId);

private:
	BOOL m_bPaused;
	DWORD m_dwWaveOutProcEnabled;
	DWORD m_dwFillerThreadId;
	BOOL m_bPlaying;
	int m_nBuffers;
	WAVEHDR *m_pWaveHeader;
  friend void CALLBACK CWaveOutput__WaveOutProc(HWAVEOUT hWaveOut, UINT nMsg, DWORD dwInstance, DWORD dwParam1, DWORD dwParam2);
	HWAVEOUT m_hWaveOut;
};

#endif // !defined(AFX_WAVEOUTPUT_H__07829C13_5B82_49F6_B7FC_68D613115B8A__INCLUDED_)
