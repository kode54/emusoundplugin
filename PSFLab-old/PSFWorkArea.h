#if !defined(AFX_PSFWORKAREA_H__065A49CF_B783_40F0_8B75_AB291D22DCEA__INCLUDED_)
#define AFX_PSFWORKAREA_H__065A49CF_B783_40F0_8B75_AB291D22DCEA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PSFWorkArea.h : header file
//

#include "EventLog.h"
#include "CallStack.h"	// Added by ClassView
#include "Console.h"	// Added by ClassView

#define PSFWORKAREA_REG_CPU_GEN (0)
#define PSFWORKAREA_REG_CPU_C0  (32)
#define PSFWORKAREA_REG_CPU_PC  (64)
#define PSFWORKAREA_REG_CPU_HI  (65)
#define PSFWORKAREA_REG_CPU_LO  (66)
#define PSFWORKAREA_REG_CPU_CI  (67)
#define PSFWORKAREA_REG_N       (68)

#define PSFWORKAREA_REGISTER_CHANGE_TIMEOUT (1)

/////////////////////////////////////////////////////////////////////////////
// CPSFWorkArea document

class CPSFWorkArea : public CDocument
{
protected:
	CPSFWorkArea();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPSFWorkArea)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPSFWorkArea)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual void OnCloseDocument();
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
//	TCHAR m_sPSF2Path[MAX_PATH];
	int GetVersion();
	void SetVersion(int n);
	int m_nVersion;
	void MemoryJumpTo(DWORD dwAddress);
	void CodeJumpTo(DWORD dwAddress);
	void RunToAddress(DWORD dwAddress);
	CCallStack* GetCallStack();
	DWORD GetRegister(int nRegisterIndex);

	CConsole console;

	CEventLog* GetEventLog();
	void SetBreakpointOnExecute(DWORD dwAddress, BOOL bBreak);
	BOOL  IsBreakpointOnExecute(DWORD dwAddress) const;
	void SetBreakpointOnRead   (DWORD dwAddress, BOOL bBreak);
	BOOL  IsBreakpointOnRead   (DWORD dwAddress) const;
	void SetBreakpointOnWrite  (DWORD dwAddress, BOOL bBreak);
	BOOL  IsBreakpointOnWrite  (DWORD dwAddress) const;
	void DeletePatchWord(DWORD dwAddress);
	DWORD GetPC() const;
	void SetPatchWord(DWORD dwAddress, DWORD dwValue);
	DWORD GetPatchWord(DWORD dwAddress) const;
	BOOL IsWordPatched(DWORD dwAddress) const;
	BOOL IsRunning() const { return m_bRunning; }
	BOOL IsPlaying() const { return m_bPlaying; }
  BOOL IsPlayPaused() const { return m_bPlayPaused; }
	DWORD PeekWord(DWORD dwAddress) const;
	BOOL RegisterChangedRecently(int nRegisterIndex) const;

  void AuditCommit(CPSFAudit &audit);
  void AuditUpload(CPSFAudit &audit);

	virtual ~CPSFWorkArea();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	virtual void DeleteContents();
	//{{AFX_MSG(CPSFWorkArea)
	afx_msg void OnUpdateDebugGo(CCmdUI* pCmdUI);
	afx_msg void OnDebugGo();
	afx_msg void OnUpdateDebugBreak(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDebugPlay(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDebugStop(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDebugPause(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDebugStepinto(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDebugStepout(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDebugStepover(CCmdUI* pCmdUI);
	afx_msg void OnDebugPlay();
	afx_msg void OnDebugPause();
	afx_msg void OnDebugStop();
	afx_msg void OnDebugBreak();
	afx_msg void OnUpdateDebugRestart(CCmdUI* pCmdUI);
	afx_msg void OnDebugRestart();
	afx_msg void OnDebugStepinto();
	afx_msg void OnDebugStepout();
	afx_msg void OnDebugStepover();
	afx_msg void OnUpdateDebugLoadstate(CCmdUI* pCmdUI);
	afx_msg void OnDebugLoadstate();
	afx_msg void OnUpdateDebugSavestate(CCmdUI* pCmdUI);
	afx_msg void OnDebugSavestate();
	afx_msg void OnFileImportbinary();
	afx_msg void OnUpdateFileImportbinary(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDebugRuntoint(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditBreakpoints(CCmdUI* pCmdUI);
	afx_msg void OnEditBreakpoints();
	afx_msg void OnUpdateEditExesettings(CCmdUI* pCmdUI);
	afx_msg void OnEditExesettings();
	afx_msg void OnDebugExecutionstopped();
	afx_msg void OnUpdateDebugSpecificline(CCmdUI* pCmdUI);
	afx_msg void OnDebugSpecificline();
	afx_msg void OnDebugRuntoint();
	afx_msg void OnDebugSelectslot();
	afx_msg void OnEditTag();
	afx_msg void OnFileSaveas();
	afx_msg void OnFileImportexe();
	afx_msg void OnUpdateFileImportexe(CCmdUI* pCmdUI);
	afx_msg void OnFileExportexe();
	afx_msg void OnFileFondue();
	afx_msg void OnUpdateEditOptimize(CCmdUI* pCmdUI);
	afx_msg void OnEditOptimize();
	afx_msg void OnFileExportbiosarea();
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveas(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditTag(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileExportexe(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void ExportBinaryToBuffer  (LPBYTE lpBuffer, DWORD dwAddress, DWORD dwLength);
	void ImportBinaryFromBuffer(LPBYTE lpBuffer, DWORD dwAddress, DWORD dwLength);
	BYTE m_EXEHeader[0x800];
	DWORD ExportEXEToBuffer(LPBYTE lpBuffer);
	void ImportEXEFromBuffer(LPBYTE lpBuffer, DWORD dwLength);

//	CString m_strTag;
  CPSFTag m_tag;

	DWORD BreakpointIndexToAddress(int nIndex);
	DWORD m_dwDebugEventMask;
	void LoadDebugState(int nSlot);
	void SaveDebugState(int nSlot);
	void ClearAllSavedStates();
	int m_nCurrentStateSlot;
	void* m_apSavedStates[10];
  CCallStack m_aSavedCallStacks[10];
  CEventLog m_aSavedEventLogs[10];

	DWORD m_dwDebugBreakpointExecuteAddress;
	DWORD m_dwDebugBreakpointWriteAddress;
	DWORD m_dwDebugBreakpointReadAddress;
	BOOL m_bDebugBreakpointExecuteFlag;
	BOOL m_bDebugBreakpointWriteFlag;
	BOOL m_bDebugBreakpointReadFlag;

  DWORD m_dwDebugWeedAddress;
  BOOL m_bDebugWeedFlag;
	BOOL m_bDebugBreakOnWeed;

	DWORD m_dwDebugRunTarget;
	BOOL m_bDebugErrorFlag;
	int m_nDebugCommand;

	BOOL m_bPlayPaused;

	BOOL m_bRunning;
	BOOL m_bPlaying;
  CWinThread *m_pDebugThread;
  CWinThread *m_pPlayThread;
	void StartDebugThreadAndBlock();
	void StartPlayThreadAndBlock();
	void KillDebugThreadAndBlock();
	void KillPlayThreadAndBlock();
	CCriticalSection m_csDebug;
	CCriticalSection m_csPlay;
  CEvent m_evDebug;
  CEvent m_evPlay;
  friend UINT AFX_CDECL CPSFWorkArea__DebugThread(LPVOID lpParam);
  friend UINT AFX_CDECL CPSFWorkArea__PlayThread(LPVOID lpParam);
	volatile DWORD m_dwDebugKillFlag;
	volatile DWORD m_dwPlayKillFlag;

	void PlayStart();
	void PlayStop();
	void PlayPause();
	void PlayUnpause();

	void ClearWorkArea();

	void RegisterMonitor();
	void RegisterMonitorReset();
  DWORD m_adwRegisterPrevious[PSFWORKAREA_REG_N];
  int m_anRegisterChangeTimeout[PSFWORKAREA_REG_N];

	CView* FindViewOfClass(CRuntimeClass *pClass);
	void DebugRestart();

	CCallStack m_callstack;
  CEventLog m_eventlog;

  int AddressToBreakpointIndex(DWORD dwAddress) const;
  DWORD m_adwBreakpointMapRead[0x5000];
  DWORD m_adwBreakpointMapWrite[0x5000];
  DWORD m_adwBreakpointMapExecute[0x5000];

  int AddressToEXEIndex(DWORD dwAddress) const;
  DWORD m_adwPatchMap[0x3E00];
  DWORD m_adwPatchData[0x7C000];
  DWORD m_adwEXEData[0x7C000];
  DWORD m_dwEXEStartAddress;
  DWORD m_dwEXEByteLength;
  DWORD m_dwEXEStartPC;
  DWORD m_dwEXEStartSP;

	void LoadEXEToState(void *pState);
	void ApplyPatchesToState(void *pState);

  void *m_pStateDebug; // current debug state
  void *m_pStatePlay; // currently playing state
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSFWORKAREA_H__065A49CF_B783_40F0_8B75_AB291D22DCEA__INCLUDED_)
