#if !defined(AFX_COLORSDLG_H__5ABE6D26_6718_43C9_9245_107C6D1DC2F8__INCLUDED_)
#define AFX_COLORSDLG_H__5ABE6D26_6718_43C9_9245_107C6D1DC2F8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorsDlg.h : header file
//

#include "ColorButton.h"

/////////////////////////////////////////////////////////////////////////////
// CColorsDlg dialog

class CColorsDlg : public CDialog
{
// Construction
public:
	CColorsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CColorsDlg)
	enum { IDD = IDD_COLORS };
	CStatic	m_cColorNum;
	CColorButton	m_cChange;
	CListBox	m_cColorList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CColorsDlg)
	afx_msg void OnChange();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeColorlist();
	virtual void OnCancel();
	afx_msg void OnDefaults();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CDWordArray m_dwaOldColors;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORSDLG_H__5ABE6D26_6718_43C9_9245_107C6D1DC2F8__INCLUDED_)
