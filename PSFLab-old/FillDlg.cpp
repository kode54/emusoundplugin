// FillDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "FillDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFillDlg dialog


CFillDlg::CFillDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFillDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFillDlg)
	//}}AFX_DATA_INIT

  m_dwStartAddress = 0;
  m_dwEndAddress = 0;
  m_dwValue = 0;

}


void CFillDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFillDlg)
	DDX_Control(pDX, IDC_VALUE, m_cValue);
	DDX_Control(pDX, IDC_STARTADDRESS, m_cStartAddress);
	DDX_Control(pDX, IDC_ENDADDRESS, m_cEndAddress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFillDlg, CDialog)
	//{{AFX_MSG_MAP(CFillDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFillDlg message handlers

void CFillDlg::OnOK() 
{
  CString s;
  m_cStartAddress.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwStartAddress, this, IDC_STARTADDRESS, _T("start address"))) return;
  m_cEndAddress.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwEndAddress  , this, IDC_ENDADDRESS  , _T("end address"))) return;
  m_cValue.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwValue       , this, IDC_VALUE       , _T("word value"))) return;
  CDialog::OnOK();
}

BOOL CFillDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
  CString s; 
  s.Format(_T("0x%08X"), m_dwStartAddress);
  m_cStartAddress.SetWindowText(s);
  s.Format(_T("0x%08X"), m_dwEndAddress);
  m_cEndAddress.SetWindowText(s);
  s.Format(_T("0x%08X"), m_dwValue);
  m_cValue.SetWindowText(s);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
