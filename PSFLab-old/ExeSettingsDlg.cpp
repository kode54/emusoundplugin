// ExeSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "ExeSettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExeSettingsDlg dialog


CExeSettingsDlg::CExeSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExeSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CExeSettingsDlg)
	//}}AFX_DATA_INIT
}


void CExeSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExeSettingsDlg)
	DDX_Control(pDX, IDC_TEXTSIZE, m_cTextSize);
	DDX_Control(pDX, IDC_PC, m_cPC);
	DDX_Control(pDX, IDC_SP, m_cSP);
	DDX_Control(pDX, IDC_TEXTSTART, m_cTextStart);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CExeSettingsDlg, CDialog)
	//{{AFX_MSG_MAP(CExeSettingsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExeSettingsDlg message handlers

BOOL CExeSettingsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString s;
  s.Format(_T("0x%08X"), m_dwTextStart);
  m_cTextStart.SetWindowText(s);
  s.Format(_T("0x%08X"), m_dwTextSize);
  m_cTextSize.SetWindowText(s);
  s.Format(_T("0x%08X"), m_dwPC);
  m_cPC.SetWindowText(s);
  s.Format(_T("0x%08X"), m_dwSP);
  m_cSP.SetWindowText(s);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CExeSettingsDlg::OnOK() 
{
	// TODO: Add extra validation here
  CString s;
  m_cTextStart.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwTextStart, this, IDC_TEXTSTART, _T("text start address"))) return;
  m_cTextSize.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwTextSize, this, IDC_TEXTSIZE, _T("text size"))) return;
  m_cPC.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwPC, this, IDC_PC, _T("initial program counter"))) return;
  m_cSP.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwSP, this, IDC_SP, _T("initial stack pointer"))) return;
	CDialog::OnOK();
}
