// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "PSFLab.h"

#include "MainFrm.h"

//#include "SubWindow.h"

#include "CodeView.h"
#include "RegisterView.h"
#include "MemoryView.h"
#include "CallStackView.h"
#include "EventView.h"

#include "FancyTextView.h"

#include "GoToDlg.h"

#include "SubSplitterWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
// create a view to occupy the client area of the frame
//	if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
//		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
//	{
//		TRACE0("Failed to create view window\n");
//		return -1;
//	}

//  child.Create(NULL, NULL, WS_CHILD|WS_VISIBLE, CRect(10,10,200,200), this);

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
  m_wndToolBar.SetWindowText(_T("Toolbar"));

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

/*
  if (!barRegisters.Create(this,
    RUNTIME_CLASS (CCodeView),
    (CCreateContext *)(lpCreateStruct->lpCreateParams),
    _T("testbar")))//AFX_IDW_CONTROLBAR_FIRST + 33 + i))
  {
    TRACE0("Failed to create ViewBar\n");
    return -1;      // fail to create
  }

  barRegisters.EnableDocking(CBRS_ALIGN_ANY);
  DockControlBar(&barRegisters);
*/


//CCreateContext  ctx;
//CCreateContext *pCtx = (CCreateContext *)(lpCreateStruct->lpCreateParams);
//  memset(&ctx,0,sizeof(ctx));
//  if(pCtx)memcpy(&ctx,pCtx,sizeof(ctx));
//
//  ctx.m_pNewViewClass = RUNTIME_CLASS(CCodeView);

//CSubWindow *sw = new CSubWindow;
//  sw->Create(NULL,_T("test"),WS_VISIBLE|WS_OVERLAPPEDWINDOW,CRect(100,100,300,300),this,IDR_MAINFRAME,&ctx);
//sw->Create(IDD_SUBWINDOW, this);

//sw->DoModal();

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class

  CSubSplitterWnd *ss, *ss2;

	m_wndSplitter.CreateStatic(this, 1, 2);
  m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CCodeView), CSize(theApp.m_nSplitterCodeWidth,1), pContext);
  m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CSubSplitterWnd), CSize(1,1), pContext);

  ss = (CSubSplitterWnd*)m_wndSplitter.GetPane(0, 1);
  ss->CreateStatic(1, 2);
  ss->sub.CreateView(0, 0, RUNTIME_CLASS(CSubSplitterWnd), CSize(theApp.m_nSplitterMemoryWidth, 1), pContext);
  ss->sub.CreateView(0, 1, RUNTIME_CLASS(CSubSplitterWnd), CSize(1, 1), pContext);

  ss2 = (CSubSplitterWnd*)ss->GetPane(0, 0);
  ss2->CreateStatic(2, 1);
  ss2->sub.CreateView(0, 0, RUNTIME_CLASS(CRegisterView), CSize(1, theApp.m_nSplitterRegisterHeight), pContext);
  ss2->sub.CreateView(1, 0, RUNTIME_CLASS(CMemoryView), CSize(1, 1), pContext);

  ss2 = (CSubSplitterWnd*)ss->GetPane(0, 1);
  ss2->CreateStatic(2, 1);
  ss2->sub.CreateView(0, 0, RUNTIME_CLASS(CCallStackView), CSize(1, theApp.m_nSplitterCallStackHeight), pContext);
  ss2->sub.CreateView(1, 0, RUNTIME_CLASS(CEventView), CSize(1, 1), pContext);

//	return CFrameWnd::OnCreateClient(lpcs, pContext);
  return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	cs.x  = theApp.m_rWindowSize.left;
	cs.y  = theApp.m_rWindowSize.top;
	cs.cx = theApp.m_rWindowSize.right - cs.x;
	cs.cy = theApp.m_rWindowSize.bottom - cs.y;
	return CFrameWnd::PreCreateWindow(cs);
}

void CMainFrame::OnDestroy() 
{
  RECT r;
  /*
  ** Get main window position
  */
  GetWindowRect(&r);
  theApp.m_rWindowSize.left   = r.left;
  theApp.m_rWindowSize.right  = r.right;
  theApp.m_rWindowSize.top    = r.top;
  theApp.m_rWindowSize.bottom = r.bottom;
  /*
  ** Get splitter positions
  */
  /* Code */
  m_wndSplitter.GetPane(0, 0)->GetWindowRect(&r);
  theApp.m_nSplitterCodeWidth = r.right - r.left;

  CSubSplitterWnd *ss, *ss2;

  ss  = (CSubSplitterWnd*)m_wndSplitter.GetPane(0, 1);
  ss2 = (CSubSplitterWnd*)ss->GetPane(0, 0);

  ss2->GetPane(0, 0)->GetWindowRect(&r);
  theApp.m_nSplitterMemoryWidth = r.right - r.left;
  theApp.m_nSplitterRegisterHeight = r.bottom - r.top;

  ss2 = (CSubSplitterWnd*)ss->GetPane(0, 1);

  ss2->GetPane(0, 0)->GetWindowRect(&r);
  theApp.m_nSplitterCallStackHeight = r.bottom - r.top;

	CFrameWnd::OnDestroy();
}
