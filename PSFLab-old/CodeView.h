#if !defined(AFX_CODEVIEW_H__6DCFF5B9_98BE_4A55_A0CB_C19A1C5B8970__INCLUDED_)
#define AFX_CODEVIEW_H__6DCFF5B9_98BE_4A55_A0CB_C19A1C5B8970__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CodeView.h : header file
//

#include "FancyTextView.h"
#include "PSFWorkArea.h"

/////////////////////////////////////////////////////////////////////////////
// CCodeView view

class CCodeView : public CFancyTextView
{
protected:
	CCodeView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCodeView)

// Attributes
public:

// Operations
public:
	void JumpTo(DWORD dwAddress);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCodeView)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCodeView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	virtual COLORREF GetBlankColor();
	virtual int GetMaxLineCharCount();
	virtual CString GetLineText(CDWordArray& craFG, CDWordArray& craBG, int nLineIndex);
	virtual int GetLineCount();
	//{{AFX_MSG(CCodeView)
	afx_msg void OnUpdateEditJump(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditGoto(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditReturn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditEmpty(CCmdUI* pCmdUI);
	afx_msg void OnEditGoto();
	afx_msg void OnUpdateEditBreakpoint(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditPatch(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditAssemble(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditFill(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditDeletepatch(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditSelectrange(CCmdUI* pCmdUI);
	afx_msg void OnEditSelectrange();
	afx_msg void OnEditAssemble();
	afx_msg void OnEditReturn();
	afx_msg void OnEditJump();
	afx_msg void OnEditEmpty();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnEditPatch();
	afx_msg void OnEditBreakpoint();
	afx_msg void OnEditDeletepatch();
	afx_msg void OnEditFill();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnEditCurrentline();
	afx_msg void OnUpdateDebugRuntocursor(CCmdUI* pCmdUI);
	afx_msg void OnDebugRuntocursor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CPSFWorkArea* GetWorkArea();
	DWORD GetCursorPC();
	void NavJump(DWORD dwAddress);
	void NavStackEmpty();
	int NavStackSize();
	CPoint NavPop();
	void NavPush();
	CUIntArray iaNavStack;
	LPCTSTR FindCompleteHex8InString(LPCTSTR lpszString, int nIndex);
	CString FindCompleteHex8AtCursor();
	BOOL IsCompleteHex8(LPCTSTR lpszString);
	int PCToLineIndex(DWORD dwPC);
	BOOL IsAddressValid(DWORD dwAddress);
	DWORD LineIndexToPC(int nLineIndex);
	int Disassemble(LPTSTR lpszDest, LPCOLORREF lpcrFG, DWORD dwPC, DWORD dwInstruction);
	int DH(LPTSTR lpszDest, LPCOLORREF lpcrFG, DWORD dwPC, DWORD dwInstruction, LPCTSTR lpszFormat);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CODEVIEW_H__6DCFF5B9_98BE_4A55_A0CB_C19A1C5B8970__INCLUDED_)
