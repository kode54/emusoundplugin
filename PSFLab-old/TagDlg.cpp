// TagDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "TagDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTagDlg dialog


CTagDlg::CTagDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTagDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTagDlg)
	m_strTag = _T("");
	//}}AFX_DATA_INIT
}


void CTagDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTagDlg)
	DDX_Text(pDX, IDC_TAGTEXT, m_strTag);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTagDlg, CDialog)
	//{{AFX_MSG_MAP(CTagDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTagDlg message handlers

