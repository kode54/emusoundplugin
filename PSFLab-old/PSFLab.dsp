# Microsoft Developer Studio Project File - Name="PSFLab" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=PSFLab - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "PSFLab.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "PSFLab.mak" CFG="PSFLab - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "PSFLab - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "PSFLab - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "PSFLab - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /G6 /Gr /MT /W3 /GX /O2 /Ob2 /D "_WINDOWS" /D ZEXPORTVA=__cdecl /D CDECL=__cdecl /D "WIN32" /D "NDEBUG" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 winmm.lib ../zlib/Release/zlib.lib ../7z/Release/7z.lib /nologo /subsystem:windows /machine:I386
# SUBTRACT LINK32 /profile

!ELSEIF  "$(CFG)" == "PSFLab - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /G6 /Gr /MTd /W3 /Gm /GX /ZI /Od /Ob1 /D "_WINDOWS" /D ZEXPORTVA=__cdecl /D CDECL=__cdecl /D "WIN32" /D "_DEBUG" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 winmm.lib ../zlib/Debug/zlib.lib ../7z/Debug/7z.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# SUBTRACT LINK32 /profile

!ENDIF 

# Begin Target

# Name "PSFLab - Win32 Release"
# Name "PSFLab - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AddBreakpointDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AssembleDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BreakpointsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BrowseForFolder.cpp
# End Source File
# Begin Source File

SOURCE=.\CallStack.cpp
# End Source File
# Begin Source File

SOURCE=.\CallStackView.cpp
# End Source File
# Begin Source File

SOURCE=.\CodeView.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorButton.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CompressionOptionsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Console.cpp
# End Source File
# Begin Source File

SOURCE=.\EmuSettingsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\EventLog.cpp
# End Source File
# Begin Source File

SOURCE=.\EventView.cpp
# End Source File
# Begin Source File

SOURCE=.\ExeSettingsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FancyTextView.cpp
# End Source File
# Begin Source File

SOURCE=.\FillDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FindDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FindHistory.cpp
# End Source File
# Begin Source File

SOURCE=.\GoToDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ImportBinaryDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MemBreakpointDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MemoryView.cpp
# End Source File
# Begin Source File

SOURCE=.\MIPSQuickAssembler.cpp
# End Source File
# Begin Source File

SOURCE=.\MyCreateDIB.cpp
# End Source File
# Begin Source File

SOURCE=.\NewDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\OptimizeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PatchDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PSFLab.cpp
# End Source File
# Begin Source File

SOURCE=.\PSFLab.rc
# End Source File
# Begin Source File

SOURCE=.\PSFWorkArea.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterView.cpp
# End Source File
# Begin Source File

SOURCE=.\RunToSpecificLineDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectRangeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StateSlotDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\SubSplitterWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\TagDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WaveOutput.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AddBreakpointDlg.h
# End Source File
# Begin Source File

SOURCE=.\AssembleDlg.h
# End Source File
# Begin Source File

SOURCE=.\BreakpointsDlg.h
# End Source File
# Begin Source File

SOURCE=.\BrowseForFolder.h
# End Source File
# Begin Source File

SOURCE=.\CallStack.h
# End Source File
# Begin Source File

SOURCE=.\CallStackView.h
# End Source File
# Begin Source File

SOURCE=.\CodeView.h
# End Source File
# Begin Source File

SOURCE=.\ColorButton.h
# End Source File
# Begin Source File

SOURCE=.\ColorsDlg.h
# End Source File
# Begin Source File

SOURCE=.\CompressionOptionsDlg.h
# End Source File
# Begin Source File

SOURCE=.\Console.h
# End Source File
# Begin Source File

SOURCE=.\EmuSettingsDlg.h
# End Source File
# Begin Source File

SOURCE=.\EventLog.h
# End Source File
# Begin Source File

SOURCE=.\EventView.h
# End Source File
# Begin Source File

SOURCE=.\ExeSettingsDlg.h
# End Source File
# Begin Source File

SOURCE=.\FancyTextView.h
# End Source File
# Begin Source File

SOURCE=.\FillDlg.h
# End Source File
# Begin Source File

SOURCE=.\FindDlg.h
# End Source File
# Begin Source File

SOURCE=.\FindHistory.h
# End Source File
# Begin Source File

SOURCE=.\GoToDlg.h
# End Source File
# Begin Source File

SOURCE=.\ImportBinaryDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MemBreakpointDlg.h
# End Source File
# Begin Source File

SOURCE=.\MemoryView.h
# End Source File
# Begin Source File

SOURCE=.\MIPSQuickAssembler.h
# End Source File
# Begin Source File

SOURCE=.\MyCreateDIB.h
# End Source File
# Begin Source File

SOURCE=.\NewDlg.h
# End Source File
# Begin Source File

SOURCE=.\OptimizeDlg.h
# End Source File
# Begin Source File

SOURCE=.\PatchDlg.h
# End Source File
# Begin Source File

SOURCE=.\PSFLab.h
# End Source File
# Begin Source File

SOURCE=.\PSFWorkArea.h
# End Source File
# Begin Source File

SOURCE=.\RegisterView.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\RunToSpecificLineDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelectRangeDlg.h
# End Source File
# Begin Source File

SOURCE=.\StateSlotDlg.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\SubSplitterWnd.h
# End Source File
# Begin Source File

SOURCE=.\TagDlg.h
# End Source File
# Begin Source File

SOURCE=.\WaveOutput.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cloud.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cloud500.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cloud500comp.bmp
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\PSFLab.ico
# End Source File
# Begin Source File

SOURCE=.\res\PSFLab.rc2
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
