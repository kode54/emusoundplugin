#if !defined(AFX_OPTIMIZEDLG_H__ECED16B0_88F4_4141_A0E1_D77C432DCD53__INCLUDED_)
#define AFX_OPTIMIZEDLG_H__ECED16B0_88F4_4141_A0E1_D77C432DCD53__INCLUDED_

#include "PSFWorkArea.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptimizeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptimizeDlg dialog

class COptimizeDlg : public CDialog
{
// Construction
public:
	BOOL m_bParanoid;
	int m_nParanoidBytes;
	CPSFWorkArea *m_pWorkArea;
	COptimizeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COptimizeDlg)
	enum { IDD = IDD_OPTIMIZE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptimizeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptimizeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	afx_msg void OnApplychanges();
	afx_msg void OnEmusettings();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void SyncVariablesToControls();
	CPSFAudit m_audit;
	BOOL m_bAuditing;
	void UpdateStatistics();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIMIZEDLG_H__ECED16B0_88F4_4141_A0E1_D77C432DCD53__INCLUDED_)
