// EventLog.cpp: implementation of the CEventLog class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PSFLab.h"
#include "EventLog.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEventLog::CEventLog() { }

CEventLog::~CEventLog() { }

int CEventLog::GetLineCount()
{
  m_cs.Lock();
  int nReturn = m_sa.GetSize();
  m_cs.Unlock();
  return nReturn;
}

void CEventLog::Add(LPCTSTR lpszText)
{
  m_cs.Lock();
  m_sa.Add(lpszText);
  m_cs.Unlock();
}

CString CEventLog::Get(int nLineIndex)
{
  m_cs.Lock();
  CString str;
  int s = m_sa.GetSize();
  if(nLineIndex >= 0 && nLineIndex < s) {
    str = m_sa.GetAt(nLineIndex);
  }
  m_cs.Unlock();
  return str;
}

void CEventLog::Clear()
{
  m_cs.Lock();
  m_sa.SetSize(0);
  m_cs.Unlock();
}

void CEventLog::CopyFrom(CEventLog &src)
{
  m_cs.Lock();
  src.m_cs.Lock();
  int s = src.m_sa.GetSize();
  m_sa.SetSize(s);
  int i;
  for(i = 0; i < s; i++) {
    m_sa[i] = src.m_sa[i];
  }
  src.m_cs.Unlock();
  m_cs.Unlock();
}
