// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__3B9608D9_00A4_4289_AD32_4B28E776E415__INCLUDED_)
#define AFX_STDAFX_H__3B9608D9_00A4_4289_AD32_4B28E776E415__INCLUDED_

// set WINVER - this warning came up when I migrated the project from VC++ 6 to VS.NET 7.
// we'd like all compiled programs to run on Win95 or higher.
#ifndef WINVER
#define WINVER 0x0400
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxmt.h>

#include <afxtempl.h>

//#include "sizecbar.h"
//#include "scbarg.h"
//#include "scbarcf.h"

#include "../Emu/emu.h"
#include "../Util/Util.h"

//#include <afxole.h>
//#include <afxadv.h>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__3B9608D9_00A4_4289_AD32_4B28E776E415__INCLUDED_)
