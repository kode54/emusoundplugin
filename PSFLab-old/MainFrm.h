// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__CC35C97D_2198_4135_AD3E_F2B291979886__INCLUDED_)
#define AFX_MAINFRM_H__CC35C97D_2198_4135_AD3E_F2B291979886__INCLUDED_

//#include "SubSplitterWnd.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "SubWindow.h"

class CMainFrame : public CFrameWnd
{
protected: 
	DECLARE_DYNCREATE(CMainFrame)
	CMainFrame();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	//void CreateViewBars();
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CSplitterWnd m_wndSplitter;
//	CViewBar barRegisters;
//	CViewBar barMemory;
//	CViewBar barCallStack;
//	CViewBar barEvents;
	//CView *FindView(CRuntimeClass *pClass);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__CC35C97D_2198_4135_AD3E_F2B291979886__INCLUDED_)
