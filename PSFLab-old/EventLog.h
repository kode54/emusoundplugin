// EventLog.h: interface for the CEventLog class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EVENTLOG_H__91AEAFF3_BBA5_4DD2_96EB_8B640D74F49A__INCLUDED_)
#define AFX_EVENTLOG_H__91AEAFF3_BBA5_4DD2_96EB_8B640D74F49A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEventLog  
{
public:
	CEventLog();
	virtual ~CEventLog();

	void Clear();
	CString Get(int nLineIndex);
	void Add(LPCTSTR lpszText);
	int GetLineCount();
  void CopyFrom(CEventLog &src);

private:
  CStringArray m_sa;
	CCriticalSection m_cs;
};

#endif // !defined(AFX_EVENTLOG_H__91AEAFF3_BBA5_4DD2_96EB_8B640D74F49A__INCLUDED_)
