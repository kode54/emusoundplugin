// FindHistory.cpp: implementation of the CFindHistory class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "psflab.h"
#include "FindHistory.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFindHistory::CFindHistory()
{
  m_bBackwards = 0;
  m_bMatchCase = 0;
  m_bMatchSpaces = 0;
}

CFindHistory::~CFindHistory()
{

}

//////////////////////////////////////////////////////////////////////

const CStringArray* CFindHistory::GetStringArray() const
{
  return &m_sa;
}

void CFindHistory::AddString(LPCTSTR lpszText)
{
  int i, j;
  int s = m_sa.GetSize();
  for(i = 0; i < s; i++) {
    if(!lstrcmp(m_sa.GetAt(i), lpszText)) break;
  }
//012345678
//-----*---
  if(i < s) {
    for(j = i; j < (s-1); j++) {
      m_sa.SetAt(j, m_sa.GetAt(j + 1));
    }
    m_sa.SetSize(s - 1);
  }
  m_sa.Add(lpszText);
}

void CFindHistory::Clear() { m_sa.SetSize(0); }

BOOL CFindHistory::IsBackwards() const { return m_bBackwards; }
BOOL CFindHistory::IsMatchCase() const { return m_bMatchCase; }
BOOL CFindHistory::IsMatchSpaces() const { return m_bMatchSpaces; }

CString CFindHistory::GetMostRecentString() const {
  int s = m_sa.GetSize();
  if(s < 1) return _T("");
  return m_sa.GetAt(s - 1);
}

void CFindHistory::SetBackwards(BOOL bBackwards) { m_bBackwards = bBackwards; }
void CFindHistory::SetMatchCase(BOOL bMatchCase) { m_bMatchCase = bMatchCase; }
void CFindHistory::SetMatchSpaces(BOOL bMatchSpaces) { m_bMatchSpaces = bMatchSpaces; }
