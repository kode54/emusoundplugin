// FindDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "FindDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFindDlg dialog


CFindDlg::CFindDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFindDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFindDlg)
	m_bMatchCase = FALSE;
	m_nDir = -1;
	m_bMatchSpaces = FALSE;
	m_strFindWhat = _T("");
	//}}AFX_DATA_INIT

  m_saHistory = NULL;
}


void CFindDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFindDlg)
	DDX_Control(pDX, IDOK, m_cOK);
	DDX_Control(pDX, IDC_FINDWHAT, m_cFindWhat);
	DDX_Check(pDX, IDC_MATCHCASE, m_bMatchCase);
	DDX_Radio(pDX, IDC_DIR, m_nDir);
	DDX_Check(pDX, IDC_MATCHSPACES, m_bMatchSpaces);
	DDX_CBString(pDX, IDC_FINDWHAT, m_strFindWhat);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFindDlg, CDialog)
	//{{AFX_MSG_MAP(CFindDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFindDlg message handlers

BOOL CFindDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if(m_saHistory) {
    int s = m_saHistory->GetSize();
    while(s--) { m_cFindWhat.AddString(m_saHistory->GetAt(s)); }
  }

  UpdateOKButton();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFindDlg::UpdateOKButton()
{
  CString s;
  m_cFindWhat.GetWindowText(s);
  m_cOK.EnableWindow(!s.IsEmpty());
}

BOOL CFindDlg::PreTranslateMessage(MSG* pMsg) 
{
  UpdateOKButton();
	return CDialog::PreTranslateMessage(pMsg);
}
