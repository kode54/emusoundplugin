#if !defined(AFX_SELECTRANGEDLG_H__03566A77_0282_4C03_AA19_3EF20A04E3B5__INCLUDED_)
#define AFX_SELECTRANGEDLG_H__03566A77_0282_4C03_AA19_3EF20A04E3B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectRangeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectRangeDlg dialog

class CSelectRangeDlg : public CDialog
{
// Construction
public:
	DWORD m_dwEndAddress;
	DWORD m_dwStartAddress;
	CSelectRangeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectRangeDlg)
	enum { IDD = IDD_SELECTRANGE };
	CEdit	m_cStartAddress;
	CEdit	m_cEndAddress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectRangeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectRangeDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTRANGEDLG_H__03566A77_0282_4C03_AA19_3EF20A04E3B5__INCLUDED_)
