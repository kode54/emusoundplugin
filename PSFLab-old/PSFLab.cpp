// PSFLab.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "PSFLab.h"

#include "MainFrm.h"

#include "TagDlg.h"
#include "BreakpointsDlg.h"
#include "CodeView.h"
#include "ColorsDlg.h"
#include "EmuSettingsDlg.h"
#include "NewDlg.h"
#include "CompressionOptionsDlg.h"

#include "../Emu/emu.h"

#include "MyCreateDIB.h"

#include "BrowseForFolder.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

LPCTSTR PSFLabBannerString = _T("PSFLab v1.04");

/***************************************************************************/

struct SInternalColorCode {
  COLORREF cr;
  LPCTSTR regkey;
  LPCTSTR name;
};

// think this is safe without thread sync
static struct SInternalColorCode internal_color_codes[PSFLAB_COLOR_TOTAL] = {
  { RGB(0x00,0x00,0x40), _T("code_bg"                 ), _T("Code - Background"                      ) },
  { RGB(0x00,0x40,0x80), _T("code_bg_current_line"    ), _T("Code - Background - Current line"       ) },
  { RGB(0x60,0x00,0x60), _T("code_bg_patched"         ), _T("Code - Background - Patched"            ) },
  { RGB(0xFF,0x00,0x00), _T("code_bg_breakpoint"      ), _T("Code - Background - Breakpoint"         ) },
  { RGB(0xC0,0xC0,0xC0), _T("code_fg"                 ), _T("Code - Foreground"                      ) },
  { RGB(0xFF,0xFF,0x80), _T("code_fg_current_line"    ), _T("Code - Foreground - Current line"       ) },
  { RGB(0xFF,0xFF,0xFF), _T("code_fg_breakpoint"      ), _T("Code - Foreground - Breakpoint"         ) },
  { RGB(0xFF,0xFF,0xFF), _T("code_fg_cursor_highlight"), _T("Code - Foreground - Cursor highlight"   ) },
  { RGB(0x80,0xFF,0x80), _T("code_fg_immediate"       ), _T("Code - Foreground - Immediate"          ) },
  { RGB(0xFF,0xFF,0x80), _T("code_fg_address"         ), _T("Code - Foreground - Address"            ) },
  { RGB(0x00,0xC0,0xC0), _T("code_fg_punctuation"     ), _T("Code - Foreground - Punctuation"        ) },
  { RGB(0x80,0x80,0x80), _T("code_fg_unknown"         ), _T("Code - Foreground - Unknown instruction") },
  { RGB(0x20,0x00,0x40), _T("reg_bg"                  ), _T("Registers - Background"                 ) },
  { RGB(0x40,0x00,0x80), _T("reg_bg_modified"         ), _T("Registers - Background - Modified"      ) },
  { RGB(0xC0,0xC0,0xC0), _T("reg_fg"                  ), _T("Registers - Foreground"                 ) },
  { RGB(0xFF,0xFF,0xFF), _T("reg_fg_modified"         ), _T("Registers - Foreground - Modified"      ) },
  { RGB(0x80,0x80,0x80), _T("reg_fg_punctuation"      ), _T("Registers - Foreground - Punctuation"   ) },
  { RGB(0x00,0x00,0x40), _T("mem_bg"                  ), _T("Memory - Background"                    ) },
  { RGB(0x60,0x00,0x60), _T("mem_bg_patched"          ), _T("Memory - Background - Patched"          ) },
  { RGB(0xFF,0x00,0x00), _T("mem_bg_breakpoint"       ), _T("Memory - Background - Breakpoint"       ) },
  { RGB(0xC0,0xC0,0xC0), _T("mem_fg"                  ), _T("Memory - Foreground"                    ) },
  { RGB(0xFF,0xFF,0xFF), _T("mem_fg_breakpoint"       ), _T("Memory - Foreground - Breakpoint"       ) },
  { RGB(0xFF,0xFF,0xFF), _T("mem_fg_cursor_highlight" ), _T("Memory - Foreground - Cursor highlight" ) },
  { RGB(0xFF,0xFF,0x80), _T("mem_fg_printable_text"   ), _T("Memory - Foreground - Printable text"   ) },
  { RGB(0x80,0x80,0x80), _T("mem_fg_unprintable_text" ), _T("Memory - Foreground - Unprintable text" ) },
  { RGB(0x00,0x30,0x00), _T("call_bg"                 ), _T("Call Stack - Background"                ) },
  { RGB(0x00,0xFF,0x80), _T("call_fg"                 ), _T("Call Stack - Foreground"                ) },
  { RGB(0xFF,0xFF,0xFF), _T("call_fg_address"         ), _T("Call Stack - Foreground - Address"      ) },
  { RGB(0xFF,0xFF,0x80), _T("call_fg_exception"       ), _T("Call Stack - Foreground - Exception"    ) },
  { RGB(0xFF,0xFF,0xFF), _T("call_fg_cause"           ), _T("Call Stack - Foreground - Cause"        ) },
  { RGB(0x40,0x00,0x00), _T("event_bg"                ), _T("Event Log - Background"                 ) },
  { RGB(0xC0,0xC0,0xC0), _T("event_fg"                ), _T("Event Log - Foreground"                 ) },
  { RGB(0xFF,0xFF,0x80), _T("event_fg_address"        ), _T("Event Log - Foreground - Address"       ) },
  { RGB(0xFF,0xFF,0xFF), _T("event_fg_event_type"     ), _T("Event Log - Foreground - Event type"    ) }
};

/***************************************************************************/

BOOL ParseNumber(LPCTSTR str, DWORD &dwValue)
{
  DWORD d = 0;
  int radix = 1;
  if(!str) return FALSE;
  while(*str && isspace(*str)) str++;
  if(!(*str)) return FALSE;
  if(!isdigit(*str)) return FALSE;
  if(str[0] == _T('0') && (str[1] == _T('x') || str[1] == _T('X'))) {
    str += 2; 
    radix = 16;
  } else if(str[0] == _T('0')) {
    radix = 8;
  } else {
    radix = 10;
  }
  while(*str && isxdigit(*str)) {
    TCHAR t = *str++;
    if(t >= _T('0') && t <= _T('9')) {
      t = t - _T('0');
    } else if(t >= _T('A') && t <= _T('F')) {
      t = (t - _T('A')) + 10;
    } else if(t >= _T('a') && t <= _T('f')) {
      t = (t - _T('a')) + 10;
    } else {
      return FALSE;
    }
    if(t >= radix) return FALSE;
    d = (d * radix) + t;
  }
  while(*str) {
    if(!(isspace(*str))) return FALSE;
    str++;
  }
  dwValue = d;
  return TRUE;
}

BOOL ParseDlgValue(LPCTSTR lpszString, DWORD &dwValue, CDialog *dlg, int nID, LPCTSTR lpszValueName)
{
  if(ParseNumber(lpszString, dwValue)) return TRUE;
  CString msg;
  msg.Format(_T("Please enter a valid %s.\nHex constants must be prefixed with '0x'."), lpszValueName);
  dlg->MessageBox(msg, _T("Error"), MB_ICONHAND|MB_OK);
  dlg->GetDlgItem(nID)->SetFocus();
  return FALSE;
}

////////////////// 
// This static function can be sed to initialize any menu 
// when you get WM_INITMENUPOPUP 
// 
void my_init_popup(
  CCmdTarget* pTarg, 
  CMenu* pMenu,
  BOOL bAutoMenuEnable
) { 
    CCmdUI state; 
    state.m_pMenu = pMenu; 
    ASSERT(state.m_pOther == NULL); 
    ASSERT(state.m_pParentMenu == NULL); 

    state.m_nIndexMax = pMenu->GetMenuItemCount(); 
    for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax; 
      state.m_nIndex++) 
    { 
        state.m_nID = pMenu->GetMenuItemID(state.m_nIndex); 
        if (state.m_nID == 0) 
            continue; // menu separator or invalid cmd - ignore it 

        ASSERT(state.m_pOther == NULL); 
        ASSERT(state.m_pMenu != NULL);
        if (state.m_nID == (UINT)-1) {
            // possibly a popup menu, route to first item of that popup 
            CMenu *sub = pMenu->GetSubMenu(state.m_nIndex);
            if(sub) my_init_popup(pTarg, sub, bAutoMenuEnable);
        } else { 
            // normal menu item 
            // Auto enable/disable if 'bAutoMenuEnable' 
            // and command is _not_ a system command. 
            state.m_pSubMenu = NULL; 
            state.DoUpdate(pTarg, bAutoMenuEnable && state.m_nID < 0xF000); 
        } 
        // adjust for menu deletions and additions 
        UINT nCount = pMenu->GetMenuItemCount(); 
        if (nCount < state.m_nIndexMax) 
        { 
            state.m_nIndex -= (state.m_nIndexMax - nCount); 
            while (state.m_nIndex < nCount && 
                pMenu->GetMenuItemID(state.m_nIndex) == state.m_nID) 
            { 
                state.m_nIndex++; 
            } 
        } 
        state.m_nIndexMax = nCount; 
    } 
} 

/////////////////////////////////////////////////////////////////////////////
// CPSFLabApp

BEGIN_MESSAGE_MAP(CPSFLabApp, CWinApp)
	//{{AFX_MSG_MAP(CPSFLabApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_UPDATE_COMMAND_UI(ID_EVENT_DMA, OnUpdateEventDma)
	ON_UPDATE_COMMAND_UI(ID_EVENT_INTR, OnUpdateEventIntr)
	ON_UPDATE_COMMAND_UI(ID_EVENT_LH, OnUpdateEventLh)
	ON_UPDATE_COMMAND_UI(ID_EVENT_SH, OnUpdateEventSh)
	ON_COMMAND(ID_EVENT_SH, OnEventSh)
	ON_COMMAND(ID_EVENT_LH, OnEventLh)
	ON_COMMAND(ID_EVENT_INTR, OnEventIntr)
	ON_COMMAND(ID_EVENT_DMA, OnEventDma)
	ON_COMMAND(ID_EVENT_ALL, OnEventAll)
	ON_COMMAND(ID_EVENT_NONE, OnEventNone)
	ON_UPDATE_COMMAND_UI(ID_TIME_CYCLES, OnUpdateTimeCycles)
	ON_COMMAND(ID_TIME_CYCLES, OnTimeCycles)
	ON_UPDATE_COMMAND_UI(ID_TIME_FRAMES, OnUpdateTimeFrames)
	ON_COMMAND(ID_TIME_FRAMES, OnTimeFrames)
	ON_UPDATE_COMMAND_UI(ID_TIME_NONE, OnUpdateTimeNone)
	ON_COMMAND(ID_TIME_NONE, OnTimeNone)
	ON_UPDATE_COMMAND_UI(ID_TIME_SAMPLES, OnUpdateTimeSamples)
	ON_COMMAND(ID_TIME_SAMPLES, OnTimeSamples)
	ON_UPDATE_COMMAND_UI(ID_TIME_SECONDS, OnUpdateTimeSeconds)
	ON_COMMAND(ID_TIME_SECONDS, OnTimeSeconds)
	ON_COMMAND(ID_VIEW_FONT, OnViewFont)
	ON_COMMAND(ID_VIEW_COLORS, OnViewColors)
	ON_COMMAND(ID_DEBUG_EMUSETTINGS, OnDebugEmusettings)
	ON_COMMAND(ID_FILE_NEWCHOOSE, OnFileNewchoose)
	ON_COMMAND(ID_FILE_COMPRESSIONOPTIONS, OnFileCompressionoptions)
	ON_COMMAND(ID_EVENT_VIO, OnEventVio)
	ON_UPDATE_COMMAND_UI(ID_EVENT_VIO, OnUpdateEventVio)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_UPDATE_COMMAND_UI(ID_EVENT_CONSOLE, OnUpdateEventConsole)
	ON_UPDATE_COMMAND_UI(ID_EVENT_EVENT, OnUpdateEventEvent)
	ON_COMMAND(ID_EVENT_CONSOLE, OnEventConsole)
	ON_COMMAND(ID_EVENT_EVENT, OnEventEvent)
	ON_COMMAND(ID_FILE_PSF2PATH, OnFilePsf2path)
	ON_UPDATE_COMMAND_UI(ID_FILE_PSF2PATH, OnUpdateFilePsf2path)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPSFLabApp construction

CPSFLabApp::CPSFLabApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CPSFLabApp object

CPSFLabApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CPSFLabApp initialization

BOOL CPSFLabApp::InitInstance()
{
  emu_init();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	SetRegistryKey(_T("Neill Corlett"));

  LoadStdProfileSettings();

  LoadFont();
  LoadColors();
  LoadOptions();
  LoadWindowPositions();

  m_nNewPSFVersion = m_nWorkAreaVersion;

  // create doc template
  CSingleDocTemplate* pDocTemplate = new CSingleDocTemplate(
    IDR_MAINFRAME,
    RUNTIME_CLASS(CPSFWorkArea),
    RUNTIME_CLASS(CMainFrame),
    RUNTIME_CLASS(CCodeView)
  );
	AddDocTemplate(pDocTemplate);

  // Parse command line for standard shell commands, DDE, file open
  CCommandLineInfo cmdInfo;
  ParseCommandLine(cmdInfo);

  // Dispatch commands specified on the command line
  if(!ProcessShellCommand(cmdInfo)) return FALSE;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

/***************************************************************************/

int CPSFLabApp::ExitInstance() 
{
  SaveFont();
  SaveColors();
  SaveOptions();
  SaveWindowPositions();
	return CWinApp::ExitInstance();
}

/***************************************************************************/

/////////////////////////////////////////////////////////////////////////////
// CPSFLabApp message handlers

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_cBanner;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	afx_msg void OnVersiondetails();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_BANNER, m_cBanner);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(IDC_VERSIONDETAILS, OnVersiondetails)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CPSFLabApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CPSFLabApp message handlers

void CPSFLabApp::OnFileNew() 
{
	// TODO: Add your command handler code here
  CWinApp::OnFileNew();
}

void CPSFLabApp::OnFileOpen() 
{
	// TODO: Add your command handler code here
	CWinApp::OnFileOpen();
}

DWORD CPSFLabApp::GetEventMask()
{
  return m_dwEventMask;
}

void CPSFLabApp::OnUpdateEventDma(CCmdUI* pCmdUI) {
	pCmdUI->Enable(TRUE); pCmdUI->SetCheck((m_dwEventMask >> (IOP_EVENT_DMA_TRANSFER)) & 1);
}

void CPSFLabApp::OnUpdateEventIntr(CCmdUI* pCmdUI) {
	pCmdUI->Enable(TRUE); pCmdUI->SetCheck((m_dwEventMask >> (IOP_EVENT_INTR_SIGNAL)) & 1);
}

void CPSFLabApp::OnUpdateEventLh(CCmdUI* pCmdUI) {
	pCmdUI->Enable(TRUE); pCmdUI->SetCheck((m_dwEventMask >> (IOP_EVENT_REG_LOAD)) & 1);
}

void CPSFLabApp::OnUpdateEventSh(CCmdUI* pCmdUI) {
	pCmdUI->Enable(TRUE); pCmdUI->SetCheck((m_dwEventMask >> (IOP_EVENT_REG_STORE)) & 1);
}

void CPSFLabApp::OnUpdateEventVio(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE); pCmdUI->SetCheck((m_dwEventMask >> (IOP_EVENT_VIRTUAL_IO)) & 1);
}

void CPSFLabApp::OnEventSh() { m_dwEventMask ^= 1 << (IOP_EVENT_REG_STORE); }
void CPSFLabApp::OnEventLh() { m_dwEventMask ^= 1 << (IOP_EVENT_REG_LOAD); }
void CPSFLabApp::OnEventIntr() { m_dwEventMask ^= 1 << (IOP_EVENT_INTR_SIGNAL); }
void CPSFLabApp::OnEventDma() { m_dwEventMask ^= 1 << (IOP_EVENT_DMA_TRANSFER); }
void CPSFLabApp::OnEventVio() { m_dwEventMask ^= 1 << (IOP_EVENT_VIRTUAL_IO); }

void CPSFLabApp::OnEventAll() { m_dwEventMask = 0xFFFFFFFF; }
void CPSFLabApp::OnEventNone() { m_dwEventMask = 0x00000000; }

int CPSFLabApp::GetEventTimeFormat() { return m_nEventTimeFormat; }

void CPSFLabApp::OnUpdateTimeNone   (CCmdUI* pCmdUI) { pCmdUI->Enable(TRUE); pCmdUI->SetCheck(m_nEventTimeFormat == PSF_EVENT_TIME_FORMAT_NONE   ); }
void CPSFLabApp::OnUpdateTimeCycles (CCmdUI* pCmdUI) { pCmdUI->Enable(TRUE); pCmdUI->SetCheck(m_nEventTimeFormat == PSF_EVENT_TIME_FORMAT_CYCLES ); }
void CPSFLabApp::OnUpdateTimeFrames (CCmdUI* pCmdUI) { pCmdUI->Enable(TRUE); pCmdUI->SetCheck(m_nEventTimeFormat == PSF_EVENT_TIME_FORMAT_FRAMES ); }
void CPSFLabApp::OnUpdateTimeSamples(CCmdUI* pCmdUI) { pCmdUI->Enable(TRUE); pCmdUI->SetCheck(m_nEventTimeFormat == PSF_EVENT_TIME_FORMAT_SAMPLES); }
void CPSFLabApp::OnUpdateTimeSeconds(CCmdUI* pCmdUI) { pCmdUI->Enable(TRUE); pCmdUI->SetCheck(m_nEventTimeFormat == PSF_EVENT_TIME_FORMAT_SECONDS); }

void CPSFLabApp::OnTimeNone   () { m_nEventTimeFormat = PSF_EVENT_TIME_FORMAT_NONE   ; UpdateEventTimeFormat(); }
void CPSFLabApp::OnTimeCycles () { m_nEventTimeFormat = PSF_EVENT_TIME_FORMAT_CYCLES ; UpdateEventTimeFormat(); }
void CPSFLabApp::OnTimeFrames () { m_nEventTimeFormat = PSF_EVENT_TIME_FORMAT_FRAMES ; UpdateEventTimeFormat(); }
void CPSFLabApp::OnTimeSamples() { m_nEventTimeFormat = PSF_EVENT_TIME_FORMAT_SAMPLES; UpdateEventTimeFormat(); }
void CPSFLabApp::OnTimeSeconds() { m_nEventTimeFormat = PSF_EVENT_TIME_FORMAT_SECONDS; UpdateEventTimeFormat(); }

void CPSFLabApp::UpdateEventTimeFormat()
{
  CDocument *pDoc = ((CFrameWnd*)::AfxGetMainWnd())->GetActiveDocument();
  if(pDoc) pDoc->UpdateAllViews(NULL);
}

void CPSFLabApp::OnViewFont() 
{
	// TODO: Add your command handler code here
	CFontDialog dlgFont;

  dlgFont.m_cf.lpLogFont = &m_lfBaseFont;
  dlgFont.m_cf.Flags |= CF_INITTOLOGFONTSTRUCT;

  dlgFont.m_cf.Flags |= CF_FORCEFONTEXIST;
  dlgFont.m_cf.Flags &= ~CF_EFFECTS;

  if(dlgFont.DoModal() != IDOK) return;

  CFrameWnd *pFrameWnd = (CFrameWnd*)(::AfxGetMainWnd());
  ASSERT(pFrameWnd);
  ASSERT(pFrameWnd->IsKindOf(RUNTIME_CLASS(CFrameWnd)));
  CDocument *pDoc = pFrameWnd->GetActiveDocument();
  ASSERT(pDoc);
  ASSERT(pDoc->IsKindOf(RUNTIME_CLASS(CDocument)));

  POSITION p = pDoc->GetFirstViewPosition();

  for(;;) {
    CView *pView = pDoc->GetNextView(p);
    if(!pView) break;
    if(pView->IsKindOf(RUNTIME_CLASS(CFancyTextView))) {
      ((CFancyTextView*)pView)->SetLogFont(m_lfBaseFont);
    }
  }

}

/***************************************************************************/

COLORREF CPSFLabApp::GetColor(int nIndex) const
{
  ASSERT(nIndex >= 0 && nIndex < PSFLAB_COLOR_TOTAL);
  return m_acrColors[nIndex];
}

LPCTSTR CPSFLabApp::GetColorName(int nIndex) const
{
  ASSERT(nIndex >= 0 && nIndex < PSFLAB_COLOR_TOTAL);
  return internal_color_codes[nIndex].name;
}

/***************************************************************************/

void CPSFLabApp::LoadColors()
{
  int i;
  for(i = 0; i < PSFLAB_COLOR_TOTAL; i++) {
    m_acrColors[i] = GetProfileInt(
      _T("Colors"),
      internal_color_codes[i].regkey,
      internal_color_codes[i].cr
    );
  }
}

void CPSFLabApp::SaveColors()
{
  int i;
  for(i = 0; i < PSFLAB_COLOR_TOTAL; i++) {
    WriteProfileInt(
      _T("Colors"),
      internal_color_codes[i].regkey,
      m_acrColors[i]
    );
  }
}

/***************************************************************************/

void CPSFLabApp::LoadFont()
{
/*
	memset(&m_lfBaseFont, 0, sizeof(m_lfBaseFont));
  CString strFace = GetProfileString(_T("Font"), _T("face"), _T("FixedSys"));
  lstrcpyn(m_lfBaseFont.lfFaceName, (LPCTSTR)strFace, LF_FACESIZE);
	m_lfBaseFont.lfHeight         = GetProfileInt(_T("Font"), _T("height"), 0);
	m_lfBaseFont.lfWeight         = GetProfileInt(_T("Font"), _T("weight"), FW_NORMAL);
	m_lfBaseFont.lfItalic         = GetProfileInt(_T("Font"), _T("italic"), FALSE);
	m_lfBaseFont.lfCharSet        = GetProfileInt(_T("Font"), _T("charset"), DEFAULT_CHARSET);
	m_lfBaseFont.lfOutPrecision   = OUT_DEFAULT_PRECIS;
	m_lfBaseFont.lfClipPrecision  = CLIP_DEFAULT_PRECIS;
	m_lfBaseFont.lfQuality        = DEFAULT_QUALITY;
	m_lfBaseFont.lfPitchAndFamily = GetProfileInt(_T("Font"), _T("pitchandfamily"), DEFAULT_PITCH);
*/
	memset(&m_lfBaseFont, 0, sizeof(m_lfBaseFont));
  lstrcpyn(m_lfBaseFont.lfFaceName, _T("FixedSys"), LF_FACESIZE);
	m_lfBaseFont.lfHeight         = 0;
	m_lfBaseFont.lfWeight         = FW_NORMAL;
	m_lfBaseFont.lfItalic         = FALSE;
	m_lfBaseFont.lfCharSet        = DEFAULT_CHARSET;
	m_lfBaseFont.lfOutPrecision   = OUT_DEFAULT_PRECIS;
	m_lfBaseFont.lfClipPrecision  = CLIP_DEFAULT_PRECIS;
	m_lfBaseFont.lfQuality        = DEFAULT_QUALITY;
	m_lfBaseFont.lfPitchAndFamily = DEFAULT_PITCH;

  LPBYTE data = NULL;
  UINT datalength = 0;
//TRACE("a\n");
  if(!GetProfileBinary(_T("Font"), _T("font"), &data, &datalength)) return;
//TRACE("b\n");
  if(data && (datalength == sizeof(m_lfBaseFont))) {
//TRACE("c\n");
    memcpy(&m_lfBaseFont, data, sizeof(m_lfBaseFont));
  }
//TRACE("d\n");
  if(data) delete[] data;
//TRACE("e\n");
}

void CPSFLabApp::SaveFont()
{
  WriteProfileBinary(_T("Font"), _T("font"), (LPBYTE)(&m_lfBaseFont), sizeof(m_lfBaseFont));
/*
  WriteProfileString(_T("Font"), _T("face"), m_lfBaseFont.lfFaceName);
  WriteProfileInt(_T("Font"), _T("height"), m_lfBaseFont.lfHeight);
  WriteProfileInt(_T("Font"), _T("weight"), m_lfBaseFont.lfWeight);
  WriteProfileInt(_T("Font"), _T("italic"), m_lfBaseFont.lfItalic);
  WriteProfileInt(_T("Font"), _T("charset"), m_lfBaseFont.lfCharSet);
  WriteProfileInt(_T("Font"), _T("pitchandfamily"), m_lfBaseFont.lfPitchAndFamily);
*/
}

/***************************************************************************/

void CPSFLabApp::LoadOptions()
{
  m_dwEventMask = GetProfileInt(_T("Options"), _T("event_mask"),
    (1 << (IOP_EVENT_VIRTUAL_IO)) |
    (1 << (IOP_EVENT_INTR_SIGNAL)) |
    (1 << (IOP_EVENT_DMA_TRANSFER))
  );
  m_nEventTimeFormat = GetProfileInt(_T("Options"), _T("event_time_format"),
    PSF_EVENT_TIME_FORMAT_SECONDS
  );

  m_bAuditParanoid = GetProfileInt(_T("Options"), _T("audit_paranoid"), 1);
  m_nAuditParanoidBytes = GetProfileInt(_T("Options"), _T("audit_paranoid_bytes"), 1024);

  m_strCompressionMethod = GetProfileString(_T("Options"), _T("compression_method"), _T(""));

  m_bConsole = GetProfileInt(_T("Options"), _T("event_console"), 0);

  m_nWorkAreaVersion = GetProfileInt(_T("WorkArea"), _T("version"), 1);
  m_strPSF2Path = GetProfileString(_T("WorkArea"), _T("psf2_path"), _T(""));

  LoadEmuSettingOptions(PSF_EMUSETTINGS_AUDIT, _T("audit"));
  LoadEmuSettingOptions(PSF_EMUSETTINGS_DEBUG, _T("debug"));
  LoadEmuSettingOptions(PSF_EMUSETTINGS_PLAY , _T("play"));

//  strncpy(m_sPSF2Path, m_strPSF2Path, MAX_PATH);
//  m_sPSF2Path[MAX_PATH-1]=0;

}

void CPSFLabApp::SaveOptions()
{
  WriteProfileInt(_T("Options"), _T("event_mask"), m_dwEventMask);
  WriteProfileInt(_T("Options"), _T("event_time_format"), m_nEventTimeFormat);

  WriteProfileInt(_T("Options"), _T("audit_paranoid"), m_bAuditParanoid);
  WriteProfileInt(_T("Options"), _T("audit_paranoid_bytes"), m_nAuditParanoidBytes);

  WriteProfileString(_T("Options"), _T("compression_method"), m_strCompressionMethod);

  WriteProfileInt(_T("Options"), _T("event_console"), m_bConsole);

  WriteProfileInt(_T("WorkArea"), _T("version"), m_nWorkAreaVersion);

  WriteProfileString(_T("WorkArea"), _T("psf2_path"), m_strPSF2Path);

  SaveEmuSettingOptions(PSF_EMUSETTINGS_AUDIT, _T("audit"));
  SaveEmuSettingOptions(PSF_EMUSETTINGS_DEBUG, _T("debug"));
  SaveEmuSettingOptions(PSF_EMUSETTINGS_PLAY , _T("play"));
}

/***************************************************************************/

void CPSFLabApp::LoadWindowPositions()
{
  int x, y, w, h;
  x = GetProfileInt(_T("Window"), _T("x"), 50);
  y = GetProfileInt(_T("Window"), _T("y"), 50);
  w = GetProfileInt(_T("Window"), _T("w"), 900);
  h = GetProfileInt(_T("Window"), _T("h"), 600);
  m_rWindowSize.left   = x;
  m_rWindowSize.right  = x + w;
  m_rWindowSize.top    = y;
  m_rWindowSize.bottom = y + h;
  m_nSplitterCodeWidth       = GetProfileInt(_T("Window"), _T("code_width"      ), 310);
  m_nSplitterMemoryWidth     = GetProfileInt(_T("Window"), _T("memory_width"    ), 400);
  m_nSplitterRegisterHeight  = GetProfileInt(_T("Window"), _T("register_height" ), 200);
  m_nSplitterCallStackHeight = GetProfileInt(_T("Window"), _T("callstack_height"), 200);
}

void CPSFLabApp::SaveWindowPositions()
{
  int x, y, w, h;
  x = m_rWindowSize.left;
  y = m_rWindowSize.top;
  w = m_rWindowSize.right - x;
  h = m_rWindowSize.bottom - y;
  WriteProfileInt(_T("Window"), _T("x"), x);
  WriteProfileInt(_T("Window"), _T("y"), y);
  WriteProfileInt(_T("Window"), _T("w"), w);
  WriteProfileInt(_T("Window"), _T("h"), h);
  WriteProfileInt(_T("Window"), _T("code_width"), m_nSplitterCodeWidth);
  WriteProfileInt(_T("Window"), _T("memory_width"), m_nSplitterMemoryWidth);
  WriteProfileInt(_T("Window"), _T("register_height"), m_nSplitterRegisterHeight);
  WriteProfileInt(_T("Window"), _T("callstack_height"), m_nSplitterCallStackHeight);
}

/***************************************************************************/

const LOGFONT* CPSFLabApp::GetLogFont() const
{
  return (const LOGFONT*)(&m_lfBaseFont);
}

void CPSFLabApp::OnViewColors() 
{
	// TODO: Add your command handler code here
	CColorsDlg dlgColors;

  if(dlgColors.DoModal() != IDOK) return;

  AfxGetMainWnd()->Invalidate(FALSE);
}

void CPSFLabApp::SetColor(int nIndex, COLORREF cr)
{
  if(nIndex < 0 || nIndex >= PSFLAB_COLOR_TOTAL) return;
  m_acrColors[nIndex] = cr;
}

void CPSFLabApp::SetDefaultColors()
{
  int i;
  for(i = 0; i < PSFLAB_COLOR_TOTAL; i++) {
    SetColor(i, internal_color_codes[i].cr);
  }
}

/***************************************************************************/

void CPSFLabApp::GetAuditOptions(BOOL &bParanoid, int &nParanoidBytes)
{
  bParanoid = m_bAuditParanoid;
  nParanoidBytes = m_nAuditParanoidBytes;
}

void CPSFLabApp::SetAuditOptions(BOOL bParanoid, int nParanoidBytes)
{
  m_bAuditParanoid = bParanoid;
  m_nAuditParanoidBytes = nParanoidBytes;
}

/***************************************************************************/

void CPSFLabApp::GetEmuSettings(int nWhich, SPSFEmuSettings &settings)
{
  memcpy(&settings, &(m_EmuSettings[nWhich]), sizeof(SPSFEmuSettings));
}

void CPSFLabApp::SetEmuSettings(int nWhich, SPSFEmuSettings &settings)
{
  memcpy(&(m_EmuSettings[nWhich]), &settings, sizeof(SPSFEmuSettings));
}

/***************************************************************************/

void CPSFLabApp::LoadEmuSettingOptions(int nWhich, LPCTSTR lpszName)
{
  CString s;
  s.Format(_T("%s_emu_friendly"), lpszName);
  m_EmuSettings[nWhich].bFriendly = GetProfileInt(_T("Options"), s, 0);
//  s.Format(_T("%s_emu_divider"), lpszName);
//  m_EmuSettings[nWhich].nDivider = GetProfileInt(_T("Options"), s, 8);
}

void CPSFLabApp::SaveEmuSettingOptions(int nWhich, LPCTSTR lpszName)
{
  CString s;
  s.Format(_T("%s_emu_friendly"), lpszName);
  WriteProfileInt(_T("Options"), s, m_EmuSettings[nWhich].bFriendly);
//  s.Format(_T("%s_emu_divider"), lpszName);
//  WriteProfileInt(_T("Options"), s, m_EmuSettings[nWhich].nDivider);
}

/***************************************************************************/

void CPSFLabApp::OnDebugEmusettings() 
{
  CEmuSettingsDlg dlg;
  dlg.m_nWhichSettingToStart = PSF_EMUSETTINGS_DEBUG;
  dlg.DoModal();
}

void CPSFLabApp::OnFileNewchoose() 
{
  CNewDlg dlg;

  if(m_nNewPSFVersion != 2) m_nNewPSFVersion = 1;
  dlg.m_nPSF = m_nNewPSFVersion - 1;

  if(dlg.DoModal() != IDOK) return;

  int nVersion = dlg.m_nPSF + 1;
  if(nVersion != 2) nVersion = 1;

  m_nNewPSFVersion = nVersion;

  m_nWorkAreaVersion = nVersion;

  OnFileNew();

}

void CAboutDlg::OnVersiondetails() 
{

	CString s;

  s += _T("Application banner string:\n  ");
  s += (LPCSTR)PSFLabBannerString;
  s += _T("\n");

  s += _T("\n");

  s += _T("Emulation core:\n  ");
  { const char *t = emu_getversion();
    for(; *t; t++) s += (TCHAR)(*t);
  }
  s += _T("\n");

  s += _T("\n");

  s += _T("Utility library:\n  ");
  s += Util::GetVersion();
  s += _T("\n");

  s += _T("\n");

  s += _T("Compression:\n");
  UINT uMethodCount = CPSFCompressor::GetMethodCount();
  for(UINT u = 0; u < uMethodCount; u++) {
    s += _T("  ");
    s += CPSFCompressor::GetMethodVersion(u);
    if(u == CPSFCompressor::GetDefaultMethod()) s += _T(" (default)");
    if(u == CPSFCompressor::GetSafeMethod   ()) s += _T(" (safe)");
    s += _T("\n");
  }

  s += _T("\n");

  s += _T("Decompression:\n  ");
  s += CPSFDecompressor::GetVersion();

  MessageBox(s, _T("Version details"));



}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
  CString s;
  s = (LPCTSTR)PSFLabBannerString;
  s += _T(" Copyright 2003 Neill Corlett");

  m_cBanner.SetWindowText(s);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPSFLabApp::OnFileCompressionoptions() 
{
  CCompressionOptionsDlg dlg;

  dlg.m_uMethod = GetCompressionMethodNumber();

  if(dlg.DoModal() != IDOK) return;

  m_strCompressionMethod = CPSFCompressor::GetMethodVersion(dlg.m_uMethod);

}

UINT CPSFLabApp::GetCompressionMethodNumber()
{
  UINT uMethod = CPSFCompressor::GetDefaultMethod();
  UINT uMethodCount = CPSFCompressor::GetMethodCount();
  for(UINT u = 0; u < uMethodCount; u++) {
    if(!lstrcmpi(CPSFCompressor::GetMethodVersion(u), m_strCompressionMethod)) {
      uMethod = u;
      break;
    }
  }
  return uMethod;
}

void CPSFLabApp::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_nWorkAreaVersion == 1);
}

void CPSFLabApp::OnUpdateEventConsole(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(m_bConsole);
}

void CPSFLabApp::OnUpdateEventEvent(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(!m_bConsole);
}

void CPSFLabApp::OnEventConsole() 
{
  m_bConsole = TRUE;
  CDocument *pDoc = ((CFrameWnd*)::AfxGetMainWnd())->GetActiveDocument();
  if(pDoc) pDoc->UpdateAllViews(NULL);
}

void CPSFLabApp::OnEventEvent() 
{
  m_bConsole = FALSE;
  CDocument *pDoc = ((CFrameWnd*)::AfxGetMainWnd())->GetActiveDocument();
  if(pDoc) pDoc->UpdateAllViews(NULL);
}

void CPSFLabApp::OnFilePsf2path() 
{
  TCHAR something[MAX_PATH];

  strncpy(something, (LPCTSTR)m_strPSF2Path, sizeof(something));
  BOOL b = BrowseForFolder(

AfxGetMainWnd()->GetSafeHwnd(),

 _T("Set PSF2 base path"), something);
  if(!b) return;

  m_strPSF2Path = (LPCTSTR)(something);

//  strncpy(m_sPSF2Path, m_strPSF2Path, MAX_PATH);
//  m_sPSF2Path[MAX_PATH-1]=0;

//  CDocument *pDoc = ((CFrameWnd*)::AfxGetMainWnd())->GetActiveDocument();
//  if(pDoc) {
//    CPSFWorkArea *wa = (CPSFWorkArea*)pDoc;
//    strcpy(wa->m_sPSF2Path, m_sPSF2Path);
//    pDoc->UpdateAllViews(NULL);
//  }


}

void CPSFLabApp::OnUpdateFilePsf2path(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(TRUE);
}
