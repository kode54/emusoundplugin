// MemoryView.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "MemoryView.h"

#include "PatchDlg.h"
#include "GoToDlg.h"
#include "FillDlg.h"
#include "MemBreakpointDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMemoryView

IMPLEMENT_DYNCREATE(CMemoryView, CFancyTextView)

CMemoryView::CMemoryView()
{
}

CMemoryView::~CMemoryView()
{
}


BEGIN_MESSAGE_MAP(CMemoryView, CFancyTextView)
	//{{AFX_MSG_MAP(CMemoryView)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PATCH, OnUpdateEditPatch)
	ON_UPDATE_COMMAND_UI(ID_EDIT_GOTO, OnUpdateEditGoto)
	ON_COMMAND(ID_EDIT_GOTO, OnEditGoto)
	ON_COMMAND(ID_EDIT_PATCH, OnEditPatch)
	ON_UPDATE_COMMAND_UI(ID_EDIT_FILL, OnUpdateEditFill)
	ON_COMMAND(ID_EDIT_FILL, OnEditFill)
	ON_UPDATE_COMMAND_UI(ID_EDIT_BREAKPOINT, OnUpdateEditBreakpoint)
	ON_COMMAND(ID_EDIT_BREAKPOINT, OnEditBreakpoint)
	ON_WM_CONTEXTMENU()
	ON_UPDATE_COMMAND_UI(ID_EDIT_DELETEPATCH, OnUpdateEditDeletepatch)
	ON_COMMAND(ID_EDIT_DELETEPATCH, OnEditDeletepatch)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMemoryView drawing

/////////////////////////////////////////////////////////////////////////////
// CMemoryView diagnostics

#ifdef _DEBUG
void CMemoryView::AssertValid() const
{
	CFancyTextView::AssertValid();
}

void CMemoryView::Dump(CDumpContext& dc) const
{
	CFancyTextView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMemoryView message handlers

int CMemoryView::GetLineCount()
{
  return 0x20000;
}

CPSFWorkArea* CMemoryView::GetWorkArea()
{
  CDocument *pDoc = GetDocument();
  ASSERT(pDoc);
  ASSERT(pDoc->IsKindOf(RUNTIME_CLASS(CPSFWorkArea)));
  return (CPSFWorkArea*)pDoc;
}

COLORREF CMemoryView::GetBlankColor()
{
  return theApp.GetColor(PSFLAB_COLOR_MEM_BG);
}

COLORREF CMemoryView::GetDefaultFGColor()
{
  return theApp.GetColor(PSFLAB_COLOR_MEM_FG);
}

CString CMemoryView::GetLineText(CDWordArray &craFG, CDWordArray &craBG, int nLineIndex)
{
  COLORREF crsetBG                = theApp.GetColor(PSFLAB_COLOR_MEM_BG);
  COLORREF crsetBGPatched         = theApp.GetColor(PSFLAB_COLOR_MEM_BG_PATCHED);
  COLORREF crsetBGBreakpoint      = theApp.GetColor(PSFLAB_COLOR_MEM_BG_BREAKPOINT);
  COLORREF crsetFG                = theApp.GetColor(PSFLAB_COLOR_MEM_FG);
  COLORREF crsetFGBreakpoint      = theApp.GetColor(PSFLAB_COLOR_MEM_FG_BREAKPOINT);
  COLORREF crsetFGCursorHighlight = theApp.GetColor(PSFLAB_COLOR_MEM_FG_CURSOR_HIGHLIGHT);
  COLORREF crsetFGPrintableText   = theApp.GetColor(PSFLAB_COLOR_MEM_FG_PRINTABLE_TEXT);
  COLORREF crsetFGUnprintableText = theApp.GetColor(PSFLAB_COLOR_MEM_FG_UNPRINTABLE_TEXT);

  if(nLineIndex < 0 || nLineIndex >= 0x20000) return _T("");
  TCHAR s[100];
  CPSFWorkArea *wa = GetWorkArea();
  DWORD dwAddress = (nLineIndex*16)|((wa->GetVersion()==1)?0x80000000:0x00000000);
  DWORD w[4];
  BOOL bPatched[4];
  BOOL bBPR[4];
  BOOL bBPW[4];
  int i, j, k;
  for(i = 0; i < 4; i++) {
    w[i] = wa->PeekWord(dwAddress+4*i);
    bPatched[i] = wa->IsWordPatched(dwAddress+4*i);
    bBPR[i] = wa->IsBreakpointOnRead(dwAddress+4*i);
    bBPW[i] = wa->IsBreakpointOnWrite(dwAddress+4*i);
  }
  // 46
  wsprintf(s, _T("%08X: %08X %08X %08X %08X "),
    dwAddress, w[0], w[1], w[2], w[3]
  );
  craFG.SetSize(62);
  craBG.SetSize(62);
  for(k = 0; k < 10; k++) {
    craFG[k] = crsetFG;
    craBG[k] = crsetBG;
  }
  for(i = 0; i < 4; i++) {
    COLORREF crFG = crsetFG;
    COLORREF crBG = crsetBG;
    if(bPatched[i]) crBG = crsetBGPatched;
    if(bBPR[i] || bBPW[i]) {
      crFG = crsetFGBreakpoint;
      crBG = crsetBGBreakpoint;
    }
    for(j = 0; j < 8; j++) {
      craFG[k  ] = crFG;
      craBG[k++] = crBG;
    }
    craFG[k  ] = crsetFG;
    craBG[k++] = crsetBG;
  }
  ASSERT(k==46);
  for(i = 0; i < 16; i++) {
    TCHAR c = (TCHAR)((w[i / 4] >> (8*(i & 3))) & 0xFF);
    if(isprint(c)) {
      s[46 + i] = c;
      craFG[46 + i] = crsetFGPrintableText;
      craBG[46 + i] = crsetBG;
    } else {
      s[46 + i] = _T('X');
      craFG[46 + i] = crsetFGUnprintableText;
      craBG[46 + i] = crsetBG;
    }
  }
  s[62] = 0;
  /*
  ** "Live" highlighting
  */
  if(IsFocused()) {
    CPoint ptAnchor, ptCursor;
    GetSelection(ptAnchor, ptCursor);
    if(ptAnchor == ptCursor && ptCursor.y == nLineIndex) {
      if(ptCursor.x >= 10 && ptCursor.x <= 45) {
        int nLiveCol = (ptCursor.x-10) / 9;
        if(!bBPR[nLiveCol] && !bBPW[nLiveCol]) {
          nLiveCol = (nLiveCol * 9) + 10;
          for(i = 0; i < 8; i++) {
            craFG[nLiveCol + i] = crsetFGCursorHighlight;
          }
        }
      }
    }
  }

  return ((LPCTSTR)(s));
}

int CMemoryView::GetMaxLineCharCount()
{
  return 62;
}

DWORD CMemoryView::GetCurrentlyHighlightedByteCount()
{
  DWORD dwAddress, dwBytes;
  GetCurrentlyHighlightedRange(dwAddress, dwBytes);
  return dwBytes;
}

void CMemoryView::OnUpdateEditPatch(CCmdUI* pCmdUI) {
  if(GetWorkArea()->IsRunning()) { pCmdUI->Enable(FALSE); return; }
  DWORD dwAddress, dwBytes;
  GetCurrentlyHighlightedRange(dwAddress, dwBytes);
  dwAddress &= 0x1FFFFFFC;
  pCmdUI->Enable((int)dwAddress >= 0x0000 && dwAddress < 0x1F000000 && dwBytes == 4);
}

void CMemoryView::OnUpdateEditGoto(CCmdUI* pCmdUI) {
	pCmdUI->Enable(TRUE);
}

void CMemoryView::OnEditGoto() 
{
  CGoToDlg dlgGoTo;
  dlgGoTo.m_dwAddress = GetCurrentlyHighlightedAddress();
  if(dlgGoTo.DoModal() != IDOK) return;
  JumpTo(dlgGoTo.m_dwAddress);
}

void CMemoryView::OnEditPatch() 
{
  DWORD dwAddress, dwBytes;
  GetCurrentlyHighlightedRange(dwAddress, dwBytes);
  CPatchDlg dlgPatch;
  dlgPatch.m_dwAddress = dwAddress;
  dlgPatch.m_dwValue = GetWorkArea()->PeekWord(dlgPatch.m_dwAddress);
  if(dlgPatch.DoModal() != IDOK) return;
  GetWorkArea()->SetPatchWord(dlgPatch.m_dwAddress, dlgPatch.m_dwValue);
  GetDocument()->UpdateAllViews(NULL);
}

void CMemoryView::OnUpdateEditFill(CCmdUI* pCmdUI) {
  if(GetWorkArea()->IsRunning()) { pCmdUI->Enable(FALSE); return; }
  DWORD dwAddress, dwBytes;
  GetCurrentlyHighlightedRange(dwAddress, dwBytes);
  dwAddress &= 0x1FFFFFFC;
  if(dwAddress >= 0x1F000000) { pCmdUI->Enable(FALSE); return; }
  dwAddress &= 0x1FFFFC;
  if((dwAddress + dwBytes) <= 0x0000) { pCmdUI->Enable(FALSE); return; }
  pCmdUI->Enable(TRUE);
}

void CMemoryView::OnEditFill() 
{
  CFillDlg dlgFill;
  GetCurrentlyHighlightedRange(dlgFill.m_dwStartAddress, dlgFill.m_dwEndAddress);
  dlgFill.m_dwEndAddress += dlgFill.m_dwStartAddress;
  dlgFill.m_dwValue = 0;
  if(dlgFill.DoModal() != IDOK) return;
  DWORD d;
  dlgFill.m_dwStartAddress &= 0x1FFFFFFC;
  dlgFill.m_dwEndAddress   &= 0x1FFFFFFC;
  for(d = dlgFill.m_dwStartAddress; d < dlgFill.m_dwEndAddress; d++) {
    GetWorkArea()->SetPatchWord(d, dlgFill.m_dwValue);
  }
  GetDocument()->UpdateAllViews(NULL);
}

void CMemoryView::OnUpdateEditBreakpoint(CCmdUI* pCmdUI) {
  pCmdUI->Enable(!(GetWorkArea()->IsRunning()) && (GetCurrentlyHighlightedByteCount() == 4));
}

void CMemoryView::OnEditBreakpoint() {
  DWORD dwAddress, dwBytes;
  GetCurrentlyHighlightedRange(dwAddress, dwBytes);
  if(dwBytes != 4) return;
  CMemBreakpointDlg dlgMemBreakpoint;
  dlgMemBreakpoint.m_dwAddress = dwAddress;
  dlgMemBreakpoint.m_bRead  = GetWorkArea()->IsBreakpointOnRead (dwAddress);
  dlgMemBreakpoint.m_bWrite = GetWorkArea()->IsBreakpointOnWrite(dwAddress);
  if(dlgMemBreakpoint.DoModal() != IDOK) return;

  GetWorkArea()->SetBreakpointOnRead (dwAddress, dlgMemBreakpoint.m_bRead );
  GetWorkArea()->SetBreakpointOnWrite(dwAddress, dlgMemBreakpoint.m_bWrite);
  GetDocument()->UpdateAllViews(NULL);
}

void CMemoryView::GetHighlightRange(CPoint ptAnchor, CPoint ptCursor, DWORD &dwAddress, DWORD &dwBytes) const
{
  CPoint ptStart, ptEnd;
  if(
    (ptAnchor.y > ptCursor.y) ||
    ((ptAnchor.y == ptCursor.y) && (ptAnchor.x > ptCursor.x))
  ) {
    ptStart = ptCursor;
    ptEnd   = ptAnchor;
  } else {
    ptStart = ptAnchor;
    ptEnd   = ptCursor;
  }

  int nStart, nSel;
  int nAddressColStart, nAddressColEnd;

  /*
  ** Special case for nothing selected
  */
  if(ptStart == ptEnd) {
    nAddressColStart = (ptStart.x - 10) / 9;
    if(nAddressColStart < 0) nAddressColStart = 0;
    if(nAddressColStart > 4) nAddressColStart = 4;
    nStart = 4*ptStart.y + nAddressColStart;
    if(ptStart.x >= 10 && ptStart.x <= 45) {
      nSel = 1;
    } else {
      nSel = 0;
    }
  } else {
    nAddressColStart = (ptStart.x - 9) / 9;
    nAddressColEnd = (ptEnd.x - 2) / 9;
    if(nAddressColStart < 0) nAddressColStart = 0;
    if(nAddressColStart > 4) nAddressColStart = 4;
    if(nAddressColEnd < 0) nAddressColEnd = 0;
    if(nAddressColEnd > 4) nAddressColEnd = 4;
    nStart = 4*ptStart.y + nAddressColStart;
    nSel = nAddressColEnd - nAddressColStart + 4*(ptEnd.y-ptStart.y);
  }
  if(nStart < 0) { nSel += nStart; nStart = 0; }
  if(nSel < 0) nSel = 0;
  if((nStart + nSel) > 0x80000) { nSel = 0x80000 - nStart; }
  if(nSel < 0) nSel = 0;
  dwAddress = 0x80000000|(4*nStart);
  dwBytes = 4 * nSel;
/* 0000000000111111111122222222223333333333444444444455555555556666666666
** 0123456789012345678901234567890123456789012345678901234567890123456789
** 01234567: 01234567 01234567 01234567 01234567 xxxxxxxxxxxxxxxx */
}

void CMemoryView::GetCurrentlyHighlightedRange(DWORD &dwAddress, DWORD &dwBytes)
{
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  GetHighlightRange(ptAnchor, ptCursor, dwAddress, dwBytes);
}

DWORD CMemoryView::GetCurrentlyHighlightedAddress()
{ 
  DWORD dwAddress, dwBytes;
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  GetHighlightRange(ptAnchor, ptCursor, dwAddress, dwBytes);
  return dwAddress;
}

BOOL CMemoryView::AddressToPoint(DWORD dwAddress, CPoint &pt)
{
  dwAddress &= 0x1FFFFFFC;
  if(dwAddress >= 0x1F000000) return FALSE;
  dwAddress &= 0x1FFFFC;
  pt.y = dwAddress / 0x10;
  pt.x = 10 + 9 * ((dwAddress / 4) & 3);
  return TRUE;
}

BOOL CMemoryView::JumpTo(DWORD dwAddress)
{
  CPoint pt;
  if(!AddressToPoint(dwAddress, pt)) return FALSE;
  SetSelection(pt, pt, 25);
  return TRUE;
}

void CMemoryView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
  CMenu Menu;
  if(Menu.LoadMenu(IDR_CONTEXT_MEMORY)) {
    CMenu* pPopup = Menu.GetSubMenu(0);
    my_init_popup(this, pPopup, TRUE);
    pPopup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,this);
  }
}

void CMemoryView::OnUpdateEditDeletepatch(CCmdUI* pCmdUI) 
{
  if(GetWorkArea()->IsRunning()) { pCmdUI->Enable(FALSE); return; }
  DWORD dwAddress, dwBytes;
  GetCurrentlyHighlightedRange(dwAddress, dwBytes);
  if(!dwBytes) { pCmdUI->Enable(FALSE); return; }
  if(dwBytes > 4) { pCmdUI->Enable(TRUE); return; }
  pCmdUI->Enable(GetWorkArea()->IsWordPatched(dwAddress));
}

void CMemoryView::OnEditDeletepatch() 
{
  CPSFWorkArea *wa = GetWorkArea();
  DWORD dwAddress, dwBytes;
  GetCurrentlyHighlightedRange(dwAddress, dwBytes);
  dwBytes &= ~3;
  while(dwBytes) {
    wa->DeletePatchWord(dwAddress);
    dwAddress += 4;
    dwBytes -= 4;
  }
  wa->UpdateAllViews(NULL);
}
