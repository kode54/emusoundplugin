// EventView.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "EventView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEventView

IMPLEMENT_DYNCREATE(CEventView, CFancyTextView)

CEventView::CEventView()
{
}

CEventView::~CEventView()
{
}


BEGIN_MESSAGE_MAP(CEventView, CFancyTextView)
	//{{AFX_MSG_MAP(CEventView)
	ON_UPDATE_COMMAND_UI(ID_EDIT_JUMP, OnUpdateEditJump)
	ON_COMMAND(ID_EDIT_JUMP, OnEditJump)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventView drawing

/////////////////////////////////////////////////////////////////////////////
// CEventView diagnostics

#ifdef _DEBUG
void CEventView::AssertValid() const
{
	CFancyTextView::AssertValid();
}

void CEventView::Dump(CDumpContext& dc) const
{
	CFancyTextView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CEventView message handlers

int CEventView::GetLineCount()
{
  if(((CPSFLabApp*)(AfxGetApp()))->m_bConsole) {
    int nLineCount = GetWorkArea()->console.GetLineCount();
    return nLineCount;
  } else {
    int nLineCount = GetWorkArea()->GetEventLog()->GetLineCount();
    //TRACE("event line count %d\n", nLineCount);
    return nLineCount;
  }
}

COLORREF CEventView::GetBlankColor()
{
  return theApp.GetColor(PSFLAB_COLOR_EVENT_BG);
}

CString CEventView::GetLineText(CDWordArray &craFG, CDWordArray &craBG, int nLineIndex)
{
  if(((CPSFLabApp*)(AfxGetApp()))->m_bConsole) {
    COLORREF crBG          = GetBlankColor();
    COLORREF crFGNormal    = GetDefaultFGColor();
    CString s = GetWorkArea()->console.GetLine(nLineIndex);
    int l = lstrlen(s);
    craFG.SetSize(l);
    craBG.SetSize(l);
    int i;
    for(i = 0; i < l; i++) {
      craFG[i] = crFGNormal;
      craBG[i] = crBG;
    }
    return s;
  }

  CString s = GetWorkArea()->GetEventLog()->Get(nLineIndex);
  if(s.IsEmpty()) return s;

  LPCTSTR src = s;

  CString t;

  INT64 time = 0;
  while(*src && isspace(*src)) src++;
  while(*src && isdigit(*src)) {
    time *= 10;
    time += (*src) - _T('0');
    src++;
  }
  if(*src == _T(':')) src++;
  if(*src == _T(' ')) src++;

  double dtime = (double)time;

  int nFormat = theApp.GetEventTimeFormat();
  switch(nFormat) {
  case PSF_EVENT_TIME_FORMAT_NONE:
    break;
  case PSF_EVENT_TIME_FORMAT_CYCLES:
    t.Format(_T("%.0f cycles: "), dtime);
    break;
  case PSF_EVENT_TIME_FORMAT_FRAMES:
    t.Format(_T("%.5f frames: "), dtime * (60.0 / 33868800.0));
    break;
  case PSF_EVENT_TIME_FORMAT_SAMPLES:
    t.Format(_T("%.0f samples: "), dtime * (1.0 / 768.0));
    break;
  case PSF_EVENT_TIME_FORMAT_SECONDS:
    t.Format(_T("%.5f sec: "), dtime * (1.0 / 33868800.0));
    break;
  }
  t += src;

  Colorize((LPCTSTR)t, craFG, craBG);
  return t;
}

CPSFWorkArea* CEventView::GetWorkArea()
{
  CDocument *pDoc = GetDocument();
  ASSERT(pDoc->IsKindOf(RUNTIME_CLASS(CPSFWorkArea)));
  return (CPSFWorkArea*)pDoc;
}

void CEventView::JumpToBottom()
{
  CPoint ptAnchor, ptCursor;
  ptCursor.y = GetLineCount();
  ptCursor.x = 0;
  ptAnchor = ptCursor;
  SetSelection(ptAnchor, ptCursor);
}

int CEventView::GetMaxLineCharCount()
{
  return 100;
}

COLORREF CEventView::GetDefaultFGColor()
{
  return theApp.GetColor(PSFLAB_COLOR_EVENT_FG);
}

void CEventView::Colorize(LPCTSTR s, CDWordArray &craFG, CDWordArray &craBG)
{
  int l = lstrlen(s);
  craFG.SetSize(l);
  craBG.SetSize(l);

  COLORREF crBG          = GetBlankColor();
  COLORREF crFGNormal    = GetDefaultFGColor();
  COLORREF crFGEventName = theApp.GetColor(PSFLAB_COLOR_EVENT_FG_EVENT_TYPE);
  COLORREF crFGPC        = theApp.GetColor(PSFLAB_COLOR_EVENT_FG_ADDRESS);

  int i;
  BOOL bEvNameStarted = FALSE;
  BOOL bEvNameEnded = FALSE;
  int nPCLeft = 0;
  for(i = 0; i < l; i++) {
    craBG[i] = crBG;
    if(i >= 3 &&
      s[i - 3] == _T('p') &&
      s[i - 2] == _T('c') &&
      s[i - 1] == _T('=') &&
      s[i - 0] == _T('0') &&
      s[i + 1] == _T('x')
    ) nPCLeft = 10;
    if(bEvNameStarted) {
      if(s[i] == _T('(')) bEvNameEnded = TRUE;
    } else {
      if(i >= 1 && s[i - 1] == _T(')')) bEvNameStarted = TRUE;
    }
    COLORREF crFG = crFGNormal;
    if(bEvNameStarted && !bEvNameEnded) {
      crFG = crFGEventName;
    }
    if(nPCLeft) {
      crFG = crFGPC;
      nPCLeft--;
    }
    craFG[i] = crFG;
  }

}

DWORD CEventView::GetPCFromLine(LPCTSTR lpszText)
{
  if(!lpszText) return 0;
  for(; *lpszText; lpszText++) {
    if(
      (lpszText[0] == _T('p')) &&
      (lpszText[1] == _T('c')) &&
      (lpszText[2] == _T('=')) &&
      (lpszText[3] == _T('0')) &&
      (lpszText[4] == _T('x'))
    ) {
      lpszText += 5;
      DWORD dwPC = 0;
      for(int i = 0; i < 8; i++) {
        int digit = lpszText[i];
             if(digit >= _T('0') && digit <= _T('9')) { digit = (digit     ) - _T('0'); }
        else if(digit >= _T('a') && digit <= _T('f')) { digit = (digit + 10) - _T('a'); }
        else if(digit >= _T('A') && digit <= _T('F')) { digit = (digit + 10) - _T('A'); }
        else { digit = 0; }
        dwPC = (dwPC * 16) + digit;
      }
      return dwPC;
    }
  }
  return 0;
}

void CEventView::OnUpdateEditJump(CCmdUI* pCmdUI) 
{
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y != ptCursor.y) { pCmdUI->Enable(FALSE); return; }
  if(ptCursor.y < 0 || ptCursor.y >= GetLineCount()) { pCmdUI->Enable(FALSE); return; }
  pCmdUI->Enable(TRUE);
}

void CEventView::OnEditJump() 
{
  CPoint ptAnchor, ptCursor;
  GetSelection(ptAnchor, ptCursor);
  if(ptAnchor.y != ptCursor.y) return;
  if(ptCursor.y < 0 || ptCursor.y >= GetLineCount()) return;
  CDWordArray craFG, craBG;
  CString str = GetLineText(craFG, craBG, ptCursor.y);
  DWORD dwPC = GetPCFromLine((LPCTSTR)str);
  GetWorkArea()->CodeJumpTo(dwPC);
}

void CEventView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	OnEditJump();
	CFancyTextView::OnLButtonDblClk(nFlags, point);
}

void CEventView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
  CMenu Menu;
  if(Menu.LoadMenu(IDR_CONTEXT_EVENT)) {
    CMenu* pPopup = Menu.GetSubMenu(0);
    my_init_popup(this, pPopup, TRUE);
    pPopup->SetDefaultItem(ID_EDIT_JUMP);
    pPopup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,this);
  }
}
