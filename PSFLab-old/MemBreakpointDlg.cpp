// MemBreakpointDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "MemBreakpointDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMemBreakpointDlg dialog


CMemBreakpointDlg::CMemBreakpointDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMemBreakpointDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMemBreakpointDlg)
	m_bRead = FALSE;
	m_bWrite = FALSE;
	//}}AFX_DATA_INIT
}


void CMemBreakpointDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMemBreakpointDlg)
	DDX_Control(pDX, IDC_ADDRESS, m_cAddress);
	DDX_Check(pDX, IDC_READ, m_bRead);
	DDX_Check(pDX, IDC_WRITE, m_bWrite);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMemBreakpointDlg, CDialog)
	//{{AFX_MSG_MAP(CMemBreakpointDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMemBreakpointDlg message handlers

BOOL CMemBreakpointDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
  CString str;
  str.Format(_T("0x%08X"), m_dwAddress);
  m_cAddress.SetWindowText((LPCTSTR)str);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
