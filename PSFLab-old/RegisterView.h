#if !defined(AFX_REGISTERVIEW_H__F780DE5F_BE42_4F61_9218_C16568F56A2D__INCLUDED_)
#define AFX_REGISTERVIEW_H__F780DE5F_BE42_4F61_9218_C16568F56A2D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegisterView.h : header file
//

#include "FancyTextView.h"
#include "PSFWorkArea.h"

/////////////////////////////////////////////////////////////////////////////
// CRegisterView view

class CRegisterView : public CFancyTextView
{
protected:
	CRegisterView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CRegisterView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegisterView)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CRegisterView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	virtual COLORREF GetDefaultFGColor();
	virtual COLORREF GetBlankColor();
	virtual int GetMaxLineCharCount();
	virtual CString GetLineText(CDWordArray &craFG, CDWordArray &craBG, int nLineIndex);
	virtual int GetLineCount();
	//{{AFX_MSG(CRegisterView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void Colorize(LPTSTR s, CDWordArray& craFG, CDWordArray& craBG);
	CPSFWorkArea* GetWorkArea();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGISTERVIEW_H__F780DE5F_BE42_4F61_9218_C16568F56A2D__INCLUDED_)
