#if !defined(AFX_COMPRESSIONOPTIONSDLG_H__8D87D652_DC49_4915_938F_5524EB3D77AC__INCLUDED_)
#define AFX_COMPRESSIONOPTIONSDLG_H__8D87D652_DC49_4915_938F_5524EB3D77AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CompressionOptionsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCompressionOptionsDlg dialog

class CCompressionOptionsDlg : public CDialog
{
// Construction
public:
	UINT m_uMethod;
	CCompressionOptionsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCompressionOptionsDlg)
	enum { IDD = IDD_COMPRESSIONOPTIONS };
	CListBox	m_cMethodList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCompressionOptionsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCompressionOptionsDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPRESSIONOPTIONSDLG_H__8D87D652_DC49_4915_938F_5524EB3D77AC__INCLUDED_)
