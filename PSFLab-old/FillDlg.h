#if !defined(AFX_FILLDLG_H__4B4C73FC_D2CA_419E_BC2F_A78E3EFB9C1C__INCLUDED_)
#define AFX_FILLDLG_H__4B4C73FC_D2CA_419E_BC2F_A78E3EFB9C1C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FillDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFillDlg dialog

class CFillDlg : public CDialog
{
// Construction
public:
	CFillDlg(CWnd* pParent = NULL);   // standard constructor

	DWORD m_dwStartAddress;
	DWORD m_dwEndAddress;
	DWORD m_dwValue;

// Dialog Data
	//{{AFX_DATA(CFillDlg)
	enum { IDD = IDD_FILL };
	CEdit	m_cValue;
	CEdit	m_cStartAddress;
	CEdit	m_cEndAddress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFillDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFillDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILLDLG_H__4B4C73FC_D2CA_419E_BC2F_A78E3EFB9C1C__INCLUDED_)
