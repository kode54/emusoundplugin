#if !defined(AFX_MEMBREAKPOINTDLG_H__97E28D4D_2548_493A_A266_8F713A58F7B9__INCLUDED_)
#define AFX_MEMBREAKPOINTDLG_H__97E28D4D_2548_493A_A266_8F713A58F7B9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MemBreakpointDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMemBreakpointDlg dialog

class CMemBreakpointDlg : public CDialog
{
// Construction
public:
	DWORD m_dwAddress;
	CMemBreakpointDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMemBreakpointDlg)
	enum { IDD = IDD_MEMBREAKPOINT };
	CEdit	m_cAddress;
	BOOL	m_bRead;
	BOOL	m_bWrite;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMemBreakpointDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMemBreakpointDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MEMBREAKPOINTDLG_H__97E28D4D_2548_493A_A266_8F713A58F7B9__INCLUDED_)
