// ColorsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "ColorsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorsDlg dialog


CColorsDlg::CColorsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CColorsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CColorsDlg)
	//}}AFX_DATA_INIT
}


void CColorsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CColorsDlg)
	DDX_Control(pDX, IDC_COLORNUM, m_cColorNum);
	DDX_Control(pDX, IDC_CHANGE, m_cChange);
	DDX_Control(pDX, IDC_COLORLIST, m_cColorList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CColorsDlg, CDialog)
	//{{AFX_MSG_MAP(CColorsDlg)
	ON_BN_CLICKED(IDC_CHANGE, OnChange)
	ON_LBN_SELCHANGE(IDC_COLORLIST, OnSelchangeColorlist)
	ON_BN_CLICKED(IDC_DEFAULTS, OnDefaults)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorsDlg message handlers

void CColorsDlg::OnChange() 
{
  int nIndex = m_cColorList.GetCurSel();
  if(nIndex == LB_ERR) return;
  int nColorIndex = m_cColorList.GetItemData(nIndex);
  if(!(nColorIndex >= 0 && nColorIndex < PSFLAB_COLOR_TOTAL)) return;

  COLORREF cr = theApp.GetColor(nColorIndex);

	CColorDialog dlgColor;

  dlgColor.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
  dlgColor.m_cc.rgbResult = cr;

  if(dlgColor.DoModal() != IDOK) return;

  theApp.SetColor(nColorIndex, dlgColor.m_cc.rgbResult);
  AfxGetMainWnd()->Invalidate(FALSE);

  OnSelchangeColorlist();
}

BOOL CColorsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	m_dwaOldColors.SetSize(PSFLAB_COLOR_TOTAL);
  int i;
  for(i = 0; i < PSFLAB_COLOR_TOTAL; i++) {
    int nNewIndex = m_cColorList.AddString(theApp.GetColorName(i));
    m_cColorList.SetItemData(nNewIndex, i);
    m_dwaOldColors[i] = theApp.GetColor(i);
  }

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CColorsDlg::OnSelchangeColorlist() 
{
  int nIndex = m_cColorList.GetCurSel();
  if(nIndex == LB_ERR) {
    m_cChange.EnableWindow(FALSE);
    m_cColorNum.SetWindowText(_T(""));
  } else {
    int nColorIndex = m_cColorList.GetItemData(nIndex);
    if(nColorIndex >= 0 && nColorIndex < PSFLAB_COLOR_TOTAL) {
      COLORREF cr = theApp.GetColor(nColorIndex);
      TCHAR str[32];
      wsprintf(str, _T("#%02X%02X%02X"),
        GetRValue(cr),
        GetGValue(cr),
        GetBValue(cr)
      );
      m_cColorNum.SetWindowText((LPCTSTR)str);
      m_cChange.SetColor(cr);
      m_cChange.EnableWindow(TRUE);
    } else {
      m_cColorNum.SetWindowText(_T(""));
      m_cChange.EnableWindow(FALSE);
    }
  }
}

void CColorsDlg::OnCancel() 
{
  /*
  ** Restore old colors
  */
  for(int i = 0; i < PSFLAB_COLOR_TOTAL; i++) {
    theApp.SetColor(i, m_dwaOldColors[i]);
  }
  AfxGetMainWnd()->Invalidate(FALSE);
	CDialog::OnCancel();
}

void CColorsDlg::OnDefaults() 
{
  theApp.SetDefaultColors();
  AfxGetMainWnd()->Invalidate(FALSE);
  OnSelchangeColorlist();
}
