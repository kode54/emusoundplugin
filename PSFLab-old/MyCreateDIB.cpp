#include "stdafx.h"
#include "MyCreateDIB.h"

//
// Easy function to create a DIB section
//
HBITMAP MyCreateDIB(
  int w,
  int h,
  int bpp,
  RGBQUAD *palette,
  LPVOID *dib_bits
) {
  if(dib_bits) *dib_bits = NULL;
  int palette_colors;
  if(bpp <= 8) {
    palette_colors = 1 << bpp;
  } else {
    palette_colors = 0;
  }
  // create bmi structure
  BITMAPINFO *bmi;
  DWORD bmisize = sizeof(BITMAPINFOHEADER) + palette_colors * sizeof(RGBQUAD);
  bmi = (BITMAPINFO*)malloc(bmisize);
  if(!bmi) return NULL;
  memset(bmi, 0, bmisize);
  // load relevant header info
  bmi->bmiHeader.biSize = sizeof(bmi->bmiHeader);
  bmi->bmiHeader.biPlanes = 1;
  bmi->bmiHeader.biBitCount = bpp;
  bmi->bmiHeader.biCompression = BI_RGB;
  bmi->bmiHeader.biSizeImage = 0; // only needed for compressed
  bmi->bmiHeader.biXPelsPerMeter = 2834;
  bmi->bmiHeader.biYPelsPerMeter = 2834;
  bmi->bmiHeader.biClrUsed = palette_colors;
  // load the palette
  if(palette && palette_colors) {
    memcpy((RGBQUAD*)(bmi->bmiColors), palette, palette_colors * sizeof(RGBQUAD));
  }
  // create bitmap
  bmi->bmiHeader.biWidth  = w;
  bmi->bmiHeader.biHeight = h;
  LPVOID my_dib_bits = NULL;
  HBITMAP hbm = ::CreateDIBSection(NULL, bmi, DIB_RGB_COLORS, (VOID**)(&my_dib_bits), NULL, 0);
  if(hbm && dib_bits) *dib_bits = my_dib_bits;
  free(bmi);
  return hbm;
}
