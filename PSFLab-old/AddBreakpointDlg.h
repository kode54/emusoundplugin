#if !defined(AFX_ADDBREAKPOINTDLG_H__849B92B9_6E26_47D7_BBAE_DB9027F14922__INCLUDED_)
#define AFX_ADDBREAKPOINTDLG_H__849B92B9_6E26_47D7_BBAE_DB9027F14922__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddBreakpointDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAddBreakpointDlg dialog

class CAddBreakpointDlg : public CDialog
{
// Construction
public:
	DWORD m_dwAddress;
	CAddBreakpointDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAddBreakpointDlg)
	enum { IDD = IDD_ADDBREAKPOINT };
	CEdit	m_cAddress;
	BOOL	m_bExecute;
	BOOL	m_bRead;
	BOOL	m_bWrite;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddBreakpointDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAddBreakpointDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDBREAKPOINTDLG_H__849B92B9_6E26_47D7_BBAE_DB9027F14922__INCLUDED_)
