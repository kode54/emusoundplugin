#if !defined(AFX_SUBSPLITTERWND_H__63B17484_B538_4824_B81C_E761F51E2FAE__INCLUDED_)
#define AFX_SUBSPLITTERWND_H__63B17484_B538_4824_B81C_E761F51E2FAE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SubSplitterWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSubSplitterWnd window

//
// okay, by making this CView-derived, apparently we avoid
// some memory leaks. MFC works in mysterious ways.
//

class CSubSplitterWnd : public CView
{
// Construction
public:
	CSubSplitterWnd();
  DECLARE_DYNCREATE(CSubSplitterWnd);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSubSplitterWnd)
	protected:
	virtual void OnDraw(CDC* pDC);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	CWnd* GetPane(int row, int col);
	BOOL CreateStatic(int nRows, int nCols);
	CSplitterWnd sub;
	BOOL sub_created;
	virtual ~CSubSplitterWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSubSplitterWnd)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SUBSPLITTERWND_H__63B17484_B538_4824_B81C_E761F51E2FAE__INCLUDED_)
