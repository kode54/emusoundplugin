// EmuSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "EmuSettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEmuSettingsDlg dialog


CEmuSettingsDlg::CEmuSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEmuSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEmuSettingsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CEmuSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEmuSettingsDlg)
	DDX_Control(pDX, IDC_SETTING_LIST, m_cSettingList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEmuSettingsDlg, CDialog)
	//{{AFX_MSG_MAP(CEmuSettingsDlg)
	ON_LBN_SELCHANGE(IDC_SETTING_LIST, OnSelchangeSettingList)
	ON_BN_CLICKED(IDC_HARSH, OnHarsh)
	ON_BN_CLICKED(IDC_FRIENDLY, OnFriendly)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEmuSettingsDlg message handlers

BOOL CEmuSettingsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  int nIndex;

  nIndex = m_cSettingList.AddString(_T("Audit"));
  m_cSettingList.SetItemData(nIndex, PSF_EMUSETTINGS_AUDIT);

  nIndex = m_cSettingList.AddString(_T("Debug"));
  m_cSettingList.SetItemData(nIndex, PSF_EMUSETTINGS_DEBUG);

  nIndex = m_cSettingList.AddString(_T("Play"));
  m_cSettingList.SetItemData(nIndex, PSF_EMUSETTINGS_PLAY);

  int i;
  for(i = 0; i < PSF_EMUSETTINGS_MAX; i++) {
    theApp.GetEmuSettings(i, m_es[i]);
  }

  SetSettingType(m_nWhichSettingToStart);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEmuSettingsDlg::OnOK() 
{
  int i;
  for(i = 0; i < PSF_EMUSETTINGS_MAX; i++) {
    theApp.SetEmuSettings(i, m_es[i]);
  }

	CDialog::OnOK();
}

void CEmuSettingsDlg::UpdateInterface()
{
  int nIndex = GetSettingType();
  if(nIndex >= 0) {
    GetDlgItem(IDC_HARSH   )->EnableWindow(TRUE);
    GetDlgItem(IDC_FRIENDLY)->EnableWindow(TRUE);
    CheckDlgButton(IDC_HARSH   , (!m_es[nIndex].bFriendly) ? BST_CHECKED : BST_UNCHECKED);
    CheckDlgButton(IDC_FRIENDLY, ( m_es[nIndex].bFriendly) ? BST_CHECKED : BST_UNCHECKED);
  } else {
    CheckDlgButton(IDC_HARSH   , BST_UNCHECKED);
    CheckDlgButton(IDC_FRIENDLY, BST_UNCHECKED);
    GetDlgItem(IDC_HARSH   )->EnableWindow(FALSE);
    GetDlgItem(IDC_FRIENDLY)->EnableWindow(FALSE);
  }
}

void CEmuSettingsDlg::OnSelchangeSettingList() 
{
	// TODO: Add your control notification handler code here
	UpdateInterface();
}

int CEmuSettingsDlg::GetSettingType()
{
  int nIndex = m_cSettingList.GetCurSel();
  if(nIndex >= 0) {
    nIndex = m_cSettingList.GetItemData(nIndex);
  }
  if(nIndex < 0 || nIndex >= PSF_EMUSETTINGS_MAX) nIndex = -1;
  return nIndex;
}

void CEmuSettingsDlg::OnHarsh() 
{
  int nIndex = GetSettingType();
  if(nIndex < 0) return;
  m_es[nIndex].bFriendly = FALSE;
}

void CEmuSettingsDlg::OnFriendly() 
{
  int nIndex = GetSettingType();
  if(nIndex < 0) return;
  m_es[nIndex].bFriendly = TRUE;
}

void CEmuSettingsDlg::SetSettingType(int nWhich)
{
  int nCount = m_cSettingList.GetCount();
  int i;
  for(i = 0; i < nCount; i++) {
    int nData = m_cSettingList.GetItemData(i);
    if(nData == nWhich) {
      m_cSettingList.SetCurSel(i);
      break;
    }
  }
  UpdateInterface();
}
