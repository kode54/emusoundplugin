#if !defined(AFX_EXESETTINGSDLG_H__5B04B318_A765_4EE0_A936_C49C3AD5165B__INCLUDED_)
#define AFX_EXESETTINGSDLG_H__5B04B318_A765_4EE0_A936_C49C3AD5165B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExeSettingsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CExeSettingsDlg dialog

class CExeSettingsDlg : public CDialog
{
// Construction
public:
	DWORD m_dwSP;
	DWORD m_dwPC;
	DWORD m_dwTextSize;
	DWORD m_dwTextStart;
	CExeSettingsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CExeSettingsDlg)
	enum { IDD = IDD_EXESETTINGS };
	CEdit	m_cTextSize;
	CEdit	m_cPC;
	CEdit	m_cSP;
	CEdit	m_cTextStart;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExeSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CExeSettingsDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXESETTINGSDLG_H__5B04B318_A765_4EE0_A936_C49C3AD5165B__INCLUDED_)
