// SelectRangeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "SelectRangeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectRangeDlg dialog


CSelectRangeDlg::CSelectRangeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectRangeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectRangeDlg)
	//}}AFX_DATA_INIT
}


void CSelectRangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectRangeDlg)
	DDX_Control(pDX, IDC_STARTADDRESS, m_cStartAddress);
	DDX_Control(pDX, IDC_ENDADDRESS, m_cEndAddress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectRangeDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectRangeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectRangeDlg message handlers

void CSelectRangeDlg::OnOK() 
{
  CString s;
  m_cStartAddress.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwStartAddress, this, IDC_STARTADDRESS, _T("start address"))) return;
  m_cEndAddress.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwEndAddress  , this, IDC_ENDADDRESS  , _T("end address"))) return;
	CDialog::OnOK();
}

BOOL CSelectRangeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString s;
  s.Format(_T("0x%08X"), m_dwStartAddress);
  m_cStartAddress.SetWindowText(s);
  s.Format(_T("0x%08X"), m_dwEndAddress);
  m_cEndAddress.SetWindowText(s);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
