// BreakpointsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "BreakpointsDlg.h"
#include "AddBreakpointDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBreakpointsDlg dialog


CBreakpointsDlg::CBreakpointsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBreakpointsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBreakpointsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBreakpointsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBreakpointsDlg)
	DDX_Control(pDX, IDC_BREAKPOINTS, m_cBreakpoints);
	DDX_Control(pDX, IDOK, m_cOK);
	DDX_Control(pDX, IDC_SELECTALL, m_cSelectAll);
	DDX_Control(pDX, IDC_REMOVE, m_cRemove);
	DDX_Control(pDX, IDC_ADD, m_cAdd);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBreakpointsDlg, CDialog)
	//{{AFX_MSG_MAP(CBreakpointsDlg)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_SELECTALL, OnSelectall)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_LBN_DBLCLK(IDC_BREAKPOINTS, OnDblclkBreakpoints)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBreakpointsDlg message handlers

/////////////////////////////////////////////////////////////////////////////

void CBreakpointsDlg::UpdateButtons()
{
  int sc = m_cBreakpoints.GetSelCount();
  m_cOK.EnableWindow(sc == 1);
  m_cRemove.EnableWindow(sc > 0);
  m_cSelectAll.EnableWindow(m_cBreakpoints.GetCount() > 0);
}

/////////////////////////////////////////////////////////////////////////////

BOOL CBreakpointsDlg::PreTranslateMessage(MSG* pMsg) 
{
	UpdateButtons();
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CBreakpointsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  ASSERT(m_pWorkArea);
  int i;
  int s = m_adwBreakpoints.GetSize();
  for(i = 0; i < s; i++) {
    DWORD dwAddress = m_adwBreakpoints.GetAt(i);
    if(m_pWorkArea->IsBreakpointOnExecute(dwAddress)) AddType(dwAddress, 0);
    if(m_pWorkArea->IsBreakpointOnRead   (dwAddress)) AddType(dwAddress, 1);
    if(m_pWorkArea->IsBreakpointOnWrite  (dwAddress)) AddType(dwAddress, 2);
  }
  m_adwBreakpoints.SetSize(0);

  UpdateButtons();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBreakpointsDlg::AddType(DWORD dwAddress, int nType)
{
  static const LPCTSTR alpszTypes[4] = {
    _T("Execute"),
    _T("Read"),
    _T("Write"),
    _T("")
  };
  nType &= 3;
  CString s;
  s.Format(_T("0x%08X: %s"), dwAddress, alpszTypes[nType]);
  int nIndex = m_cBreakpoints.AddString((LPCTSTR)s);
  m_cBreakpoints.SetItemData(nIndex, (dwAddress & 0xFFFFFFFC) | nType);
}

void CBreakpointsDlg::OnOK() 
{
  int nIndex = GetSinglySelectedIndex();
  if(nIndex >= 0) {
    DWORD dwAddress = m_cBreakpoints.GetItemData(nIndex);
    int nType = dwAddress & 3;
    dwAddress &= 0xFFFFFFFC;
    switch(nType) {
    case 0:
      m_pWorkArea->CodeJumpTo(dwAddress);
      break;
    case 1:
    case 2:
      m_pWorkArea->MemoryJumpTo(dwAddress);
      break;
    }
	  CDialog::OnOK();
  }
}

void CBreakpointsDlg::OnRemove() 
{
  while(m_cBreakpoints.GetSelCount()) {
    int nIndex = 0;
    m_cBreakpoints.GetSelItems(1, &nIndex);
    DWORD dwAddress = m_cBreakpoints.GetItemData(nIndex);
    int nType = dwAddress & 3;
    dwAddress &= 0xFFFFFFFC;
    switch(nType) {
    case 0: m_pWorkArea->SetBreakpointOnExecute(dwAddress, FALSE); break;
    case 1: m_pWorkArea->SetBreakpointOnRead   (dwAddress, FALSE); break;
    case 2: m_pWorkArea->SetBreakpointOnWrite  (dwAddress, FALSE); break;
    }
    m_cBreakpoints.DeleteString(nIndex);
  }
  m_pWorkArea->UpdateAllViews(NULL);
}

int CBreakpointsDlg::GetSinglySelectedIndex()
{
  int nSel = -1;
  if(m_cBreakpoints.GetSelCount() == 1) {
    int nSet = m_cBreakpoints.GetSelItems(1, &nSel);
    if(nSet != 1) nSel = -1;
  }
  return nSel;  
}

void CBreakpointsDlg::OnSelectall() 
{
  int nLast = m_cBreakpoints.GetCount();
  if(nLast < 1) return;
  m_cBreakpoints.SelItemRange(TRUE, 0, nLast);
}

void CBreakpointsDlg::OnAdd() 
{
  CAddBreakpointDlg dlgAddBreakpoint;

  if(dlgAddBreakpoint.DoModal() != IDOK) return;


  DWORD dwAddress = dlgAddBreakpoint.m_dwAddress;
  dwAddress &= 0x1FFFFFFC;
  if(dwAddress < 0x1F000000) {
    dwAddress = 0x80000000 + (dwAddress & 0x1FFFFC);
  } else {
    dwAddress = 0xBFC00000 + (dwAddress &  0x7FFFC);
  }
  if(dlgAddBreakpoint.m_bExecute && !m_pWorkArea->IsBreakpointOnExecute(dwAddress)) { m_pWorkArea->SetBreakpointOnExecute(dwAddress, TRUE); AddType(dwAddress, 0); }
  if(dlgAddBreakpoint.m_bRead    && !m_pWorkArea->IsBreakpointOnRead   (dwAddress)) { m_pWorkArea->SetBreakpointOnRead   (dwAddress, TRUE); AddType(dwAddress, 1); }
  if(dlgAddBreakpoint.m_bWrite   && !m_pWorkArea->IsBreakpointOnWrite  (dwAddress)) { m_pWorkArea->SetBreakpointOnWrite  (dwAddress, TRUE); AddType(dwAddress, 2); }


}

void CBreakpointsDlg::OnDblclkBreakpoints() 
{
  OnOK();
}
