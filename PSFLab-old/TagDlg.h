#if !defined(AFX_TAGDLG_H__310EEE98_E39D_490E_9892_9416D14CA00F__INCLUDED_)
#define AFX_TAGDLG_H__310EEE98_E39D_490E_9892_9416D14CA00F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TagDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTagDlg dialog

class CTagDlg : public CDialog
{
// Construction
public:
	CTagDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTagDlg)
	enum { IDD = IDD_TAG };
	CString	m_strTag;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTagDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTagDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TAGDLG_H__310EEE98_E39D_490E_9892_9416D14CA00F__INCLUDED_)
