// AddBreakpointDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "AddBreakpointDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddBreakpointDlg dialog


CAddBreakpointDlg::CAddBreakpointDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddBreakpointDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAddBreakpointDlg)
	m_bExecute = FALSE;
	m_bRead = FALSE;
	m_bWrite = FALSE;
	//}}AFX_DATA_INIT
}


void CAddBreakpointDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddBreakpointDlg)
	DDX_Control(pDX, IDC_ADDRESS, m_cAddress);
	DDX_Check(pDX, IDC_EXECUTE, m_bExecute);
	DDX_Check(pDX, IDC_READ, m_bRead);
	DDX_Check(pDX, IDC_WRITE, m_bWrite);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddBreakpointDlg, CDialog)
	//{{AFX_MSG_MAP(CAddBreakpointDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddBreakpointDlg message handlers

void CAddBreakpointDlg::OnOK() 
{
  CString s;
  m_cAddress.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwAddress, this, IDC_ADDRESS, _T("address"))) return;
	CDialog::OnOK();
}
