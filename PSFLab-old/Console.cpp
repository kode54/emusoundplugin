// Console.cpp: implementation of the CConsole class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "psflab.h"
#include "Console.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConsole::CConsole()
{

}

CConsole::~CConsole()
{

}

void CConsole::Clear()
{
  cs.Lock();
  saData.SetSize(0);
  cs.Unlock();
}

void CConsole::AddChar(TCHAR c)
{
  cs.Lock();
  int l = saData.GetSize();
  if(!l) { 
    saData.Add("");
    l = 1;
  }
  switch(c) {
  case 0xD:
    break;
  case 0xA:
    saData.Add("");
    break;
  default:
    saData[l - 1] = saData[l - 1] + c;
//TRACE("something '%s'\n",(LPCTSTR)(saData.GetAt(l-1)));
    break;
  }
  cs.Unlock();
}

int CConsole::GetLineCount()
{
  cs.Lock();
  int l = saData.GetSize();
  cs.Unlock();
  return l;
}

CString CConsole::GetLine(int nLine)
{
  cs.Lock();
  int l = saData.GetSize();
  if(!l) { 
    saData.Add("");
    l = 1;
  }
  CString s;
  if(nLine >= 0 && nLine < l) {
    s = saData.GetAt(nLine);
  }
  cs.Unlock();
  return s;
}
