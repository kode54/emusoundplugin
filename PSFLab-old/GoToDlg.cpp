// GoToDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "GoToDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGoToDlg dialog


CGoToDlg::CGoToDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGoToDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGoToDlg)
	//}}AFX_DATA_INIT
}


void CGoToDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGoToDlg)
	DDX_Control(pDX, IDC_ADDRESS, m_cAddress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGoToDlg, CDialog)
	//{{AFX_MSG_MAP(CGoToDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGoToDlg message handlers

BOOL CGoToDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString s;
  s.Format(_T("0x%08X"), m_dwAddress);
  m_cAddress.SetWindowText(s);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGoToDlg::OnOK() 
{
  CString s;
  m_cAddress.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwAddress, this, IDC_ADDRESS, _T("address"))) return;
	CDialog::OnOK();
}
