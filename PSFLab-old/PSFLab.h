// PSFLab.h : main header file for the PSFLAB application
//

#if !defined(AFX_PSFLAB_H__898D99FE_4A61_456D_B44C_3940AEE44715__INCLUDED_)
#define AFX_PSFLAB_H__898D99FE_4A61_456D_B44C_3940AEE44715__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////

#define PSF_EVENT_TIME_FORMAT_NONE    (0)
#define PSF_EVENT_TIME_FORMAT_CYCLES  (1)
#define PSF_EVENT_TIME_FORMAT_FRAMES  (2)
#define PSF_EVENT_TIME_FORMAT_SAMPLES (3)
#define PSF_EVENT_TIME_FORMAT_SECONDS (4)

/////////////////////////////////////////////////////////////////////////////

#define PSF_EMUSETTINGS_AUDIT (0)
#define PSF_EMUSETTINGS_DEBUG (1)
#define PSF_EMUSETTINGS_PLAY  (2)
#define PSF_EMUSETTINGS_MAX   (3)

/////////////////////////////////////////////////////////////////////////////

enum {
  PSFLAB_COLOR_CODE_BG = 0,
  PSFLAB_COLOR_CODE_BG_CURRENT_LINE,
  PSFLAB_COLOR_CODE_BG_PATCHED,
  PSFLAB_COLOR_CODE_BG_BREAKPOINT,
  PSFLAB_COLOR_CODE_FG,
  PSFLAB_COLOR_CODE_FG_CURRENT_LINE,
  PSFLAB_COLOR_CODE_FG_BREAKPOINT,
  PSFLAB_COLOR_CODE_FG_CURSOR_HIGHLIGHT,
  PSFLAB_COLOR_CODE_FG_IMMEDIATE,
  PSFLAB_COLOR_CODE_FG_ADDRESS,
  PSFLAB_COLOR_CODE_FG_PUNCTUATION,
  PSFLAB_COLOR_CODE_FG_UNKNOWN,
  PSFLAB_COLOR_REG_BG,
  PSFLAB_COLOR_REG_BG_MODIFIED,
  PSFLAB_COLOR_REG_FG,
  PSFLAB_COLOR_REG_FG_MODIFIED,
  PSFLAB_COLOR_REG_FG_PUNCTUATION,
  PSFLAB_COLOR_MEM_BG,
  PSFLAB_COLOR_MEM_BG_PATCHED,
  PSFLAB_COLOR_MEM_BG_BREAKPOINT,
  PSFLAB_COLOR_MEM_FG,
  PSFLAB_COLOR_MEM_FG_BREAKPOINT,
  PSFLAB_COLOR_MEM_FG_CURSOR_HIGHLIGHT,
  PSFLAB_COLOR_MEM_FG_PRINTABLE_TEXT,
  PSFLAB_COLOR_MEM_FG_UNPRINTABLE_TEXT,
  PSFLAB_COLOR_CALL_BG,
  PSFLAB_COLOR_CALL_FG,
  PSFLAB_COLOR_CALL_FG_ADDRESS,
  PSFLAB_COLOR_CALL_FG_EXCEPTION,
  PSFLAB_COLOR_CALL_FG_CAUSE,
  PSFLAB_COLOR_EVENT_BG,
  PSFLAB_COLOR_EVENT_FG,
  PSFLAB_COLOR_EVENT_FG_ADDRESS,
  PSFLAB_COLOR_EVENT_FG_EVENT_TYPE,
  PSFLAB_COLOR_TOTAL
};

/////////////////////////////////////////////////////////////////////////////

BOOL ParseNumber(LPCTSTR str, DWORD &dwValue);
BOOL ParseDlgValue(LPCTSTR lpszString, DWORD &dwValue, CDialog *dlg, int nID, LPCTSTR lpszValueName);

/////////////////////////////////////////////////////////////////////////////

void my_init_popup(
  CCmdTarget* pTarg,
  CMenu* pMenu,
  BOOL bAutoMenuEnable
);

/////////////////////////////////////////////////////////////////////////////

struct SPSFEmuSettings {
  BOOL bFriendly;
//  int nDivider;
};

/////////////////////////////////////////////////////////////////////////////
// CPSFLabApp:
// See PSFLab.cpp for the implementation of this class
//

class CPSFLabApp : public CWinApp
{
public:
	CPSFLabApp();
//	virtual ~CPSFLabApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPSFLabApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	CString m_strPSF2Path;
//  TCHAR m_sPSF2Path[MAX_PATH];
	BOOL m_bConsole;
	UINT GetCompressionMethodNumber();
	int m_nNewPSFVersion;
  int m_nWorkAreaVersion;
	void SetEmuSettings(int nWhich, SPSFEmuSettings &settings);
	void GetEmuSettings(int nWhich, SPSFEmuSettings &settings);
	void SetAuditOptions(BOOL bParanoid, int nParanoidBytes);
	void GetAuditOptions(BOOL &bParanoid, int &nParanoidBytes);
	void SetDefaultColors();
	void SetColor(int nIndex, COLORREF cr);
	LPCTSTR GetColorName(int nIndex) const;
	const LOGFONT* GetLogFont() const;
	CRect m_rWindowSize;
	int m_nSplitterCallStackHeight;
	int m_nSplitterRegisterHeight;
	int m_nSplitterMemoryWidth;
	int m_nSplitterCodeWidth;
	COLORREF GetColor(int nIndex) const;
	int GetEventTimeFormat();
	DWORD GetEventMask();
	//{{AFX_MSG(CPSFLabApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFileNew();
	afx_msg void OnFileOpen();
	afx_msg void OnUpdateEventDma(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEventIntr(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEventLh(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEventSh(CCmdUI* pCmdUI);
	afx_msg void OnEventSh();
	afx_msg void OnEventLh();
	afx_msg void OnEventIntr();
	afx_msg void OnEventDma();
	afx_msg void OnEventAll();
	afx_msg void OnEventNone();
	afx_msg void OnUpdateTimeCycles(CCmdUI* pCmdUI);
	afx_msg void OnTimeCycles();
	afx_msg void OnUpdateTimeFrames(CCmdUI* pCmdUI);
	afx_msg void OnTimeFrames();
	afx_msg void OnUpdateTimeNone(CCmdUI* pCmdUI);
	afx_msg void OnTimeNone();
	afx_msg void OnUpdateTimeSamples(CCmdUI* pCmdUI);
	afx_msg void OnTimeSamples();
	afx_msg void OnUpdateTimeSeconds(CCmdUI* pCmdUI);
	afx_msg void OnTimeSeconds();
	afx_msg void OnViewFont();
	afx_msg void OnViewColors();
	afx_msg void OnDebugEmusettings();
	afx_msg void OnFileNewchoose();
	afx_msg void OnFileCompressionoptions();
	afx_msg void OnEventVio();
	afx_msg void OnUpdateEventVio(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEventConsole(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEventEvent(CCmdUI* pCmdUI);
	afx_msg void OnEventConsole();
	afx_msg void OnEventEvent();
	afx_msg void OnFilePsf2path();
	afx_msg void OnUpdateFilePsf2path(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void SaveEmuSettingOptions(int nWhich, LPCTSTR lpszName);
	void LoadEmuSettingOptions(int nWhich, LPCTSTR lpszName);
  SPSFEmuSettings m_EmuSettings[PSF_EMUSETTINGS_MAX];
  CString m_strCompressionMethod;
	int m_nAuditParanoidBytes;
	BOOL m_bAuditParanoid;
	void SaveWindowPositions();
	void LoadWindowPositions();
	void SaveOptions();
	void LoadOptions();
	void SaveFont();
	void LoadFont();
	void SaveColors();
	void LoadColors();
	COLORREF m_acrColors[PSFLAB_COLOR_TOTAL];
	LOGFONT m_lfBaseFont;
	void UpdateEventTimeFormat();
	int m_nEventTimeFormat;
	DWORD m_dwEventMask;
};

/////////////////////////////////////////////////////////////////////////////

extern CPSFLabApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSFLAB_H__898D99FE_4A61_456D_B44C_3940AEE44715__INCLUDED_)
