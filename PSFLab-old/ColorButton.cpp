// ColorButton.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "ColorButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorButton

CColorButton::CColorButton()
{
  m_crCurrent = RGB(0x00,0x00,0x00);
}

CColorButton::~CColorButton()
{
}


BEGIN_MESSAGE_MAP(CColorButton, CButton)
	//{{AFX_MSG_MAP(CColorButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorButton message handlers

void CColorButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your code to draw the specified item
//	TRACE("drawitem\n");

  CDC *dc = CDC::FromHandle(lpDrawItemStruct->hDC); // Get a CDC we can use
  CRect r(lpDrawItemStruct->rcItem);
  CPoint fpos(0,0);			   // Focus position

  // Dimensions of a border line
  // We use ::GetSystemMetrics so we work on
  // hi-res displays
  CSize border(::GetSystemMetrics(SM_CXBORDER), ::GetSystemMetrics(SM_CYBORDER));

  // Offset amount if button-down
  CPoint baseOffset(border.cx, border.cy);
  // Save the DC for later restoration
  int saved = dc->SaveDC();
  // Gray out stock image?
  BOOL grayout = FALSE;
  CPoint bltStart(0, 0);		   // Nominal start of BLT

  // The first thing we do is deflate our useful area by the width of a
  // frame, which is twice a border. 
  r.InflateRect(-2 * border.cx, -2 * border.cy);

  CSize clientArea(r.Width(), r.Height());

  CSize image;

  image.cx = r.Width() - baseOffset.x;
  image.cy = r.Height() - baseOffset.y;
  fpos.x = 2 * border.cx;
  fpos.y = 2 * border.cy;

  CRect focus(fpos.x,
  fpos.y,
  fpos.x + image.cx,
  fpos.y + image.cy);

  // For visual effect, if the button is down, shift the image over and
  // down to make it look "pushed".
  CPoint useOffset(0, 0);
  if(lpDrawItemStruct->itemState & ODS_SELECTED)
	{ /* down */
	 useOffset = baseOffset;
	} /* down */

  // Draw the traditional pushbutton edge using DrawEdge
  if(lpDrawItemStruct->itemState & ODS_SELECTED) {
    /* down */
    dc->DrawEdge(&lpDrawItemStruct->rcItem, EDGE_SUNKEN, BF_RECT | BF_MIDDLE | BF_SOFT);
	} else {
    /* up */
    dc->DrawEdge(&lpDrawItemStruct->rcItem, EDGE_RAISED, BF_RECT | BF_MIDDLE | BF_SOFT);
	}

  if(IsWindowEnabled()) {
    dc->FillSolidRect(r, m_crCurrent);
  }

  // Adjust the focus position and image position by shifting them
  // right-and-down by the desired offset
  focus += useOffset;

  if(lpDrawItemStruct->itemState & ODS_FOCUS) {
    ::DrawFocusRect(lpDrawItemStruct->hDC, &focus);
  }

  dc->RestoreDC(saved);
}

void CColorButton::SetColor(COLORREF cr)
{
  m_crCurrent = cr;
  if(IsWindowEnabled()) Invalidate(FALSE);
}
