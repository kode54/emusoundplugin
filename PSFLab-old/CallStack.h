// CallStack.h: interface for the CCallStack class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CALLSTACK_H__206FECD9_F5D6_4E2D_81F0_01D00E001761__INCLUDED_)
#define AFX_CALLSTACK_H__206FECD9_F5D6_4E2D_81F0_01D00E001761__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

struct SCallStackEntry {
  BOOL bException;
  DWORD dwInvokedAddress;
  DWORD dwCause;
  DWORD dwReturnPC;
  DWORD dwReturnSP;
  DWORD dwArg[4];
};

class CCallStack  
{
public:
	void CheckForReturn(DWORD dwPC, DWORD dwSP);
	CCallStack();
	virtual ~CCallStack();

	void CopyFrom(CCallStack &src);
	void Add(SCallStackEntry &e);
	void Pop();
	void Clear();

	int GetSize();
	void GetEntry(int nIndex, SCallStackEntry& e);

private:
  CCriticalSection m_cs;

  CArray <SCallStackEntry, SCallStackEntry&> m_aCalls;
};

#endif // !defined(AFX_CALLSTACK_H__206FECD9_F5D6_4E2D_81F0_01D00E001761__INCLUDED_)
