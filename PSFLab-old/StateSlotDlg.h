#if !defined(AFX_STATESLOTDLG_H__FCAFF568_386F_4F66_AF22_4E1A4E0F572F__INCLUDED_)
#define AFX_STATESLOTDLG_H__FCAFF568_386F_4F66_AF22_4E1A4E0F572F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StateSlotDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStateSlotDlg dialog

class CStateSlotDlg : public CDialog
{
// Construction
public:
	CStateSlotDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CStateSlotDlg)
	enum { IDD = IDD_STATESLOT };
	int		m_nSlot;
	CString	m_strInUse0;
	CString	m_strInUse1;
	CString	m_strInUse2;
	CString	m_strInUse3;
	CString	m_strInUse4;
	CString	m_strInUse5;
	CString	m_strInUse6;
	CString	m_strInUse7;
	CString	m_strInUse8;
	CString	m_strInUse9;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStateSlotDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStateSlotDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATESLOTDLG_H__FCAFF568_386F_4F66_AF22_4E1A4E0F572F__INCLUDED_)
