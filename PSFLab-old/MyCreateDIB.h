//
// Easy function to create a DIB section
//
HBITMAP MyCreateDIB(
  int w,
  int h,
  int bpp,
  RGBQUAD *palette,
  LPVOID *dib_bits
);
