-----------------------------------------------------------------------------
PSFLab v1.04
PSF emulator/editor/debugger
Copyright (C) 2003 Neill Corlett
-----------------------------------------------------------------------------

Introduction
------------

PSFLab is a GUI frontend to my Playstation emulation core.  It allows you to
create PSF files, or test and debug existing PSF files that are under
construction.

Although PSFLab doesn't support the PSF2 file format yet, you can still use
it to debug PSF2s by specifying a base directory containing all the virtual
files.  Be sure to read the PSF format specification for more details on how
PSF2 works.

-----------------------------------------------------------------------------

Basic Interface
---------------

PSFLab presents you with a "work area" for debugging, with five information
panes:

+---------------+-----------------+-----------------+
|               |                 |                 |
|  Code         |  Registers      |  Call Stack     |
|               |                 |                 |
|               +-----------------+-----------------+
|               |                 |                 |
|               |  Memory         |  Event Log      |
|               |                 |                 |
|               |                 |                 |
|               |                 |                 |
+---------------+-----------------+-----------------+

Code:  Shows code.  The current program counter line is highlighted.  You can
  move around in it freely.  Pressing ENTER while the cursor is over an
  address will navigate to that address (I call this "jump" even though it
  doesn't actually cause the program to jump there).  Pressing ESC will go
  back.

Registers:  Shows the current state of the R3000 CPU registers.

Memory:  A view of memory divided into 32-bit words.  Since the Playstation
  uses a little-endian CPU, this obviously means the bytes don't appear in
  order.

Call Stack:  Shows a list of functions that are currently being called.
  Addresses get added here automatically on a jal/jalr/bgezal/bltzal
  instruction or an exception, and removed when the return address is
  reached and the stack pointer matches.

  Double-clicking on a line in the Call Stack will jump to the corresponding
  function in the Code pane.

Event Log:  Shows a list of events, such as register accesses, interrupts,
  or DMA transfers (actually those are the only events supported right now).
  You can enable or disable logging of a particular event using the Event
  menu.

  Double-clicking on a line in the Event Log will jump to the corresponding
  program counter address in the Code pane.

Optionally, the Event Log window can be used to display console output.

-----------------------------------------------------------------------------

Commands
--------

File commands (File menu)
-------------------------

New, Open, Save:  Self-explanatory.  You can create work areas for either
  PSF (Playstation) or PSF2 (Playstation 2 IOP).

Set PSF2 base path:  Lets you select a directory which PSFLab will use for
  loading virtual files in PS2 IOP mode.

Compression Options:  Lets you select a compression method to use when saving
  PSF files.

Import PS-X EXE:  Replaces the current work area with the contents of a given
  EXE, and restarts the debugger.

Import Binary Data:  Adds the contents of a binary file to a given address in
  the current work area.  (such as, but not limited to, SEQ or VAB data)

Export PS-X EXE:  Saves the current work area into a PS-X EXE file.


Navigation commands (Edit menu)
-------------------------------

Jump:  If the cursor is over an address in the Code, Call Stack, or Event Log
  panes, this will cause the Code pane to jump to that address.

Go to:  Moves the cursor to a specified address in the current pane (Code or
  Memory).

Return:  Returns to the previous address from the last Jump or Go To command.

Empty Stack:  Forgets all previous addresses from Jump or Go To commands.

Select Range:  Highlights a range of given addresses (useful for copying).

Current Line:  Moves the cursor to the current program counter line.


Editing commands (Edit menu)
----------------------------

Patch:  Patches a 32-bit word in memory.

Fill:  Fills a range of 32-bit words in memory with a given value.

Assemble:  Assembles an instruction at the current Code pane address and
  saves the resulting 32-bit word as a patch.

Delete Patch:  Deletes a 32-bit word patch at the current cursor location.
  (The data then reverts to the original data from the loaded PSF or EXE.)
  You can also select a range of addresses and delete many patches at once.

Toggle Breakpoint:  Toggles a breakpoint at the current address in either the
  Code or Memory panes.  For memory, you can also choose whether the
  breakpoint is to be triggered on a read or write operation.

Breakpoints:  Shows you a dialog where you can manage all breakpoints in the
  current work area.


Information commands (Edit menu)
-------------------------------------

Tag:  Lets you edit the PSF tag (same as the Raw Mode button in the Winamp
  plugin).

EXE Settings:  Lets you edit the EXE text segment size and initial PC/SP.


Remove Unused Data (Edit menu)
------------------------------

Applicable to PS1 mode only.

This command executes the current work area in a special auditing mode that
determines which data is actually used.  You can start and stop the audit at
any time and watch its progress.  When you're done, clicking Apply Changes
will zero out all unused bytes and adjust the EXE text section size.

Of course, you should make sure that you audit the work area for an ample
length of time (preferably a bit longer than the duration of the song).
Otherwise, you will end up eliminating useful data.

You can avoid zeroing out data within a specified proximity of other used
data by checking the "I'm paranoid" box and entering in a byte range into
the edit box.  Note:  Currently you can't enter hex values here.


Debugging commands (Debug menu)
-------------------------------

Go, Restart, Break, Step Into, Step Over, Step Out, Run to Cursor:
  These behave the same as the Visual C++ debugger commands.

Run to Specific Line:
  Runs until the program counter hits an address you specify.

Run to Interrupt:
  Runs until an interrupt occurs.


State commands (Debug menu)
---------------------------

Save State:  Saves the current debug state into a TEMPORARY memory slot.
  These states are destroyed if you create a new work area or open a
  different PSF file.

Select Slot:  Selects a memory slot to use for saved states (1-10).

Load State:  Loads the debug state from the current slot.


Playing commands (Debug menu)
-----------------------------

Play:  Attempts to play the music from the current work area.  This command
  doesn't disturb the current debug state.  This is useful for quickly
  testing whether a piece of music driver code works.

Pause:  Pauses playback.

Stop:  Stops playback.


Emulation Settings (Debug menu)
-------------------------------

Adjusts settings related to the emulation core.  Currently you can only
choose the compatibility mode (harsh or friendly).


Event menu
----------

Show Events:  Sets the Event Log pane to show emulated hardware events.

Show Console Output:  Sets the Event Log pane to show console output.  For
  debugging PSF2s, this may be more helpful.

The rest of the options in this menu let you enable or disable certain events
or choose the time format that the Event Log pane uses.


View menu
---------

Enable or disable the toolbar or status bar, or change the font and color
scheme.

-----------------------------------------------------------------------------

Known Bugs, Issues, Unhappy Things
----------------------------------

* PSF2 support is a complete bolted-on hack.  I know it is.  I plan on fixing
  this, but it's at least adequate for now.  It was good enough for my rips.

* Don't do anything that's not aligned to a 32-bit boundary (importing data,
  filling, etc.)  It probably won't work the way you were expecting.

* The same emulation issues from Highly Experimental also apply here.

-----------------------------------------------------------------------------

Terms of Use
------------

PSFLab is freeware and may be distributed freely as long as it is not
modified, and this documentation is included and not modified either.
"Distributed freely" means no money, goods, or services may be charged or
solicited for PSFLab or any media or software package containing it.

Anyone using PSFLab does so at their own risk.  No warranty is expressed or
implied.

-----------------------------------------------------------------------------

PSFLab includes code from the following projects:

* zlib (C) 1995-2002 Jean-loup Gailly and Mark Adler (LGPL)
  http://www.gzip.org/zlib/

* 7-Zip (C) 1999-2003 Igor Pavlov (LGPL)
  http://www.7-zip.org/

* AdvanceCOMP (C) 2002 Andrea Mazzoleni (LGPL)
  http://advancemame.sourceforge.net/

-----------------------------------------------------------------------------

Thanks to
---------

* TNSe, whose NSFTool was an excellent utility and a great inspiration.
* All the people who followed my progress on this without getting fed up.
* ProtoCat, for no reason at all.

-----------------------------------------------------------------------------

Where to find me
----------------

email: corlett@lfx.org
web:   http://lfx.org/~corlett/

-----------------------------------------------------------------------------
