#if !defined(AFX_PATCHDLG_H__3C05FB32_EEE0_42AF_91D9_473D6B9C7AC3__INCLUDED_)
#define AFX_PATCHDLG_H__3C05FB32_EEE0_42AF_91D9_473D6B9C7AC3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PatchDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPatchDlg dialog

class CPatchDlg : public CDialog
{
// Construction
public:
	DWORD m_dwValue;
	DWORD m_dwAddress;
	CPatchDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPatchDlg)
	enum { IDD = IDD_PATCH };
	CEdit	m_cValue;
	CEdit	m_cAddress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPatchDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPatchDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PATCHDLG_H__3C05FB32_EEE0_42AF_91D9_473D6B9C7AC3__INCLUDED_)
