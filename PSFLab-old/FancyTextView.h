#if !defined(AFX_FANCYTEXTVIEW_H__7CCFD56F_17C4_46C8_8FE8_E2C7B255816B__INCLUDED_)
#define AFX_FANCYTEXTVIEW_H__7CCFD56F_17C4_46C8_8FE8_E2C7B255816B__INCLUDED_

#include "FindHistory.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FancyTextView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFancyTextView view

class CFancyTextView : public CView
{
public:
  void GetLogFont(LOGFONT &lf);
  void SetLogFont(const LOGFONT &lf);

protected:
	CFancyTextView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CFancyTextView)

  // should be overridden by subclasses
	virtual int            GetLineCount();
	virtual int            GetMaxLineCharCount();
	virtual CString        GetLineText(CDWordArray& craFG, CDWordArray& craBG, int nLineIndex);
	virtual COLORREF       GetDefaultFGColor();
  virtual COLORREF       GetBlankColor();

  // usable by subclasses but can't be overridden
  void GetSelection(CPoint& ptAnchor, CPoint& ptCursor) const;
	void SetSelection(const CPoint &ptAnchor, const CPoint &ptCursor, int nScrollPercent = -1);
	BOOL IsFocused() const { return m_bFocused; }

private:
	void Find(LPCTSTR lpszPattern, BOOL bBackwards, BOOL bMatchCase, BOOL bMatchSpaces);
	CFindHistory m_findhistory;

	int FindStringWithOptions(LPCTSTR lpszLine, LPCTSTR lpszPattern, BOOL bBackwards, BOOL bMatchCase, BOOL bMatchSpaces);
	int FindString(LPCTSTR lpszLine, LPCTSTR lpszPattern, BOOL bBackwards) const;
	COLORREF SelColor(COLORREF cr);
	void EnforcePointOrder(CPoint &ptStart, CPoint &ptEnd);
	CString GetSelectedText();
	void InvalidateSelectedLines();
	CString GetTextRange(CPoint ptStart, CPoint ptEnd);
	void DrawSingleLine(CDC *pdc, const CRect &rect, int nLineIndex);
	CFont *GetFont();
	CPoint ToEnd(CPoint pt);
	CPoint ToStart(CPoint pt);
	CPoint ToLineEnd(CPoint pt);
	CPoint ToLineStart(CPoint pt);
	CPoint PageDown(CPoint pt);
	CPoint BoundsCheck(CPoint pt);
	CPoint PageUp(CPoint pt);
	CPoint LineDown(CPoint pt);
	CPoint LineUp(CPoint pt);
	void MoveCursor(CPoint point, BOOL bSelect);
	void AdjustTextPoint(CPoint& point);
	void ScrollToLine(int nNewTopLine, BOOL bNoSmoothScroll = FALSE, BOOL bTrackScrollBar = TRUE);
	void ScrollToChar(int nNewOffsetChar, BOOL bNoSmoothScroll = FALSE, BOOL bTrackScrollBar = TRUE);
	int GetScreenChars();
	int GetScreenLines();
  CPoint CharToLeft (CPoint pt);
  CPoint CharToRight(CPoint pt);

	void EnsureVisible(CPoint pt, int nScrollPercent);
	void HideCursor();
	void ShowCursor();
	CPoint ClientToText(const CPoint& point);
	void InvalidateLines(int nLine1, int nLine2);
	BOOL m_bVertScrollBarLocked, m_bHorzScrollBarLocked;
	CPoint m_ptDraggedTextBegin, m_ptDraggedTextEnd;

	CFont* m_pFont;
  LOGFONT m_lfBaseFont;

	BOOL m_bDragSelection;
	UINT m_nDragSelTimer;

	void RecalcHorzScrollBar(BOOL bPositionOnly = FALSE);
	void RecalcVertScrollBar(BOOL bPositionOnly = FALSE);
	int m_nScreenChars;
	int m_nScreenLines;
	CPoint TextToClient(const CPoint& pt);
	BOOL m_bCursorHidden;
	void UpdateCaret();
	BOOL IsInsideSelection(CPoint ptTextPos);
	void ResetView();

	CBitmap *m_pCacheBitmap;
	int m_nLineHeight, m_nCharWidth;
	void CalcLineCharDim();

	BOOL m_bFocused;
	BOOL m_bShowInactiveSelection;

  CPoint m_ptDisplay;
	CPoint m_ptAnchor;
	CPoint m_ptCursor;

	int GetLineHeight();
	int GetCharWidth();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFancyTextView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CFancyTextView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CFancyTextView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditFind(CCmdUI* pCmdUI);
	afx_msg void OnEditFind();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FANCYTEXTVIEW_H__7CCFD56F_17C4_46C8_8FE8_E2C7B255816B__INCLUDED_)
