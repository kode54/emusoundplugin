#if !defined(AFX_BREAKPOINTSDLG_H__A00E56AD_F9B4_4663_B621_744B7F964FD1__INCLUDED_)
#define AFX_BREAKPOINTSDLG_H__A00E56AD_F9B4_4663_B621_744B7F964FD1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BreakpointsDlg.h : header file
//

#include "PSFWorkArea.h"

/////////////////////////////////////////////////////////////////////////////
// CBreakpointsDlg dialog

class CBreakpointsDlg : public CDialog
{
// Construction
public:
	CPSFWorkArea* m_pWorkArea;
	CDWordArray m_adwBreakpoints;
	CBreakpointsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBreakpointsDlg)
	enum { IDD = IDD_BREAKPOINTS };
	CListBox	m_cBreakpoints;
	CButton	m_cOK;
	CButton	m_cSelectAll;
	CButton	m_cRemove;
	CButton	m_cAdd;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBreakpointsDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBreakpointsDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnRemove();
	afx_msg void OnSelectall();
	afx_msg void OnAdd();
	afx_msg void OnDblclkBreakpoints();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int GetSinglySelectedIndex();
	void AddType(DWORD dwAddress, int nType);
	void UpdateButtons();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BREAKPOINTSDLG_H__A00E56AD_F9B4_4663_B621_744B7F964FD1__INCLUDED_)
