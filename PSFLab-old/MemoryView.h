#if !defined(AFX_MEMORYVIEW_H__A407C592_5ABB_4E99_A93D_5C2E5E774335__INCLUDED_)
#define AFX_MEMORYVIEW_H__A407C592_5ABB_4E99_A93D_5C2E5E774335__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MemoryView.h : header file
//

#include "FancyTextView.h"
#include "PSFWorkArea.h"

/////////////////////////////////////////////////////////////////////////////
// CMemoryView view

class CMemoryView : public CFancyTextView
{
protected:
	CMemoryView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CMemoryView)

// Attributes
public:

// Operations
public:
	BOOL JumpTo(DWORD dwAddress);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMemoryView)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CMemoryView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	virtual int GetMaxLineCharCount();
	virtual CString GetLineText(CDWordArray& craFG, CDWordArray& craBG, int nLineIndex);
	virtual COLORREF GetDefaultFGColor();
	virtual COLORREF GetBlankColor();
	virtual int GetLineCount();
	//{{AFX_MSG(CMemoryView)
	afx_msg void OnUpdateEditPatch(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditGoto(CCmdUI* pCmdUI);
	afx_msg void OnEditGoto();
	afx_msg void OnEditPatch();
	afx_msg void OnUpdateEditFill(CCmdUI* pCmdUI);
	afx_msg void OnEditFill();
	afx_msg void OnUpdateEditBreakpoint(CCmdUI* pCmdUI);
	afx_msg void OnEditBreakpoint();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnUpdateEditDeletepatch(CCmdUI* pCmdUI);
	afx_msg void OnEditDeletepatch();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL AddressToPoint(DWORD dwAddress, CPoint& pt);
	DWORD GetCurrentlyHighlightedAddress();
	void GetHighlightRange(CPoint ptAnchor, CPoint ptCursor, DWORD &dwAddress, DWORD &dwBytes) const;
	void GetCurrentlyHighlightedRange(DWORD& dwAddress, DWORD& dwBytes);
	DWORD GetCurrentlyHighlightedByteCount();
	CPSFWorkArea* GetWorkArea();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MEMORYVIEW_H__A407C592_5ABB_4E99_A93D_5C2E5E774335__INCLUDED_)
