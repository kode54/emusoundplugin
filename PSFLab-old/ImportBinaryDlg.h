#if !defined(AFX_IMPORTBINARYDLG_H__4ED0A4BF_9CCF_4DBD_98B9_DFD2FA904080__INCLUDED_)
#define AFX_IMPORTBINARYDLG_H__4ED0A4BF_9CCF_4DBD_98B9_DFD2FA904080__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImportBinaryDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImportBinaryDlg dialog

class CImportBinaryDlg : public CDialog
{
// Construction
public:
	CString m_strPathName;
	DWORD m_dwOffset;
	DWORD m_dwLength;
	DWORD m_dwDestinationAddress;
	CImportBinaryDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CImportBinaryDlg)
	enum { IDD = IDD_IMPORTBINARY };
	CButton	m_cOffset;
	CButton	m_cLength;
	CButton	m_cBeginning;
	CButton	m_cEnd;
	CEdit	m_cOffsetEdit;
	CEdit	m_cLengthEdit;
	CEdit	m_cDestination;
	CEdit	m_cFile;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImportBinaryDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CImportBinaryDlg)
	afx_msg void OnBrowse();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnBeginning();
	afx_msg void OnOffset();
	afx_msg void OnEnd();
	afx_msg void OnLength();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORTBINARYDLG_H__4ED0A4BF_9CCF_4DBD_98B9_DFD2FA904080__INCLUDED_)
