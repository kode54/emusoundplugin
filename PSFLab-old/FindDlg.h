#if !defined(AFX_FINDDLG_H__33F41CC7_B0CC_4430_BA5B_0193BB34F18D__INCLUDED_)
#define AFX_FINDDLG_H__33F41CC7_B0CC_4430_BA5B_0193BB34F18D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FindDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFindDlg dialog

class CFindDlg : public CDialog
{
// Construction
public:
	const CStringArray* m_saHistory;
	CFindDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFindDlg)
	enum { IDD = IDD_FIND };
	CButton	m_cOK;
	CComboBox	m_cFindWhat;
	BOOL	m_bMatchCase;
	int		m_nDir;
	BOOL	m_bMatchSpaces;
	CString	m_strFindWhat;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFindDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFindDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void UpdateOKButton();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FINDDLG_H__33F41CC7_B0CC_4430_BA5B_0193BB34F18D__INCLUDED_)
