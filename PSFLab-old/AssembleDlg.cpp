// AssembleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "AssembleDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAssembleDlg dialog


CAssembleDlg::CAssembleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAssembleDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAssembleDlg)
	m_strAddress = _T("");
	m_strInstruction = _T("");
	//}}AFX_DATA_INIT
}


void CAssembleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAssembleDlg)
	DDX_Text(pDX, IDC_ADDRESS, m_strAddress);
	DDX_Text(pDX, IDC_INSTRUCTION, m_strInstruction);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAssembleDlg, CDialog)
	//{{AFX_MSG_MAP(CAssembleDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAssembleDlg message handlers
