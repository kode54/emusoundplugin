// SubSplitterWnd.cpp : implementation file
//

#include "stdafx.h"
#include "PSFLab.h"
#include "SubSplitterWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CSubSplitterWnd, CView);

/////////////////////////////////////////////////////////////////////////////
// CSubSplitterWnd

CSubSplitterWnd::CSubSplitterWnd()
{
  sub_created = FALSE;
}

CSubSplitterWnd::~CSubSplitterWnd()
{
}


BEGIN_MESSAGE_MAP(CSubSplitterWnd, CView)
	//{{AFX_MSG_MAP(CSubSplitterWnd)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSubSplitterWnd message handlers

BOOL CSubSplitterWnd::CreateStatic(int nRows, int nCols)
{
  BOOL r;
  r = sub.CreateStatic(this, nRows, nCols);
  if(!r) return 0;
  sub_created = TRUE;
  return r;
}

CWnd* CSubSplitterWnd::GetPane(int row, int col)
{
  ASSERT(sub_created);
  return sub.GetPane(row, col);
}

void CSubSplitterWnd::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);
	if(sub_created) sub.SetWindowPos(this, 0, 0, cx, cy, SWP_NOMOVE | SWP_NOZORDER);
}

void CSubSplitterWnd::OnDraw(CDC* pDC) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	
}

BOOL CSubSplitterWnd::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class

  cs.style = WS_CHILD|WS_VISIBLE;
  cs.dwExStyle = 0;
  cs.lpszClass = AfxRegisterWndClass(0);

	return CView::PreCreateWindow(cs);
}
