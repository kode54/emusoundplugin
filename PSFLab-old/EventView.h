#if !defined(AFX_EVENTVIEW_H__B09495BE_EE5B_4D25_9080_0FB3B1E3B5FB__INCLUDED_)
#define AFX_EVENTVIEW_H__B09495BE_EE5B_4D25_9080_0FB3B1E3B5FB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EventView.h : header file
//

#include "FancyTextView.h"
#include "PSFWorkArea.h"

/////////////////////////////////////////////////////////////////////////////
// CEventView view

class CEventView : public CFancyTextView
{
protected:
	CEventView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CEventView)

// Attributes
public:

// Operations
public:
	void JumpToBottom();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEventView)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CEventView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	virtual COLORREF GetDefaultFGColor();
	virtual int GetMaxLineCharCount();
	virtual CString GetLineText(CDWordArray& craFG, CDWordArray& craBG, int nLineIndex);
	virtual COLORREF GetBlankColor();
	virtual int GetLineCount();
	//{{AFX_MSG(CEventView)
	afx_msg void OnUpdateEditJump(CCmdUI* pCmdUI);
	afx_msg void OnEditJump();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	DWORD GetPCFromLine(LPCTSTR lpszText);
	void Colorize(LPCTSTR s, CDWordArray &craFG, CDWordArray &craBG);
	CPSFWorkArea* GetWorkArea();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EVENTVIEW_H__B09495BE_EE5B_4D25_9080_0FB3B1E3B5FB__INCLUDED_)
