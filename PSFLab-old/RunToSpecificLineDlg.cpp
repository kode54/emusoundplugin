// RunToSpecificLineDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psflab.h"
#include "RunToSpecificLineDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRunToSpecificLineDlg dialog


CRunToSpecificLineDlg::CRunToSpecificLineDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRunToSpecificLineDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRunToSpecificLineDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CRunToSpecificLineDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRunToSpecificLineDlg)
	DDX_Control(pDX, IDC_ADDRESS, m_cAddress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRunToSpecificLineDlg, CDialog)
	//{{AFX_MSG_MAP(CRunToSpecificLineDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRunToSpecificLineDlg message handlers

void CRunToSpecificLineDlg::OnOK() 
{
  CString s;
  m_cAddress.GetWindowText(s);
  if(!ParseDlgValue(s, m_dwAddress, this, IDC_ADDRESS, _T("address"))) return;
	CDialog::OnOK();
}

BOOL CRunToSpecificLineDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString s;
  s.Format(_T("0x%08X"), m_dwAddress);
  m_cAddress.SetWindowText(s);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
