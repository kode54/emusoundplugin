#if !defined(AFX_RUNTOSPECIFICLINEDLG_H__B86C4700_9D97_440B_B363_6CAAF9C8BB46__INCLUDED_)
#define AFX_RUNTOSPECIFICLINEDLG_H__B86C4700_9D97_440B_B363_6CAAF9C8BB46__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RunToSpecificLineDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRunToSpecificLineDlg dialog

class CRunToSpecificLineDlg : public CDialog
{
// Construction
public:
	DWORD m_dwAddress;
	CRunToSpecificLineDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRunToSpecificLineDlg)
	enum { IDD = IDD_RUNTOSPECIFICLINE };
	CEdit	m_cAddress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRunToSpecificLineDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRunToSpecificLineDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RUNTOSPECIFICLINEDLG_H__B86C4700_9D97_440B_B363_6CAAF9C8BB46__INCLUDED_)
