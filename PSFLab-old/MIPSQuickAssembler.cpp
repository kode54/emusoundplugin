// MIPSQuickAssembler.cpp: implementation of the CMIPSQuickAssembler class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "psflab.h"
#include "MIPSQuickAssembler.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMIPSQuickAssembler::CMIPSQuickAssembler() { }
CMIPSQuickAssembler::~CMIPSQuickAssembler() { }

/***************************************************************************/

enum {
  TOKEN_NONE,
  TOKEN_REG,
  TOKEN_C0REG,
  TOKEN_LPAREN,
  TOKEN_RPAREN,
  TOKEN_COMMA,
  TOKEN_NUMBER,
  TOKEN_UNKNOWN
};

/***************************************************************************/

LPCTSTR CMIPSQuickAssembler::Scan(LPCTSTR lpszText, DWORD &dwToken, DWORD &dwSubToken)
{
  dwToken = TOKEN_UNKNOWN;
  /*
  ** Skip opening space
  */
  while(*lpszText && isspace(*lpszText)) lpszText++;
  LPCTSTR s = lpszText;
  /*
  ** Token: None
  */
  if(!(*s)) {
    dwToken = TOKEN_NONE;
    return lpszText;
  }
  /*
  ** Token: Register
  */
  if(*s == _T('$')) {
    s++;
    int regnum = 0;
    if(isdigit(*s)) {
      regnum = *(s++) - _T('0');
      if(isdigit(*s)) {
        regnum = (regnum * 10) + (*(s++) - _T('0'));
      }
      if(regnum >= 32) return lpszText;
    } else {
      TCHAR a = 0;
      TCHAR b = 0;
      if(*s) { a = tolower(*s); s++; }
      if(*s) { b = tolower(*s); s++; }
           if(a==_T('a')&&b==_T('t')) { regnum =  1; }
      else if(a==_T('v')&&b==_T('0')) { regnum =  2; }
      else if(a==_T('v')&&b==_T('1')) { regnum =  3; }
      else if(a==_T('a')&&b==_T('0')) { regnum =  4; }
      else if(a==_T('a')&&b==_T('1')) { regnum =  5; }
      else if(a==_T('a')&&b==_T('2')) { regnum =  6; }
      else if(a==_T('a')&&b==_T('3')) { regnum =  7; }
      else if(a==_T('t')&&b==_T('0')) { regnum =  8; }
      else if(a==_T('t')&&b==_T('1')) { regnum =  9; }
      else if(a==_T('t')&&b==_T('2')) { regnum = 10; }
      else if(a==_T('t')&&b==_T('3')) { regnum = 11; }
      else if(a==_T('t')&&b==_T('4')) { regnum = 12; }
      else if(a==_T('t')&&b==_T('5')) { regnum = 13; }
      else if(a==_T('t')&&b==_T('6')) { regnum = 14; }
      else if(a==_T('t')&&b==_T('7')) { regnum = 15; }
      else if(a==_T('s')&&b==_T('0')) { regnum = 16; }
      else if(a==_T('s')&&b==_T('1')) { regnum = 17; }
      else if(a==_T('s')&&b==_T('2')) { regnum = 18; }
      else if(a==_T('s')&&b==_T('3')) { regnum = 19; }
      else if(a==_T('s')&&b==_T('4')) { regnum = 20; }
      else if(a==_T('s')&&b==_T('5')) { regnum = 21; }
      else if(a==_T('s')&&b==_T('6')) { regnum = 22; }
      else if(a==_T('s')&&b==_T('7')) { regnum = 23; }
      else if(a==_T('t')&&b==_T('8')) { regnum = 24; }
      else if(a==_T('t')&&b==_T('9')) { regnum = 25; }
      else if(a==_T('k')&&b==_T('0')) { regnum = 26; }
      else if(a==_T('k')&&b==_T('1')) { regnum = 27; }
      else if(a==_T('g')&&b==_T('p')) { regnum = 28; }
      else if(a==_T('s')&&b==_T('p')) { regnum = 29; }
      else if(a==_T('f')&&b==_T('p')) { regnum = 30; }
      else if(a==_T('r')&&b==_T('a')) { regnum = 31; }
      else { return lpszText; }
    }
    dwToken = TOKEN_REG;
    dwSubToken = regnum;
    return s;
  }
  /*
  ** Token: C0 register
  */
  if(tolower(s[0]) == _T('C') && s[1] == _T('0') && s[2] == _T('_')) {
    s += 3;
    int regnum = 0;
    if(isdigit(*s)) {
      regnum = *(s++) - _T('0');
      if(isdigit(*s)) {
        regnum = (regnum * 10) + (*(s++) - _T('0'));
      }
      if(regnum >= 32) return lpszText;
    } else {
           if(AlphaCompare(s, _T("status"))) { regnum = 12; }
      else if(AlphaCompare(s, _T("cause" ))) { regnum = 13; }
      else if(AlphaCompare(s, _T("epc"   ))) { regnum = 14; }
      else { return lpszText; }
    }
    dwToken = TOKEN_C0REG;
    dwSubToken = regnum;
    return s;
  }
  /*
  ** Token: Various punctuation
  */
  switch(*s) {
  case _T(','): s++; dwToken = TOKEN_COMMA ; return s;
  case _T('('): s++; dwToken = TOKEN_LPAREN; return s;
  case _T(')'): s++; dwToken = TOKEN_RPAREN; return s;
  }
  /*
  ** Token: Number
  */
  if(isdigit(*s) || *s == _T('-')) {
    BOOL bNegative = FALSE;
    if(*s == _T('-')) { bNegative = TRUE; s++; }
    int radix = 10;
    int num = 0;
    if(*s == _T('0')) {
      s++;
      if(tolower(*s) == _T('x')) {
        radix = 16;
        s++;
      } else {
        radix = 8;
      }
    }
    for(;;) {
      TCHAR c = *s;
      if(!c) break;
      if(!isxdigit(c)) break;
      int digit = 0;
           if(c >= _T('0') && c <= _T('9')) { digit =      c - _T('0'); }
      else if(c >= _T('a') && c <= _T('f')) { digit = 10 + c - _T('a'); }
      else if(c >= _T('A') && c <= _T('F')) { digit = 10 + c - _T('A'); }
      else { return lpszText; }
      if(digit >= radix) return lpszText;
      num *= radix;
      num += digit;
      s++;
    }
    if(bNegative) { num = -num; }
    dwToken = TOKEN_NUMBER;
    dwSubToken = num;
    return s;
  }
  /*
  ** Token was unknown
  */
  return lpszText;
}

/***************************************************************************/
/*
** Returns TRUE on match
*/
BOOL CMIPSQuickAssembler::AlphaCompare(LPCTSTR lpszText, LPCTSTR lpszPattern)
{
  for(; *lpszPattern; lpszPattern++, lpszText++) {
    if(!(*lpszText)) return FALSE;
    if(tolower(*lpszText) != tolower(*lpszPattern)) return FALSE;
  }
  /*
  ** The key: The trailing text must be non-alnum
  */
  if(isalnum(*lpszText)) return FALSE;
  return TRUE;
}

/***************************************************************************/

CString CMIPSQuickAssembler::Assemble(DWORD dwPC, LPCTSTR lpszText, DWORD &dwIns)
{
  /*
  ** Skip opening space
  */
  while(*lpszText && isspace(*lpszText)) lpszText++;

  /*
  ** E = D and T
  ** F = D and S
  ** G = T and S
  */

  if(AlphaCompare(lpszText, _T("nop"    ))) return AssemblePatterns(""         , dwPC, lpszText + 3, dwIns, 0x00);

  if(AlphaCompare(lpszText, _T("sll"    ))) return AssemblePatterns("E,H/D,T,H", dwPC, lpszText + 3, dwIns, 0x00);
  if(AlphaCompare(lpszText, _T("srl"    ))) return AssemblePatterns("E,H/D,T,H", dwPC, lpszText + 3, dwIns, 0x02);
  if(AlphaCompare(lpszText, _T("sra"    ))) return AssemblePatterns("E,H/D,T,H", dwPC, lpszText + 3, dwIns, 0x03);
  if(AlphaCompare(lpszText, _T("sllv"   ))) return AssemblePatterns("E,S/D,T,S", dwPC, lpszText + 4, dwIns, 0x04);
  if(AlphaCompare(lpszText, _T("srlv"   ))) return AssemblePatterns("E,S/D,T,S", dwPC, lpszText + 4, dwIns, 0x06);
  if(AlphaCompare(lpszText, _T("srav"   ))) return AssemblePatterns("E,S/D,T,S", dwPC, lpszText + 4, dwIns, 0x07);

  if(AlphaCompare(lpszText, _T("jr"     ))) return AssemblePatterns("S"        , dwPC, lpszText + 2, dwIns, 0x08);
  if(AlphaCompare(lpszText, _T("jalr"   ))) return AssemblePatterns("S/D,S"    , dwPC, lpszText + 4, dwIns, 0x09|(0x1F<<11));

  if(AlphaCompare(lpszText, _T("syscall"))) return AssemblePatterns(""         , dwPC, lpszText + 7, dwIns, 0x0C);

  if(AlphaCompare(lpszText, _T("mfhi"   ))) return AssemblePatterns("D"        , dwPC, lpszText + 4, dwIns, 0x10);
  if(AlphaCompare(lpszText, _T("mthi"   ))) return AssemblePatterns("D"        , dwPC, lpszText + 4, dwIns, 0x11);
  if(AlphaCompare(lpszText, _T("mflo"   ))) return AssemblePatterns("D"        , dwPC, lpszText + 4, dwIns, 0x12);
  if(AlphaCompare(lpszText, _T("mtlo"   ))) return AssemblePatterns("D"        , dwPC, lpszText + 4, dwIns, 0x13);

  if(AlphaCompare(lpszText, _T("mult"   ))) return AssemblePatterns("S,T"      , dwPC, lpszText + 4, dwIns, 0x18);
  if(AlphaCompare(lpszText, _T("multu"  ))) return AssemblePatterns("S,T"      , dwPC, lpszText + 5, dwIns, 0x19);
  if(AlphaCompare(lpszText, _T("div"    ))) return AssemblePatterns("S,T"      , dwPC, lpszText + 3, dwIns, 0x1A);
  if(AlphaCompare(lpszText, _T("divu"   ))) return AssemblePatterns("S,T"      , dwPC, lpszText + 4, dwIns, 0x1B);

  if(AlphaCompare(lpszText, _T("add"    ))) return AssemblePatterns("F,T/D,S,T", dwPC, lpszText + 3, dwIns, 0x20);
  if(AlphaCompare(lpszText, _T("addu"   ))) return AssemblePatterns("F,T/D,S,T", dwPC, lpszText + 4, dwIns, 0x21);
  if(AlphaCompare(lpszText, _T("sub"    ))) return AssemblePatterns("F,T/D,S,T", dwPC, lpszText + 3, dwIns, 0x22);
  if(AlphaCompare(lpszText, _T("subu"   ))) return AssemblePatterns("F,T/D,S,T", dwPC, lpszText + 4, dwIns, 0x23);
  if(AlphaCompare(lpszText, _T("and"    ))) return AssemblePatterns("F,T/D,S,T", dwPC, lpszText + 3, dwIns, 0x24);
  if(AlphaCompare(lpszText, _T("or"     ))) return AssemblePatterns("F,T/D,S,T", dwPC, lpszText + 2, dwIns, 0x25);
  if(AlphaCompare(lpszText, _T("xor"    ))) return AssemblePatterns("F,T/D,S,T", dwPC, lpszText + 3, dwIns, 0x26);
  if(AlphaCompare(lpszText, _T("nor"    ))) return AssemblePatterns("F,T/D,S,T", dwPC, lpszText + 3, dwIns, 0x27);

  if(AlphaCompare(lpszText, _T("slt"    ))) return AssemblePatterns("D,S,T"    , dwPC, lpszText + 3, dwIns, 0x2A);
  if(AlphaCompare(lpszText, _T("sltu"   ))) return AssemblePatterns("D,S,T"    , dwPC, lpszText + 4, dwIns, 0x2B);

  if(AlphaCompare(lpszText, _T("bltz"   ))) return AssemblePatterns("S,B"      , dwPC, lpszText + 4, dwIns, (0x01<<26)|(0x00<<16));
  if(AlphaCompare(lpszText, _T("bgez"   ))) return AssemblePatterns("S,B"      , dwPC, lpszText + 4, dwIns, (0x01<<26)|(0x01<<16));
  if(AlphaCompare(lpszText, _T("bltzal" ))) return AssemblePatterns("S,B"      , dwPC, lpszText + 6, dwIns, (0x01<<26)|(0x10<<16));
  if(AlphaCompare(lpszText, _T("bgezal" ))) return AssemblePatterns("S,B"      , dwPC, lpszText + 6, dwIns, (0x01<<26)|(0x11<<16));

  if(AlphaCompare(lpszText, _T("j"      ))) return AssemblePatterns("J"        , dwPC, lpszText + 1, dwIns, (0x02<<26));
  if(AlphaCompare(lpszText, _T("jal"    ))) return AssemblePatterns("J"        , dwPC, lpszText + 3, dwIns, (0x03<<26));

  if(AlphaCompare(lpszText, _T("beq"    ))) return AssemblePatterns("S,T,B"    , dwPC, lpszText + 3, dwIns, (0x04<<26));
  if(AlphaCompare(lpszText, _T("bne"    ))) return AssemblePatterns("S,T,B"    , dwPC, lpszText + 3, dwIns, (0x05<<26));
  if(AlphaCompare(lpszText, _T("beqz"   ))) return AssemblePatterns("S,B"      , dwPC, lpszText + 4, dwIns, (0x04<<26));
  if(AlphaCompare(lpszText, _T("bnez"   ))) return AssemblePatterns("S,B"      , dwPC, lpszText + 4, dwIns, (0x05<<26));

  if(AlphaCompare(lpszText, _T("blez"   ))) return AssemblePatterns("S,B"      , dwPC, lpszText + 4, dwIns, (0x06<<26));
  if(AlphaCompare(lpszText, _T("bgtz"   ))) return AssemblePatterns("S,B"      , dwPC, lpszText + 4, dwIns, (0x07<<26));

  if(AlphaCompare(lpszText, _T("addi"   ))) return AssemblePatterns("G,I/T,S,I", dwPC, lpszText + 4, dwIns, (0x08<<26));
  if(AlphaCompare(lpszText, _T("addiu"  ))) return AssemblePatterns("G,I/T,S,I", dwPC, lpszText + 5, dwIns, (0x09<<26));
  if(AlphaCompare(lpszText, _T("slti"   ))) return AssemblePatterns("T,S,I"    , dwPC, lpszText + 4, dwIns, (0x0A<<26));
  if(AlphaCompare(lpszText, _T("sltiu"  ))) return AssemblePatterns("T,S,I"    , dwPC, lpszText + 5, dwIns, (0x0B<<26));

  if(AlphaCompare(lpszText, _T("andi"   ))) return AssemblePatterns("G,I/T,S,I", dwPC, lpszText + 4, dwIns, (0x0C<<26));
  if(AlphaCompare(lpszText, _T("ori"    ))) return AssemblePatterns("G,I/T,S,I", dwPC, lpszText + 3, dwIns, (0x0D<<26));
  if(AlphaCompare(lpszText, _T("xori"   ))) return AssemblePatterns("G,I/T,S,I", dwPC, lpszText + 4, dwIns, (0x0E<<26));
  if(AlphaCompare(lpszText, _T("lui"    ))) return AssemblePatterns("T,I"      , dwPC, lpszText + 3, dwIns, (0x0F<<26));

  if(AlphaCompare(lpszText, _T("mfc0"   ))) return AssemblePatterns("T,C"      , dwPC, lpszText + 4, dwIns, (0x10<<26)|(0x00<<21));
  if(AlphaCompare(lpszText, _T("mtc0"   ))) return AssemblePatterns("T,C"      , dwPC, lpszText + 4, dwIns, (0x10<<26)|(0x04<<21));
  if(AlphaCompare(lpszText, _T("rfe"    ))) return AssemblePatterns(""         , dwPC, lpszText + 3, dwIns, (0x10<<26)|(0x10<<21));

  if(AlphaCompare(lpszText, _T("lb"     ))) return AssemblePatterns("T,(S)/T,I(S)", dwPC, lpszText + 2, dwIns, (DWORD)(0x20<<26));
  if(AlphaCompare(lpszText, _T("lh"     ))) return AssemblePatterns("T,(S)/T,I(S)", dwPC, lpszText + 2, dwIns, (DWORD)(0x21<<26));
  if(AlphaCompare(lpszText, _T("lw"     ))) return AssemblePatterns("T,(S)/T,I(S)", dwPC, lpszText + 2, dwIns, (DWORD)(0x23<<26));
  if(AlphaCompare(lpszText, _T("lbu"    ))) return AssemblePatterns("T,(S)/T,I(S)", dwPC, lpszText + 3, dwIns, (DWORD)(0x24<<26));
  if(AlphaCompare(lpszText, _T("lhu"    ))) return AssemblePatterns("T,(S)/T,I(S)", dwPC, lpszText + 3, dwIns, (DWORD)(0x25<<26));
  if(AlphaCompare(lpszText, _T("sb"     ))) return AssemblePatterns("T,(S)/T,I(S)", dwPC, lpszText + 2, dwIns, (DWORD)(0x28<<26));
  if(AlphaCompare(lpszText, _T("sh"     ))) return AssemblePatterns("T,(S)/T,I(S)", dwPC, lpszText + 2, dwIns, (DWORD)(0x29<<26));
  if(AlphaCompare(lpszText, _T("sw"     ))) return AssemblePatterns("T,(S)/T,I(S)", dwPC, lpszText + 2, dwIns, (DWORD)(0x2B<<26));

  /*
  ** Friendly instructions like not, move, neg, negu, li
  ** "li" always expands to a signed addiu - not an ori.
  */
  if(AlphaCompare(lpszText, _T("move"   ))) return AssemblePatterns("D,T"  , dwPC, lpszText + 4, dwIns, 0x21);
  if(AlphaCompare(lpszText, _T("neg"    ))) return AssemblePatterns("E/D,T", dwPC, lpszText + 3, dwIns, 0x22);
  if(AlphaCompare(lpszText, _T("negu"   ))) return AssemblePatterns("E/D,T", dwPC, lpszText + 4, dwIns, 0x23);
  if(AlphaCompare(lpszText, _T("not"    ))) return AssemblePatterns("E/D,T", dwPC, lpszText + 3, dwIns, 0x27);
  if(AlphaCompare(lpszText, _T("li"     ))) return AssemblePatterns("T,I"  , dwPC, lpszText + 2, dwIns, (0x09<<26));

  return _T("Unknown instuction name");
}

/***************************************************************************/

CString CMIPSQuickAssembler::AssemblePatterns(LPCSTR lpszPatterns, DWORD dwPC, LPCTSTR lpszText, DWORD &dwIns, DWORD dwInsTemplate)
{
  CString complaint;
  do {
    dwIns = dwInsTemplate;
    char pattemp[20];
    for(int i = 0;; i++) {
      char c = *lpszPatterns;
      if(!c) break;
      lpszPatterns++;
      if(c == '/') break;
      pattemp[i] = c;
    }
    pattemp[i] = 0;
    complaint = AssemblePattern(pattemp, dwPC, lpszText, dwIns);
    if(complaint.IsEmpty()) break;
  } while(*lpszPatterns);
  return complaint;
}

/***************************************************************************/

CString CMIPSQuickAssembler::AssemblePattern(LPCSTR lpszPattern, DWORD dwPC, LPCTSTR lpszText, DWORD &dwIns)
{
  DWORD dwToken;
  DWORD dwSubToken;
  for(;;) {
    lpszText = Scan(lpszText, dwToken, dwSubToken);
    char p = *lpszPattern++;
    switch(p) {
    case 0:
      if(dwToken != TOKEN_NONE) return _T("Expected ','");
      return _T("");
    case ',':
      if(dwToken != TOKEN_COMMA) return _T("Expected ','");
      break;
    case '(':
      if(dwToken != TOKEN_LPAREN) return _T("Expected '('");
      break;
    case ')':
      if(dwToken != TOKEN_RPAREN) return _T("Expected ')'");
      break;
    /*
    ** S is 21
    ** T is 16
    ** D is 11
    */
    case 'S':
      if(dwToken != TOKEN_REG) return _T("Expected register name");
      ReplaceInsField(dwIns, 21, 5, dwSubToken);
      break;
    case 'T':
      if(dwToken != TOKEN_REG) return _T("Expected register name");
      ReplaceInsField(dwIns, 16, 5, dwSubToken);
      break;
    case 'D':
      if(dwToken != TOKEN_REG) return _T("Expected register name");
      ReplaceInsField(dwIns, 11, 5, dwSubToken);
      break;
    /*
    ** E = D and T
    ** F = D and S
    ** G = T and S
    */
    case 'E':
      if(dwToken != TOKEN_REG) return _T("Expected register name");
      ReplaceInsField(dwIns, 11, 5, dwSubToken);
      ReplaceInsField(dwIns, 16, 5, dwSubToken);
      break;
    case 'F':
      if(dwToken != TOKEN_REG) return _T("Expected register name");
      ReplaceInsField(dwIns, 11, 5, dwSubToken);
      ReplaceInsField(dwIns, 21, 5, dwSubToken);
      break;
    case 'G':
      if(dwToken != TOKEN_REG) return _T("Expected register name");
      ReplaceInsField(dwIns, 16, 5, dwSubToken);
      ReplaceInsField(dwIns, 21, 5, dwSubToken);
      break;
    case 'H':
      if(dwToken != TOKEN_NUMBER) return _T("Expected shift constant");
      ReplaceInsField(dwIns, 6, 5, dwSubToken);
      break;
    case 'I':
      if(dwToken != TOKEN_NUMBER) return _T("Expected number");
      ReplaceInsField(dwIns, 0, 16, dwSubToken);
      break;
    case 'C':
      if(dwToken != TOKEN_C0REG) return _T("Expected C0 register name");
      ReplaceInsField(dwIns, 11, 5, dwSubToken);
      break;
    case 'J':
      if(dwToken != TOKEN_NUMBER) return _T("Expected address");
      ReplaceInsField(dwIns, 0, 26, dwSubToken>>2);
      break;
    case 'B':
      if(dwToken != TOKEN_NUMBER) return _T("Expected address");
      ReplaceInsField(dwIns, 0, 16, (dwSubToken-(dwPC+4))>>2);
      break;
    }
  }
}

/***************************************************************************/

void CMIPSQuickAssembler::ReplaceInsField(DWORD &dwIns, int nBitPosition, int nBits, DWORD dwValue)
{
  DWORD dwMask = (1 << nBits) - 1;
  dwValue &= dwMask;
  dwIns &= ~(dwMask  << nBitPosition);
  dwIns |=  (dwValue << nBitPosition);
}
