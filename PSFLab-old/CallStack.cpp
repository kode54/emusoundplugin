// CallStack.cpp: implementation of the CCallStack class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "psflab.h"
#include "CallStack.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCallStack::CCallStack()
{
  m_aCalls.SetSize(0);
}

CCallStack::~CCallStack()
{
}

void CCallStack::Add(SCallStackEntry &e)
{
  m_cs.Lock();
  /*
  ** Check for duplicate invocations.
  **
  ** This could handle retriggered interrupts or many other bizarre
  ** program execution patterns.
  */
  int s = m_aCalls.GetSize();
  int i;
  for(i = 0; i < s; i++) {
    if(
      (m_aCalls[i].dwInvokedAddress == e.dwInvokedAddress) &&
      (m_aCalls[i].dwReturnSP == e.dwReturnSP)
    ) {
      m_aCalls.SetSize(i);
      break;
    }
  }
  /*
  ** Add the new call
  */
  m_aCalls.Add(e);
  m_cs.Unlock();
}

void CCallStack::Pop()
{
  m_cs.Lock();
  int s = m_aCalls.GetSize();
  if(s) {
    m_aCalls.SetSize(s - 1);
  }
  m_cs.Unlock();
}

int CCallStack::GetSize()
{
  m_cs.Lock();
  int n = m_aCalls.GetSize();
  m_cs.Unlock();
  return n;
}

void CCallStack::GetEntry(int nIndex, SCallStackEntry& e)
{
  memset(&e, 0, sizeof(e));
  m_cs.Lock();
  int s = m_aCalls.GetSize();
  nIndex = (s-1) - nIndex;
  if(nIndex >= 0 && nIndex < s) {
    memcpy(&e, &(m_aCalls[nIndex]), sizeof(SCallStackEntry));
  }
  m_cs.Unlock();
}

void CCallStack::Clear()
{
  m_cs.Lock();
  m_aCalls.SetSize(0);
  m_cs.Unlock();
}

void CCallStack::CopyFrom(CCallStack &src)
{
  m_cs.Lock();
  src.m_cs.Lock();
  int s = src.m_aCalls.GetSize();
  m_aCalls.SetSize(s);
  int i;
  for(i = 0; i < s; i++) {
    memcpy(&(m_aCalls[i]), &(src.m_aCalls[i]), sizeof(SCallStackEntry));
  }
  src.m_cs.Unlock();
  m_cs.Unlock();
}

void CCallStack::CheckForReturn(DWORD dwPC, DWORD dwSP)
{
  m_cs.Lock();
  int i;
  int s = m_aCalls.GetSize();
  for(i = 0; i < s; i++) {
    DWORD dwReturnPC = m_aCalls[i].dwReturnPC;
    DWORD dwReturnSP = m_aCalls[i].dwReturnSP;
    if(
      (dwSP == dwReturnSP) &&
      ((dwPC == dwReturnPC) || (dwPC == (dwReturnPC + 4)))
    ) {
      m_aCalls.SetSize(i);
      break;
    }
  }
  m_cs.Unlock();
}
