#include <stdio.h>
#include <math.h>

int main(void) {
  int i;
  for(i=0;i<=32;i++) {
    double d = sqrt(i) * (4096.0/sqrt(32.0));
    d += 0.5;
    printf("%10d\n", (int)d);
  }
  return 0;
}
