/////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

/////////////////////////////////////////////////////////////////////////////

#include "qsound.h"

/////////////////////////////////////////////////////////////////////////////

static uint8 z80rom   [0x040000];
static uint8 samplerom[0x800000];

/////////////////////////////////////////////////////////////////////////////

static uint32 kabuki_swap_key1=0;
static uint32 kabuki_swap_key2=0;
static uint16 kabuki_addr_key =0;
static uint8  kabuki_xor_key  =0;

#if 1

void load_romset(void) {
  FILE *f;
  f = fopen("rom\\cd.z80", "rb"); if(!f)abort();
  fread(z80rom,1,0x40000,f);fclose(f);
  f = fopen("rom\\cd.smp", "rb"); if(!f)abort();
  fread(samplerom,1,0x800000,f);fclose(f);
  kabuki_swap_key1=0x76543210;
  kabuki_swap_key2=0x24601357;
  kabuki_addr_key =0x4343;
  kabuki_xor_key  =0x43;
}

#else

void load_romset(void) {
  FILE *f;
  f = fopen("rom\\19x.01", "rb"); if(!f)abort();
  fread(z80rom,1,0x40000,f);fclose(f);
  f = fopen("rom\\19x.11", "rb"); if(!f)abort();
  fread(samplerom+0x000000,1,0x200000,f);fclose(f);
  f = fopen("rom\\19x.12", "rb"); if(!f)abort();
  fread(samplerom+0x200000,1,0x200000,f);fclose(f);
}

#endif

/////////////////////////////////////////////////////////////////////////////

int main(void) {
  FILE *f;
  void *state;
  int r;
  sint16 buf[1000];
  uint32 samples;
  int n;

  load_romset();
  if(qsound_init()) return -1;

  state = malloc(qsound_get_state_size());
  if(!state) abort();
  qsound_clear_state(state);
  qsound_set_z80_rom   (state, z80rom   , 0x40000);
  qsound_set_kabuki_key(state,
    kabuki_swap_key1,
    kabuki_swap_key2,
    kabuki_addr_key,
    kabuki_xor_key
  );
  qsound_set_sample_rom(state, samplerom, 0x800000);

//  qsound_write_ram_C(state, 0xFFD, 0x88);
//  qsound_write_ram_C(state, 0xFFE, 0x01);

//  qsound_write_ram_C(state, 0x000, 0x00);
//  qsound_write_ram_C(state, 0x001, 0x01);

  printf("executing\n");
  f = fopen("out.raw", "wb"); if(!f) abort();
for(n=0;n<20000;n++){
    samples = 500;
    r = qsound_execute(state, 0x7FFFFFFF, buf, &samples);
    if(r < 0) { printf("qsound_execute error\n"); break; }
    fwrite(buf, 1, sizeof(buf), f);
    fflush(f);
  }
  fclose(f);

f=fopen("raw","wb");if(!f)abort();fwrite(state,qsound_get_state_size(),1,f);fclose(f);

  return 0;
}

/////////////////////////////////////////////////////////////////////////////
