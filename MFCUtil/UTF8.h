#pragma once

namespace Util {

CStringA __fastcall StringWtoUTF8(LPCWSTR str);
CStringW __fastcall StringUTF8toW(LPCSTR str);

}
