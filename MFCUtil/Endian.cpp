/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "Endian.h"

/////////////////////////////////////////////////////////////////////////////

DWORD Util::Get32LSB(LPBYTE lpSrc)
{
  return (
    ((((DWORD)(lpSrc[0])) & 0xFF) <<  0) |
    ((((DWORD)(lpSrc[1])) & 0xFF) <<  8) |
    ((((DWORD)(lpSrc[2])) & 0xFF) << 16) |
    ((((DWORD)(lpSrc[3])) & 0xFF) << 24)
  );
}

void Util::Put32LSB(LPBYTE lpDest, DWORD dwValue)
{
  lpDest[0] = (BYTE)(dwValue >>  0);
  lpDest[1] = (BYTE)(dwValue >>  8);
  lpDest[2] = (BYTE)(dwValue >> 16);
  lpDest[3] = (BYTE)(dwValue >> 24);
}

/////////////////////////////////////////////////////////////////////////////
