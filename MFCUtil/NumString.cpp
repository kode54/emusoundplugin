////////////////////////////////////////////////////////////////////////////////
//
// NumString - Predictable behavior for number/string conversions
//
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "NumString.h"
#include "SafeDB.h"

////////////////////////////////////////////////////////////////////////////////

#include <math.h>

////////////////////////////////////////////////////////////////////////////////
//
//
//
CStringW Util::UINT64ToString(UINT64 u, int mindigits, DWORD flags) {
    WCHAR s[32];
    s[31] = 0;
    int n = 31;
    int comma = 3;
    if(mindigits < 1) mindigits = 1;
    while(n && (u || mindigits)) {
        if(!comma) {
            if(flags & Util::NSFLAG_COMMAS) { s[--n] = ','; }
            comma = 3;
        }
        comma--;
        s[--n] = (L"0123456789")[u % 10];
        u /= 10;
        if(mindigits) { mindigits--; }
    }
    CStringW str(s + n);
    return str;
}

UINT64 Util::StringToUINT64(CStringW &str) {
    UINT64 u = 0;
    int i;
    for(i = 0; i < str.GetLength(); i++) {
        WCHAR t = str[i];
        if(!(t >= '0' && t <= '9')) {
            str = str.Mid(i);
            break;
        }
        u *= 10;
        u += (t - '0');
    }
    return u;
}

#define MAX_VALUE (100000000000000.0)

CStringW Util::NumToString(double d, DWORD flags) {
    if(flags & Util::NSFLAG_DB) {
        d = Util::SafeConvertToDB(d);
    }

    BOOL bNegative = FALSE;
    if(d < 0) { bNegative = TRUE; d = -d; }

    if(d > MAX_VALUE) d = MAX_VALUE;

    UINT64 u = (INT64)((d * 10000.0)+0.5);
    CStringW strReturn;
    if(flags & Util::NSFLAG_COLONS) {
        strReturn =
            UINT64ToString((u / (10000*60*60)), 1, flags) +
            L":" +
            UINT64ToString((u / (10000*60)) % 60, 2, 0) +
            L":" +
            UINT64ToString((u / (10000)) % 60, 2, 0);
    } else {
        strReturn =
            UINT64ToString((u / (10000)), 1, flags);
    }
    strReturn.TrimLeft(L"0:");
    if(flags & Util::NSFLAG_DB) {
        strReturn += L"." + UINT64ToString((u % (10000)) / 100, 2, 0);
    } else {
        strReturn += L"." + UINT64ToString((u % (10000)), 4, 0);
        strReturn.TrimRight(L"0" );
        strReturn.TrimRight(L"." );
    }
    // Add a leading zero sometimes
    if(
        strReturn.IsEmpty() ||
        (strReturn[0] == ':') ||
        (strReturn[0] == '.')
    ) {
        strReturn = L"0" + strReturn;
    }
    if(u && bNegative) {
        strReturn = L"-" + strReturn;
    } else if(flags & Util::NSFLAG_PLUS) {
        strReturn = L"+" + strReturn;
    }
    if(flags & Util::NSFLAG_DB) {
        strReturn += L" dB";
    }
    return strReturn;
}

BOOL Util::StringToNum(CStringW str, double &d, DWORD flags) {
    str = str.Trim();

    bool db_convert = false;

    // detect 'db'
    if((flags & Util::NSFLAG_DB) && (str.GetLength() >= 2)) {
        if(!wcsicmp(str.Right(2), L"db")) {
            db_convert = true;
            str = str.Left(str.GetLength()-2);
            str = str.Trim();
        }
    }

    //
    // Look for any invalid characters
    // Also count colons (no more than 2, and none after decimals)
    // and decimals (no more than 1)
    //
    int i;
    int nColons = 0;
    int nDecimals = 0;
    int nDigits = 0;

    for(i = 0; i < str.GetLength(); i++) {
        switch(str[i]) {
        case '0': case '1':
        case '2': case '3':
        case '4': case '5':
        case '6': case '7':
        case '8': case '9':
            nDigits++;
            break;
        // Negative only allowed at the beginning
        case '-':
            if(i) return FALSE;
            break;
        // Positive only allowed at the beginning and only if the flags allow it
        case '+':
            if(i) return FALSE;
            if(!(flags & Util::NSFLAG_PLUS)) return FALSE;
            break;
        // Commas...
        case ',':
            // if separator flag is set, ignore commas
            if(flags & Util::NSFLAG_COMMAS) break;
            // otherwise tolerate them as decimal points
            str.SetAt(i, L'.');
            // DELIBERATE FALL-THROUGH
        case '.':
            nDecimals++;
            if(nDecimals > 1) return FALSE;
            break;
        // Colons may or may not be allowed
        case ':':
            if(nDecimals) return FALSE;
            if(!(flags & Util::NSFLAG_COLONS)) return FALSE;
            nColons++;
            if(nColons > 2) return FALSE;
            break;
        // Anything else is an error
        default:
            return FALSE;
        }
    }
    // If there weren't any actual digits, this shouldn't parse
    if(!nDigits) return FALSE;

    BOOL bNegative = FALSE;
    UINT64 u64Hours   = 0;
    UINT64 u64Minutes = 0;
    UINT64 u64Seconds = 0;
    UINT64 u64Decimal = 0;
    UINT64 u64DecimalRange = 1;

    if(str[0] == '+') {
        bNegative = FALSE;
        str = str.Mid(1);
    } else if(str[0] == '-') {
        bNegative = TRUE;
        str = str.Mid(1);
    }

    if(nColons >= 2) {
        u64Hours = StringToUINT64(str);
        if(str[0] != ':') return FALSE;
        str = str.Mid(1);
    }

    if(nColons >= 1) {
        u64Minutes = StringToUINT64(str);
        if(str[0] != ':') return FALSE;
        str = str.Mid(1);
    }

    u64Seconds = StringToUINT64(str);

    if(str[0] == '.') {
        str = str.Mid(1);
        u64DecimalRange = 1;
        i = str.GetLength();
        // we'll support maybe 10 decimal digits max
        if(i > 10) { i = 10; str = str.Left(10); }
        while(i--) { u64DecimalRange *= 10; }
        u64Decimal = StringToUINT64(str);
    }

    // Ensure things aren't over 60 if colons were used
    if(nColons >= 1) {
        if(u64Seconds >= 60) return FALSE;
    }
    if(nColons >= 2) {
        if(u64Minutes >= 60) return FALSE;
    }

    /* Set the result */
    u64Minutes += 60 * u64Hours;
    u64Seconds += 60 * u64Minutes;
    d =
        ((double)((INT64)u64Seconds))+
        (((double)((INT64)u64Decimal))/((double)((INT64)u64DecimalRange)));
    if(bNegative) { d = -d; }

    if(db_convert) d = Util::SafeConvertFromDB(d);
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
