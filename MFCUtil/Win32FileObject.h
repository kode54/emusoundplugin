////////////////////////////////////////////////////////////////////////////////
//
// Win32FileObject - Implementation of FileObject using Win32 file I/O.
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "FileObject.h"

class Win32FileObject : public FileObject
{
public:
    Win32FileObject(CStringW path);
    virtual ~Win32FileObject();

    virtual FileObject* CreateNew(CStringW path) const { return new Win32FileObject(path); }

    virtual bool  SameFileAs(const FileObject *f) const;

    virtual bool  Open    (File::MODE mode);
    virtual bool  Close   ();
    virtual INT64 Length  ();
    virtual INT64 Read    (void *buffer, INT64 length);
    virtual INT64 Write   (void *buffer, INT64 length);
    virtual INT64 Seek    (INT64 position, File::METHOD method);
    virtual bool  Truncate();

private:
    HANDLE m_handle;

    bool IsOpen() { return(m_handle != INVALID_HANDLE_VALUE); }
    bool CheckOpen();

};

////////////////////////////////////////////////////////////////////////////////
