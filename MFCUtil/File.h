////////////////////////////////////////////////////////////////////////////////
//
// File - API endpoint for file I/O
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

class FileObject;

class File
{
public:
    //
    // Default constructor (creates a NULL FileObject)
    //
    File();
    //
    // Copy
    //
    File(const File& f);
    //
    // Construct a File from a FileObject
    //
    File(const FileObject &fo);
    //
    // Construct from a previous File, but with a different path.
    //
    File(const File& f, CStringW path);
    //
    // Destruct
    //
    ~File();

    //
    // Assignment
    //
    File& operator = (const File& f);
    File& operator = (const FileObject& fo);

    //
    // Comparison.
    // This is subclassed as FileObject::SameFileAs, so comparisons can be
    // made different for different types of files.
    //
    bool operator == (const File& f) const;
    bool operator != (const File& f) const { return !((*this) == f); }

    //
    // (various constants)
    //
    enum MODE { READ, WRITE, CREATE };
    enum METHOD { BEGIN, CURRENT, END };
    enum { INVALID_POSITION = -1 };

    //
    // Open/close the file with the current path.
    // Returns true on success
    //
    bool Open(MODE mode);
    bool Close();

    //
    // Get the file length.
    // Returns INVALID_POSITION on failure
    //
    INT64 Length();

    //
    // Read/write binary.
    // Returns the number of bytes written or read
    // 0 on error
    //
    INT64 Read (void *buffer, INT64 length);
    INT64 Write(void *buffer, INT64 length);

    //
    // Seek (supports begin/current/end).
    // Returns INVALID_POSITION on failure.
    //
    INT64 Seek(INT64 position, METHOD method);
    INT64 Tell() { return Seek(0, CURRENT); }

    //
    // Truncate the file at the current position.
    // Returns true on success.
    //
    bool Truncate();


    bool Read32LSB(DWORD &value);

    bool Read32LSB(INT32 &value) { return Read32LSB(*((DWORD*)(&value))); }

    //
    // Get path or last error strings
    //
    CStringW GetPath     () const;
    CStringW GetLastError() const;

    void Empty();

    operator bool() const { return (m_fo != NULL); }

private:
    //
    // Pointer to the abstract class where the magic actually happens.
    //
    FileObject *m_fo;

};
