// LinkText.cpp

#include "StdAfx.h"

#include "LinkText.h"

#include "HandCursor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLinkText

CLinkText::CLinkText()
{
	m_url.Empty();
	m_colLink = RGB(0,0,224);
	m_colHover = RGB(0,0,224);
	m_bOver = FALSE;
}

BEGIN_MESSAGE_MAP(CLinkText, CStatic)
	//{{AFX_MSG_MAP(CLinkText)
  ON_WM_NCHITTEST()
  ON_WM_SETCURSOR()
	ON_WM_CTLCOLOR_REFLECT()
	ON_CONTROL_REFLECT(STN_CLICKED, OnClicked)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLinkText

UINT CLinkText::OnNcHitTest(CPoint point) { return HTCLIENT; }

HBRUSH CLinkText::CtlColor(CDC* pDC,UINT nCtlColor)
{
	ASSERT(nCtlColor == CTLCOLOR_STATIC);
	if (m_bOver) pDC->SetTextColor(m_colHover);
	else pDC->SetTextColor(m_colLink);
	pDC->SetBkMode(TRANSPARENT);
	return (HBRUSH)GetStockObject(NULL_BRUSH);
}

void CLinkText::PreSubclassWindow()
{
	::SetWindowLong(GetSafeHwnd(),GWL_STYLE, GetStyle() | SS_NOTIFY);
	if(m_url.IsEmpty()) GetWindowText(m_url);
//	CString s;
//	GetWindowText(s);
//	if(s.IsEmpty() && m_url.IsEmpty()) SetWindowText(m_url);

	LOGFONT lf;
	GetFont()->GetLogFont(&lf);
	lf.lfUnderline = TRUE;
	m_font.CreateFontIndirect(&lf);
	SetFont(&m_font);

//	PositionWindow();
//	CRect rect; 
//	GetClientRect(rect);
//	m_toolTip.Create(this);
//	m_toolTip.AddTool(this, m_url, rect,1);

  CStatic::PreSubclassWindow();
}
/*
void CLinkText::PositionWindow()
{
	if (!::IsWindow(GetSafeHwnd())) return;
	CRect rect;
	GetWindowRect(rect);
	CWnd* pParent = GetParent();
	if (pParent) pParent->ScreenToClient(rect);

	CString s;
	GetWindowText(s);
	CDC* pDC = GetDC();
	CFont* pOldFont = pDC->SelectObject(&m_font);
	CSize Extent = pDC->GetTextExtent(s);
	pDC->SelectObject(pOldFont);
	ReleaseDC(pDC);

	DWORD dwStyle = GetStyle();
	if (dwStyle & SS_CENTERIMAGE)
		rect.DeflateRect(0, (rect.Height() - Extent.cy)/2);
	else rect.bottom = rect.top + Extent.cy;

	if (dwStyle & SS_CENTER)   
		rect.DeflateRect((rect.Width() - Extent.cx)/2, 0);
	else if (dwStyle & SS_RIGHT)
		rect.left  = rect.right - Extent.cx;
	else rect.right = rect.left + Extent.cx;
	SetWindowPos(NULL,rect.left,rect.top,rect.Width(),rect.Height(),SWP_NOZORDER);
}
*/
LONG CLinkText::GetRegKey(HKEY key,LPCTSTR subkey,LPTSTR ret)
{
	HKEY hkey;
	LONG retval = RegOpenKeyEx(key,subkey,0,KEY_QUERY_VALUE,&hkey);
	if (retval == ERROR_SUCCESS) {
		long datasize = MAX_PATH;
		TCHAR data[MAX_PATH];
		RegQueryValue(hkey,NULL,data,&datasize);
		lstrcpy(ret,data);
		RegCloseKey(hkey);
	}
	return retval;
}

HINSTANCE CLinkText::GotoURL(LPCTSTR url,int swcmd)
{
	TCHAR key[MAX_PATH + MAX_PATH];
	HINSTANCE result = ShellExecute(NULL,_T("open"),url,NULL,NULL,swcmd);
	if ((UINT)result <= HINSTANCE_ERROR) {
		if (GetRegKey(HKEY_CLASSES_ROOT,_T(".htm"),key) == ERROR_SUCCESS) {
			lstrcat(key,_T("\\shell\\open\\command"));
			if (GetRegKey(HKEY_CLASSES_ROOT,key,key) == ERROR_SUCCESS) {
				TCHAR *pos = _tcsstr(key,_T("\"%1\""));
				if (pos == NULL) {
					pos = _tcsstr(key,_T("%1"));
					if (pos == NULL)
						pos = key+lstrlen(key)-1;
					else *pos = '\0';
				} else *pos = '\0';
				lstrcat(pos,_T(" "));
				lstrcat(pos,url);
				result = (HINSTANCE)WinExec(CStringA(key),swcmd);
			}
		}
	}
	return result;
}

//////////////////

BOOL CLinkText::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) {
    return Util::SetHandCursor();
}
