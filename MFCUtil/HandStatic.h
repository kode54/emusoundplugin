#pragma once

class CHandStatic : public CStatic
{
public:
    CHandStatic();
	virtual ~CHandStatic();

protected:
	//{{AFX_MSG(CLinkText)
    afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
