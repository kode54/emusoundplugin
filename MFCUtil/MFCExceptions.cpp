////////////////////////////////////////////////////////////////////////////////
/*
** MFCExceptions - Error messages from MFC CExceptions without the resources
*/
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "MFCExceptions.h"
#include "ErrorString.h"

////////////////////////////////////////////////////////////////////////////////

static CStringW GetMFCFileExceptionMessage(CFileException *pEx) {
    return Util::Win32ErrorString(pEx->m_lOsError);
}

////////////////////////////////////////////////////////////////////////////////

CStringW Util::GetMFCExceptionMessage(CException *pEx) {
    ASSERT(pEx);
    if(pEx->IsKindOf(RUNTIME_CLASS(CFileException))) return GetMFCFileExceptionMessage((CFileException*)pEx);
    CStringW s = L"Unknown exception: ";
    s += pEx->GetRuntimeClass()->m_lpszClassName;
    return s;
}

////////////////////////////////////////////////////////////////////////////////
