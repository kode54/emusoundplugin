/////////////////////////////////////////////////////////////////////////////
//
// UTF8 conversion to/from wide string
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "UTF8.h"

/////////////////////////////////////////////////////////////////////////////

CStringA Util::StringWtoUTF8(LPCWSTR str) {
    CStringA result;
    UINT32 c;
    for(;;) {
        c = ((UINT16)(str[0]));
        if(c < 0x80) {
            if(!c) break;
            result += (CHAR)c;
            str++;
        } else if(c < 0x800) {
            result += (CHAR)(0xC0 | ((c >>  6) & 0x1F));
            result += (CHAR)(0x80 | ((c      ) & 0x3F));
            str++;
        } else if(((c & 0xFC00) == 0xD800) && ((((UINT16)(str[1])) & 0xFC00) == 0xDC00)) {
            // surrogate pair
            c = 0x10000 + ((c & 0x3FF) << 10) + (((UINT16)(str[1])) & 0x3FF);
            result += (CHAR)(0xF0 | ((c >> 18) & 0x07));
            result += (CHAR)(0x80 | ((c >> 12) & 0x3F));
            result += (CHAR)(0x80 | ((c >>  6) & 0x3F));
            result += (CHAR)(0x80 | ((c      ) & 0x3F));
            str += 2;
        } else {
            result += (CHAR)(0xE0 | ((c >> 12) & 0x0F));
            result += (CHAR)(0x80 | ((c >>  6) & 0x3F));
            result += (CHAR)(0x80 | ((c      ) & 0x3F));
            str++;
        }
    }
    return result;
}

/////////////////////////////////////////////////////////////////////////////

CStringW Util::StringUTF8toW(LPCSTR str) {
    CStringW result;
    UINT32 c;
    for(;;) {
        c = ((UINT8)(str[0]));
        if(c < 0x80) {
            if(!c) break;
            result += (WCHAR)c;
            str++;
        } else if(c >= 0xC2 && c < 0xE0 && (((UINT8)(str[1])) & 0xC0) == 0x80) {
            c = ((c & 0x1F) << 6);
            c |= ((UINT32)(str[1] & 0x3F))      ;
            result += (WCHAR)c;
            str += 2;
        } else if(c >= 0xE0 && c < 0xF0 && (((UINT8)(str[1])) & 0xC0) == 0x80 && (((UINT8)(str[2])) & 0xC0) == 0x80) {
            c = ((c & 0x0F) << 12);
            c |= ((UINT32)(str[1] & 0x3F)) <<  6;
            c |= ((UINT32)(str[2] & 0x3F))      ;
            if(c >= 0x800 && c <= 0xFFFF) {
                result += (WCHAR)c;
                str += 3;
            } else {
                str++; // invalid overlong code
            }
        } else if(c >= 0xF0 && c < 0xF8 && (((UINT8)(str[1])) & 0xC0) == 0x80 && (((UINT8)(str[2])) & 0xC0) == 0x80 && (((UINT8)(str[3])) & 0xC0) == 0x80) {
            c = ((c & 0x07) << 18);
            c |= ((UINT32)(str[1] & 0x3F)) << 12;
            c |= ((UINT32)(str[2] & 0x3F)) <<  6;
            c |= ((UINT32)(str[3] & 0x3F))      ;
            if(c >= 0x10000 && c <= 0x10FFFF) {
                // encode surrogate pair
                result += (WCHAR)(0xD800 | ((c - 0x10000) >> 10  ));
                result += (WCHAR)(0xDC00 | ((c - 0x10000) & 0x3FF));
                str += 4;
            } else {
                str++; // invalid overlong code
            }
        }
    }
    return result;
}

/////////////////////////////////////////////////////////////////////////////
