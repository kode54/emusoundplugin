#pragma once

namespace Util {

double __fastcall SafeConvertToDB(double scale);
double __fastcall SafeConvertFromDB(double db);

};
