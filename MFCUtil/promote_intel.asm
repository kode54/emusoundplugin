section .text
bits 32

extern ___intel_cpu_indicator

;
; Procedure to promote __intel_cpu_indicator on non-Intel CPUs.
;

global @promote_intel_cpu_indicator@0
@promote_intel_cpu_indicator@0:

	push eax
	push ebx
	push ecx
	push edx
	push esi
	push edi
	push ebp

	; first detect if we can do CPUID at all
	pushf
	pop eax
	mov ecx, eax
	xor eax, 200000h
	push eax
	popf
	pushf
	pop eax
	push ecx
	popf
	cmp eax, ecx
	jz short ProcessorIsRetarded

	; now do the first CPUID and detect the standard level
	xor eax,eax
	cpuid
	test eax,eax
	jz short ProcessorIsRetarded

	; now we get the interesting data
	mov eax,1
	cpuid

	; put SSE3 in bit 31 of EAX, other bits in 0-30
	shl ecx,31
	and edx,7FFFFFFFh
	mov eax,ecx
	or  eax,edx
	not eax ; bits are zero if the feature is present
	; 00000001h - FPU
	; 00008000h - CMOV
	; 00800000h - MMX
	; 01000000h - FXSAVE
	; 02000000h - SSE
	; 04000000h - SSE2
	; 80000000h - SSE3

	mov  edx,800h      ; processor is equivalent to a newer Pentium 4
	test eax,87808001h ; check FPU+CMOV+MMX+FXSAVE+SSE+SSE2+SSE3
	jz short Done

	mov  edx,200h      ; processor is equivalent to an older Pentium 4
	test eax,07808001h ; check FPU+CMOV+MMX+FXSAVE+SSE+SSE2
	jz short Done

	mov  edx,080h      ; processor is equivalent to a Pentium 3
	test eax,03808001h ; check FPU+CMOV+MMX+FXSAVE+SSE
	jz short Done

	mov  edx,020h      ; processor is equivalent to ?
	test eax,01808001h ; check FPU+CMOV+MMX+FXSAVE
	jz short Done

	mov  edx,010h      ; processor is equivalent to a Pentium 2
	test eax,00808001h ; check FPU+CMOV+MMX
	jz short Done

	mov  edx,008h      ; processor is equivalent to a Pentium MMX
	test eax,00800001h ; check FPU+MMX
	jz short Done

	mov  edx,004h      ; processor is equivalent to a Pentium Pro
	test eax,00008001h ; check FPU+CMOV
	jz short Done

ProcessorIsRetarded:
	mov edx,1
Done:
	cmp edx,[___intel_cpu_indicator]
	jb short NoChange
	mov [___intel_cpu_indicator],edx 
NoChange:

	pop ebp
	pop edi
	pop esi
	pop edx
	pop ecx
	pop ebx
	pop eax
	ret
