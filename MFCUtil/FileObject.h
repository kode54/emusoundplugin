////////////////////////////////////////////////////////////////////////////////
//
// FileObject - Abstract object for file I/O
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "File.h"

class FileObject
{
public:
    FileObject(CStringW path);
    virtual ~FileObject(void);
    FileObject* Clone() const { return CreateNew(GetPath()); }
    virtual FileObject* CreateNew(CStringW path) const = 0;

    virtual bool  SameFileAs(const FileObject *f) const = 0;

    virtual bool  Open    (File::MODE mode) = 0;
    virtual bool  Close   () = 0;
    virtual INT64 Length  () = 0;
    virtual INT64 Read    (void *buffer, INT64 length) = 0;
    virtual INT64 Write   (void *buffer, INT64 length) = 0;
    virtual INT64 Seek    (INT64 position, File::METHOD method) = 0;
    virtual bool  Truncate() = 0;

    CStringW GetPath     () const { return m_strPath     ; }
    CStringW GetLastError() const { return m_strLastError; }

protected:
    CStringW m_strPath;
    CStringW m_strLastError;

    void SetLastError(CStringW s) { m_strLastError = s; }
};
