////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "MFCFileObject.h"

#include "MFCExceptions.h"

#include "Win32Filenames.h"

////////////////////////////////////////////////////////////////////////////////

MFCFileObject::MFCFileObject(CStringW path) : FileObject(path) {
    m_pFile = NULL;
}

MFCFileObject::~MFCFileObject(void)
{
    if(m_pFile) delete m_pFile;
}

////////////////////////////////////////////////////////////////////////////////

bool MFCFileObject::CheckOpen() {
    if(!m_pFile) {
        SetLastError(L"File not open");
        return false;
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////

bool MFCFileObject::Close() {
    if(!CheckOpen()) return false;
    try {
        delete m_pFile;
        m_pFile = NULL;
        return true;
    } catch(CException *e) {
        SetLastError(Util::GetMFCExceptionMessage(e));
        delete e;
        return false;
    }
}

////////////////////////////////////////////////////////////////////////////////

bool MFCFileObject::Open(File::MODE mode) {
    Close();

    UINT nOpenFlags = 0;

    switch(mode) {
    case File::CREATE: nOpenFlags = CFile::modeCreate | CFile::modeReadWrite | CFile::shareDenyWrite | CFile::typeBinary; break;
    case File::READ  : nOpenFlags =                     CFile::modeRead      | CFile::shareDenyWrite | CFile::typeBinary; break;
    case File::WRITE : nOpenFlags =                     CFile::modeReadWrite | CFile::shareDenyWrite | CFile::typeBinary; break;
    default: SetLastError(L"Invalid mode"); return false;
    }

    CFileException e;

    m_pFile = new CFile;

    BOOL b = m_pFile->Open(CString(m_strPath), nOpenFlags, &e);
    if(!b) {
        SetLastError(Util::GetMFCExceptionMessage(&e));
        delete m_pFile;
        m_pFile = NULL;
        return false;
    }

    m_mode = mode;

    return true; // success
}

////////////////////////////////////////////////////////////////////////////////

INT64 MFCFileObject::Length() {
    if(!CheckOpen()) return File::INVALID_POSITION;

    try {
        if(m_mode == File::WRITE || m_mode == File::CREATE) {
            m_pFile->Flush();
        }
        return m_pFile->GetLength();
    } catch(CException *e) {
        SetLastError(Util::GetMFCExceptionMessage(e));
        delete e;
        return File::INVALID_POSITION;
    }

}

////////////////////////////////////////////////////////////////////////////////

bool MFCFileObject::Truncate() {
    if(!CheckOpen()) return false;

    try {
        if(m_mode == File::WRITE || m_mode == File::CREATE) {
            m_pFile->Flush();
        }
        m_pFile->SetLength(m_pFile->GetPosition());
        return true;
    } catch(CException *e) {
        SetLastError(Util::GetMFCExceptionMessage(e));
        delete e;
        return false;
    }

}

////////////////////////////////////////////////////////////////////////////////

INT64 MFCFileObject::Seek(INT64 position, File::METHOD method) {
    if(!CheckOpen()) return File::INVALID_POSITION;

    UINT nFrom;
    switch(method) {
    case File::BEGIN  : nFrom = CFile::begin  ; break;
    case File::CURRENT: nFrom = CFile::current; break;
    case File::END    : nFrom = CFile::end    ; break;
    default: SetLastError(L"Invalid method"); return File::INVALID_POSITION;
    }

    try {
        return m_pFile->Seek(position, nFrom);
    } catch(CException *e) {
        SetLastError(Util::GetMFCExceptionMessage(e));
        delete e;
        return File::INVALID_POSITION;
    }

}

////////////////////////////////////////////////////////////////////////////////

INT64 MFCFileObject::Read(void *buffer, INT64 length) {
    if(!CheckOpen()) return 0;

    if(length <= 0 || length > 0x7FFFFFFF) {
        SetLastError(L"Invalid length");
        return 0;
    }

    try {
        return m_pFile->Read(buffer, (UINT)length);
    } catch(CException *e) {
        SetLastError(Util::GetMFCExceptionMessage(e));
        delete e;
        return 0;
    }

}

////////////////////////////////////////////////////////////////////////////////

INT64 MFCFileObject::Write(void *buffer, INT64 length) {
    if(!CheckOpen()) return 0;

    if(length <= 0 || length > 0x7FFFFFFF) {
        SetLastError(L"Invalid length");
        return 0;
    }

    try {
        m_pFile->Write(buffer, (UINT)length);
        return length;
    } catch(CException *e) {
        SetLastError(Util::GetMFCExceptionMessage(e));
        delete e;
        return 0;
    }

}

////////////////////////////////////////////////////////////////////////////////

bool MFCFileObject::SameFileAs(const FileObject *f) const {
    return f && Util::SameWin32Filenames(GetPath(), f->GetPath());
}

////////////////////////////////////////////////////////////////////////////////
