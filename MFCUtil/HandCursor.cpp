////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "HandCursor.h"

////////////////////////////////////////////////////////////////////////////////

static HCURSOR s_hCursorLink = NULL;
static BOOL s_bTriedOnce = FALSE;

////////////////////////////////////////////////////////////////////////////////
//
// Set "hand" cursor to cue user that this is a link. If app has not set
// g_hCursorLink, then try to get the cursor from winhlp32.exe,
// resource 106, which is a pointing finger. This is a bit of a kludge,
// but it works.
//
BOOL Util::SetHandCursor()
{
    if(s_hCursorLink == NULL) {
        if(!s_bTriedOnce) {
            s_bTriedOnce = TRUE;
            s_hCursorLink = ::LoadCursor(NULL,MAKEINTRESOURCE(32649)); // try to load system cursor
            if(s_hCursorLink == NULL) {
                CString windir;
                GetWindowsDirectory(windir.GetBuffer(MAX_PATH), MAX_PATH);
                windir.ReleaseBuffer();
                windir += _T("\\winhlp32.exe");
                HMODULE hModule = LoadLibrary(windir);
                if(hModule) {
                    s_hCursorLink = CopyCursor(::LoadCursor(hModule, MAKEINTRESOURCE(106)));
                }
                FreeLibrary(hModule);
            }
        }
    }
    if(s_hCursorLink != NULL) {
        ::SetCursor(s_hCursorLink);
        return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////
