////////////////////////////////////////////////////////////////////////////////
//
// File
//
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "File.h"
#include "FileObject.h"

////////////////////////////////////////////////////////////////////////////////

File::File() {
    m_fo = NULL;
}

File::File(const FileObject &fo) {
    if(&fo) {
        m_fo = fo.Clone();
    } else {
        m_fo = NULL;
    }
}

File::File(const File& f) {
    if((&f) && f.m_fo) {
        m_fo = f.m_fo->Clone();
    } else {
        m_fo = NULL;
    }
}

File::File(const File& f, CStringW path) {
    if((&f) && f.m_fo) {
        m_fo = f.m_fo->CreateNew(path);
    } else {
        m_fo = NULL;
    }
}

File::~File() {
    if(m_fo) delete m_fo;
}

////////////////////////////////////////////////////////////////////////////////

File& File::operator = (const File& f) {
    if(this != &f) {
        if(m_fo) delete m_fo;
        if(&f) {
            m_fo = f.m_fo->Clone();
        } else {
            m_fo = NULL;
        }
    }
    return *this;
}

////////////////////////////////////////////////////////////////////////////////

File& File::operator = (const FileObject& fo) {
    if(m_fo) delete m_fo;
    if(&fo) {
        m_fo = fo.Clone();
    } else {
        m_fo = NULL;
    }
    return *this;
}

////////////////////////////////////////////////////////////////////////////////

bool File::operator == (const File& f) const {
    if(!m_fo) return false;
    if(!f.m_fo) return false;
    return m_fo->SameFileAs(f.m_fo);
}

////////////////////////////////////////////////////////////////////////////////

bool File::Open(MODE mode) {
    if(!m_fo) return false;
    return m_fo->Open(mode);
}

bool File::Close() {
    if(!m_fo) return false;
    return m_fo->Close();
}

INT64 File::Length() {
    if(!m_fo) return INVALID_POSITION;
    return m_fo->Length();
}

INT64 File::Read(void *buffer, INT64 length) {
    if(!m_fo) return 0;
    return m_fo->Read(buffer, length);
}

INT64 File::Write(void *buffer, INT64 length) {
    if(!m_fo) return 0;
    return m_fo->Write(buffer, length);
}

INT64 File::Seek(INT64 position, METHOD method) {
    if(!m_fo) return INVALID_POSITION;
    return m_fo->Seek(position, method);
}

bool File::Truncate() {
    if(!m_fo) return false;
    return m_fo->Truncate();
}

////////////////////////////////////////////////////////////////////////////////

CStringW File::GetPath() const {
    if(!m_fo) return L"";
    return m_fo->GetPath();
}

CStringW File::GetLastError() const {
    if(!m_fo) return L"FileObject not initialized (program error)";
    return m_fo->GetLastError();
}

////////////////////////////////////////////////////////////////////////////////

void File::Empty() {
    if(m_fo) delete m_fo;
    m_fo = NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool File::Read32LSB(DWORD &value) {
    if(!m_fo) return false;
    BYTE buf[4];
    if(Read(buf, 4) != 4) return false;
    value = 
        (((DWORD)(buf[0])) <<  0) |
        (((DWORD)(buf[1])) <<  8) |
        (((DWORD)(buf[2])) << 16) |
        (((DWORD)(buf[3])) << 24);
    return true;
}

////////////////////////////////////////////////////////////////////////////////
