#pragma once

namespace Util {

CStringW __fastcall UINT64ToString(UINT64 u, int mindigits, DWORD flags);
UINT64   __fastcall StringToUINT64(CStringW &str);

CStringW __fastcall NumToString(double d, DWORD flags);
BOOL     __fastcall StringToNum(CStringW str, double &d, DWORD flags);

enum {
    NSFLAG_COMMAS = 0x00000001,
    NSFLAG_COLONS = 0x00000002,
    NSFLAG_PLUS   = 0x00000004,
    NSFLAG_DB     = 0x00000008
};

};
