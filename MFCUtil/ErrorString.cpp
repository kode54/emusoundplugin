////////////////////////////////////////////////////////////////////////////////
//
// ErrorString - Format Win32 error strings
//
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "ErrorString.h"

////////////////////////////////////////////////////////////////////////////////
//
// Return system error string.
//
CStringW Util::Win32ErrorString(DWORD err)
{
    CStringW strError;
    LPTSTR s;
    //
    // Probably OK to use ANSI, because local system messages are likely to be
    // valid in the local code page.
    //
    if(!::FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
		    FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		err,
		0,
		(LPTSTR)&s,
		0,
        NULL)
    ) {
        strError.Format(L"Unknown error %08X", err);
	} else {
	    LPTSTR p = s + lstrlen(s);
        // trim trailing CR,LF
        while((p > s) && (p[-1] == '\r' || p[-1] == '\n')) { p--; }
        *p = 0;
	    strError = s;
	    ::LocalFree(s);
	}
    return strError;
}

////////////////////////////////////////////////////////////////////////////////
