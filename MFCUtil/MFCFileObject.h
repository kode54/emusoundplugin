////////////////////////////////////////////////////////////////////////////////
//
// MFCFileObject - Implementation of FileObject using MFC CFile.
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "FileObject.h"

class MFCFileObject : public FileObject
{
public:
    MFCFileObject(CStringW path);
    virtual ~MFCFileObject(void);

    virtual FileObject* CreateNew(CStringW path) const { return new MFCFileObject(path); }

    virtual bool  SameFileAs(const FileObject *f) const;

    virtual bool  Open    (File::MODE mode);
    virtual bool  Close   ();
    virtual INT64 Length  ();
    virtual INT64 Read    (void *buffer, INT64 length);
    virtual INT64 Write   (void *buffer, INT64 length);
    virtual INT64 Seek    (INT64 position, File::METHOD method);
    virtual bool  Truncate();

private:
    CFile *m_pFile;

    File::MODE m_mode;

    bool CheckOpen();

};

////////////////////////////////////////////////////////////////////////////////
