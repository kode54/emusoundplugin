////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "SafeDB.h"

////////////////////////////////////////////////////////////////////////////////

#include <math.h>

////////////////////////////////////////////////////////////////////////////////

// valid range is +200dB to -200dB
// because I said so
#define DB_LOG_MAX (200.0)
#define DB_LOG_MIN (-200.0)
#define DB_SCALE_MAX (10000000000.0)
#define DB_SCALE_MIN (0.0000000001)

double Util::SafeConvertToDB(double scale) {
    if(scale < 0.0) scale = -scale;
    if(scale > DB_SCALE_MAX) scale = DB_SCALE_MAX;
    if(scale < DB_SCALE_MIN) scale = DB_SCALE_MIN;
    return 20.0*log10(scale);
}

double Util::SafeConvertFromDB(double db) {
    if(db > DB_LOG_MAX) db = DB_LOG_MAX;
    if(db < DB_LOG_MIN) db = DB_LOG_MIN;
    return pow(10.0, db/20.0);
}

////////////////////////////////////////////////////////////////////////////////
