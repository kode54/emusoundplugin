#include "StdAfx.h"

#include "HandStatic.h"

#include "HandCursor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CHandStatic::CHandStatic() : CStatic() { }
CHandStatic::~CHandStatic() { }

BEGIN_MESSAGE_MAP(CHandStatic, CStatic)
	//{{AFX_MSG_MAP(CHandStatic)
    ON_WM_SETCURSOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CHandStatic::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) {
    return Util::SetHandCursor();
}
