#pragma once

/////////////////////////////////////////////////////////////////////////////
// CLinkText

class CLinkText : public CStatic
{
public:
	CLinkText();
	virtual ~CLinkText(){}
//	void SetURL(CString strURL);
//	CString GetURL() const{return m_url;}
	//void SetColor(COLORREF colLink,COLORREF colHover = 0xFFFFFF);
	//COLORREF GetLinkColor()const{return m_colLink;}
	//COLORREF GetHoverColor()const{return m_colHover;}

	// ClassWizard
	//{{AFX_VIRTUAL(CLinkText)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL
protected:
	HINSTANCE GotoURL(LPCTSTR url,int swcmd);
	LONG GetRegKey(HKEY key,LPCTSTR subkey,LPTSTR retdata);
	//void PositionWindow();
protected:
	BOOL     m_bOver;
	COLORREF m_colLink;
	COLORREF m_colHover;
	CString  m_url;
	CFont    m_font;
//	CToolTipCtrl	m_toolTip;

protected:
	//{{AFX_MSG(CLinkText)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
  afx_msg UINT OnNcHitTest(CPoint point);
  afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
	afx_msg void OnClicked(){GotoURL(m_url,SW_SHOW);}
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
