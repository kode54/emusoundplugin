/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "Win32Filenames.h"

/////////////////////////////////////////////////////////////////////////////

bool Util::SameWin32Filenames(CStringW f1, CStringW f2) {
    int l1 = f1.GetLength();
    int l2 = f2.GetLength();
    if(l1 != l2) return false;

    bool is_same;

    if(sizeof(TCHAR) == sizeof(WCHAR)) {
        //
        // This is the correct way to compare Win32 Unicode filenames, according to:
        //
        // http://blogs.msdn.com/michkap/archive/2005/10/17/481600.aspx
        //
        LPWSTR buf1 = f1.GetBuffer();
        LPWSTR buf2 = f2.GetBuffer();
        CharUpperW(buf1);
        CharUpperW(buf2);
        is_same = !memcmp(buf1, buf2, sizeof(WCHAR) * l1);
        f1.ReleaseBuffer();
        f2.ReleaseBuffer();
    } else {
        //
        // I have no idea if this is the right way to compare them in ANSI, but
        // whatever.
        //
        is_same = !::lstrcmpi(CString(f1), CString(f2));
    }

    return is_same;

}

/////////////////////////////////////////////////////////////////////////////
