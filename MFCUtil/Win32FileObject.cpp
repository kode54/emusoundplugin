////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "Win32FileObject.h"
#include "ErrorString.h"
#include "Win32Filenames.h"

////////////////////////////////////////////////////////////////////////////////

Win32FileObject::Win32FileObject(CStringW path) : FileObject(path) {
    m_handle = INVALID_HANDLE_VALUE;
}

Win32FileObject::~Win32FileObject()
{
    if(m_handle != INVALID_HANDLE_VALUE) {
        ::CloseHandle(m_handle);
    }
}

////////////////////////////////////////////////////////////////////////////////

static inline bool myalpha(WCHAR w) {
    if(w >= 'A' && w <= 'Z') return true;
    if(w >= 'a' && w <= 'z') return true;
    return false;
}

static inline bool mysep(WCHAR w) {
    return (w == '\\' || w == '/');
}

static CStringW UNCName(CStringW path) {
    CStringW result;

    if(mysep(path[0]) && mysep(path[1])) {
        if(path[2] == '?') {
            // already UNC
        } else {
            // server share
            result = L"\\\\?\\UNC\\";
        }
    } else if(myalpha(path[0]) && path[1] == ':' && mysep(path[2])) {
        // drive letter
        result = L"\\\\?\\";
    }
    result += path;
    // only backslashes allowed in UNC paths
    result.Replace(L'/', L'\\');
    return result;
}

////////////////////////////////////////////////////////////////////////////////

bool Win32FileObject::Close() {
    if(!CheckOpen()) return false;
    if(!::CloseHandle(m_handle)) {
        SetLastError(Util::Win32ErrorString(::GetLastError()));
    }
    m_handle = INVALID_HANDLE_VALUE;
    return true;
}

bool Win32FileObject::Open(File::MODE mode) {
    // If already open, close the existing handle
    if(IsOpen()) Close();

    DWORD access;
    DWORD share = FILE_SHARE_READ;
    DWORD creation;

    switch(mode) {
    case File::CREATE: access   = GENERIC_READ | GENERIC_WRITE; creation = CREATE_ALWAYS; break;
    case File::READ  : access   = GENERIC_READ;                 creation = OPEN_EXISTING; break;
    case File::WRITE : access   = GENERIC_READ | GENERIC_WRITE; creation = OPEN_EXISTING; break;
    default: SetLastError(L"Invalid mode"); return false;
    }

    //
    // TODO: Make this Win9x-friendly.
    //
    m_handle = ::CreateFileW(
        UNCName(m_strPath),
        access,
        share,
        NULL,
        creation,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );

    if(m_handle == INVALID_HANDLE_VALUE) {
        SetLastError(Util::Win32ErrorString(::GetLastError()));
        return false;
    }

    return true; // success
}

////////////////////////////////////////////////////////////////////////////////

bool Win32FileObject::CheckOpen() {
    if(!IsOpen()) {
        SetLastError(L"File not open");
        return false;
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////

INT64 Win32FileObject::Length() {
    if(!CheckOpen()) return File::INVALID_POSITION;

    DWORD high = 0;
    DWORD low = ::GetFileSize(m_handle, &high);

    if(low == INVALID_FILE_SIZE) {
        DWORD e = ::GetLastError();
        if(e != NO_ERROR) {
            SetLastError(Util::Win32ErrorString(e));
            return File::INVALID_POSITION;
        }
    }

    //
    // We can't really cope with files bigger than 2^63-1, but this is unlikely
    // to actually happen
    //
    if(high & 0x80000000) {
        SetLastError(L"File reports invalid size");
        return File::INVALID_POSITION;
    }

    return (((UINT64)high) << 32) | ((UINT64)low);
}

////////////////////////////////////////////////////////////////////////////////

bool Win32FileObject::Truncate() {
    if(!CheckOpen()) return false;

    if(!::SetEndOfFile(m_handle)) {
        SetLastError(Util::Win32ErrorString(::GetLastError()));
        return false;
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////

INT64 Win32FileObject::Seek(INT64 position, File::METHOD method) {
    if(!CheckOpen()) return File::INVALID_POSITION;

    LONG low  = (LONG)(position      );
    LONG high = (LONG)(position >> 32);

    DWORD m;
    switch(method) {
    case File::BEGIN  : m = FILE_BEGIN  ; break;
    case File::CURRENT: m = FILE_CURRENT; break;
    case File::END    : m = FILE_END    ; break;
    default: SetLastError(L"Invalid method"); return File::INVALID_POSITION;
    }

    DWORD set = ::SetFilePointer(m_handle, low, &high, m);
    if(set == INVALID_SET_FILE_POINTER) {
        DWORD e = ::GetLastError();
        if(e != NO_ERROR) {
            SetLastError(Util::Win32ErrorString(e));
            return File::INVALID_POSITION;
        }
    }

    return (((UINT64)high) << 32) | ((UINT64)set);
}

////////////////////////////////////////////////////////////////////////////////

INT64 Win32FileObject::Read(void *buffer, INT64 length) {
    if(!CheckOpen()) return 0;

    if(length <= 0 || length > 0x7FFFFFFF) {
        SetLastError(L"Invalid length");
        return 0;
    }

    DWORD bytes = 0;
    if(!::ReadFile(m_handle, buffer, (DWORD)length, &bytes, NULL)) {
        SetLastError(Util::Win32ErrorString(::GetLastError()));
        return 0;
    }

    return (UINT64)bytes;
}

////////////////////////////////////////////////////////////////////////////////

INT64 Win32FileObject::Write(void *buffer, INT64 length) {
    if(!CheckOpen()) return 0;

    if(length <= 0 || length > 0x7FFFFFFF) {
        SetLastError(L"Invalid length");
        return 0;
    }

    DWORD bytes = 0;
    if(!::WriteFile(m_handle, buffer, (DWORD)length, &bytes, NULL)) {
        SetLastError(Util::Win32ErrorString(::GetLastError()));
        return 0;
    }

    return (UINT64)bytes;
}

////////////////////////////////////////////////////////////////////////////////

bool Win32FileObject::SameFileAs(const FileObject *f) const {
    return f && Util::SameWin32Filenames(GetPath(), f->GetPath());
}

////////////////////////////////////////////////////////////////////////////////
