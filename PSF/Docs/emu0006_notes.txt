------------------------------------------------------------------------------
emu0006 - 23 May 2003

PRE-PSF2 version.

- 11% faster play / 26% faster seek.
  What I don't mention in the public readme is that this was due to a stupid 
  bugfix in spucore's envelope handling.
- Reverb tweaked very slightly (in emu0005+) - downsampler coefficients were
  normalized, I changed the state machine input from /2 to *2/3, and got rid
  of the output scaling. result is kinda not noticable. this essentially means
  the overall reverb depth went from 0.610 to 0.666.

------------------------------------------------------------------------------

No notable changes to the API since emu0005.
