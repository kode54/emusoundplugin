-----------------------------------------------------------------------------
PSF (Playstation Sound Format) specification v1.1
by Neill Corlett
-----------------------------------------------------------------------------

Introduction
------------

The PSF format brings the functionality of NSF, SID, SPC, and GBS to the
Playstation.  PSF utilizes the original music driver code from each game to
replay sequenced music in a perfectly authentic, and size-efficient, way.

A PSF file contains three basic parts:

- A reserved section, which is unused for now.
- A zlib-compressed Playstation EXE file.
  When executed on an actual console, this EXE would simply play the music.
- An ASCII tag containing the title, length, and other relevant information.

More info about zlib is available at http://www.gzip.org/zlib/.

-----------------------------------------------------------------------------

File Layout
-----------

Alignment to any particular data type is not guaranteed in the PSF file.
Exercise appropriate caution.

First 4 bytes: 'P','S','F',version byte
  Version byte: 0x01 for PSF1 (Playstation)
                0x02 for PSF2 (Playstation 2) [PROPOSED]
Next 4 bytes: Size of reserved area (R), little-endian unsigned long
Next 4 bytes: Compressed PSX EXE length (N), little-endian unsigned long
Next 4 bytes: Compressed PSX EXE CRC-32, little-endian unsigned long
Next R bytes: Reserved area (may be empty if R is 0 bytes)
Next N bytes: Compressed PSX EXE, in zlib compress() format.

* For PSF1: Uncompressed size of the EXE must not exceed 2,033,664 bytes

Optionally, an uncompressed tag may follow the compressed EXE.
The tag is in standard ASCII and uses the following format:

- The first 5 bytes must be "[TAG]".  All data that follows is parsed as
  tag information.
- All characters 0x01-0x20 are considered whitespace
- There must be no null (0x00) characters
- 0x0A is the newline character
- Additional lines of the form "variable=value" may follow
- Variable names are case-insensitive and must be valid C identifiers
- Whitespace at the beginning/end of the line and before/after the = are
  ignored
- Blank lines are ignored
- Multiple-line variables must appear as consecutive lines using the same
  variable name.  For instance:

    comment=This is a
    comment=multiple-line
    comment=comment.

- Total size of the tag, not including the initial [TAG] marker, must not
  exceed 50,000 bytes

Example:

[TAG]title=Earth Painting
artist=Yoko Shimomura
game=Legend of Mana
year=1999

-----------------------------------------------------------------------------

PSX EXE Notes
-------------

This is a self-contained executable which, when run on a real console, would
simply play the music.  It's responsible for initializing the SPU, loading
samples, setting up interrupts, etc. - anything a real program must do.

* For PSF1: The executable must be in the standard consumer "PS-X EXE"
  format, which is described below.
* For PSF2: The executable must be in ELF format. [PROPOSED]

Quick reference for the PS-X EXE format:

First 0x800 bytes - header
Next N bytes - text section

Header format:

0x000 (8 bytes): ASCII "PS-X EXE"
0x010 (4 bytes): Initial PC, little-endian unsigned long
0x018 (4 bytes): Text section start address, little-endian unsigned long
0x01C (4 bytes): Text section size, little-endian unsigned long
0x030 (4 bytes): Initial SP ($29), little-endian unsigned long
0x04C: ASCII marker: "Sony Computer Entertainment Inc. for North America area"
  (or similar for other regions)
Everything else is zero.

Text section should probably be a multiple of 2048 bytes.

-----------------------------------------------------------------------------

Tag Notes
---------

The following variable names are predefined:

title, artist, game, year, genre, comment, copyright
  (These are self-explanatory.)

psfby
  The name of the person responsible for creating the .PSF file.  This does
  not imply that said person wrote the music driver code.

volume
  Relative volume of the PSF.  1.0 is the default.  It can by any real
  number.  (What happens if you make it negative?  ...  The phase gets
  inverted so EVERYTHING YOU ONCE KNEW IS WRONG.)

length
fade
  Length of the song, and the length of the ending fadeout.
  These may be in one of three formats:
    seconds.decimal
    minutes:seconds.decimal
    hours:minutes:seconds.decimal
  The decmial portion may be omitted.  Commas are also recognized as decimal
  separators.

The following variables are reserved by my plugin and should probably not be
used:

- filedir, filename, fileext
- Anything starting with _lib (see the minipsf/psflib section)

-----------------------------------------------------------------------------

MiniPSF and PSFLib files
------------------------

For now, this section pertains to PSF1 only.

.minipsf files are regular PSF files which import data from one or more
.psflib files residing in the same directory (for shared driver code, sound
banks, etc.)

.psflib files are also regular PSF files.  They can also recursively import
data from other .psflib files.

This is done via tag variables called _lib, _lib2, _lib3, etc.

The proper way to load a minipsf is as follows:

- Load the PSF EXE data - this becomes the current EXE

- Check for the presence of a "_lib" tag.
  If present:
  - RECURSIVELY load the EXE data from the given library file
    (make sure to limit recursion to avoid crashing - I limit it to 10 levels)
  - Make the _lib EXE the current one.
  - We will use the initial PC/SP from the _lib EXE.
  - Superimpose the originally loaded PSF EXE on top of the current EXE using
    its text start address and text size.

- Check for the presence of "_libN" tags for N=2 and up (use "_lib%d")
  - RECURSIVELY load and superimpose all these EXEs on top of the current
    EXE.  Do not modify the current PC/SP.
  - Start at N=2.  Stop at the first tag name that doesn't exist.

- (done)

EXEs must always be contiguous.  When superimposing one EXE on top of
another, grow the target EXE start/end points as necessary and fill the
unused space with zeroes.

-----------------------------------------------------------------------------

Emulation Notes
---------------

Though this isn't relevant to the actual format, the following information
may prove helpful.  It pertains to PSF1 only.

The following are available in my emulation core, used in the Winamp plugin
and in PSFLab:
- R3000 CPU
- Interrupts and syscalls
- 2MB RAM (mirrored throughout the first 8MB)
- 1KB scratchpad
- SPU
- DMA channel 4 (SPU)
- Root counter 2, VBlank IRQs
- All kernel functions

The following are not available and should not be used or accessed:
- Overflow exceptions
- Breakpoints
- GTE instructions
- GPU, CDROM, SIO, etc. - hardware unrelated to sound
- DMA channels other than 4
- Root counters 0 and 1

The following R3000 code sequence is detected as idle, and may be used to
conserve CPU time on the host side:

  j (current location)
  nop

-----------------------------------------------------------------------------

Where to Find Neill Corlett
---------------------------

email: corlett@lfx.org
web: http://lfx.org/~corlett/

-----------------------------------------------------------------------------
