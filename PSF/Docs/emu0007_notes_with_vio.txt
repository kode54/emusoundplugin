------------------------------------------------------------------------------
emu0007 - 

PSF2 debut version.

------------------------------------------------------------------------------

Many more details in emu.h.

PSF1 uses the same API as emu0006, except emu_upload_program has been renamed
to emu_upload_psxexe.  So you can skip the PSF1 part of the following guide if
you're already familiar with that.

------------------------------------------------------------------------------

THE DEFINITIVE HOWTO for the Highly Experimental emulation core:


General rules
-------------

You must call emu_init() before doing absolutely anything at all.  The only
call which is safe to make before emu_init is emu_getversion.

emu_getversion() returns a pointer to a string containing the library version
and build date.  You may want to consider displaying this in your plugin so
that users can easily match library versions across different plugins.

You may get a NULL dereference if you forget to call emu_init or if the
library was compiled wrong.  I do this to catch my mistakes.

Everything after emu_init is accomplished by way of "states".  You must
manage space for these states yourself (the library contains no malloc or
free calls).  Most library calls take a state pointer as the first parameter.
Commonly, you're only going to need one state to play one PSF.

The library contains no thread synchronization calls.  However, it's safe to
use different states simultaneously from different threads, as they'll never
touch each other.  No static data is written after emu_init.

States may be relocated anywhere in memory, as long as they maintain 8-byte
alignment.  They contain no self-referential pointers.  It's safe to treat
them as raw data.

States are only guaranteed valid within the currently-running process, and
should not be saved or loaded from a file.  In fact, it is a security hazard
to write _any_ data into a state, except data taken from another state.


For PSF1 playing
----------------

First, create a state and clear it using version 1:

  void *myemustate = malloc(emu_get_state_size(1));
  emu_clear_state(myemustate, 1);

Next, upload your PS-X EXE.  You can do this in two ways:

- Upload it from a memory buffer which INCLUDES the header:

  emu_upload_psxexe(myemustate, psxexe_ptr, psxexe_size);

*OR*

- Reach directly into memory/registers using iop_upload_to_ram and
  r3000_setreg:

  void *myiopstate = emu_get_iop_state(myemustate);
  void *myr3000state = iop_get_r3000_state(myiopstate);
  iop_upload_to_ram(myiopstate, psx_address, buffer, len);
  r3000_setreg(myr3000state, R3000_REG_something, value); // see r3000.h

Finally, play using the emu_execute function:

  signed short sound_buf[2 * 1000];
  unsigned sound_samples = 1000;
  int c = emu_execute(myemustate, 0x7FFFFFFF, sound_buf, &sound_samples, 0);

The return value (c) is:
  0 or positive: Success
  -1: Graceful exit (the EXE simply finished)
  -2 or lower: An error occurred

sound_buf points to an interleaved (left, right) 16-bit signed integer PCM
buffer in the native byte order.

sound_samples contains the number of samples (left/right pairs) in the
buffer.  On return, it will be set to the number of samples that were
actually generated.  This number may be ZERO, or LESS than the number you
requested.  For this reason, you should probably use a loop such as:

  signed short sound_buf[2 * 1000];
  unsigned sound_samples_needed = 1000;
  unsigned sound_samples_generated = 0;
  while(sound_samples_generated < sound_samples_needed) {
    unsigned sound_samples = sound_samples_needed - sound_samples_generated;
    int c = emu_execute(
      myemustate, 0x7FFFFFFF, sound_buf + 2 * sound_samples_generated,
      &sound_samples, 0
    );
    sound_samples_generated += sound_samples;
    if(c < 0) break;
  }

------------------------------------------------------------------------------

For PSF2 playing
----------------

Create a state and clear it, as before, but using version 2:

  void *myemustate = malloc(emu_get_state_size(2));
  emu_clear_state(myemustate, 2);

Now instead of uploading the program directly, you will be registering a
number of virtual I/O callbacks, allowing the emulated PS2 environment to load
the necessary files _by itself_.  The functions are as follows:

  sint32 EMU_CALL virtual_open(void *context, const char *path);

    Open a file for reading (O_RDONLY is implied).

    Return value:
    - On success, return its file descriptor.  You can make up any descriptors
      you want, but they must be zero-or-positive 32-bit integers.
    - If the file doesn't exist, return -2.
    - If there's no room for another open file, return -24.

  sint32 EMU_CALL virtual_close(void *context, sint32 fd);

    Close a file descriptor.

    Return value:
    - On success, return 0.
    - If fd is invalid, return -9.

  sint32 EMU_CALL virtual_read(void *context, sint32 fd, uint8 *buffer,
    sint32 len);

    Read from an open file descriptor.  Don't worry about byte order issues;
    the library will handle them.

    Return value:
    - On success, return the number of bytes that were actually read.
    - If fd is invalid, return -9.
    - If len is negative, return -22.

  sint32 EMU_CALL virtual_lseek(void *context, sint32 fd, sint32 offset,
    sint32 whence);

    Seek within an open file descriptor.
    - If whence==0 (SEEK_SET): Set the offset directly
    - If whence==1 (SEEK_CUR): Add the offset to the current offset
    - If whence==2 (SEEK_END): Set the offset relative to the end of the file
      (positive values seek past the end of the file, negative before).

    Return value:
    - On success, return the new current offset.
    - If fd is invalid, return -9.
    - If whence wasn't 0, 1, or 2, return -22.
    - If the resulting offset would be negative, do not set it, and instead
      return -22.

The context parameter is described below.

You may also return -5 from any of the functions to indicate a fatal error.
This special value will cause emu_execute to immediately turn around and
return an error (something -2 or lower).

Store pointers to these functions in a EMU_VIO_TABLE struct,
for example:

  struct EMU_VIO_TABLE mycallbacks = {
    my_virtual_open,
    my_virtual_close,
    my_virtual_read,
    my_virtual_lseek,
  };

Then you can register this table with an emulation state by calling
emu_set_vio:

  emu_set_vio(state, &mycallbacks, mycontext_ptr);

Whatever you specify as mycontext_ptr will end up reflected as the "context"
parameter to subsequent virtual I/O callbacks.

With the virtual I/O set up, the only thing left to do is execute.  You can
use the emu_execute function with PSF2 in the same way as PSF1.

------------------------------------------------------------------------------
