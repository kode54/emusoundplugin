-----------------------------------------------------------------------------
Neill Corlett
Playstation SPU envelope timing notes
-----------------------------------------------------------------------------

This is preliminary.  This may be wrong.  But the model described herein fits
all of my experimental data, and it's just simple enough to sound right.

ADSR envelope level ranges from 0x00000000 to 0x7FFFFFFF internally.
The value returned by channel reg 0xC is (envelope_level>>16).

Each sample, an increment or decrement value will be added to or
subtracted from this envelope level.

Create the rate log table.  The values double every 4 entries.
   entry #0 = 4

    4, 5, 6, 7,
    8,10,12,14,
   16,20,24,28, ...

   entry #40 = 4096...
   entry #44 = 8192...
   entry #48 = 16384...
   entry #52 = 32768...
   entry #56 = 65536...

increments and decrements are in terms of ratelogtable[n]
n may exceed the table bounds (plan on n being between -32 and 127).
table values are all clipped between 0x00000000 and 0x3FFFFFFF

when you "voice on", the envelope is always fully reset.
(yes, it may click. the real thing does this too.)

envelope level begins at zero.

each state happens for at least 1 cycle
(transitions are not instantaneous)
this may result in some oddness: if the decay rate is uberfast, it will cut
the envelope from full down to half in one sample, potentially skipping over
the sustain level

ATTACK
------
- if the envelope level has overflowed past the max, clip to 0x7FFFFFFF and
  proceed to DECAY.

Linear attack mode:
- line extends upward to 0x7FFFFFFF
- increment per sample is ratelogtable[(Ar^0x7F)-0x10]

Logarithmic attack mode:
if envelope_level < 0x60000000:
  - line extends upward to 0x60000000
  - increment per sample is ratelogtable[(Ar^0x7F)-0x10]
else:
  - line extends upward to 0x7FFFFFFF
  - increment per sample is ratelogtable[(Ar^0x7F)-0x18]

DECAY
-----
- if ((envelope_level>>27)&0xF) <= Sl, proceed to SUSTAIN.
  Do not clip to the sustain level.
- current line ends at (envelope_level & (~0x07FFFFFF))
- decrement per sample depends on (envelope_level>>28)&0x7
  0: ratelogtable[(4*(Dr^0x1F))-0x18+0]
  1: ratelogtable[(4*(Dr^0x1F))-0x18+4]
  2: ratelogtable[(4*(Dr^0x1F))-0x18+6]
  3: ratelogtable[(4*(Dr^0x1F))-0x18+8]
  4: ratelogtable[(4*(Dr^0x1F))-0x18+9]
  5: ratelogtable[(4*(Dr^0x1F))-0x18+10]
  6: ratelogtable[(4*(Dr^0x1F))-0x18+11]
  7: ratelogtable[(4*(Dr^0x1F))-0x18+12]
  (note that this is the same as the release rate formula, except that
   decay rates 10-1F aren't possible... those would be slower in theory)

SUSTAIN
-------
- no terminating condition except for voice off
- Sd=0 (increase) behavior is identical to ATTACK for both log and linear.
- Sd=1 (decrease) behavior:
Linear sustain decrease:
- line extends to 0x00000000
- decrement per sample is ratelogtable[(Sr^0x7F)-0x0F]
Logarithmic sustain decrease:
- current line ends at (envelope_level & (~0x07FFFFFF))
- decrement per sample depends on (envelope_level>>28)&0x7
  0: ratelogtable[(Sr^0x7F)-0x1B+0]
  1: ratelogtable[(Sr^0x7F)-0x1B+4]
  2: ratelogtable[(Sr^0x7F)-0x1B+6]
  3: ratelogtable[(Sr^0x7F)-0x1B+8]
  4: ratelogtable[(Sr^0x7F)-0x1B+9]
  5: ratelogtable[(Sr^0x7F)-0x1B+10]
  6: ratelogtable[(Sr^0x7F)-0x1B+11]
  7: ratelogtable[(Sr^0x7F)-0x1B+12]

RELEASE
-------
- if the envelope level has overflowed to negative, clip to 0 and QUIT.

Linear release mode:
- line extends to 0x00000000
- decrement per sample is ratelogtable[(4*(Rr^0x1F))-0x0C]

Logarithmic release mode:
- line extends to (envelope_level & (~0x0FFFFFFF))
- decrement per sample depends on (envelope_level>>28)&0x7
  0: ratelogtable[(4*(Rr^0x1F))-0x18+0]
  1: ratelogtable[(4*(Rr^0x1F))-0x18+4]
  2: ratelogtable[(4*(Rr^0x1F))-0x18+6]
  3: ratelogtable[(4*(Rr^0x1F))-0x18+8]
  4: ratelogtable[(4*(Rr^0x1F))-0x18+9]
  5: ratelogtable[(4*(Rr^0x1F))-0x18+10]
  6: ratelogtable[(4*(Rr^0x1F))-0x18+11]
  7: ratelogtable[(4*(Rr^0x1F))-0x18+12]

-----------------------------------------------------------------------------
