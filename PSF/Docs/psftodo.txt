HIGHLY EXPERIMENTAL:

DONE:
- Fix playlist advance for nonexistent files





PSFLAB:

TODO:
- Make pageup/pagedown key behavior a little less gay
- Fix the no-text-in-a-pane scrolling bug
- Fix behavior when dealing with unaligned data
- Help file?
- Fix the splitter ugliness
- Put all language-specific strings in resources
- Unicode-proofing, in the rare event I'd ever want to compile it in Unicode mode
- Rip every last PSF from every last game that ever existed

DONE:
- Fix what happens when the user selects a non-monospace font
- Breakpoints
- Live double-clicks in call stack/event windows
- Saved registry settings (font, window/splitter sizes, colors, options)
- Color select dialog
- Make the current color scheme the default
- Tag
- Save PSF
- Save As
- Import binary
- Import PS-X EXE
- Export PS-X EXE
- Add a "reset defaults" button to the color dialog
- Make the default commands in the context menus bold


CORE:

TODO:
- Investigate click bug on suikoden 2 - silver wolf, and other songs?
- PS2 support.
