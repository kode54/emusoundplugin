////////////////////////////////////////////////////////////////////////////////
//
// CPSFPlugin: An EmuSoundPlugin specifically for Portable Sound Format files.
//
// Handles title formatting, and most tag/metadata related stuff.
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../../EmuSoundPlugin/EmuSoundPlugin.h"

#include "PSFTag.h"

#include "../../MFCUtil/File.h"

class CPSFPlugin : public CEmuSoundPlugin  
{
public:
    virtual void InfoDialog(File f, HWND hWndParent, LONG *plNowMS);
    virtual CMusicFileTag* GetMetaData(File f, CStringW &strTitle, INT64 &nLengthMS);
    virtual CMusicFileTag* NewMetaData(File f);

	virtual LPCWSTR* GetAllPSFTypes();
	virtual LPCWSTR GetPSFType(int nPSFVersion);
	virtual int GetPSFLogoBitmap(File f, CPSFTag *pPSFTag, int nVersion);

	CPSFPlugin();
	virtual ~CPSFPlugin();

    CStringW opt_strTitleFormat;
    CStringW opt_strTitleFormatFallback;

    static int GrabPSFVersion(File f, CStringW &strError);

protected:
    ESP_OPTION_DECLARE

    virtual void BuildConfigPropertySheet(CPropertySheet *pSheet);

};
