// PSFCompressor.cpp: implementation of the CPSFCompressor class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "PSFCompressor.h"

// needed for verify
#include "PSFDecompressor.h"

#include "../../zlib/zlib.h"
#include "../../7z/7z.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPSFCompressor::CPSFCompressor()
{

}

CPSFCompressor::~CPSFCompressor()
{

}

//////////////////////////////////////////////////////////////////////////////
//
// Get the version string for a given method
//
CStringW CPSFCompressor::GetMethodVersion(UINT uMethod)
{
    CStringW ver;

    switch(uMethod) {
    case METHOD_ZLIB_STORE:
    case METHOD_ZLIB_9:
        ver.Format(
            L"zlib %s compress2 level %d",
            LPCWSTR(CStringW(zlibVersion())), 
            (uMethod == METHOD_ZLIB_9) ? 9 : 0
        );
        break;
    case METHOD_7ZIP_NORMAL : ver = L"7-Zip AdvanceCOMP 1.5 shrink_normal"  ; break;
    case METHOD_7ZIP_EXTRA  : ver = L"7-Zip AdvanceCOMP 1.5 shrink_extra"   ; break;
    case METHOD_7ZIP_EXTREME: ver = L"7-Zip AdvanceCOMP 1.5 shrink_extreme" ; break;
    default:
        ver = L"Unknown method (program error)";
    }

    return ver;
}

//////////////////////////////////////////////////////////////////////////////
//
// Calculate the maximum compressed size of a given uncompressed data block
// using a given method
//
DWORD CPSFCompressor::GetMaxCompressedSize(DWORD dwUncompressedSize, UINT uMethod)
{
    switch(uMethod) {
    case METHOD_ZLIB_STORE:
    case METHOD_ZLIB_9:
        // this formula is from the zlib manual
        return (dwUncompressedSize) + ((dwUncompressedSize + 999) / 1000) + 12;
    case METHOD_7ZIP_NORMAL:
    case METHOD_7ZIP_EXTRA:
    case METHOD_7ZIP_EXTREME:
        // TODO: figure out what this really should be.
        // it'd be awfully nice if 7zip had any documentation whatsoever.
        return dwUncompressedSize + (dwUncompressedSize / 8) + 1024;
    default:
        // invalid input, but we have no way to report errors, so just return
        // something reasonable
        return dwUncompressedSize + (dwUncompressedSize / 8) + 1024;
    }
}

//////////////////////////////////////////////////////////////////////////////
//
// Compress using the given method, then verify
//
int CPSFCompressor::Compress(LPVOID lpDest, DWORD *pdwDestSize, LPVOID lpSrc, DWORD dwSrcSize, UINT uMethod)
{
    int r;
    // attempt compression depending on method
    switch(uMethod) {
    case METHOD_ZLIB_STORE  : r = CompressZLIB(lpDest, pdwDestSize, lpSrc, dwSrcSize, 0); break;
    case METHOD_ZLIB_9      : r = CompressZLIB(lpDest, pdwDestSize, lpSrc, dwSrcSize, 9); break;
    case METHOD_7ZIP_NORMAL : r = Compress7zip(lpDest, pdwDestSize, lpSrc, dwSrcSize, 1, 64); break;
    case METHOD_7ZIP_EXTRA  : r = Compress7zip(lpDest, pdwDestSize, lpSrc, dwSrcSize, 3, 128); break;
    case METHOD_7ZIP_EXTREME: r = Compress7zip(lpDest, pdwDestSize, lpSrc, dwSrcSize, 5, 255); break;
    default:
        m_strLastError.Format(L"Invalid compression method selected: %u (program error)", uMethod);
        return RESULT_FATAL_ERROR;
    }
    // if that returned an error, return with an error (same string)
    if(r != RESULT_OK) return r;

    // otherwise verify
    if(!Verify(lpDest, *pdwDestSize, lpSrc, dwSrcSize)) {
        m_strLastError.Format(L"Verification failed on compress method %u (program error)", uMethod);
        return RESULT_FATAL_ERROR;
    }

    // if that worked, success
    return RESULT_OK;
}

//////////////////////////////////////////////////////////////////////////////
//
// Verify a compressed block by decompressing it again and comparing it with
// the original.
//
// This DOES NOT set m_strLastError; it merely returns TRUE or FALSE.
//
BOOL CPSFCompressor::Verify(LPVOID lpComp, DWORD dwCompSize, LPVOID lpOrig, DWORD dwOrigSize)
{
    // allocate some space for verification
    CByteArray aTemp;
    DWORD dwTempSize = dwOrigSize;
    aTemp.SetSize(dwTempSize);

    CPSFDecompressor d;
    int r = d.Decompress(aTemp.GetData(), &dwTempSize, lpComp, dwCompSize);

    // if that failed, we fail
    if(r != CPSFDecompressor::RESULT_OK) return FALSE;

    // if the size changed at all, we fail
    if(dwTempSize != dwOrigSize) return FALSE;

    // finally, the lengthy memory comparison
    if(memcmp(aTemp.GetData(), lpOrig, dwOrigSize)) return FALSE;

    // otherwise, success
    return TRUE;
}

//////////////////////////////////////////////////////////////////////////////
//
// Compress using zlib with a given compression level (0-9)
//
int CPSFCompressor::CompressZLIB(LPVOID lpDest, DWORD *pdwDestSize, LPVOID lpSrc, DWORD dwSrcSize, int nLevel)
{
    int r = compress2((LPBYTE)lpDest, pdwDestSize, (LPBYTE)lpSrc, dwSrcSize, nLevel);
    switch(r) {
    case Z_OK:
        return RESULT_OK;
    case Z_MEM_ERROR:
        m_strLastError = L"Out of memory";
        return RESULT_FATAL_ERROR;
    case Z_BUF_ERROR:
        m_strLastError = L"Not enough space in the compression buffer";
        return RESULT_SIZE_ERROR;
    case Z_STREAM_ERROR:
        m_strLastError = L"CPSFCompressor::CompressZLIB - Z_STREAM_ERROR (program error)";
        return RESULT_FATAL_ERROR;
    default:
        m_strLastError.Format(L"Unknown error (%d)", r);
        return RESULT_FATAL_ERROR;
    }
}

//////////////////////////////////////////////////////////////////////////////
//
// Compress using AdvanceCOMP's compress_rfc1950_7z
//
int CPSFCompressor::Compress7zip(LPVOID lpDest, DWORD *pdwDestSize, LPVOID lpSrc, DWORD dwSrcSize, int nPasses, int nFastBytes) {
    bool b;
    unsigned int out_size = *pdwDestSize;
    b = compress_rfc1950_7z(
        (const unsigned char*)lpSrc,
        dwSrcSize,
        (unsigned char*)lpDest,
        out_size,
        nPasses,
        nFastBytes
    );
    *pdwDestSize = out_size;

    if(!b) {
        m_strLastError = L"compress_rfc1950_7z failed (unknown error)";
        return RESULT_FATAL_ERROR;
    }

    return RESULT_OK;
}

//////////////////////////////////////////////////////////////////////////////
