#pragma once

class CPSFDecompressor  
{
public:
	int Decompress(LPVOID lpDest, DWORD *pdwDestSize, LPVOID lpSrc, DWORD dwSrcSize);
	static DWORD CRC32(LPVOID lpData, DWORD dwSize);

    CPSFDecompressor();
	virtual ~CPSFDecompressor();

    enum {
        RESULT_OK          = 0,
        RESULT_SIZE_ERROR  = 1,
        RESULT_FATAL_ERROR = 2
    };

    CStringW GetLastError() const { return m_strLastError; }

    static CStringW GetVersion();

private:
	CStringW m_strLastError;
};
