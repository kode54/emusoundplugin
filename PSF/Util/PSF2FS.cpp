////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "PSF2FS.h"

#include "PSFTag.h"

#include "../../MFCUtil/Endian.h"

#include "../../zlib/zlib.h"

////////////////////////////////////////////////////////////////////////////////

CPSF2FS::CPSF2FS() {
    m_sources = NULL;
    m_root = NULL;
    m_adderror = false;
}

CPSF2FS::~CPSF2FS() {
    if(m_sources) FreeSources(m_sources);
    if(m_root) FreeDir(m_root);
}

CPSF2FS::SourceFile::SourceFile() {
    reserved_size = 0;
    next = NULL;
}

CPSF2FS::SourceFile::~SourceFile() {
}

CPSF2FS::DirEntry::DirEntry(DirEntry *n)
{
    name[0] = 0;
    sub = NULL;
    length = 0;
    block_size = 0;
    source = NULL;
    next = n;
}

CPSF2FS::DirEntry::~DirEntry() {
}

////////////////////////////////////////////////////////////////////////////////

void CPSF2FS::FreeSources(SourceFile *s) {
    while(s) {
        SourceFile *n = s->next;
        delete s;
        s = n;
    }
}

void CPSF2FS::FreeDir(DirEntry *d) {
    while(d) {
        DirEntry *n = d->next;
        if(d->sub) FreeDir(d->sub);
        delete d;
        d = n;
    }
}

////////////////////////////////////////////////////////////////////////////////

static bool isdirsep(WCHAR c) { return (c == '/' || c == '\\'); }

/////////////////////////////////////////////////////////////////////////////

CPSF2FS::DirEntry* CPSF2FS::FindDirEntry(DirEntry *dir, LPCSTR name, int name_l)
{
    if(name_l > 36) return NULL;
    while(dir) {
        if(!memicmp(dir->name, name, name_l) && dir->name[name_l] == 0) return dir;
        dir = dir->next;
    }
    return NULL;
}

/////////////////////////////////////////////////////////////////////////////
//
// Make a DIR_ENTRY list for a given file and Reserved offset.
// Recurses subdirectories also.
// All entries are set to point to the given SOURCE_FILE.
//
CPSF2FS::DirEntry* CPSF2FS::ReadDirectory(File &f, int offset, SourceFile *source)
{
    DirEntry *dir = NULL;
    int n, num;
    if(offset < 0) goto corrupt;
    if(offset >= source->reserved_size) goto corrupt;
    if((offset + 4) > source->reserved_size) goto corrupt;

    f.Seek(16 + offset, File::BEGIN);
    if(!f.Read32LSB(num)) goto fileerror;
    offset += 4;
    if(num < 0) goto corrupt;
    for(n = 0; n < num; n++) {
        int o, u, b;
        if((offset + 48) > source->reserved_size) goto corrupt;
        dir = new DirEntry(dir);

        f.Seek(16 + offset, File::BEGIN);

        if(f.Read(dir->name, 36) != 36) goto fileerror;
        if(!f.Read32LSB(o)) goto fileerror;
        if(!f.Read32LSB(u)) goto fileerror;
        if(!f.Read32LSB(b)) goto fileerror;

        offset += 48;

        if(o < 0) goto corrupt;
        if(u < 0) goto corrupt;
        if(b < 0) goto corrupt;

        if(o && o < offset) goto corrupt;

        // if this new entry describes a subdirectory:
        if(u == 0 && b == 0 && o != 0) {
            dir->sub = ReadDirectory(f, o, source);
            if(m_adderror) goto error;
        // if this new entry describes a zero-length file:
        } else if(u == 0 || b == 0 || o == 0) {
            // fields were zero anyway
        // if this new entry describes a real source file:
        } else {
            int i;
            int blocks = (u + (b-1)) / b;
            int dataofs = o + 4 * blocks;
            if(dataofs >= source->reserved_size) goto corrupt;
            // record the info
            dir->length = u;
            dir->block_size = b;
            dir->source = source;
            dir->offset_table.SetSize(blocks + 1);
            for(i = 0; i < blocks; i++) {
                int cbs;
                if((o + 4) > source->reserved_size) goto corrupt;
                f.Seek(16 + o, File::BEGIN);
                if(!f.Read32LSB(cbs)) goto fileerror;
                o += 4;
                dir->offset_table.SetAt(i, dataofs);
                dataofs += cbs;
            }
            dir->offset_table.SetAt(i, dataofs);
        }
    }

    return dir;

corrupt:
    m_strLastError += L"Virtual directory is corrupt";
    goto error;
fileerror:
    m_strLastError += f.GetLastError();
    goto error;
error:
    FreeDir(dir);
    m_adderror = true;
    return NULL;
}

/////////////////////////////////////////////////////////////////////////////
//
// Merge two SourceFile lists.
// Guaranteed to succeed and not to free anything.
//
CPSF2FS::SourceFile* CPSF2FS::MergeSource(SourceFile *to, SourceFile *from)
{
    SourceFile *to_tail;
    if(!to && !from) return NULL;
    if(!to) {
        SourceFile *t = to; to = from; from = t;
    }
    to_tail = to;
    while(to_tail->next) { to_tail = to_tail->next; }
    to_tail->next = from;
    return to;
}

/////////////////////////////////////////////////////////////////////////////
//
// Merge two DIR_ENTRY lists.
// Guaranteed to succeed. May free some structures.
//
CPSF2FS::DirEntry* CPSF2FS::MergeDir(DirEntry *to, DirEntry *from)
{
    // will traverse "from", and add to "to".
    while(from) {
        DirEntry *entry_to;
        DirEntry *entry_from;
        entry_from = from;
        from = from->next;
        // delink entry_from
        entry_from->next = NULL;
        // look for a duplicate entry in "to"
        entry_to = FindDirEntry(to, entry_from->name, strlen(entry_from->name));
        // if there is one, do something fancy and then free entry_from.
        if(entry_to) {
            // if both are subdirs, merge the subdirs
            if((entry_to->sub) && (entry_from->sub)) {
                entry_to->sub = MergeDir(entry_to->sub, entry_from->sub);
                entry_from->sub = NULL;
            // if both are files, copy over the info
            } else if((!(entry_to->sub)) && (!(entry_from->sub))) {
                entry_to->length     = entry_from->length;
                entry_to->block_size = entry_from->block_size;
                entry_to->source     = entry_from->source;
                entry_to->offset_table.Copy(entry_from->offset_table);
            // if one's a subdir but the other's not, we lose "from". this is fine.
            }
            FreeDir(entry_from);
            entry_from = NULL;
        // otherwise, just relink to the top of "to"
        } else {
            entry_from->next = to;
            to = entry_from;
        }
    }
    return to;
}

/////////////////////////////////////////////////////////////////////////////
//
// Returns true on success
// only modifies *psource and *pdir on success
//
bool CPSF2FS::AddArchiveInternal(
    File f,
    BYTE compare_version,
    int level,
    SourceFile **psource,
    DirEntry **pdir
) {
    CPSFTag tag;
    int r, fl, tag_ofs, tag_bytes;
    unsigned char header[16];
    unsigned char tagmarker[5];
    int reserved_size;
    int program_size;
    bool skip_tag = false;
    int libnum = 1;
    // these will accumulate info from the various _libs
    SourceFile *source = NULL;
    DirEntry *dir = NULL;
    // these relate to the current file
    SourceFile *this_source = NULL;
    DirEntry *this_dir = NULL;

    // default to no error
    m_adderror = false;

    // Error message prefix
    m_strLastError = (level ? L"While loading a _lib file: " : L"");

    if(level >= 10) {
        m_strLastError += L"Recursion limit reached";
        goto error;
    }

dofile:
    if(!f.Open(File::READ)) goto fileerror;

    fl = f.Length();
    if(fl < 16) goto invalidformat;

    if(f.Read(header, 16) != 16) goto fileerror;
    if((memcmp(header, "PSF", 3)) || (header[3] != compare_version)) goto invalidformat;

    reserved_size = Util::Get32LSB(header + 4);
    program_size  = Util::Get32LSB(header + 8);

    if(reserved_size < 0 ) goto invalidformat;
    if(program_size  < 0 ) goto invalidformat;
    if(reserved_size > fl) goto invalidformat;
    if(program_size  > fl) goto invalidformat;

    tag_ofs = 16 + reserved_size + program_size;
    if(tag_ofs < 16) goto invalidformat;
    if(tag_ofs > fl) goto invalidformat;

    tag_bytes = fl - (tag_ofs + 5);
    if(tag_bytes > 50000) tag_bytes = 50000;
    if(tag_bytes <= 0) goto notag;

    if(skip_tag) goto notag;

    f.Seek(tag_ofs, File::BEGIN);

    if(f.Read(tagmarker, 5) != 5) goto notag;
    if(memcmp(tagmarker, "[TAG]", 5)) goto notag;

    {   char *tagbuffer = tag.StartModifying();
        tagbuffer[tag_bytes] = 0;
        if(f.Read(tagbuffer, tag_bytes) != tag_bytes) goto fileerror;
        tag.StopModifying();
    }
    f.Close();

    for(libnum = 1;; libnum++) {
        CStringW libstr;

        if(libnum == 1) {
            libstr = L"_lib";
        } else {
            libstr.Format(L"_lib%d", libnum);
        }

        CStringW lib_entry = tag.GetVarW(libstr);
        if(lib_entry.IsEmpty()) break;

        if(
            !AddArchiveInternal(
                File(f, CPSF::MakeLibPath(f.GetPath(), lib_entry)),
                compare_version,
                level + 1,
                &source,
                &dir
            )
        ) goto error;
    }
    skip_tag = true;
    goto dofile;

notag:
    // create a source entry for this psf2
    this_source = new SourceFile;
    this_source->file = f;
    this_source->reserved_size = reserved_size;
    this_dir = ReadDirectory(f, 0, this_source);
    if(m_adderror) goto error;
    f.Close();

    // success
    // now merge everything
    *psource = MergeSource(source, this_source);
    *pdir = MergeDir(dir, this_dir);
//success:
    return true;

invalidformat:
    m_strLastError += L"File format is invalid";
    goto error;
fileerror:
    m_strLastError += f.GetLastError();
    goto error;
error:
    f.Close();
    if(dir        ) FreeDir    (dir        );
    if(source     ) FreeSources(source     );
    if(this_dir   ) FreeDir    (this_dir   );
    if(this_source) FreeSources(this_source);

    return false;
}

////////////////////////////////////////////////////////////////////////////////
//
//
//
bool CPSF2FS::AddArchive(File f, BYTE compare_version) {
    return AddArchiveInternal(f, compare_version, 0, &m_sources, &m_root);
}

////////////////////////////////////////////////////////////////////////////////
//
//
//
int CPSF2FS::VirtualReadInternal(DirEntry *entry, int offset, char *buffer, int length)
{
    int length_read = 0;
    int r;
    if(offset >= entry->length) return 0;
    if((offset + length) > entry->length) length = entry->length - offset;
    while(length_read < length) {
        // get info on the current block
        int blocknum = offset / entry->block_size;
        int ofs_within_block = offset % entry->block_size;
        int canread;
        int block_zofs  = entry->offset_table[blocknum];
        int block_zsize = entry->offset_table[blocknum+1] - block_zofs;
        int block_usize;
        if(block_zofs <= 0 || block_zofs >= entry->source->reserved_size) goto bounds;
        if((block_zofs+block_zsize) > entry->source->reserved_size) goto bounds;

        // get the actual uncompressed size of this block
        block_usize = entry->length - (blocknum * entry->block_size);
        if(block_usize > entry->block_size) block_usize = entry->block_size;

        // if it's not already in the cache block, read it
        if(
            (m_cache_source != entry->source) ||
            (m_cache_offset != block_zofs)
        ) {
            CByteArray zdata;
            zdata.SetSize(block_zsize);

            File f = entry->source->file;

            if(!f.Open(File::READ)) {
                m_strLastError = f.GetLastError();
                goto error;
            }
            f.Seek(16 + block_zofs, File::BEGIN);
            if(f.Read(zdata.GetData(), block_zsize) != block_zsize) {
                m_strLastError = L"Error reading virtual block: ";
                m_strLastError += f.GetLastError();
                goto error;
            }
            f.Close();

            // invalidate cache first
            m_cache_source = NULL;

            m_cache.SetSize(block_usize);

            unsigned long destlen = block_usize;

            // attempt decompress
            r = ::uncompress(m_cache.GetData(), &destlen, zdata.GetData(), block_zsize);

            if(r != Z_OK || destlen != block_usize) {
                m_strLastError = L"Error decompressing virtual block";
                goto error;
            }

            // mark cache as valid
            m_cache_source = entry->source;
            m_cache_offset = block_zofs;
        }

        // at this point, we can read whatever we want out of the cacheblock
        canread = m_cache.GetSize() - ofs_within_block;
        if(canread > (length - length_read)) canread = length - length_read;

        // copy
        memcpy(buffer, m_cache.GetData() + ofs_within_block, canread);

        // advance pointers/counters
        offset += canread;
        length_read += canread;
        buffer += canread;
    }

//success:
    return length_read;

bounds:
    m_strLastError = L"Virtual file block out of bounds";
    goto error;
error:
    // if cacheblock was invalidated, we can free it
    if(!m_cache_source) m_cache.SetSize(0);

    return -1;
}

////////////////////////////////////////////////////////////////////////////////
//
//
//
int CPSF2FS::VirtualRead(LPCSTR path, int offset, char *buffer, int length)
{
    DirEntry *entry = m_root;

    if(!path) goto invalidarg;
    if(offset < 0) goto invalidarg;
    if(!buffer) goto invalidarg;
    if(length < 0) goto invalidarg;

    for(;;) {
        int l;
        int need_dir;
        if(!entry) goto pathnotfound;
        while(isdirsep(*path)) path++;
        for(l = 0;; l++) {
            if(!path[l]) { need_dir = 0; break; }
            if(isdirsep(path[l])) { need_dir = 1; break; }
        }
        entry = FindDirEntry(entry, path, l);
        if(!entry) goto pathnotfound;
        if(!need_dir) break;
        entry = entry->sub;
        path += l;
    }

    // if we "found" a file but it's a directory, then we didn't find it
    if(entry->sub) goto pathnotfound;

    // special case: if requested length is 0, return the total file length
    if(!length) return entry->length;

    // otherwise, read from source
    return VirtualReadInternal(entry, offset, buffer, length);

pathnotfound:
    m_strLastError = L"Path not found";
    goto error;
invalidarg:
    m_strLastError = L"Invalid argument";
    goto error;
error:
    return -1;
}

////////////////////////////////////////////////////////////////////////////////
