// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__3B0B7890_2C12_4AC0_80F8_7166C76BA563__INCLUDED_)
#define AFX_STDAFX_H__3B0B7890_2C12_4AC0_80F8_7166C76BA563__INCLUDED_

// set WINVER - this warning came up when I migrated the project from VC++ 6 to VS.NET 7.
// we'd like all compiled programs to run on Win95 or higher.
#ifndef WINVER
#define WINVER 0x0400
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afx.h>
#include <afxwin.h>

#include <afxcmn.h>
#include <afxdlgs.h>

#include <afxmt.h>

// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__3B0B7890_2C12_4AC0_80F8_7166C76BA563__INCLUDED_)
