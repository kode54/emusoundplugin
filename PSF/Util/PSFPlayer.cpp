////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

////////////////////////////////////////////////////////////////////////////////

#include "PSFPlayer.h"

#include "../../EmuSoundPlugin/EmuSoundPlugin.h"

#include "../../MFCUtil/NumString.h"
#include "../../MFCUtil/File.h"

#include "PSF.h"
#include "PSFTag.h"

////////////////////////////////////////////////////////////////////////////////

CPSFPlayer::CPSFPlayer(File f) : CPlayer(f)
{
}

CPSFPlayer::~CPSFPlayer()
{
}

////////////////////////////////////////////////////////////////////////////////

#define SCALE_MIN (0.0000000001)

static double safe_peak_to_maxvol(double peak) {
    if(peak < 0.0) peak = -peak;
    if(peak < SCALE_MIN) peak = SCALE_MIN;
    return 1.0 / peak;
}

////////////////////////////////////////////////////////////////////////////////

void CPSFPlayer::ReloadTag()
{
    CStringW strTitle;
    INT64 nLengthMS;
    CMusicFileTag *mftag = g_pPlugin->GetMetaData(GetFile(), strTitle, nLengthMS);
    if(!mftag) return;

    // make sure it's a CPSFTag
    if(strcmp(mftag->GetTagType(), "PSF")) { delete mftag; return; }
    CPSFTag *psftag = (CPSFTag*)mftag;

    m_dSongLength = -1.0;
    m_dSongFade   =  0.0;
    m_dManualVolume  = 1.0;
    m_bHasRGTrackVolume = FALSE;
    m_bHasRGAlbumVolume = FALSE;
    m_bHasRGTrackMaxVol = FALSE;
    m_bHasRGAlbumMaxVol = FALSE;
    m_dRGTrackVolume = 1.0;
    m_dRGAlbumVolume = 1.0;
    m_dRGTrackMaxVol = 1.0;
    m_dRGAlbumMaxVol = 1.0;

    CStringW s;
    s = psftag->GetVarW(L"length"); if(!s.IsEmpty()) Util::StringToNum(s, m_dSongLength   , Util::NSFLAG_COLONS);
    s = psftag->GetVarW(L"fade"  ); if(!s.IsEmpty()) Util::StringToNum(s, m_dSongFade     , Util::NSFLAG_COLONS);
    s = psftag->GetVarW(L"volume"); if(!s.IsEmpty()) Util::StringToNum(s, m_dManualVolume , 0);

    m_bHasRGTrackVolume = psftag->GetValue(m_dRGTrackVolume, CMusicFileTag::VALUE_RG_TRACK_VOLUME);
    m_bHasRGAlbumVolume = psftag->GetValue(m_dRGAlbumVolume, CMusicFileTag::VALUE_RG_ALBUM_VOLUME);
    double t_peak = 1.0;
    double a_peak = 1.0;
    m_bHasRGTrackMaxVol = psftag->GetValue(t_peak          , CMusicFileTag::VALUE_RG_TRACK_PEAK  );
    m_bHasRGAlbumMaxVol = psftag->GetValue(a_peak          , CMusicFileTag::VALUE_RG_ALBUM_PEAK  );
    if(m_bHasRGTrackMaxVol) m_dRGTrackMaxVol = safe_peak_to_maxvol(t_peak);
    if(m_bHasRGAlbumMaxVol) m_dRGAlbumMaxVol = safe_peak_to_maxvol(a_peak);

    delete psftag;
}

////////////////////////////////////////////////////////////////////////////////

void CPSFPlayer::BuildExecutableError(File f, CStringW strError)
{
    m_strLastError.Empty();
    if(f == GetFile()) {
        m_strLastError.Format(
            L"An error occurred while reading:\n%s\n",
            LPCWSTR(f.GetPath())
        );
    }
    m_strLastError += strError;
}

////////////////////////////////////////////////////////////////////////////////
