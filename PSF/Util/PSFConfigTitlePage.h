#if !defined(AFX_PSFCONFIGTITLEPAGE_H__158BBFEF_F789_4CD4_A002_1CDA96DD1D34__INCLUDED_)
#define AFX_PSFCONFIGTITLEPAGE_H__158BBFEF_F789_4CD4_A002_1CDA96DD1D34__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PSFConfigTitlePage.h : header file
//

#include "PSFPlugin.h"

#include "PSFResource.h"

/////////////////////////////////////////////////////////////////////////////
// CPSFConfigTitlePage dialog

class CPSFConfigTitlePage : public CPropertyPage
{
	DECLARE_DYNCREATE(CPSFConfigTitlePage)

// Construction
public:
	void BindToPlugin(CPSFPlugin *pPSFPlugin);
	CPSFConfigTitlePage();
	~CPSFConfigTitlePage();

// Dialog Data
	//{{AFX_DATA(CPSFConfigTitlePage)
	enum { IDD = IDD_PSF_CONFIG_TITLE };
	CEdit	m_cTitleFormat;
	CEdit	m_cTitleFormatFallback;
	CStatic	m_cTitleFormatHelp;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPSFConfigTitlePage)
	public:
	virtual void OnOK();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPSFConfigTitlePage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CPSFPlugin * m_pPSFPlugin;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSFCONFIGTITLEPAGE_H__158BBFEF_F789_4CD4_A002_1CDA96DD1D34__INCLUDED_)
