// PSFRawTagDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psfresource.h"
#include "PSFRawTagDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPSFRawTagDlg dialog


CPSFRawTagDlg::CPSFRawTagDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPSFRawTagDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPSFRawTagDlg)
	m_strRaw = _T("");
	//}}AFX_DATA_INIT
}


void CPSFRawTagDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPSFRawTagDlg)
	DDX_Text(pDX, IDC_PSF_TAG, m_strRaw);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPSFRawTagDlg, CDialog)
	//{{AFX_MSG_MAP(CPSFRawTagDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPSFRawTagDlg message handlers
