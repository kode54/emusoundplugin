#pragma once

class CPSFCompressor  
{
public:

	int Compress(LPVOID lpDest, DWORD *pdwDestSize, LPVOID lpSrc, DWORD dwSrcSize, UINT uMethod);

	CPSFCompressor();
	virtual ~CPSFCompressor();

    enum {
        RESULT_OK          = 0,
        RESULT_SIZE_ERROR  = 1,
        RESULT_FATAL_ERROR = 2
    };

    CStringW GetLastError() const { return m_strLastError; }

 	static UINT GetMethodCount  () { return METHOD_MAX; }
    static UINT GetDefaultMethod() { return METHOD_7ZIP_NORMAL; }
    static UINT GetSafeMethod   () { return METHOD_ZLIB_9; }

    static CStringW GetMethodVersion(UINT uMethod);
	static DWORD GetMaxCompressedSize(DWORD dwUncompressedSize, UINT uMethod);

private:
	CStringW m_strLastError;

    enum {
        METHOD_ZLIB_STORE   = 0,
        METHOD_ZLIB_9       = 1,
        METHOD_7ZIP_NORMAL  = 2,
        METHOD_7ZIP_EXTRA   = 3,
        METHOD_7ZIP_EXTREME = 4,
        METHOD_MAX          = 5
    };

	int CompressZLIB(LPVOID lpDest, DWORD *pdwDestSize, LPVOID lpSrc, DWORD dwSrcSize, int nLevel);
    int Compress7zip(LPVOID lpDest, DWORD *pdwDestSize, LPVOID lpSrc, DWORD dwSrcSize, int nPasses, int nFastBytes);

	static BOOL Verify(LPVOID lpComp, DWORD dwCompSize, LPVOID lpOrig, DWORD dwOrigSize);

};
