////////////////////////////////////////////////////////////////////////////////
//
// PSFTag - A class that represents a PSF tag and can do all the variable
//          parsing voodoo.  Now in Unicode flavor!
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "PSF.h"

#include "../../EmuSoundPlugin/MusicFileTag.h"

#include "../../MFCUtil/File.h"

class CPSFTag : public CMusicFileTag
{
public:

    ////////////////////////////////////////
    //
    // CMusicFileTag interface functions
    //
    virtual BOOL ReadFromFile(File f, BOOL bStrict);
    virtual BOOL WriteToFile (File f, BOOL bStrict);

    virtual BOOL GetField(CStringW &value, FIELD f);
    virtual BOOL SetField(LPCWSTR   value, FIELD f);

    virtual BOOL GetValue(double &value, VALUE v);
    virtual BOOL SetValue(double  value, VALUE v);

    virtual const char* GetTagType() { return "PSF"; }

    ////////////////////////////////////////

	CPSFTag();
	virtual ~CPSFTag();

	BOOL WriteToPSF (CPSF &psf, BOOL bStrict);
	BOOL ReadFromPSF(CPSF &psf, BOOL bStrict);

    void     Empty();

	CStringW GetVarW(LPCWSTR variable);
	void     SetVarW(LPCWSTR variable, LPCWSTR value);

	CStringW GetRawW();
	void     SetRawW(LPCWSTR raw);

    char    *StartModifying();
    void     StopModifying();

    UINT     GetImportedCodePage() { return m_imported_code_page; }

    CStringW GetLastError() const { return m_strLastError; }

private:
    char *m_szTag;
	CStringW m_strLastError;
    UINT m_imported_code_page;

    void Import();

    UINT UpgradeToUTF8();

};

////////////////////////////////////////////////////////////////////////////////
