//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PSFResource.rc
//
#define IDD_PSF_CONFIG_TITLE            15000
#define IDD_PSF_INFO                    15001
#define IDD_PSF_RAWTAG                  15002
#define IDC_PSF_TITLEFORMAT             16000
#define IDC_PSF_TITLEFORMATFALLBACK     16001
#define IDC_PSF_TITLEFORMATHELP         16002
#define IDC_PSF_PATHNAME                16003
#define IDC_PSF_TITLEFORMATHELP2        16003
#define IDC_PSF_TITLE                   16004
#define IDC_PSF_ARTIST                  16005
#define IDC_PSF_GAME                    16006
#define IDC_PSF_YEAR                    16007
#define IDC_PSF_GENRE                   16008
#define IDC_PSF_COMMENT                 16009
#define IDC_PSF_COPYRIGHT               16010
#define IDC_PSF_BY                      16011
#define IDC_PSF_RAWMODE                 16012
#define IDC_PSF_VOLUMESLIDER            16013
#define IDC_PSF_BYTES                   16014
#define IDC_PSF_FADE                    16015
#define IDC_PSF_LENGTH                  16016
#define IDC_PSF_NOW                     16017
#define IDC_PSF_VOLUME                  16018
#define IDC_PSF_LOGO                    16019
#define IDC_PSF_TAG                     16021
#define IDB_PSF                         17000
#define IDC_PSF_PSFBYLABEL              21000
#define IDC_PSF_CONFIGURATION           21001
#define IDC_PSF_SCAN                    21002
#define IDC_PSF_REPLAYGAIN              21003
#define IDC_PSF_USING_RG                21004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        20104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         21005
#define _APS_NEXT_SYMED_VALUE           20101
#endif
#endif
