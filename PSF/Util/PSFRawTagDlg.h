#if !defined(AFX_PSFRAWTAGDLG_H__CA199911_8931_4CE2_8CEE_CBDA10E636C9__INCLUDED_)
#define AFX_PSFRAWTAGDLG_H__CA199911_8931_4CE2_8CEE_CBDA10E636C9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PSFRawTagDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPSFRawTagDlg dialog

class CPSFRawTagDlg : public CDialog
{
// Construction
public:
	CPSFRawTagDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPSFRawTagDlg)
	enum { IDD = IDD_PSF_RAWTAG };
	CString	m_strRaw;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPSFRawTagDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPSFRawTagDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSFRAWTAGDLG_H__CA199911_8931_4CE2_8CEE_CBDA10E636C9__INCLUDED_)
