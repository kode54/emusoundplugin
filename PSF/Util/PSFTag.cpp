#include "StdAfx.h"

#include "PSFTag.h"

#include "../../MFCUtil/UTF8.h"
#include "../../MFCUtil/NumString.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

////////////////////////////////////////////////////////////////////////////////

enum { UTF8_SIG_SIZE = 7 };
static const unsigned char s_utf8_marker[UTF8_SIG_SIZE] = { 'u','t','f','8','=','1',0xA };

static BOOL IsUTF8(LPCSTR s) {
    return (!memcmp(s, s_utf8_marker, UTF8_SIG_SIZE));
}

/////////////////////////////////////////////////////////////////////////////
//
// Raw tag functions from public-domain psftag.c.
// These work fine with UTF-8 strings.
//

//
// Returns index, or -1 if not found
// Returns the index starting at the first non-whitespace character of
// the actual variable name
//
static int find_tag_var_start(const char *tagbuffer, const char *varname) {
  int i, j;
  if(!tagbuffer || !varname) return -1;
  for(i = 0;;) {
    //
    // Find first non-whitespace
    // (this is the variable name on the current line)
    //
    for(;; i++) {
      unsigned u = ((unsigned)(tagbuffer[i])) & 0xFF;
      // If the tag ends here, we wouldn't have had any data anyway
      if(!u) return -1;
      if(u <= 0x20) continue;
      break;
    }
    /*
    ** Compare case-insensitively to the var name
    */
    for(j = 0;; j++) {
      unsigned ucmp = ((unsigned)(varname  [    j])) & 0xFF;
      unsigned u    = ((unsigned)(tagbuffer[i + j])) & 0xFF;
      /* if the varname ends, we'll break here and do the equals check */
      if(!ucmp) break;
      /* If the tag ends here, we wouldn't have had any data anyway */
      if(!u) return -1;
      /* lowercase */
      if(ucmp >= 'A' && ucmp <= 'Z') { ucmp -= 'A'; ucmp += 'a'; }
      if(u    >= 'A' && u    <= 'Z') { u    -= 'A'; u    += 'a'; }
      /* if they're unequal, break... */
      if(u != ucmp) break;
    }
    /*
    ** Only if we exhausted the varname will we do the equals check
    */
    if(!varname[j]) {
      /*
      ** Ensure that the next non-whitespace character in [i+j] is an '='
      */
      for(;; j++) {
        unsigned u = ((unsigned)(tagbuffer[i + j])) & 0xFF;
        /* If the tag ends here, we wouldn't have had any data anyway */
        if(!u) return -1;
        /* quit at the first '=' - success! */
        if(u == '=') return i;
        /* shouldn't be a newline here! */
        if(u == 0x0A) break;
        /* ignore whitespace */
        if(u <= 0x20) continue;
        /* any other character is an error */
        break;
      }
    }
    i += j + 1;
    /*
    ** Find newline or end-of-tag
    */
    for(;; i++) {
      unsigned u = ((unsigned)(tagbuffer[i])) & 0xFF;
      /* If the tag ends here, we wouldn't have had any data anyway */
      if(!u) return -1;
      if(u == 0x0A) break;
    }
  }
  return -1;
}

/*
** Returns the index at which the current variable ends
** (Includes any ending newline and possibly whitespace after that)
**
** Buffer points to the first non-whitespace character of the variable name
*/
static int find_tag_var_end(const char *tagbuffer) {
  int i, j;
  if(!tagbuffer) return 0;
  for(i = 0;;) {
    /*
    ** Find first non-whitespace
    ** (this is the variable name on the current line)
    */
    for(;; i++) {
      unsigned u = ((unsigned)(tagbuffer[i])) & 0xFF;
      if(!u) return i;
      if(u <= 0x20) continue;
      break;
    }
    /*
    ** Compare case-insensitively to the original var name
    */
    for(j = 0;; j++) {
      unsigned ucmp = ((unsigned)(tagbuffer[    j])) & 0xFF;
      unsigned u    = ((unsigned)(tagbuffer[i + j])) & 0xFF;
      /* If the tag ends here, we wouldn't have had any data anyway */
      if(!u) return i;
      /* lowercase */
      if(ucmp >= 'A' && ucmp <= 'Z') { ucmp -= 'A'; ucmp += 'a'; }
      if(u    >= 'A' && u    <= 'Z') { u    -= 'A'; u    += 'a'; }
      /* if they're both whitespace or '=', we win */
      if((u <= 0x20 || u == '=') && (ucmp <= 0x20 || ucmp == '=')) break;
      /* if they're unequal, we lose */
      if(u != ucmp) return i;
      /* otherwise, keep trying */
    }
    /*
    ** Ensure that the next non-whitespace character in [i+j] is an '='
    */
    for(;; j++) {
      unsigned u = ((unsigned)(tagbuffer[i + j])) & 0xFF;
      if(!u) return i;
      /* quit at the first '=' */
      if(u == '=') break;
      /* shouldn't be a newline here! */
      if(u == 0x0A) return i;
      /* ignore whitespace */
      if(u <= 0x20) continue;
      /* any other character is an error */
      break;
    }
    i += j + 1;
    /*
    ** Find newline or end-of-tag
    */
    for(;; i++) {
      unsigned u = ((unsigned)(tagbuffer[i])) & 0xFF;
      if(!u) return i;
      if(u == 0x0A) break;
    }
  }
}

/////////////////////////////////////////////////////////////////////////////
/*
** Get tag variable
** The destination value buffer must be as big as the entire tag
*/
static int raw_getvar(
  const char *tag,
  const char *variable,
  char *value_out,
  int value_out_size
) {
  char *v = value_out;
  char *vmax = v + value_out_size;
  char *v_linebegin;
  int i, i_end;
  //
  // Safety check
  //
  if(value_out_size < 1) return -1;
  /*
  ** Default to empty string
  */
  *v = 0;
  /*
  ** Find the variable start/end index
  */
  i = find_tag_var_start(tag, variable);
  if(i < 0) return -1;
  i_end = i + find_tag_var_end(tag + i);
  /*
  ** Extract the variable data
  */
  while(i < i_end) {
    /*
    ** Skip to first '='
    */
    while((tag[i] != '=') && (i < i_end)) { i++; }
    if(i >= i_end) break;
    /*
    ** If this is not the first line, add a newline
    */
    if(v > value_out) { 
      if(v < vmax) { *v++ = 0x0A; }
    }
    /*
    ** Now that we're at a '=', skip past it
    */
    i++;
    if(i >= i_end) break;
    /*
    ** Skip past any whitespace except newlines
    */
    for(; i < i_end; i++) {
      unsigned u = ((unsigned)(tag[i])) & 0xFF;
      if(u == 0x0A) break;
      if(u <= 0x20) continue;
      break;
    }
    if(i >= i_end) break;
    /*
    ** Consume line data
    */
    v_linebegin = v;
    while(i < i_end) {
      unsigned u = ((unsigned)(tag[i++])) & 0xFF;
      if(u == 0x0A) break;
      if(v < vmax) { *v++ = u; }
    }
    /*
    ** Eat end-of-line whitespace
    */
    while(v > v_linebegin && (((unsigned)(v[-1]))&0xFF) <= 0x20) {
      v--;
    }
  }
  /*
  ** Set variable end
  */
  if(v >= vmax) { v = vmax - 1; }
  *v = 0;
  return 0;
}

/////////////////////////////////////////////////////////////////////////////

static void raw_setvar(
  char *tag,
  int tag_max_size,
  const char *variable,
  const char *value
) {
  int tag_l = strlen(tag);
  int i, i_end, z;
  int insert_i;
  int insert_l;
  int value_exists = 0;
  int tag_max_usable_size = tag_max_size - 1;
  //
  // Safety check
  //
  if(tag_max_size < 1) return;
  //
  // We will assume we can at least use what's there
  //
  if(tag_max_usable_size < tag_l) {
    tag_max_usable_size = tag_l;
    tag_max_size = tag_l + 1;
  }
  /*
  ** Determine the insertion length of the new variable
  */
  { const char *v;
    int nl = strlen(variable);
    insert_l = nl + 2;
    for(v = value; *v; v++) {
      insert_l++;
      if(*v == 0x0A) {
        /* Value exists if it's multi-line */
        value_exists = 1;
        insert_l += nl + 1;
      } else if((((unsigned)(*v))&0xFF) > 0x20) {
        /* Value exists if there are non-whitespace characters */
        value_exists = 1;
      }
    }
  }
  /*
  ** If the value is blank, force the insert length to zero
  */
  if(!value_exists) insert_l = 0;
  /*
  ** Find the variable start index
  */
  i = find_tag_var_start(tag, variable);
  /*
  ** If not found, add a new variable
  */
  if(i < 0) {
    /* Insert position is at the end */
    insert_i = tag_l;
    /* Eat trailing whitespace in the file */
    while(insert_i && (((unsigned)(tag[insert_i - 1]))&0xFF) <= 0x20) { insert_i--; }
    /* Insert a newline if there's room and if there's stuff before */
    if(insert_i && (insert_i < tag_max_usable_size)) { tag[insert_i++] = 0x0A; }
    /* Clamp insert length */
    if((insert_i + insert_l) > tag_max_usable_size) { insert_l = tag_max_usable_size - insert_i; }
    z = insert_i + insert_l;
  /*
  ** Otherwise, find the variable end index
  */
  } else {
    int movel;
    insert_i = i;
    /* Clamp insert length */
    if((insert_i + insert_l) > tag_max_usable_size) { insert_l = tag_max_usable_size - insert_i; }
    i_end = i + find_tag_var_end(tag + i);
    /* Move remaining file data */
    movel = tag_l - i_end;
    if(movel > (tag_max_usable_size-(insert_i+insert_l))) { movel = tag_max_usable_size - (insert_i+insert_l); }
    /* perform the move */
    if(movel && ((insert_i+insert_l) != i_end)) {
      memmove(tag+insert_i+insert_l, tag+i_end, movel);
    }
    z = insert_i+insert_l+movel;
  }
  /* Add terminating null ahead of time */
  if(z > tag_max_usable_size) z = tag_max_usable_size;
  tag[z] = 0;
  /*
  ** Write the variable to index insert_i, max length insert_l
  */
  insert_l += insert_i;
  while(insert_i < insert_l) {
    const char *v;
    for(v = variable; (*v) && (insert_i < insert_l); v++) {
      tag[insert_i++] = *v;
    }
    if(insert_i >= insert_l) break;
    tag[insert_i++] = '=';
    if(insert_i >= insert_l) break;
    for(; (*value) && ((*value) != 0x0A) && (insert_i < insert_l); value++) {
      tag[insert_i++] = *value;
    }
    if(insert_i >= insert_l) break;
    tag[insert_i++] = 0x0A;
    if(insert_i >= insert_l) break;
    if(!(*value)) break;
    if((*value) == 0x0A) value++;
  }

}

////////////////////////////////////////////////////////////////////////////////

static CStringW ConvertToCRLF(LPCWSTR src) {
    CStringW result;
    while(*src) {
        if(src[0] == 0xD && src[1] == 0xA) {
            // preserve CR+LF
            result += src[0];
            result += src[1];
            src += 2;
        } else if(src[0] == 0xA) {
            // convert LF to CR+LF
            result += (WCHAR)(0xD);
            result += src[0];
            src++;
        } else {
            // preserve all other characters
            result += src[0];
            src++;
        }
    }
    return result;
}

static CStringW ConvertToLF(LPCWSTR src) {
    CStringW result;
    while(*src) {
        if(src[0] == 0xD && src[1] == 0xA) {
            // convert CR+LF to LF
            result += src[1];
            src += 2;
        } else {
            // preserve all other characters
            result += src[0];
            src++;
        }
    }
    return result;
}

////////////////////////////////////////////////////////////////////////////////
//
// Construction/Destruction
//

//
// start with blank tag in UTF-8 mode
//
CPSFTag::CPSFTag()
{
    m_szTag = new char[CPSF::MAX_TAG_BYTES + 1];

    memcpy(m_szTag, s_utf8_marker, UTF8_SIG_SIZE);
    m_szTag[UTF8_SIG_SIZE] = 0;
    m_imported_code_page = 0; // was not imported

}

CPSFTag::~CPSFTag()
{
    if(m_szTag) delete m_szTag;
}

////////////////////////////////////////////////////////////////////////////////
//
// Get/set raw data
//

CStringW CPSFTag::GetRawW()
{
    return ConvertToCRLF(Util::StringUTF8toW(m_szTag + UTF8_SIG_SIZE));
}

void CPSFTag::SetRawW(LPCWSTR raw)
{
    Empty();
    strncpy(
        m_szTag + UTF8_SIG_SIZE,
        Util::StringWtoUTF8(ConvertToLF(raw)),
        CPSF::MAX_TAG_BYTES + 1 - UTF8_SIG_SIZE
    );
    m_szTag[CPSF::MAX_TAG_BYTES] = 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Get/set variable
//

CStringW CPSFTag::GetVarW(LPCWSTR variable) {
    char *buf = new char[CPSF::MAX_TAG_BYTES + 1];

    raw_getvar(
        m_szTag + UTF8_SIG_SIZE,
        Util::StringWtoUTF8(variable),
        buf,
        CPSF::MAX_TAG_BYTES + 1 - UTF8_SIG_SIZE
    );

    CStringW result = ConvertToCRLF(Util::StringUTF8toW(buf));

    delete[] buf;
    return result;
}

void CPSFTag::SetVarW(LPCWSTR variable, LPCWSTR value)
{
    raw_setvar(
        m_szTag + UTF8_SIG_SIZE,
        CPSF::MAX_TAG_BYTES + 1 - UTF8_SIG_SIZE,
        Util::StringWtoUTF8(variable),
        Util::StringWtoUTF8(ConvertToLF(value))
    );
}

////////////////////////////////////////////////////////////////////////////////
//
// start with blank tag in UTF-8 mode
//
void CPSFTag::Empty()
{
    memcpy(m_szTag, s_utf8_marker, UTF8_SIG_SIZE);
    m_szTag[UTF8_SIG_SIZE] = 0;
}

////////////////////////////////////////////////////////////////////////////////

static int CountLikelyShiftJISBytes(const char *src, int len) {
    int count = 0;
    while(len >= 2) {
        unsigned c0 = ((unsigned char)(src[0]));
        unsigned c1 = ((unsigned char)(src[1]));
        if(
            (c0 == 0x82               && c1 >= 0x4F && c1 <= 0xF1) ||
            (c0 == 0x83               && c1 >= 0x40 && c1 <= 0x96) ||
            (c0 >= 0x88 && c0 <= 0x9F && c1 >= 0x40 && c1 <= 0xFC)
        ) {
            count += 2;
            src += 2;
            len -= 2;
        } else {
            src++;
            len--;
        }
    }
    return count;
}

//
// String is considered likely to be Shift-JIS if at least half the bytes in it
// are commonly-occuring Shift-JIS byte pairs.
//
static BOOL IsStringLikelyShiftJIS(const char *src, int len) {
    int count = CountLikelyShiftJISBytes(src, len);
    // adjust len by eliminating spaces
    int i;
    for(i = len; i > 0; i--) {
        unsigned char u = (unsigned char)(*src++);
        if(u <= 0x20) len--;
    }
    // if len is pretty low, avoid false positives
    if(len < 6) return FALSE;
    // compare and return
    return (count >= (len / 2));
}

//
// If any one variable value is likely Shift-JIS, then the tag is likely too.
//
static BOOL IsTagLikelyShiftJIS(const char *tag) {
    while(*tag) {
        char c = *tag++;
        if(c != '=') continue; // search for '='
        const char *start = tag;
        while(*tag && (*tag != 0xA)) *tag++;
        if(IsStringLikelyShiftJIS(start, tag-start)) return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////
//
// Try to convert a string using MBTOWC with the given codepage.
// Returns TRUE if successful.
//
static BOOL ConvertStringUsingMBTOWC(UINT codepage, CStringW &wide_tag, LPCSTR tag) {
    wide_tag.Empty();

    if(!(*tag)) return TRUE;

    int tag_len = strlen(tag);

    int len = ::MultiByteToWideChar(codepage, 0, tag, tag_len, NULL, 0);
    if(!len) return FALSE;

    LPWSTR wide_tag_buf = wide_tag.GetBufferSetLength(len);

    int newlen = ::MultiByteToWideChar(codepage, 0, tag, tag_len, wide_tag_buf, len);

    wide_tag.ReleaseBuffer(len);

    if(!newlen) { wide_tag.Empty(); return FALSE; }

    return TRUE;
}

//
// Convert a string manually using the Win1252 codepage.
// Always succeeds.
//
static void ConvertStringUsingWin1252(CStringW &wide_tag, LPCSTR tag) {
    int tag_len = strlen(tag);

    LPWSTR wide_tag_buf = wide_tag.GetBufferSetLength(tag_len);

    int i;
    for(i = 0; i < tag_len; i++) {
        unsigned c = ((unsigned char)(tag[i]));

        if(c >= 0x80 && c < 0xA0) {
            static const WCHAR win1252_extra[0x20] = {
                0x20AC,0x003F,0x201A,0x0192,0x201E,0x2026,0x2020,0x2021,0x02C6,0x2030,0x0160,0x2039,0x0152,0x003F,0x017D,0x003F,
                0x003F,0x2018,0x2019,0x201C,0x201D,0x2022,0x2013,0x2014,0x02DC,0x2122,0x0161,0x203A,0x0153,0x003F,0x017E,0x0178
            };
            wide_tag_buf[i] = win1252_extra[c - 0x80];
        } else {
            wide_tag_buf[i] = c;
        }
    }

    wide_tag.ReleaseBuffer(tag_len);

    // always succeeds
}

////////////////////////////////////////////////////////////////////////////////
//
// Upgrade tag from ANSI to UTF-8.
// Returns the code page that was used.
//
UINT CPSFTag::UpgradeToUTF8() {
    UINT icp = 0;

    UINT acp = GetACP();

    //
    // First, try to convert the tag into a wide string.
    //
    CStringW wide_tag;
    BOOL success = FALSE;

    //
    // If the first few bytes look like a UTF-8 BOM, do the smart thing
    //
    if(!strncmp(m_szTag, "\xEF\xBB\xBF", 3)) {
        BYTE *c = (BYTE*)(m_szTag + 3);
        while(c[0] == '=') c++;
        while((c[0] == 0x0D) || (c[0] == 0x0A)) c++;
        icp = 65001;
        wide_tag = Util::StringUTF8toW((char*)c);
        success = TRUE;
    } else if(!strncmp(m_szTag, "\xC3\xAF\xC2\xBB\xC2\xBF", 6)) {
        BYTE *c = (BYTE*)(m_szTag + 6);
        while(c[0] == '=') c++;
        while((c[0] == 0x0D) || (c[0] == 0x0A)) c++;
        icp = 65001;
        wide_tag = Util::StringUTF8toW((char*)c);
        success = TRUE;
    //
    // If we have a utf8 variable already, do the smart thing
    //
    } else if(find_tag_var_start(m_szTag, "utf8") >= 0) {
        icp = 65001;
        wide_tag = Util::StringUTF8toW(m_szTag);
        success = TRUE;
    }
    //
    // If the current system codepage is one of the western or middle-eastern
    // ANSI sets, try to detect if the tag data is actually Shift-JIS.
    //
    // I think this is likely to catch Japanese tags, without interfering with
    // systems running other East Asian encodings.
    //
    if(!success) {
        if(acp >= 1250 && acp <= 1257) {
            if(IsTagLikelyShiftJIS(m_szTag)) {
                icp = 932;
                success = ConvertStringUsingMBTOWC(932, wide_tag, m_szTag);
            }
        }
    }
    //
    // Next, try just using the system codepage.
    //
    if(!success) {
        icp = acp;
        success = ConvertStringUsingMBTOWC(CP_ACP, wide_tag, m_szTag);
    }
    //
    // In the extremely unlikely event THAT fails, just pretend it's Win1252.
    //
    if(!success) {
        icp = 1252;
        ConvertStringUsingWin1252(wide_tag, m_szTag);
    }

    //
    // Now upgrade, using the wide string we just generated.
    //
    SetRawW(wide_tag);

    //
    // Eliminate any extraneous utf8 tags
    //
    while(!GetVarW(L"utf8").IsEmpty()) SetVarW(L"utf8", L"");

    return icp;
}

////////////////////////////////////////////////////////////////////////////////

void CPSFTag::Import() {
    //
    // if our tag isn't UTF-8, we'll need to upgrade it.
    //
    if(!IsUTF8(m_szTag)) {
        m_imported_code_page = UpgradeToUTF8();
    } else {
        m_imported_code_page = 65001; // already UTF-8
    }

}

////////////////////////////////////////////////////////////////////////////////

char *CPSFTag::StartModifying() {
    return m_szTag;
}

void CPSFTag::StopModifying() {
    Import();
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSFTag::ReadFromPSF(CPSF &psf, BOOL bStrict)
{
    DWORD s = psf.GetTagSize();
    if(s > CPSF::MAX_TAG_BYTES) {
        if(bStrict) {
            m_strLastError.Format(L"Tag is too large (%d bytes, max %d)", s, CPSF::MAX_TAG_BYTES);
            return FALSE;
        }
        s = CPSF::MAX_TAG_BYTES;
    }
    m_szTag[s] = 0;
    if(s) memcpy(m_szTag, psf.GetTagPtr(), s);

    Import();

    return TRUE;
}

BOOL CPSFTag::WriteToPSF(CPSF &psf, BOOL bStrict)
{
  DWORD s = strlen(m_szTag);
  psf.SetTagSize(s);
  if(s) memcpy(psf.GetTagPtr(), m_szTag, s);
  return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSFTag::ReadFromFile(File f, BOOL bStrict) {
    CPSF psf;
    if(!psf.ReadFromFile(f, CPSF::PSF_FILE_TAG, bStrict)) {
        m_strLastError = psf.GetLastError();
        return NULL;
    }
    return ReadFromPSF(psf, bStrict);
}

BOOL CPSFTag::WriteToFile(File f, BOOL bStrict) {
    CPSF psf;
    if(!WriteToPSF(psf, bStrict)) return FALSE;

    if(!psf.WriteToFile(f, CPSF::PSF_FILE_TAG, bStrict)) {
        m_strLastError = psf.GetLastError();
        return FALSE;
    }

    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSFTag::GetField(CStringW &value, FIELD f) {
    switch(f) {
    case FIELD_TITLE  : value = GetVarW(L"title"  ); return TRUE;
    case FIELD_ARTIST : value = GetVarW(L"artist" ); return TRUE;
    case FIELD_ALBUM  : value = GetVarW(L"game"   ); return TRUE;
    case FIELD_YEAR   : value = GetVarW(L"year"   ); return TRUE;
    case FIELD_GENRE  : value = GetVarW(L"genre"  ); return TRUE;
    case FIELD_COMMENT: value = GetVarW(L"comment"); return TRUE;
    case FIELD_TRACK  : value = GetVarW(L"track"  ); return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSFTag::SetField(LPCWSTR value, FIELD f) {
    switch(f) {
    case FIELD_TITLE  : SetVarW(L"title"  , value); return TRUE;
    case FIELD_ARTIST : SetVarW(L"artist" , value); return TRUE;
    case FIELD_ALBUM  : SetVarW(L"game"   , value); return TRUE;
    case FIELD_YEAR   : SetVarW(L"year"   , value); return TRUE;
    case FIELD_GENRE  : SetVarW(L"genre"  , value); return TRUE;
    case FIELD_COMMENT: SetVarW(L"comment", value); return TRUE;
    case FIELD_TRACK  : SetVarW(L"track"  , value); return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSFTag::GetValue(double &value, VALUE v) {
    switch(v) {
    case VALUE_RG_TRACK_VOLUME: return Util::StringToNum(GetVarW(L"replaygain_track_gain"), value, Util::NSFLAG_PLUS | Util::NSFLAG_DB);
    case VALUE_RG_ALBUM_VOLUME: return Util::StringToNum(GetVarW(L"replaygain_album_gain"), value, Util::NSFLAG_PLUS | Util::NSFLAG_DB);
    case VALUE_RG_TRACK_PEAK  : return Util::StringToNum(GetVarW(L"replaygain_track_peak"), value, 0);
    case VALUE_RG_ALBUM_PEAK  : return Util::StringToNum(GetVarW(L"replaygain_album_peak"), value, 0);
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSFTag::SetValue(double value, VALUE v) {
    double db;
    switch(v) {
    case VALUE_RG_TRACK_VOLUME: SetVarW(L"replaygain_track_gain", Util::NumToString(value, Util::NSFLAG_PLUS | Util::NSFLAG_DB)); return TRUE;
    case VALUE_RG_ALBUM_VOLUME: SetVarW(L"replaygain_album_gain", Util::NumToString(value, Util::NSFLAG_PLUS | Util::NSFLAG_DB)); return TRUE;
    case VALUE_RG_TRACK_PEAK  : SetVarW(L"replaygain_track_peak", Util::NumToString(value, 0                                  )); return TRUE;
    case VALUE_RG_ALBUM_PEAK  : SetVarW(L"replaygain_album_peak", Util::NumToString(value, 0                                  )); return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////
