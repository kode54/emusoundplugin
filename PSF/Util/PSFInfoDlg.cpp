// PSFInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "psfresource.h"
#include "PSFInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "PSFRawTagDlg.h"
#include "../../MFCUtil/NumString.h"
#include ".\psfinfodlg.h"

#include "../../EmuSoundPlugin/ESPReplayGainDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CPSFInfoDlg dialog


CPSFInfoDlg::CPSFInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPSFInfoDlg::IDD, pParent)
{
    m_plNowMS = NULL;
    m_pPSFTag = NULL;
    m_pPlugin = NULL;
    m_nPSFVersion = -1;
    m_nBytes = 0;

    m_bReplaygainKnown = false;
    m_bRGWindowOn = true;
    m_bMVWindowOn = true;

	//{{AFX_DATA_INIT(CPSFInfoDlg)
	m_strPathName = _T("");
	//}}AFX_DATA_INIT
}


void CPSFInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPSFInfoDlg)
	DDX_Control(pDX, IDC_PSF_PSFBYLABEL, m_cPSFByLabel);
	DDX_Control(pDX, IDC_PSF_BYTES, m_cBytes);
	DDX_Control(pDX, IDC_PSF_LOGO, m_cLogo);
	DDX_Control(pDX, IDC_PSF_LENGTH, m_cLength);
	DDX_Control(pDX, IDC_PSF_VOLUMESLIDER, m_cVolumeSlider);
	DDX_Control(pDX, IDC_PSF_VOLUME, m_cVolume);
	DDX_Control(pDX, IDC_PSF_REPLAYGAIN, m_cReplaygain);
    DDX_Control(pDX, IDC_PSF_USING_RG, m_cUsingRG);
    DDX_Control(pDX, IDC_PSF_SCAN, m_cScan);
	DDX_Text(pDX, IDC_PSF_PATHNAME, m_strPathName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPSFInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CPSFInfoDlg)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_PSF_NOW, OnPsfNow)
	ON_EN_CHANGE(IDC_PSF_VOLUME, OnChangePsfVolume)
	ON_BN_CLICKED(IDC_PSF_RAWMODE, OnPsfRawmode)
	ON_BN_CLICKED(IDC_PSF_CONFIGURATION, OnPsfConfiguration)
	//}}AFX_MSG_MAP
    ON_STN_CLICKED(IDC_PSF_LOGO, OnStnClickedPsfLogo)
    ON_BN_CLICKED(IDC_PSF_SCAN, OnBnClickedPsfScan)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPSFInfoDlg message handlers

void CPSFInfoDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
  if(pScrollBar) {
    switch(pScrollBar->GetDlgCtrlID()) {
    case IDC_PSF_VOLUMESLIDER:
      UpdateVolumeFromSlider();
      break;
    }
  }
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CPSFInfoDlg::UpdateVolumeFromSlider()
{
    int i = m_cVolumeSlider.GetPos();
    if(i < 0) i = 0;
    if(i > 200) i = 200;
    CStringW ws = Util::NumToString(((double)i)*0.01, 0);
    m_cVolume.SetWindowText(CString(ws));
    SetVolume(ws);
}

void CPSFInfoDlg::UpdateSliderFromVolume()
{
    CString s;
    m_cVolume.GetWindowText(s);
    CStringW ws = s;
    SetVolume(ws);
    double d;
    if(!Util::StringToNum(ws, d, 0)) return;
    int i = (int)(d * 100.0);
    if(i < 0) i = 0;
    if(i > 200) i = 200;
    m_cVolumeSlider.SetPos(i);
}

void CPSFInfoDlg::OnPsfNow() 
{
    //
    // Get the current output time
    // (if not playing, then exit)
    //

    LONG lMS = (LONG)::InterlockedCompareExchange(m_plNowMS, 0, 0);
    if(lMS < 0) return;
    //
    // Round to the nearest tenth of a second
    //
    LONG lTenth = lMS / 100;
    double dSeconds = lTenth;
    dSeconds /= 10.0;
    //
    // Set the length text field
    //
    m_cLength.SetWindowText(CString(Util::NumToString(dSeconds, Util::NSFLAG_COLONS)));
}

void CPSFInfoDlg::SetVolume(LPCWSTR lpszVolString)
{
    if((!lpszVolString) || (!(*lpszVolString))) lpszVolString = L"1.0";
    double dVol = 0.0;
    if(!Util::StringToNum(lpszVolString, dVol, 0)) return;

    if(m_pPlugin && m_file) m_pPlugin->NotifyManualVolumeChange(m_file, dVol);
}

BOOL CPSFInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

    CStringW strExt;
    //
    // Orwellian changing of various labels
    //
    if(m_pPlugin) {
        strExt = m_pPlugin->GetPSFType(m_nPSFVersion);
    }
    if(!(strExt.IsEmpty())) {
        strExt.MakeUpper();
        CStringW s;
        s.Format(L"%s File Information", LPCWSTR(strExt));
        SetWindowText(CString(s));
        s.Format(L"%s &by:", LPCWSTR(strExt));
        m_cPSFByLabel.SetWindowText(CString(s));
    }

    //
    // Do the bytes thing
    //
    CStringW strBytes = Util::UINT64ToString(m_nBytes, 1, Util::NSFLAG_COMMAS);
    strBytes += L" byte";
    if(m_nBytes != 1) strBytes += L"s";
    m_cBytes.SetWindowText(CString(strBytes));

    m_cVolumeSlider.SetRange(0, 200);
    m_cVolumeSlider.SetLineSize(1);
    m_cVolumeSlider.SetPageSize(10);
    m_cVolumeSlider.SetTicFreq(100);
    m_cVolumeSlider.SetPos(100);

    //
    // Get the bitmap resource number
    //
    int nBitmapResource = -1;
    if(m_pPlugin) {
        nBitmapResource = m_pPlugin->GetPSFLogoBitmap(m_file, m_pPSFTag, m_nPSFVersion);
    }
    if(nBitmapResource >= 0) {
        m_cLogo.SetBitmap(::LoadBitmap(::AfxGetInstanceHandle(), MAKEINTRESOURCE(nBitmapResource)));
    }

    if(m_pPSFTag) m_strCancelVolume = m_pPSFTag->GetVarW(L"volume");

    SetAllFieldsFromTag();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

struct SPSFInfoDlgField {
    int nID;
    LPCWSTR lpszVarFormat;
};

static const struct SPSFInfoDlgField fields[] = {
  { IDC_PSF_TITLE    , L"title"     },
  { IDC_PSF_ARTIST   , L"artist"    },
  { IDC_PSF_GAME     , L"game"      },
  { IDC_PSF_YEAR     , L"year"      },
  { IDC_PSF_GENRE    , L"genre"     },
  { IDC_PSF_COMMENT  , L"comment"   },
  { IDC_PSF_COPYRIGHT, L"copyright" },
  { IDC_PSF_BY       , L".by"       },
  { IDC_PSF_VOLUME   , L"volume"    },
  { IDC_PSF_LENGTH   , L"length"    },
  { IDC_PSF_FADE     , L"fade"      },
  { 0                , NULL         }
};

CStringW CPSFInfoDlg::ActualVarName(LPCWSTR lpszVarFormat) {
    CStringW s;
    if(!lpszVarFormat) return s;
    for(; *lpszVarFormat; lpszVarFormat++) {
        if(*lpszVarFormat == '.') {
            CStringW ext; // default to empty... just "by" I guess
            if(m_pPlugin) { ext = m_pPlugin->GetPSFType(m_nPSFVersion); }
            ext.MakeLower();
            s += ext;
        } else {
            s += (*lpszVarFormat);
        }
    }
    return s;
}

void CPSFInfoDlg::UpdateShowReplaygainFrame() {
    g_pPlugin->LockOptions();
    BOOL bRG = g_pPlugin->opt_bReplayGainEnable;
    g_pPlugin->UnlockOptions();


    if(bRG && m_bReplaygainKnown) {
        if(m_bMVWindowOn) {
            m_cVolume.ShowWindow(SW_HIDE);
            m_cVolumeSlider.ShowWindow(SW_HIDE);
            m_cVolume.EnableWindow(FALSE);
            m_cVolumeSlider.EnableWindow(FALSE);
            m_bMVWindowOn = false;
        }
        if(!m_bRGWindowOn) {
            m_cUsingRG.ShowWindow(SW_SHOW);
            m_bRGWindowOn = true;
        }
    } else {
        if(m_bRGWindowOn) {
            m_cUsingRG.ShowWindow(SW_HIDE);
            m_bRGWindowOn = false;
        }
        if(!m_bMVWindowOn) {
            m_cVolume.EnableWindow(TRUE);
            m_cVolumeSlider.EnableWindow(TRUE);
            m_cVolume.ShowWindow(SW_SHOW);
            m_cVolumeSlider.ShowWindow(SW_SHOW);
            m_bMVWindowOn = true;
        }
    }
}

void CPSFInfoDlg::UpdateReplaygainFromTag() {
    m_bReplaygainKnown = false;
    double dVolume = 1.0;
    CStringW db;
    if(m_pPSFTag) {
        m_bReplaygainKnown = m_pPSFTag->GetValue(dVolume, CMusicFileTag::VALUE_RG_TRACK_VOLUME);
    }
    if(m_bReplaygainKnown) {
        db = Util::NumToString(dVolume, Util::NSFLAG_PLUS | Util::NSFLAG_DB);
    } else {
        db = L"unknown";
    }
    m_cReplaygain.SetWindowText(CString(L"Replay gain: " + db));

    if(m_bReplaygainKnown) {
        m_cScan.SetWindowText(CString(L"Details"));
    } else {
        m_cScan.SetWindowText(CString(L"Scan"));
    }

    UpdateShowReplaygainFrame();
}

void CPSFInfoDlg::SetAllFieldsFromTag()
{
    for(int i = 0; fields[i].lpszVarFormat; i++) {
        if(m_pPSFTag) SetDlgItemText(fields[i].nID, CString(m_pPSFTag->GetVarW(ActualVarName(fields[i].lpszVarFormat))));
    }
    UpdateSliderFromVolume();
    UpdateReplaygainFromTag();
}

void CPSFInfoDlg::SyncTagToAllFields()
{
    for(int i = 0; fields[i].lpszVarFormat; i++) {
        CString s;
        GetDlgItemText(fields[i].nID, s);
        CStringW ws = s;
        if(m_pPSFTag) m_pPSFTag->SetVarW(ActualVarName(fields[i].lpszVarFormat), ws);
        if(fields[i].nID == IDC_PSF_VOLUME) { SetVolume(ws); }
    }
}

void CPSFInfoDlg::OnCancel() 
{
	SetVolume(m_strCancelVolume);
	CDialog::OnCancel();
}

void CPSFInfoDlg::OnOK() 
{
  SyncTagToAllFields();
	CDialog::OnOK();
}

void CPSFInfoDlg::OnChangePsfVolume() 
{
    //
    // some safeguarding: don't update the slider/volume if it's out of range
    //
    CString s;
    m_cVolume.GetWindowText(s);
    if(s.IsEmpty()) return;
    double dVol = 0.0;
    if(!Util::StringToNum(CStringW(s), dVol, 0)) return;
    if(dVol < 0.0 || dVol > 2.0) return;

	UpdateSliderFromVolume();
}

void CPSFInfoDlg::OnPsfRawmode() 
{
  SyncTagToAllFields();

  CPSFRawTagDlg dlg(this);
  if(m_pPSFTag) dlg.m_strRaw = m_pPSFTag->GetRawW();
  if(dlg.DoModal() != IDOK) return;
  if(m_pPSFTag) m_pPSFTag->SetRawW(CStringW(dlg.m_strRaw));

  SetAllFieldsFromTag();
}

void CPSFInfoDlg::OnPsfConfiguration() 
{
    if(m_pPlugin) {
        m_pPlugin->Config(GetSafeHwnd());
        UpdateShowReplaygainFrame();
    }
}

void CPSFInfoDlg::OnStnClickedPsfLogo()
{
    if(m_pPlugin) {
        m_pPlugin->About(GetSafeHwnd());
    }
}

void CPSFInfoDlg::OnBnClickedPsfScan()
{
    CESPReplayGainDlg dlg(this);

    // todo: actually finish this

    dlg.DoModal();

}

