// PSFConfigTitlePage.cpp : implementation file
//

#include "stdafx.h"
#include "psfresource.h"
#include "PSFConfigTitlePage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPSFConfigTitlePage property page

IMPLEMENT_DYNCREATE(CPSFConfigTitlePage, CPropertyPage)

CPSFConfigTitlePage::CPSFConfigTitlePage() : CPropertyPage(CPSFConfigTitlePage::IDD)
{
  m_pPSFPlugin = NULL;

	//{{AFX_DATA_INIT(CPSFConfigTitlePage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CPSFConfigTitlePage::~CPSFConfigTitlePage()
{
}

void CPSFConfigTitlePage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPSFConfigTitlePage)
	DDX_Control(pDX, IDC_PSF_TITLEFORMAT, m_cTitleFormat);
	DDX_Control(pDX, IDC_PSF_TITLEFORMATFALLBACK, m_cTitleFormatFallback);
	DDX_Control(pDX, IDC_PSF_TITLEFORMATHELP, m_cTitleFormatHelp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPSFConfigTitlePage, CPropertyPage)
	//{{AFX_MSG_MAP(CPSFConfigTitlePage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPSFConfigTitlePage message handlers

BOOL CPSFConfigTitlePage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

    LPCWSTR *alltypes = NULL;

    if(m_pPSFPlugin) {
        m_pPSFPlugin->LockOptions();
        m_cTitleFormat.SetWindowText(CString(m_pPSFPlugin->opt_strTitleFormat));
        m_cTitleFormatFallback.SetWindowText(CString(m_pPSFPlugin->opt_strTitleFormatFallback));
        alltypes = m_pPSFPlugin->GetAllPSFTypes();
        m_pPSFPlugin->UnlockOptions();
    }

    CStringW strHelp;

    strHelp = L"Pre-defined variables: %filedir%, %filename%, %fileext%, %title%, %artist%, %game%, %year%, %genre%, %comment%, %copyright%";

    int i;
    for(i = 0; alltypes && alltypes[i]; i++) {
        CStringW t = alltypes[i];
        t.MakeLower();
        CStringW s;
        s.Format(L", %%%sby%%", LPCWSTR(t));
        strHelp += s;
    }

    m_cTitleFormatHelp.SetWindowText(CString(strHelp));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPSFConfigTitlePage::BindToPlugin(CPSFPlugin *pPSFPlugin)
{
  m_pPSFPlugin = pPSFPlugin;
}

void CPSFConfigTitlePage::OnOK() 
{
    if(m_pPSFPlugin) {
        m_pPSFPlugin->LockOptions();

        CString s;

        m_cTitleFormat.GetWindowText(s);
        m_pPSFPlugin->opt_strTitleFormat = s;

        m_cTitleFormatFallback.GetWindowText(s);
        m_pPSFPlugin->opt_strTitleFormatFallback = s;

        m_pPSFPlugin->UnlockOptions();
    }

	CPropertyPage::OnOK();
}
