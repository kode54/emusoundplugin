////////////////////////////////////////////////////////////////////////////////
//
// PSF2FS - Interface for using PSF2 filesystems
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../../MFCUtil/File.h"

class CPSF2FS
{
public:
    CPSF2FS();
	virtual ~CPSF2FS();

    bool AddArchive(File f, BYTE compare_version);

    int VirtualRead(LPCSTR path, int offset, char *buffer, int length);

    CStringW GetLastError() const { return m_strLastError; }

private:
	CStringW m_strLastError;

    //
    // Source file - PSF2 or PSF2lib
    //
    class SourceFile {
    public:
        SourceFile();
        virtual ~SourceFile();

        File        file;
        DWORD       reserved_size; // size of PSF2 'reserved' block

        SourceFile *next;
    };

    //
    // Virtual directory entry
    //
    class DirEntry {
    public:
        DirEntry(DirEntry *n = NULL);
        virtual ~DirEntry();

        char name[37]; // fixed according to spec
        DirEntry *sub;
        INT32 length;
        INT32 block_size;
        SourceFile *source;
        CDWordArray offset_table;

        DirEntry *next;
    };

    SourceFile *m_sources;

    DirEntry *m_root;

    //
    // Can cache one block at a time
    //
    CByteArray  m_cache;
    SourceFile *m_cache_source;
    int         m_cache_offset;

    bool m_adderror;

    void FreeSources(SourceFile *s);
    void FreeDir(DirEntry *d);

    DirEntry* FindDirEntry(DirEntry *dir, LPCSTR name, int name_l);

    DirEntry* ReadDirectory(File &f, int offset, SourceFile *source);

    SourceFile* MergeSource(SourceFile *to, SourceFile *from);

    DirEntry* MergeDir(DirEntry *to, DirEntry *from);

    bool AddArchiveInternal(
        File f,
        BYTE compare_version,
        int level,
        SourceFile **psource,
        DirEntry **pdir
    );

    int VirtualReadInternal(DirEntry *entry, int offset, char *buffer, int length);

};
