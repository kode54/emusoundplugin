////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "PSF.h"

#include "PSFCompressor.h"
#include "PSFDecompressor.h"

#include "../../MFCUtil/Endian.h"
#include "../../MFCUtil/MFCExceptions.h"

////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPSF::CPSF()
{
  m_dwValidFlags = 0;
}

CPSF::~CPSF()
{

}

void CPSF::Invalidate(DWORD dwMask)
{
    dwMask &= m_dwValidFlags;
    m_dwValidFlags &= (~dwMask);
    FreeAllInvalid();
}

BOOL CPSF::FileError(File &f, LPCWSTR message) {
    if((!message) || (!(message[0]))) {
        m_strLastError = f.GetLastError();
    } else {
        m_strLastError = message;
    }
    f.Close();
    FreeAllInvalid();
    return FALSE;
}

//
// Read parts from the PSF file into memory
// Returns TRUE on success
//
BOOL CPSF::ReadFromFile(File f, DWORD dwMask, BOOL bStrict)
{
    BOOL bSuccess = FALSE;

    Invalidate(dwMask);

    if(!f.Open(File::READ)) return FileError(f);

    if(!CheckValidPSF(f, bStrict)) return FALSE;

    // TODO: we're not handling >4GB PSFs properly, but this is rather unlikely in practice
    DWORD l = (DWORD)(f.Length());
    BYTE hdr[0x10];
    memset(hdr, 0, sizeof(hdr));
    f.Seek(0, File::BEGIN);
    if(f.Read(hdr, 0x10) != 0x10) return FileError(f);

    DWORD dwReservedSize = Util::Get32LSB(hdr + 0x4);
    DWORD dwZEXESize     = Util::Get32LSB(hdr + 0x8);
    DWORD dwZEXECRC      = Util::Get32LSB(hdr + 0xC);

    DWORD dwReservedOffset = 0x10;
    DWORD dwZEXEOffset = dwReservedOffset + dwReservedSize;
    DWORD dwTagOffset = dwZEXEOffset + dwZEXESize;
    DWORD dwTagTotalSize = l - dwTagOffset;
    if(dwTagTotalSize > (MAX_TAG_BYTES+5)) dwTagTotalSize = MAX_TAG_BYTES+5;

    //
    // Read the various parts
    //
    if(dwMask & PSF_FILE_VERSION) {
        // We technically do not "know" the version unless we were explicitly
        // told to read the version byte
        m_ucVersion = hdr[3];
        m_dwValidFlags |= PSF_FILE_VERSION;
    }
    if(dwMask & PSF_FILE_RESERVED) {
        m_aReserved.SetSize(dwReservedSize);
        f.Seek(dwReservedOffset, File::BEGIN);
        if(dwReservedSize) {
            if(f.Read(m_aReserved.GetData(), dwReservedSize) != dwReservedSize) return FileError(f);
        }
        m_dwValidFlags |= PSF_FILE_RESERVED;
    }
    if(dwMask & PSF_FILE_ZEXE) {
        m_aZEXE.SetSize(dwZEXESize);
        f.Seek(dwZEXEOffset, File::BEGIN);
        if(dwZEXESize) {
            if(f.Read(m_aZEXE.GetData(), dwZEXESize) != dwZEXESize) return FileError(f);
        }
        // Validate CRC only if strict
        if(bStrict && dwZEXESize) {
            if(CPSFDecompressor::CRC32(m_aZEXE.GetData(), dwZEXESize) != dwZEXECRC) {
                return FileError(f, L"Executable data failed CRC check");
            }
        }
        m_dwValidFlags |= PSF_FILE_ZEXE;
    }
    if(dwMask & PSF_FILE_TAG) {
        m_aTag.SetSize(0);
        if(dwTagTotalSize > 5) {
            BYTE tmp[5];
            memset(tmp, 0, 5);
            f.Seek(dwTagOffset, File::BEGIN);
            f.Read(tmp, 5);
            if(!memcmp(tmp, "[TAG]", 5)) {
                m_aTag.SetSize(dwTagTotalSize - 5);
                if(f.Read(m_aTag.GetData(), dwTagTotalSize - 5) != (dwTagTotalSize-5)) return FileError(f);
            }
        }
        m_dwValidFlags |= PSF_FILE_TAG;
    }

    // Success
    m_dwLastKnownFileLength = (DWORD)(f.Length());
    f.Close();
    
    return TRUE;
}

void CPSF::FreeAllInvalid()
{
  if(!(m_dwValidFlags & PSF_FILE_RESERVED)) { m_aReserved.SetSize(0); }
  if(!(m_dwValidFlags & PSF_FILE_ZEXE)) { m_aZEXE.SetSize(0); }
  if(!(m_dwValidFlags & PSF_FILE_TAG)) { m_aTag.SetSize(0); }
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSF::WriteToFile(File f, DWORD dwMask, BOOL bStrict)
{
    if((dwMask & PSF_FILE_ALL) != dwMask) {
        m_strLastError = L"CPSF::WriteToFile - Invalid dwMask argument (program error)";
        return FALSE;
    }
    if((dwMask & m_dwValidFlags) != dwMask) {
        m_strLastError = L"CPSF::WriteToFile - Attempted to write nonexistent sections (program error)";
        return FALSE;
    }
    if(!dwMask) return TRUE;
    if(dwMask == PSF_FILE_ALL) return WriteToWholeFile(f);
    return WriteToPartialFile(f, dwMask, bStrict);
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSF::WriteToWholeFile(File &f)
{
    ASSERT(m_dwValidFlags == PSF_FILE_ALL);

    if(!f.Open(File::CREATE)) return FileError(f);

    DWORD l = 0;

    BYTE hdr[0x10];
    memcpy(hdr, "PSF", 3);
    hdr[3] = m_ucVersion;
    Util::Put32LSB(hdr + 0x4, m_aReserved.GetSize());
    Util::Put32LSB(hdr + 0x8, m_aZEXE.GetSize());
    Util::Put32LSB(hdr + 0xC, CPSFDecompressor::CRC32(m_aZEXE.GetData(), m_aZEXE.GetSize()));
    if(f.Write(hdr, sizeof(hdr)) != sizeof(hdr)) return FileError(f);
    l += sizeof(hdr);
    if(m_aReserved.GetSize()) if(f.Write(m_aReserved.GetData(), m_aReserved.GetSize()) != m_aReserved.GetSize()) return FileError(f);
    l += m_aReserved.GetSize();
    if(m_aZEXE    .GetSize()) if(f.Write(m_aZEXE    .GetData(), m_aZEXE    .GetSize()) != m_aZEXE    .GetSize()) return FileError(f);
    l += m_aZEXE.GetSize();
    if(m_aTag     .GetSize()) {
        if(f.Write("[TAG]", 5) != 5) return FileError(f);
        l += 5;
        if(f.Write(m_aTag.GetData(), m_aTag.GetSize()) != m_aTag.GetSize()) return FileError(f);
        l += m_aTag.GetSize();
    }
    // done
    m_dwLastKnownFileLength = l;
    f.Close();
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

BOOL CPSF::WriteToPartialFile(File &f, DWORD dwMask, BOOL bStrict)
{
    ASSERT((dwMask & m_dwValidFlags & PSF_FILE_ALL) == dwMask);
    ASSERT(dwMask != PSF_FILE_ALL);

    if(!f.Open(File::WRITE)) return FileError(f);

    if(!CheckValidPSF(f, bStrict)) return FALSE;

    // get some specs on the current file
    BYTE hdr[0x10];
    BOOL bRewriteHeader = FALSE;
    f.Seek(0, File::BEGIN);
    if(f.Read(hdr, 0x10) != 0x10) return FileError(f);
    DWORD dwExistingLength       = (DWORD)(f.Length());
    DWORD dwExistingReservedSize = Util::Get32LSB(hdr + 0x4);
    DWORD dwExistingZEXESize     = Util::Get32LSB(hdr + 0x8);
    DWORD dwExistingZEXECRC      = Util::Get32LSB(hdr + 0xC);
    DWORD dwExistingReservedOffset = 0x10;
    DWORD dwExistingZEXEOffset     = dwExistingReservedOffset + dwExistingReservedSize;
    DWORD dwExistingTotalTagOffset = dwExistingZEXEOffset + dwExistingZEXESize;
    DWORD dwExistingTotalTagSize   = dwExistingLength - dwExistingTotalTagOffset;

    // we may need to temporarily preserve the ZEXE or Tag in some cases
    // though we can always leave Reserved alone, since that'll never move
    CByteArray aExistingZEXE;
    CByteArray aExistingTotalTag;
    BOOL bSaveExistingZEXE =
      (dwMask & PSF_FILE_RESERVED) &&
      (!(dwMask & PSF_FILE_ZEXE));
    BOOL bSaveExistingTotalTag =
      (dwMask & (PSF_FILE_RESERVED | PSF_FILE_ZEXE)) &&
      (!(dwMask & PSF_FILE_TAG));

    // TODO: don't necessarily have to save existing if some sizes are the same

    if(bSaveExistingZEXE) {
        aExistingZEXE.SetSize(dwExistingZEXESize);
        f.Seek(dwExistingZEXEOffset, File::BEGIN);
        if(dwExistingZEXESize) {
            if(f.Read(aExistingZEXE.GetData(), dwExistingZEXESize) != dwExistingZEXESize) return FileError(f);
        }
    }
    if(bSaveExistingTotalTag) {
        aExistingTotalTag.SetSize(dwExistingTotalTagSize);
        f.Seek(dwExistingTotalTagOffset, File::BEGIN);
        if(dwExistingTotalTagSize) {
            if(f.Read(aExistingTotalTag.GetData(), dwExistingTotalTagSize) != dwExistingTotalTagSize) return FileError(f);
        }
    }

    // if we're rewriting the version, handle that here
    if(dwMask & PSF_FILE_VERSION) {
        hdr[3] = m_ucVersion;
        bRewriteHeader = TRUE;
    }

    // Reserved area: write, or skip
    DWORD dwNewReservedSize;
    if(dwMask & PSF_FILE_RESERVED) {
        dwNewReservedSize = m_aReserved.GetSize();
        f.Seek(0x10, File::BEGIN);
        if(dwNewReservedSize) {
            if(f.Write(m_aReserved.GetData(), dwNewReservedSize) != dwNewReservedSize) return FileError(f);
        }
        Util::Put32LSB(hdr + 0x4, dwNewReservedSize);
        bRewriteHeader = TRUE;
    } else {
        dwNewReservedSize = dwExistingReservedSize;
    }

    // ZEXE: write, skip, or restore from saved
    DWORD dwNewZEXESize;
    if(dwMask & PSF_FILE_ZEXE) {
        dwNewZEXESize = m_aZEXE.GetSize();
        f.Seek(0x10 + dwNewReservedSize, File::BEGIN);
        if(dwNewZEXESize) {
            if(f.Write(m_aZEXE.GetData(), dwNewZEXESize) != dwNewZEXESize) return FileError(f);
        }
        Util::Put32LSB(hdr + 0x8, dwNewZEXESize);
        Util::Put32LSB(hdr + 0xC, CPSFDecompressor::CRC32(m_aZEXE.GetData(), dwNewZEXESize));
        bRewriteHeader = TRUE;
    } else if(bSaveExistingZEXE) {
        dwNewZEXESize = dwExistingZEXESize;
        f.Seek(0x10 + dwNewReservedSize, File::BEGIN);
        if(dwNewZEXESize) {
            if(f.Write(aExistingZEXE.GetData(), dwNewZEXESize) != dwNewZEXESize) return FileError(f);
        }
        aExistingZEXE.SetSize(0);
    } else {
        dwNewZEXESize = dwExistingZEXESize;
    }

    // Tag: write, skip, or restore from saved
    DWORD dwNewTotalTagSize;
    if(dwMask & PSF_FILE_TAG) {
        dwNewTotalTagSize = 5 + m_aTag.GetSize();
        if(dwNewTotalTagSize <= 5) dwNewTotalTagSize = 0;
        f.Seek(0x10 + dwNewReservedSize + dwNewZEXESize, File::BEGIN);
        if(dwNewTotalTagSize > 5) {
            if(f.Write("[TAG]", 5) != 5) return FileError(f);
            if(f.Write(m_aTag.GetData(), dwNewTotalTagSize - 5) != (dwNewTotalTagSize-5)) return FileError(f);
        }
    } else if(bSaveExistingTotalTag) {
        dwNewTotalTagSize = dwExistingTotalTagSize;
        f.Seek(0x10 + dwNewReservedSize + dwNewZEXESize, File::BEGIN);
        if(dwNewTotalTagSize) {
            if(f.Write(aExistingTotalTag.GetData(), dwNewTotalTagSize) != dwNewTotalTagSize) return FileError(f);
        }
        aExistingTotalTag.SetSize(0);
    } else {
        dwNewTotalTagSize = dwExistingTotalTagSize;
    }

    // truncate file
    f.Seek(0x10 + dwNewReservedSize + dwNewZEXESize + dwNewTotalTagSize, File::BEGIN);
    f.Truncate();

    // rewrite header, if necessary
    if(bRewriteHeader) {
        f.Seek(0, File::BEGIN);
        if(f.Write(hdr, 0x10) != 0x10) return FileError(f);
    }

    // done
    m_dwLastKnownFileLength = 0x10 + dwNewReservedSize + dwNewZEXESize + dwNewTotalTagSize;

    f.Close();

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
//
// Checks that the PSF file structure is valid.
//
// (Does NOT verify the program CRC or validity of the tag.)
//
BOOL CPSF::CheckValidPSF(File &f, BOOL bStrict)
{
    DWORD l = (DWORD)(f.Length());
    if(l < 0x10) return FileError(f, L"Not a valid PSF file");
    if(l >= 0x10000000) return FileError(f, L"PSF file is too large; likely corrupt");

    BYTE hdr[0x10];
    memset(hdr, 0, sizeof(hdr));
    f.Seek(0, File::BEGIN);
    if(f.Read(hdr, 0x10) != 0x10) return FileError(f);
    if(memcmp(hdr, "PSF", 3)) return FileError(f, L"Not a valid PSF file");

    DWORD dwReservedSize = Util::Get32LSB(hdr + 0x4);
    DWORD dwZEXESize     = Util::Get32LSB(hdr + 0x8);
    //
    // Sanity check on all lengths
    //
    if(
        (dwReservedSize > l) ||
        (dwZEXESize > l) ||
        ((dwReservedSize + dwZEXESize) > l)
    ) {
        return FileError(f, L"Not a valid PSF file");
    }

    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
/*
** Decompress and obtain program data
** pdwDestSize should be initialized to the size of the buffer in lpDest
** pdwDestSize will be set to the actual size of the program
** returns FALSE on error:
** - if pdwDestSize was set to PSF_SIZE_ERROR, there wasn't enough room
** - for other errors, refer to GetLastError
*/
BOOL CPSF::GetProgramData(LPVOID lpDest, DWORD *pdwDestSize)
{
  ASSERT(pdwDestSize);

  // Make a local copy of this ahead of time and set a default return value
  DWORD dwDestSize = *pdwDestSize;
  *pdwDestSize = 0;

  // fail if the ZEXE wasn't loaded
  if(!(m_dwValidFlags & PSF_FILE_ZEXE)) {
    m_strLastError = L"CPSF::GetProgramData - PSF_FILE_ZEXE not present (program error)";
    return FALSE;
  }

  if(!(m_aZEXE.GetSize())) {
    m_strLastError = L"Compressed program data is not present";
    return FALSE;
  }

  CPSFDecompressor d;
  int r = d.Decompress(lpDest, &dwDestSize, m_aZEXE.GetData(), m_aZEXE.GetSize());
  switch(r) {
  case CPSFDecompressor::RESULT_OK:
    *pdwDestSize = dwDestSize;
    return TRUE;
  case CPSFDecompressor::RESULT_SIZE_ERROR:
    m_strLastError = d.GetLastError();
    *pdwDestSize = (DWORD)PSF_SIZE_ERROR;
    break;
  default:
    m_strLastError = d.GetLastError();
    break;
  }
  return FALSE;
}

////////////////////////////////////////////////////////////////////////////////
/*
** Recompress program data
** returns FALSE on error
**
** Can handle lpSrc == NULL if dwSrcSize is 0
*/
BOOL CPSF::SetProgramData(LPVOID lpSrc, DWORD dwSrcSize, UINT uMethod)
{
  // invalidate the existing zexe if it's there
  Invalidate(PSF_FILE_ZEXE);

  // if there's no data, should at least point lpSrc somewhere
  BYTE b = 0;
  if(!dwSrcSize) lpSrc = &b;

  CPSFCompressor c;

  // create buffer room for the new zexe
  DWORD dwBufferSize = c.GetMaxCompressedSize(dwSrcSize, uMethod);
  m_aZEXE.SetSize(dwBufferSize);

  // attempt compression
  int r = c.Compress(m_aZEXE.GetData(), &dwBufferSize, lpSrc, dwSrcSize, uMethod);

  // on success:
  if(r == CPSFCompressor::RESULT_OK) {
    // scale the final zexe size down
    m_aZEXE.SetSize(dwBufferSize);
    // and set the valid flag
    m_dwValidFlags |= PSF_FILE_ZEXE;
    return TRUE;
  }

  // don't bother retrying on size error - we followed the rules
  m_strLastError = c.GetLastError();

  FreeAllInvalid();

  return FALSE;
}

////////////////////////////////////////////////////////////////////////////////

DWORD CPSF::GetCompressedProgramSize()
{
  ASSERT(m_dwValidFlags & PSF_FILE_ZEXE);
  return m_aZEXE.GetSize();
}

////////////////////////////////////////////////////////////////////////////////

void CPSF::SetReservedSize(DWORD dwSize) { m_aReserved.SetSize(dwSize); m_dwValidFlags |= PSF_FILE_RESERVED; }
void CPSF::SetTagSize     (DWORD dwSize) { m_aTag     .SetSize(dwSize); m_dwValidFlags |= PSF_FILE_TAG     ; }

LPVOID CPSF::GetReservedPtr() { ASSERT(m_dwValidFlags & PSF_FILE_RESERVED); return m_aReserved.GetData(); }
LPVOID CPSF::GetTagPtr     () { ASSERT(m_dwValidFlags & PSF_FILE_TAG     ); return m_aTag     .GetData(); }
DWORD CPSF::GetReservedSize() { ASSERT(m_dwValidFlags & PSF_FILE_RESERVED); return m_aReserved.GetSize(); }
DWORD CPSF::GetTagSize     () { ASSERT(m_dwValidFlags & PSF_FILE_TAG     ); return m_aTag     .GetSize(); }

////////////////////////////////////////////////////////////////////////////////

void CPSF::DeleteFromMemory(DWORD dwMask)
{
  Invalidate(dwMask);
}

////////////////////////////////////////////////////////////////////////////////
//
// Returns -1 if unknown
//
int CPSF::GetVersion() {
    if(!(m_dwValidFlags & PSF_FILE_VERSION)) return -1;
    return m_ucVersion;
}

void CPSF::SetVersion(BYTE ucVersion) {
    m_ucVersion = ucVersion;
    m_dwValidFlags |= PSF_FILE_VERSION;
}

////////////////////////////////////////////////////////////////////////////////

DWORD CPSF::GetLastKnownFileLength()
{
    return m_dwLastKnownFileLength;
}

////////////////////////////////////////////////////////////////////////////////
//
// Create a psflib path from a PSF file path and a _lib entry
//
CStringW CPSF::MakeLibPath(CStringW psf_path, CStringW lib_path) {
    int i;
    int psf_path_len = psf_path.GetLength();
    int psf_cut_len = 0;
    for(i = 0; i < psf_path_len; i++) {
        if(psf_path[i] == '\\' || psf_path[i] == '/') {
            psf_cut_len = i + 1;
        }
    }
    psf_path.Truncate(psf_cut_len);
    lib_path.TrimLeft(L"/\\");
    return psf_path + lib_path;
}

////////////////////////////////////////////////////////////////////////////////
