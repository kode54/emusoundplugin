// PSFDecompressor.cpp: implementation of the CPSFDecompressor class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "PSFDecompressor.h"

#include "../../zlib/zlib.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPSFDecompressor::CPSFDecompressor()
{

}

CPSFDecompressor::~CPSFDecompressor()
{

}

//
// Static function to compute a CRC32
//
DWORD CPSFDecompressor::CRC32(LPVOID lpData, DWORD dwSize)
{
    return crc32(crc32(0, 0, 0), (unsigned char*)lpData, dwSize);
}

int CPSFDecompressor::Decompress(LPVOID lpDest, DWORD *pdwDestSize, LPVOID lpSrc, DWORD dwSrcSize)
{
    int r = uncompress((LPBYTE)lpDest, pdwDestSize, (LPBYTE)lpSrc, dwSrcSize);
    switch(r) {
    case Z_OK:
        return RESULT_OK;
    case Z_MEM_ERROR:
        m_strLastError = L"Out of memory";
        return RESULT_FATAL_ERROR;
    case Z_BUF_ERROR:
        m_strLastError = L"Not enough space in the decompression buffer";
        return RESULT_SIZE_ERROR;
    case Z_DATA_ERROR:
        m_strLastError = L"Compressed data is corrupt";
        return RESULT_FATAL_ERROR;
    default:
        m_strLastError.Format(L"Unknown error (%d)", r);
        return RESULT_FATAL_ERROR;
    }
}

CStringW CPSFDecompressor::GetVersion()
{
    CStringW ver;
    ver.Format(L"zlib %s uncompress", LPCWSTR(CStringW(zlibVersion())));
    return ver;
}
