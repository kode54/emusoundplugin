#pragma once

#include "../../EmuSoundPlugin/Player.h"

class CPSFPlayer : public CPlayer  
{
public:

	CPSFPlayer(File f);
	virtual ~CPSFPlayer();

	virtual void ReloadTag();

protected:
    void BuildExecutableError(File f, CStringW strError);

};
