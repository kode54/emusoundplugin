// PSFPlugin.cpp: implementation of the CPSFPlugin class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PSFPlugin.h"

#include "PSFTag.h"
#include "../../MFCUtil/NumString.h"

#include "PSFConfigTitlePage.h"
#include "PSFInfoDlg.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPSFPlugin::CPSFPlugin()
{
    opt_strTitleFormat          = L"%game% - %title%";
    opt_strTitleFormatFallback  = L"%filename%";
}

CPSFPlugin::~CPSFPlugin()
{

}

/////////////////////////////////////////////////////////////////////////////

ESP_OPTION_START(CPSFPlugin, CEmuSoundPlugin)
    
ESP_OPTION(L"TitleFormat"        , OPT_WSTRING, opt_strTitleFormat        , ESP_WINAMP2)
ESP_OPTION(L"TitleFormatFallback", OPT_WSTRING, opt_strTitleFormatFallback, ESP_WINAMP2)

ESP_OPTION_END(CPSFPlugin, CEmuSoundPlugin)

/////////////////////////////////////////////////////////////////////////////

static int FormatTitleString(LPCWSTR lpszFormat, LPCWSTR lpszPathName, CPSFTag *pPSFTag, CStringW &strResult)
{
  int nVariablesFilled = 0;
  strResult.Empty();
  /*
  ** Extract the file dir, name, and ext from the given pathname
  */
  CStringW strFileDir;
  CStringW strFileName = lpszPathName;
  CStringW strFileExt;
  int pl = wcslen(lpszPathName);
  int dotindex = -1;
  int slashindex = -1;
  int i;
  for(i = 0; i < pl; i++) {
    switch(lpszPathName[i]) {
    case '/': case '\\': slashindex = i; dotindex = -1; break;
    case '.': dotindex = i; break;
    }
  }
  if(slashindex >= 0) {
    strFileDir = strFileName.Left(slashindex + 1);
    strFileName = strFileName.Mid(slashindex + 1);
    dotindex -= (slashindex + 1);
  }
  if(dotindex >= 0) {
    strFileExt = strFileName.Mid(dotindex);
    strFileName = strFileName.Left(dotindex);
  }

  for(; *lpszFormat; lpszFormat++) {
    /* Variable */
    if(*lpszFormat == '%') {
      CHAR szVar[100];
      int i = 0;
      lpszFormat++;
      while((i < 99) && (*lpszFormat) && ((*lpszFormat) != L'%')) {
        WCHAR c = *lpszFormat++;
        if(iswspace(c) && (!i)) continue;
        if(c >= 'A' && c <= 'Z') {
          szVar[i++] = (CHAR)c + 'a' - 'A';
        } else if((c >= 'a' && c <= 'z') || c == '_') {
          szVar[i++] = (CHAR)c;
        } else if(c >= '0' && c <= '9' && i) {
          szVar[i++] = (CHAR)c;
        } else {
          break;
        }
      }
      /* Skip to the end of variable anyway regardless of ending condition */
      while((*lpszFormat) && ((*lpszFormat) != '%')) { lpszFormat++; }
      szVar[i] = 0;
      /*
      ** IMPORTANT - if the last character was a null, we must backtrack by one!
      */
      if(!(*lpszFormat)) { lpszFormat--; }
      /* If it's a %%, just output a percent sign */
      if(!i) {
        strResult += L'%';
      } else {
        /*
        ** Internal variables
        ** These are always filled by definition, even if they were empty
        */
               if(!strcmp(szVar, "filedir")) {
          strResult += strFileDir;
          nVariablesFilled++;
        } else if(!strcmp(szVar, "filename")) {
          strResult += strFileName;
          nVariablesFilled++;
        } else if(!strcmp(szVar, "fileext")) {
          strResult += strFileExt;
          nVariablesFilled++;
        /* External variable - get from tag */
        } else {
          CStringW strValue;
          if(pPSFTag) strValue = pPSFTag->GetVarW(CStringW(szVar));
          if(!(strValue.IsEmpty())) {
            strResult += strValue;
            nVariablesFilled++;
          }
        }
      }
    /* Not a variable - just a regular character */
    } else {
      strResult += (*lpszFormat);
    }
  }
  return nVariablesFilled;
}

/////////////////////////////////////////////////////////////////////////////
//
// Path must be valid
//
CMusicFileTag* CPSFPlugin::GetMetaData(File f, CStringW &strTitle, INT64 &nLengthMS)
{
    nLengthMS = 0;

    CStringW strWidePath = f.GetPath();

    LockOptions();
    BOOL bStrict = opt_bStrictFormatChecking;
    CStringW strTitleFormat         = opt_strTitleFormat;
    CStringW strTitleFormatFallback = opt_strTitleFormatFallback;
    UnlockOptions();

    // may end up being NULL
    CPSFTag *pPSFTag = new CPSFTag;
    if(!pPSFTag->ReadFromFile(f, bStrict)) { delete pPSFTag; pPSFTag = NULL; }

    strTitle.Empty();
    // Try to format using normal title format
    int nVariablesFilled = FormatTitleString(strTitleFormat, strWidePath, pPSFTag, strTitle);
    // If no variables were filled, try the callback
    if(!nVariablesFilled) {
        FormatTitleString(strTitleFormatFallback, strWidePath, pPSFTag, strTitle);
    }

    // Try to get the length from the tag
    CStringW strLength;
    CStringW strFade;
    double dLength = 0.0;
    double dFade = 0.0;
    BOOL bLengthParsed = FALSE;
    BOOL bFadeParsed = FALSE;

    if(pPSFTag) {
        strLength = pPSFTag->GetVarW(L"length");
        strFade   = pPSFTag->GetVarW(L"fade"  );
        bLengthParsed = Util::StringToNum(strLength, dLength, Util::NSFLAG_COLONS);
        bFadeParsed   = Util::StringToNum(strFade  , dFade  , Util::NSFLAG_COLONS);
    }

    // If the fade didn't parse, just assume it's zero
    if(!bFadeParsed) { dFade = 0.0; }

    // If the length didn't parse, then we have problems.
    // Use the global default length.
    if(!bLengthParsed) {
        LockOptions();
        dLength = opt_dDefaultLength;
        dFade   = opt_dDefaultFade;
        UnlockOptions();
    }

    nLengthMS = (INT64)(1000.0 * (dLength + dFade));

    return pPSFTag; // may be NULL
}

/////////////////////////////////////////////////////////////////////////////

CMusicFileTag* CPSFPlugin::NewMetaData(File f)
{
    return new CPSFTag();
}

/////////////////////////////////////////////////////////////////////////////

void CPSFPlugin::BuildConfigPropertySheet(CPropertySheet *pSheet) {

    CEmuSoundPlugin::BuildConfigPropertySheet(pSheet);

    //
    // Add Title page if this was invoked from Winamp2
    //
    CPSFPlugin *plug = (CPSFPlugin*)g_pPlugin;
    if(plug->IsOptionSupported(&plug->opt_strTitleFormat)) {
        CPSFConfigTitlePage *pPageTitle = new CPSFConfigTitlePage;
        pPageTitle->BindToPlugin(this);
        pSheet->AddPage(pPageTitle);
    }

}

/////////////////////////////////////////////////////////////////////////////

void CPSFPlugin::InfoDialog(File f, HWND hWndParent, LONG *plNowMS)
{
    LockOptions();
    BOOL bStrict = opt_bStrictFormatChecking;
    UnlockOptions();

    CPSFTag *pPSFTag = new CPSFTag;
    if(!pPSFTag->ReadFromFile(f, bStrict)) {

        CStringW m = L"Unable to read tag for:\n";
        m += f.GetPath();

        CStringW strError = pPSFTag->GetLastError();
        if(!strError.IsEmpty()) {
            m += L"\n\n";
            m += strError;
        }
        ErrorBox(hWndParent, m);

        delete pPSFTag;
        return;
    }

    int version = -1;
    int bytes = 0;

    if(f.Open(File::READ)) {
        bytes = f.Length();

        BYTE buf[4];
        buf[3] = 0;
        f.Read(buf, 4);
        version = buf[3];

        f.Close();
    }

    CPSFInfoDlg dlg(CWnd::FromHandle(hWndParent));

    dlg.m_file = f;
    dlg.m_strPathName = f.GetPath();
    dlg.m_nBytes = bytes;
    dlg.m_pPSFTag = pPSFTag;
    dlg.m_plNowMS = plNowMS;
    dlg.m_pPlugin = this;
    dlg.m_nPSFVersion = version;

    int r = dlg.DoModal();

    if(r == IDOK) {
        BOOL b = pPSFTag->WriteToFile(f, bStrict);
        if(!b) {
            CStringW m = L"Unable to write tag for:\n";
            m += f.GetPath();

            CStringW strError = pPSFTag->GetLastError();
            if(!strError.IsEmpty()) {
                m += L"\n\n";
                m += strError;
            }
            ErrorBox(hWndParent, m);
        }
    }

    delete pPSFTag;
}

int CPSFPlugin::GetPSFLogoBitmap(File f, CPSFTag *pPSFTag, int nVersion)
{
    return 0;
}

LPCWSTR* CPSFPlugin::GetAllPSFTypes()
{
    static LPCWSTR list[] = {
        NULL
    };
    return list;
}

LPCWSTR CPSFPlugin::GetPSFType(int nPSFVersion)
{
    // default implementation: empty string
    return L"";
}

////////////////////////////////////////////////////////////////////////////////

int CPSFPlugin::GrabPSFVersion(File f, CStringW &strError) {
    CPSF psf;
    if(!psf.ReadFromFile(f, CPSF::PSF_FILE_VERSION, FALSE)) {
        strError = psf.GetLastError();
        return -1;
    }
    return psf.GetVersion();
}

////////////////////////////////////////////////////////////////////////////////
