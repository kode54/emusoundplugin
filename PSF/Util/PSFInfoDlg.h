#pragma once

#include "PSFTag.h"
#include "PSFPlugin.h"

#include "../../MFCUtil/HandStatic.h"
#include "../../MFCUtil/File.h"

/////////////////////////////////////////////////////////////////////////////
// CPSFInfoDlg dialog

class CPSFInfoDlg : public CDialog
{
// Construction
public:
    File m_file;

    LONG *m_plNowMS;
	CPSFTag *m_pPSFTag;
	int m_nPSFVersion;
    CPSFPlugin *m_pPlugin;
    int m_nBytes;

    bool m_bReplaygainKnown;

	CPSFInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPSFInfoDlg)
	enum { IDD = IDD_PSF_INFO };
	CStatic	m_cPSFByLabel;
	CEdit	m_cBytes;
	CHandStatic	m_cLogo;
	CEdit	m_cLength;
	CSliderCtrl	m_cVolumeSlider;
	CEdit	m_cVolume;
	CString	m_strPathName;
    CStatic m_cReplaygain;
    CStatic m_cUsingRG;
    CButton m_cScan;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPSFInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPSFInfoDlg)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnPsfNow();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnChangePsfVolume();
	afx_msg void OnPsfRawmode();
	afx_msg void OnPsfConfiguration();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
    void SetVolume(LPCWSTR lpszVolString);
    void UpdateVolumeFromSlider();
    void UpdateSliderFromVolume();
    void UpdateReplaygainFromTag();
    void UpdateShowReplaygainFrame();

    CStringW ActualVarName(LPCWSTR lpszVarFormat);

    void SetAllFieldsFromTag();
    void SyncTagToAllFields();

    CStringW m_strCancelVolume;

    bool m_bRGWindowOn;
    bool m_bMVWindowOn;

public:
    afx_msg void OnStnClickedPsfLogo();
    afx_msg void OnBnClickedPsfScan();
};
