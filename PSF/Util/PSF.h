////////////////////////////////////////////////////////////////////////////////
//
// PSF - A class that represents a PSF file
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../../MFCUtil/File.h"

class CPSF  
{
public:
	DWORD GetLastKnownFileLength();
	void SetVersion(BYTE ucVersion);
	int GetVersion();
	void DeleteFromMemory(DWORD dwMask);
	DWORD GetReservedSize();
	DWORD GetTagSize();
	LPVOID GetReservedPtr();
	LPVOID GetTagPtr();
	void SetReservedSize(DWORD dwSize);
	void SetTagSize(DWORD dwSize);
	DWORD GetCompressedProgramSize();
	BOOL SetProgramData(LPVOID lpSrc, DWORD dwSrcSize, UINT uMethod);
	BOOL GetProgramData(LPVOID lpDest, DWORD *pdwDestSize);
	
    BOOL ReadFromFile(File f, DWORD dwMask, BOOL bStrict);
	BOOL WriteToFile (File f, DWORD dwMask, BOOL bStrict);

    static CStringW MakeLibPath(CStringW psf_path, CStringW lib_path);

    CPSF();
	virtual ~CPSF();

    enum {
        PSF_FILE_VERSION  = 0x01,
        PSF_FILE_RESERVED = 0x02,
        PSF_FILE_ZEXE     = 0x04,
        PSF_FILE_TAG      = 0x08,
        PSF_FILE_ALL      = 0x0F,
        PSF_SIZE_ERROR    = 0xFFFFFFFF,
        PSF_PROTOCAT_TYPE_R = 86
    };

    enum { MAX_TAG_BYTES = 50000 };

    CStringW GetLastError() const { return m_strLastError; }

private:
	DWORD m_dwLastKnownFileLength;

    BOOL WriteToWholeFile  (File &f);
	BOOL WriteToPartialFile(File &f, DWORD dwMask, BOOL bStrict);

	BOOL CheckValidPSF     (File &f, BOOL bStrict);
	void Invalidate(DWORD dwMask);
	void FreeAllInvalid();

    BOOL FileError(File &f, LPCWSTR message = L"");

	DWORD m_dwValidFlags;
	BYTE m_ucVersion;
	CByteArray m_aReserved;
	CByteArray m_aZEXE;
	CByteArray m_aTag;

	CStringW m_strLastError;
};
