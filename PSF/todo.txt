- eliminate all CreateThread (there's just 3 left, in psfocycle)
- redo winamp2.cpp so that playerCurrent is allocated dynamically
- redo CPlayer to handle silence-eating internally
- eliminate GetDlgItem*/SetDlgItem* in PSFLab, PSFoCycle and perhaps the HE field updater loop
- pitch mod
- eliminate PreTranslateMessage?
  used in: 
   1. HE about box for the 'dbg' secret code
   2. two places in PSFLab for button updating
   3. two places in PSFoCycle for button updating
  that's it
- examine control management, especially in psflab/psfocycle
- replace WM_USER with WM_APP where possible (various places in psfocycle)
- consider subclassing the LogBox in psfocycle
- consider using message-based queries for global variables
  i.e. var = GetParent()->SendMessage(blah)
- clean up uses of global headers, i.e. remove the WinApp header #include from each module


- getting requests for channel muting, should maybe see about implementing
