/////////////////////////////////////////////////////////////////////////////
//
// Simple utility to create a PSF2 file from a given directory
// Written by Neill Corlett
// Released under the terms of the GNU General Public License
//
// You need zlib to compile this.
// It's available at http://www.gzip.org/zlib/
//
/////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dir.h>
#include <dirent.h>

#include "zlib.h"

/////////////////////////////////////////////////////////////////////////////

unsigned char *reserved_buffer = NULL;
int reserved_size = 0;
int reserved_max_size = 0;
int reserved_max_growby = 0x40000;

void grow_reserved(int size) {
  if(size > reserved_max_size) {
    while(size > reserved_max_size) reserved_max_size += reserved_max_growby;
    reserved_buffer = realloc(reserved_buffer, reserved_max_size);
    if(!reserved_buffer) abort();
  }
  reserved_size = size;
}

int reserved_alloc(int size) {
  int ofs = reserved_size;
  grow_reserved(ofs + size);
  return ofs;
}

int get_reserved_top(void) {
  return reserved_size;
}

/////////////////////////////////////////////////////////////////////////////
//
// see if we can overwrite the given filename
//
int overwrite_check(const char *filename) {
  char hdr[4];
  int r;
  FILE *f = fopen(filename, "rb");
  if(!f) return 0;
  r = fread(hdr, 1, 4, f);
  fclose(f);
  if(r != 4) {
    printf("can't verify whether %s is overwritable; quitting\n", filename);
    return -1;
  }
  if(memcmp(hdr, "PSF\x2", 4)) {
    printf("%s exists and is not PSF2; will not overwrite\n", filename);
    return -1;
  }
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
//
// is this a directory?
//
int isdir(const char *name) {
  if(chdir(name) < 0) return 0; // no
  chdir("..");
  return 1; // yes
}

int getfilelen(const char *name) {
  FILE *f;
  int l;
  f = fopen(name, "rb");
  if(!f) return -1;
  fseek(f, 0, SEEK_END);
  l = ftell(f);
  fseek(f, 0, SEEK_SET);
  fclose(f);
  return l;
}

/////////////////////////////////////////////////////////////////////////////

void put32lsb(unsigned char *p, unsigned value) {
  p[0] = (value >>  0) & 0xFF;
  p[1] = (value >>  8) & 0xFF;
  p[2] = (value >> 16) & 0xFF;
  p[3] = (value >> 24) & 0xFF;
}

unsigned get32lsb(const unsigned char *p) {
  return (
    ((((unsigned)(p[0])) & 0xFF) <<  0) |
    ((((unsigned)(p[1])) & 0xFF) <<  8) |
    ((((unsigned)(p[2])) & 0xFF) << 16) |
    ((((unsigned)(p[3])) & 0xFF) << 24)
  );
}

/////////////////////////////////////////////////////////////////////////////

int buildfile(const char *filename, int length, int block_size) {
  int b, blocks, base;
  FILE *f = NULL;
  char *block_buffer = NULL;
  char *block_zbuffer = NULL;
  int block_zbuffer_size = block_size + 13 + (block_size / 1000);

  block_buffer = malloc(block_size);
  block_zbuffer = malloc(block_zbuffer_size);

  printf("[%d] file: %s\n", get_reserved_top(), filename);

  f = fopen(filename, "rb");
  if(!f) {
    printf("cannot open %s: %s\n", filename, strerror(errno));
    goto error;
  }

  blocks = (length + (block_size - 1)) / block_size;
  base = reserved_alloc(4 * blocks);

  for(b = 0; b < blocks; b++) {
    int r, l, o;
    unsigned long destlen = block_zbuffer_size;
    l = length - (b * block_size);
    if(l > block_size) l = block_size;
    r = fread(block_buffer, 1, l, f);
    if(r != l) {
      printf("error reading position %d\n", b*block_size);
      goto error;
    }
    r = compress2(block_zbuffer, &destlen, block_buffer, l, 9);
    if(r != Z_OK) {
      printf("error compressing position %d\n", b*block_size);
      goto error;
    }
    put32lsb(reserved_buffer + base + 4 * b, destlen);
    o = reserved_alloc(destlen);
    memcpy(reserved_buffer + o, block_zbuffer, destlen);
  }

  fclose(f);

  return 0;
error:
  if(f) fclose(f);
  if(block_buffer) free(block_buffer);
  if(block_zbuffer) free(block_zbuffer);
  return -1;
}

/////////////////////////////////////////////////////////////////////////////

int builddir(const char *dirname, int block_size) {
  int base = get_reserved_top();
  int n;
  int n_entries = 0;
  DIR *dir = NULL;

  char lastpath[1024];
  char *p;
  int r;
  printf("[%d] entering %s...\n", get_reserved_top(), dirname);

  p = getcwd(lastpath, sizeof(lastpath));
  if(!p) {
    printf("unable to save previous dir; giving up\n");
    return -1;
  }
  r = chdir(dirname);
  if(r < 0) {
    printf("unable to switch to directory; giving up\n");
    return -1;
  }

  // allocate space for the number of entries
  reserved_alloc(4);

  dir = opendir(".");
  if(!dir) {
    printf("unable to read directory; giving up\n");
    return -1;
  }

  //
  // pass A: build the directory list
  //
  for(;;) {
    int ofs;
    struct dirent *ent = readdir(dir);
    if(!ent) break;
    if(!strcmp(ent->d_name, ".")) continue;
    if(!strcmp(ent->d_name, "..")) continue;
    if(strlen(ent->d_name) > 36) {
      printf("filename '%s' is too long\n", ent->d_name);
      goto error;
    }
    ofs = reserved_alloc(48);
    memset(reserved_buffer + ofs, 0, 48);
    strncpy(reserved_buffer + ofs, ent->d_name, 36);
    if(isdir(ent->d_name)) {
      put32lsb(reserved_buffer + ofs + 36, 1);
    } else {
      int l = getfilelen(ent->d_name);
      if(l < 0) {
        printf("unable to get file size for %s\n", ent->d_name);
        goto error;
      }
      put32lsb(reserved_buffer + ofs + 40, l);
      put32lsb(reserved_buffer + ofs + 44, block_size);
    }
    n_entries++;
  }
  put32lsb(reserved_buffer + base, n_entries);

  closedir(dir);
  dir = NULL;

  //
  // pass B: include files and traverse subdirectories
  //
  for(n = 0; n < n_entries; n++) {
    int ofs = base + 4 + 48 * n;
    int is_sub;
    char name[37];
    memcpy(name, reserved_buffer + ofs, 36);
    name[36] = 0;
    is_sub = get32lsb(reserved_buffer + ofs + 36);
    put32lsb(reserved_buffer + ofs + 36, get_reserved_top());
    if(is_sub) {
      // subdirectory
      // recursively traverse
      if(builddir(name, block_size) < 0) goto error;
    } else {
      // file
      int l = get32lsb(reserved_buffer + ofs + 40);
      if(l) {
        if(buildfile(name, l, block_size) < 0) goto error;
      }
    }

  }


  r = chdir(lastpath);
  if(r < 0) {
    printf("unable to switch to previous directory; giving up\n");
    goto error;
  }

  printf("[%d] exiting %s (%d %s)\n", get_reserved_top(), dirname, n_entries, n_entries==1?"entry":"entries");
  return 0;

error:
  if(dir) closedir(dir);
  return -1;
}

/////////////////////////////////////////////////////////////////////////////

int psf2create(const char *psf2filename, const char *dirname) {
  FILE *f;
  int r;
  if(overwrite_check(psf2filename) < 0) return -1;
  if(builddir(dirname, 32768) < 0) return -1;
  printf("[%d] finished\n", get_reserved_top());
  printf("creating %s...\n", psf2filename);
  f = fopen(psf2filename, "wb");
  if(!f) {
    printf("unable to create %s: %s\n", psf2filename, strerror(errno));
    goto error;
  }
  r = get_reserved_top();
  fwrite("PSF\x2", 1, 4, f);
  fputc((r >>  0) & 0xFF, f);
  fputc((r >>  8) & 0xFF, f);
  fputc((r >> 16) & 0xFF, f);
  fputc((r >> 24) & 0xFF, f);
  fputc(0, f);fputc(0, f);fputc(0, f);fputc(0, f);
  fputc(0, f);fputc(0, f);fputc(0, f);fputc(0, f);
  fwrite(reserved_buffer, 1, r, f);
  fclose(f);

  printf("done\n");
  return 0;
error:
  return -1;
}

/////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv) {
  int r;
  if(argc != 3) {
    printf("usage: %s psf2file directory\n", argv[0]);
    return 1;
  }
  r = psf2create(argv[1], argv[2]);
  if(r) return r;
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
