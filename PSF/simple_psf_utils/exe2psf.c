/*
** Simple utility to convert a Playstation EXE to a PSF1.
** Written by Neill Corlett
** Released under the terms of the GNU General Public License
**
** You need zlib to compile this.
** It's available at http://www.gzip.org/zlib/
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "zlib.h"

Byte   compbuf[0x300000];
Byte uncompbuf[0x300000];

int doexe2psf(const char *from, const char *to) {
  FILE *f;
  uLong ucl;
  uLong cl;
  uLong ccrc;
  int r;

  fprintf(stderr, "%s->%s: ", from, to);

  f=fopen(from,"rb");if(!f){perror(from);return 1;}
  ucl=fread(uncompbuf,1,sizeof(uncompbuf),f);
  fclose(f);
//  fprintf(stdout,"uncompressed: %ld bytes\n",ucl);fflush(stdout);

  cl = sizeof(compbuf);
  r=compress2(compbuf,&cl,uncompbuf,ucl,9);
  if(r!=Z_OK){fprintf(stderr,"zlib compress2() failed (%d)\n", r);return 1;}

//  fprintf(stdout,"compressed: %ld bytes\n",cl);fflush(stdout);

  f=fopen(to,"wb");if(!f){perror(to);return 1;}
  fputc('P',f);fputc('S',f);fputc('F',f);fputc(0x01,f);
  fputc(0,f);fputc(0,f);fputc(0,f);fputc(0,f);
  fputc(cl  >> 0,f);
  fputc(cl  >> 8,f);
  fputc(cl  >>16,f);
  fputc(cl  >>24,f);
  ccrc=crc32(crc32(0L, Z_NULL, 0), compbuf, cl);
  fputc(ccrc>> 0,f);
  fputc(ccrc>> 8,f);
  fputc(ccrc>>16,f);
  fputc(ccrc>>24,f);
  fwrite(compbuf,1,cl,f);
  fclose(f);
  fprintf(stderr,"ok\n");
  return 0;
}


int main(int argc, char **argv) {
  char s[1000];
  int i;
  int errors = 0;
  if(argc<2){
    fprintf(stderr,"usage: %s <psx-exe-files>\n", argv[0]);
    return 1;
  }
  for(i = 1; i < argc; i++) {
    strncpy(s, argv[i], 900);
    s[900] = 0;
    { char *e = s + strlen(s) - 4;
      if(!strcasecmp(e, ".exe")) { *e = 0; }
    }
    strcat(s, ".psf");
    errors += doexe2psf(argv[i], s);
  }
  fprintf(stderr, "%d error(s)\n", errors);
  return 0;
}
