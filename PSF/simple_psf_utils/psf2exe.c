/*
** Simple utility to convert a PSF1 to a Playstation EXE.
** Written by Neill Corlett
** Released under the terms of the GNU General Public License
**
** You need zlib to compile this.
** It's available at http://www.gzip.org/zlib/
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "zlib.h"

#define uint32 unsigned
#define uint16 unsigned short
#define uint8 unsigned char

static uint32 get32lsb(uint8 *src) {
  return
    ((((uint32)(src[0])) & 0xFF) <<  0) |
    ((((uint32)(src[1])) & 0xFF) <<  8) |
    ((((uint32)(src[2])) & 0xFF) << 16) |
    ((((uint32)(src[3])) & 0xFF) << 24);
}

unsigned realdestlen;

/***************************************************************************/
/*
** Read the EXE from a PSF file
**
** Returns the error message, or NULL on success
*/
const char *psf_read_exe(
  const char *filename,
  unsigned char *exebuffer,
  unsigned exebuffersize
) {
  FILE *f;
  uint8 *zexebuf;
  uLong destlen;
  uint32 reserved_size;
  uint32 exe_size;
  uint32 exe_crc;
  uint32 fl;
  uint8 hdr[0x10];

  f=fopen(filename,"rb");
  if(!f) return "Unable to open the file";
  fseek(f,0,SEEK_END);fl=ftell(f);fseek(f,0,SEEK_SET);
  /*
  ** Idiot check for insanely large or small files
  */
  if(fl >= 0x10000000) {
    fclose(f);
    return "PSF too large - likely corrupt";
  }
  if(fl < 0x10) {
    fclose(f);
    return "PSF too small - likely corrupt";
  }
  fread(hdr, 1, 0x10,f);
  if(memcmp(hdr, "PSF", 3)) {
    fclose(f);
    return "Invalid PSF format";
  }
  reserved_size = get32lsb(hdr +  4);
  exe_size      = get32lsb(hdr +  8);
  exe_crc       = get32lsb(hdr + 12);
  /*
  ** Consistency check on section lengths
  */
  if(
    (reserved_size > fl) ||
    (exe_size > fl) ||
    ((16+reserved_size+exe_size) > fl)
  ) {
    fclose(f);
    return "PSF header is inconsistent";
  }

  zexebuf=malloc(exe_size);
  if(!zexebuf) {
    fclose(f);
    return "Out of memory reading the file";
  }
  fseek(f, 16+reserved_size, SEEK_SET);
  fread(zexebuf, 1, exe_size, f);
  fclose(f);

  if(exe_crc != crc32(crc32(0L, Z_NULL, 0), zexebuf, exe_size)) {
    free(zexebuf);
    return "CRC failure - executable data is corrupt";
  }

  destlen = exebuffersize;

  if(uncompress(exebuffer, &destlen, zexebuf, exe_size) != Z_OK) {
    free(zexebuf);
    return "Decompression failed";
  }

  realdestlen = destlen;

  /*
  ** Okay, if the decompression worked, then the file is PROBABLY not corrupt.
  ** Hooray for that.
  */

  free(zexebuf);

  return NULL;
}

Byte exebuf[0x4000008];

int dopsf2exe(const char *from, const char *to) {
  const char *complaint;
  FILE*f;
  unsigned textsize;

  fprintf(stderr, "%s->%s: ", from, to);

  complaint = psf_read_exe(from, exebuf, 0x4000008);
  if(complaint) { fprintf(stderr, "%s\n", complaint); return 1; }

//  textsize = get32lsb(exebuf+0x1C);
  f=fopen(to, "wb");if(!f){perror(to);return 1;}
  fwrite(exebuf,realdestlen,1,f);
  fclose(f);

  fprintf(stderr,"ok\n");
  return 0;
}

int main(int argc, char **argv) {
  char s[1000];
  int i;
  int errors = 0;
  if(argc<2){
    fprintf(stderr,"usage: %s <psf-files>\n", argv[0]);
    return 1;
  }
  for(i = 1; i < argc; i++) {
    strncpy(s, argv[i], 900);
    s[900] = 0;
    { char *e = s + strlen(s) - 4;
      if(!strcasecmp(e, ".psf")) { *e = 0; }
    }
    strcat(s, ".exe");
    errors += dopsf2exe(argv[i], s);
  }
  fprintf(stderr, "%d error(s)\n", errors);
  return 0;
}
