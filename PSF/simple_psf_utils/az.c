#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "zlib.h"

unsigned char z[]={
#include "../Emu/biosaz.h"
};
unsigned char u[0x100000];

int main(void) {
  FILE*f;
  unsigned destlen;
  destlen = 0x100000;
  if(uncompress(u, &destlen, z, sizeof(z)) != Z_OK) abort();

  f=fopen("biosa.bin","wb");if(!f)abort();
  fwrite(u,0x10000,1,f);
fclose(f);
  return 0;
}
