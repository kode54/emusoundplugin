/*
** Simple utility to convert a binary file to a PSF file with any version number.
** Written by Neill Corlett
** Released under the terms of the GNU General Public License
**
** You need zlib to compile this.
** It's available at http://www.gzip.org/zlib/
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "zlib.h"

Byte   *compbuf=NULL;
Byte *uncompbuf=NULL;

int doexe2psf(int version, const char *from, const char *to) {
  FILE *f;
  uLong ucl;
  uLong cl;
  uLong ccrc;
  int r;

  fprintf(stderr, "%s->%s: ", from, to);

  f=fopen(from,"rb");if(!f){perror(from);return 1;}
  fseek(f,0,SEEK_END);ucl=ftell(f);fseek(f,0,SEEK_SET);
  cl = ucl+50+(ucl/100);

  if(uncompbuf) free(uncompbuf);
  if(compbuf) free(compbuf);
  uncompbuf = malloc(ucl);
  compbuf = malloc(cl);
  if(!uncompbuf) abort();
  if(!compbuf) abort();
  memset(uncompbuf,0,ucl);
  memset(compbuf,0,cl);

  fread(uncompbuf,1,ucl,f);
  fclose(f);
//  fprintf(stdout,"uncompressed: %ld bytes\n",ucl);fflush(stdout);

  r=compress2(compbuf,&cl,uncompbuf,ucl,9);
  if(r!=Z_OK){fprintf(stderr,"zlib compress2() failed (%d)\n", r);return 1;}

//  fprintf(stdout,"compressed: %ld bytes\n",cl);fflush(stdout);

  f=fopen(to,"wb");if(!f){perror(to);return 1;}
  fputc('P',f);fputc('S',f);fputc('F',f);fputc(version,f);
  fputc(0,f);fputc(0,f);fputc(0,f);fputc(0,f);
  fputc(cl  >> 0,f);
  fputc(cl  >> 8,f);
  fputc(cl  >>16,f);
  fputc(cl  >>24,f);
  ccrc=crc32(crc32(0L, Z_NULL, 0), compbuf, cl);
  fputc(ccrc>> 0,f);
  fputc(ccrc>> 8,f);
  fputc(ccrc>>16,f);
  fputc(ccrc>>24,f);
  fwrite(compbuf,1,cl,f);
  fclose(f);
  fprintf(stderr,"ok\n");
  return 0;
}


int main(int argc, char **argv) {
  char s[1000];
  int i;
  int errors = 0;
  int version = 0;
  if(argc<4){
    fprintf(stderr,"usage: %s <extension> <version-byte> <bin-files>\n", argv[0]);
    return 1;
  }
  version = strtoul(argv[2], NULL, 0);
  for(i = 3; i < argc; i++) {
    strncpy(s, argv[i], 998);
    s[998] = 0;
    { char *e = s + strlen(s) - 4;
      if(!strcasecmp(e, ".bin")) { *e = 0; }
    }
    strcat(s, ".");
    strcat(s, argv[1]);
    errors += doexe2psf(version, argv[i], s);
  }
  fprintf(stderr, "%d error(s)\n", errors);
  return 0;
}
