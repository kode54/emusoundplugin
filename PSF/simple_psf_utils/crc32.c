#include <stdio.h>
#include <stdlib.h>
#include "zlib.h"

unsigned char buf[65536];

int main(int argc, char **argv) {
  FILE *f;
  uLong mycrc = crc32(0L, Z_NULL, 0);
  if(argc != 2) {
    fprintf(stderr, "usage: %s filename\n", argv[0]);
    return 1;
  }
  f = fopen(argv[1], "rb");
  if(!f) {
    perror(argv[1]);
    return 1;
  }
  for(;;) {
    int l = fread(buf, 1, sizeof(buf), f);
    if(l <= 0) break;
    mycrc = crc32(mycrc, buf, l);
  }
  fclose(f);
  fprintf(stdout, "%08X\n", (unsigned)mycrc);
  return 0;
}
