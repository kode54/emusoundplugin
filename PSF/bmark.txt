C:\Corlett\Projects\PSF\dragongod_benchmark.psf

v1.09dev using basically emu0003:
60-sec play: 7831 ticks
60-sec seek: 4336 ticks

with the 12-bit opcode table:
60-sec play: 7921 ticks
60-sec seek: 4577 ticks

***CPU ONLY*** with the old decode method:
60-sec play: 4176 ticks
60-sec seek: 3175 ticks

with starscream-style memory mapping:
60-sec play: 7581 ticks
60-sec seek: 4196 ticks

3.1% faster playing
8.3% faster seeking

with new spu2 stuff:
60-sec play: 8112 ticks
60-sec seek: 4416 ticks
