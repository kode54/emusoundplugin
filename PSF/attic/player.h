#ifndef __EMU_PLAYER_H__
#define __EMU_PLAYER_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
** One-time static init
** Returns nonzero on error
*/
int EMU_CALL player_init(void);

/*
** Open a player using a PSF filename
*/
void* EMU_CALL player_open(const char *filename);

/*
** Read samples from the player.
**
** Returns the number of samples read.
** MAY return less than the number of samples requested on EOF or error.
*/
int EMU_CALL player_read(void *player, sint16 *buffer, int samples);

/*
** Tell how many samples have been read
*/
sint64 EMU_CALL player_tell(void *player);

/*
** Close a player
*/
void EMU_CALL player_close(void *player);

/*
** Rewind a player to zero samples
*/
void EMU_CALL player_rewind(void *player);

/*
** Seek to a given player position
**
** You may give it a callback and/or a callback context if you wish to
** receive progress notification.  Optional.  Either may be NULL.
**
** If the progress callback routine returns nonzero, the seek is canceled
** and you're stuck at wherever the halfway progress was.
*/
void EMU_CALL player_seek(
  void *player,
  sint64 samples,
  int (EMU_CALL *progresscallback)(void*),
  void *context
);

#ifdef __cplusplus
}
#endif

#endif
