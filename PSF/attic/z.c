#include <stdio.h>
#include <stdlib.h>
#include "zlib.h"

Byte   compbuf[0x200000];
Byte uncompbuf[0x200000];

int main(int argc, char **argv) {
  FILE *f;
  uLong ucl;
  uLong cl;
  int r;

  if(argc!=3){
    fprintf(stderr,"usage: %s file zfile\n", argv[0]);
    return 1;
  }

  f=fopen(argv[1],"rb");if(!f){perror(argv[1]);abort();}
  ucl=fread(uncompbuf,1,sizeof(uncompbuf),f);
  fclose(f);
  fprintf(stdout,"uncompressed: %ld bytes\n",ucl);fflush(stdout);

  r=compress2(compbuf,&cl,uncompbuf,ucl,9);
  if(r!=Z_OK){fprintf(stderr,"zlib compress2() failed\n");abort();}

  fprintf(stdout,"compressed: %ld bytes\n",cl);fflush(stdout);

  f=fopen(argv[2],"wb");if(!f){perror(argv[2]);abort();}
  fwrite(compbuf,1,cl,f);
  fclose(f);
  fprintf(stdout,"ok\n");

  return 0;
}
