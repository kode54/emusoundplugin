/***************************************************************************/
/*
** player.c - PSF playing/management
**
** With thread-safe synchronization! (optional)
*/
/***************************************************************************/

#include "emu.h"

/***************************************************************************/
/*
** One-time static init
** Returns nonzero on error
*/
int EMU_CALL player_init(void) {
  return 0;
}

/***************************************************************************/

struct PLAYER_STATE {
  void *hw;
  void *filter;
  uint8 *exebuffer;
  uint32 exebuffersize;
  sint64 samples_decoded;
};
#define PLAYER ((struct PLAYER_STATE*)(player))

/***************************************************************************/
/*
** Tell how many samples have been read
*/
sint64 EMU_CALL player_tell(void *player) {
  sint64 p;
  if(!player) return 0;
  p = PLAYER->samples_decoded;
  return p;
}

/***************************************************************************/
/*
** Rewind a player to zero samples
*/
void EMU_CALL player_rewind(void *player) {
  uint32 text_start;
  uint32 text_length;
  uint32 pc;
  uint32 sp;
  uint32 u;

  if(!player) return;

  PLAYER->samples_decoded = 0;

  if(!(PLAYER->exebuffer)) return;
  if((PLAYER->exebuffersize) < 0x800) return;

  text_start  = *((uint32*)((PLAYER->exebuffer)+0x18));
  text_length = *((uint32*)((PLAYER->exebuffer)+0x1C));
  pc          = *((uint32*)((PLAYER->exebuffer)+0x10));
  sp          = *((uint32*)((PLAYER->exebuffer)+0x30));

  text_start &= 0xFFFFFFFC;
  text_length &= 0xFFFFFFFC;
  if(text_length > ((PLAYER->exebuffersize)-0x800)) {
    text_length = ((PLAYER->exebuffersize)-0x800);
  }

  hw_clear_state(PLAYER->hw);
  for(u = 0; u < text_length; u += 4) {
    hw_sw(PLAYER->hw, text_start + u, *((uint32*)((PLAYER->exebuffer)+0x800+u)));
  }
  hw_setreg(PLAYER->hw, HW_REG_CPU_PC    , pc);
  hw_setreg(PLAYER->hw, HW_REG_CPU_GEN+29, sp);
}

/***************************************************************************/

void* EMU_CALL player_open(const char *filename) {
  struct PLAYER_STATE *player;

  player = malloc(sizeof(struct PLAYER_STATE));
  if(!player) return NULL;

  memset(player, 0, sizeof(struct PLAYER_STATE));

  player->hw = malloc(hw_get_state_size());
  if(!player->hw) {
    player_close(player);
    return NULL;
  }

  player->exebuffersize = 0x200000;
  player->exebuffer = malloc(player->exebuffersize);
  if(!player->exebuffer) {
    player_close(player);
    return NULL;
  }

  if(psf_read_exe(filename, player->exebuffer, player->exebuffersize)) {
    player_close(player);
    return NULL;
  }

  player_rewind(player);

  player->filter = filter_open();
  if(!player->filter) {
    player_close(player);
    return NULL;
  }

  return player;
}

/***************************************************************************/
/*
** ONLY returns less than the number of requested samples on unrecoverable error.
**
** buffer can be NULL for seeking purposes
*/
static int raw_read(struct PLAYER_STATE *player, sint16 *buffer, int samples) {
  int samples_left = samples;
  sint16 *b = buffer;
  while(samples_left > 0) {
    int n = samples_left;
    int r;
    r = hw_execute(PLAYER->hw, 0x7FFFFFFF, b, (unsigned*)(&n), 0);
    if(r < 0) break;
    if(b) { b += 2 * n; }
    samples_left -= n;
    PLAYER->samples_decoded += n;
  }
  return samples - samples_left;
}

/***************************************************************************/
/*
** Read with filtering, max length etc.
**
** ONLY returns less than the requested number of samples on error or EOF
*/
int EMU_CALL player_read(void *player, sint16 *buffer, int samples) {
  int samples_left = samples;
  sint16 *b = buffer;
  int done = 0;

  if(!player) return 0;

  while(!done) {
    int n;
    n = raw_read(PLAYER, b, samples_left);
    if(n < samples_left) { done = 1; }
    if(b) {
      n = filter_process(PLAYER->filter, b, n);
      b += 2 * n;
    }
    samples_left -= n;
    if(samples_left <= 0) { done = 1; }
  }

  return samples - samples_left;
}

/***************************************************************************/
/*
** Close a player
*/
void EMU_CALL player_close(void *player) {
  if(!player) return;
  if(PLAYER->filter) filter_close(PLAYER->filter);
  if(PLAYER->hw) free(PLAYER->hw);
  if(PLAYER->exebuffer) free(PLAYER->exebuffer);
  free(player);
}

/***************************************************************************/
/*
** In the future, this routine will do some smart state management
**
** For now, it just seeks dumbly.
*/
void EMU_CALL player_seek(
  void *player,
  sint64 samples,
  int (EMU_CALL *progresscallback)(void*),
  void *context
) {
  int cancel = 0;
  if(!player) return;
  if(samples < 0) return;
  for(;;) {
    sint64 d = player_tell(PLAYER);
    if(d == samples) break;
    if(d > samples) {
      player_rewind(PLAYER);
      continue;
    }
    d = samples - d;
    if(d > 576) d = 576;
    if(player_read(PLAYER, NULL, (int)d) < ((int)d)) break;
    if(progresscallback) {
      if(progresscallback(context)) {
        cancel = 1;
        break;
      }
    }
  }
  if(progresscallback) {
    if(!cancel) progresscallback(context);
  }
}

/***************************************************************************/
