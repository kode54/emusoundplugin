#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int freq[256];

unsigned char map[256];

int main(void) {

  int i;
  memset(freq,0,sizeof(freq));

  for(i=0;i<256;i++) {
    unsigned char a;
    unsigned char b;
    a = i;
    b = a * 37;

    map[a] = b;
    freq[b]++;
  }

  for(i=0;i<256;i++){
    if(freq[i]!=1){printf("f[%3d]=%d\n",i,freq[i]);fflush(stdout);abort();}
  }

  printf("encoding map:\n");
  for(i=0;i<256;i++) {
    int j;
    for(j=0;j<256;j++)if(map[j]==i)printf("0x%02X,",j);
    if((i&0xF)==0xF)printf("\n");
  }

  printf("done\n");

  return 0;
}
