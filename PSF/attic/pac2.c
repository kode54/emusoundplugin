#include <stdio.h>
#include <stdlib.h>

unsigned char buf[0x100000];

int findstr(unsigned char *src, unsigned char *thing, int len, int thinglen) {
  int i;
  len-=(thinglen-1);
  for(i=0;i<len;i++){
    if(!memcmp(src+i,thing,thinglen)) return i;
  }
  return -1;
}

int main(int argc, char **argv) {
  FILE *f;
  int pacl;
  int vhbegin;
  int vbbegin;
  int seqbegin;
  char s[100];

  if(argc!=2)abort();
  sprintf(s,"%s.PAC",argv[1]);
  f=fopen(s,"rb");if(!f)abort();
  pacl=fread(buf,1,sizeof(buf),f);
  fclose(f);

  vhbegin=findstr(buf,"pBAV",pacl,4);
  seqbegin=findstr(buf,"pQES",pacl,4);
  if(vhbegin<0)abort();
  if(seqbegin<0)abort();
  vbbegin=vhbegin+0x3000;

  sprintf(s,"%s.SEQ",argv[1]);
  f=fopen(s,"wb");if(!f)abort();
  fwrite(buf+seqbegin,pacl-seqbegin,1,f);
  fclose(f);

  sprintf(s,"%s.VH",argv[1]);
  f=fopen(s,"wb");if(!f)abort();
  fwrite(buf+vhbegin,vbbegin-vhbegin,1,f);
  fclose(f);

  sprintf(s,"%s.VB",argv[1]);
  f=fopen(s,"wb");if(!f)abort();
  fwrite(buf+vbbegin,seqbegin-vbbegin,1,f);
  fclose(f);

  return 0;
}
