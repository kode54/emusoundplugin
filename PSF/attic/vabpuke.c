#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define uint32 unsigned int
#define sint32 signed int
#define sint16 signed short
#define uint16 unsigned short
#define sint8 signed char
#define uint8 unsigned char

/***************************************************************************/
/*
** State information
*/
#define STATE ((struct SPU_STATE*)(state))

#define SAMPLE_STATE_OFF    (0)
#define SAMPLE_STATE_ENDING (1)
#define SAMPLE_STATE_ON     (2)

struct SPU_SAMPLE {
  int    state;
  sint32 array[32];
  uint32 phase;
  uint32 phase_limit;
  uint32 block_addr;
  uint32 start_block_addr;
  uint32 loop_block_addr;
};

#define ENVELOPE_STATE_OFF     (0)
#define ENVELOPE_STATE_ATTACK  (1)
#define ENVELOPE_STATE_DECAY   (2)
#define ENVELOPE_STATE_SUSTAIN (3)
#define ENVELOPE_STATE_RELEASE (4)

struct SPU_ENVELOPE {
  uint32 reg_ad;
  uint32 reg_sr;
  uint32 reg_ad_x;
  uint32 reg_sr_x;
  sint32 level;
  sint32 delta;
  int    state;
};

struct SPU_CHAN {
  sint32 voice_vol[2];
  sint32 voice_start_vol[2];
  uint32 voice_pitch;
  struct SPU_SAMPLE   sample;
  struct SPU_ENVELOPE env;
};

#define SPU_REG_MAIN_VOL_L   (((0x1D80)/2)&0x3F)
#define SPU_REG_MAIN_VOL_R   (((0x1D82)/2)&0x3F)
#define SPU_REG_REVERB_VOL_L (((0x1D84)/2)&0x3F)
#define SPU_REG_REVERB_VOL_R (((0x1D86)/2)&0x3F)
#define SPU_REG_VOICE_ON_0   (((0x1D88)/2)&0x3F)
#define SPU_REG_VOICE_ON_16  (((0x1D8A)/2)&0x3F)
#define SPU_REG_VOICE_OFF_0  (((0x1D8C)/2)&0x3F)
#define SPU_REG_VOICE_OFF_16 (((0x1D8E)/2)&0x3F)

#define SPU_REG_ADDR         (((0x1DA6)/2)&0x3F)
#define SPU_REG_DATA         (((0x1DA8)/2)&0x3F)
#define SPU_REG_CTRL         (((0x1DAA)/2)&0x3F)
#define SPU_REG_STAT         (((0x1DAE)/2)&0x3F)

struct SPU_STATE {
  uint32 ram[0x80000/4];
  uint16 regs[0x40];
  struct SPU_CHAN chan[24];
  uint32 transfer_address;
};

int spu_get_state_size(void) {
  return(sizeof(struct SPU_STATE));
}

/*
** We can just initialize everything to zero
*/
void spu_clear_state(void *state) {
  memset(state, 0, sizeof(struct SPU_STATE));
}

signed short suck[28];

/***************************************************************************/
/*
** ADPCM sample decoding
*/

#define SPU_PREDICT_SKEL(prednum,coef1,coef2) \
void spu_predict_##prednum(uint8 *src, sint32 *dest, uint32 destpos, uint32 shift) {      \
  uint32 i;                                                                               \
  sint32 p_a = dest[(destpos-2)&31];                                                      \
  sint32 p_b = dest[(destpos-1)&31];                                                      \
  shift += 16;                                                                            \
  for(i = 0; i < 14; i++) {                                                               \
    sint32 a = *src++; sint32 b = (a & 0xF0) << 24; a = a << 28; b >>= shift; a >>= shift; \
    a += ( ((coef1)*p_b) + ((coef2)*p_a) )/64;                                         \
    if(a > 32767) { a = 32767; } if(a < (-32768)) { a = (-32768); }                       \
 suck[2*i+0]=a;\
    dest[destpos++] = a;                                                                  \
    b += ( ((coef1)* a ) + ((coef2)*p_b) )/64;                                         \
    if(b > 32767) { b = 32767; } if(b < (-32768)) { b = (-32768); }                       \
 suck[2*i+1]=b;\
    dest[destpos++] = b;                                                                  \
    destpos &= 31; p_a = a; p_b = b;                                                      \
  }                                                                                       \
}

SPU_PREDICT_SKEL(0,0,0)
SPU_PREDICT_SKEL(1,60,0)
SPU_PREDICT_SKEL(2,115,-52)
SPU_PREDICT_SKEL(3,98,-55)
SPU_PREDICT_SKEL(4,122,-60)

typedef void (*spu_predict_callback_t)(uint8*, sint32*, uint32, uint32);

static spu_predict_callback_t spu_predict[8] = {
  spu_predict_0, spu_predict_1, spu_predict_2, spu_predict_3,
  spu_predict_4, spu_predict_1, spu_predict_2, spu_predict_3
};

static void decode_sample_block(
  uint8 *ram,
  struct SPU_SAMPLE *sample
) {
//  uint32 i;
  uint32 p;
  p = ((sample->phase_limit >> 12) + 2) & 31;
//  if(sample->state != SAMPLE_STATE_ON) {
//    for(i = 0; i < 28; i++) { sample->array[p]=0; p=(p+1)&31; }
//    sample->state = SAMPLE_STATE_OFF;
//  } else {
    ram += (sample->block_addr)&0x7FFF8;
    /* decode */
    (spu_predict[(ram[0]>>4)&7])(
      ram + 2,
      sample->array,
      p,
      ram[0] & 0xF
    );
    /* set loop start address if necessary */
//    if(ram[1]&4) {
//      sample->loop_block_addr = sample->block_addr;
//    }
    /* sample end? */
//    if(ram[1]&1) {
//      /* loop? */
//      if(ram[1]&2) {
//        sample->block_addr = sample->loop_block_addr;
//      /* no loop, just end */
//      } else {
//        sample->state = SAMPLE_STATE_ENDING;
//      }
//    } else {
      /* advance to next block */
      sample->block_addr += 16;
//    }
//  }
  sample->phase_limit += 0x1C000;
}

static void sample_prime(struct SPU_SAMPLE *sample) {
  sample->state = SAMPLE_STATE_ON;
  sample->array[0] = 0;
  sample->array[1] = 0;
  sample->phase       = 0x02000;
  sample->phase_limit = 0x00000;
  sample->block_addr = sample->start_block_addr;
  sample->loop_block_addr = 0;
}

/***************************************************************************/

struct SPU_STATE i_hate_computers;



int main(int argc, char **argv) {
int i,a;
FILE*f;
int fl;

char infilename[1000];
char outfilename[1000];

for(a=1;a<argc;a++){
char *t;
  strcpy(infilename,argv[a]);
  strcpy(outfilename,argv[a]);
t=strchr(outfilename,'.');
if(t) *t=0;
strcat(outfilename,".raw");

fprintf(stdout,"%s->%s\n",infilename,outfilename);fflush(stdout);

spu_clear_state(&i_hate_computers);

memset(&(i_hate_computers.ram), 0, 0x80000);

f=fopen(infilename,"rb");if(!f){perror(infilename);return 1;}
fl=fread(i_hate_computers.ram, 1, 0x80000, f);
fclose(f);

f=fopen(outfilename,"wb");if(!f){perror(outfilename);return 1;}


i_hate_computers.chan[0].sample.start_block_addr=0;

sample_prime(&(i_hate_computers.chan[0].sample));

for(i=0;i<0x8000;i++){
decode_sample_block(
(uint8*)(i_hate_computers.ram),
&(i_hate_computers.chan[0].sample)
);

fwrite(suck,1,28*2,f);

}

fclose(f);

}

  return 0;
}


/***************************************************************************/
