/***************************************************************************/
/*
** dis.c - Disassembler
*/
/***************************************************************************/

#include "main.h"

/***************************************************************************/

#include "dis.h"

/***************************************************************************/

#define INS_S (uint32)((instruction>>21)&0x1F)
#define INS_T (uint32)((instruction>>16)&0x1F)
#define INS_D (uint32)((instruction>>11)&0x1F)
#define INS_H (uint32)((instruction>>6)&0x1F)
#define INS_I (uint32)(instruction&0xFFFF)

#define SIGNED16(x) ((sint32)(((sint16)(x))))

#define REL_I (pc+4+(((sint32)(SIGNED16(INS_I)))<<2))
#define ABS_I ((pc&0xF0000000)|((instruction<<2)&0x0FFFFFFC))

void disassemble(
  uint32 pc,
  uint32 instruction
) {
  printf("0x%08X: 0x%08X   ", pc, instruction);
  if(instruction < 0x04000000) {
    switch(instruction & 0x3F) {
    case 0x00: printf("sll   $%d, $%d, %d" , INS_D, INS_T, INS_H); break;
    case 0x02: printf("srl   $%d, $%d, %d" , INS_D, INS_T, INS_H); break;
    case 0x03: printf("sra   $%d, $%d, %d" , INS_D, INS_T, INS_H); break;
    case 0x04: printf("sllv  $%d, $%d, $%d", INS_D, INS_T, INS_S); break;
    case 0x06: printf("srlv  $%d, $%d, $%d", INS_D, INS_T, INS_S); break;
    case 0x07: printf("srav  $%d, $%d, $%d", INS_D, INS_T, INS_S); break;
    case 0x08: printf("jr    $%d", INS_S); break;
    case 0x09: printf("jalr  $%d, $%d", INS_D, INS_S); break;
    case 0x0C: printf("syscall"); break;
    case 0x10: printf("mfhi  $%d", INS_D); break;
    case 0x11: printf("mthi  $%d", INS_S); break;
    case 0x12: printf("mflo  $%d", INS_D); break;
    case 0x13: printf("mtlo  $%d", INS_S); break;
    case 0x18: printf("mult  $%d, $%d", INS_S, INS_T); break;
    case 0x19: printf("multu $%d, $%d", INS_S, INS_T); break;
    case 0x1A: printf("div   $%d, $%d", INS_S, INS_T); break;
    case 0x1B: printf("divu  $%d, $%d", INS_S, INS_T); break;
    case 0x20: printf("add   $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    case 0x21: printf("addu  $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    case 0x22: printf("sub   $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    case 0x23: printf("subu  $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    case 0x24: printf("and   $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    case 0x25: printf("or    $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    case 0x26: printf("xor   $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    case 0x27: printf("nor   $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    case 0x2A: printf("slt   $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    case 0x2B: printf("sltu  $%d, $%d, $%d", INS_D, INS_S, INS_T); break;
    default: printf("unknown minor 0x%02X", instruction & 0x3F);
    }
  } else {
    switch(instruction >> 26) {
    case 0x01:
      switch(INS_T) {
      case 0x00: printf("bltz  $%d, 0x%08X", INS_S, REL_I); break;
      case 0x01: printf("bgez  $%d, 0x%08X", INS_S, REL_I); break;
      case 0x10: printf("bltzal$%d, 0x%08X", INS_S, REL_I); break;
      case 0x11: printf("bgezal$%d, 0x%08X", INS_S, REL_I); break;
      default: printf("unknown major 0x%02X T=0x%02X", instruction>>26, INS_T); break;
      }
      break;
    case 0x02: printf("j     0x%08X", ABS_I); break;
    case 0x03: printf("jal   0x%08X", ABS_I); break;
    case 0x04: printf("beq   $%d, $%d, 0x%08X", INS_S, INS_T, REL_I); break;
    case 0x05: printf("bne   $%d, $%d, 0x%08X", INS_S, INS_T, REL_I); break;
    case 0x06: printf("blez  $%d, 0x%08X", INS_S, REL_I); break;
    case 0x07: printf("bgtz  $%d, 0x%08X", INS_S, REL_I); break;
    case 0x08: printf("addi  $%d, $%d, 0x%04X", INS_T, INS_S, INS_I); break;
    case 0x09: printf("addiu $%d, $%d, 0x%04X", INS_T, INS_S, INS_I); break;
    case 0x0A: printf("slti  $%d, $%d, 0x%04X", INS_T, INS_S, INS_I); break;
    case 0x0B: printf("sltiu $%d, $%d, 0x%04X", INS_T, INS_S, INS_I); break;
    case 0x0C: printf("andi  $%d, $%d, 0x%04X", INS_T, INS_S, INS_I); break;
    case 0x0D: printf("ori   $%d, $%d, 0x%04X", INS_T, INS_S, INS_I); break;
    case 0x0E: printf("xori  $%d, $%d, 0x%04X", INS_T, INS_S, INS_I); break;
    case 0x0F: printf("lui   $%d, 0x%04X", INS_T, INS_I); break;
    case 0x10:
      switch(INS_S) {
      case 0x00: printf("mfc0  $%d, C0_%d", INS_T, INS_D); break;
      case 0x04: printf("mtc0  $%d, C0_%d", INS_T, INS_D); break;
      case 0x10: printf("rfe"); break;
      default: printf("unknown major 0x%02X S=0x%02X", instruction>>26, INS_S); break;
      }
      break;
    case 0x20: printf("lb    $%d, 0x%04X($%d)", INS_T, INS_I, INS_S); break;
    case 0x21: printf("lh    $%d, 0x%04X($%d)", INS_T, INS_I, INS_S); break;
    case 0x23: printf("lw    $%d, 0x%04X($%d)", INS_T, INS_I, INS_S); break;
    case 0x24: printf("lbu   $%d, 0x%04X($%d)", INS_T, INS_I, INS_S); break;
    case 0x25: printf("lhu   $%d, 0x%04X($%d)", INS_T, INS_I, INS_S); break;
    case 0x28: printf("sb    $%d, 0x%04X($%d)", INS_T, INS_I, INS_S); break;
    case 0x29: printf("sh    $%d, 0x%04X($%d)", INS_T, INS_I, INS_S); break;
    case 0x2B: printf("sw    $%d, 0x%04X($%d)", INS_T, INS_I, INS_S); break;
    default: printf("unknown major 0x%02X", instruction >> 26);
    }
  }
  printf("\n");
}

/***************************************************************************/
