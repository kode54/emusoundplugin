#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int freq[0x10000]={0};

int getshort(FILE *f) {
  unsigned a = fgetc(f) & 0xFF;
  unsigned b = fgetc(f) & 0xFF;
  signed short s = (b << 8) | a;
  return ((signed int)((signed short)(s)));
}
void putshort(int i, FILE *g) { fputc(i,g); fputc(i>>8,g); }

int main(void) {
  int i;
  FILE *f, *g;
  f = fopen("release.wav", "rb");
  g = fopen("analyze.raw", "wb");

  fseek(f,0x2C,SEEK_SET);
  while(!feof(f)) {
    i=getshort(f);
    i+=0x8000;
    freq[i]++;
  }
  for(i=0;i<0x10000;i++)putshort(freq[i],g);

  fclose(f);
  fclose(g);

  return 0;
}
