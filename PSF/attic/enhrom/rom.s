  .set noreorder
  .set nomacro
  .set noat
rom:
  .long 0x1F000000+start-rom
  .ascii "Licensed by Sony Computer Entertainment Inc."
  .long 0,0,0,0
  .long 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  .long 0,0,0,0,0,0,0,0,0,0,0,0,0

start:
  lui $8, 0x1F00
  sw $0, 0($8)
  lw $15, 0x810($8)
  lw $29, 0x830($8)
  lw $4, 0x818($8)
  lw $3, 0x81C($8)
  addiu $3, 0x7FF
  srl $3, 11
copywords:
  beqz $3, finish
  addiu $6, $8, 0x0800
  sw $9, 0($8)
  addiu $7, $8, 0x1000
copywords_loop:
  lw $2, 0($6)
  addiu $6, 4
  sw $2, 0($4)
  bne $6, $7, copywords_loop
  addiu $4, 4
  beq $0, $0, copywords
  addiu $3, -1
finish:
  jr $15
  nop
