/***************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "enh.h"

/***************************************************************************/

static uint32 enh_rom[0x800/4] = {
#include "enhrom.h"
};

/***************************************************************************/

static uint32  enh_bank = 0;
static uint32 *enh_data = NULL;
static uint32  enh_size = 0;

/***************************************************************************/

void enh_set_dataarea(uint32 *data, uint32 size) {
  enh_data = data;
  enh_size = size >> 2;
}

/***************************************************************************/

uint32 enh_lw(uint32 a) {
  a = (a >> 2) & 0x3FF;
  if(a & 0x200) {
    uint32 p = 0x200 * enh_bank + (a & 0x1FF);
    if(p >= enh_size) p = 0;
    if(!enh_data) return 0;
    return enh_data[p];
  } else {
    return enh_rom[a & 0x1FF];
  }
}

/***************************************************************************/

void enh_sw(uint32 a, uint32 d) {
  enh_bank = d;
}

/***************************************************************************/
