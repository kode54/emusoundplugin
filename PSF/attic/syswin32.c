/***************************************************************************/
/*
** System-specific routines (Win32/OpenGL)
*/
/***************************************************************************/

#include <windows.h>
#include <mmsystem.h>
#include <gl\gl.h>
#include <gl\glu.h>
//#include <gl\glaux.h>

#include <stdio.h>
#include <stdlib.h>

/***************************************************************************/

#include "sys.h"

#include "ddr.h"
#include "mixer.h"

/***************************************************************************/

unsigned long long previous_performance_timer = 0;
unsigned long long current_performance_timer = 0;
unsigned long long performance_frequency = 0;

unsigned long long llvalueof(LARGE_INTEGER *li) {
  return li->QuadPart;
}

float sys_performance_timer(void) {
  LARGE_INTEGER li;
  double d;
  li.QuadPart=0;
  QueryPerformanceCounter(&li);
  current_performance_timer = llvalueof(&li);
  d = current_performance_timer - previous_performance_timer;
  d /= ((double)(performance_frequency));
  previous_performance_timer = current_performance_timer;
  return d;
}

void sys_performance_timer_init(void) {
  LARGE_INTEGER li;
  li.QuadPart=0;
  QueryPerformanceFrequency(&li);
  performance_frequency = llvalueof(&li);
  if(!performance_frequency) performance_frequency = 1;
  sys_performance_timer();
}

/***************************************************************************/

unsigned sys_randseed = 0;

int sys_raw_rand(void) {
  sys_randseed=sys_randseed*1103515245+12345;
  return (sys_randseed>>16)&0x7FFF;
}

int sys_rand(int a, int b) {
  if(b<a){int t=a;a=b;b=t;}
  b++; b-=a;
  return a+(sys_raw_rand()%b);
}

/***************************************************************************/
/*
** Emulate paletted textures for lamers with ATI cards
*/
int ati_lamer_mode_default = 0;
int ati_lamer_mode = 0;

int texture_resampling_enabled = 1;

/***************************************************************************/
/*
** Error reporting system
*/
static char sys_nullerrorstring[1] = {0};
static char *sys_errorstring = NULL;

char *sys_strerror(void) {
  if(!sys_errorstring) return sys_nullerrorstring;
  return sys_errorstring;
}

/***************************************************************************/
/*
** Graphical stuff
*/

#define GL_UNSIGNED_SHORT_4_4_4_4_EXT (0x8033)
#define GL_COLOR_TABLE_EXT            (0x80D0)
#define COLOR_INDEX4_EXT              (0x80E4)
#define COLOR_INDEX8_EXT              (0x80E5)

#ifndef PFD_SWAP_LAYER_BUFFERS
#define PFD_SWAP_LAYER_BUFFERS  (0x00000800)
#endif
#ifndef PFD_GENERIC_ACCELERATED
#define PFD_GENERIC_ACCELERATED (0x00001000)
#endif

PFNGLCOLORTABLEEXTPROC glColorTableEXT = NULL;

HGLRC sys_hrc  = NULL; // Permanent Rendering Context
HDC   sys_hdc  = NULL; // Private GDI Device Context
HWND  sys_hwnd = NULL; // Holds Our Window Handle
int sys_is2dmode = 0;
int sys_is3dmode = 0;
int sys_screenwidth;
int sys_screenheight;
int sys_active = 0;
int sys_fullscreen;

/***************************************************************************/
/*
** Utility functions
*/

void *sys_malloc(unsigned size) { return malloc(size); }
void sys_free(void *ptr) { free(ptr); }

//int sys_rand(int a, int b) { return (rand()%((b-a)+1))+a; }

struct SYS_GFX_TEXTURE {
  struct SYS_GFX_TEXTURE *next;
  unsigned gltexnum;
  unsigned w;
  unsigned h;
  unsigned type;
  unsigned palettebank;
  unsigned palette_reload;
  unsigned *ati_lamer_palette;
  unsigned char *ati_lamer_indexdata;
  unsigned ati_lamer_valid;
};

/***************************************************************************/

struct SYS_GFX_TEXTURE *master_texture = NULL;

/***************************************************************************/

unsigned global_palette[1024];

/***************************************************************************/

void sys_gfx_upload_palette(unsigned start, unsigned *pal, unsigned n) {
  unsigned bs4, be4;
  unsigned bs8, be8;
  if(start >= 1024) return;
  if((start + n) > 1024) n = 1024 - start;
  if(!n) return;
  memcpy(global_palette + start, pal, sizeof(unsigned) * n);
  bs4 = (start        ) / 16;
  be4 = (start + n - 1) / 16;
  bs8 = (start        ) / 256;
  be8 = (start + n - 1) / 256;
  { struct SYS_GFX_TEXTURE *t;
    for(t = master_texture; t; t = t->next) {
      switch((t->type) & SYS_GFX_PFMASK) {
      case SYS_GFX_INDEX4: if(((t->palettebank) >= bs4) && ((t->palettebank) <= be4)) t->palette_reload = 1; break;
      case SYS_GFX_INDEX8: if(((t->palettebank) >= bs8) && ((t->palettebank) <= be8)) t->palette_reload = 1; break;
      }
    }
  }
}

/***************************************************************************/

static void ati_lamer_redopalette(
  struct SYS_GFX_TEXTURE *t
) {
  unsigned *buf, size, i;
  switch((t->type) & SYS_GFX_PFMASK) {
  case SYS_GFX_INDEX4:
  case SYS_GFX_INDEX8: break;
  default: return;
  }
  size=t->w*t->h;
  buf=sys_malloc(size*4);
  if(!buf) return;
  glBindTexture(GL_TEXTURE_2D, t->gltexnum);
  for(i=0;i<size;i++){
    buf[i]=t->ati_lamer_palette[t->ati_lamer_indexdata[i]];
  }
  glTexImage2D(
    GL_TEXTURE_2D,
    0,
    GL_RGBA8,
    t->w,
    t->h,
    0,
    GL_RGBA,
    GL_UNSIGNED_BYTE,
    buf
  );
  sys_free(buf);
  t->ati_lamer_valid = 1;
}

static void sys_gfx_upload_texture_palette(void *t, unsigned *pal, unsigned n) {
  if(!t || !pal || !n) return;
  switch((((struct SYS_GFX_TEXTURE*)t)->type) & SYS_GFX_PFMASK) {
  case SYS_GFX_INDEX4:
  case SYS_GFX_INDEX8: break;
  default: return;
  }
  if(ati_lamer_mode) {
    /* selectively invalidate only if the palette changed */
    if(((struct SYS_GFX_TEXTURE*)t)->ati_lamer_valid) {
      unsigned i;
      for(i=0;i<n;i++){
        if(((struct SYS_GFX_TEXTURE*)t)->ati_lamer_palette[i]!=pal[i]){
          ((struct SYS_GFX_TEXTURE*)t)->ati_lamer_valid=0;
          break;
        }
      }
    }
    memcpy(
      (((struct SYS_GFX_TEXTURE*)t)->ati_lamer_palette),
      pal,
      4*n
    );
  } else {
    glBindTexture(GL_TEXTURE_2D, ((struct SYS_GFX_TEXTURE*)t)->gltexnum);
    glColorTableEXT(
      GL_TEXTURE_2D,
      GL_RGBA,
      n,
      GL_RGBA,
      GL_UNSIGNED_BYTE,
      pal
    );
  }
}

void *sys_gfx_alloc_texture(
  unsigned w,
  unsigned h,
  unsigned type
) {
  unsigned internalformat;
  unsigned externalformat;
  GLuint n[1];
  struct SYS_GFX_TEXTURE *t;

  t = sys_malloc(sizeof(struct SYS_GFX_TEXTURE));
  if(!t) return NULL;

  /* opengl doesn't support u/v flip modes; must emulate */
  if(type & SYS_GFX_UFLIP) w *= 2;
  if(type & SYS_GFX_VFLIP) h *= 2;

  glGenTextures(1, n);
  glBindTexture(GL_TEXTURE_2D, n[0]);
  if(!glIsTexture(n[0])) {
    sys_free(t);
    return NULL;
  }
  t->gltexnum = n[0];

  t->next = master_texture;
  master_texture = t;

  t->ati_lamer_indexdata = NULL;
  switch(type & SYS_GFX_PFMASK) {
  case SYS_GFX_INDEX4:
    internalformat = ati_lamer_mode ? GL_RGBA8 : COLOR_INDEX4_EXT;
    externalformat = GL_COLOR_INDEX;
    if(ati_lamer_mode) {
      if(!(t->ati_lamer_indexdata = sys_malloc(w*h))) return NULL; /* unclean exit */
      if(!(t->ati_lamer_palette = sys_malloc(4 * 256))) return NULL; /* unclean exit */
    }
    break;
  case SYS_GFX_INDEX8:
    internalformat = ati_lamer_mode ? GL_RGBA8 : COLOR_INDEX8_EXT;
    externalformat = GL_COLOR_INDEX;
    if(ati_lamer_mode) {
      if(!(t->ati_lamer_indexdata = sys_malloc(w*h))) return NULL; /* unclean exit */
      if(!(t->ati_lamer_palette = sys_malloc(4 * 256))) return NULL; /* unclean exit */
    }
    break;
  case SYS_GFX_ARGB1555:
    internalformat = GL_RGBA8;
    externalformat = GL_RGBA;
    break;
  default:
    return NULL; /* unclean idiot-check exit */
  }
  glTexImage2D(
    GL_TEXTURE_2D,
    0,
    internalformat,
    w,
    h,
    0,
    externalformat,
    GL_UNSIGNED_BYTE,
    NULL
  );
  t->type = type;
  t->w = w;
  t->h = h;
  t->ati_lamer_valid = 0;
  t->palettebank = 0;
  t->palette_reload = 1;
  return t;
}

void sys_gfx_set_texture_palette(void *t, unsigned bank) {
  if(!t) return;
  switch((((struct SYS_GFX_TEXTURE*)t)->type) & SYS_GFX_PFMASK) {
  case SYS_GFX_INDEX4: bank &= 63; break;
  case SYS_GFX_INDEX8: bank &=  3; break;
  }
  if(bank != ((struct SYS_GFX_TEXTURE*)t)->palettebank) {
    ((struct SYS_GFX_TEXTURE*)t)->palettebank = bank;
    ((struct SYS_GFX_TEXTURE*)t)->palette_reload = 1;
  }
}

void sys_gfx_free_texture(void *t) {
  GLuint n[1];
  if(!t) return;
  n[0] = ((struct SYS_GFX_TEXTURE*)t)->gltexnum;
  glDeleteTextures(1, n);
  if(((struct SYS_GFX_TEXTURE*)t)->ati_lamer_indexdata) {
    sys_free(((struct SYS_GFX_TEXTURE*)t)->ati_lamer_indexdata);
  }
  if(((struct SYS_GFX_TEXTURE*)t)->ati_lamer_palette) {
    sys_free(((struct SYS_GFX_TEXTURE*)t)->ati_lamer_palette);
  }
  if(t == master_texture) {
    master_texture = ((struct SYS_GFX_TEXTURE*)t)->next;
  } else {
    struct SYS_GFX_TEXTURE *temp = master_texture;
    while(temp && (temp->next != t)) temp = temp->next;
    if(temp) temp->next = ((struct SYS_GFX_TEXTURE*)t)->next;
  }
  sys_free(t);
}

void sys_gfx_upload_texture(
  void *t, unsigned x, unsigned y,
  void *data, unsigned trace, unsigned w, unsigned h
) {
  unsigned char *buffer = NULL;
  unsigned j;
  unsigned ati_index_defer = 0;
  unsigned buftypesize = 1;
  unsigned externalformat = GL_RGBA;
  if(!t) return;
  if(!data) return;
  switch(((struct SYS_GFX_TEXTURE*)t)->type & SYS_GFX_PFMASK) {
  case SYS_GFX_INDEX4  : externalformat = GL_COLOR_INDEX; ati_index_defer = ati_lamer_mode; buftypesize = 1; w &= ~1; trace /= 2; break;
  case SYS_GFX_INDEX8  : externalformat = GL_COLOR_INDEX; ati_index_defer = ati_lamer_mode; buftypesize = 1; break;
  case SYS_GFX_ARGB1555: externalformat = GL_RGBA       ; ati_index_defer = 0;              buftypesize = 4; trace *= 2; break;
  }
  buffer = sys_malloc(w * buftypesize);
  if(!buffer) return;
  if(!ati_index_defer) {
    glBindTexture(GL_TEXTURE_2D, ((struct SYS_GFX_TEXTURE*)t)->gltexnum);
  }
  while(h--) {
    unsigned char *d = data;
    switch(((struct SYS_GFX_TEXTURE*)t)->type & SYS_GFX_PFMASK) {
    case SYS_GFX_INDEX4:
      for(j = 0; j < w; j += 2) {
        buffer[j    ] = *d >> 4;
        buffer[j + 1] = *d & 0xF;
        d++;
      }
      break;
    case SYS_GFX_INDEX8:
      memcpy(buffer, d, w);
      break;
    case SYS_GFX_ARGB1555:
      for(j = 0; j < w; j++) {
        unsigned short s = *((unsigned short*)(d));
        buffer[4 * j + 3] = ((s >> 15) & 0x01)?0xFF:0;
        buffer[4 * j + 2] = (((s >> 10) & 0x1F)*255)/31;
        buffer[4 * j + 1] = (((s >>  5) & 0x1F)*255)/31;
        buffer[4 * j + 0] = (((s >>  0) & 0x1F)*255)/31;
        d += 2;
      }
      break;
    }
    if(ati_index_defer) {
      memcpy(
        (((struct SYS_GFX_TEXTURE*)t)->ati_lamer_indexdata)+
         ((((struct SYS_GFX_TEXTURE*)t)->w)*y),
        buffer, w
      );
    } else {
      glTexSubImage2D(
        GL_TEXTURE_2D,
        0, x, y, w, 1,
        externalformat,
        GL_UNSIGNED_BYTE,
        buffer
      );
      if((((struct SYS_GFX_TEXTURE*)t)->type) & SYS_GFX_VFLIP) {
        glTexSubImage2D(
          GL_TEXTURE_2D,
          0, x, ((((struct SYS_GFX_TEXTURE*)t)->h) - 1) - y, w, 1,
          externalformat,
          GL_UNSIGNED_BYTE,
          buffer
        );
      }
      if((((struct SYS_GFX_TEXTURE*)t)->type) & SYS_GFX_UFLIP) {
        int rx = ((((struct SYS_GFX_TEXTURE*)t)->w) - x) - w;
        int q0;
        unsigned char temp[4];
        for(q0 = 0; q0 < (w / 2); q0++) {
          int q1 = (w - 1) - q0;
          memcpy(temp, buffer + buftypesize * q0, buftypesize);
          memcpy(buffer + buftypesize * q0, buffer + buftypesize * q1, buftypesize);
          memcpy(buffer + buftypesize * q1, temp, buftypesize);
        }
        glTexSubImage2D(
          GL_TEXTURE_2D,
          0, rx, y, w, 1,
          externalformat,
          GL_UNSIGNED_BYTE,
          buffer
        );
        if((((struct SYS_GFX_TEXTURE*)t)->type) & SYS_GFX_VFLIP) {
          glTexSubImage2D(
            GL_TEXTURE_2D,
            0, rx, ((((struct SYS_GFX_TEXTURE*)t)->h) - 1) - y, w, 1,
            externalformat,
            GL_UNSIGNED_BYTE,
            buffer
          );
        }
      }

    }
    data = ((char*)data) + trace;
    y++;
  }
  /* invalidate */
  if(ati_index_defer) ((struct SYS_GFX_TEXTURE*)t)->ati_lamer_valid = 0;
  sys_free(buffer);
}

/***************************************************************************/
/*
** Internal routine to select 2D orthogonal geometry
*/
static void sys_set2d(void) {
  if(!sys_is2dmode) {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(
      0, sys_screenwidth,
      0, sys_screenheight,
      -1, 1
    );
  }
  sys_is2dmode = 1;
  sys_is3dmode = 0;
}

static void sys_set3d(void) {
  if(!sys_is3dmode) {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(
      45.0f,
      ((GLfloat)sys_screenwidth)/((GLfloat)sys_screenheight),
      0.1f,
      1000.0f
    );
  }
  sys_is2dmode = 0;
  sys_is3dmode = 1;
}

static void sys_setflags(unsigned flags) {
  if(flags & SYS_GFX_BLEND) {
    glEnable(GL_BLEND);
    glBlendFunc(
      (flags & SYS_GFX_SRCADD  ) ? GL_ONE : GL_SRC_ALPHA,
      (flags & SYS_GFX_ADDITIVE) ? GL_ONE : GL_ONE_MINUS_SRC_ALPHA
    );
  } else {
    glDisable(GL_BLEND);
  }
  if(flags & SYS_GFX_PUNCH) {
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GEQUAL, 1.0);
  } else {
    glDisable(GL_ALPHA_TEST);
  }
  if(flags & SYS_GFX_GOURAUD) {
    glShadeModel(GL_SMOOTH);
  } else {
    glShadeModel(GL_FLAT);
  }
}

static void sys_settexflags(unsigned flags) {
  int param = (texture_resampling_enabled && (flags & SYS_GFX_LINEAR)) ?
    GL_LINEAR : GL_NEAREST;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, param);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, param);
}

static void sys_beginobj(unsigned objtype) {
  switch(objtype) {
  case SYS_GFX_OBJ_QUADS   : glBegin(GL_QUADS         ); break;
  case SYS_GFX_OBJ_TRIS    : glBegin(GL_TRIANGLES     ); break;
  case SYS_GFX_OBJ_TRISTRIP: glBegin(GL_TRIANGLE_STRIP); break;
  case SYS_GFX_OBJ_TRIFAN  : glBegin(GL_TRIANGLE_FAN  ); break;
  }
}

/***************************************************************************/

static void checkpalette(struct SYS_GFX_TEXTURE *t) {
  if(!t) return;
  if(t->palette_reload) {
    switch((t->type) & SYS_GFX_PFMASK) {
    case SYS_GFX_INDEX4: sys_gfx_upload_texture_palette(t, global_palette +  16 * ((t->palettebank) & 63),  16); break;
    case SYS_GFX_INDEX8: sys_gfx_upload_texture_palette(t, global_palette + 256 * ((t->palettebank) &  3), 256); break;
    }
    t->palette_reload = 0;
  }
}

/***************************************************************************/
/*
** Draw 2D objects (can be textured or not)
*/
void sys_gfx_2d_obj(
  unsigned objtype,
  void *t,
  int *xy,
  int *uv,
  unsigned *c,
  unsigned points,
  unsigned flags
) {
  sys_set2d();
  sys_setflags(flags);
  if(t && uv) {
    checkpalette(t);
    if(ati_lamer_mode) {
      if(!(((struct SYS_GFX_TEXTURE*)t)->ati_lamer_valid)) {
        ati_lamer_redopalette(t);
      }
    }
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, ((struct SYS_GFX_TEXTURE*)t)->gltexnum);
    sys_settexflags(flags);
  } else {
    glDisable(GL_TEXTURE_2D);
    t = uv = NULL;
  }
  sys_beginobj(objtype);
  if(!c) glColor4ub(0xFF, 0xFF, 0xFF, 0xFF);
  while(points--) {
    if(uv) {
      glTexCoord2f(
        ((float)(uv[0])) / ((float)(((struct SYS_GFX_TEXTURE*)t)->w)),
        ((float)(uv[1])) / ((float)(((struct SYS_GFX_TEXTURE*)t)->h))
      );
      uv += 2;
    }
    if(c) {
      glColor4ub(
        (c[0]>> 0)&0xFF,
        (c[0]>> 8)&0xFF,
        (c[0]>>16)&0xFF,
        (c[0]>>24)&0xFF
      );
      if(flags & SYS_GFX_GOURAUD) c++;
    }
    glVertex2i(xy[0], 480 - xy[1]);
    xy += 2;
  }
  glEnd();
}

/***************************************************************************/
/*
** Prepare for 3D mode and reset the modelview matrix
*/
void sys_gfx_3d_reset(void) {
  sys_set3d();
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void sys_gfx_3d_translate(
  float x,
  float y,
  float z
) {
  glMatrixMode(GL_MODELVIEW);
  glTranslatef(x, y, z);
}

void sys_gfx_3d_rotate(
  float degrees,
  int xyzaxis
) {
  glMatrixMode(GL_MODELVIEW);
  glRotatef(degrees,
    xyzaxis==0?1.0:0.0,
    xyzaxis==1?1.0:0.0,
    xyzaxis==2?1.0:0.0
  );
}

void sys_gfx_3d_scale(
  float x,
  float y,
  float z
) {
  glMatrixMode(GL_MODELVIEW);
  glScalef(x, y, z);
}

void sys_gfx_3d_obj(
  unsigned objtype,
  void *t,
  float *xyz,
  float *uv,
  unsigned *c,
  unsigned points,
  unsigned flags
) {
  sys_set3d();
  sys_setflags(flags);
  if(t && uv) {
    checkpalette(t);
    if(ati_lamer_mode) {
      if(!(((struct SYS_GFX_TEXTURE*)t)->ati_lamer_valid)) {
        ati_lamer_redopalette(t);
      }
    }
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, ((struct SYS_GFX_TEXTURE*)t)->gltexnum);
    sys_settexflags(flags);
  } else {
    glDisable(GL_TEXTURE_2D);
    t = uv = NULL;
  }
  sys_beginobj(objtype);
  if(!c) glColor4ub(0xFF, 0xFF, 0xFF, 0xFF);
  while(points--) {
    if(uv) {
      glTexCoord2f(
        ((float)(uv[0])) / ((float)(((struct SYS_GFX_TEXTURE*)t)->w)),
        ((float)(uv[1])) / ((float)(((struct SYS_GFX_TEXTURE*)t)->h))
      );
      uv += 2;
    }
    if(c) {
      glColor4ub(
        (c[0]>> 0)&0xFF,
        (c[0]>> 8)&0xFF,
        (c[0]>>16)&0xFF,
        (c[0]>>24)&0xFF
      );
      c++;
    }
    glVertex3f(xyz[0], xyz[1], xyz[2]);
    xyz += 3;
  }
  glEnd();
}

/***************************************************************************/
/*
** Wave buffer stuff
*/
unsigned wavebufnum=0;

/* WAVEFORMATEX structure for reading in the WAVE fmt chunk */
WAVEFORMATEX WaveFormat = {
  WAVE_FORMAT_PCM,
  2,
  44100,
  44100*4,
  4,
  16,
  0
};

HWAVEOUT HWaveOut;    /* Handle of opened WAVE Out device */
DWORD    WaveBufSize; /* The size of each WAVEHDR's wave buffer */
#define BUFFERS (4)
WAVEHDR  WaveHeader[BUFFERS];

int wavebegun=0;

//int wavepositionstart=-1500;
int wavepositionstart=0;

/***************************************************************************/

static CRITICAL_SECTION WaveCriticalSection;

static void CALLBACK WaveOutProc(
  HWAVEOUT waveOut,
  UINT uMsg,
  DWORD dwInstance,
  DWORD dwParam1,
  DWORD dwParam2
) {
  /* Has a buffer finished playing? if so, refill it */
  if(uMsg == MM_WOM_DONE) {
    WAVEHDR *pwh=(WAVEHDR*)dwParam1;
    waveOutWrite(HWaveOut, pwh, sizeof(WAVEHDR));
    EnterCriticalSection(&WaveCriticalSection);
    mixer_refill(pwh->lpData, (pwh->dwBufferLength)/4);
    LeaveCriticalSection(&WaveCriticalSection);
  }
}

/***************************************************************************/
/*
** Returns nonzero on error
*/
int sys_wav_open(void) {
	int nsamplesperbuffer = 1024;
	DWORD	err;

	wavepositionstart -= 4 * nsamplesperbuffer;

	/* Open the default WAVE Out Device, specifying my callback */
	if ((err = waveOutOpen(
           &HWaveOut,
           WAVE_MAPPER,
           &WaveFormat,
           (DWORD)WaveOutProc, 0, CALLBACK_FUNCTION)
        )) {
		sys_errorstring = "Unable to open wave output device";
		return 1;
	}

	WaveBufSize = 4*nsamplesperbuffer;

  if (!(WaveHeader[0].lpData = (char *)VirtualAlloc(0, WaveBufSize*BUFFERS, MEM_COMMIT, PAGE_READWRITE))){
    sys_errorstring = "Unable to allocate memory for wave buffer";
    goto bad1;
  }

  ZeroMemory(WaveHeader[0].lpData,WaveBufSize*BUFFERS);

	/* Fill in WAVEHDR fields for buffer starting address and size */
	{int i;for(i=0;i<BUFFERS;i++){
	WaveHeader[i].lpData=WaveHeader[0].lpData+i*WaveBufSize;
	WaveHeader[i].dwBufferLength = WaveBufSize;
	WaveHeader[i].dwFlags=0;
	}}
	/* Leave other WAVEHDR fields at 0 */

	{int i;for(i=0;i<BUFFERS;i++){
	if ((err = waveOutPrepareHeader(HWaveOut, &WaveHeader[i], sizeof(WAVEHDR)))) {
		sys_errorstring = "Unable to prepare wave output headers";
		goto bad1;
	}
	}}

InitializeCriticalSection(&WaveCriticalSection);

	/* get things started */
	wavebufnum=0;
	{int i;for(i=0;i<BUFFERS;i++){
	waveOutWrite(HWaveOut, WaveHeader+i, sizeof(WAVEHDR));
	}}

	wavebegun=1;

	return 0;

bad1:
	/* Close WAVE Out device */
	waveOutClose(HWaveOut);
return 1;
}

unsigned wav_getsamples(void){
MMTIME mmt;
if(!wavebegun)return 0;
mmt.wType=TIME_SAMPLES;
waveOutGetPosition(HWaveOut,&mmt,sizeof(mmt));
return mmt.u.sample;
}

void sys_wav_close(void) {
int i;
if(!wavebegun)return;

	/* Close WAVE Out device */
	waveOutClose(HWaveOut);
wavebegun=0;

DeleteCriticalSection(&WaveCriticalSection);

return;
	/* Stop Windows queuing of buffers (and calling of my callback) */
fprintf(stdout,"a\n");fflush(stdout);
//	waveOutReset(HWaveOut);
fprintf(stdout,"b\n");fflush(stdout);
	/* Unprepare WAVE buffers */
	for(i=0;i<BUFFERS;i++){
	waveOutUnprepareHeader(HWaveOut, &WaveHeader[i], sizeof(WAVEHDR));
	}
	/* Free WAVE buffers */
	VirtualFree(WaveHeader[0].lpData, WaveBufSize*BUFFERS, MEM_FREE);
	/* Close WAVE Out device */
	waveOutClose(HWaveOut);
	wavebegun=0;
}

void wav_pause  (void) { if(!wavebegun) return; waveOutPause  (HWaveOut); }
void wav_unpause(void) { if(!wavebegun) return; waveOutRestart(HWaveOut); }

/***************************************************************************/
/*
** Get system time ID (in samples)
** Ties in with the wave stuff above.
*/
unsigned sys_gettimeid(void) {
  return wav_getsamples() + wavepositionstart;
}

/***************************************************************************/
/*
** Choose a pixel format and set it
** Returns 0 on error
*/
static int sys_setpixelformat(
  HDC hdc,
  int *p_bpp,
  int *p_depth,
  int *p_dbl,
  int *p_acc
) {
  int wbpp, wdepth, wdbl, wacc;
  int num, i;
  int max_bpp = 0;
  int max_depth = 0;
  int max_dbl = 0;
  int max_acc = 0;
  unsigned maxqual = 0;
  int maxindex = 0;
  PIXELFORMATDESCRIPTOR pfd;
  if(!p_bpp  ) { wbpp   = -1; } else { wbpp   = *p_bpp;   }
  if(!p_depth) { wdepth = 16; } else { wdepth = *p_depth; }
  if(!p_dbl  ) { wdbl   = -1; } else { wdbl   = *p_dbl;   }
  if(!p_acc  ) { wacc   =  1; } else { wacc   = *p_acc;   }
  ZeroMemory(&pfd, sizeof(pfd));
  pfd.nSize    = sizeof(pfd);
  pfd.nVersion = 1;
  num = DescribePixelFormat(hdc, 1, sizeof(pfd), &pfd);
  if(!num) return 0;
  for(i = 1; i <= num; i++) {
    int bpp, depth, pal, mcd, soft, icd, opengl, window, bitmap, dbuff;
    unsigned q;
    ZeroMemory(&pfd, sizeof(pfd));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    DescribePixelFormat(hdc, i, sizeof(pfd), &pfd);
    bpp    = pfd.cColorBits;
    depth  = pfd.cDepthBits;
    pal    = (pfd.iPixelType==PFD_TYPE_COLORINDEX);
    mcd    = ((pfd.dwFlags & PFD_GENERIC_FORMAT) && (pfd.dwFlags & PFD_GENERIC_ACCELERATED));
    soft   = ((pfd.dwFlags & PFD_GENERIC_FORMAT) && !(pfd.dwFlags & PFD_GENERIC_ACCELERATED));
    icd    = (!(pfd.dwFlags & PFD_GENERIC_FORMAT) && !(pfd.dwFlags & PFD_GENERIC_ACCELERATED));
    opengl = (pfd.dwFlags & PFD_SUPPORT_OPENGL);
    window = (pfd.dwFlags & PFD_DRAW_TO_WINDOW);
    bitmap = (pfd.dwFlags & PFD_DRAW_TO_BITMAP);
    dbuff  = (pfd.dwFlags & PFD_DOUBLEBUFFER);
    q = 0;
    if(opengl && window) q += 0x8000;
    if(wdepth==-1 || (wdepth>0 && depth>0)) q+=0x4000;
    if(wdbl==-1 || (wdbl==0 && !dbuff) || (wdbl==1 && dbuff)) q+=0x2000;
    if(wacc==-1 || (wacc==0 && soft) || (wacc==1 && (mcd || icd))) q+=0x1000;
    if(mcd || icd) q+=0x0040; if (icd) q+=0x0002;
    if(wbpp==-1 || (wbpp==bpp)) q+=0x0800;
    if(bpp>=16) q+=0x0020; if (bpp==16) q+=0x0008;
    if(wdepth==-1 || (wdepth==depth)) q+=0x0400;
    if(depth>=16) q+=0x0010; if (depth==16) q+=0x0004;
    if(!pal     ) q+=0x0080;
    if(bitmap   ) q+=0x0001;
    if(q > maxqual) {
      maxqual   = q;
      maxindex  = i;
      max_bpp   = bpp;
      max_depth = depth;
      max_dbl   = dbuff ? 1 : 0;
      max_acc   = soft ? 0 : 1;
    }
  }
  if(maxindex) {
//fprintf(stdout,"maxindex=%d\n",maxindex);ExitProcess(0);
    if(p_bpp  ) *p_bpp  = max_bpp;
    if(p_depth) *p_depth= max_depth;
    if(p_dbl  ) *p_dbl  = max_dbl;
    if(p_acc  ) *p_acc  = max_acc;
    DescribePixelFormat(hdc, maxindex, sizeof(pfd), &pfd);
    if(!SetPixelFormat(hdc, maxindex, &pfd)) return 0;
  }
  return maxindex;
}

/***************************************************************************/
/*
** Kill the window.
** Since any errors here are probably not recoverable, there's not much
** point to checking for them.
*/
void sys_gfx_close(void) {
  while(master_texture) sys_gfx_free_texture(master_texture);
  if(sys_fullscreen) {
    ChangeDisplaySettings(NULL, 0);
    ShowCursor(TRUE);
  }
  if(sys_hrc) {
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(sys_hrc);
    sys_hrc = NULL;
  }
  if(sys_hdc && sys_hwnd) {
    ReleaseDC(sys_hwnd, sys_hdc);
    sys_hdc = NULL;
  }
  if(sys_hwnd) {
    DestroyWindow(sys_hwnd);
    sys_hwnd = NULL;
  }
  UnregisterClass("OpenGL", GetModuleHandle(NULL));
}

/***************************************************************************/
/*
** Needed prototype for WndProc
*/
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/***************************************************************************/
/*
** Initialize/create window/all that good stuff
** Returns nonzero on error
*/
int sys_gfx_open(
  int width,
  int height,
  int bits,
  int fullscreen
) {
  WNDCLASS wc;     // Windows Class Structure
  DWORD dwExStyle; // Window Extended Style
  DWORD dwStyle;   // Window Style
  RECT WindowRect; // Grabs Rectangle Upper Left / Lower Right Values
  WindowRect.left   = 0;
  WindowRect.right  = width;
  WindowRect.top    = 0;
  WindowRect.bottom = height;

  sys_fullscreen = fullscreen;
  sys_screenwidth  = width;
  sys_screenheight = height;

  // Redraw On Move, And Own DC For Window
  wc.style  = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  wc.lpfnWndProc	= (WNDPROC) WndProc;		// WndProc Handles Messages
  wc.cbClsExtra		= 0;				// No Extra Window Data
  wc.cbWndExtra		= 0;				// No Extra Window Data
  wc.hInstance		= GetModuleHandle(NULL);			// Set The Instance
  wc.hIcon		= LoadIcon(NULL, IDI_WINLOGO);	// Load The Default Icon
  wc.hCursor		= LoadCursor(NULL, IDC_ARROW);	// Load The Arrow Pointer
  wc.hbrBackground	= NULL;				// No Background Required For GL
  wc.lpszMenuName	= NULL;				// We Don't Want A Menu
  wc.lpszClassName	= "OpenGL";			// Set The Class Name

  // Attempt To Register The Window Class
  if(!RegisterClass(&wc)) {
    sys_errorstring = "Failed to register the window class";
    return 1;
  }

  // Attempt Fullscreen Mode?
  if(sys_fullscreen) {
    DEVMODE dmScreenSettings; // Device Mode
    ZeroMemory(&dmScreenSettings, sizeof(dmScreenSettings));
    dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
    dmScreenSettings.dmPelsWidth  = width;	// Selected Screen Width
    dmScreenSettings.dmPelsHeight = height;	// Selected Screen Height
    dmScreenSettings.dmBitsPerPel = bits;	// Selected Bits Per Pixel
    dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;
    // Try To Set Selected Mode And Get Results.
    // NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
    if(
      ChangeDisplaySettings(
        &dmScreenSettings,
        CDS_FULLSCREEN
      ) != DISP_CHANGE_SUCCESSFUL
    ) {
      sys_fullscreen = FALSE;
    }
  }

  // Are We Still In Fullscreen Mode?
  if(sys_fullscreen) {
    dwExStyle = WS_EX_APPWINDOW;
    dwStyle = WS_POPUP;
    ShowCursor(FALSE);
  } else {
    dwExStyle = WS_EX_APPWINDOW;
    dwStyle = WS_OVERLAPPEDWINDOW;
  }

  // Adjust Window To True Requested Size
  AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);

  /* Create window */
  sys_hwnd = CreateWindowEx(
    dwExStyle,				// Extended Style For The Window
    "OpenGL",				// Class Name
    "",					// Window Title
    WS_CLIPSIBLINGS |			// Required Window Style
    WS_CLIPCHILDREN |			// Required Window Style
    dwStyle,				// Selected Window Style
    0, 0,				// Window Position
    WindowRect.right-WindowRect.left,	// Calculate Adjusted Window Width
    WindowRect.bottom-WindowRect.top,	// Calculate Adjusted Window Height
    NULL,				// No Parent Window
    NULL,				// No Menu
    GetModuleHandle(NULL),		// Instance
    NULL				// Don't Pass Anything To WM_CREATE
  );
  if(!sys_hwnd) {
    sys_gfx_close();
    sys_errorstring = "Unable to create window";
    return 1;
  }

  // Did We Get A Device Context?
  sys_hdc = GetDC(sys_hwnd);
  if(!sys_hdc) {
    sys_gfx_close();
    sys_errorstring = "Unable to create GL device context";
    return 1;
  }

  /* Choose a pixel format and set it */
  { int xbpp   = -1; // don't care. (or a positive integer)
    int xdepth = 0; // don't care. (or a positive integer)
    int xdbl   = 1;  // we want double-buffering. (-1=don't care, 0=none)
    int xacc   = 1;  // we want acceleration. (or -1 or 0)
    int pf = sys_setpixelformat(sys_hdc, &xbpp, &xdepth, &xdbl, &xacc);
    if(!pf) {
      sys_gfx_close();
      sys_errorstring = "Unable to set a suitable pixel format";
      return 1;
    }
    fprintf(stdout,"pixel format %08X was selected\n", pf);
  }

  // Get A Rendering Context
  if(!(sys_hrc = wglCreateContext(sys_hdc))) {
    sys_gfx_close();
    sys_errorstring = "Unable to create GL rendering context";
    return 1;
  }

  // Try To Activate The Rendering Context
  if(!wglMakeCurrent(sys_hdc, sys_hrc)) {
    sys_gfx_close();
    sys_errorstring = "Unable to activate GL rendering context";
    return 1;
  }

  // Try to get the glColorTableEXT function
  ati_lamer_mode = ati_lamer_mode_default;
  glColorTableEXT = (PFNGLCOLORTABLEEXTPROC)wglGetProcAddress("glColorTableEXT");
  if(!glColorTableEXT) {
    ati_lamer_mode = 1;
    fprintf(stderr, "**********************************************************************\n");
    fprintf(stderr, "* Paletted texture support does not exist.  Entering ATI lamer mode. *\n");
    fprintf(stderr, "* NOTE: This may result in a severe performance drop.                *\n");
    fprintf(stderr, "**********************************************************************\n");
//    sys_gfx_close();
//    sys_errorstring = "This OpenGL driver does not include the required paletted texture support.";
//    return 1;
  }

  if(ati_lamer_mode) fprintf(stderr, "(ATI lamer mode enabled)\n");

  ShowWindow(sys_hwnd, SW_SHOW); // Show The Window
  SetForegroundWindow(sys_hwnd); // Slightly Higher Priority
  SetFocus(sys_hwnd);            // Sets Keyboard Focus To The Window

  /* Do some OpenGL related inits */
  glViewport(0, 0, width, height);
  glShadeModel(GL_SMOOTH);
  glDisable(GL_COLOR_MATERIAL);
  glDisable(GL_POLYGON_SMOOTH);
  glDisable(GL_CULL_FACE);
  glDisable(GL_LIGHTING);
  glDisable(GL_ALPHA_TEST);
  glDisable(GL_DEPTH_TEST);
  glClearColor(0, 0, 0, 0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  /* Blank screen */
  glClear(GL_COLOR_BUFFER_BIT); SwapBuffers(sys_hdc);
  glClear(GL_COLOR_BUFFER_BIT); SwapBuffers(sys_hdc);
  /* Success */
  return 0;
}

/***************************************************************************/
/*
** Input related stuff
*/
unsigned char sys_keys[256];

/***************************************************************************/
/*
** Window message handler
*/
LRESULT CALLBACK WndProc(
  HWND   hWnd,   // Handle For This Window
  UINT   uMsg,   // Message For This Window
  WPARAM wParam, // Additional Message Information
  LPARAM lParam  // Additional Message Information
) {
  // Check For Windows Messages
  switch(uMsg) {
    // Watch For Window Activate Message
    case WM_ACTIVATE:
      // Check Minimization State
      if(!HIWORD(wParam)) { sys_active = TRUE;  }
      else                { sys_active = FALSE; }
      return 0;
    // Intercept System Commands
    case WM_SYSCOMMAND:
      // Check System Calls
      switch(wParam) {
        case SC_SCREENSAVE:   // Screensaver Trying To Start?
        case SC_MONITORPOWER: // Monitor Trying To Enter Powersave?
          return 0;           // Prevent From Happening
      }
      break;
    // Did We Receive A Close Message?
    case WM_CLOSE:
      PostQuitMessage(0); // Send A Quit Message
      return 0;
    // Is A Key Being Held Down?
    case WM_KEYDOWN:
      if(!sys_keys[wParam & 0xFF]) {
        unsigned tid = sys_gettimeid();
        sys_keys[wParam & 0xFF] = TRUE; // If So, Mark It As TRUE
        if(wParam == VK_LEFT  ) ddr_input_event(tid, INPUT_ON | INPUT_P1LEFT );
        if(wParam == VK_DOWN  ) ddr_input_event(tid, INPUT_ON | INPUT_P1DOWN );
        if(wParam == VK_UP    ) ddr_input_event(tid, INPUT_ON | INPUT_P1UP   );
        if(wParam == VK_RIGHT ) ddr_input_event(tid, INPUT_ON | INPUT_P1RIGHT);
//        if(wParam == VK_F2   ) mixer_pause();
        if(wParam == VK_ESCAPE) PostQuitMessage(0);
      }
      return 0;
    // Has A Key Been Released?
    case WM_KEYUP:
      { unsigned tid = sys_gettimeid();
        sys_keys[wParam & 0xFF] = FALSE; // If So, Mark It As FALSE
        if(wParam == VK_LEFT ) ddr_input_event(tid, INPUT_OFF | INPUT_P1LEFT );
        if(wParam == VK_DOWN ) ddr_input_event(tid, INPUT_OFF | INPUT_P1DOWN );
        if(wParam == VK_UP   ) ddr_input_event(tid, INPUT_OFF | INPUT_P1UP   );
        if(wParam == VK_RIGHT) ddr_input_event(tid, INPUT_OFF | INPUT_P1RIGHT);
//        if(wParam == VK_F2   ) mixer_unpause();
      }
      return 0;
  }
  // Pass All Unhandled Messages To DefWindowProc
  return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

/***************************************************************************/

int WINAPI WinMain(
  HINSTANCE hInstance,     // Instance
  HINSTANCE hPrevInstance, // Previous Instance
  LPSTR     lpCmdLine,     // Command Line Parameters
  int       nCmdShow       // Window Show State
) {
  MSG msg;
  int done = 0;
  unsigned char infilename[200];
  unsigned char diffic[200];
  unsigned char audvols[200];
  int dancediff=0;
  int dancetype=0;

  sys_randseed = GetTickCount();

  sys_performance_timer_init();

  ZeroMemory(sys_keys, sizeof(sys_keys));

  fprintf(stdout, "Neill's l33t DDR engine (Win32/OpenGL system)\n");
  fflush(stdout);

  ddr_preinit();

  if(!lpCmdLine || strlen(lpCmdLine)==0){
    fprintf(stdout,"usage: ddr songfile < basic | trick | maniac >\n");
    return 1;
  }
  infilename[0]=0;
  diffic    [0]=0;
  audvols   [0]=0;
  sscanf(lpCmdLine, "%s %s %s",infilename,diffic,audvols);

  // ms audition mode
  if(!strcmp(infilename,"aud")){
    int audvol=atoi(audvols);
    fprintf(stdout, "ms-audition-mode: %s vol=%d\n",diffic,audvol);
    if(sys_wav_open()) {
      fprintf(stdout, "error: %s\n", sys_strerror());
      return 1;
    }
    Sleep(100);
    ddr_audition_ms(diffic, audvol);
    while(mixer_channel_isactive(3)) Sleep(100);
    sys_wav_close();
    return 0;
  }

  if(!strcasecmp(diffic,"basic")) dancediff=1;
  if(!strcasecmp(diffic,"light")) dancediff=1;
  if(!strcasecmp(diffic,"trick")) dancediff=2;
  if(!strcasecmp(diffic,"another")) dancediff=2;
  if(!strcasecmp(diffic,"standard")) dancediff=2;
  if(!strcasecmp(diffic,"maniac")) dancediff=3;
  if(!strcasecmp(diffic,"ssr")) dancediff=3;
  if(!strcasecmp(diffic,"heavy")) dancediff=3;
  if(!strcasecmp(diffic,"maniac2")) dancediff=4;
  if(!strcasecmp(diffic,"aud")) dancediff=-1;

  if(!strcasecmp(diffic,"music")) dancediff=1;

  if(!dancediff){fprintf(stdout,"Unknown difficulty setting \"%s\"\n",diffic);fflush(stdout);ExitProcess(1);}
  dancetype=1;

  fprintf(stdout,"Dance type/difficulty: %d/%d\n",dancetype,dancediff);fflush(stdout);

  if(sys_wav_open()) {
    fprintf(stdout, "error: %s\n", sys_strerror());
    return 1;
  }
  if(dancediff>=0) {
    if(sys_gfx_open(640, 480, 32, 1)) {
      fprintf(stdout, "error: %s\n", sys_strerror());
      sys_wav_close();
      return 1;
    }
  }

  if(ddr_playsong(infilename, dancetype, dancediff)) done = 1;

  while(!done) {
    if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if(msg.message == WM_QUIT) {
        done = 1;
        break;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    // If There Are No Messages
    } else {
      /* Process a frame */

// glClearColor(0.0,((float)(sys_gettimeid()))/44100.0,0.5,0.0 );
// glClear(GL_COLOR_BUFFER_BIT);

      ddr_frame(sys_gettimeid());
      SwapBuffers(sys_hdc);
    }
  }

  /* Close window/wave output */
  sys_gfx_close();
  sys_wav_close();

  fprintf(stdout,"Exiting.\n");

//{float a,b;mixer_getvar(&a,&b);fprintf(stdout,"btw, minabs=%f;maxabs=%f\n",a,b);}

  // Exit The Program
  return(msg.wParam);
}

/***************************************************************************/
