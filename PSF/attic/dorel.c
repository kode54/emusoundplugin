#include <stdio.h>
#include <stdlib.h>
#include <math.h>



int getshort(FILE *f) {
  unsigned a = fgetc(f) & 0xFF;
  unsigned b = fgetc(f) & 0xFF;
  signed short s = (b << 8) | a;
  return ((signed int)((signed short)(s)));
}
void putshort(int i, FILE *g) { fputc(i,g); fputc(i>>8,g); }

int main(void) {
  int myshort;
  int prevpeak=0;
  FILE *f, *g;
  f = fopen("release.wav", "rb");
  g = fopen("analyze.raw", "wb");

  fseek(f,0x2C,SEEK_SET);
  while(!feof(f)) {
    int i;
    double p;
    for(i=0;i<256;i++){
      int s=getshort(f);
      double d=s;
      p+=d*d;
    }
    p=sqrt(p);

    myshort = p-prevpeak;
    prevpeak=p;

    if(myshort<(-32767))myshort=-32767;
    if(myshort>32767)myshort=32767;

    for(i=0;i<128;i++){
      putshort( myshort,g);
      putshort(-myshort,g);
    }
//    printf("%f\n",delta);
  }

  fclose(f);
  fclose(g);

  return 0;
}
