#ifndef __EMU_DIS_H__
#define __EMU_DIS_H__

#ifdef __cplusplus
extern "C" {
#endif

void disassemble(
  uint32 pc,
  uint32 instruction
);

#ifdef __cplusplus
}
#endif

#endif
