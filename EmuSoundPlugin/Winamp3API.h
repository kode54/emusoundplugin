#ifndef _CNV_EMUSOUND_WINAMP3API_H
#define _CNV_EMUSOUND_WINAMP3API_H

#include "wasabi/studio/studio/wac.h"
#include "wasabi/studio/studio/services/svc_mediaconverter.h"
#include "wasabi/studio/attribs/attrint.h"
#include "wasabi/studio/attribs/attrbool.h"

/*

leet trick.
to avoid having to manually keep central registry of all our services in cnv_wavpcm.cpp to register them all
we use a class that keeps track of all its instances
then just converter_make_service() on whatever converter class we want to add

NOTE
this is supposed to work with static instances only
dont create service_factory_base stuff dynamically

*/

class service_factory_base
{
public:
	virtual waServiceFactoryI * getPtr()=0;
	service_factory_base * next;
	static service_factory_base * list;
	service_factory_base() {next=list;list=this;}
};

template<class foo,class bar>
class service_factory : public service_factory_base
{
private:
	waServiceFactoryT<foo,bar> m_factory;
	virtual waServiceFactoryI * getPtr() {return &m_factory;}
};

template<class foo>
class service_factory_converter : public service_factory<svc_mediaConverter,foo> {};

#define converter_make_service(foo) static service_factory_converter<foo> g_svc_##foo;
#define make_service(foo,bar) static service_factory<foo,bar> g_svc_##foo##_##bar;

#endif
