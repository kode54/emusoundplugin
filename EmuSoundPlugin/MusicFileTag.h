#pragma once

#include "../MFCUtil/File.h"

class CMusicFileTag
{
public:
    CMusicFileTag();
    virtual ~CMusicFileTag();

    virtual BOOL ReadFromFile(File f, BOOL bStrict) = 0;
    virtual BOOL WriteToFile (File f, BOOL bStrict) = 0;

    enum FIELD {
        FIELD_TITLE,
        FIELD_ARTIST,
        FIELD_ALBUM,
        FIELD_YEAR,
        FIELD_GENRE,
        FIELD_COMMENT,
        FIELD_TRACK,
        FIELD_BITRATE
    };

    virtual BOOL GetField(CStringW &value, FIELD f) = 0;
    virtual BOOL SetField(LPCWSTR   value, FIELD f) = 0;

    enum VALUE {
        VALUE_RG_TRACK_VOLUME,
        VALUE_RG_ALBUM_VOLUME,
        VALUE_RG_TRACK_PEAK,
        VALUE_RG_ALBUM_PEAK
    };

    virtual BOOL GetValue(double &value, VALUE v) = 0;
    virtual BOOL SetValue(double  value, VALUE v) = 0;

    virtual const char* GetTagType() = 0;
};
