// ESPConfigErrorPage.cpp : implementation file
//

#include "StdAfx.h"
#include "ESPResource.h"

#include "ESPConfigErrorPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CESPConfigErrorPage property page

IMPLEMENT_DYNCREATE(CESPConfigErrorPage, CPropertyPage)

CESPConfigErrorPage::CESPConfigErrorPage() : CPropertyPage(CESPConfigErrorPage::IDD)
{
  m_pPlugin = NULL;

	//{{AFX_DATA_INIT(CESPConfigErrorPage)
	//}}AFX_DATA_INIT
}

CESPConfigErrorPage::~CESPConfigErrorPage()
{
}

void CESPConfigErrorPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESPConfigErrorPage)
	DDX_Control(pDX, IDC_ESP_STRICT, m_cStrict);
	DDX_Control(pDX, IDC_ESP_SHOWERROR, m_cShowError);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CESPConfigErrorPage, CPropertyPage)
	//{{AFX_MSG_MAP(CESPConfigErrorPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESPConfigErrorPage message handlers

void CESPConfigErrorPage::OnOK() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();

    m_pPlugin->opt_bShowErrorMessages = (m_cShowError.GetCheck() == BST_CHECKED);
    m_pPlugin->opt_bStrictFormatChecking = (m_cStrict.GetCheck() == BST_CHECKED);

    m_pPlugin->UnlockOptions();
  }

	CPropertyPage::OnOK();
}

BOOL CESPConfigErrorPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

  if(m_pPlugin) {
    m_pPlugin->LockOptions();

    m_cShowError.SetCheck(m_pPlugin->opt_bShowErrorMessages ? BST_CHECKED : BST_UNCHECKED);
    m_cStrict.SetCheck(m_pPlugin->opt_bStrictFormatChecking ? BST_CHECKED : BST_UNCHECKED);

    m_pPlugin->UnlockOptions();
  }

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/////////////////////////////////////////////////////////////////////////////

void CESPConfigErrorPage::BindToPlugin(CEmuSoundPlugin *pPlugin)
{
  m_pPlugin = pPlugin;
}

/////////////////////////////////////////////////////////////////////////////
