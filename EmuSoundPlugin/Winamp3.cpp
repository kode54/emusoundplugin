////////////////////////////////////////////////////////////////////////////////
/*
** Winamp3.cpp - Winamp 3.x support
*/
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "Winamp3API.h"

////////////////////////////////////////////////////////////////////////////////

#include "../MFCUtil/NumString.h"
#include "../MFCUtil/MFCFileObject.h"

#include "EmuSoundPlugin.h"

#define WACNAME WACcnv_emusound

// this is here to ensure everything is linked in
int Winamp3Link = 0;

//{A399D6DB-5838-4DD8-9E71-7D73CFF49A2E}
static const GUID guid = 
{ 0xA399D6DB, 0x5838, 0x4DD8, { 0x9E, 0x71, 0x7D, 0x73, 0xCF, 0xF4, 0x9A, 0x2E } };

service_factory_base * service_factory_base::list=0;

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

class WACNAME : public WAComponentClient{
public:
	WACNAME()
	{
#ifdef FORTIFY
		FortifySetName("cnv_emusound.wac");
		FortifyEnterScope();
#endif
	}
	virtual ~WACNAME()
	{
#ifdef FORTIFY
		FortifyLeaveScope();
#endif
	}

	virtual const char *getName() {
        static CStringA name;
        if(name.IsEmpty()) name = CStringA(g_pPlugin->GetBanner());
        return name;
    }

	virtual GUID getGUID() {return guid;}

	virtual void onCreate()
	{
        g_pPlugin->InvokeFrom(ESP_WINAMP3);

		service_factory_base * ptr = service_factory_base::list;
		while(ptr)
		{
			api->service_register(ptr->getPtr());
			ptr=ptr->next;
		}

    LPCWSTR *ex = g_pPlugin->GetSupportedExtensions();

    for(; *ex; ex += 2) {
      CStringW exstar = ex[0];
      CStringW exname = ex[1];
      exstar.MakeLower();
      exstar = L"*." + exstar;
      api->core_registerExtension(CStringA(exstar), CStringA(exname), "Audio");
    }

	}

	virtual void onDestroy()
	{
		service_factory_base * ptr = service_factory_base::list;
		while(ptr)
		{
			api->service_deregister(ptr->getPtr());
			ptr=ptr->next;
		}
	}

	virtual int getDisplayComponent() { return FALSE; };

	virtual CfgItem *getCfgInterface(int n) { return this; }
};

static WACNAME wac;
WAComponentClient *the = &wac;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class CEmuSoundWinamp3Converter : public svc_mediaConverterI
{
public:
	static const char *getServiceName() {
        static CStringA name;
        if(name.IsEmpty()) name = g_pPlugin->GetBanner();
        return name;
    }

    CPlayer *m_pPlayer;
    CCriticalSection m_csPlayer;

    volatile LONG m_lNeedSeekMS;

    short m_an16PersistentSampleBuffer[576 * 2];
    float m_afPersistentSampleBuffer[576 * 2];

    char m_szTitle[MAX_PATH+1];
    char m_szInfo[MAX_PATH+1];

	CEmuSoundWinamp3Converter() {
        m_szTitle[0] = 0;
        m_szInfo[0] = 0;
        m_lNeedSeekMS = -1;
        m_pPlayer = NULL;
    }

	~CEmuSoundWinamp3Converter() {
        if(m_pPlayer) g_pPlugin->Close(m_pPlayer);
    }

	virtual int corecb_onSeeked(int newpos) {
        if(newpos <= 0) newpos = -1;
        ::InterlockedExchange(&m_lNeedSeekMS, newpos);
        return 1;
    }
	virtual const char *getConverterTo() { return "PCM"; }

	virtual int canConvertFrom(svc_fileReader *reader, const char *name, const char *chunktype)
	{
        if (!name || chunktype || 
			!reader || !reader->canSeek()
        ) return 0;

        m_csPlayer.Lock();
        if(m_pPlayer) { g_pPlugin->Close(m_pPlayer); m_pPlayer = NULL; }
        m_csPlayer.Unlock();

        /*
        ** TODO: fail for other reasons
        */
        CStringW strError;
        m_csPlayer.Lock();
        m_pPlayer = g_pPlugin->Open(MFCFileObject(CStringW(name)), strError);
        m_csPlayer.Unlock();
        if(!m_pPlayer) {
            // report?
            return 0;
        }

        return 1;
	}

	virtual int processData(MediaInfo *infos, ChunkList *chunk_list, bool *killswitch)
	{
        DWORD dwSR = 44100;
        DWORD dwCh = 2;
        m_csPlayer.Lock();
        if(m_pPlayer) {
            m_pPlayer->Lock();
            dwSR = m_pPlayer->GetSampleRate();
            dwCh = m_pPlayer->GetChannelCount();
            m_pPlayer->Unlock();
        }
        m_csPlayer.Unlock();
        for(;;) {
            LONG lMS = (LONG)::InterlockedCompareExchange((&m_lNeedSeekMS), 0, 0);
            if(lMS < 0) break;
            INT64 n64Seek = ::MSToSamples(lMS, dwSR);
            INT64 seekposition = -1;
            m_csPlayer.Lock();
            if(m_pPlayer) {
                m_pPlayer->Lock();
                seekposition = m_pPlayer->IncrementalSeek(n64Seek, 5000);
                m_pPlayer->Unlock();
            }
            m_csPlayer.Unlock();
            if(seekposition < 0) {
                /* failed: cancel seek */
                ::InterlockedExchange(&m_lNeedSeekMS, -1);
            } else {
                /* success - if position is the same, end seek */
                if(seekposition == n64Seek) {
                    ::InterlockedExchange(&m_lNeedSeekMS, -1);
                }
            }
		}

        int nsamples = 576;

        BOOL bEOF = TRUE;
        int n = -1;
        m_csPlayer.Lock();
        if(m_pPlayer) {
            m_pPlayer->Lock();
            n = m_pPlayer->Read(m_afPersistentSampleBuffer, nsamples);
            bEOF = (n < nsamples);
            m_pPlayer->Unlock();
        }
        m_csPlayer.Unlock();

        if(n < 0) { n = 0; }
        if(n > 0) {
            BOOL bSoftSaturate = TRUE;
            g_pPlugin->LockOptions();
            bSoftSaturate = g_pPlugin->opt_bSoftSaturate;
            g_pPlugin->UnlockOptions();

            if(bSoftSaturate) CPlayer::SoftSaturate(m_afPersistentSampleBuffer, n*dwCh);

            CPlayer::ConvertFloatTo16bit(m_afPersistentSampleBuffer, m_an16PersistentSampleBuffer, n*dwCh);
        }

		ChunkInfosI *ci=new ChunkInfosI();
		ci->addInfo("srate", dwSR);
		ci->addInfo("bps", 16);
		ci->addInfo("nch", dwCh);
		chunk_list->setChunk("PCM",(char*)m_an16PersistentSampleBuffer,n*2*dwCh,ci);

		return !bEOF;
	}

    virtual int getPosition(void) {
        LONG lMS = (LONG)::InterlockedCompareExchange((&m_lNeedSeekMS), 0, 0);
        if(lMS >= 0) {
            INT64 n64Tell = 0;
            DWORD dwSR = 44100;
            m_csPlayer.Lock();
            if(m_pPlayer) {
                m_pPlayer->Lock();
                n64Tell = m_pPlayer->Tell();
                dwSR = m_pPlayer->GetSampleRate();
                m_pPlayer->Unlock();
            }
            m_csPlayer.Unlock();
            return SamplesToMS(n64Tell, dwSR);
        }
        return -1;
    }

	virtual int getInfos(MediaInfo *infos)
	{
        File f;
        m_csPlayer.Lock();
        if(m_pPlayer) {
            m_pPlayer->Lock();
            f = m_pPlayer->GetFile();
            m_pPlayer->Unlock();
        }
        m_csPlayer.Unlock();

        INT64 nLengthMS = 0;

        CStringW strTitleW;
        CMusicFileTag *tag = g_pPlugin->GetMetaData(f, strTitleW, nLengthMS);
        if(tag) delete tag;
        if(nLengthMS > 0x7FFFFFFF) nLengthMS = 0x7FFFFFFF;
        CStringA strTitleA = strTitleW;

        strncpy(m_szTitle, strTitleA, sizeof(m_szTitle)-1);
        m_szTitle[sizeof(m_szTitle)-1] = 0;

        CStringA apath = f.GetPath();
        CStringA e = Std::extension(apath);

        LPCSTR lpszInfoType = e;
        CStringA strInfo = lpszInfoType;

        int l = 0;
        if(f.Open(File::READ)) {
            l = f.Length();
            f.Close();
        }
        if(l) {
            strInfo += " (";
            strInfo += Util::UINT64ToString(l, 1, Util::NSFLAG_COMMAS);
            strInfo += " byte";
            if(l != 1) strInfo += "s";
            strInfo += ")";
        }

        strncpy(m_szInfo, strInfo, sizeof(m_szInfo)-1);
        m_szInfo[sizeof(m_szInfo)-1] = 0;

        infos->setInfo(m_szInfo);
		infos->setLength(nLengthMS);
		infos->setTitle(m_szTitle);

		return 1;
	}

};

////////////////////////////////////////////////////////////////////////////////

converter_make_service(CEmuSoundWinamp3Converter);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
