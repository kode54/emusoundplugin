#if !defined(AFX_ESPCONFIGOUTPUTPAGE_H__F914FA45_3BD1_4A4A_A5BB_F7D71F53E6FD__INCLUDED_)
#define AFX_ESPCONFIGOUTPUTPAGE_H__F914FA45_3BD1_4A4A_A5BB_F7D71F53E6FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ESPConfigOutputPage.h : header file
//

#include "EmuSoundPlugin.h"

/////////////////////////////////////////////////////////////////////////////
// CESPConfigOutputPage dialog

class CESPConfigOutputPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CESPConfigOutputPage)

// Construction
public:
	void BindToPlugin(CEmuSoundPlugin *pPlugin);
	CESPConfigOutputPage();
	~CESPConfigOutputPage();

// Dialog Data
	//{{AFX_DATA(CESPConfigOutputPage)
	enum { IDD = IDD_ESP_CONFIG_OUTPUT };
	CSliderCtrl	m_cVolumeSlider;
	CStatic m_cVolume;
	CButton	m_cSoftSaturate;
	CEdit	m_cResampleRate;
	CButton	m_cResample;
    CStatic m_cLabelReplayGain;
    CStatic m_cLabelClipProtect;
    CComboBox m_cComboReplayGain;
    CComboBox m_cComboClipProtect;
    CStatic m_cNoteXMPlay;
    CStatic m_cNoteVolume;
	//}}AFX_DATA

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CESPConfigOutputPage)
	public:
	virtual void OnOK();
	virtual void OnCancel();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CESPConfigOutputPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnEspSoftsaturate();
//	afx_msg void OnChangeEspVolume();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CEmuSoundPlugin* m_pPlugin;

	void UpdateVolumeFromSlider();
	void UpdateSliderFromVolume();

    double m_dOriginalGlobalVolume;
    BOOL m_bOriginalSoftSaturate;

    BOOL m_bOriginalReplayGainEnable;
    BOOL m_bOriginalReplayGainAlbum;
    BOOL m_bOriginalClipProtectEnable;
    BOOL m_bOriginalClipProtectAlbum;

public:
    afx_msg void OnCbnSelchangeEspComboReplaygain();
    afx_msg void OnCbnSelchangeEspComboClipprotect();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ESPCONFIGOUTPUTPAGE_H__F914FA45_3BD1_4A4A_A5BB_F7D71F53E6FD__INCLUDED_)
