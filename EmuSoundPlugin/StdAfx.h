#pragma once

// set WINVER - this warning came up when I migrated the project from VC++ 6 to VS.NET 7.
// we'd like all compiled programs to run on Win95 or higher.
#ifndef WINVER
#define WINVER 0x0400
#endif

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afx.h>
#include <afxwin.h>

#include <afxcmn.h>
#include <afxdlgs.h>

#include <afxmt.h>
