#pragma once

class CReplayGainAnalyzer
{
public:
    CReplayGainAnalyzer(void);
    virtual ~CReplayGainAnalyzer(void);

    static bool IsFrequencyOK(int freq);
    
    bool Init(long samplefreq);

    bool AnalyzeSamples(const float* l, const float* r, size_t num_samples);

    float GetTitleGain();
    float GetAlbumGain();

private:

    float  *         linprebuf;
    float  *         linpre;                                          // left input samples, with pre-buffer
    float  *         lstepbuf;
    float  *         lstep;                                           // left "first step" (i.e. post first filter) samples
    float  *         loutbuf;
    float  *         lout;                                            // left "out" (i.e. post second filter) samples
    float  *         rinprebuf;
    float  *         rinpre;                                          // right input samples ...
    float  *         rstepbuf;
    float  *         rstep;
    float  *         routbuf;
    float  *         rout;
    long             sampleWindow;                                    // number of samples required to reach number of milliseconds required for RMS window
    long             totsamp;
    double           lsum;
    double           rsum;
    int              freqindex;
    int              first;
    DWORD   *        A;
    DWORD   *        B;

};
