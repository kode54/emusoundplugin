// ESPConfigGeneralPage.cpp : implementation file
//

#include "StdAfx.h"
#include "ESPResource.h"

#include "ESPConfigGeneralPage.h"

#include "../MFCUtil/NumString.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CESPConfigGeneralPage property page

IMPLEMENT_DYNCREATE(CESPConfigGeneralPage, CPropertyPage)

CESPConfigGeneralPage::CESPConfigGeneralPage() : CPropertyPage(CESPConfigGeneralPage::IDD)
{
  m_pPlugin = NULL;

	//{{AFX_DATA_INIT(CESPConfigGeneralPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CESPConfigGeneralPage::~CESPConfigGeneralPage()
{
}

void CESPConfigGeneralPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESPConfigGeneralPage)
	DDX_Control(pDX, IDC_ESP_STOPSILENCE, m_cStopSilence);
	DDX_Control(pDX, IDC_ESP_STOPSECONDS, m_cStopSeconds);
	DDX_Control(pDX, IDC_ESP_OPENINGSILENCE, m_cOpeningSilence);
	DDX_Control(pDX, IDC_ESP_LENGTH, m_cLength);
	DDX_Control(pDX, IDC_ESP_INDEFINITE, m_cIndefinite);
	DDX_Control(pDX, IDC_ESP_FADE, m_cFade);
	DDX_Control(pDX, IDC_ESP_PRIORITYNAME, m_cPriorityName);
	DDX_Control(pDX, IDC_ESP_PRIORITY, m_cPriority);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CESPConfigGeneralPage, CPropertyPage)
	//{{AFX_MSG_MAP(CESPConfigGeneralPage)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESPConfigGeneralPage message handlers

static const LPCWSTR alpszPriorityNames[5] = {
    L"Lowest",
    L"Below Normal",
    L"Normal",
    L"Above Normal",
    L"Highest"
};

void CESPConfigGeneralPage::SyncPriorityNameToSlider()
{
    int n = m_cPriority.GetPos();
    if(n < 0) n = 0;
    if(n > 4) n = 4;
    m_cPriorityName.SetWindowText(CString(alpszPriorityNames[n]));
}

void CESPConfigGeneralPage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
  SyncPriorityNameToSlider();
	CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}

BOOL CESPConfigGeneralPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: Add extra initialization here
  m_cPriority.SetRange(0, 4);
  m_cPriority.SetPageSize(1);
  m_cPriority.SetLineSize(1);
  m_cPriority.SetTicFreq(1);

  //
  // Get options
  //
  if(m_pPlugin) {
    m_pPlugin->LockOptions();

    m_cIndefinite.SetCheck(m_pPlugin->opt_bForceIndefinite ? BST_CHECKED : BST_UNCHECKED);
    m_cLength.SetWindowText(CString(Util::NumToString(m_pPlugin->opt_dDefaultLength, Util::NSFLAG_COLONS)));
    m_cFade.SetWindowText(CString(Util::NumToString(m_pPlugin->opt_dDefaultFade, Util::NSFLAG_COLONS)));
    m_cOpeningSilence.SetCheck(m_pPlugin->opt_bSuppressOpeningSilence ? BST_CHECKED : BST_UNCHECKED);
    m_cStopSilence.SetCheck(m_pPlugin->opt_bSuppressEndingSilence ? BST_CHECKED : BST_UNCHECKED);
    m_cStopSeconds.SetWindowText(CString(Util::NumToString(m_pPlugin->opt_dEndingSilenceSeconds, Util::NSFLAG_COLONS)));
    int n = m_pPlugin->opt_nThreadPriority;
    if(n < 0) n = 0;
    if(n > 4) n = 4;
    m_cPriority.SetPos(n);

    m_pPlugin->UnlockOptions();
  }

    SyncPriorityNameToSlider();

    //
    // Thread priority irrelevant on non-Winamp2
    //
    if(!m_pPlugin->IsOptionSupported(&m_pPlugin->opt_nThreadPriority)) {
        m_cPriority.ShowWindow(SW_HIDE);
        m_cPriority.EnableWindow(FALSE);
        m_cPriorityName.ShowWindow(SW_HIDE);
        m_cPriorityName.EnableWindow(FALSE);
        CWnd *w = GetDlgItem(IDC_ESP_FRAME_PRIORITY);
        if(w) {
            w->ShowWindow(SW_HIDE);
            w->EnableWindow(FALSE);
        }
    }

    if(!m_pPlugin->IsOptionSupported(&m_pPlugin->opt_bSuppressOpeningSilence)) {
        m_cOpeningSilence.ShowWindow(SW_HIDE);
        m_cOpeningSilence.EnableWindow(FALSE);
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CESPConfigGeneralPage::BindToPlugin(CEmuSoundPlugin *pPlugin)
{
  m_pPlugin = pPlugin;
}

void CESPConfigGeneralPage::OnOK() 
{

    //
    // Set options
    //
    if(m_pPlugin) {
        m_pPlugin->LockOptions();

        CString s;

        m_pPlugin->opt_bForceIndefinite = (m_cIndefinite.GetCheck() == BST_CHECKED);

        s.Empty(); m_cLength.GetWindowText(s);
        Util::StringToNum(CStringW(s), m_pPlugin->opt_dDefaultLength, Util::NSFLAG_COLONS);

        s.Empty(); m_cFade.GetWindowText(s);
        Util::StringToNum(CStringW(s), m_pPlugin->opt_dDefaultFade, Util::NSFLAG_COLONS);

        m_pPlugin->opt_bSuppressOpeningSilence = (m_cOpeningSilence.GetCheck() == BST_CHECKED);
        m_pPlugin->opt_bSuppressEndingSilence = (m_cStopSilence.GetCheck() == BST_CHECKED);

        s.Empty(); m_cStopSeconds.GetWindowText(s);
        Util::StringToNum(CStringW(s), m_pPlugin->opt_dEndingSilenceSeconds, Util::NSFLAG_COLONS);

        int n = m_cPriority.GetPos();
        if(n < 0) n = 0;
        if(n > 4) n = 4;
        m_pPlugin->opt_nThreadPriority = n;

        m_pPlugin->UnlockOptions();
    }

    CPropertyPage::OnOK();
}
