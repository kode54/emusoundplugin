// EmuSoundPlugin.cpp: implementation of the CEmuSoundPlugin class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ESPResource.h"

#include "EmuSoundPlugin.h"

#include "../MFCUtil/NumString.h"

#include "ESPConfigGeneralPage.h"
#include "ESPConfigOutputPage.h"
#include "ESPConfigErrorPage.h"

/////////////////////////////////////////////////////////////////////////////

CStringW CEmuSoundPlugin::GetFrameworkVersion()
{
    CStringW v;
    v = L"ESP0005 (built ";
    v += __DATE__;
    v += L")";
    return v;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

extern "C" void __cdecl __intel_cpu_indicator_init(void);
#include "../MFCUtil/promote_intel.h"

//extern "C" int __intel_cpu_indicator;

CEmuSoundPlugin::CEmuSoundPlugin()
{
  __intel_cpu_indicator_init();
  promote_intel_cpu_indicator();

//  CStringW str;
//  str.Format(L"__intel_cpu_indicator=0x%X", __intel_cpu_indicator);
//  ::MessageBox(NULL, CString(str), CString("testing"), MB_OK);

  // clear invoked-from mask
  m_bInvokedFrom = 0;

  // This code ensures all the imports get linked
  m_pWinamp2Link = &Winamp2Link;
  m_pWinamp3Link = &Winamp3Link;
  m_pXMPlayLink  = &XMPlayLink;

  m_pPlayerList = NULL;

  //
  // Set default options
  //
  opt_bForceIndefinite        = FALSE;
  opt_dDefaultLength          = 170.0;
  opt_dDefaultFade            = 10.0;
  opt_bSuppressOpeningSilence = TRUE;
  opt_bSuppressEndingSilence  = TRUE;
  opt_dEndingSilenceSeconds   = 5.0;
  opt_nThreadPriority         = 2;

  opt_dGlobalVolume           = K14_DEFAULT_VOLUME;
  opt_bSoftSaturate           = TRUE;
  opt_bResample               = FALSE;
  opt_nResampleFreq           = 44100;

  opt_bReplayGainEnable       = TRUE;
  opt_bReplayGainAlbum        = FALSE;
  opt_bClipProtectEnable      = TRUE;
  opt_bClipProtectAlbum       = FALSE;

  opt_bStrictFormatChecking   = FALSE;
  opt_bShowErrorMessages      = FALSE;
}

CEmuSoundPlugin::~CEmuSoundPlugin()
{

}

/////////////////////////////////////////////////////////////////////////////

ESP_OPTION_START(CEmuSoundPlugin,CEmuSoundPlugin)

ESP_OPTION(L"ForceIndefinite"       , OPT_BOOL       , opt_bForceIndefinite       , ESP_ALL_PLAYERS)
ESP_OPTION(L"DefaultLength"         , OPT_DOUBLE_TIME, opt_dDefaultLength         , ESP_ALL_PLAYERS)
ESP_OPTION(L"DefaultFade"           , OPT_DOUBLE_TIME, opt_dDefaultFade           , ESP_ALL_PLAYERS)
ESP_OPTION(L"SuppressOpeningSilence", OPT_BOOL       , opt_bSuppressOpeningSilence, ESP_WINAMP2)
ESP_OPTION(L"SuppressEndingSilence" , OPT_BOOL       , opt_bSuppressEndingSilence , ESP_ALL_PLAYERS)
ESP_OPTION(L"EndingSilenceSeconds"  , OPT_DOUBLE_TIME, opt_dEndingSilenceSeconds  , ESP_ALL_PLAYERS)
ESP_OPTION(L"ThreadPriority"        , OPT_INT        , opt_nThreadPriority        , ESP_WINAMP2)
ESP_OPTION(L"GlobalVolume"          , OPT_DOUBLE     , opt_dGlobalVolume          , ESP_ALL_PLAYERS)
ESP_OPTION(L"SoftSaturate"          , OPT_BOOL       , opt_bSoftSaturate          , ESP_WINAMP2 | ESP_WINAMP3)
ESP_OPTION(L"Resample"              , OPT_BOOL       , opt_bResample              , ESP_ALL_PLAYERS)
ESP_OPTION(L"ResampleFreq"          , OPT_INT        , opt_nResampleFreq          , ESP_ALL_PLAYERS)
ESP_OPTION(L"ReplayGainEnable"      , OPT_BOOL       , opt_bReplayGainEnable      , ESP_WINAMP2 | ESP_WINAMP3)
ESP_OPTION(L"ReplayGainAlbum"       , OPT_BOOL       , opt_bReplayGainAlbum       , ESP_WINAMP2 | ESP_WINAMP3)
ESP_OPTION(L"ClipProtectEnable"     , OPT_BOOL       , opt_bClipProtectEnable     , ESP_ALL_PLAYERS)
ESP_OPTION(L"ClipProtectAlbum"      , OPT_BOOL       , opt_bClipProtectAlbum      , ESP_ALL_PLAYERS)
ESP_OPTION(L"StrictFormatChecking"  , OPT_BOOL       , opt_bStrictFormatChecking  , ESP_ALL_PLAYERS)
ESP_OPTION(L"ShowErrorMessages"     , OPT_BOOL       , opt_bShowErrorMessages     , ESP_ALL_PLAYERS)

ESP_OPTION_END(CEmuSoundPlugin,CEmuSoundPlugin)

/////////////////////////////////////////////////////////////////////////////

void CEmuSoundPlugin::LockOptions() { m_csOption.Lock(); }
void CEmuSoundPlugin::UnlockOptions() { m_csOption.Unlock(); }

/////////////////////////////////////////////////////////////////////////////

CPlayer* CEmuSoundPlugin::Open(File f, CStringW &strError)
{
    CPlayer *p = CreatePlayer(f, strError);
    if(!p) return NULL;

    // Attempt to open
    BOOL b = p->Open();
    if(!b) {
        strError = p->GetLastError();
        delete p;
        return NULL;
    }

    // Link with the player list
    m_csPlayerList.Lock();
    m_pPlayerList = p->CreateLink(m_pPlayerList);
    m_csPlayerList.Unlock();

    return p;
}

/////////////////////////////////////////////////////////////////////////////

CPlayer* CEmuSoundPlugin::CreatePlayer(File f, CStringW &strError)
{
    strError = L"CEmuSoundPlugin::CreatePlayer not implemented";
    return NULL;
}

/////////////////////////////////////////////////////////////////////////////

int CEmuSoundPlugin::GetDecodeThreadPriority()
{
  LockOptions();
  int n = opt_nThreadPriority;
  UnlockOptions();
  int nTPV = THREAD_PRIORITY_NORMAL;
  switch(n) {
  case 0: nTPV = THREAD_PRIORITY_LOWEST; break;
  case 1: nTPV = THREAD_PRIORITY_BELOW_NORMAL; break;
  case 2: nTPV = THREAD_PRIORITY_NORMAL; break;
  case 3: nTPV = THREAD_PRIORITY_ABOVE_NORMAL; break;
  case 4: nTPV = THREAD_PRIORITY_HIGHEST; break;
  }
  return nTPV;
}

/////////////////////////////////////////////////////////////////////////////

int CEmuSoundPlugin::GetSafeDecodeThreadPriority()
{
  int n = GetDecodeThreadPriority();
  if(
    (n == THREAD_PRIORITY_ABOVE_NORMAL) ||
    (n == THREAD_PRIORITY_HIGHEST)
  ) {
    n = THREAD_PRIORITY_NORMAL;
  }
  return n;
}

/////////////////////////////////////////////////////////////////////////////
//
// Utility calls for converting sample counts to/from milliseconds, at a
// given sample rate
//
int __fastcall SamplesToMS(INT64 n64Samples, DWORD dwSR) {
  n64Samples = (n64Samples * 1000) / ((INT64)(dwSR));
  return (int)n64Samples;
}

INT64 __fastcall MSToSamples(int nMS, DWORD dwSR) {
  if(nMS < 0) return 0;
  INT64 n64Samples = nMS;
  n64Samples = (n64Samples * ((INT64)(dwSR))) / 1000;
  return n64Samples;
}

/////////////////////////////////////////////////////////////////////////////

int CEmuSoundPlugin::Config(HWND hWndParent) {
    CPropertySheet dlg(CString(GetShortBanner()), CWnd::FromHandle(hWndParent));
    dlg.m_psh.dwFlags |= PSH_NOAPPLYNOW;

    BuildConfigPropertySheet(&dlg);

    int r = dlg.DoModal();
    while(dlg.GetPageCount() > 0) {
        CPropertyPage *pPage = dlg.GetPage(0);
        dlg.RemovePage(0);
        delete pPage;
    }

    if(r == IDOK) SaveOptions();

    return r;
}

/////////////////////////////////////////////////////////////////////////////

void CEmuSoundPlugin::BuildConfigPropertySheet(CPropertySheet *pSheet) {

  { CESPConfigGeneralPage *p = new CESPConfigGeneralPage;
    p->BindToPlugin(this);
    pSheet->AddPage(p);
  }

  { CESPConfigOutputPage *p = new CESPConfigOutputPage;
    p->BindToPlugin(this);
    pSheet->AddPage(p);
  }

  { CESPConfigErrorPage *p = new CESPConfigErrorPage;
    p->BindToPlugin(this);
    pSheet->AddPage(p);
  }

}

/////////////////////////////////////////////////////////////////////////////

void CEmuSoundPlugin::About(HWND hWndParent) {
    ::MessageBox(hWndParent, CString(L"CEmuSoundPlugin::About"), CString(L"Error"), MB_OK|MB_ICONHAND);
}

void CEmuSoundPlugin::InfoDialog(File f, HWND hWndParent, LONG *plNowMS) {
    CStringW s;
    s.Format(L"CEmuSoundPlugin::InfoDialog(%s)", LPCWSTR(f.GetPath()));
    ::MessageBox(hWndParent, CString(s), CString(L"Error"), MB_OK|MB_ICONHAND);
}

/////////////////////////////////////////////////////////////////////////////

void CEmuSoundPlugin::Close(CPlayer *pPlayer)
{
  if(!pPlayer) return;

  // Unlink from player list
  m_csPlayerList.Lock();
  m_pPlayerList = pPlayer->DeleteLink(m_pPlayerList);
  m_csPlayerList.Unlock();

  // Delete
  delete pPlayer;
}

/////////////////////////////////////////////////////////////////////////////

void CEmuSoundPlugin::NotifyManualVolumeChange(File f, double dVol)
{
    m_csPlayerList.Lock();

    CPlayer *p;
    for(p = m_pPlayerList; p; p = p->m_pNext) {
        p->Lock();
        if(p->GetFile() == f) p->ChangeManualVolume(dVol);
        p->Unlock();
    }

    m_csPlayerList.Unlock();
}

/////////////////////////////////////////////////////////////////////////////

void __fastcall Winamp2ML_NotifyTagChange(File f);

void CEmuSoundPlugin::NotifyTagChange(File f)
{
    m_csPlayerList.Lock();

    CPlayer *p;

    for(p = m_pPlayerList; p; p = p->m_pNext) {
        p->Lock();
        if(p->GetFile() == f) p->ReloadTag();
        p->Unlock();
    }

    m_csPlayerList.Unlock();

    Winamp2ML_NotifyTagChange(f);
}

/////////////////////////////////////////////////////////////////////////////

void CEmuSoundPlugin::SaveOptionsToINI(CStringW strPath, CStringW strSection)
{
    int n;
    for(n = 0;; n++) {
        CStringW s;
        void *pData = NULL;
        int nType = OPT_NONE;
        DWORD dwPlugins = 0;
        LPCWSTR lpszKey = GetEnumeratedOption(n, &nType, &pData, &dwPlugins, s);
        if(!lpszKey) break;
        ::WritePrivateProfileString(
            CString(strSection),
            CString(lpszKey),
            CString(s),
            CString(strPath)
        );
    }
}

/////////////////////////////////////////////////////////////////////////////

void CEmuSoundPlugin::LoadOptionsFromINI(CStringW strPath, CStringW strSection)
{
    TCHAR szReturned[1000];
    int n;
    for(n = 0;; n++) {
        CStringW strExisting;
        void *pData = NULL;
        int nType = OPT_NONE;
        DWORD dwPlugins = 0;
        LPCWSTR lpszKey = GetEnumeratedOption(n, &nType, &pData, &dwPlugins, strExisting);
        if(!lpszKey) break;
        szReturned[0] = 0;
        ::GetPrivateProfileString(
            CString(strSection),
            CString(lpszKey),
            CString(strExisting),
            szReturned,
            sizeof(szReturned)/sizeof(szReturned[0]),
            CString(strPath)
        );
        double d = 0;
        switch(nType) {
        case OPT_BOOL       : if(Util::StringToNum(szReturned, d, 0                  )) *((BOOL  *)(pData)) = (BOOL  )d; break;
        case OPT_INT        : if(Util::StringToNum(szReturned, d, 0                  )) *((int   *)(pData)) = (int   )d; break;
        case OPT_DWORD      : if(Util::StringToNum(szReturned, d, 0                  )) *((DWORD *)(pData)) = (DWORD )d; break;
        case OPT_FLOAT      : if(Util::StringToNum(szReturned, d, 0                  )) *((float *)(pData)) = (float )d; break;
        case OPT_DOUBLE     : if(Util::StringToNum(szReturned, d, 0                  )) *((double*)(pData)) = (double)d; break;
        case OPT_DOUBLE_TIME: if(Util::StringToNum(szReturned, d, Util::NSFLAG_COLONS)) *((double*)(pData)) = (double)d; break;
        case OPT_WSTRING    : *((CStringW*)(pData)) = szReturned; break;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////

LPCWSTR CEmuSoundPlugin::GetEnumeratedOption(int n, int *pnType, void **ppData, DWORD *pdwPlugins, CStringW &s)
{
    if(pnType) { *pnType = OPT_NONE; }
    if(ppData) { *ppData = NULL; }
    if(pdwPlugins) { *pdwPlugins = 0; }
    LPCWSTR lpszKey = EnumerateOption(n, pnType, ppData, pdwPlugins);
    if(!lpszKey) return NULL;
    s.Empty();
    switch(*pnType) {
    case OPT_BOOL       : s = Util::NumToString(*((BOOL  *)(*ppData)), 0); break;
    case OPT_INT        : s = Util::NumToString(*((int   *)(*ppData)), 0); break;
    case OPT_DWORD      : s = Util::NumToString(*((DWORD *)(*ppData)), 0); break;
    case OPT_FLOAT      : s = Util::NumToString(*((float *)(*ppData)), 0); break;
    case OPT_DOUBLE     : s = Util::NumToString(*((double*)(*ppData)), 0); break;
    case OPT_DOUBLE_TIME: s = Util::NumToString(*((double*)(*ppData)), Util::NSFLAG_COLONS); break;
    case OPT_WSTRING    : s = *((CStringW*)(*ppData)); break;
    }
    return lpszKey;
}

/////////////////////////////////////////////////////////////////////////////

bool CEmuSoundPlugin::IsOptionSupported(void *opt) {
    int n;
    for(int n = 0;; n++) {
        int nType = OPT_NONE;
        void *pData = NULL;
        DWORD dwPlugins = 0;
        LPCWSTR lpszKey = EnumerateOption(n, &nType, &pData, &dwPlugins);
        if(!lpszKey) break;
        if(pData == opt) return WasInvokedFrom(dwPlugins);
    }
    return false;
}

/////////////////////////////////////////////////////////////////////////////

int CEmuSoundPlugin::ErrorBox(HWND hWndParent, CStringW strMessage)
{
    LockOptions();
    BOOL bShow = opt_bShowErrorMessages;
    UnlockOptions();
    if(!bShow) return IDOK;
    int r = ::MessageBox(hWndParent, CString(strMessage), CString(GetShortBanner()), MB_OK|MB_ICONHAND);
    return r;
}

/////////////////////////////////////////////////////////////////////////////

bool CEmuSoundPlugin::CheckMagic(BYTE *buf, DWORD length) {
    return false; // no, can't play it
}

/////////////////////////////////////////////////////////////////////////////
//
// Path must be valid
//
CMusicFileTag *CEmuSoundPlugin::GetMetaData(File f, CStringW &strTitle, INT64 &nLengthMS)
{
    strTitle = f.GetPath();
    nLengthMS = 3 * 60 * 1000; // default 3 minutes
    return NULL;
}

CMusicFileTag *CEmuSoundPlugin::NewMetaData(File f)
{
    return NULL;
}

/////////////////////////////////////////////////////////////////////////////

CStringW __fastcall Winamp2PluginINIGetFilename(void);

void CEmuSoundPlugin::SaveOptions()
{
    if(WasInvokedFrom(ESP_WINAMP2)) {
        SaveOptionsToINI(
            Winamp2PluginINIGetFilename(),
            GetOptionSection()
        );
    }
}

void CEmuSoundPlugin::LoadOptions()
{
    if(WasInvokedFrom(ESP_WINAMP2)) {
        LoadOptionsFromINI(
            Winamp2PluginINIGetFilename(),
            GetOptionSection()
        );
    }
}

/////////////////////////////////////////////////////////////////////////////
