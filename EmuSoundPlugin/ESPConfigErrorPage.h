#if !defined(AFX_ESPCONFIGERRORPAGE_H__E0265DBA_0433_4C2D_8E41_73E3005E80DB__INCLUDED_)
#define AFX_ESPCONFIGERRORPAGE_H__E0265DBA_0433_4C2D_8E41_73E3005E80DB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ESPConfigErrorPage.h : header file
//

#include "EmuSoundPlugin.h"

/////////////////////////////////////////////////////////////////////////////
// CESPConfigErrorPage dialog

class CESPConfigErrorPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CESPConfigErrorPage)

// Construction
public:
	void BindToPlugin(CEmuSoundPlugin *pPlugin);

	CESPConfigErrorPage();
	~CESPConfigErrorPage();

// Dialog Data
	//{{AFX_DATA(CESPConfigErrorPage)
	enum { IDD = IDD_ESP_CONFIG_ERROR };
	CButton	m_cStrict;
	CButton	m_cShowError;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CESPConfigErrorPage)
	public:
	virtual void OnOK();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CESPConfigErrorPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CEmuSoundPlugin* m_pPlugin;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ESPCONFIGERRORPAGE_H__E0265DBA_0433_4C2D_8E41_73E3005E80DB__INCLUDED_)
