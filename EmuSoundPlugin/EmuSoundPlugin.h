////////////////////////////////////////////////////////////////////////////////
//
// CEmuSoundPlugin
//
// Base class for all plugins
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ESPResource.h"

//
// useful for option enumeration in base class as well as subclasses
//
#define ESP_OPTION_DECLARE virtual LPCWSTR EnumerateOption(int n, int *pnType, void **ppData, DWORD *pdwPlugins);
#define ESP_OPTION_START(c,b) LPCWSTR c::EnumerateOption(int n, int *pnType, void **ppData, DWORD *pdwPlugins) { if(!pnType || !ppData || !pdwPlugins) { return NULL; }
#define ESP_OPTION(s,t,d,p) if(!n){*pnType=(t);*ppData=&(d);*pdwPlugins=(p);return(s);}else{n--;}
#define ESP_OPTION_END(c,b) if((c::EnumerateOption)==(b::EnumerateOption)) { return NULL; } else { return b::EnumerateOption(n, pnType, ppData, pdwPlugins); } }
enum {
    ESP_WINAMP2     = 0x0001,
    ESP_WINAMP3     = 0x0002,
    ESP_XMPLAY      = 0x0004,
    ESP_ALL_PLAYERS = 0x0007
};

#define K14_DEFAULT_VOLUME (1.9952623149688796013524553967395)

#include "Player.h"
#include "MusicFileTag.h"

#include "../MFCUtil/File.h"

class CEmuSoundPlugin  
{
public:
	CStringW GetFrameworkVersion();
	void LoadOptions();
	void SaveOptions();

	int ErrorBox(HWND hWndParent, CStringW strMessage);
	void NotifyManualVolumeChange(File f, double dVol);
	void NotifyTagChange(File f);

    // also deletes pPlayer
	void Close(CPlayer *pPlayer);

    // Decode thread priorities
    // "Safe" caps the priority to NORMAL, suitable for initial loading.
	int GetSafeDecodeThreadPriority();
	int GetDecodeThreadPriority();

	CPlayer* Open(File f, CStringW &strError);

    // check the signature to see if this is a file format we can play
    virtual bool CheckMagic(BYTE *buf, DWORD length);

    virtual CMusicFileTag* GetMetaData(File f, CStringW &strTitle, INT64 &nLengthMS);
    virtual CMusicFileTag* NewMetaData(File f);

	int Config(HWND hWndParent);
	virtual void About(HWND hWndParent);
	virtual void InfoDialog(File f, HWND hWndParent, LONG *plNowMS);
	virtual LPCWSTR* GetSupportedExtensions() = 0;
	virtual LPCWSTR  GetBanner() = 0;
	virtual LPCWSTR  GetShortBanner() = 0;
	virtual LPCWSTR  GetOptionSection() = 0;

	void UnlockOptions();
	void LockOptions();

    enum {
        OPT_NONE = 0,
        OPT_BOOL,
        OPT_INT,
        OPT_DWORD,
        OPT_FLOAT,
        OPT_DOUBLE,
        OPT_DOUBLE_TIME,
        OPT_WSTRING
    };

    BOOL    opt_bForceIndefinite;
    double  opt_dDefaultLength;
    double  opt_dDefaultFade;
    BOOL    opt_bSuppressOpeningSilence;
    BOOL    opt_bSuppressEndingSilence;
    double  opt_dEndingSilenceSeconds;
    int     opt_nThreadPriority;

    // global volume, also preamp for replaygain
    double  opt_dGlobalVolume;

    BOOL    opt_bSoftSaturate;

    BOOL    opt_bResample;
    int     opt_nResampleFreq;

    BOOL    opt_bReplayGainEnable;
    BOOL    opt_bReplayGainAlbum;
    BOOL    opt_bClipProtectEnable;
    BOOL    opt_bClipProtectAlbum;

    BOOL    opt_bStrictFormatChecking;
    BOOL    opt_bShowErrorMessages;

    bool IsOptionSupported(void *opt);

	CEmuSoundPlugin();
	virtual ~CEmuSoundPlugin();

    // to ensure everything is linked in
    int *m_pWinamp2Link;
    int *m_pWinamp3Link;
    int *m_pXMPlayLink;

    void InvokeFrom(DWORD plugins) { m_bInvokedFrom |= plugins; }
    bool WasInvokedFrom(DWORD plugins) const { return (m_bInvokedFrom & plugins) != 0; }

    bool IsVolumeImmediate() const { return WasInvokedFrom(ESP_WINAMP2 | ESP_WINAMP3); }

protected:
    ESP_OPTION_DECLARE

    virtual void BuildConfigPropertySheet(CPropertySheet *pSheet);
    virtual CPlayer* CreatePlayer(File f, CStringW &strError);

private:
	void LoadOptionsFromINI(CStringW lpszPath, CStringW lpszSection);
	void SaveOptionsToINI  (CStringW lpszPath, CStringW lpszSection);

    DWORD m_bInvokedFrom;

	LPCWSTR GetEnumeratedOption(int n, int *pnType, void **ppData, DWORD *pdwPlugins, CStringW &s);

	//void PlayerOptionSetup(CPlayer *p);
	CCriticalSection m_csPlayerList;
	CPlayer* m_pPlayerList;
	CCriticalSection m_csOption;
};

// there is one global plugin instance
extern CEmuSoundPlugin *g_pPlugin;

// to ensure everything is linked in
extern int Winamp2Link;
extern int Winamp3Link;
extern int XMPlayLink;

int __fastcall SamplesToMS(INT64 n64Samples, DWORD dwSR);
INT64 __fastcall MSToSamples(int nMS, DWORD dwSR);
