// Player.cpp: implementation of the CPlayer class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ESPResource.h"

#include "Player.h"

#include "EmuSoundPlugin.h"

#include "../MFCUtil/NumString.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define COEF_16BIT_TO_FLOAT (0.000030517578125f)
#define COEF_FLOAT_TO_16BIT (32768.0f)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPlayer::CPlayer(File f)
{
    m_pNext = NULL;
    m_pPrev = NULL;

    m_dwInternalSampleRate = 44100;
    m_dwOutputSampleRate = 44100;
    m_dwChannels = 2;
    m_n64SamplePosition = 0;
    m_n64SilenceRunLength = 0;
    m_bOpeningSilence = true;

    m_strLastError.Empty();

    m_openfile = f;

    m_dSongLength = -1.0;
    m_dSongFade   =  0.0;
    m_dManualVolume  = 1.0;
    m_bHasRGTrackVolume = FALSE;
    m_bHasRGAlbumVolume = FALSE;
    m_bHasRGTrackMaxVol = FALSE;
    m_bHasRGAlbumMaxVol = FALSE;
    m_dRGTrackVolume = 1.0;
    m_dRGAlbumVolume = 1.0;
    m_dRGTrackMaxVol = 1.0;
    m_dRGAlbumMaxVol = 1.0;

    m_nFilterPoints = 7;

    m_aResampleBuffer.SetSize(2 * 576);

}

CPlayer::~CPlayer()
{
}

/////////////////////////////////////////////////////////////////////////////

BOOL CPlayer::Open() {
    Close();
    m_n64SamplePosition = 0;
    m_n64SilenceRunLength = 0;
    m_bOpeningSilence = true;

    m_dSongLength = -1.0;
    m_dSongFade   =  0.0;
    m_dManualVolume  = 1.0;
    m_bHasRGTrackVolume = FALSE;
    m_bHasRGAlbumVolume = FALSE;
    m_bHasRGTrackMaxVol = FALSE;
    m_bHasRGAlbumMaxVol = FALSE;
    m_dRGTrackVolume = 1.0;
    m_dRGAlbumVolume = 1.0;
    m_dRGTrackMaxVol = 1.0;
    m_dRGAlbumMaxVol = 1.0;

    BOOL b = RawOpen();
    if(!b) return FALSE;
    ReloadTag();

    g_pPlugin->LockOptions();
    BOOL bResample     = g_pPlugin->opt_bResample;
    int  nResampleFreq = g_pPlugin->opt_nResampleFreq;
    g_pPlugin->UnlockOptions();

    if(nResampleFreq < 0) { nResampleFreq = -nResampleFreq; }
    if(nResampleFreq < 1000) { nResampleFreq = 1000; }
    if(nResampleFreq > 1000000) { nResampleFreq = 1000000; }

    m_dwOutputSampleRate = m_dwInternalSampleRate;
    if(bResample) { m_dwOutputSampleRate = nResampleFreq; }

    m_resampler.Redesign(
        m_dwInternalSampleRate,
        m_dwOutputSampleRate,
        m_dwChannels,
        m_nFilterPoints
    );

    return TRUE;
}

BOOL CPlayer::Rewind()
{
    return Open();
}

INT64 CPlayer::Tell()
{
    return m_n64SamplePosition;
}

//
// conforms only if ReadEnvelope conforms
//
int CPlayer::Read(float *buffer, int samples) {
    int r = ReadEnvelope(buffer, samples);
    if(r > 0) {
        int nSilent = 0; // when seeking, assume non-silent
        if(buffer) {
            nSilent = GetEndingSilentSamples(buffer, r, m_dwChannels);
        }
        if(nSilent) {
            m_n64SilenceRunLength += r;
        } else {
            m_n64SilenceRunLength = nSilent;
            m_bOpeningSilence = false;
        }
        m_n64SamplePosition += r;
    }
    return r;
}

//
// think now conforms
//
int CPlayer::ReadEnvelope(
    float *buffer,
    int samples
) {
    BOOL bForceIndefinite = FALSE;

    g_pPlugin->LockOptions();
    bForceIndefinite = g_pPlugin->opt_bForceIndefinite;
    g_pPlugin->UnlockOptions();

    if(bForceIndefinite) return RawReadAtOutputRate(buffer, samples);

    INT64 n64PositionFade;
    INT64 n64PositionSilence;
    INT64 n64PositionEnd;
    ComputeLengths(n64PositionFade, n64PositionSilence, n64PositionEnd);

    int sdone = 0;

    int nError = -2;

    //
    // See what we can read at full volume
    //
    if((m_n64SamplePosition+sdone) < n64PositionFade) {
        INT64 stry = n64PositionFade - (m_n64SamplePosition+sdone);
        if(stry > samples) stry = samples;
        int left = (int)stry;
        while(left > 0) {
            int n = RawReadAtOutputRate(buffer, left);
            if(n < 0) { nError = n; break; }
            if(buffer) buffer += m_dwChannels * n;
            samples -= n;
            left -= n;
            sdone += n;
        }
        if(stry == left) {
            if(sdone) return sdone;
            return nError;
        }
        if(left) return sdone;
    }

    //
    // If we're still within the fade range, do a slope
    //
    if(
        ((m_n64SamplePosition+sdone) >= n64PositionFade) &&
        ((m_n64SamplePosition+sdone) < n64PositionSilence)
    ) {
        double dfadesamples = (double)(n64PositionSilence - n64PositionFade);
        INT64 stry = n64PositionSilence - (m_n64SamplePosition+sdone);
        if(stry > samples) stry = samples;
        int left = (int)stry;
        while(left > 0) {
            float fade_a = (float)( 1.0-(((double)((m_n64SamplePosition+sdone     ) - n64PositionFade))/dfadesamples) );
            float fade_b = (float)( 1.0-(((double)((m_n64SamplePosition+sdone+left) - n64PositionFade))/dfadesamples) );
            int n = ReadWithVolumeSlope(buffer, left, fade_a, fade_b);
            if(n < 0) { nError = n; break; }
            if(buffer) buffer += m_dwChannels * n;
            samples -= n;
            left -= n;
            sdone += n;
        }
        if(stry == left) {
            if(sdone) return sdone;
            return nError;
        }
        if(left) return sdone;
    }

    //
    // Finish with silence
    // (this always works to the full length)
    //
    if(
        ((m_n64SamplePosition+sdone) >= n64PositionSilence) &&
        ((m_n64SamplePosition+sdone) < n64PositionEnd)
    ) {
        INT64 stry = n64PositionEnd - (m_n64SamplePosition+sdone);
        if(stry > samples) stry = samples;
        int s = (int)stry;

        if(buffer) {
            memset(buffer, 0, sizeof(float) * m_dwChannels * s);
            buffer += m_dwChannels * s;
        }
        samples -= s;
        sdone += s;

    }

    //
    // At this point we can just return sdone
    //
    if(sdone) return sdone;
    return nError;
}

/*
** Returns the resulting position
** Note that due to seek-past-end, this MAY be different from
** the value given by Tell().
**
** Returns negative on error
*/
INT64 CPlayer::IncrementalSeek(INT64 n64Target, INT64 n64MaxProcess) {
  while(n64MaxProcess) {
    INT64 d = Tell();
    if(d == n64Target) break;
    if(d > n64Target) {
      BOOL b = Rewind();
      // if rewind failed, error string must already be set
      if(!b) return -2;
      continue;
    }
    d = n64Target - d;
    if(d > 576) d = 576;
    if(d > n64MaxProcess) d = n64MaxProcess;
    n64MaxProcess -= d;
    int r = Read(NULL, (int)d);
    if(r < 0) return r;
    //
    // On seek-past-end, we'll return EOF (-1)
    //
    if(r < ((int)d)) return -1;
  }
  return Tell();
}

void CPlayer::ChangeManualVolume(double dVolume)
{
    m_dManualVolume = dVolume;
}

//
// May return negative on error
//
INT64 CPlayer::GetSampleCount()
{
  /*
  ** Don't count the little second of silence we add to the end
  ** when reporting the length to Winamp.
  ** (people might wonder why the lengths don't add up right)
  */
  INT64 n64PositionFade;
  INT64 n64PositionSilence;
  INT64 n64PositionEnd;
  ComputeLengths(n64PositionFade, n64PositionSilence, n64PositionEnd);
  return n64PositionSilence;
}

//
// conforms
//
int CPlayer::ReadWithVolumeSlope(
    float *buffer,
    int samples,
    float vol_a,
    float vol_b
) {
    int nError = 0;

    if(samples <= 0) return 0;

    float vol_slope = (vol_b - vol_a) / ((float)samples);

    int samples_left = samples;
    float *b = buffer;
    while(samples_left > 0) {
        int n;
        n = RawReadAtOutputRate(b, samples_left);
        if(n < 0) { nError = n; break; }
        if(b) b += m_dwChannels * n;
        samples_left -= n;
    }

    samples -= samples_left;

    if(buffer) {
        int i;
        switch(m_dwChannels) {
        case 1:
            for(i = 0; i < samples; i++) {
                float mul = vol_a + vol_slope * i;
                buffer[i] *= mul;
            }
            break;
        case 2:
            for(i = 0; i < samples; i++) {
                float mul = vol_a + vol_slope * i;
                buffer[2*i + 0] *= mul;
                buffer[2*i + 1] *= mul;
            }
            break;
        }
    }

    if(!samples) return nError;
    return samples;
}

void CPlayer::ComputeLengths(INT64 &n64Fade, INT64 &n64Silence, INT64 &n64End)
{
  double dDefaultLength = 170.0;
  double dDefaultFade   = 10.0;

  g_pPlugin->LockOptions();
  dDefaultLength = g_pPlugin->opt_dDefaultLength;
  dDefaultFade   = g_pPlugin->opt_dDefaultFade;
  g_pPlugin->UnlockOptions();

  if(m_dSongLength >= 0.0) {
    dDefaultLength = m_dSongLength;
    dDefaultFade   = m_dSongFade;
  }
  double dSR = m_dwOutputSampleRate;
  n64Fade    =              ((INT64)(dSR*dDefaultLength));
  n64Silence = n64Fade    + ((INT64)(dSR*dDefaultFade));
  n64End     = n64Silence + ((INT64)(m_dwOutputSampleRate));
}

DWORD CPlayer::GetSampleRate() { return m_dwOutputSampleRate; }
DWORD CPlayer::GetChannelCount() { return m_dwChannels; }

/////////////////////////////////////////////////////////////////////////////

void CPlayer::Close() {
    m_resampler.Reset();
    RawClose();
}

/////////////////////////////////////////////////////////////////////////////

BOOL CPlayer::RawOpen() {
    m_strLastError = L"CPlayer::RawOpen not implemented";
    return FALSE;
}

void CPlayer::RawClose() { }

int CPlayer::RawRead(float *buffer, int nSamples) {
    m_strLastError = L"CPlayer::RawRead not implemented";
    return -2;
}

//
// Base implementation of this does nothing
//
void CPlayer::ReloadTag(void) {
}

/////////////////////////////////////////////////////////////////////////////

int CPlayer::GetKBPS() { return 0; }

/////////////////////////////////////////////////////////////////////////////

void CPlayer::Lock() { m_cs.Lock(); }
void CPlayer::Unlock() { m_cs.Unlock(); }

/////////////////////////////////////////////////////////////////////////////

CPlayer* CPlayer::CreateLink(CPlayer *pList)
{
  // Put myself at the head of the list
  m_pPrev = NULL;
  m_pNext = pList;
  if(pList) { pList->m_pPrev = this; }
  return this;
}

/////////////////////////////////////////////////////////////////////////////

CPlayer* CPlayer::DeleteLink(CPlayer *pList)
{
  // If I'm at the head, change pList to the next entry
  if(pList == this) { pList = m_pNext; }
  // Change next/prev
  if(m_pPrev) { m_pPrev->m_pNext = m_pNext; }
  if(m_pNext) { m_pNext->m_pPrev = m_pPrev; }
  // Clear my own next/prev pointers
  m_pPrev = NULL;
  m_pNext = NULL;
  return pList;
}

/////////////////////////////////////////////////////////////////////////////
//
// nonconforming but should be OK
//
int CPlayer::RawReadAtOutputRate(float *buffer, int nSamples)
{
    int nError = -2;

    if(nSamples <= 0) return 0;

    if(m_dwInternalSampleRate == m_dwOutputSampleRate) {
        return RawRead(buffer, nSamples);
    }

    float *tempbuffer = (float*)m_aResampleBuffer.GetData();

    int nOutputSamplesLeft = nSamples;
    for(;;) {
        int o = m_resampler.Read(buffer, nOutputSamplesLeft);
        if(o < 0) {
            m_strLastError = L"Error in resampler";
            nError = -2;
            break;
        }
        if(buffer) buffer += m_dwChannels * o;

        ASSERT(o <= nOutputSamplesLeft);

        nOutputSamplesLeft -= o;

        if(nOutputSamplesLeft <= 0) break;

        int nInputDo = 1 + ((nOutputSamplesLeft * m_dwInternalSampleRate) / m_dwOutputSampleRate);
        if(nInputDo > 576) nInputDo = 576;
        int nInputGot = RawRead(buffer ? tempbuffer : NULL, nInputDo);
        if(nInputGot < 0) {
            nError = nInputGot;
            break;
        }
        m_resampler.Write(buffer ? tempbuffer : NULL, nInputGot);

    }
    nSamples -= nOutputSamplesLeft;

    if(!nSamples) return nError;
    return nSamples;
}

/////////////////////////////////////////////////////////////////////////////

#include <math.h>

static __inline float saturate_f(float x) {
#define SATURATE_AT (0.1f)
#define SAT_RANGE_LOW  (1.0f*SATURATE_AT)
#define SAT_RANGE_HIGH (1.0f-SAT_RANGE_LOW)
    if(x > SAT_RANGE_LOW) {
        x =  (SAT_RANGE_LOW + SAT_RANGE_HIGH * tanhf((( x)-SAT_RANGE_LOW)*(1.0f/SAT_RANGE_HIGH)));
    } else if(x < -(SAT_RANGE_LOW)) {
        x = -(SAT_RANGE_LOW + SAT_RANGE_HIGH * tanhf(((-x)-SAT_RANGE_LOW)*(1.0f/SAT_RANGE_HIGH)));
    }
    return x;
}

void CPlayer::SoftSaturate(float *buffer, int nValues) {
    float *buffer_end = buffer + nValues;
    for(; buffer < buffer_end; buffer++) {
        *buffer = saturate_f(*buffer);
    }
}

void CPlayer::ConvertFloatTo16bit(const float *src, signed short *dest, int nValues) {
    const float *src_end = src + nValues;
    while(src < src_end) {
        int x = (int)((*src++) * COEF_FLOAT_TO_16BIT);
        if(x > ( 32767)) x = ( 32767);
        if(x < (-32768)) x = (-32768);
        *dest++ = x;
    }
}

void CPlayer::Convert16bitToFloat(const signed short *src, float *dest, int nValues) {
    const signed short *src_end = src + nValues;
    while(src < src_end) {
        *dest++ = (*src++) * COEF_16BIT_TO_FLOAT;
    }
}

void CPlayer::ScaleFloat(float *buf, int nValues, float coef) {
    float *buf_end = buf + nValues;
    for(; buf < buf_end; buf++) {
        *buf *= coef;
    }
}

/////////////////////////////////////////////////////////////////////////////

float CPlayer::GetVolumeScale(bool bForceAlbum) {
    g_pPlugin->LockOptions();
    BOOL bReplayGainEnable  = g_pPlugin->opt_bReplayGainEnable;
    BOOL bReplayGainAlbum   = g_pPlugin->opt_bReplayGainAlbum;
    BOOL bClipProtectEnable = g_pPlugin->opt_bClipProtectEnable;
    BOOL bClipProtectAlbum  = g_pPlugin->opt_bClipProtectAlbum;
    double dGlobalVolume    = g_pPlugin->opt_dGlobalVolume; // preamp
    g_pPlugin->UnlockOptions();

    if(bForceAlbum) bReplayGainAlbum = TRUE;

    double scale;
    //
    // Pick scale factor:
    // 1. RG Album gain, if enabled and present
    // 2. RG Track gain, if enabled and present
    // 3. Manual volume
    //
    if(m_bHasRGAlbumVolume && bReplayGainEnable && bReplayGainAlbum) {
        scale = m_dRGAlbumVolume;
    } else if(m_bHasRGTrackVolume && bReplayGainEnable) {
        scale = m_dRGTrackVolume;
    } else {
        scale = m_dManualVolume;
    }
    scale *= dGlobalVolume;

    //
    // Clip protect
    //
    if(m_bHasRGAlbumMaxVol && bClipProtectEnable && bClipProtectAlbum) {
        if(scale > m_dRGAlbumMaxVol) scale = m_dRGAlbumMaxVol;
    } else if(m_bHasRGTrackMaxVol && bClipProtectEnable) {
        if(scale > m_dRGTrackMaxVol) scale = m_dRGTrackMaxVol;
    }

    return scale;
}

/////////////////////////////////////////////////////////////////////////////

int CPlayer::GetEndingSilentSamples(const float *buf, int nSamples, int nChannels, float threshold) {
    if(nChannels < 1) return nSamples;
    if(threshold < 0) threshold = -threshold;
    float n_threshold = -threshold;
    int i;
    for(i = nSamples * nChannels; i > 0; i--) {
        float s = buf[i - 1];
        if(s >   threshold) break;
        if(s < n_threshold) break;
    }
    return nSamples - (i / nChannels);
}

/////////////////////////////////////////////////////////////////////////////
