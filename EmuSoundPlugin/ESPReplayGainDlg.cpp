// ESPReplayGainDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmuSoundPlugin.h"
#include "ESPReplayGainDlg.h"


// CESPReplayGainDlg dialog

IMPLEMENT_DYNAMIC(CESPReplayGainDlg, CDialog)
CESPReplayGainDlg::CESPReplayGainDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESPReplayGainDlg::IDD, pParent)
{
}

CESPReplayGainDlg::~CESPReplayGainDlg()
{
}

void CESPReplayGainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CESPReplayGainDlg, CDialog)
END_MESSAGE_MAP()


// CESPReplayGainDlg message handlers
