/*
** Winamp 2 API
**
** Input/Output plugins
*/

#ifdef __cplusplus
extern "C" {
#endif

#define WM_WA_IPC      (WM_USER)

#define IPC_GETVERSION 0

// post this to the main window at end of file (after playback as stopped)
#define WM_WA_MPEG_EOF (WM_USER+2)

#define OUT_VER 0x10

#define WINAMP_CALL __cdecl

typedef struct 
{
	int version;				// module version (OUT_VER)
	const char *description;	// description of module, with version string
	int id;						// module id. each input module gets its own. non-nullsoft modules should
								// be >= 65536. 

	HWND hMainWindow;			// winamp's main window (filled in by winamp)
	HINSTANCE hDllInstance;		// DLL instance handle (filled in by winamp)

	void (WINAMP_CALL *Config)(HWND hWndParent); // configuration dialog 
	void (WINAMP_CALL *About)(HWND hWndParent);  // about dialog

	void (WINAMP_CALL *Init)();				// called when loaded
	void (WINAMP_CALL *Quit)();				// called when unloaded

	int (WINAMP_CALL *Open)(int samplerate, int numchannels, int bitspersamp, int bufferlenms, int prebufferms); 
					// returns >=0 on success, <0 on failure
					// NOTENOTENOTE: bufferlenms and prebufferms are ignored in most if not all output plug-ins. 
					//    ... so don't expect the max latency returned to be what you asked for.
					// returns max latency in ms (0 for diskwriters, etc)
					// bufferlenms and prebufferms must be in ms. 0 to use defaults. 
					// prebufferms must be <= bufferlenms

	void (WINAMP_CALL *Close)();	// close the ol' output device.

	int (WINAMP_CALL *Write)(char *buf, int len);	
					// 0 on success. Len == bytes to write (<= 8192 always). buf is straight audio data. 
					// 1 returns not able to write (yet). Non-blocking, always.

	int (WINAMP_CALL *CanWrite)();	// returns number of bytes possible to write at a given time. 
						// Never will decrease unless you call Write (or Close, heh)

	int (WINAMP_CALL *IsPlaying)(); // non0 if output is still going or if data in buffers waiting to be
						// written (i.e. closing while IsPlaying() returns 1 would truncate the song

	int (WINAMP_CALL *Pause)(int pause); // returns previous pause state

	void (WINAMP_CALL *SetVolume)(int volume); // volume is 0-255
	void (WINAMP_CALL *SetPan)(int pan); // pan is -128 to 128

	void (WINAMP_CALL *Flush)(int t);	// flushes buffers and restarts output at time t (in ms) 
							// (used for seeking)

	int (WINAMP_CALL *GetOutputTime)(); // returns played time in MS
	int (WINAMP_CALL *GetWrittenTime)(); // returns time written in MS (used for synching up vis stuff)

} Out_Module;

// note: exported symbol is now winampGetInModule2.

#define IN_VER 0x100

typedef struct 
{
	int version;				// module type (IN_VER)
	const char *description;	// description of module, with version string

	HWND hMainWindow;			// winamp's main window (filled in by winamp)
	HINSTANCE hDllInstance;		// DLL instance handle (Also filled in by winamp)

	const char *FileExtensions; // "mp3\0Layer 3 MPEG\0mp2\0Layer 2 MPEG\0mpg\0Layer 1 MPEG\0"
								// May be altered from Config, so the user can select what they want
	
	int is_seekable;			// is this stream seekable? 
	int UsesOutputPlug;			// does this plug-in use the output plug-ins? (musn't ever change, ever :)

	void (WINAMP_CALL *Config)(HWND hwndParent); // configuration dialog
	void (WINAMP_CALL *About)(HWND hwndParent);  // about dialog

	void (WINAMP_CALL *Init)();				// called at program init
	void (WINAMP_CALL *Quit)();				// called at program quit

	void (WINAMP_CALL *GetFileInfo)(char *file, char *title, int *length_in_ms); // if file == NULL, current playing is used
	int (WINAMP_CALL *InfoBox)(char *file, HWND hwndParent);
	
	int (WINAMP_CALL *IsOurFile)(char *fn);	// called before extension checks, to allow detection of mms://, etc
	// playback stuff
	int (WINAMP_CALL *Play)(char *fn);		// return zero on success, -1 on file-not-found, some other value on other (stopping winamp) error
	void (WINAMP_CALL *Pause)();			// pause stream
	void (WINAMP_CALL *UnPause)();			// unpause stream
	int (WINAMP_CALL *IsPaused)();			// ispaused? return 1 if paused, 0 if not
	void (WINAMP_CALL *Stop)();				// stop (unload) stream

	// time stuff
	int (WINAMP_CALL *GetLength)();			// get length in ms
	int (WINAMP_CALL *GetOutputTime)();		// returns current output time in ms. (usually returns outMod->GetOutputTime()
	void (WINAMP_CALL *SetOutputTime)(int time_in_ms);	// seeks to point in stream (in ms). Usually you signal yoru thread to seek, which seeks and calls outMod->Flush()..

	// volume stuff
	void (WINAMP_CALL *SetVolume)(int volume);	// from 0 to 255.. usually just call outMod->SetVolume
	void (WINAMP_CALL *SetPan)(int pan);	// from -127 to 127.. usually just call outMod->SetPan
	
	// in-window builtin vis stuff

	void (WINAMP_CALL *SAVSAInit)(int maxlatency_in_ms, int srate);		// call once in Play(). maxlatency_in_ms should be the value returned from outMod->Open()
	// call after opening audio device with max latency in ms and samplerate
	void (WINAMP_CALL *SAVSADeInit)();	// call in Stop()


	// simple vis supplying mode
	void (WINAMP_CALL *SAAddPCMData)(void *PCMData, int nch, int bps, int timestamp); 
											// sets the spec data directly from PCM data
											// quick and easy way to get vis working :)
											// needs at least 576 samples :)

	// advanced vis supplying mode, only use if you're cool. Use SAAddPCMData for most stuff.
	int (WINAMP_CALL *SAGetMode)();		// gets csa (the current type (4=ws,2=osc,1=spec))
							// use when calling SAAdd()
	void (WINAMP_CALL *SAAdd)(void *data, int timestamp, int csa); // sets the spec data, filled in by winamp


	// vis stuff (plug-in)
	// simple vis supplying mode
	void (WINAMP_CALL *VSAAddPCMData)(void *PCMData, int nch, int bps, int timestamp); // sets the vis data directly from PCM data
											// quick and easy way to get vis working :)
											// needs at least 576 samples :)

	// advanced vis supplying mode, only use if you're cool. Use VSAAddPCMData for most stuff.
	int (WINAMP_CALL *VSAGetMode)(int *specNch, int *waveNch); // use to figure out what to give to VSAAdd
	void (WINAMP_CALL *VSAAdd)(void *data, int timestamp); // filled in by winamp, called by plug-in


	// call this in Play() to tell the vis plug-ins the current output params. 
	void (WINAMP_CALL *VSASetInfo)(int nch, int srate);


	// dsp plug-in processing: 
	// (filled in by winamp, called by input plug)

	// returns 1 if active (which means that the number of samples returned by dsp_dosamples
	// could be greater than went in.. Use it to estimate if you'll have enough room in the
	// output buffer
	int (WINAMP_CALL *dsp_isactive)(); 

	// returns number of samples to output. This can be as much as twice numsamples. 
	// be sure to allocate enough buffer for samples, then.
	int (WINAMP_CALL *dsp_dosamples)(short int *samples, int numsamples, int bps, int nch, int srate);


	// eq stuff
	void (WINAMP_CALL *EQSet)(int on, char data[10], int preamp); // 0-64 each, 31 is +0, 0 is +12, 63 is -12. Do nothing to ignore.

	// info setting (filled in by winamp)
	void (WINAMP_CALL *SetInfo)(int bitrate, int srate, int stereo, int synched); // if -1, changes ignored? :)

	Out_Module *outMod; // filled in by winamp, optionally used :)
} In_Module;

#ifdef __cplusplus
}
#endif
