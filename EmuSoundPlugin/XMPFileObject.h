/*
////////////////////////////////////////////////////////////////////////////////
//
// XMPFileObject - Implementation of FileObject using XMPFILE API
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../MFCUtil/FileObject.h"

#include "XMPlayAPI.h"

//
// Reference-counted wrapper for XMPFILE
//
class XMPFILEWrapper {
public:
    XMPFILEWrapper();
    virtual ~XMPFILEWrapper();

    XMPFILEWrapper* NewRef();
    bool DeleteRef();

private:
    volatile long m_count;
    XMPFILE m_file;
};

class XMPFileObject : public FileObject
{
public:
    XMPFileObject(CStringW path);
    virtual ~Win32FileObject();

    virtual FileObject* CreateNew(CStringW path) const { return new Win32FileObject(path); }

    virtual bool  SameFileAs(const FileObject *f) const;

    virtual bool  Open    (File::MODE mode);
    virtual bool  Close   ();
    virtual INT64 Length  ();
    virtual INT64 Read    (void *buffer, INT64 length);
    virtual INT64 Write   (void *buffer, INT64 length);
    virtual INT64 Seek    (INT64 position, File::METHOD method);
    virtual bool  Truncate();

private:
    HANDLE m_handle;

    bool IsOpen() { return(m_handle != INVALID_HANDLE_VALUE); }
    bool CheckOpen();

};

////////////////////////////////////////////////////////////////////////////////
*/
