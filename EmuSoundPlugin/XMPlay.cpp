////////////////////////////////////////////////////////////////////////////////
//
// XMPlay.cpp - XMPlay native support
//
////////////////////////////////////////////////////////////////////////////////

// TODO list:
// - seeking
// - option saving
// - use XMPFILE?

#include "StdAfx.h"

////////////////////////////////////////////////////////////////////////////////

#include "EmuSoundPlugin.h"
#include "Player.h"

#include "../MFCUtil/UTF8.h"
#include "../MFCUtil/MFCFileObject.h"
#include "../MFCUtil/SafeDB.h"

////////////////////////////////////////////////////////////////////////////////

#include "XMPlayAPI.h"

////////////////////////////////////////////////////////////////////////////////

// this is here to ensure everything is linked in
int XMPlayLink = 0;

////////////////////////////////////////////////////////////////////////////////

XMPIN_FUNCS *XMPlay_functions;

////////////////////////////////////////////////////////////////////////////////
//
// String conversion - uses UTF-8 or ANSI depending on NT or not
//
static bool isWinNT = true;

CStringW XMPlay_StringAtoW(LPCSTR str) {
    if(isWinNT) {
        return Util::StringUTF8toW(str);
    } else {
        return CStringW(str);
    }
}

CStringA XMPlay_StringWtoA(LPCWSTR str) {
    if(isWinNT) {
        return Util::StringWtoUTF8(str);
    } else {
        return CStringA(str);
    }
}

////////////////////////////////////////////////////////////////////////////////

static CPlayer *pPlayer = NULL;
static CCriticalSection csPlayer;

enum { BUFFER_SAMPLES = 576 };

static CDWordArray sample_buffer;
static int         sample_buffer_channels;
static int         sample_buffer_pos;
static int         sample_buffer_max;

////////////////////////////////////////////////////////////////////////////////

static void WINAPI ESPXMP_About(HWND hWndParent) {
    g_pPlugin->About(hWndParent);
}

static void WINAPI ESPXMP_Config(HWND hWndParent) {
    g_pPlugin->Config(hWndParent);
}

////////////////////////////////////////////////////////////////////////////////
//
// check if a file is playable by this plugin
// more thorough checks can be saved for the GetFileInfo and Open functions
// length = size of "buf" memory block
//
static BOOL WINAPI ESPXMP_CheckFile(char *filename, BYTE *buf, DWORD length)
{
    return g_pPlugin->CheckMagic(buf, length) ? TRUE : FALSE;
}

////////////////////////////////////////////////////////////////////////////////

static bool get_xmp_tag(CMusicFileTag *tag, char **xmptag, CMusicFileTag::FIELD field) {
    if(!tag) return false;
    if(!xmptag) return false;
    CStringW valuew;
    if(tag->GetField(valuew, field)) {
        LPWSTR w = valuew.GetBuffer();
        *xmptag = XMPlay_functions->tag.Unicode(w, -1);
        valuew.ReleaseBuffer();
        return true;
    } else {
        return false;
    }
}

static BOOL get_xmp_metadata(File f, float *length, char *tags[7]) {
    CStringW strTitle;
    INT64 nLengthMS = 0;
    CMusicFileTag *tag = g_pPlugin->GetMetaData(f, strTitle, nLengthMS);

    if(length) { *length = (float)(nLengthMS*0.001); }

    if(tag) {
        get_xmp_tag(tag, tags+0, CMusicFileTag::FIELD_TITLE);
        get_xmp_tag(tag, tags+1, CMusicFileTag::FIELD_ARTIST);
        get_xmp_tag(tag, tags+2, CMusicFileTag::FIELD_ALBUM);
        get_xmp_tag(tag, tags+3, CMusicFileTag::FIELD_YEAR);
        get_xmp_tag(tag, tags+4, CMusicFileTag::FIELD_TRACK);
        get_xmp_tag(tag, tags+5, CMusicFileTag::FIELD_GENRE);
        get_xmp_tag(tag, tags+6, CMusicFileTag::FIELD_COMMENT);
        delete tag;
    }

    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
//
// get file info (length & tags);
// ID3v1/v2 & APEv2 tags may already be in the "tags" array (don't free them - XMPlay will if replaced)
//
static BOOL WINAPI ESPXMP_GetFileInfo(char *filename, XMPFILE file, float *length, char *tags[7])
{
//    return get_xmp_metadata(XMPFileObject(XMPlay_StringAtoW(filename), file), length, tags);

    return get_xmp_metadata(MFCFileObject(XMPlay_StringAtoW(filename)), length, tags);
}

// get the tags
// return TRUE to delay the title update when there are no tags (use UpdateTitle to request update when ready)
static BOOL WINAPI ESPXMP_GetTags(char *tags[7])
{
    csPlayer.Lock();
    if(pPlayer) {
        pPlayer->Lock();
        get_xmp_metadata(pPlayer->GetFile(), NULL, tags);
        pPlayer->Unlock();
    }
    csPlayer.Unlock();
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

static void set_gain() {
    double dTrack = 1.0;
    double dAlbum = 1.0;
    csPlayer.Lock();
    if(pPlayer) {
        pPlayer->Lock();
        dTrack = pPlayer->GetVolumeScale();
        dAlbum = pPlayer->GetVolumeScale(true);
        pPlayer->Unlock();
    }
    csPlayer.Unlock();
    XMPlay_functions->SetGain(0, Util::SafeConvertToDB(dTrack));
    XMPlay_functions->SetGain(1, Util::SafeConvertToDB(dAlbum));
}

////////////////////////////////////////////////////////////////////////////////
//
// open a file for playback
// return:  0=failed, 1=success, 2=success and XMPlay can close the file
//
static DWORD WINAPI ESPXMP_Open(char *filename, XMPFILE file)
{
    csPlayer.Lock();
    if(pPlayer) {
        g_pPlugin->Close(pPlayer);
        pPlayer = NULL;
    }
    csPlayer.Unlock();

    g_pPlugin->LockOptions();
    BOOL bShowErrorMessages = g_pPlugin->opt_bShowErrorMessages;
    g_pPlugin->UnlockOptions();

    CStringW strFilename = XMPlay_StringAtoW(filename);
    CStringW strError;
//    pPlayer = g_pPlugin->Open(XMPFileObject(strFilename, file), strError);
    csPlayer.Lock();
    pPlayer = g_pPlugin->Open(MFCFileObject(strFilename), strError);
    BOOL b = (pPlayer != NULL);
    csPlayer.Unlock();

    if(!b) {
        CStringW m = L"Unable to play:\n";
        m += strFilename;
        if(!strError.IsEmpty()) {
            m += L"\n\n";
            m += strError;
        }
        g_pPlugin->ErrorBox(NULL, m); // todo: main xmplay window as parent?
        return 0;
    }

    sample_buffer_channels = 2;
    DWORD dwSR = 44100;
    INT64 n64SampleCount = 3*60*44100;
    csPlayer.Lock();
    if(pPlayer) {
        sample_buffer_channels = pPlayer->GetChannelCount();
        dwSR = pPlayer->GetSampleRate();
        n64SampleCount = pPlayer->GetSampleCount();
    }
    csPlayer.Unlock();
    if(dwSR < 1) dwSR = 1;
    if(n64SampleCount < 0) n64SampleCount = 3*60*dwSR;

    sample_buffer.SetSize(sample_buffer_channels * BUFFER_SAMPLES);
    sample_buffer_pos = 0;
    sample_buffer_max = 0;

    XMPlay_functions->SetLength(((double)n64SampleCount) / ((double)dwSR), TRUE);

    //
    // do replaygain
    //
    set_gain();

    // success (and close)
    return 2;
}

////////////////////////////////////////////////////////////////////////////////
//
// close the file
//
static void WINAPI ESPXMP_Close()
{
    csPlayer.Lock();
    if(pPlayer) {
        g_pPlugin->Close(pPlayer);
        pPlayer = NULL;
    }
    csPlayer.Unlock();
    sample_buffer.SetSize(0);
}

////////////////////////////////////////////////////////////////////////////////
//
// return the sample format
//
static DWORD WINAPI ESPXMP_GetFormat(DWORD *chan, DWORD *res)
{
    DWORD dwSR = 44100;
    DWORD dwCh = 2;
    DWORD dwRes = 2; // 2 bytes per sample
    csPlayer.Lock();
    if(pPlayer) {
        pPlayer->Lock();
        dwSR = pPlayer->GetSampleRate();
        dwCh = pPlayer->GetChannelCount();
        pPlayer->Unlock();
    }
    csPlayer.Unlock();
	if (res) *res = dwRes;
	if (chan) *chan = dwCh;
	return dwSR;
}

////////////////////////////////////////////////////////////////////////////////
//
// get the main panel info text
//
static void WINAPI ESPXMP_GetInfoText(char *format, char *length)
{
    // should be like PSF - 44100hz

    csPlayer.Lock();
    bool valid = (pPlayer != NULL);
    csPlayer.Unlock();
    if(!valid) return;

    CStringW fmt;
    DWORD dwSR = 0;

    csPlayer.Lock();
    if(pPlayer) {
        pPlayer->Lock();
        fmt = pPlayer->GetFormat();
        dwSR = pPlayer->GetSampleRate();
        pPlayer->Unlock();
    }
    csPlayer.Unlock();

    CStringW final;
    final.Format(L"%s - %dhz", LPCWSTR(fmt), dwSR);

    // pray there's room in the buffer...
    strcpy(format, XMPlay_StringWtoA(final));
}

////////////////////////////////////////////////////////////////////////////////
//
// get text for "General" info window
// separate headings and values with a tab (\t), end each line with a carriage-return (\r)
//
static void WINAPI ESPXMP_GetGeneralInfo(char *buf)
{
    if(!buf) return;

//    sprintf(buf, "GetGeneralInfo");
}

// get text for "Message" info window
// separate tag names and values with a tab (\t), and end each line with a carriage-return (\r)
static void WINAPI ESPXMP_GetMessage(char *buf)
{
	if(!buf) return;

    // todo: anything useful to add here?
}

// get some sample data, always floating-point
// count=number of floats to write (not bytes or samples)
// return number of floats written. if it's less than requested, playback is ended...
// so wait for more if there is more to come (use CheckCancel function to check if user wants to cancel)
static DWORD WINAPI ESPXMP_Process(float *buffer, DWORD count)
{
    csPlayer.Lock();
    bool valid = (pPlayer != NULL);
    csPlayer.Unlock();
    if(!valid) return 0;

    int done = 0;

    float *sb_base = (float*)(sample_buffer.GetData());

    for(;;) {
        int n = count - done;
        if(n <= 0) break;

        int s_remain = sample_buffer_max - sample_buffer_pos;
        if(n > s_remain) n = s_remain;

        if(n <= 0) {
            sample_buffer_pos = 0;
            int s = 0;
            bool bOpeningSilence = false;
            INT64 nSilenceRunLength = 0;
            csPlayer.Lock();
            if(pPlayer) {
                s = pPlayer->Read(sb_base, BUFFER_SAMPLES);
                bOpeningSilence = pPlayer->OpeningSilence();
                nSilenceRunLength = pPlayer->SilenceRunLength();
            }
            DWORD dwSR = pPlayer->GetSampleRate();
            csPlayer.Unlock();
            if(s <= 0) {
                //
                // todo: check for error conditions
                //
                break;
            }
            if(s > BUFFER_SAMPLES) s = BUFFER_SAMPLES;
            sample_buffer_max = s * sample_buffer_channels;
            //
            // Re-grab the global options related to silence suppression
            //
            g_pPlugin->LockOptions();
            BOOL bEatingOpeningSilence   = g_pPlugin->opt_bSuppressOpeningSilence;
            BOOL bEatingEndingSilence    = g_pPlugin->opt_bSuppressEndingSilence;
            double dEndingSilenceSeconds = g_pPlugin->opt_dEndingSilenceSeconds;
            g_pPlugin->UnlockOptions();
            INT64 n64EndingSilenceSamples = ((INT64)(dEndingSilenceSeconds * ((double)(dwSR))));
            //
            // Are we eating opening silence?
            //
            if(bEatingOpeningSilence && bOpeningSilence) {
                // todo: loop back and read more
            //
            // Are we trying to detect ending silence?
            //
            } else if(bEatingEndingSilence && (!bOpeningSilence)) {
                if(nSilenceRunLength >= n64EndingSilenceSamples) {
                    // done playing - silence length exceeded
                    break;
                }
            }

        } else {
            memcpy(buffer, sb_base + sample_buffer_pos, sizeof(float) * n);
            sample_buffer_pos += n;
            buffer += n;
            done += n;
        }
    }

	return done;
}

// Get the seeking granularity in seconds
static double WINAPI ESPXMP_GetGranularity()
{
	return 0.1; // seek in units of a 1/10th of a second
}

// Seek to a position (in granularity units)
// return the new position in seconds (-1 = failed)
static double WINAPI ESPXMP_SetPosition(DWORD pos)
{
	double time = ((double)(pos)) * ESPXMP_GetGranularity(); // convert "pos" into seconds

    // todo: actually make this work

    return time;
}

////////////////////////////////////////////////////////////////////////////////
//
// plugin function table
//
static XMPIN xmpin = {
	XMPIN_FLAG_NOXMPFILE, //XMPIN_FLAG_CANSTREAM, // can stream from 'net, and only using XMPFILE
	"", // description banner - fill in later
	"", // extensions - fill in later
	ESPXMP_About,
	ESPXMP_Config,
	ESPXMP_CheckFile,
	ESPXMP_GetFileInfo,
	ESPXMP_Open,
	ESPXMP_Close,
	ESPXMP_GetFormat,
	NULL, // SetFormat doesn't apply (XMPlay gets the format, not sets it)
	ESPXMP_GetTags,
	ESPXMP_GetInfoText,
	ESPXMP_GetGeneralInfo,
	ESPXMP_GetMessage,
	ESPXMP_SetPosition,
	ESPXMP_GetGranularity,
	NULL, // GetBuffering only applies when using your own file routines
	ESPXMP_Process,
	NULL, // WriteFile, see GetBuffering
	NULL, // don't have any "Samples"
	NULL, // nor any sub-songs
};

////////////////////////////////////////////////////////////////////////////////

static CStringA s_banner;
static CByteArray s_extensions;

////////////////////////////////////////////////////////////////////////////////
//
// get the plugin's XMPIN interface
//
static XMPIN* WINAPI get_xmp_interface(DWORD face, XMPIN_FUNCS *funcs)
{
	if(face != XMPIN_FACE) return NULL;

    g_pPlugin->InvokeFrom(ESP_XMPLAY);

    //
	// useful to know when doing your own file opening
    //
	isWinNT = (((INT32)GetVersion()) >= 0);

    //
    // Create persistent app banner name
    //
    if(s_banner.IsEmpty()) {
        s_banner = XMPlay_StringWtoA(g_pPlugin->GetBanner());
        xmpin.name = LPCSTR(s_banner);
    }

    //
    // Create persistent extension data
    //
    if(s_extensions.IsEmpty()) {
        LPCWSTR *ex = g_pPlugin->GetSupportedExtensions();

        CStringA str_banner = g_pPlugin->GetShortBanner();
        CStringA str_exlist;

        for(; *ex; ex += 2) {
            CStringW str_exstar_w = ex[0];
            str_exstar_w.MakeLower();
            str_exlist += XMPlay_StringWtoA(str_exstar_w);
            if(ex[2]) str_exlist += "/";
        }

        LPCSTR banner = str_banner;
        LPCSTR exlist = str_exlist;
        int banner_l = strlen(banner);
        int exlist_l = strlen(exlist);

        s_extensions.SetSize(2 + banner_l + exlist_l);
        char *dest = (char*)s_extensions.GetData();

        strcpy(dest, banner);
        strcpy(dest + banner_l + 1, exlist);

        xmpin.exts = dest;
    }

	XMPlay_functions = funcs;

	return &xmpin;
}

////////////////////////////////////////////////////////////////////////////////
//
// Ugly hack to get the right decoration on XMPIN_GetInterface...
//
extern "C" __declspec(dllexport) __declspec(naked) void __cdecl XMPIN_GetInterface() {
    __asm jmp get_xmp_interface;
}

////////////////////////////////////////////////////////////////////////////////
