#pragma once

#include "Resampler.h"

#include "../MFCUtil/File.h"

class CPlayer
{
public:
	CPlayer* CreateLink(CPlayer* pList);
	CPlayer* DeleteLink(CPlayer* pList);
	void Unlock();
	void Lock();

	virtual int GetKBPS();

    virtual CStringW GetFormat() = 0;

    virtual void ReloadTag();

    DWORD GetSampleRate();
    DWORD GetChannelCount();

    CStringW GetLastError() { return m_strLastError; }

    INT64 Tell();

    BOOL Open();
    void Close();

    void ChangeManualVolume(double dVolume);
    INT64 IncrementalSeek(INT64 n64Target, INT64 n64MaxProcess);
    int Read(float *buffer, int samples);
    INT64 GetSampleCount();
    BOOL Rewind();

    File GetFile() const { return m_openfile; }
    CStringW GetPath() const { return m_openfile.GetPath(); }

    CPlayer *m_pNext;
    CPlayer *m_pPrev;

    CPlayer(File f);
    virtual ~CPlayer();

    static void __fastcall SoftSaturate(float *buffer, int nValues);
    static void __fastcall ConvertFloatTo16bit(const float *src, signed short *dest, int nValues);
    static void __fastcall Convert16bitToFloat(const signed short *src, float *dest, int nValues);
    static void __fastcall ScaleFloat(float *buf, int nValues, float coef);
    static int  __fastcall GetEndingSilentSamples(const float *buf, int nSamples, int nChannels, float threshold = 0.001f); // default is -60dB

    //
    // Get volume scale using current options (global, RG enable, etc.)
    //
    // You can also force it into album mode which is useful for reporting gain to XMPlay
    //
    float GetVolumeScale(bool bForceAlbum = false);

    bool OpeningSilence() const { return m_bOpeningSilence; }
    INT64 SilenceRunLength() const { return m_n64SilenceRunLength; }

protected:
    virtual BOOL RawOpen();
    virtual void RawClose();
    virtual int RawRead(float *buffer, int nSamples);

    DWORD m_dwInternalSampleRate;
    DWORD m_dwOutputSampleRate;
    DWORD m_dwChannels;
    CStringW m_strLastError;

    double m_dSongLength;
    double m_dSongFade;

    double m_dManualVolume;
    BOOL   m_bHasRGTrackVolume;
    BOOL   m_bHasRGAlbumVolume;
    BOOL   m_bHasRGTrackMaxVol;
    BOOL   m_bHasRGAlbumMaxVol;
    double m_dRGTrackVolume;
    double m_dRGAlbumVolume;
    double m_dRGTrackMaxVol;
    double m_dRGAlbumMaxVol;

private:
    INT64 m_n64SamplePosition;
	int m_nFilterPoints;
	CResampler m_resampler;

    INT64 m_n64SilenceRunLength;
    bool  m_bOpeningSilence;

    int ReadEnvelope(float *buffer, int samples);
	int RawReadAtOutputRate(float *buffer, int nSamples);
	CCriticalSection m_cs;

    File m_openfile;

    CCriticalSection m_csPathVolume;
    void ComputeLengths(INT64 &n64Fade, INT64 &n64Silence, INT64 &n64End);
    int ReadWithVolumeSlope(float *buffer, int samples, float vol_a, float vol_b);

    CDWordArray m_aResampleBuffer;
};
