//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ESPResource.rc
//
#define IDD_ESP_CONFIG_ERROR            10000
#define IDD_ESP_CONFIG_GENERAL          10001
#define IDD_ESP_CONFIG_OUTPUT           10002
#define IDD_ESP_REPLAYGAIN              10103
#define IDC_ESP_STRICT                  11000
#define IDC_ESP_RESAMPLERATE            11000
#define IDC_ESP_SHOWERROR               11001
#define IDC_ESP_COMBO_REPLAYGAIN        11001
#define IDC_ESP_FADE                    11002
#define IDC_ESP_COMBO_CLIPPROTECT       11002
#define IDC_ESP_LENGTH                  11003
#define IDC_ESP_OPENINGSILENCE          11004
#define IDC_ESP_STOPSILENCE             11005
#define IDC_ESP_STOPSECONDS             11006
#define IDC_ESP_PRIORITY                11007
#define IDC_ESP_PRIORITYNAME            11008
#define IDC_ESP_INDEFINITE              11009
#define IDC_ESP_VOLUMESLIDER            11010
#define IDC_ESP_FRAME_PRIORITY          11010
#define IDC_ESP_SOFTSATURATE            11011
#define IDC_ESP_RESAMPLE                11012
#define IDC_ESP_VOLUME                  11013
#define IDC_ESP_LABEL_REPLAYGAIN        11014
#define IDC_ESP_LABEL_CLIPPROTECT       11015
#define IDC_ESP_NOTE_XMPLAY             11016
#define IDC_ESP_NOTE_VOLUME             11017
#define IDC_PROGRESS1                   11018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        10104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         11019
#define _APS_NEXT_SYMED_VALUE           10101
#endif
#endif
