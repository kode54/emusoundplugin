#if !defined(AFX_ESPCONFIGGENERALPAGE_H__D693F17F_842B_42EA_BB0F_4FFCAB5D4B0F__INCLUDED_)
#define AFX_ESPCONFIGGENERALPAGE_H__D693F17F_842B_42EA_BB0F_4FFCAB5D4B0F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ESPConfigGeneralPage.h : header file
//

#include "EmuSoundPlugin.h"

/////////////////////////////////////////////////////////////////////////////
// CESPConfigGeneralPage dialog

class CESPConfigGeneralPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CESPConfigGeneralPage)

// Construction
public:
	void BindToPlugin(CEmuSoundPlugin *pPlugin);
	CESPConfigGeneralPage();
	~CESPConfigGeneralPage();

// Dialog Data
	//{{AFX_DATA(CESPConfigGeneralPage)
	enum { IDD = IDD_ESP_CONFIG_GENERAL };
	CButton	m_cStopSilence;
	CEdit	m_cStopSeconds;
	CButton	m_cOpeningSilence;
	CEdit	m_cLength;
	CButton	m_cIndefinite;
	CEdit	m_cFade;
	CStatic	m_cPriorityName;
	CSliderCtrl	m_cPriority;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CESPConfigGeneralPage)
	public:
	virtual void OnOK();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CESPConfigGeneralPage)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CEmuSoundPlugin* m_pPlugin;
	void SyncPriorityNameToSlider();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ESPCONFIGGENERALPAGE_H__D693F17F_842B_42EA_BB0F_4FFCAB5D4B0F__INCLUDED_)
