// Resampler.cpp: implementation of the CResampler class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "Resampler.h"

#include <math.h>

#define COEF_16BIT_TO_FLOAT (0.000030517578125f)
#define COEF_FLOAT_TO_16BIT (32768.0f)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CResampler::CResampler()
{
  m_pfBlackfilt = NULL; // sign that we've been initialized properly
  m_pfInputQueue = NULL;
  m_nInputQueueSize = 0;
  m_nInputQueueStart = 0;
  m_nInputQueueSamples = 0;
}

CResampler::~CResampler()
{
  if(m_pfBlackfilt) free(m_pfBlackfilt);
  if(m_pfInputQueue) free(m_pfInputQueue);
}

/////////////////////////////////////////////////////////////////////////////

#define MAXBPC (320)

#define MY_PI (3.14159265358979323846)

/////////////////////////////////////////////////////////////////////////////

static DWORD gcd(DWORD i, DWORD j) {
  return j ? gcd(j, i % j) : i;
}

/////////////////////////////////////////////////////////////////////////////

// resampling via FIR filter, blackman window
static double blackman(double x, double fcn, int l) {
  double bkwn,x2;
  double wcn = (MY_PI * fcn);

  x /= ((double)l);
  if (x<0.0) x=0.0;
  if (x>1.0) x=1.0;
  x2 = x - 0.5;

  bkwn = 0.42 - 0.5*cos(2.0*x*MY_PI)  + 0.08*cos(4.0*x*MY_PI);
  if (fabs(x2)<1e-9) {
    return wcn/MY_PI;
  } else {
    return  (  bkwn*sin(l*wcn*x2)  / (MY_PI*l*x2)  );
  }
}

/////////////////////////////////////////////////////////////////////////////

BOOL CResampler::Redesign(int nSRIn, int nSROut, int nChannels, int nFilterLength)
{
    if(nSRIn <= 0 || nSROut <= 0) {
        m_strLastError = L"Invalid sample rate";
        return FALSE;
    }
    if(nChannels < 1) {
        m_strLastError = L"Invalid channel count";
        return FALSE;
    }
    if(nFilterLength < 1) {
        m_strLastError = L"Invalid filter length";
        return FALSE;
    }

    m_nSRIn = nSRIn;
    m_nSROut = nSROut;

    ResetQueue();

    m_nChannels = nChannels;

  /* number of convolution functions to pre-compute */
  m_nBPC = nSROut / gcd(nSROut, nSRIn);
  if(m_nBPC > MAXBPC) m_nBPC = MAXBPC;

  // resample ratio: input over output
  double resample_ratio = ((double)(nSRIn)) / ((double)(nSROut));

  // intratio means we will never have a fractional phase
  BOOL bIntratio = ((nSRIn % nSROut) == 0);
//( fabs(resample_ratio - floor(0.5+resample_ratio)) < 0.000001 );
  double fcn = 1.00 / resample_ratio;
  if(fcn > 1.00) fcn = 1.00;

//  m_nFilterLength = 31; // might also be 7
  m_nFilterLength = nFilterLength;

  if((m_nFilterLength % 2) == 0) { m_nFilterLength--; } // must be odd
  if(bIntratio) { m_nFilterLength++; } // unless resample_ratio=int, in which case it must be even

  if(m_pfBlackfilt) free(m_pfBlackfilt);
  m_pfBlackfilt = (float*) malloc(sizeof(float) * (m_nFilterLength+1) * ((2*m_nBPC)+1));

  int i, j;
  /* precompute blackman filter coefficients */
  for(j = 0; j <= 2*m_nBPC; j++) {
    double dSum = 0.0;
    double dOfs = 0.0;
    //
    // for int ratio (never a fractional phase),
    // cheat and make all offsets zero
    //
    if(!bIntratio) {
      dOfs = ((double)(j - m_nBPC)) / ((double)(2.0 * ((double)(m_nBPC))));
    }
    float *pfFilt = m_pfBlackfilt + (m_nFilterLength+1)*j;
    for(i = 0; i <= m_nFilterLength; i++) {
      double dCoef = blackman(((double)i) - dOfs, fcn, m_nFilterLength);
      pfFilt[i] = (float)dCoef;
      dSum += dCoef;
    }
	  for(i = 0; i <= m_nFilterLength; i++) {
	    pfFilt[i] /= (float)dSum;
    }
  }
  //
  // create this handy value to convert from fractional phase to 2bpc
  //
  m_fPhaseTo2BPC = ((float)(2*m_nBPC)) / ((float)(nSROut));

  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
//
// must be able to handle "input buffer has negative sample count"
// meaning, "must eat this many samples before writing again"
//
// can take NULL for buffer
//
int CResampler::Write(const float *buffer, int nSamples)
{
  if(!m_pfBlackfilt) {
    m_strLastError = L"Not initialized";
    return -1;
  }

  int nTotal = nSamples * m_nChannels;

  //
  // sample eating is handled here
  //
  if(m_nInputQueueSamples < 0) {
    int nEat = -m_nInputQueueSamples;
    if(nEat > nTotal) nEat = nTotal;
    nTotal -= nEat;
    if(buffer) buffer += nEat;
    m_nInputQueueSamples += nEat;
  }
  if(!nTotal) return nSamples;

  //
  //
  //

  int nAvailableSamples = m_nInputQueueSize - m_nInputQueueSamples;
  if(nTotal > nAvailableSamples) {
    GrowQueue(nTotal - nAvailableSamples);
  }

  int i;
  int p = m_nInputQueueStart + m_nInputQueueSamples;
  for(i = 0; i < nTotal; i++) {
    while(p >= m_nInputQueueSize) p -= m_nInputQueueSize;
    float v = 0.0f;
    if(buffer) { v = (*buffer++); }
    m_pfInputQueue[p] = v;
    p++;
  }
  m_nInputQueueSamples += nTotal;

  return nSamples;
}

/////////////////////////////////////////////////////////////////////////////
//
// can take NULL for buffer
//
int CResampler::Read(float *buffer, int nSamples)
{
  if(!m_pfBlackfilt) {
    m_strLastError = L"Not initialized";
    return -1;
  }
  if(!m_pfInputQueue) return 0;

  int nSamplesNeededPerLoop = m_nChannels * (m_nFilterLength + 1);

  int k;
  for(k = 0; k < nSamples; k++) {
    // make sure there's enough room in the queue
    if(nSamplesNeededPerLoop > m_nInputQueueSamples) break;

    if(buffer) {
      /* find the closest precomputed window for this offset: */
      int joff = (int)floorf((m_fPhaseTo2BPC*((float)(m_nInputPhase))) + 0.5f);
      float *pfFilt = m_pfBlackfilt + (m_nFilterLength+1) * joff;
      int ch;
      for(ch = 0; ch < m_nChannels; ch++) {
        // get the sample pointer
        int p = m_nInputQueueStart + ch;
        float xvalue = 0.0f;
        int i;
        for(i = 0; i <= m_nFilterLength; i++) {
          xvalue += (*pfFilt++) * (m_pfInputQueue[p]);
          p += m_nChannels;
          if(p >= m_nInputQueueSize) { p -= m_nInputQueueSize; }
        }
        *buffer = xvalue;
//        *buffer = m_pfInputQueue[m_nInputQueueStart];

        buffer++;
      }
    }

    m_nInputPhase += m_nSRIn;
    while(m_nInputPhase >= m_nSROut) {
      m_nInputQueueStart += m_nChannels;
      m_nInputQueueSamples -= m_nChannels;
      m_nInputPhase -= m_nSROut;
    }
    while(m_nInputQueueStart >= m_nInputQueueSize) {
      m_nInputQueueStart -= m_nInputQueueSize;
    }

  }

  // return the number samples created at the new samplerate
  return k;

}

/////////////////////////////////////////////////////////////////////////////

void CResampler::GrowQueue(int nGrow)
{
  if(nGrow <= 0) return;
  m_pfInputQueue = (float*) realloc(m_pfInputQueue, sizeof(float) * (m_nInputQueueSize + nGrow));

  // move upper half of queue if necessary
  if((m_nInputQueueStart + m_nInputQueueSamples) > m_nInputQueueSize) {
    memmove(
      m_pfInputQueue + m_nInputQueueStart + nGrow,
      m_pfInputQueue + m_nInputQueueStart,
      sizeof(float) * (m_nInputQueueSize - m_nInputQueueStart)
    );
    m_nInputQueueStart += nGrow;
  }

  m_nInputQueueSize += nGrow;
}

/////////////////////////////////////////////////////////////////////////////

void CResampler::ResetQueue()
{
  if(m_pfInputQueue) {
    free(m_pfInputQueue);
    m_pfInputQueue = NULL;
  }
  m_nInputQueueSize = 0;
  m_nInputQueueStart = 0;
  m_nInputQueueSamples = 0;

  m_nInputPhase = 0;
}

/////////////////////////////////////////////////////////////////////////////

void CResampler::Reset()
{
  ResetQueue();
}

/////////////////////////////////////////////////////////////////////////////
