////////////////////////////////////////////////////////////////////////////////
//
// Winamp2.cpp - Winamp 2.x support
//
////////////////////////////////////////////////////////////////////////////////

// TODO list:
// - replaygain scanning/details
// - verify all the new gain stuff takes effect when it should

#include "StdAfx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////////////
//
// Define this to include the ANSI Media Library tag support...
//
#define WINAMP_ANSI_ML

////////////////////////////////////////////////////////////////////////////////

#include "EmuSoundPlugin.h"
#include "Player.h"

#include "Winamp2API.h"

#include "../MFCUtil/NumString.h"
#include "../MFCUtil/File.h"
#include "../MFCUtil/MFCFileObject.h"

// this is here to ensure everything is linked in
int Winamp2Link = 0;

////////////////////////////////////////////////////////////////////////////////

static CPlayer *pPlayer = NULL;
static CCriticalSection csPlayer;

static LONG s_lSeekMS = -1;
static volatile LONG s_lPause = 0;
static LONG s_lNowMS = -1;

static CCriticalSection csStr;
static CStringW strDecodeError;
static CStringW strDecodeErrorPath;

////////////////////////////////////////////////////////////////////////////////
//
// Prototypes for Winamp module struct
//
static void WINAMP_CALL config(HWND hWndParent);
static void WINAMP_CALL about(HWND hWndParent);
static void WINAMP_CALL init(void);
static void WINAMP_CALL quit(void);
static int  WINAMP_CALL isourfile(char *lpszPathName);
static int  WINAMP_CALL play(char *lpszPathName);
static void WINAMP_CALL pause(void);
static void WINAMP_CALL unpause(void);
static int  WINAMP_CALL ispaused(void);
static void WINAMP_CALL stop(void);
static int  WINAMP_CALL getlength(void);
static int  WINAMP_CALL getoutputtime(void);
static void WINAMP_CALL setoutputtime(int nTimeMS);
static void WINAMP_CALL setvolume(int nVolume);
static void WINAMP_CALL setpan(int nPan);
static int  WINAMP_CALL info(char *lpszPathName, HWND hWndParent);
static void WINAMP_CALL getfileinfo(char *lpszPathName, char *lpszTitle, int *pnLengthMS);
static void WINAMP_CALL eq_set(int on, char data[10], int preamp);

////////////////////////////////////////////////////////////////////////////////
//
// The Winamp module struct
//
static In_Module mod = {
	IN_VER,
	"", // description - fill in by GetInModule2
	0, // hMainWindow
	0, // hDllInstance
	"", // FileExtensions - fill in by GetInModule2
	1, // is_seekable
	1, // uses output
	config,
	about,
	init,
	quit,
	getfileinfo,
	info,
	isourfile,
	play,
	pause,
	unpause,
	ispaused,
	stop,
	getlength,
	getoutputtime,
	setoutputtime,
	setvolume,
	setpan,
	0,0,0,0,0,0,0,0,0, // vis stuff
	0,0, // dsp
	eq_set,
	NULL,		// setinfo
	0 // out_mod
};

static CStringA s_banner;
static CByteArray s_extensions;

extern "C" __declspec(dllexport) In_Module* WINAMP_CALL winampGetInModule2(void) {
    //
    // Create persistent app banner name
    //
    if(s_banner.IsEmpty()) {
        s_banner = g_pPlugin->GetBanner();
        mod.description = LPCSTR(s_banner);
    }

    //
    // Create persistent extension data
    //
    if(s_extensions.IsEmpty()) {
        LPCWSTR *ex = g_pPlugin->GetSupportedExtensions();

        for(; *ex; ex += 2) {
            CStringW exstar_ws = ex[0];
            CStringW exname_ws = ex[1];

            exstar_ws.MakeUpper();

            exname_ws += L" (*." + exstar_ws + L")";

            CStringA exstar_s = exstar_ws;
            CStringA exname_s = exname_ws;
            LPCSTR   exstar   = exstar_s;
            LPCSTR   exname   = exname_s;

            int pos_0 = s_extensions.GetSize();
            int pos_1 = pos_0 + strlen(exstar) + 1;
            int pos_2 = pos_1 + strlen(exname) + 1;
            s_extensions.SetSize(pos_2);

            char *b = (char*)s_extensions.GetData();
            strcpy(b + pos_0, exstar);
            strcpy(b + pos_1, exname);
        }

        // add final terminator
        s_extensions.Add(0);

        mod.FileExtensions = (char*)s_extensions.GetData();
    }

    return &mod;
}

////////////////////////////////////////////////////////////////////////////////

#define BPS 16

////////////////////////////////////////////////////////////////////////////////

static LPTSTR SeekToFileName(LPTSTR lpszFile)
{
  LPTSTR r = lpszFile;
  for(; *lpszFile; lpszFile++) {
    switch(*lpszFile) {
    case '/':
    case '\\':
      r = lpszFile + 1;
      break;
    }
  }
  return r;
}

// Obtain the PLUGIN.INI path/filename
CStringW __fastcall Winamp2PluginINIGetFilename(void) {
    TCHAR buf[MAX_PATH + 1];
    memset(buf, 0, sizeof(buf));
    GetModuleFileName(mod.hDllInstance, buf, MAX_PATH);
    *(SeekToFileName(buf)) = 0;
    CStringW r = buf;
    r += L"plugin.ini";
    return r;
}

////////////////////////////////////////////////////////////////////////////////
//
// Decode thread prototype
//
//static unsigned long __stdcall DecodeThread(void *pParam);
static UINT AFX_CDECL DecodeThread(LPVOID lpParam);

//static HANDLE hDecodeThread = NULL; // only accessed by main thread
static CWinThread *pDecodeThread = NULL;
static LONG lThreadKill = 0; // always use Interlocked calls to access
static CCriticalSection csDecodeThread;

static void EndDecodeThread_(void) {
  if(!pDecodeThread) return;
  ::InterlockedExchange(&lThreadKill, 1);
  ::WaitForSingleObject(pDecodeThread->m_hThread, INFINITE);
  delete pDecodeThread;
  pDecodeThread = NULL;
  ::InterlockedExchange(&lThreadKill, 0);
}

static void BeginDecodeThread_(void) {
  ::InterlockedExchange(&lThreadKill, 0);

  // starting priority is always normal
  int nPriority = THREAD_PRIORITY_NORMAL;

  pDecodeThread = ::AfxBeginThread(DecodeThread, NULL, nPriority, 0, CREATE_SUSPENDED, NULL);
  if(!pDecodeThread) {
    return;
  }
  pDecodeThread->m_bAutoDelete = FALSE;
  ::ResumeThread(pDecodeThread->m_hThread);
}

static void EndDecodeThread(void) {
  csDecodeThread.Lock();
  EndDecodeThread_();
  csDecodeThread.Unlock();
  ::InterlockedExchange(&s_lNowMS, -1); /* not playing */
}

static void BeginDecodeThread(void) {
  csDecodeThread.Lock();
  EndDecodeThread_();
  ::InterlockedExchange(&s_lNowMS, -1); /* not playing */
  BeginDecodeThread_();
  csDecodeThread.Unlock();
}

////////////////////////////////////////////////////////////////////////////////

static void WINAMP_CALL config(HWND hWndParent) {
    g_pPlugin->Config(hWndParent);
}

static void WINAMP_CALL about(HWND hWndParent) {
    g_pPlugin->About(hWndParent);
}

////////////////////////////////////////////////////////////////////////////////
/*
** Winamp init/quit calls
*/
static void WINAMP_CALL init(void) {
    g_pPlugin->InvokeFrom(ESP_WINAMP2);
    g_pPlugin->LoadOptions();
}

static void MLClearTag();

static void WINAMP_CALL quit(void) {
    EndDecodeThread();
    csPlayer.Lock();
    if(pPlayer) { g_pPlugin->Close(pPlayer); pPlayer = NULL; }
    csPlayer.Unlock();
    g_pPlugin->SaveOptions();

    MLClearTag();
}

////////////////////////////////////////////////////////////////////////////////
/*
** used for detecting URL streams.. unused here.
** strncmp(fn,"http://",7) to detect HTTP streams, etc
*/
static int WINAMP_CALL isourfile(char *lpszPathName) {
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
/*
** Update Winamp's kbps value
*/
static void setkbps(LONG n, BOOL bUpdate) {
    static LONG shown = 0;
    LONG prev = ::InterlockedExchange(&shown, n);
    if(prev == n) return;
    if(bUpdate) mod.SetInfo(n,-1,-1,-1);
}

////////////////////////////////////////////////////////////////////////////////

static int WINAMP_CALL play(char *lpszPathName) {
    int maxlatency;
    DWORD dwSR;
    DWORD dwCh;

    EndDecodeThread();

    csPlayer.Lock();
    if(pPlayer) { g_pPlugin->Close(pPlayer); pPlayer = NULL; }
    csPlayer.Unlock();

    g_pPlugin->LockOptions();
    BOOL bShowErrorMessages = g_pPlugin->opt_bShowErrorMessages;
    g_pPlugin->UnlockOptions();

    BOOL b;

    ::InterlockedExchange(&s_lSeekMS, -1);
    ::InterlockedExchange(&s_lPause, 0);
    ::InterlockedExchange(&s_lNowMS, 0);

    csStr.Lock();
    strDecodeError.Empty();
    strDecodeErrorPath.Empty();
    csStr.Unlock();

    CStringW strError;

    csPlayer.Lock();
    pPlayer = g_pPlugin->Open(MFCFileObject(CStringW(lpszPathName)), strError);
    b = (pPlayer != NULL);
    csPlayer.Unlock();
    if(!b) {
        CStringW m = L"Unable to play:\n";
        m += lpszPathName;
        if(!strError.IsEmpty()) {
            m += L"\n\n";
            m += strError;
        }
        g_pPlugin->ErrorBox(mod.hMainWindow, m);
        goto return_error;
    }

    dwSR = 44100;
    dwCh = 2;
    csPlayer.Lock();
    if(pPlayer) {
        pPlayer->Lock();
        dwSR = pPlayer->GetSampleRate();
        dwCh = pPlayer->GetChannelCount();
        pPlayer->Unlock();
    }
    csPlayer.Unlock();

    maxlatency = mod.outMod->Open(dwSR, dwCh, BPS, -1, -1);
    if(maxlatency < 0) {
        goto return_error;
    }

	// dividing by 1000 for the first parameter of setinfo makes it
	// display 'H'... for hundred.. i.e. 14H Kbps.

    mod.SetInfo(0,dwSR/1000,dwCh,1);
    setkbps(0, FALSE);

    //
    // Initialize vis stuff
    //
    mod.SAVSAInit(maxlatency,dwSR);
    mod.VSASetInfo(dwSR,dwCh);
    //
    // Set the output plug-ins default volume
    //
    mod.outMod->SetVolume(-666);

    //
    // Start the decode thread
    //
    BeginDecodeThread();

//  csWinamp.Unlock();
	return 0; 

return_error:
    csPlayer.Lock();
    if(pPlayer) { g_pPlugin->Close(pPlayer); pPlayer = NULL; }
    csPlayer.Unlock();

//  csWinamp.Unlock();
    return -1;
}

////////////////////////////////////////////////////////////////////////////////

static void WINAMP_CALL pause(void) {
  ::InterlockedExchange(&s_lPause, 1);
  mod.outMod->Pause(1);
}

static void WINAMP_CALL unpause(void) {
  ::InterlockedExchange(&s_lPause, 0);
  mod.outMod->Pause(0);
}

static int WINAMP_CALL ispaused(void) {
//  return (int)(_InterlockedCompareExchange(&s_lPause, 0, 0));
  return (int)(::InterlockedCompareExchange(&s_lPause, 0, 0));
}

////////////////////////////////////////////////////////////////////////////////

static void WINAMP_CALL stop(void) {
//  csWinamp.Lock();
    EndDecodeThread();

    csPlayer.Lock();
    if(pPlayer) { g_pPlugin->Close(pPlayer); pPlayer = NULL; }
    csPlayer.Unlock();

	mod.outMod->Close();
	mod.SAVSADeInit();
    //
    // Check for decode errors
    //
    CStringW s, p;
    csStr.Lock();
    s = strDecodeError;
    p = strDecodeErrorPath;
    strDecodeError.Empty();
    strDecodeErrorPath.Empty();
    csStr.Unlock();

//  csWinamp.Unlock();

    if(!s.IsEmpty()) {
        CStringW m = L"An error occurred while playing:\n";
        m += p;
        m += L"\n\n";
        m += s;
        g_pPlugin->ErrorBox(mod.hMainWindow, m);
    }

}

////////////////////////////////////////////////////////////////////////////////
/*
** Return the length of the currently playing file in ms
*/
static int WINAMP_CALL getlength(void) { 
//  csWinamp.Lock();

  INT64 n64Samples = -1;
  DWORD dwSR       = 44100;
  csPlayer.Lock();
  if(pPlayer) {
    pPlayer->Lock();
    n64Samples = pPlayer->GetSampleCount();
    dwSR       = pPlayer->GetSampleRate();
    pPlayer->Unlock();
  }
  csPlayer.Unlock();
  int l;
  if(n64Samples <= 0) {
    /* completely punt and return something like this if it's unknown */
    l = 3*60*1000;
  } else {
    l = SamplesToMS(n64Samples, dwSR);
  }

//  csWinamp.Unlock();
  return l;
}

////////////////////////////////////////////////////////////////////////////////

static int WINAMP_CALL getoutputtime(void) {
//  csWinamp.Lock();

  INT64 n64Tell = 0;
  DWORD dwSR    = 44100;

  csPlayer.Lock();
  if(pPlayer) {
    pPlayer->Lock();
    n64Tell = pPlayer->Tell();
    dwSR    = pPlayer->GetSampleRate();
    pPlayer->Unlock();
  }
  csPlayer.Unlock();

	int r = (
    SamplesToMS(n64Tell, dwSR) +
    (mod.outMod->GetOutputTime()-mod.outMod->GetWrittenTime())
  );

//  csWinamp.Unlock();
  return r;
}

////////////////////////////////////////////////////////////////////////////////

static void WINAMP_CALL setoutputtime(int nTimeMS) { 
  ::InterlockedExchange(&s_lSeekMS, nTimeMS);
}

////////////////////////////////////////////////////////////////////////////////

static void WINAMP_CALL setvolume(int nVolume) {
//  csWinamp.Lock();
  mod.outMod->SetVolume(nVolume);
//  csWinamp.Unlock();
}

static void WINAMP_CALL setpan(int nPan) {
//  csWinamp.Lock();
  mod.outMod->SetPan(nPan);
//  csWinamp.Unlock();
}

////////////////////////////////////////////////////////////////////////////////
//
// No sync needed here
//
static int WINAMP_CALL info(char *lpszPathName, HWND hWndParent) {
    CStringW strPathName;
    // Currently playing file
	if((!lpszPathName) || (!(*lpszPathName))) {
        csPlayer.Lock();
        if(pPlayer) {
            pPlayer->Lock();
            strPathName = pPlayer->GetPath();
            pPlayer->Unlock();
        }
        csPlayer.Unlock();
    // Some other file
    } else {
        strPathName = lpszPathName;
    }

    g_pPlugin->InfoDialog(MFCFileObject(CStringW(lpszPathName)), hWndParent, &s_lNowMS);
    // no need to sync tag

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Get information about a file
// DO NOT sync this.
//
static void WINAMP_CALL getfileinfo(
    char *lpszPathName,
    char *lpszTitle,
    int *pnLengthMS
) {
    File f;

    // Currently playing file
	if((!lpszPathName) || (!(*lpszPathName))) {
        csPlayer.Lock();
        if(pPlayer) {
            pPlayer->Lock();
            f = pPlayer->GetFile();
            pPlayer->Unlock();
        }
        csPlayer.Unlock();
    // Some other file
    } else {
        f = MFCFileObject(CStringW(lpszPathName));
    }

    INT64 l = 0;
    CStringW strTitleW;

    CMusicFileTag *tag = g_pPlugin->GetMetaData(f, strTitleW, l);
    if(tag) delete tag;

    if(l > 0x7FFFFFFF) l = 0x7FFFFFFF;
    if(pnLengthMS) { *pnLengthMS = (int)l; }

    CStringA strTitleA = strTitleW;
    if(strTitleA.GetLength() > ((MAX_PATH) - 1)) {
        strTitleA = strTitleA.Left((MAX_PATH) - 1);
    }

    strcpy(lpszTitle, strTitleA);
}

////////////////////////////////////////////////////////////////////////////////

static void WINAMP_CALL eq_set(int on, char data[10], int preamp) { }

////////////////////////////////////////////////////////////////////////////////
/*
** Decode thread
*/

/*
** Check for thread kill flag or priority change
*/
static LONG DecodeThreadCheckin(BOOL bDuringPlay) {
  //
  // kill flag
  //
  LONG l = ::InterlockedExchange(&lThreadKill, 0);
  if(l) return l;
  //
  // priority change
  //
  HANDLE h = ::GetCurrentThread();
  int n;
  if(!bDuringPlay) {
    n = g_pPlugin->GetSafeDecodeThreadPriority();
  } else {
    n = g_pPlugin->GetDecodeThreadPriority();
  }
  if(n != ::GetThreadPriority(h)) { ::SetThreadPriority(h, n); }
  //
  // done
  //
  return 0;
}

//static unsigned long __stdcall DecodeThread(void *pParam) {
static UINT AFX_CDECL DecodeThread(LPVOID lpParam) {
    LONG lKill = 0;
//  BOOL b;
    bool bUnsilent = false;
    CStringW strError;
    BOOL bError = FALSE;
    BOOL bDone = FALSE;

    CWordArray  wa2_sample_buffer;
    CDWordArray wa2_sample_buffer_f;
    wa2_sample_buffer  .SetSize(576 * 2 * 2);
    wa2_sample_buffer_f.SetSize(576 * 2    );
    short *sample_buffer   = (short*)wa2_sample_buffer  .GetData();
    float *sample_buffer_f = (float*)wa2_sample_buffer_f.GetData();

    DWORD dwSR = 44100;
    DWORD dwCh = 2;
    csPlayer.Lock();
    if(pPlayer) {
        pPlayer->Lock();
        dwSR = pPlayer->GetSampleRate();
        dwCh = pPlayer->GetChannelCount();
        pPlayer->Unlock();
    }
    csPlayer.Unlock();

  for(;;) {
    if(lKill |= DecodeThreadCheckin(bUnsilent)) goto endthread;

    ::InterlockedExchange(&s_lNowMS, getoutputtime());

    //
    // Showing of the CPU usage handled here
    //
    int kbps = 0;
    csPlayer.Lock();
    if(pPlayer) {
      pPlayer->Lock();
      kbps = pPlayer->GetKBPS();
      pPlayer->Unlock();
    }
    csPlayer.Unlock();
    setkbps(kbps, TRUE);

    //
    // Seeking handled here
    //
    LONG lSeekMS = ::InterlockedExchange(&s_lSeekMS, -1);
    if(lSeekMS >= 0) {
      INT64 n64SeekSamples = MSToSamples(lSeekMS, dwSR);
      //
      // If we're seeking, then we are by definition not done
      //
      bDone = FALSE;
      mod.outMod->Flush(getoutputtime());
      INT64 seekposition = 0;
      csPlayer.Lock();
      if(pPlayer) {
        pPlayer->Lock();
        seekposition = pPlayer->Tell();
        pPlayer->Unlock();
      }
      csPlayer.Unlock();
      for(;;) {
        if(lKill |= DecodeThreadCheckin(FALSE)) goto endthread;
        //
        // If we've reached the seek destination, quit
        //
        if(n64SeekSamples == seekposition) break;

        ::InterlockedExchange(&s_lNowMS, getoutputtime());

        /* Check for a new seek destination */
        LONG lNewSeekMS = ::InterlockedExchange(&s_lSeekMS, -1);
        if(lNewSeekMS >= 0) {
          n64SeekSamples = MSToSamples(lNewSeekMS, dwSR);
        }
        /* Perform the seek incrementally */
        seekposition = -1;
        csPlayer.Lock();
        if(pPlayer) {
          pPlayer->Lock();
          seekposition = pPlayer->IncrementalSeek(n64SeekSamples, 5000);
          if(seekposition < -1) {
            bError = TRUE;
            strError = pPlayer->GetLastError();
          }
          pPlayer->Unlock();
        }
        csPlayer.Unlock();
        if(seekposition < 0) break;
      }
      INT64 n64SamplesFlush = 0;
      csPlayer.Lock();
      if(pPlayer) {
        pPlayer->Lock();
        n64SamplesFlush = pPlayer->Tell();
        pPlayer->Unlock();
      }
      csPlayer.Unlock();
      mod.outMod->Flush(SamplesToMS(n64SamplesFlush, dwSR));
      continue;
		}
    /*
    ** If we're done, post the EOF message and all that
    */
		if(bDone || bError) {
			mod.outMod->CanWrite();
			if(!mod.outMod->IsPlaying()) {
				PostMessage(mod.hMainWindow, WM_WA_MPEG_EOF, 0, 0);
				break;
			}
			Sleep(20);
      continue;
    }
    /*
    ** If we can't write anything out, wait a while then check again
    */
    if(
      (mod.outMod->CanWrite()) <
      (int)(((576*dwCh*(BPS/8))<<(mod.dsp_isactive()?1:0)))
    ) {
      Sleep(20);
      continue;
    }

  readmoreloop:
    if(lKill |= DecodeThreadCheckin(bUnsilent)) goto endthread;

    /*
    ** Get 576 samples
    ** (Winamp 2 works best if you do things 576 samples at a time)
    **
    ** Returns the number of samples actually read
    ** This may ONLY be less than 576 FOR ONE REASON: END OF SONG.
    */
    int l = -1;
    INT64 nSilenceRunLength = 0;
    bool bOpeningSilence = false;
    float volume_scale = 1.0f;
    csPlayer.Lock();
    if(pPlayer) {
      pPlayer->Lock();
      l = pPlayer->Read(sample_buffer_f, 576);
      bOpeningSilence = pPlayer->OpeningSilence();
      nSilenceRunLength = pPlayer->SilenceRunLength();
      if(l < 0) {
        bError = TRUE;
        strError = pPlayer->GetLastError();
      } else if(l > 0) {
        volume_scale = pPlayer->GetVolumeScale();
      }
      pPlayer->Unlock();
    }
    csPlayer.Unlock();
    //
    // Handle error or end-of-track
    //
    if(l <= 0) {
      bDone = TRUE;
      continue;
    }
    /*
    ** Re-grab the global options related to silence suppression
    */
    g_pPlugin->LockOptions();
    BOOL bEatingOpeningSilence   = g_pPlugin->opt_bSuppressOpeningSilence;
    BOOL bEatingEndingSilence    = g_pPlugin->opt_bSuppressEndingSilence;
    double dEndingSilenceSeconds = g_pPlugin->opt_dEndingSilenceSeconds;
    g_pPlugin->UnlockOptions();
    INT64 n64EndingSilenceSamples = ((INT64)(dEndingSilenceSeconds * ((double)(dwSR))));
    bUnsilent = (!bOpeningSilence);
    /*
    ** Are we eating opening silence?
    */
    if(bEatingOpeningSilence && bOpeningSilence) {
      goto readmoreloop; //continue;
    /*
    ** Are we trying to detect ending silence?
    */
    } else if(bEatingEndingSilence && (!bOpeningSilence)) {
      if(nSilenceRunLength >= n64EndingSilenceSamples) {
        bDone = TRUE;
        continue;
      }
    }
    //
    // Amplification
    //
    CPlayer::ScaleFloat(sample_buffer_f, l * dwCh, volume_scale);
    {   BOOL soft = TRUE;
        g_pPlugin->LockOptions();
        soft = g_pPlugin->opt_bSoftSaturate;
        if(g_pPlugin->opt_bClipProtectEnable) soft = FALSE; // disable soft saturation if ClipProtect is on
        g_pPlugin->UnlockOptions();
        if(soft) CPlayer::SoftSaturate(sample_buffer_f, l * dwCh);
    }
    //
    // Float conversion
    //
    CPlayer::ConvertFloatTo16bit(sample_buffer_f, sample_buffer, l * dwCh);
    /*
    ** Write the output
    */
    INT64 tell = 0;
    csPlayer.Lock();
    if(pPlayer) {
      pPlayer->Lock();
      tell = pPlayer->Tell();
      pPlayer->Unlock();
    }
    csPlayer.Unlock();
    int ms = SamplesToMS(tell, dwSR);
    mod.SAAddPCMData ((char*)sample_buffer, dwCh, BPS, ms);
    mod.VSAAddPCMData((char*)sample_buffer, dwCh, BPS, ms);
    if(mod.dsp_isactive()) {
      l = mod.dsp_dosamples(
        sample_buffer,
        l,
        BPS,
        dwCh,
        dwSR
      );
    }
    mod.outMod->Write((char*)sample_buffer, l * (dwCh*(BPS/8)));
    continue;

	}

    //
    // Report errors if any happened
    //
    if(bError) {
        CStringW p;
        csPlayer.Lock();
        if(pPlayer) {
            pPlayer->Lock();
            p = pPlayer->GetPath();
            pPlayer->Unlock();
        }
        csPlayer.Unlock();
        csStr.Lock();
        strDecodeError = strError;
        strDecodeErrorPath = p;
        csStr.Unlock();
    }

endthread:
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//
// Media Library support
//
static CCriticalSection csMediaLibrary;

////////////////////////////////////////////////////////////////////////////////
//
// Cache for media library info
//
static File           s_ml_file;
static CMusicFileTag *s_ml_tag  = NULL;
static INT64          s_ml_length_ms = 0;

static void MLClearTag() {
    if(s_ml_tag ) { delete s_ml_tag ; s_ml_tag  = NULL; }
}

static void MLLoadTag(File f) {
    if(f != s_ml_file) MLClearTag();
    s_ml_file = f;
    if(!s_ml_tag) {
        CStringW strTitle;
        s_ml_tag = g_pPlugin->GetMetaData(s_ml_file, strTitle, s_ml_length_ms);
    }
}

////////////////////////////////////////////////////////////////////////////////
//
// Clear cache if a tag was changed
//
void __fastcall Winamp2ML_NotifyTagChange(File f) {
    if(s_ml_file == f) MLClearTag();
}

////////////////////////////////////////////////////////////////////////////////

static BOOL GetExtendedFileInfoW(File f, LPCSTR field, CStringW &result) {
    result.Empty();

    MLLoadTag(f);

    if(!s_ml_tag) return FALSE;
    if(!stricmp(field, "type")) {
        result = L"0"; // audio
        return TRUE;
    } else if(!stricmp(field, "length"          )) {
        INT64 sec64 = s_ml_length_ms;
        if(sec64 > 0x7FFFFFFF) sec64 = 0x7FFFFFFF;
        result.Format(L"%d", ((int)sec64));
        return TRUE;
    } else if(!stricmp(field, "bitrate")) { return s_ml_tag->GetField(result, CMusicFileTag::FIELD_BITRATE);
    } else if(!stricmp(field, "title"  )) { return s_ml_tag->GetField(result, CMusicFileTag::FIELD_TITLE  );
    } else if(!stricmp(field, "artist" )) { return s_ml_tag->GetField(result, CMusicFileTag::FIELD_ARTIST );
    } else if(!stricmp(field, "album"  )) { return s_ml_tag->GetField(result, CMusicFileTag::FIELD_ALBUM  );
    } else if(!stricmp(field, "year"   )) { return s_ml_tag->GetField(result, CMusicFileTag::FIELD_YEAR   );
    } else if(!stricmp(field, "genre"  )) { return s_ml_tag->GetField(result, CMusicFileTag::FIELD_GENRE  );
    } else if(!stricmp(field, "comment")) { return s_ml_tag->GetField(result, CMusicFileTag::FIELD_COMMENT);
    } else if(!stricmp(field, "track"  )) { return s_ml_tag->GetField(result, CMusicFileTag::FIELD_TRACK  );
    }
    return FALSE;
}

extern "C" __declspec(dllexport) int WINAMP_CALL winampGetExtendedFileInfoW(
    LPCSTR path,
    LPCSTR field,
    LPWSTR result,
    int    result_len // length in wide chars; NOT bytes
) {
    csMediaLibrary.Lock();

    CStringW value;
    BOOL success = GetExtendedFileInfoW(MFCFileObject(CStringW(path)), field, value);
    if(success) {
        LPCWSTR v = value;
        int l; for(l = 0; v[l]; l++) {}
        if(result && (result_len > 0)) {
            if(l > (result_len-1)) l = result_len-1;
            if(l > 0) memcpy(result, v, l*sizeof(result[0]));
            result[l] = 0;
        }
    }
    csMediaLibrary.Unlock();
    return success;
}

#ifdef WINAMP_ANSI_ML
extern "C" __declspec(dllexport) int WINAMP_CALL winampGetExtendedFileInfo(
    LPCSTR path,
    LPCSTR field,
    LPSTR  result,
    int    result_len
) {
    csMediaLibrary.Lock();

    CStringW value;
    BOOL success = GetExtendedFileInfoW(MFCFileObject(CStringW(path)), field, value);
    if(success) {
        CStringA valuea = value;
        LPCSTR v = valuea;
        int l; for(l = 0; v[l]; l++) {}
        if(result && (result_len > 0)) {
            if(l > (result_len-1)) l = result_len-1;
            if(l > 0) memcpy(result, v, l*sizeof(result[0]));
            result[l] = 0;
        }
    }
    csMediaLibrary.Unlock();
    return success;
}
#endif

////////////////////////////////////////////////////////////////////////////////
//
// Set info
//
static void SetExtendedFileInfoW(File f, LPCSTR field, LPCWSTR value) {

    MLLoadTag(f);

    if(!s_ml_tag) s_ml_tag = g_pPlugin->NewMetaData(s_ml_file);
    if(!s_ml_tag) return;

         if(!stricmp(field, "title"  )) { s_ml_tag->SetField(value, CMusicFileTag::FIELD_TITLE  ); }
    else if(!stricmp(field, "artist" )) { s_ml_tag->SetField(value, CMusicFileTag::FIELD_ARTIST ); }
    else if(!stricmp(field, "album"  )) { s_ml_tag->SetField(value, CMusicFileTag::FIELD_ALBUM  ); }
    else if(!stricmp(field, "year"   )) { s_ml_tag->SetField(value, CMusicFileTag::FIELD_YEAR   ); }
    else if(!stricmp(field, "genre"  )) { s_ml_tag->SetField(value, CMusicFileTag::FIELD_GENRE  ); }
    else if(!stricmp(field, "comment")) { s_ml_tag->SetField(value, CMusicFileTag::FIELD_COMMENT); }
    else if(!stricmp(field, "track"  )) { s_ml_tag->SetField(value, CMusicFileTag::FIELD_TRACK  ); }
}

extern "C" __declspec(dllexport) int WINAMP_CALL winampSetExtendedFileInfoW(
    LPCSTR path,
    LPCSTR field,
    LPCWSTR wvalue
) {
    csMediaLibrary.Lock();
    SetExtendedFileInfoW(MFCFileObject(CStringW(path)), field, wvalue);
    csMediaLibrary.Unlock();
    return TRUE;
}

#ifdef WINAMP_ANSI_ML
extern "C" __declspec(dllexport) int WINAMP_CALL winampSetExtendedFileInfo(
    LPCSTR path,
    LPCSTR field,
    LPCSTR value
) {
    csMediaLibrary.Lock();
    SetExtendedFileInfoW(MFCFileObject(CStringW(path)), field, CStringW(value));
    csMediaLibrary.Unlock();
    return TRUE;
}
#endif

extern "C" __declspec(dllexport) int WINAMP_CALL winampWriteExtendedFileInfo(void) {
    csMediaLibrary.Lock();

    BOOL success = FALSE;
    if(s_ml_file && s_ml_tag) {
        success = s_ml_tag->WriteToFile(s_ml_file, FALSE); // no error notification - no need to strictly check
        if(success) {
            g_pPlugin->NotifyTagChange(s_ml_file);
        }
    }

    csMediaLibrary.Unlock();
    return success;
}

////////////////////////////////////////////////////////////////////////////////
