// Resampler.h: interface for the CResampler class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RESAMPLER_H__2319C9D6_438B_4E22_AC70_9CEE9EFD4B04__INCLUDED_)
#define AFX_RESAMPLER_H__2319C9D6_438B_4E22_AC70_9CEE9EFD4B04__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CResampler
{
public:
	void Reset();
	int Read(float *buffer, int nSamples);
	int Write(const float *buffer, int nSamples);
	BOOL Redesign(int nSRIn, int nSROut, int nChannels, int nFilterLength);
	CResampler();
	virtual ~CResampler();

    CStringW GetLastError() const { return m_strLastError; }

private:
    int m_nSRIn;
    int m_nSROut;
	float m_fPhaseTo2BPC;
	void ResetQueue();
	int m_nChannels;
	void GrowQueue(int nGrow);
	float* m_pfInputQueue;
    int m_nInputQueueSize;
    int m_nInputQueueStart;
    int m_nInputQueueSamples;
    int m_nInputPhase;

	CStringW m_strLastError;
	float* m_pfBlackfilt;
	int m_nBPC;
	int m_nFilterLength;
};

#endif // !defined(AFX_RESAMPLER_H__2319C9D6_438B_4E22_AC70_9CEE9EFD4B04__INCLUDED_)
