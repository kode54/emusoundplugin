// ESPConfigOutputPage.cpp : implementation file
//

#include "StdAfx.h"
#include "espresource.h"
#include "ESPConfigOutputPage.h"

#include "../MFCUtil/NumString.h"
#include "../MFCUtil/SafeDB.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CESPConfigOutputPage property page

IMPLEMENT_DYNCREATE(CESPConfigOutputPage, CPropertyPage)

CESPConfigOutputPage::CESPConfigOutputPage() : CPropertyPage(CESPConfigOutputPage::IDD)
{
    m_pPlugin = NULL;

    m_dOriginalGlobalVolume = K14_DEFAULT_VOLUME;
    m_bOriginalSoftSaturate = TRUE;

    m_bOriginalReplayGainEnable  = TRUE;
    m_bOriginalReplayGainAlbum   = FALSE;
    m_bOriginalClipProtectEnable = TRUE;
    m_bOriginalClipProtectAlbum  = FALSE;

    //{{AFX_DATA_INIT(CESPConfigOutputPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CESPConfigOutputPage::~CESPConfigOutputPage()
{
}

void CESPConfigOutputPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESPConfigOutputPage)
	DDX_Control(pDX, IDC_ESP_VOLUMESLIDER, m_cVolumeSlider);
	DDX_Control(pDX, IDC_ESP_VOLUME, m_cVolume);
	DDX_Control(pDX, IDC_ESP_SOFTSATURATE, m_cSoftSaturate);
	DDX_Control(pDX, IDC_ESP_RESAMPLERATE, m_cResampleRate);
	DDX_Control(pDX, IDC_ESP_RESAMPLE, m_cResample);
    DDX_Control(pDX, IDC_ESP_LABEL_REPLAYGAIN, m_cLabelReplayGain);
    DDX_Control(pDX, IDC_ESP_LABEL_CLIPPROTECT, m_cLabelClipProtect);
    DDX_Control(pDX, IDC_ESP_COMBO_REPLAYGAIN, m_cComboReplayGain);
    DDX_Control(pDX, IDC_ESP_COMBO_CLIPPROTECT, m_cComboClipProtect);
    DDX_Control(pDX, IDC_ESP_NOTE_XMPLAY, m_cNoteXMPlay);
    DDX_Control(pDX, IDC_ESP_NOTE_VOLUME, m_cNoteVolume);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CESPConfigOutputPage, CPropertyPage)
	//{{AFX_MSG_MAP(CESPConfigOutputPage)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_ESP_SOFTSATURATE, OnEspSoftsaturate)
	//}}AFX_MSG_MAP
    ON_CBN_SELCHANGE(IDC_ESP_COMBO_REPLAYGAIN, OnCbnSelchangeEspComboReplaygain)
    ON_CBN_SELCHANGE(IDC_ESP_COMBO_CLIPPROTECT, OnCbnSelchangeEspComboClipprotect)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESPConfigOutputPage message handlers

void CESPConfigOutputPage::OnOK() 
{
    if(m_pPlugin) {
        m_pPlugin->LockOptions();

        CString s;
        double d;

//        m_cVolume.GetWindowText(s);
//        d = m_pPlugin->opt_dGlobalVolume;
//        Util::StringToNum(CStringW(s), d, Util::NSFLAG_PLUS | Util::NSFLAG_DB);
//        m_pPlugin->opt_dGlobalVolume = d;

        m_pPlugin->opt_bSoftSaturate = (m_cSoftSaturate.GetCheck() == BST_CHECKED);

        m_pPlugin->opt_bResample = (m_cResample.GetCheck() == BST_CHECKED);

        m_cResampleRate.GetWindowText(s);
        d = m_pPlugin->opt_nResampleFreq;
        Util::StringToNum(CStringW(s), d, 0);
        m_pPlugin->opt_nResampleFreq = (int)d;

        m_pPlugin->UnlockOptions();
    }

	CPropertyPage::OnOK();
}

void CESPConfigOutputPage::OnCancel() 
{
//    bool bReadjust = false;
    //
    // Restore original options from the ones that took effect immediately
    //
    if(m_pPlugin) {
        m_pPlugin->LockOptions();

//        bReadjust = (m_pPlugin->opt_dGlobalVolume != m_dOriginalGlobalVolume);
        m_pPlugin->opt_dGlobalVolume = m_dOriginalGlobalVolume;
        m_pPlugin->opt_bSoftSaturate = m_bOriginalSoftSaturate;

        m_pPlugin->opt_bReplayGainEnable  = m_bOriginalReplayGainEnable;
        m_pPlugin->opt_bReplayGainAlbum   = m_bOriginalReplayGainAlbum;
        m_pPlugin->opt_bClipProtectEnable = m_bOriginalClipProtectEnable;
        m_pPlugin->opt_bClipProtectAlbum  = m_bOriginalClipProtectAlbum;

        m_pPlugin->UnlockOptions();
    }

	CPropertyPage::OnCancel();
}

#define VOLUME_SLIDER_MIN        (-2000)
#define VOLUME_SLIDER_MAX         (2000)
#define VOLUME_SCALE_TO_SLIDER     (100.0)
#define VOLUME_SCALE_FROM_SLIDER     (0.01)

BOOL CESPConfigOutputPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
    //
	// Get options from plugin
    // and ALSO save the cancel values
    //

    if(m_pPlugin) {
        m_pPlugin->LockOptions();
        double dGlobalVolume = m_pPlugin->opt_dGlobalVolume;
        BOOL bSoftSaturate = m_pPlugin->opt_bSoftSaturate;
        BOOL bResample = m_pPlugin->opt_bResample;
        int nResampleFreq = m_pPlugin->opt_nResampleFreq;
        BOOL bRGE = m_pPlugin->opt_bReplayGainEnable;
        BOOL bRGA = m_pPlugin->opt_bReplayGainAlbum;
        BOOL bCPE = m_pPlugin->opt_bClipProtectEnable;
        BOOL bCPA = m_pPlugin->opt_bClipProtectAlbum;
        m_pPlugin->UnlockOptions();

        m_bOriginalSoftSaturate = bSoftSaturate;
        m_dOriginalGlobalVolume = dGlobalVolume;
        m_bOriginalReplayGainEnable  = bRGE;
        m_bOriginalReplayGainAlbum   = bRGA;
        m_bOriginalClipProtectEnable = bCPE;
        m_bOriginalClipProtectAlbum  = bCPA;

        m_cVolume.SetWindowText(CString(Util::NumToString(dGlobalVolume, Util::NSFLAG_PLUS | Util::NSFLAG_DB)));
        m_cSoftSaturate.SetCheck(bSoftSaturate ? BST_CHECKED : BST_UNCHECKED);

        m_cResample.SetCheck(bResample ? BST_CHECKED : BST_UNCHECKED);
        m_cResampleRate.SetWindowText(CString(Util::NumToString(nResampleFreq, 0)));

        m_cComboReplayGain .SetCurSel(bRGE ? (bRGA ? 2 : 1) : 0);
        m_cComboClipProtect.SetCurSel(bCPE ? (bCPA ? 2 : 1) : 0);

        if(!m_pPlugin->IsOptionSupported(&m_pPlugin->opt_bSoftSaturate)) {
            m_cSoftSaturate.ShowWindow(SW_HIDE);
            m_cSoftSaturate.EnableWindow(FALSE);
        }

        if(!m_pPlugin->IsOptionSupported(&m_pPlugin->opt_bReplayGainEnable)) {
            m_cLabelReplayGain.ShowWindow(SW_HIDE);
            m_cLabelReplayGain.EnableWindow(FALSE);
            m_cComboReplayGain.ShowWindow(SW_HIDE);
            m_cComboReplayGain.EnableWindow(FALSE);
        }

        if(!m_pPlugin->WasInvokedFrom(ESP_XMPLAY)) {
            m_cNoteXMPlay.ShowWindow(SW_HIDE);
            m_cNoteXMPlay.EnableWindow(FALSE);
        }

        if(m_pPlugin->IsVolumeImmediate()) {
            m_cNoteVolume.SetWindowText(L"Caution: Volume takes effect immediately");
        } else {
            m_cNoteVolume.SetWindowText(L"Volume takes effect on the next play");
        }
    }

    //
    // Update slider
    //
    m_cVolumeSlider.SetRange(VOLUME_SLIDER_MIN, VOLUME_SLIDER_MAX);

    m_cVolumeSlider.SetTic(0);
    m_cVolumeSlider.SetTic(600);

    m_cVolumeSlider.SetPageSize(600);
    m_cVolumeSlider.SetLineSize(1);

    UpdateSliderFromVolume();

    return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CESPConfigOutputPage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
  if(pScrollBar) {
    switch(pScrollBar->GetDlgCtrlID()) {
    case IDC_ESP_VOLUMESLIDER:
      UpdateVolumeFromSlider();
      break;
    }
  }
	CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CESPConfigOutputPage::OnEspSoftsaturate() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();
    m_pPlugin->opt_bSoftSaturate = (m_cSoftSaturate.GetCheck() == BST_CHECKED);
    m_pPlugin->UnlockOptions();
  }
}

void CESPConfigOutputPage::BindToPlugin(CEmuSoundPlugin *pPlugin)
{
  m_pPlugin = pPlugin;
}

void CESPConfigOutputPage::UpdateVolumeFromSlider()
{
    int i = m_cVolumeSlider.GetPos();
    if(i < VOLUME_SLIDER_MIN) i = VOLUME_SLIDER_MIN;
    if(i > VOLUME_SLIDER_MAX) i = VOLUME_SLIDER_MAX;
    double dVol = Util::SafeConvertFromDB(i*VOLUME_SCALE_FROM_SLIDER);

    m_cVolume.SetWindowText(CString(Util::NumToString(dVol, Util::NSFLAG_PLUS | Util::NSFLAG_DB)));

    //
    // Live update of the global volume
    //
    if(m_pPlugin) {
        m_pPlugin->LockOptions();
        m_pPlugin->opt_dGlobalVolume = dVol;
        m_pPlugin->UnlockOptions();
    }
}

void CESPConfigOutputPage::UpdateSliderFromVolume()
{
    double dVol = K14_DEFAULT_VOLUME;
    if(m_pPlugin) {
        m_pPlugin->LockOptions();
        dVol = m_pPlugin->opt_dGlobalVolume;
        m_pPlugin->UnlockOptions();
    }

    int i = (int)(Util::SafeConvertToDB(dVol) * VOLUME_SCALE_TO_SLIDER);
    if(i < VOLUME_SLIDER_MIN) i = VOLUME_SLIDER_MIN;
    if(i > VOLUME_SLIDER_MAX) i = VOLUME_SLIDER_MAX;

    m_cVolumeSlider.SetPos(i);
}

void CESPConfigOutputPage::OnCbnSelchangeEspComboReplaygain()
{
    if(!m_pPlugin) return;
    m_pPlugin->LockOptions();

    switch(m_cComboReplayGain.GetCurSel()) {
    case 0:
        m_pPlugin->opt_bReplayGainEnable = FALSE;
        m_pPlugin->opt_bReplayGainAlbum  = FALSE;
        break;
    case 1:
        m_pPlugin->opt_bReplayGainEnable = TRUE;
        m_pPlugin->opt_bReplayGainAlbum  = FALSE;
        break;
    case 2:
        m_pPlugin->opt_bReplayGainEnable = TRUE;
        m_pPlugin->opt_bReplayGainAlbum  = TRUE;
        break;
    }

    m_pPlugin->UnlockOptions();
}

void CESPConfigOutputPage::OnCbnSelchangeEspComboClipprotect()
{
    if(!m_pPlugin) return;
    m_pPlugin->LockOptions();

    switch(m_cComboClipProtect.GetCurSel()) {
    case 0:
        m_pPlugin->opt_bClipProtectEnable = FALSE;
        m_pPlugin->opt_bClipProtectAlbum  = FALSE;
        break;
    case 1:
        m_pPlugin->opt_bClipProtectEnable = TRUE;
        m_pPlugin->opt_bClipProtectAlbum  = FALSE;
        break;
    case 2:
        m_pPlugin->opt_bClipProtectEnable = TRUE;
        m_pPlugin->opt_bClipProtectAlbum  = TRUE;
        break;
    }

    m_pPlugin->UnlockOptions();
}
