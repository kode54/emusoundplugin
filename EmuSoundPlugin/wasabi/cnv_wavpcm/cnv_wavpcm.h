#ifndef _CNV_WAVE_H
#define _CNV_WAVE_H

#include "../studio/wac.h"
#include "../studio/services/svc_mediaconverter.h"
#include "../attribs/attrint.h"
#include "../attribs/attrbool.h"

DWORD _inline rev32(DWORD X) {return ((((DWORD)(X)&0xFF)<<24)|(((DWORD)(X)&0xFF00)<<8)|(((DWORD)(X)&0xFF0000)>>8)|(((DWORD)(X)&0xFF000000)>>24));}
WORD _inline rev16(WORD X) {return (X>>8)|(X<<8);}


class StringPrintChan : public String
{
public:
	StringPrintChan(int x) : String(x==1 ? "mono" : x==2 ? "stereo" : StringPrintf("%ich",x)) {}
};

class StringPrintFreq : public String
{
public:
	StringPrintFreq(int x) 
		: String(
		x<1000 ? StringPrintf("%iHz",x) : 
	x%1000==0 ? StringPrintf("%ikHz",x/1000) : 
	StringPrintf("%i.%ikHz",(x+50)/1000,((x+50)/100)%10)
		) {}
};

class StringPrintInfo : public StringPrintf
{
public:
	StringPrintInfo(int bytes_per_sec,int channels,int srate)
		: StringPrintf("%ukbps %s %s",(bytes_per_sec*8+500)/1000,StringPrintChan(channels),StringPrintFreq(srate))
	{}
};


class converter_base : public svc_mediaConverterI
{
protected:
	int need_seek;
	converter_base() {need_seek=-1;}
	virtual int corecb_onSeeked(int newpos)
	{
		need_seek=newpos;
		return 1;
	}

	virtual const char *getConverterTo() { return "PCM"; }
};

void fix_byte_order(void * data,int size,int bps);
void fix_8bit_sign(void * data,int size);

class ima_adpcm_decoder
{
private:
	int sample,index;
public:
	ima_adpcm_decoder() {sample=index=0;}
	void reset(int s,int i)
	{
		sample=s;
		index=i;
		if (index<0) index=0;
		else if (index>88) index=88;
	}
	int decode(int data);
};


/*

leet trick.
to avoid having to manually keep central registry of all our services in cnv_wavpcm.cpp to register them all
we use a class that keeps track of all its instances
then just converter_make_service() on whatever converter class we want to add

NOTE
this is supposed to work with static instances only
dont create service_factory_base stuff dynamically

*/

class service_factory_base
{
public:
	virtual waServiceFactoryI * getPtr()=0;
	service_factory_base * next;
	static service_factory_base * list;
	service_factory_base() {next=list;list=this;}
};

template<class foo,class bar>
class service_factory : public service_factory_base
{
private:
	waServiceFactoryT<foo,bar> m_factory;
	virtual waServiceFactoryI * getPtr() {return &m_factory;}
};

template<class foo>
class service_factory_converter : public service_factory<svc_mediaConverter,foo> {};

#define converter_make_service(foo) static service_factory_converter<foo> g_svc_##foo;
#define make_service(foo,bar) static service_factory<foo,bar> g_svc_##foo##_##bar;

#endif
