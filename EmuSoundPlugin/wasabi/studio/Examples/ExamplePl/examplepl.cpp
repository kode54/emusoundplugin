/*

  Nullsoft WASABI Source File License

  Copyright 1999-2001 Nullsoft, Inc.

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.


  Brennan Underwood
  brennan@nullsoft.com

*/

#include "examplepl.h"

#include "../../studio/api.h"
#include "../../studio/services/creators.h"
#include "../../bfc/wndcreator.h"

#include "../../common/metatags.h"

#include "../../bfc/virtualwnd.h"
#include "../../bfc/canvas.h"
#include "../../common/corehandle.h"

// {D99950FD-7083-416a-B7BD-3F42C945C2A8}
static const GUID WACNAME_guid = 
{ 0xd99950fd, 0x7083, 0x416a, { 0xb7, 0xbd, 0x3f, 0x42, 0xc9, 0x45, 0xc2, 0xa8 } };

static WACNAME examplepl;
WAComponentClient *the = &examplepl;

//========================================================================
// ExamplePlWnd Class
//========================================================================

class ExamplePlWnd : public VirtualWnd {
public:
  ExamplePlWnd();
  virtual ~ExamplePlWnd();
  
  virtual int onPaint(Canvas *canvas);

  static const char *getWindowTypeName() { return "ExamplePl Window"; }
  static GUID getWindowTypeGuid() { return WACNAME_guid; }

};

ExamplePlWnd::ExamplePlWnd() {
  setName("ExamplePl Window");
}

ExamplePlWnd::~ExamplePlWnd() {
}

int ExamplePlWnd::onPaint(Canvas *canvas) {
  PaintCanvas pc;
  RGB32 color = RGB(120,120,120);
  char buf[4096];


  CoreHandle  * core = new CoreHandle("Main");

  if (canvas == NULL) {
    if (!pc.beginPaint(this)) return 0;
    canvas = &pc;
  }

  canvas->fillRect(&clientRect(), color);
  canvas->setTextSize(14);
  canvas->textOut(20,20,400,20,StringPrintf("Current Playstring: %s" , core->getCurrent()));
  canvas->textOut(20,40,400,20,StringPrintf("Current Song Number: %i", core->getCurPlaybackNumber()+1));

  if(core->getCurrent() != NULL) {
    api->metadb_getInfo(core->getCurrent(), buf, 4096);
    canvas->textOut(20,60,400,20,StringPrintf("Current Info: %s", buf));
  }

  if(core->getCurrent() != NULL) {
    api->metadb_getMetaData(core->getCurrent(), MT_NAME, buf, 4096, MDT_STRINGZ);
    canvas->textOut(20,80,400,20,StringPrintf("Current Song Name: %s", buf));
  }
  
  return 1;
}


//========================================================================  
//WAC CLASS
//========================================================================

WACNAME::WACNAME() {
  registerService(new WndCreateCreatorSingle< CreateWndByGuid<ExamplePlWnd> >);
  registerAutoPopup(ExamplePlWnd::getWindowTypeGuid(), ExamplePlWnd::getWindowTypeName());
}

WACNAME::~WACNAME() {
}

void WACNAME::onCreate() {
}

void WACNAME::onDestroy() {
}

GUID WACNAME::getGUID() {
  return WACNAME_guid;
}