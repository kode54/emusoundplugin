#pragma once

#include "EmuSoundPlugin.h"

// CESPReplayGainDlg dialog

class CESPReplayGainDlg : public CDialog
{
	DECLARE_DYNAMIC(CESPReplayGainDlg)

public:
	CESPReplayGainDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESPReplayGainDlg();

// Dialog Data
	enum { IDD = IDD_ESP_REPLAYGAIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
