#if !defined(AFX_UPGRADINGDLG_H__A15333FD_242C_4208_836B_514CF8ABEF9C__INCLUDED_)
#define AFX_UPGRADINGDLG_H__A15333FD_242C_4208_836B_514CF8ABEF9C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UpgradingDlg.h : header file
//

#include "PSFDriverInfo.h"

/////////////////////////////////////////////////////////////////////////////
// CUpgradingDlg dialog

class CUpgradingDlg : public CDialog
{
// Construction
public:
	CPSXEXE* m_pDriverEXE;
	CPSFDriverInfo* m_pDriverInfo;
	CStringArray m_saFilesChanged;
	CStringArray m_saFiles;
	CUpgradingDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUpgradingDlg)
	enum { IDD = IDD_UPGRADING };
	CEdit	m_cLogBox;
	CProgressCtrl	m_cProgress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUpgradingDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUpgradingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void WorkLog(LPCTSTR lpszPath, LPCTSTR lpszMessage);
	int m_nErrorFiles;
	int m_nSkippedFiles;
	int m_nSuccessfulFiles;
	void ReportDone();
	void SyncLogBox();
	void QueueLogLine(LPCTSTR lpszLine);
	void UpdateInterface();
	BOOL PerformUpgrade(LPCTSTR lpszPathName);
  friend unsigned long __stdcall CUpgradingDlg__Thread(LPVOID lpParam);
	void StopThread();
	void StartThread();
	BOOL m_bFinished;
	CString m_strLogQueue;
	int m_nFile;
	CEvent m_evThreadStarted;
  BOOL m_bThreadKillFlag;
	CCriticalSection m_csThreadRunning;
  CCriticalSection m_csLiveInfo;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UPGRADINGDLG_H__A15333FD_242C_4208_836B_514CF8ABEF9C__INCLUDED_)
