// ParametersDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "ParametersDlg.h"
#include "ParameterModifyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParametersDlg dialog


CParametersDlg::CParametersDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CParametersDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CParametersDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
  m_bSafeToUpdateInterface = FALSE;
}


void CParametersDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParametersDlg)
	DDX_Control(pDX, IDC_PARAMLIST, m_cParamList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CParametersDlg, CDialog)
	//{{AFX_MSG_MAP(CParametersDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_MODIFY, OnModify)
	ON_NOTIFY(NM_DBLCLK, IDC_PARAMLIST, OnDblclkParamlist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParametersDlg message handlers

BOOL CParametersDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  CWaitCursor cwc;

  BOOL b;

  CPSF psf;
  b = psf.ReadFromFile(m_lpszFile,
    CPSF::PSF_FILE_VERSION |
    CPSF::PSF_FILE_ZEXE,
    TRUE
  );
  if(!b) {
    cwc.Restore();
    InitError((LPCTSTR)psf.GetLastError(), MB_ICONHAND);
    return TRUE;
  }

  CPSXEXE exe;
  b = exe.ReadFromPSF(psf, TRUE);
  if(!b) {
    cwc.Restore();
    InitError((LPCTSTR)exe.GetLastError(), MB_ICONHAND);
    return TRUE;
  }

  b = m_driverinfo.FindInBuffer(
    exe.GetTextBuffer(),
    exe.GetTextSize(),
    exe.GetTextStart()
  );
  if(!b) {
    cwc.Restore();
    InitError((LPCTSTR)m_driverinfo.GetLastError(), MB_ICONHAND);
    return TRUE;
  }

  //
  // If there's no parameters to change, then that's an error too
  //
  if(!(m_driverinfo.m_dwaParamLocations.GetSize())) {
    cwc.Restore();
    InitError(_T("This driver has no parameters to change"), MB_ICONINFORMATION);
    return TRUE;
  }

  /*
  ** Copy parameters
  */
  m_dwaParams.SetSize(m_driverinfo.m_dwaParamLocations.GetSize());
  int i;
  for(i = 0; i < m_driverinfo.m_dwaParamLocations.GetSize(); i++) {
    /*
    ** Cap param sizes at 4 bytes (will not permanently affect the driver)
    */
    if(m_driverinfo.m_dwaParamSizes.GetAt(i) > 4) {
      m_driverinfo.m_dwaParamSizes.SetAt(i, 4);
    }
    BYTE v[4];
    memset(v, 0, 4);
    exe.ReadAddressRange(
      v,
      m_driverinfo.m_dwaParamLocations.GetAt(i),
      m_driverinfo.m_dwaParamSizes.GetAt(i)
    );
    m_dwaParams.SetAt(i, Util::Get32LSB(v));
  }

  SetDlgItemText(IDC_FILENAME, m_lpszFile);
  SetDlgItemText(IDC_DRIVERTEXT, m_driverinfo.m_strDriverText);

  m_cParamList.InsertColumn(0, _T("Name"));
  m_cParamList.InsertColumn(1, _T("Type"));
  m_cParamList.InsertColumn(2, _T("Data"));

  RECT r;
  m_cParamList.GetClientRect(&r);

  m_cParamList.SetColumnWidth(0, (r.right-r.left)/3);
  m_cParamList.SetColumnWidth(1, (r.right-r.left)/3);
  m_cParamList.SetColumnWidth(2, (r.right-r.left)/3);

  for(i = 0; i < m_driverinfo.m_dwaParamLocations.GetSize(); i++) {
    LPCTSTR t = _T("");
    switch(m_driverinfo.m_dwaParamSizes.GetAt(i)) {
    case 1: t = _T("byte"); break;
    case 2: t = _T("short"); break;
    case 3: t = _T("3 bytes"); break;
    case 4: t = _T("long"); break;
    }
    m_cParamList.InsertItem(i, m_driverinfo.m_saParamNames.GetAt(i));
    m_cParamList.SetItemText(i, 1, t);
    CString s;
    s.Format(_T("%u"), m_dwaParams.GetAt(i));
    m_cParamList.SetItemText(i, 2, s);
  }

  //PostMessage(WM_COMMAND, IDCANCEL, 0);

  m_bSafeToUpdateInterface = TRUE;
  m_bModified = FALSE;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CParametersDlg::OnDestroy() 
{
  m_bSafeToUpdateInterface = FALSE;
	CDialog::OnDestroy();
}

LRESULT CParametersDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(m_bSafeToUpdateInterface) UpdateInterface();

	return CDialog::WindowProc(message, wParam, lParam);
}

void CParametersDlg::UpdateInterface()
{
  GetDlgItem(IDC_MODIFY)->EnableWindow(m_cParamList.GetSelectedCount() == 1);
}

void CParametersDlg::OnModify() 
{
  POSITION p = m_cParamList.GetFirstSelectedItemPosition();
  if(!p) return;
  int nIndex = m_cParamList.GetNextSelectedItem(p);
  if(nIndex < 0 || nIndex >= m_driverinfo.m_dwaParamLocations.GetSize()) return;

  CParameterModifyDlg dlg;

  dlg.m_strName = m_driverinfo.m_saParamNames.GetAt(nIndex) + _T(" =");
  dlg.m_dwValue = m_dwaParams.GetAt(nIndex);

  if(dlg.DoModal() != IDOK) return;

  DWORD dwValue = dlg.m_dwValue;
  switch(m_driverinfo.m_dwaParamSizes.GetAt(nIndex)) {
  case 1: if(dwValue > 0x000000FF) { dwValue = 0x000000FF; } break;
  case 2: if(dwValue > 0x0000FFFF) { dwValue = 0x0000FFFF; } break;
  }
  if(dwValue != m_dwaParams.GetAt(nIndex)) {
    m_dwaParams.SetAt(nIndex, dwValue);
    m_bModified = TRUE;

    CString s;
    s.Format(_T("%u"), m_dwaParams.GetAt(nIndex));
    m_cParamList.SetItemText(nIndex, 2, s);
  }
}

void CParametersDlg::OnDblclkParamlist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnModify();
	*pResult = 0;
}

void CParametersDlg::OnOK() 
{
  if(!m_bModified) {
    CDialog::OnOK();
    return;
  }
  CWaitCursor cwc;

  BOOL b;

  CPSF psf;
  b = psf.ReadFromFile(m_lpszFile,
    CPSF::PSF_FILE_VERSION |
    CPSF::PSF_FILE_ZEXE,
    TRUE
  );
  if(!b) {
    cwc.Restore();
    // TODO: error message.
	  CDialog::OnOK();
    return;
  }

  CPSXEXE exe;
  b = exe.ReadFromPSF(psf, TRUE);
  if(!b) {
    cwc.Restore();
    // TODO: error message.
	  CDialog::OnOK();
    return;
  }

  for(int i = 0; i < m_driverinfo.m_dwaParamLocations.GetSize(); i++) {
    BYTE v[4];
    Util::Put32LSB(v, m_dwaParams.GetAt(i));
    exe.WriteAddressRange(
      m_driverinfo.m_dwaParamLocations.GetAt(i),
      v,
      m_driverinfo.m_dwaParamSizes.GetAt(i)
    );
  }

  b = exe.WriteToPSF(psf, CPSFCompressor::GetDefaultMethod());
  if(!b) {
    cwc.Restore();
    // TODO: error message.
	  CDialog::OnOK();
    return;
  }

  b = psf.WriteToFile(m_lpszFile, CPSF::PSF_FILE_ZEXE, TRUE);
  if(!b) {
    cwc.Restore();
    // TODO: error message.
	  CDialog::OnOK();
    return;
  }

	CDialog::OnOK();
}

void CParametersDlg::InitError(LPCTSTR lpszMessage, UINT uIcon)
{
  CString s;
  s.Format("%s\n%s", m_lpszFile, lpszMessage);
  MessageBox(s, _T("Parameters"), MB_OK | uIcon);
  PostMessage(WM_COMMAND, IDCANCEL, 0);
}
