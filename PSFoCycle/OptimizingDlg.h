#if !defined(AFX_OPTIMIZINGDLG_H__5C964B98_0924_41B8_802A_898878405B44__INCLUDED_)
#define AFX_OPTIMIZINGDLG_H__5C964B98_0924_41B8_802A_898878405B44__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptimizingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptimizingDlg dialog

class COptimizingDlg : public CDialog
{
// Construction
public:
	CStringArray m_saFiles;
	CStringArray m_saFilesChanged;
	COptimizingDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COptimizingDlg)
	enum { IDD = IDD_OPTIMIZING };
	CButton	m_cSkipFile;
	CEdit	m_cLogBox;
	CProgressCtrl	m_cProgress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptimizingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptimizingDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnSkipfile();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CPSF m_psf;
	INT64 m_n64TotalSavings;
	DWORD m_dwAfterLength;
	DWORD m_dwBeforeLength;
	void CancelCurrentAudit();
	void CommitCurrentAudit();
	int m_nSkippedFiles;
	void ReportSuccess(LPCTSTR lpszFile);
	void ReportSkipped(LPCTSTR lpszFile);
	void ReportError(LPCTSTR lpszFile, LPCTSTR lpszError);
	void ReportDone();
	void AddLogLine(LPCTSTR lpszLine);
	BOOL m_bFinished;
	void StateMachine();
	int m_nFile;
  int m_nSuccessfulFiles;
  int m_nErrorFiles;
	BOOL StartNewAudit();
	double m_dRedundantSecondsRecord;
	BOOL m_bAuditing;
	CPSFAudit m_audit;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIMIZINGDLG_H__5C964B98_0924_41B8_802A_898878405B44__INCLUDED_)
