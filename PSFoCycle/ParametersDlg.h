#if !defined(AFX_PARAMETERSDLG_H__C4AF9765_10BB_4FB4_A29E_BB3EF9437612__INCLUDED_)
#define AFX_PARAMETERSDLG_H__C4AF9765_10BB_4FB4_A29E_BB3EF9437612__INCLUDED_

#include "PSFDriverInfo.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ParametersDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CParametersDlg dialog

class CParametersDlg : public CDialog
{
// Construction
public:
	BOOL m_bSafeToUpdateInterface;
	LPCTSTR m_lpszFile;
	CParametersDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CParametersDlg)
	enum { IDD = IDD_PARAMETERS };
	CListCtrl	m_cParamList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParametersDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CParametersDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnModify();
	afx_msg void OnDblclkParamlist(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void InitError(LPCTSTR lpszMessage, UINT uIcon);
	BOOL m_bModified;
	void UpdateInterface();
	CDWordArray m_dwaParams;
	CPSFDriverInfo m_driverinfo;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARAMETERSDLG_H__C4AF9765_10BB_4FB4_A29E_BB3EF9437612__INCLUDED_)
