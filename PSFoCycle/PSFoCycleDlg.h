// PSFoCycleDlg.h : header file
//

#if !defined(AFX_PSFOCYCLEDLG_H__3124779D_B3FE_410E_8136_F5620E1C7CFE__INCLUDED_)
#define AFX_PSFOCYCLEDLG_H__3124779D_B3FE_410E_8136_F5620E1C7CFE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CPSFoCycleDlg dialog

class CPSFoCycleDlg : public CDialog
{
// Construction
public:
	CPSFoCycleDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CPSFoCycleDlg)
	enum { IDD = IDD_PSFOCYCLE_DIALOG };
	CListCtrl	m_cFiles;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPSFoCycleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CPSFoCycleDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	afx_msg void OnBuilddriver();
	afx_msg void OnOptimize();
	afx_msg void OnAdd();
	afx_msg void OnDestroy();
	afx_msg void OnSelectall();
	afx_msg void OnRemove();
	virtual void OnCancel();
	afx_msg void OnParameters();
	afx_msg void OnDblclkFiles(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpgradedriver();
	afx_msg void OnAbout();
	afx_msg void OnRecompress();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bInterfaceSafeToUpdate;
	void FindDriverThreadMachine(LPTSTR lpszFileName, LPTSTR lpszDriverText);
	void GetFileSelectedList(CStringArray &r);
  //friend UINT AFX_CDECL CPSFoCycleDlg__FindDriverThread(LPVOID lpParam);
  friend unsigned long __stdcall CPSFoCycleDlg__FindDriverThread(LPVOID lpParam);
	DWORD m_dwFindDriverThreadKillFlag;
	CEvent m_evFindDriverThreadStarted;
	CCriticalSection m_csFindDriverThread;
	void StopFindDriverThread();
	void StartFindDriverThread();
	int FindPathNameInList(LPCTSTR lpszPathName);
	void InvalidateDriverString(LPCTSTR lpszPathName);
	void InvalidateDriverStrings(CStringArray &saFilesChanged);
	void PerformOptimize(CStringArray &saFiles);
	//void GetFileList(CStringArray &r);
	BOOL GetDriverStringFromFile(LPCTSTR lpszPathName, CString &r);
	CString FileNameOnly(LPCTSTR lpszPathName);
	void AddFile(LPCTSTR lpszPathName);
	void UpdateInterface();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSFOCYCLEDLG_H__3124779D_B3FE_410E_8136_F5620E1C7CFE__INCLUDED_)
