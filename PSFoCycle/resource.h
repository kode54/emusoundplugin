//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by PSFoCycle.rc
//
#define IDC_MODIFY                      3
#define IDC_SKIPFILE                    3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PSFOCYCLE_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDD_BUILDDRIVER                 129
#define IDD_OPTIMIZE                    130
#define IDD_EMUSETTINGS                 131
#define IDD_OPTIMIZING                  132
#define IDD_PARAMETERS                  133
#define IDD_PARAMETER_MODIFY            134
#define IDD_UPGRADING                   135
#define IDD_RECOMPRESS                  136
#define IDD_RECOMPRESSING               137
#define IDC_FILES                       1000
#define IDC_ADD                         1001
#define IDC_SELECTALL                   1002
#define IDC_REMOVE                      1003
#define IDC_UPGRADEDRIVER               1004
#define IDC_PARAMETERS                  1005
#define IDC_BUILDDRIVER                 1006
#define IDC_OPTIMIZE                    1007
#define IDC_RECOMPRESS                  1008
#define IDC_PSFDRV_FILE                 1009
#define IDC_ABOUT                       1009
#define IDC_PSFDRV_BROWSE               1010
#define IDC_GAMEEXE_FILE                1011
#define IDC_EMUSETTINGS                 1011
#define IDC_GAMEEXE_BROWSE              1012
#define IDC_RUNFIXED_EDIT               1013
#define IDC_RUNSMART_EDIT               1015
#define IDC_NOSONGSPECIFIC              1016
#define IDC_RUNFIXED                    1017
#define IDC_RUNSMART                    1018
#define IDC_SETLENGTH                   1019
#define IDC_SETFADE                     1020
#define IDC_SETFADE_SEC                 1021
#define IDC_FORCELENGTH                 1022
#define IDC_CURRENTFILE                 1023
#define IDC_PROGRESS                    1024
#define IDC_INFO                        1025
#define IDC_PSFDRV_INFO                 1026
#define IDC_PARANOID_BYTES              1027
#define IDC_PARAMLIST                   1027
#define IDC_FILENAME                    1028
#define IDC_DRIVERTEXT                  1029
#define IDC_VALUE                       1031
#define IDC_NAME                        1032
#define IDC_LOGBOX                      1033
#define IDC_BANNER                      1035
#define IDC_METHODLIST                  1036
#define IDC_SMALLERONLY                 1038
#define IDC_VERSIONDETAILS              1047
#define IDC_PARANOID                    1068
#define IDC_HARSH                       1070
#define IDC_FRIENDLY                    1071

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1039
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
