#if !defined(AFX_BUILDDRIVERDLG_H__1A7E2FEF_2DC6_4986_9DF8_FA4D0E1FD110__INCLUDED_)
#define AFX_BUILDDRIVERDLG_H__1A7E2FEF_2DC6_4986_9DF8_FA4D0E1FD110__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BuildDriverDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBuildDriverDlg dialog

class CBuildDriverDlg : public CDialog
{
// Construction
public:
	void UpdatePSFDRVInfo();
	CBuildDriverDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBuildDriverDlg)
	enum { IDD = IDD_BUILDDRIVER };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBuildDriverDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBuildDriverDlg)
	afx_msg void OnPsfdrvBrowse();
	afx_msg void OnGameexeBrowse();
	virtual void OnOK();
	afx_msg void OnKillfocusPsfdrvFile();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BUILDDRIVERDLG_H__1A7E2FEF_2DC6_4986_9DF8_FA4D0E1FD110__INCLUDED_)
