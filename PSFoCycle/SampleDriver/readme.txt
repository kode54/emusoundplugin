PSF-o-Cycle sample driver stub

This is a mini-SDK for the PSF-o-Cycle driver system.  With it, you can write
a small "stub" program in C which can be compiled, then combined with the
original game executable to create a driver suitable for PSF playing.

psfdrv.c - Example of the stub C program
mk.bat   - Example batch file to compile it

How to use:

1. Customize psfdrv.c with new library call addresses, parameters, whatever
   other info you need to put in there
2. Compile with mk.bat.  This assumes you're using the PSY-Q environment, so
   if you're not, you'll need to figure out another way to compile/link.
3. Use the "Build Driver" button in PSF-o-Cycle to combine psfdrv.bin and
   the original game executable, building a complete driver.
4. Insert song-specific (i.e. SEQ/VH/VB) data into the built driver, using
   PSFLab or your own tools.
5. Test, debug, lather, rinse, repeat!
