// OptimizingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "OptimizingDlg.h"
#include "PSFDriverInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptimizingDlg dialog


COptimizingDlg::COptimizingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptimizingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptimizingDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
  m_nFile = 0;
  m_bAuditing = FALSE;
  m_dRedundantSecondsRecord = 0.0;
}

void COptimizingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptimizingDlg)
	DDX_Control(pDX, IDC_SKIPFILE, m_cSkipFile);
	DDX_Control(pDX, IDC_LOGBOX, m_cLogBox);
	DDX_Control(pDX, IDC_PROGRESS, m_cProgress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptimizingDlg, CDialog)
	//{{AFX_MSG_MAP(COptimizingDlg)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_SKIPFILE, OnSkipfile)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptimizingDlg message handlers

void COptimizingDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	//CDialog::OnOK();
}

void COptimizingDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

BOOL COptimizingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  m_bFinished = FALSE;
  m_nSuccessfulFiles = 0;
  m_nErrorFiles = 0;
  m_nSkippedFiles = 0;
  m_n64TotalSavings = 0;

  SetTimer(10, 500, NULL);
  m_cProgress.SetRange(0, 10000);

  StateMachine();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//////////////////////////////////////////////////////////////////////////////
//
// Optimizing state machine.
//
// The mojo of this function is such that, outside it, we are either always
// auditing (m_bAuditing is true), or we're finished (m_bFinished is true).
//
void COptimizingDlg::StateMachine()
{
statemachinetop:

  //
  // If we're finished, this does nothing
  //
  if(m_bFinished) return;

  //
  // If we're not auditing, we should be, so start!
  //
  // Attempt auditing each file in turn until there's nothing left
  // to audit.
  //
  while(!m_bAuditing) {
    //
    // If we ran out of files, then we're done
    //
    if(m_nFile >= m_saFiles.GetSize()) {
      m_bFinished = TRUE;
      KillTimer(10);
      ReportDone();
      m_psf.DeleteFromMemory(CPSF::PSF_FILE_ALL);
      return;
    }
    //
    // Attempt to start a new audit.
    // If it fails, advance to the next file.
    // (In this case m_bAuditing will have remained FALSE.)
    //
    if(!StartNewAudit()) m_nFile++;
  }

  //
  // We're auditing, so m_audit should have valid information for us
  //

  //
  // If an error occurred, we'll need to report it and move on
  //
  if(m_audit.DidAuditErrorOccur()) {
    ReportError(m_saFiles.GetAt(m_nFile), m_audit.GetAuditError());
    CancelCurrentAudit();
    m_nFile++;
    goto statemachinetop;
  }

  //
  // Now we update the blinkenlights
  //
  int nBytes = m_audit.GetBytes();
  double dSeconds = m_audit.GetSeconds();
  double dUsedSeconds = m_audit.GetUsedSeconds();

  CString s;
  s.Format(_T("Duration: %s\nBytes used: %d"), Util::NumToString(dSeconds, TRUE), nBytes);
  SetDlgItemText(IDC_INFO, s);

  double dRedundantSeconds = dSeconds - dUsedSeconds;
  if(dRedundantSeconds > m_dRedundantSecondsRecord) {
    m_dRedundantSecondsRecord = dRedundantSeconds;
  }

  //
  // Figure out progress for our progress bar
  //
  double dInFileProgress = 0.0;
  if(goOptions.m_bRunFixed) {
    if(goOptions.m_dRunFixedSeconds > 0.01) {
      dInFileProgress = dSeconds / goOptions.m_dRunFixedSeconds;
    }
  } else if(goOptions.m_bRunSmart) {
    if(goOptions.m_dRunSmartSeconds > 0.01) {
      dInFileProgress = m_dRedundantSecondsRecord / goOptions.m_dRunSmartSeconds;
    }
  }
  int nFiles = m_saFiles.GetSize();
  double dTotalProgress =
    (((double)(m_nFile)) + dInFileProgress) / ((double)((nFiles < 1) ? 1 : nFiles));
  m_cProgress.SetPos((int)(10000.0 * dTotalProgress));

  //
  // Figure out if we should move on to the next file
  //
  BOOL bShouldMoveOn = FALSE;
  if(goOptions.m_bRunFixed) {
    if(dSeconds >= goOptions.m_dRunFixedSeconds) { bShouldMoveOn = TRUE; }
  }
  if(goOptions.m_bRunSmart) {
    if(m_dRedundantSecondsRecord >= goOptions.m_dRunSmartSeconds) { bShouldMoveOn = TRUE; }
  }

  //
  // If we should move on, then do so
  //
  if(bShouldMoveOn) {
    CommitCurrentAudit();
    m_nFile++;
    goto statemachinetop;
  }
}

void COptimizingDlg::OnTimer(UINT nIDEvent) 
{
	CDialog::OnTimer(nIDEvent);
  if(nIDEvent != 10) return;
  StateMachine();
}

void COptimizingDlg::OnDestroy() 
{
  KillTimer(10);
  m_audit.Close();
	CDialog::OnDestroy();
}

//
// On success, this always returns with m_bAuditing as TRUE
//
BOOL COptimizingDlg::StartNewAudit()
{
  m_audit.Close();
  m_dRedundantSecondsRecord = 0.0;
  CString strFile = m_saFiles.GetAt(m_nFile);

  //
  // Read the version and zexe from the psf file
  //
  m_psf.DeleteFromMemory(CPSF::PSF_FILE_ALL);
  BOOL b = m_psf.ReadFromFile(strFile,
    CPSF::PSF_FILE_VERSION |
    CPSF::PSF_FILE_ZEXE |
    CPSF::PSF_FILE_TAG,
    TRUE
  );
  if(!b) {
    ReportError(strFile, (LPCTSTR)m_psf.GetLastError());
    return FALSE;
  }

  //
  // If _lib or _lib2 tags exist, we should not optimize
  //
  CPSFTag tag;
  b = tag.ReadFromPSF(m_psf, TRUE);
  if(b) {
    CString s;
    s = tag.GetVar(_T("_lib"));
    if(s.IsEmpty()) s = tag.GetVar(_T("_lib2"));
    if(!(s.IsEmpty())) {
      ReportError(strFile, _T("MiniPSFs should not be optimized"));
      return FALSE;
    }
  }

  //
  // Snag the "before" length while we're here
  //
  m_dwBeforeLength = m_psf.GetLastKnownFileLength();

  //
  // Attempt to extract the PSX EXE
  //
  CPSXEXE exe;
  b = exe.ReadFromPSF(m_psf, TRUE);
  if(!b) {
    ReportError(strFile, (LPCTSTR)exe.GetLastError());
    return FALSE;
  }

  //
  // Open a new audit, upload the program, set options
  //
  m_audit.Open();
  if(!m_audit.UploadProgram(exe.GetEXEBuffer(), exe.GetEXESize())) {
    ReportError(strFile, (LPCTSTR)m_audit.GetLastError());
    return FALSE;
  }
  m_audit.SetCompat(goOptions.m_bEmuFriendly ? IOP_COMPAT_FRIENDLY : IOP_COMPAT_HARSH);

  //
  // Update the UI
  //
  CString s;
  s.Format(_T("Optimizing (%d of %d)"), m_nFile + 1, m_saFiles.GetSize());
  SetWindowText(s);
  SetDlgItemText(IDC_CURRENTFILE, strFile);

  //
  // Start the audit
  //
  m_audit.Start();
  m_bAuditing = TRUE;
  return TRUE;
}

void COptimizingDlg::CommitCurrentAudit()
{
  m_audit.Stop();
  m_bAuditing = FALSE;

  /*
  ** Grow audit area if paranoid
  */
  if(goOptions.m_bAuditParanoid) {
    m_audit.GrowBy(goOptions.m_nAuditParanoidBytes);
  }

  /*
  ** Get current filename
  */
  CString strFile = m_saFiles.GetAt(m_nFile);

  //
  // Get the EXE from the PSF file, which we kept
  //
  CPSXEXE exe;
  BOOL b = exe.ReadFromPSF(m_psf, TRUE);
  if(!b) { m_audit.Close(); ReportError(strFile, (LPCTSTR)exe.GetLastError()); return; }

  //
  // Get info on the EXE
  //
  LPBYTE lpEXE = exe.GetEXEBuffer();
  DWORD dwTextStart = exe.GetTextStart();
  DWORD dwTextSize  = exe.GetTextSize();

  //
  // Read tag
  // (if this fails, the tag will simply be empty)
  //
  CPSFTag tag;
  tag.ReadFromPSF(m_psf, TRUE);

  /*
  ** If "do not optimize song-specific area" was checked:
  ** - See whether or not there is a PSF driver here
  ** - If there is, force all song-specific areas as used in their entirety
  */
  if(goOptions.m_bDoNotOptimizeSongSpecific) {
    CPSFDriverInfo driverinfo;
    if(driverinfo.FindInBuffer(lpEXE + 0x800, dwTextSize, dwTextStart)) {
      /*
      ** Force "used" status on all song-specific areas
      */
      int nSongSpecificAreas = driverinfo.m_dwaSongSpecificStarts.GetSize();
      int i;
      for(i = 0; i < nSongSpecificAreas; i++) {
        DWORD dwStart = driverinfo.m_dwaSongSpecificStarts.GetAt(i);
        DWORD dwLen   = driverinfo.m_dwaSongSpecificLengths.GetAt(i);
        DWORD dwEnd   = dwStart + dwLen;
        while(dwStart < dwEnd) {
          m_audit.ForceWordUsed(dwStart);
          dwStart += 4;
        }
      }
    }
  }

  /*
  ** Perform optimization on the EXE
  */
  DWORD dwOffset;
  for(dwOffset = 0; dwOffset < dwTextSize; dwOffset += 4) {
    if(!(m_audit.IsWordUsed(dwTextStart + dwOffset))) {
      lpEXE[0x800 + dwOffset + 0] = 0;
      lpEXE[0x800 + dwOffset + 1] = 0;
      lpEXE[0x800 + dwOffset + 2] = 0;
      lpEXE[0x800 + dwOffset + 3] = 0;
    }
  }

  /*
  ** Adjust the length/fade tags, if that option was desired
  */
  BOOL bUpdateTag = FALSE;
  if(goOptions.m_bSetLength) {
    CString strLengthTag;
    if(!goOptions.m_bForceLength) {
      strLengthTag = tag.GetVar(_T("length"));
    }
    if(strLengthTag.IsEmpty()) {
      bUpdateTag = TRUE;
      tag.SetVar(_T("length"), Util::NumToString(m_audit.GetUsedSeconds(), TRUE));
      if(goOptions.m_bSetFade) {
        tag.SetVar(_T("fade"), Util::NumToString(goOptions.m_dFadeSeconds, TRUE));
      }
    }
  }

  //
  // Close the audit
  //
  m_audit.Close();

  //
  // Recompress the EXE
  //
  b = exe.WriteToPSF(m_psf, CPSFCompressor::GetDefaultMethod());
  if(!b) {
    ReportError(strFile, (LPCTSTR)exe.GetLastError());
    return;
  }

  //
  // Rewrite the tag, if we need to
  //
  if(bUpdateTag) {
    // Try to write
    b = tag.WriteToPSF(m_psf, TRUE);
    // If it failed, then we simply won't write the tag
    if(!b) {
      bUpdateTag = FALSE;
    }
  }

  //
  // Save changes to the PSF
  //
  DWORD dwMask = CPSF::PSF_FILE_ZEXE;
  if(bUpdateTag) dwMask |= CPSF::PSF_FILE_TAG;
  b = m_psf.WriteToFile(strFile, dwMask, TRUE);
  if(!b) {
    ReportError(strFile, (LPCTSTR)m_psf.GetLastError());
    return;
  }

  //
  // Now we grab the "After" length while we're here
  //
  m_dwAfterLength = m_psf.GetLastKnownFileLength();

  m_psf.DeleteFromMemory(CPSF::PSF_FILE_ALL);

  //
  // Success - Add it to the list of files that changed
  //
  ReportSuccess(strFile);

  m_saFilesChanged.Add(strFile);
}

void COptimizingDlg::CancelCurrentAudit()
{
  if(m_bAuditing) {
    m_audit.Stop();
    m_audit.Close();
    m_bAuditing = FALSE;
  }
}

void COptimizingDlg::ReportDone()
{
  SetWindowText(_T("Optimizing (done)"));
  SetDlgItemText(IDC_CURRENTFILE, _T("Done"));
  SetDlgItemText(IDC_INFO, _T(""));
  m_cProgress.SetPos(10000);
  AddLogLine(_T("Done"));

  CString s;
  if(m_nSuccessfulFiles) {
    s.Format(_T("- %d file%s optimized"), m_nSuccessfulFiles, m_nSuccessfulFiles==1?_T(""):_T("s"));
    AddLogLine(s);
  }
  if(m_nErrorFiles) {
    s.Format(_T("- %d error%s"), m_nErrorFiles, m_nErrorFiles==1?_T(""):_T("s"));
    AddLogLine(s);
  } else {
    AddLogLine(_T("- No errors"));
  }
  if(m_nSkippedFiles) {
    s.Format(_T("- %d file%s skipped"), m_nSkippedFiles, m_nSkippedFiles==1?_T(""):_T("s"));
    AddLogLine(s);
  }

  if(m_n64TotalSavings > 0) {
    s = _T("- Total savings: ");
    s += Util::NumToString(m_n64TotalSavings, FALSE);
    s += _T(" bytes");
    AddLogLine(s);
  }

  // Do nothing more fancy than make the "Cancel" button say "OK"
  SetDlgItemText(IDCANCEL, _T("OK"));
  // and disable Skip File
  m_cSkipFile.EnableWindow(FALSE);
}

//
// Since all threading is handled on the audit side, there are
// no thread/buffering issues here
//
void COptimizingDlg::AddLogLine(LPCTSTR lpszLine)
{
  CString s;
  m_cLogBox.GetWindowText(s);
  CTime t = CTime::GetCurrentTime();
  s += t.Format((LPCSTR)"[%I:%M:%S %p] ");
  s += lpszLine;
  s += _T("\r\n");
  m_cLogBox.SetWindowText(s);

  m_cLogBox.LineScroll(m_cLogBox.GetLineCount());
}

//
// Report an error with a file, and increment the counter
//
void COptimizingDlg::ReportError(LPCTSTR lpszFile, LPCTSTR lpszError)
{
  CString s = Util::SeekToFileName(lpszFile);
  s += _T(": ");
  s += lpszError;
  AddLogLine(s);
  m_nErrorFiles++;
}

//
// Report success with a file, increment the counter
//
void COptimizingDlg::ReportSuccess(LPCTSTR lpszFile)
{
  CString s = Util::SeekToFileName(lpszFile);
  s += _T(": OK (");
  s += Util::NumToString(m_dwBeforeLength, FALSE);
  s += _T("->");
  s += Util::NumToString(m_dwAfterLength, FALSE);
  s += _T(" bytes)");
  AddLogLine(s);
  m_nSuccessfulFiles++;
  { INT64 b = (INT64)m_dwBeforeLength;
    INT64 a = (INT64)m_dwAfterLength;
    m_n64TotalSavings += (b - a);
  }
}

//
// Report success with a file, increment the counter
//
void COptimizingDlg::ReportSkipped(LPCTSTR lpszFile)
{
  CString s = Util::SeekToFileName(lpszFile);
  s += _T(": Skipped by user");
  AddLogLine(s);
  m_nSkippedFiles++;
}

//////////////////////////////////////////////////////////////////////////////
//
// Skip File button
//
void COptimizingDlg::OnSkipfile() 
{
  //
  // If we're finished, or not auditing, then this does nothing
  //
  if(m_bFinished || (!m_bAuditing)) return;

  //
  // Cancel current audit, then call the state machine to continue
  //
  ReportSkipped(m_saFiles.GetAt(m_nFile));
  CancelCurrentAudit();
  m_nFile++;

  StateMachine();
}

//////////////////////////////////////////////////////////////////////////////
