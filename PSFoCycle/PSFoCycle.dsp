# Microsoft Developer Studio Project File - Name="PSFoCycle" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=PSFoCycle - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "PSFoCycle.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "PSFoCycle.mak" CFG="PSFoCycle - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "PSFoCycle - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "PSFoCycle - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "PSFoCycle - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /G6 /Gr /MT /W3 /GX /O2 /Ob2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D ZEXPORTVA=__cdecl /D CDECL=__cdecl /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 ../zlib/Release/zlib.lib ../7z/Release/7z.lib /nologo /subsystem:windows /machine:I386
# SUBTRACT LINK32 /profile

!ELSEIF  "$(CFG)" == "PSFoCycle - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /G6 /Gr /MTd /W3 /Gm /GX /ZI /Od /Ob1 /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D ZEXPORTVA=__cdecl /D CDECL=__cdecl /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ../zlib/Debug/zlib.lib ../7z/Debug/7z.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "PSFoCycle - Win32 Release"
# Name "PSFoCycle - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\BuildDriverDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\EmuSettingsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GlobalOptions.cpp
# End Source File
# Begin Source File

SOURCE=.\OptimizeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\OptimizingDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ParameterModifyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ParametersDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PSFDriverInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\PSFoCycle.cpp
# End Source File
# Begin Source File

SOURCE=.\PSFoCycle.rc
# End Source File
# Begin Source File

SOURCE=.\PSFoCycleDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RecompressDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RecompressingDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\UpgradingDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\BuildDriverDlg.h
# End Source File
# Begin Source File

SOURCE=.\EmuSettingsDlg.h
# End Source File
# Begin Source File

SOURCE=.\GlobalOptions.h
# End Source File
# Begin Source File

SOURCE=.\OptimizeDlg.h
# End Source File
# Begin Source File

SOURCE=.\OptimizingDlg.h
# End Source File
# Begin Source File

SOURCE=.\ParameterModifyDlg.h
# End Source File
# Begin Source File

SOURCE=.\ParametersDlg.h
# End Source File
# Begin Source File

SOURCE=.\PSFDriverInfo.h
# End Source File
# Begin Source File

SOURCE=.\PSFoCycle.h
# End Source File
# Begin Source File

SOURCE=.\PSFoCycleDlg.h
# End Source File
# Begin Source File

SOURCE=.\RecompressDlg.h
# End Source File
# Begin Source File

SOURCE=.\RecompressingDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\UpgradingDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\PSFoCycle.ico
# End Source File
# Begin Source File

SOURCE=.\res\PSFoCycle.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
