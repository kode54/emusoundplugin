// BuildDriverDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "BuildDriverDlg.h"
#include "PSFDriverInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBuildDriverDlg dialog


CBuildDriverDlg::CBuildDriverDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBuildDriverDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBuildDriverDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBuildDriverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBuildDriverDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBuildDriverDlg, CDialog)
	//{{AFX_MSG_MAP(CBuildDriverDlg)
	ON_BN_CLICKED(IDC_PSFDRV_BROWSE, OnPsfdrvBrowse)
	ON_BN_CLICKED(IDC_GAMEEXE_BROWSE, OnGameexeBrowse)
	ON_EN_KILLFOCUS(IDC_PSFDRV_FILE, OnKillfocusPsfdrvFile)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBuildDriverDlg message handlers

void CBuildDriverDlg::OnPsfdrvBrowse() 
{
  CFileDialog dlg(
    TRUE, // open
    NULL,
    NULL,
    //OFN_ALLOWMULTISELECT|
      OFN_FILEMUSTEXIST|
      OFN_HIDEREADONLY,
    _T("Stub binary files (*.bin)|*.bin|All files (*.*)|*.*||"),
    this
  );
  dlg.m_ofn.lpstrTitle = _T("Browse");
  if(dlg.DoModal() != IDOK) return;
  SetDlgItemText(IDC_PSFDRV_FILE, dlg.GetPathName());
  UpdatePSFDRVInfo();
}

void CBuildDriverDlg::OnGameexeBrowse() 
{
	// TODO: Add your control notification handler code here
  CFileDialog dlg(
    TRUE, // open
    NULL,
    NULL,
    //OFN_ALLOWMULTISELECT|
      OFN_FILEMUSTEXIST|
      OFN_HIDEREADONLY,
    _T("All files (*.*)|*.*||"),
    this
  );
  dlg.m_ofn.lpstrTitle = _T("Browse");
  if(dlg.DoModal() != IDOK) return;
  SetDlgItemText(IDC_GAMEEXE_FILE, dlg.GetPathName());
}

void CBuildDriverDlg::OnOK() 
{
  CByteArray stub;

  CString strStub;
  GetDlgItemText(IDC_PSFDRV_FILE, strStub);
  CString strGameEXE;
  GetDlgItemText(IDC_GAMEEXE_FILE, strGameEXE);

  if(strStub.IsEmpty()) {
    MessageBox(_T("Please specify a stub filename"), _T("Build Driver"), MB_OK|MB_ICONEXCLAMATION);
    return;
  }
  if(strGameEXE.IsEmpty()) {
    MessageBox(_T("Please specify a game EXE filename"), _T("Build Driver"), MB_OK|MB_ICONEXCLAMATION);
    return;
  }

  FILE *f;

  /*
  ** Read the PSF driver file
  */
  f = fopen(strStub, "rb");
  if(!f) {
    CString s;
    s.Format(_T("Couldn't open stub file:\n%s"), strStub);
    MessageBox(s, _T("Build Driver"), MB_OK|MB_ICONHAND);
    return;
  }
  { fseek(f,0,SEEK_END);
    int l=ftell(f);
    fseek(f,0,SEEK_SET);
    if(l > 0x200000) { l = 0x200000; }
    stub.SetSize(l);
  }
  fread(stub.GetData(), 1, stub.GetSize(), f);
  fclose(f);

  CPSFDriverInfo driverinfo;
  if(!driverinfo.FindInBuffer(stub.GetData(), stub.GetSize(), 0xFFFFFFFF)) {
    CString s;
    s.Format(_T("Couldn't find driver information in the stub file:\n%s"), strStub);
    MessageBox(s, _T("Build Driver"), MB_OK|MB_ICONHAND);
    return;
  }

  /*
  ** Read the game EXE
  */
  CPSXEXE exe;
  if(!exe.ReadFromEXEFile(strGameEXE, TRUE)) {
    CString s;
    s.Format(_T("Error reading game EXE:\n%s\n%s"), strGameEXE, (LPCTSTR)exe.GetLastError());
    MessageBox(s, _T("Build Driver"), MB_OK|MB_ICONHAND);
    return;
  }
  DWORD checkcrc = exe.GetCRC();
  if(exe.GetCRC() != driverinfo.m_dwGameEXECRC) {
    CString s;
    s.Format(_T("CRC does not match in the specified EXE:\n%s"), strGameEXE);
    MessageBox(s, _T("Build Driver"), MB_OK|MB_ICONHAND);
    return;
  }

  /*
  ** Copy stub and patch the jump address
  */
  exe.WriteAddressRange(driverinfo.m_dwDriverLoadAddress, stub.GetData(), stub.GetSize());
  { BYTE v[4];
    Util::Put32LSB(v,
      (0x02<<26) | ((driverinfo.m_dwDriverEntryAddress & 0x0FFFFFFF)>>2)
    );
    exe.WriteAddressRange(driverinfo.m_dwJumpPatchAddress, v, 4);
  }
  /*
  ** Write zero bytes at the beginning/end of each song-specific area
  ** to make the EXE grow to the necessary size
  */
  int i;
  for(i = 0; i < driverinfo.m_dwaSongSpecificStarts.GetSize(); i++) {
    BYTE t = 0;
    exe.WriteAddressRange(driverinfo.m_dwaSongSpecificStarts.GetAt(i), &t, 1);
    exe.WriteAddressRange(
      (driverinfo.m_dwaSongSpecificStarts.GetAt(i) +
        driverinfo.m_dwaSongSpecificLengths.GetAt(i)) - 1,
      &t, 1
    );
  }

  /*
  ** Driver is now built
  */

  /*
  ** Get a save filename
  */
  CFileDialog dlg(
    FALSE, // save
    NULL,
    NULL,
    OFN_OVERWRITEPROMPT|
      OFN_HIDEREADONLY,
    _T("PSF drivers (*.exe)|*.exe|All files (*.*)|*.*||"),
    this
  );
  dlg.m_ofn.lpstrTitle = _T("Build");
  if(dlg.DoModal() != IDOK) return;
  CString strBuildDriverPath = dlg.GetPathName();
  if(strBuildDriverPath.IsEmpty()) return;

  /*
  ** Save
  */
  BOOL b = exe.WriteToEXEFile(strBuildDriverPath);
  if(!b) {
    CString s;
    s.Format(_T("Error saving %s:\n%s"), strBuildDriverPath, (LPCTSTR)exe.GetLastError());
    MessageBox(s, _T("Build Driver"), MB_OK|MB_ICONHAND);
    return;
  }

	CDialog::OnOK();
}

void CBuildDriverDlg::UpdatePSFDRVInfo()
{
  CString strFile;
  GetDlgItemText(IDC_PSFDRV_FILE, strFile);
  if(strFile.IsEmpty()) {
    SetDlgItemText(IDC_PSFDRV_INFO, _T(""));
    return;
  }

  FILE *f=fopen(strFile,"rb");
  if(!f) {
    SetDlgItemText(IDC_PSFDRV_INFO, _T(""));
    return;
  }
  fseek(f,0,SEEK_END);
  int fl = ftell(f);
  fseek(f,0,SEEK_SET);
  if(fl > 0x1F0000) fl = 0x1F0000;
  if(fl < 1) fl = 1;

  CByteArray buf;
  buf.SetSize(fl);
  fread(buf.GetData(),1,buf.GetSize(),f);
  fclose(f);
  CPSFDriverInfo driverinfo;
  BOOL b = driverinfo.FindInBuffer(buf.GetData(), buf.GetSize(), 0xFFFFFFFF);

  if(!b) {
    CString c;
    c.Format(_T("[%s]"), (LPCTSTR)driverinfo.GetLastError());
    SetDlgItemText(IDC_PSFDRV_INFO, (LPCTSTR)c);
    return;
  }

  CString strInfo;

  CString s;
  s.Format(_T("%s\nOriginal game EXE: %s (CRC=0x%08X)\n"),
    driverinfo.m_strDriverText,
    driverinfo.m_strGameEXE,
    driverinfo.m_dwGameEXECRC
  );
  strInfo += s;
  
  s.Format(_T("load=0x%08X entry=0x%08X patch=0x%08X\n"),
    driverinfo.m_dwDriverLoadAddress,
    driverinfo.m_dwDriverEntryAddress,
    driverinfo.m_dwJumpPatchAddress
  );
  strInfo += s;
  strInfo += _T("Song-specific areas:\n");
  int n = driverinfo.m_dwaSongSpecificStarts.GetSize();
  if(!n) {
    strInfo += _T("(none specified)\n");
  } else {
    int i;
    for(i = 0; i < n; i++) {
      if((i >= 2) && (n > 3)) {
        strInfo += _T("(more)\n");
        break;
      }
      DWORD st = driverinfo.m_dwaSongSpecificStarts.GetAt(i);
      DWORD ln = driverinfo.m_dwaSongSpecificLengths.GetAt(i);
      s.Format(_T("0x%08X...0x%08X\n"), st, (st+ln-1));
      strInfo += s;
    }
  }

  SetDlgItemText(IDC_PSFDRV_INFO, strInfo);
}

void CBuildDriverDlg::OnKillfocusPsfdrvFile() 
{
	// TODO: Add your control notification handler code here
	UpdatePSFDRVInfo();
}
