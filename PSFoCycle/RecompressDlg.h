#if !defined(AFX_RECOMPRESSDLG_H__C47CC6A9_2EFA_4098_BA93_FB79470B400D__INCLUDED_)
#define AFX_RECOMPRESSDLG_H__C47CC6A9_2EFA_4098_BA93_FB79470B400D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RecompressDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRecompressDlg dialog

class CRecompressDlg : public CDialog
{
// Construction
public:
	BOOL m_bSmallerOnly;
	UINT m_uMethod;
	CRecompressDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRecompressDlg)
	enum { IDD = IDD_RECOMPRESS };
	CButton	m_cSmallerOnly;
	CListBox	m_cMethodList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRecompressDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRecompressDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RECOMPRESSDLG_H__C47CC6A9_2EFA_4098_BA93_FB79470B400D__INCLUDED_)
