// RecompressDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "RecompressDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRecompressDlg dialog


CRecompressDlg::CRecompressDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRecompressDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRecompressDlg)
	//}}AFX_DATA_INIT

  m_uMethod = 0;
  m_bSmallerOnly = TRUE;
}

void CRecompressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRecompressDlg)
	DDX_Control(pDX, IDC_SMALLERONLY, m_cSmallerOnly);
	DDX_Control(pDX, IDC_METHODLIST, m_cMethodList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRecompressDlg, CDialog)
	//{{AFX_MSG_MAP(CRecompressDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRecompressDlg message handlers

BOOL CRecompressDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  UINT uMethodCount   = CPSFCompressor::GetMethodCount();
  UINT uDefaultMethod = CPSFCompressor::GetDefaultMethod();
  UINT uSafeMethod    = CPSFCompressor::GetSafeMethod();
  for(UINT u = 0; u < uMethodCount; u++) {
    CString s = CPSFCompressor::GetMethodVersion(u);
    if(u == uDefaultMethod) s += _T(" (default)");
    if(u == uSafeMethod   ) s += _T(" (safe)");
    m_cMethodList.AddString(s);
  }

  m_cMethodList.SetCurSel(m_uMethod);
  m_cSmallerOnly.SetCheck(m_bSmallerOnly ? BST_CHECKED : BST_UNCHECKED);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRecompressDlg::OnOK() 
{
  int s = m_cMethodList.GetCurSel();
  if(s == LB_ERR) return;
  if(s < 0) return;
  if(s >= CPSFCompressor::GetMethodCount()) return;
  m_uMethod = s;
  m_bSmallerOnly = (m_cSmallerOnly.GetCheck() == BST_CHECKED);

	CDialog::OnOK();
}
