// EmuSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "EmuSettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEmuSettingsDlg dialog


CEmuSettingsDlg::CEmuSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEmuSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEmuSettingsDlg)
	m_nCompat = -1;
	//}}AFX_DATA_INIT
}


void CEmuSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEmuSettingsDlg)
	DDX_Radio(pDX, IDC_HARSH, m_nCompat);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEmuSettingsDlg, CDialog)
	//{{AFX_MSG_MAP(CEmuSettingsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEmuSettingsDlg message handlers

BOOL CEmuSettingsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
