// PSFDriverInfo.h: interface for the CPSFDriverInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PSFDRIVERINFO_H__BD6D3D75_8A07_4670_A37F_337F64FCE21A__INCLUDED_)
#define AFX_PSFDRIVERINFO_H__BD6D3D75_8A07_4670_A37F_337F64FCE21A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CPSFDriverInfo  
{
public:
	LPCTSTR GetLastError();
	void Clear();
	BOOL FindInBuffer(LPBYTE lpBuffer, DWORD dwBufferSize, DWORD dwAddressOfBuffer);
	BOOL m_bExists;
	DWORD m_dwDriverEntryAddress;
	DWORD m_dwDriverLoadAddress;
	CDWordArray m_dwaSongSpecificLengths;
	CDWordArray m_dwaSongSpecificStarts;
	DWORD m_dwJumpPatchAddress;
	DWORD m_dwGameEXECRC;
	CString m_strGameEXE;
	CString m_strDriverText;
	CDWordArray m_dwaParamSizes;
	CDWordArray m_dwaParamLocations;
	CStringArray m_saParamNames;
	CPSFDriverInfo();
	virtual ~CPSFDriverInfo();

private:
	LPCTSTR m_lpszLastError;
	BOOL RetrieveString(LPBYTE lpBufferStart, LPBYTE lpBufferEnd, DWORD dwBufferStartAddress, DWORD dwStringAddress, CString &r);
};

#endif // !defined(AFX_PSFDRIVERINFO_H__BD6D3D75_8A07_4670_A37F_337F64FCE21A__INCLUDED_)
