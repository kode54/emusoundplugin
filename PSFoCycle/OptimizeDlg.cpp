// OptimizeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "OptimizeDlg.h"

#include "EmuSettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptimizeDlg dialog


COptimizeDlg::COptimizeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptimizeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptimizeDlg)
	m_bForceLength = FALSE;
	m_bNoSongSpecific = FALSE;
	m_bParanoid = FALSE;
	m_nParanoidBytes = 0;
	m_bRunFixed = FALSE;
	m_strRunFixedTime = _T("");
	m_bRunSmart = FALSE;
	m_strRunSmartTime = _T("");
	m_bSetFade = FALSE;
	m_strFadeTime = _T("");
	m_bSetLength = FALSE;
	//}}AFX_DATA_INIT
}


void COptimizeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptimizeDlg)
	DDX_Check(pDX, IDC_FORCELENGTH, m_bForceLength);
	DDX_Check(pDX, IDC_NOSONGSPECIFIC, m_bNoSongSpecific);
	DDX_Check(pDX, IDC_PARANOID, m_bParanoid);
	DDX_Text(pDX, IDC_PARANOID_BYTES, m_nParanoidBytes);
	DDV_MinMaxInt(pDX, m_nParanoidBytes, 0, 2097152);
	DDX_Check(pDX, IDC_RUNFIXED, m_bRunFixed);
	DDX_Text(pDX, IDC_RUNFIXED_EDIT, m_strRunFixedTime);
	DDX_Check(pDX, IDC_RUNSMART, m_bRunSmart);
	DDX_Text(pDX, IDC_RUNSMART_EDIT, m_strRunSmartTime);
	DDX_Check(pDX, IDC_SETFADE, m_bSetFade);
	DDX_Text(pDX, IDC_SETFADE_SEC, m_strFadeTime);
	DDX_Check(pDX, IDC_SETLENGTH, m_bSetLength);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptimizeDlg, CDialog)
	//{{AFX_MSG_MAP(COptimizeDlg)
	ON_BN_CLICKED(IDC_EMUSETTINGS, OnEmusettings)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptimizeDlg message handlers

void COptimizeDlg::OnEmusettings() 
{
  CEmuSettingsDlg dlg;

  dlg.m_nCompat = goOptions.m_bEmuFriendly ? 1 : 0;
//  dlg.m_nDivider = goOptions.m_nEmuDivider;

  if(dlg.DoModal() != IDOK) return;

  goOptions.m_bEmuFriendly = (dlg.m_nCompat == 0) ? FALSE : TRUE;
//  goOptions.m_nEmuDivider = dlg.m_nDivider;

}

void COptimizeDlg::OnOK() 
{
  if(
    (!IsDlgButtonChecked(IDC_RUNFIXED)) &&
    (!IsDlgButtonChecked(IDC_RUNSMART))
  ) {
    MessageBox(_T("You must check at least one of the options under \"Run time\"."), _T("Optimize"), MB_OK|MB_ICONEXCLAMATION);
    return;
  }

	CDialog::OnOK();
}
