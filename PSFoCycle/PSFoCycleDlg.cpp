// PSFoCycleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "PSFoCycleDlg.h"

#include "OptimizeDlg.h"
#include "OptimizingDlg.h"
#include "BuildDriverDlg.h"
#include "PSFDriverInfo.h"
#include "ParametersDlg.h"
#include "UpgradingDlg.h"
#include "RecompressDlg.h"
#include "RecompressingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_cBanner;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	afx_msg void OnVersiondetails();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_BANNER, m_cBanner);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(IDC_VERSIONDETAILS, OnVersiondetails)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPSFoCycleDlg dialog

CPSFoCycleDlg::CPSFoCycleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPSFoCycleDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPSFoCycleDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

  m_bInterfaceSafeToUpdate = FALSE;
}

void CPSFoCycleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPSFoCycleDlg)
	DDX_Control(pDX, IDC_FILES, m_cFiles);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPSFoCycleDlg, CDialog)
	//{{AFX_MSG_MAP(CPSFoCycleDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUILDDRIVER, OnBuilddriver)
	ON_BN_CLICKED(IDC_OPTIMIZE, OnOptimize)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_SELECTALL, OnSelectall)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_PARAMETERS, OnParameters)
	ON_NOTIFY(NM_DBLCLK, IDC_FILES, OnDblclkFiles)
	ON_BN_CLICKED(IDC_UPGRADEDRIVER, OnUpgradedriver)
	ON_BN_CLICKED(IDC_ABOUT, OnAbout)
	ON_BN_CLICKED(IDC_RECOMPRESS, OnRecompress)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPSFoCycleDlg message handlers

BOOL CPSFoCycleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

  RECT r;
  m_cFiles.GetClientRect(&r);

  m_cFiles.InsertColumn(0, _T("Filename"));
  m_cFiles.InsertColumn(1, _T("Driver info"));
  m_cFiles.SetColumnWidth(0, (r.right-r.left)/2);
  m_cFiles.SetColumnWidth(1, (r.right-r.left)/2);

  StartFindDriverThread();

  m_bInterfaceSafeToUpdate = TRUE;
  UpdateInterface();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPSFoCycleDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPSFoCycleDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPSFoCycleDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CPSFoCycleDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CPSFoCycleDlg::UpdateInterface()
{
  //m_csList.Lock();
  UINT u;
  u = m_cFiles.GetSelectedCount();
  GetDlgItem(IDC_REMOVE       )->EnableWindow(u?TRUE:FALSE);
  GetDlgItem(IDC_OPTIMIZE     )->EnableWindow(u?TRUE:FALSE);
  GetDlgItem(IDC_PARAMETERS   )->EnableWindow((u==1)?TRUE:FALSE);
  GetDlgItem(IDC_UPGRADEDRIVER)->EnableWindow(u?TRUE:FALSE);
  GetDlgItem(IDC_RECOMPRESS   )->EnableWindow(u?TRUE:FALSE);
  u = m_cFiles.GetItemCount();
  GetDlgItem(IDC_SELECTALL    )->EnableWindow(u?TRUE:FALSE);
  //m_csList.Unlock();
}

void CPSFoCycleDlg::OnBuilddriver() 
{
  CBuildDriverDlg dlg;
  dlg.DoModal();
}

void CPSFoCycleDlg::OnOptimize() 
{
  CStringArray sa;
  GetFileSelectedList(sa);

  COptimizeDlg dlg;

  dlg.m_bRunFixed = goOptions.m_bRunFixed;
  dlg.m_bRunSmart = goOptions.m_bRunSmart;
  dlg.m_strRunFixedTime = Util::NumToString(goOptions.m_dRunFixedSeconds, TRUE);
  dlg.m_strRunSmartTime = Util::NumToString(goOptions.m_dRunSmartSeconds, TRUE);

  dlg.m_bNoSongSpecific = goOptions.m_bDoNotOptimizeSongSpecific;
  dlg.m_bParanoid = goOptions.m_bAuditParanoid;
  dlg.m_nParanoidBytes = goOptions.m_nAuditParanoidBytes;

  dlg.m_bSetLength = goOptions.m_bSetLength;
  dlg.m_bSetFade = goOptions.m_bSetFade;
  dlg.m_strFadeTime = Util::NumToString(goOptions.m_dFadeSeconds, TRUE);
  dlg.m_bForceLength = goOptions.m_bForceLength;

  if(dlg.DoModal() != IDOK) return;

  goOptions.m_bRunFixed = dlg.m_bRunFixed;
  goOptions.m_bRunSmart = dlg.m_bRunSmart;
  Util::StringToNum(dlg.m_strRunFixedTime, goOptions.m_dRunFixedSeconds, TRUE);
  Util::StringToNum(dlg.m_strRunSmartTime, goOptions.m_dRunSmartSeconds, TRUE);

  goOptions.m_bDoNotOptimizeSongSpecific = dlg.m_bNoSongSpecific;
  goOptions.m_bAuditParanoid = dlg.m_bParanoid;
  goOptions.m_nAuditParanoidBytes = dlg.m_nParanoidBytes;

  goOptions.m_bSetLength = dlg.m_bSetLength;
  goOptions.m_bSetFade   = dlg.m_bSetFade;
  Util::StringToNum(dlg.m_strFadeTime, goOptions.m_dFadeSeconds, TRUE);
  goOptions.m_bForceLength = dlg.m_bForceLength;

  /*
  ** Now optimize!
  */
  PerformOptimize(sa);

}

void CPSFoCycleDlg::OnAdd() 
{
  CFileDialog dlg(
    TRUE, // open
    NULL,
    NULL,
    OFN_ALLOWMULTISELECT|
      OFN_FILEMUSTEXIST|
      OFN_HIDEREADONLY,
    _T("Playstation Sound Format files|*.psf;*.minipsf;*.psf1;*.psf2;*.minipsf2|All files (*.*)|*.*||"),
    this
  );
  dlg.m_ofn.lpstrTitle = _T("Add");

  LPTSTR mybuffer = new TCHAR[100001];
  mybuffer[0] = 0;

  dlg.m_ofn.lpstrFile = mybuffer;
  dlg.m_ofn.nMaxFile = 100000;

  if(dlg.DoModal() != IDOK) {
    delete[] mybuffer;
    return;
  }

  POSITION p = dlg.GetStartPosition();
  if(p) {
    CWaitCursor cwc;
    while(p) {
      //TRACE("p %08X\n",(DWORD)p);
      CString s = dlg.GetNextPathName(p);
      if(s.IsEmpty()) continue;
      AddFile(s);
    }
  }

  delete[] mybuffer;
}

BOOL __stdcall AfxFullPath(LPTSTR lpszPathOut, LPCTSTR lpszFileIn);

void CPSFoCycleDlg::AddFile(LPCTSTR lpszPathName)
{
  TCHAR fqpath[MAX_PATH + 1];
  AfxFullPath(fqpath, lpszPathName);
  //TRACE("addfile %s %s\n",lpszPathName,fqpath);
  /*
  ** Search for the pathname in the list already
  */
  //m_csList.Lock();
  int nExistingIndex = FindPathNameInList(fqpath);
  if(nExistingIndex < 0) {
    /*
    ** Insert the new unique item
    */
    int newindex = m_cFiles.InsertItem(0, FileNameOnly(fqpath));
    if(newindex >= 0) {
      LPTSTR copydata = new TCHAR[lstrlen(fqpath) + 1];
      lstrcpy(copydata, fqpath);
      m_cFiles.SetItemData(newindex, (DWORD)copydata);
      m_cFiles.SetItemText(newindex, 1, _T(""));
      m_cFiles.SetItemState(newindex, LVIS_SELECTED, LVIS_SELECTED);
    }
  }
//  m_csList.Unlock();
}

CString CPSFoCycleDlg::FileNameOnly(LPCTSTR lpszPathName)
{
  LPCTSTR lastslash = NULL;
  LPCTSTR t;
  for(t = lpszPathName; *t; t++) {
    switch(*t) {
    case _T('/'):
    case _T('\\'):
      lastslash = t;
      break;
    }
  }
  if(lastslash) {
    lastslash++;
  } else {
    lastslash = lpszPathName;
  }
  return (CString)lastslash;
}

void CPSFoCycleDlg::OnDestroy() 
{
  m_bInterfaceSafeToUpdate = FALSE;
  StopFindDriverThread();
  int count = m_cFiles.GetItemCount();
  int i;
  for(i = 0; i < count; i++) {
    DWORD data = m_cFiles.GetItemData(i);
    if(data) {
      LPTSTR copydata = (LPTSTR)data;
      delete[] copydata;
    }
  }
	CDialog::OnDestroy();
}

BOOL CPSFoCycleDlg::GetDriverStringFromFile(LPCTSTR lpszPathName, CString &r)
{
  BOOL b;

  CPSF psf;
  b = psf.ReadFromFile(lpszPathName,
    CPSF::PSF_FILE_VERSION |
    CPSF::PSF_FILE_ZEXE,
    TRUE
  );
  if(!b) {
    r.Format(_T("(%s)"), (LPCTSTR)psf.GetLastError());
    return TRUE;
  }

  CPSXEXE exe;
  b = exe.ReadFromPSF(psf, TRUE);
  if(!b) {
    r.Format(_T("(%s)"), (LPCTSTR)exe.GetLastError());
    return TRUE;
  }

  LPBYTE lpEXEText     = exe.GetTextBuffer();
  DWORD dwEXETextStart = exe.GetTextStart();
  DWORD dwEXETextSize  = exe.GetTextSize();

  CPSFDriverInfo driverinfo;

  b = driverinfo.FindInBuffer(lpEXEText, dwEXETextSize, dwEXETextStart);
  if(!b) {
    r.Format(_T("(%s)"), (LPCTSTR)driverinfo.GetLastError());
    return TRUE;
  }

  r = (LPCTSTR)(driverinfo.m_strDriverText);
  return TRUE;
}

void CPSFoCycleDlg::OnSelectall() 
{
  //m_csList.Lock();
  int n = m_cFiles.GetItemCount();
  for(int i = 0; i < n; i++) m_cFiles.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
  //m_csList.Unlock();
}

void CPSFoCycleDlg::OnRemove() 
{
  //m_csList.Lock();
  for(;;) {
    UINT n = m_cFiles.GetSelectedCount();
    if(!n) return;
    POSITION pos = m_cFiles.GetFirstSelectedItemPosition();
    if(!pos) return;
    int nIndex = m_cFiles.GetNextSelectedItem(pos);
    DWORD data = m_cFiles.GetItemData(nIndex);
    LPTSTR copydata = (LPTSTR)data;
    if(copydata) { delete[] copydata; }
    m_cFiles.DeleteItem(nIndex);
  }
  //m_csList.Unlock();
}

/*
void CPSFoCycleDlg::GetFileList(CStringArray &r)
{
  m_csList.Lock();
  r.SetSize(0);
  int n = m_cFiles.GetItemCount();
  for(int i = 0; i < n; i++) {
    DWORD data = m_cFiles.GetItemData(i);
    if(data) r.Add((LPCTSTR)data);
  }
  m_csList.Unlock();
}
*/

void CPSFoCycleDlg::PerformOptimize(CStringArray &saFiles)
{
  COptimizingDlg dlg;

  /*
  ** Copy file list
  */
  int nFiles = saFiles.GetSize();
  int i;
  for(i = 0; i < nFiles; i++) { dlg.m_saFiles.Add(saFiles.GetAt(i)); }

  /*
  ** Perform the optimizations
  */
  dlg.DoModal();
  InvalidateDriverStrings(dlg.m_saFilesChanged);
}

void CPSFoCycleDlg::InvalidateDriverStrings(CStringArray &saFilesChanged)
{
  for(int i = 0; i < saFilesChanged.GetSize(); i++) {
    InvalidateDriverString(saFilesChanged.GetAt(i));
  }
}

void CPSFoCycleDlg::InvalidateDriverString(LPCTSTR lpszPathName)
{
  //m_csList.Lock();
  int nIndex = FindPathNameInList(lpszPathName);
  if(nIndex >= 0) {
    m_cFiles.SetItemText(nIndex, 1, _T(""));
  }
  //m_csList.Unlock();
}

int CPSFoCycleDlg::FindPathNameInList(LPCTSTR lpszPathName)
{
  TCHAR fqpath[MAX_PATH + 1];
  AfxFullPath(fqpath, lpszPathName);
  int count = m_cFiles.GetItemCount();
  int i;
  for(i = 0; i < count; i++) {
    DWORD data = m_cFiles.GetItemData(i);
    if(data) {
      if(!lstrcmpi(fqpath, (LPCTSTR)data)) return i;
    }
  }
  return -1;
}

/***************************************************************************/
/*
** Auditing thread
*/
//UINT AFX_CDECL CPSFoCycleDlg__FindDriverThread(LPVOID lpParam) {
static unsigned long __stdcall CPSFoCycleDlg__FindDriverThread(LPVOID lpParam) {
  CPSFoCycleDlg *me = (CPSFoCycleDlg*)lpParam;
  ASSERT(me);
  /*
  ** Signal thread start
  */
  me->m_csFindDriverThread.Lock();
  me->m_evFindDriverThreadStarted.SetEvent();

  TCHAR szFileName[MAX_PATH + 1];
  TCHAR szDriverText[1001];
  szFileName[0] = 0;
  szDriverText[0] = 0;
  /*
  ** Loop
  */
  while(!me->m_dwFindDriverThreadKillFlag) {
    /*
    ** If we're not replying with any driver text,
    ** don't confuse the main thread by supplying a filename
    */
    if(!(szDriverText[0])) { szFileName[0] = 0; }
    /*
    ** Ask the main thread what to do / send it results
    */
    me->SendMessage(WM_USER, (WPARAM)szFileName, (LPARAM)szDriverText);
    /*
    ** If we were given a filename request, try to fill it
    */
    if(szFileName[0]) {
      CString strDriver;
      if(me->GetDriverStringFromFile(szFileName, strDriver)) {
        /*
        ** Avoid empty strings
        */
        if(strDriver.IsEmpty()) { strDriver = _T("(empty driver string)"); }
      } else {
        strDriver = _T("(No driver info found)");
      }
      if(strDriver.GetLength() > 1000) { strDriver = strDriver.Left(1000); }
      lstrcpy(szDriverText, strDriver);
    /*
    ** Otherwise, clear the driver text and sleep
    */
    } else {
      szDriverText[0] = 0;
      Sleep(100);
    }
  }
  /*
  ** Signal thread end; return
  */
  me->m_csFindDriverThread.Unlock();
  return 0;
}

void CPSFoCycleDlg::StartFindDriverThread()
{
  StopFindDriverThread();
  m_evFindDriverThreadStarted.ResetEvent();
  m_dwFindDriverThreadKillFlag = 0;

DWORD tid;
HANDLE h = ::CreateThread(NULL,0,CPSFoCycleDlg__FindDriverThread,(LPVOID)this,0,&tid);
::SetThreadPriority(h, THREAD_PRIORITY_LOWEST);

  //AfxBeginThread(CPSFoCycleDlg__FindDriverThread, (LPVOID)this);
  m_evFindDriverThreadStarted.Lock();
}

void CPSFoCycleDlg::StopFindDriverThread()
{
  m_dwFindDriverThreadKillFlag = 1;
  m_csFindDriverThread.Lock();
  m_csFindDriverThread.Unlock();
}

void CPSFoCycleDlg::GetFileSelectedList(CStringArray &r)
{
  r.SetSize(0);
  //m_csList.Lock();
  POSITION pos = m_cFiles.GetFirstSelectedItemPosition();
  while(pos) {
    int nIndex = m_cFiles.GetNextSelectedItem(pos);
    DWORD data = m_cFiles.GetItemData(nIndex);
    if(data) { r.Add((LPCTSTR)data); }
  }
  //m_csList.Unlock();
}

void CPSFoCycleDlg::FindDriverThreadMachine(LPTSTR lpszFileName, LPTSTR lpszDriverText)
{
  if(!lpszFileName) return;

  /*
  ** If we're being given information, use it
  */
  if(lpszDriverText && (*lpszDriverText) && (*lpszFileName)) {
    /*
    ** Hopefully the filename is still in there
    */
    int nIndex = FindPathNameInList(lpszFileName);
    if(nIndex >= 0) {
      /*
      ** If it is, set the new driver string
      */
      m_cFiles.SetItemText(nIndex, 1, lpszDriverText);
    }
  }

  *lpszFileName = 0;

  CString strFileName;
  strFileName.Empty();
  /*
  ** Look for any items that have blank subcolumn strings
  */
  int nItems = m_cFiles.GetItemCount();
  int i;
  for(i = 0; i < nItems; i++) {
    CString s = m_cFiles.GetItemText(i, 1);
    if(s.IsEmpty()) {
      /* Must also have valid copydata! */
      DWORD d = m_cFiles.GetItemData(i);
      if(d) {
        strFileName = ((LPCTSTR)d);
        break;
      }
    }
  }

  /*
  ** If we have a filename to work with, work with it
  */
  if(!(strFileName.IsEmpty())) {
    if(strFileName.GetLength() > MAX_PATH) { strFileName = strFileName.Left(MAX_PATH); }
    lstrcpy(lpszFileName, strFileName);
  /*
  ** Otherwise set the filename to empty
  */
  } else {
    *lpszFileName = 0;
  }

}

LRESULT CPSFoCycleDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
  if(m_bInterfaceSafeToUpdate) {
    UpdateInterface();
  }
  if(message == WM_USER) {
    FindDriverThreadMachine((LPTSTR)wParam, (LPTSTR)lParam);
  }
	return CDialog::WindowProc(message, wParam, lParam);
}

void CPSFoCycleDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	StopFindDriverThread();
	CDialog::OnOK();
//	CDialog::OnCancel();
}

void CPSFoCycleDlg::OnParameters() 
{
  CStringArray sa;
  GetFileSelectedList(sa);
  if(sa.GetSize() != 1) return;
//TRACE("%d\n",sa.GetSize());
//TRACE("%s\n",sa.GetAt(0));
  CParametersDlg dlg;
  dlg.m_lpszFile = (LPCTSTR)(sa.GetAt(0));
  dlg.DoModal();
}

void CPSFoCycleDlg::OnDblclkFiles(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnParameters();

	*pResult = 0;
}

void CPSFoCycleDlg::OnUpgradedriver() 
{
  CStringArray sa;
  GetFileSelectedList(sa);

  CString strDriverPathName;
  { CFileDialog fdlg(
      TRUE, // open
      NULL,
      NULL,
        OFN_FILEMUSTEXIST|
        OFN_HIDEREADONLY,
      _T("PSF drivers (*.exe)|*.exe|All files (*.*)|*.*||"),
      this
    );
    fdlg.m_ofn.lpstrTitle = _T("Select replacement driver");
    if(fdlg.DoModal() != IDOK) return;
    strDriverPathName = fdlg.GetPathName();
  }

  CPSXEXE exeDriver;
  if(!exeDriver.ReadFromEXEFile(strDriverPathName, TRUE)) {
    CString s;
    s.Format(_T("Unable to read %s\n%s"), strDriverPathName, (LPCTSTR)exeDriver.GetLastError());
    MessageBox(s, _T("Upgrade Driver"), MB_OK|MB_ICONHAND);
    return;
  }

  BOOL b;

  CPSFDriverInfo driverinfo;
  b = driverinfo.FindInBuffer(
    exeDriver.GetTextBuffer(),
    exeDriver.GetTextSize(),
    exeDriver.GetTextStart()
  );
  if(!b) {
    CString s;
    s.Format(_T("%s\n%s"), strDriverPathName, (LPCTSTR)driverinfo.GetLastError());
    MessageBox(s, _T("Upgrade Driver"), MB_OK|MB_ICONHAND);
    return;
  }

  CString s;
  s.Format(
    _T("Are you sure you want to upgrade all the selected PSFs to:\n\"%s\"?"),
    driverinfo.m_strDriverText
  );
  if(MessageBox(s, _T("Confirm"), MB_YESNO|MB_ICONEXCLAMATION) != IDYES) {
    return;
  }

  CUpgradingDlg dlg;
  dlg.m_pDriverEXE = &exeDriver;
  dlg.m_pDriverInfo = &driverinfo;

  int i;
  for(i = 0; i < sa.GetSize(); i++) {
    dlg.m_saFiles.Add(sa.GetAt(i));
  }
  dlg.DoModal();

  InvalidateDriverStrings(dlg.m_saFilesChanged);
}

void CAboutDlg::OnVersiondetails() 
{
	CString s;

  s += _T("Application banner string:\n  ");
  s += g_lpszPSFoCycleBanner;
  s += _T("\n");

  s += _T("\n");

  s += _T("Emulation core:\n  ");
  { const char *t = emu_getversion();
    for(; *t; t++) s += (TCHAR)(*t);
  }
  s += _T("\n");

  s += _T("\n");

  s += _T("Utility library:\n  ");
  s += Util::GetVersion();
  s += _T("\n");

  s += _T("\n");

  s += _T("Compression:\n");
  UINT uMethodCount = CPSFCompressor::GetMethodCount();
  for(UINT u = 0; u < uMethodCount; u++) {
    s += _T("  ");
    s += CPSFCompressor::GetMethodVersion(u);
    if(u == CPSFCompressor::GetDefaultMethod()) s += _T(" (default)");
    if(u == CPSFCompressor::GetSafeMethod   ()) s += _T(" (safe)");
    s += _T("\n");
  }

  s += _T("\n");

  s += _T("Decompression:\n  ");
  s += CPSFDecompressor::GetVersion();

  MessageBox(s, _T("Version details"));

}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

  CString strBanner;

  strBanner = g_lpszPSFoCycleBanner;
  strBanner += _T("\n\nCopyright 2003 Neill Corlett");

  m_cBanner.SetWindowText(strBanner);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPSFoCycleDlg::OnAbout() 
{
  CAboutDlg dlgAbout;
  dlgAbout.DoModal();
}

void CPSFoCycleDlg::OnRecompress() 
{
  CStringArray sa;
  GetFileSelectedList(sa);

  UINT uMethod;
  BOOL bOnlyIfSmaller;

  uMethod = CPSFCompressor::GetDefaultMethod();
  UINT uMethodCount = CPSFCompressor::GetMethodCount();
  for(UINT u = 0; u < uMethodCount; u++) {
    if(!lstrcmpi(CPSFCompressor::GetMethodVersion(u), goOptions.m_strRecompressMethod)) {
      uMethod = u;
      break;
    }
  }
  bOnlyIfSmaller = goOptions.m_bRecompressOnlyIfSmaller;

  { CRecompressDlg dlg;
    dlg.m_uMethod = uMethod;
    dlg.m_bSmallerOnly = bOnlyIfSmaller;
    if(dlg.DoModal() != IDOK) return;
    uMethod = dlg.m_uMethod;
    bOnlyIfSmaller = dlg.m_bSmallerOnly;
  }

  goOptions.m_strRecompressMethod = CPSFCompressor::GetMethodVersion(uMethod);
  goOptions.m_bRecompressOnlyIfSmaller = bOnlyIfSmaller;

  CRecompressingDlg dlg;
  dlg.m_uMethod = uMethod;
  dlg.m_bOnlyIfSmaller = bOnlyIfSmaller;
  for(int i = 0; i < sa.GetSize(); i++) {
    dlg.m_saFiles.Add(sa.GetAt(i));
  }
  dlg.DoModal();

  InvalidateDriverStrings(dlg.m_saFilesChanged);
}
