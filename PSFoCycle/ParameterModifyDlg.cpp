// ParameterModifyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "ParameterModifyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParameterModifyDlg dialog


CParameterModifyDlg::CParameterModifyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CParameterModifyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CParameterModifyDlg)
	m_strName = _T("");
	m_dwValue = 0;
	//}}AFX_DATA_INIT
}


void CParameterModifyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParameterModifyDlg)
	DDX_Text(pDX, IDC_NAME, m_strName);
	DDX_Text(pDX, IDC_VALUE, m_dwValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CParameterModifyDlg, CDialog)
	//{{AFX_MSG_MAP(CParameterModifyDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParameterModifyDlg message handlers
