// GlobalOptions.h: interface for the CGlobalOptions class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLOBALOPTIONS_H__5C75FCEA_C5FC_4C0E_8658_76512728E040__INCLUDED_)
#define AFX_GLOBALOPTIONS_H__5C75FCEA_C5FC_4C0E_8658_76512728E040__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGlobalOptions  
{
public:
	BOOL m_bRecompressOnlyIfSmaller;
	CString m_strRecompressMethod;
  double m_dFadeSeconds;
	BOOL m_bForceLength;
	BOOL m_bSetFade;
	BOOL m_bSetLength;
  double m_dRunSmartSeconds;
  double m_dRunFixedSeconds;
	BOOL m_bRunSmart;
	BOOL m_bRunFixed;
	BOOL m_bDoNotOptimizeSongSpecific;
	int m_nAuditParanoidBytes;
	BOOL m_bAuditParanoid;
	void Save();
	void Load();
//int m_nEmuDivider;
	BOOL m_bEmuFriendly;
	CGlobalOptions();
	virtual ~CGlobalOptions();

};

extern CGlobalOptions goOptions;

#endif // !defined(AFX_GLOBALOPTIONS_H__5C75FCEA_C5FC_4C0E_8658_76512728E040__INCLUDED_)
