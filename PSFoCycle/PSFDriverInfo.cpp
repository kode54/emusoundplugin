// PSFDriverInfo.cpp: implementation of the CPSFDriverInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PSFoCycle.h"
#include "PSFDriverInfo.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPSFDriverInfo::CPSFDriverInfo()
{
  m_bExists = FALSE;
  // should be safe and make sure this is never NULL
  m_lpszLastError = _T("");
}

CPSFDriverInfo::~CPSFDriverInfo()
{

}

//
// Returns FALSE on error and sets m_strLastError
//
BOOL CPSFDriverInfo::FindInBuffer(
  LPBYTE lpBuffer,
  DWORD dwBufferSize,
  DWORD dwAddressOfBuffer
) {
  Clear();

  LPBYTE lpBufferStart = lpBuffer;
  LPBYTE lpBufferEnd   = lpBuffer + dwBufferSize;
  LPBYTE lpSearch;
  //
  // Find the signature
  //
  for(lpSearch = lpBuffer; (lpSearch + 16) <= lpBufferEnd; lpSearch++) {
    if(*lpSearch != 'P') continue;
    if(!memcmp(lpSearch, "PSF_DRIVER_INFO:", 16)) break;
  }
  if((lpSearch + 16) > lpBufferEnd) {
    Clear();
    m_lpszLastError = _T("No driver info found");
    return FALSE;
  }
  lpSearch += 16;

  //
  // At this point, all errors are of the "corrupt" type, so just set that here
  //
  m_lpszLastError = _T("Driver info is invalid or corrupt");

  //
  // Read information
  //
  if((lpSearch + 24) > lpBufferEnd) { Clear(); return FALSE; }
  m_dwDriverLoadAddress  = Util::Get32LSB(lpSearch); lpSearch += 4;
  m_dwDriverEntryAddress = Util::Get32LSB(lpSearch); lpSearch += 4;
  DWORD dwDriverText     = Util::Get32LSB(lpSearch); lpSearch += 4;
  DWORD dwGameEXE        = Util::Get32LSB(lpSearch); lpSearch += 4;
  m_dwGameEXECRC         = Util::Get32LSB(lpSearch); lpSearch += 4;
  m_dwJumpPatchAddress   = Util::Get32LSB(lpSearch); lpSearch += 4;

  /*
  ** If the address of the buffer was given as 0xFFFFFFFF (we don't know what it is), 
  ** replace it now with the DriverLoadAddress
  */
  if(dwAddressOfBuffer == 0xFFFFFFFF) {
    dwAddressOfBuffer = m_dwDriverLoadAddress;
  }

  /*
  ** Sanity checking on load address
  */
  if(m_dwDriverLoadAddress < dwAddressOfBuffer) { Clear(); return FALSE; }

  /*
  ** For better range checking on strings, limit the current buffer area
  */
  { DWORD leading_bytes = m_dwDriverLoadAddress - dwAddressOfBuffer;
    if(leading_bytes >= dwBufferSize) { Clear(); return FALSE; }
    lpBuffer          += leading_bytes;
    lpBufferStart     += leading_bytes;
    dwAddressOfBuffer += leading_bytes;
    dwBufferSize      -= leading_bytes;
  }
  /*
  ** To weed out caitsith2's bad PSF_DRIVER_INFO blocks, abort if lpSearch is now out of range
  */
  if((lpSearch < lpBufferStart) || (lpSearch >= lpBufferEnd)) { Clear(); return FALSE; }

  /*
  ** Try to read strings
  */
  if(!RetrieveString(lpBufferStart, lpBufferEnd, dwAddressOfBuffer, dwDriverText, m_strDriverText)) { Clear(); return FALSE; }
  if(!RetrieveString(lpBufferStart, lpBufferEnd, dwAddressOfBuffer, dwGameEXE   , m_strGameEXE   )) { Clear(); return FALSE; }

  /*
  ** Attempt to read song-specific region list
  */
  for(;;) {
    if((lpSearch + 4) > lpBufferEnd) { Clear(); return FALSE; }
    DWORD d = Util::Get32LSB(lpSearch); lpSearch += 4;
    if(!d) break;
    m_dwaSongSpecificStarts.Add(d);
    if((lpSearch + 4) > lpBufferEnd) { Clear(); return FALSE; }
    m_dwaSongSpecificLengths.Add(Util::Get32LSB(lpSearch)); lpSearch += 4;
  }

  /*
  ** Attempt to read parameter list
  */
  for(;;) {
    if((lpSearch + 4) > lpBufferEnd) { Clear(); return FALSE; }
    DWORD d = Util::Get32LSB(lpSearch); lpSearch += 4;
    if(!d) break;
    CString s;
    if(!RetrieveString(lpBufferStart, lpBufferEnd, dwAddressOfBuffer, d, s)) { Clear(); return FALSE; }
    m_saParamNames.Add(s);
    if((lpSearch + 8) > lpBufferEnd) { Clear(); return FALSE; }
    m_dwaParamLocations.Add(Util::Get32LSB(lpSearch)); lpSearch += 4;
    m_dwaParamSizes.Add(Util::Get32LSB(lpSearch)); lpSearch += 4;
  }

  /*
  ** Success
  */
  m_bExists = TRUE;
  return TRUE;
}

BOOL CPSFDriverInfo::RetrieveString(
  LPBYTE lpBufferStart,
  LPBYTE lpBufferEnd,
  DWORD dwBufferStartAddress,
  DWORD dwStringAddress,
  CString &r
) {

//TRACE("RetrieveString(%08X,%08X,%08X,%08X,whatever)\n",lpBufferStart,lpBufferEnd,dwBufferStartAddress,dwStringAddress);

  r.Empty();
  LPBYTE lpHostString = (lpBufferStart - dwBufferStartAddress) + dwStringAddress;
  for(;;) {
    if(
      (lpHostString < lpBufferStart) ||
      (lpHostString >= lpBufferEnd) 
    ) {
      r.Empty();
      return FALSE;
    }
    if(!(*lpHostString)) break;
    r += (TCHAR)(*lpHostString++);
  }
  return TRUE;
}

void CPSFDriverInfo::Clear()
{
  m_bExists = FALSE;
  m_dwaSongSpecificStarts.SetSize(0);
  m_dwaSongSpecificLengths.SetSize(0);
  m_saParamNames.SetSize(0);
  m_dwaParamLocations.SetSize(0);
  m_dwaParamSizes.SetSize(0);
}

LPCTSTR CPSFDriverInfo::GetLastError()
{
  return m_lpszLastError;
}
