// RecompressingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "RecompressingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRecompressingDlg dialog


CRecompressingDlg::CRecompressingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRecompressingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRecompressingDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
  m_uMethod = 0;
  m_bOnlyIfSmaller = TRUE;
}


void CRecompressingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRecompressingDlg)
	DDX_Control(pDX, IDC_PROGRESS, m_cProgress);
	DDX_Control(pDX, IDC_LOGBOX, m_cLogBox);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRecompressingDlg, CDialog)
	//{{AFX_MSG_MAP(CRecompressingDlg)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRecompressingDlg message handlers

void CRecompressingDlg::ReportDone()
{
  SetWindowText(_T("Recompressing (done)"));
  SetDlgItemText(IDC_CURRENTFILE, _T("Done"));
  m_cProgress.SetPos(10000);
  QueueLogLine(_T("Done"));

  CString s;
  if(m_nSuccessfulFiles) {
    s.Format(_T("- %d file%s recompressed"), m_nSuccessfulFiles, m_nSuccessfulFiles==1?_T(""):_T("s"));
    QueueLogLine(s);
  }
  if(m_nErrorFiles) {
    s.Format(_T("- %d error%s"), m_nErrorFiles, m_nErrorFiles==1?_T(""):_T("s"));
    QueueLogLine(s);
  } else {
    QueueLogLine(_T("- No errors"));
  }
  if(m_nSkippedFiles) {
    s.Format(_T("- %d file%s skipped"), m_nSkippedFiles, m_nSkippedFiles==1?_T(""):_T("s"));
    QueueLogLine(s);
  }

  if(m_n64TotalSavings > 0) {
    s = _T("- Total savings: ");
    s += Util::NumToString(m_n64TotalSavings, FALSE);
    s += _T(" bytes");
    QueueLogLine(s);
  }

  // Do nothing more fancy than make the "Cancel" button say "OK"
  SetDlgItemText(IDCANCEL, _T("OK"));

  SyncLogBox();
}

BOOL CRecompressingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
  m_cProgress.SetRange(0, 10000);

  //
  // Initialize file counts
  //
  m_nFile = 0;
  m_nSuccessfulFiles = 0;
  m_nErrorFiles = 0;
  m_nSkippedFiles = 0;

  m_n64TotalSavings = 0;

  m_bFinished = FALSE;

  StartThread();

  UpdateInterface();
  SetTimer(10, 500, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//////////////////////////////////////////////////////////////////////////////
//
// This thread does the actual work
//
static unsigned long __stdcall CRecompressingDlg__Thread(LPVOID lpParam) {
  CRecompressingDlg *me = (CRecompressingDlg*)lpParam;
  ASSERT(me);
  /*
  ** Signal thread start
  */
  me->m_csThreadRunning.Lock();
  me->m_evThreadStarted.SetEvent();
  //
  // Loop
  //
  for(;;) {
    me->m_csLiveInfo.Lock();
    BOOL b;
    b = me->m_bThreadKillFlag;
    me->m_csLiveInfo.Unlock();
    if(b) break;
    //
    // Thread state machine
    //
    CString strPath;
    me->m_csLiveInfo.Lock();
    int n = me->m_nFile;
    if(n < me->m_saFiles.GetSize()) {
      strPath = me->m_saFiles.GetAt(n);
    } else {
      b = TRUE;
    }
    me->m_csLiveInfo.Unlock();
    //
    // If we're out of files, the thread should probably quit
    //
    if(b) break;
    //
    // Perform the recompress on this file
    //
    b = me->PerformRecompress(strPath);
    //
    // Update numbers accordingly
    // If the recompress was a success, add it to the list of files changed
    //
    if(b) {
      me->m_csLiveInfo.Lock();
      me->m_nSuccessfulFiles++;
      me->m_saFilesChanged.Add(strPath);
      me->m_csLiveInfo.Unlock();
    } else {
      if(me->m_bSkip) {
        me->m_csLiveInfo.Lock();
        me->m_nSkippedFiles++;
        me->m_csLiveInfo.Unlock();
      } else {
        me->m_csLiveInfo.Lock();
        me->m_nErrorFiles++;
        me->m_csLiveInfo.Unlock();
      }
    }
    //
    // Increment file number
    //
    me->m_csLiveInfo.Lock();
    if(me->m_nFile < me->m_saFiles.GetSize()) {
      me->m_nFile++;
    }
    me->m_csLiveInfo.Unlock();
    //
    // Signal UI update
    //
    ::PostMessage(me->m_hWnd, WM_USER, 0, 0);
  }
  //
  // Signal thread end
  //
  me->m_csThreadRunning.Unlock();
  return 0;
}

void CRecompressingDlg::StartThread()
{
  StopThread();
  m_evThreadStarted.ResetEvent();
  m_bThreadKillFlag = FALSE;
  DWORD tid;
  ::CreateThread(NULL, 0, CRecompressingDlg__Thread, (LPVOID)this, 0, &tid);
  m_evThreadStarted.Lock();
  m_evThreadStarted.ResetEvent();
}

void CRecompressingDlg::StopThread()
{
  m_csLiveInfo.Lock();
  m_bThreadKillFlag = TRUE;
  m_csLiveInfo.Unlock();
  m_csThreadRunning.Lock();
  m_csThreadRunning.Unlock();
  m_bThreadKillFlag = FALSE;
}

//////////////////////////////////////////////////////////////////////////////
//
// To be called from work thread only
//
void CRecompressingDlg::QueueLogLine(LPCTSTR lpszLine)
{
  CString s;
  CTime t = CTime::GetCurrentTime();
  s += t.Format((LPCSTR)"[%I:%M:%S %p] ");
  s += lpszLine;
  s += _T("\r\n");
  m_csLiveInfo.Lock();
  m_strLogQueue += s;
  m_csLiveInfo.Unlock();
}

//////////////////////////////////////////////////////////////////////////////
//
// To be called from UI thread only
//
void CRecompressingDlg::SyncLogBox()
{
  CString q;
  m_csLiveInfo.Lock();
  q = m_strLogQueue;
  m_strLogQueue.Empty();
  m_csLiveInfo.Unlock();
  if(q.IsEmpty()) return;

  CString s;
  m_cLogBox.GetWindowText(s);
  s += q;
  m_cLogBox.SetWindowText(s);
  m_cLogBox.LineScroll(m_cLogBox.GetLineCount());
}

//////////////////////////////////////////////////////////////////////////////
//
// Front end for QueueLogLine
// Should only be called from the work thread
//
void CRecompressingDlg::WorkLog(LPCTSTR lpszPath, LPCTSTR lpszMessage)
{
  CString s;
  s.Format(_T("%s: %s"), Util::SeekToFileName(lpszPath), lpszMessage);
  QueueLogLine(s);
}

//////////////////////////////////////////////////////////////////////////////
//
// Called from work thread
//
// Returns FALSE on error (this is how they're counted)
//
BOOL CRecompressingDlg::PerformRecompress(LPCTSTR lpszPathName)
{
  BOOL b;

  //
  // (begin by assuming the file will not be skipped)
  //
  m_bSkip = FALSE;

  //
  // Try to read the ZEXE
  //
  CPSF psf;
  b = psf.ReadFromFile(lpszPathName, CPSF::PSF_FILE_ZEXE, TRUE);
  if(!b) {
    WorkLog(lpszPathName, (LPCTSTR)psf.GetLastError());
    return FALSE;
  }

  //
  // Allocate a raw buffer for the ZEXE.
  // Attempt decompression in steps until there's room
  //
  DWORD dwMinSize = 0x00200000; //  2 megs min
  DWORD dwMaxSize = 0x02000000; // 32 megs max

  CByteArray aProgram;
  aProgram.SetSize(dwMinSize);
  for(;;) {
    DWORD dwSize = aProgram.GetSize();
    b = psf.GetProgramData(aProgram.GetData(), &dwSize);
    //
    // on success, shrink the buffer to fit the new data and proceed
    //
    if(b) {
      aProgram.SetSize(dwSize);
      break;
    }
    //
    // on size error, if there's room yet to expand the buffer,
    // expand it and retry
    //
    if(dwSize == (DWORD)(CPSF::PSF_SIZE_ERROR)) {
      dwSize = aProgram.GetSize();
      if(dwSize < dwMaxSize) {
        dwSize *= 2;
        if(dwSize > dwMaxSize) dwSize = dwMaxSize;
        aProgram.SetSize(dwSize);
        continue;
      }
    }
    //
    // otherwise just report the error
    //
    WorkLog(lpszPathName, (LPCTSTR)psf.GetLastError());
    return FALSE;
  }

  DWORD dwCompressedSizeBefore = psf.GetCompressedProgramSize();

  //
  // Attempt recompress
  //
  b = psf.SetProgramData(aProgram.GetData(), aProgram.GetSize(), m_uMethod);
  if(!b) {
    WorkLog(lpszPathName, (LPCTSTR)psf.GetLastError());
    return FALSE;
  }

  //
  // Check the new size
  //
  DWORD dwCompressedSizeAfter = psf.GetCompressedProgramSize();

  CString strSizeReport;
  strSizeReport.Format(_T("(%u->%u bytes)"), dwCompressedSizeBefore, dwCompressedSizeAfter);

  //
  // If the new size is bigger, and we're only rewriting if smaller,
  // then skip this file
  //
  if(m_bOnlyIfSmaller && (dwCompressedSizeAfter >= dwCompressedSizeBefore)) {
    CString s;
    s.Format(_T("Skipping %s"), (LPCTSTR)strSizeReport);
    WorkLog(lpszPathName, s);
    m_bSkip = TRUE;
    return FALSE;
  }

  //
  // Write ZEXE
  //
  b = psf.WriteToFile(lpszPathName, CPSF::PSF_FILE_ZEXE, TRUE);
  if(!b) {
    WorkLog(lpszPathName, (LPCTSTR)psf.GetLastError());
    return FALSE;
  }

  //
  // Update savings
  //
  { INT64 a = dwCompressedSizeAfter;
    INT64 b = dwCompressedSizeBefore;
    m_csLiveInfo.Lock();
    m_n64TotalSavings += (b - a);
    m_csLiveInfo.Unlock();
  }

  CString s; 
  s.Format(_T("OK %s"), (LPCTSTR)strSizeReport);
  WorkLog(lpszPathName, s);
  return TRUE;
}

//////////////////////////////////////////////////////////////////////////////

void CRecompressingDlg::UpdateInterface()
{
  if(m_bFinished) return;

  CString strCurrentFile;
  int nCurrent;
  int nMax;
  m_csLiveInfo.Lock();
  nCurrent = m_nFile;
  nMax = m_saFiles.GetSize();
  if(nCurrent >= 0 && nCurrent < nMax) {
    strCurrentFile = m_saFiles.GetAt(nCurrent);
  }
  m_csLiveInfo.Unlock();

  CString s;
  s.Format(_T("Recompressing (%d of %d)"), nCurrent + 1, nMax);
  SetWindowText(s);
  SetDlgItemText(IDC_CURRENTFILE, strCurrentFile);
  m_cProgress.SetPos((nCurrent * 10000) / nMax);

  SyncLogBox();

  if(nCurrent >= nMax) {
    m_bFinished = TRUE;
    ReportDone();
    StopThread();
    KillTimer(10);
  }
}

//////////////////////////////////////////////////////////////////////////////

void CRecompressingDlg::OnDestroy() 
{
  KillTimer(10);
  StopThread();
	CDialog::OnDestroy();
}

//////////////////////////////////////////////////////////////////////////////

void CRecompressingDlg::OnTimer(UINT nIDEvent) 
{
	CDialog::OnTimer(nIDEvent);
  if(nIDEvent != 10) return;
  UpdateInterface();
}

//////////////////////////////////////////////////////////////////////////////

BOOL CRecompressingDlg::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_USER) {
    UpdateInterface();
  }
	return CDialog::PreTranslateMessage(pMsg);
}

//////////////////////////////////////////////////////////////////////////////
