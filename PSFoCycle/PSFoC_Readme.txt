-----------------------------------------------------------------------------
PSF-o-Cycle v1.02
Copyright (C) 2003 Neill Corlett
-----------------------------------------------------------------------------

Introduction
------------

What is PSF-o-Cycle?  PSF-o-Cycle is many things.

It's primarily a development system that lets you patch game EXE files into
PSF drivers, using compiled C stub code.  No more futzing around with hand
assembly patches in PSFLab!  The stub code can also include adjustable
parameters, which can then be easily modified using the PSF-o-Cycle GUI.

But PSF-o-Cycle also lets you:

* Mass-optimize.  Queue up a list of PSF files and run them through the
  optimizer - the same basic operation as "Remove Unused Data" from PSFLab.
  No babysitting required!

* A Tag Assistant can automatically set the Length and Fade tags for you,
  using data from the optimizer.

* Recompress.  PSF-o-Cycle includes the new Deflate algorithm used by the
  7-Zip archiver - a 2 to 10% improvement in size, using the same format.

* Upgrade the driver code in a list of PSFs, while leaving the song data
  intact.

Refer to the included SampleDriver.zip for more information on how to build
the stub code.

-----------------------------------------------------------------------------

Requirements
------------

* A PC running Windows 95 or higher.
* Some PSF files.
* If you wish to use the included mini-SDK, you need a R3000 C cross-compiler
  such as the one included in the PSY-Q development kit.

-----------------------------------------------------------------------------

Interface
---------

You have a list of PSF files on the left and a bunch of buttons on the right.
Here's what they do:

* Add - Add new PSF files to the list.
* Remove - Remove selected files from the list.
* Select All - Select all files from the list.

* Parameters - Edit the adjustable parameters in a given PSF file.  The file
  must contain valid driver information, and you can only edit parameters for
  one file at a time.

* Optimize - Run the selected files through the optimizer.  The optimizer
  audits the code in the PSF and removes unused data to reduce the overall
  file size.

* Recompress - Recompress the selected files with one of a variety of
  compression methods.  By default, the files are only rewritten if the
  recompression actually helped.  You can also force the files to be
  rewritten, no matter the size.

* Upgrade Driver - Replace the driver code in the selected files with a new
  driver, keeping the original song data intact.  The replacement driver must
  contain valid driver information, so that PSF-o-Cycle knows which areas are
  song-specific and which belong to the driver code itself.

* Build Driver - Build a new driver from a given stub file and a game EXE.
  The stub is patched onto the game EXE to form the final built driver.

* About - Displays program information.
* Exit - Exits.

-----------------------------------------------------------------------------

Optimizer Options
-----------------

* Run Time: You can instruct the optimizer to end at a fixed time, end when
  no new data has been used for a certain length of time, or both (whichever
  happens sooner).  Times should be given in minutes:seconds.

* Do not optimize song-specific regions: Check this if you do not want the
  optimizer to remove any data considered "song-specific" by the driver
  information block.

* I'm paranoid: Checking this will make the optimizer refuse to throw away
  any data within a certain byte range of used data.

* Emulation settings: Changes settings related to the emulation core.  As a
  rule, you should optimize in "Harsh compatibility mode", which is designed
  to more closely approximate the real hardware.  "Friendly" is optional.

* Tag Assistant: The optimizer can set the Length and Fade tags appropriately
  if they don't already exist.  Optionally, you can also force it to set
  these tags even if they do exist.  The length of a song is considered to be
  the time after which no new data is used.

-----------------------------------------------------------------------------

Compression Methods
-------------------

* zlib compress2 level 0: This actually stores the executable data without
  using any compression.  If you want the data to remain uncompressed for
  whatever reason (i.e. for RAR solid archiving), you can select this method
  and uncheck "only rewrite if the new file is smaller".

* zlib compress2 level 9: This was the old default for most PSF utilities
  prior to PSF-o-Cycle's release.  Fairly fast and provides good
  compression.

* 7-Zip shrink_normal: This is the default compression scheme used in all
  PSF-o-Cycle operations.  It improves the compression ratio significantly
  over zlib, while still taking a reasonable amount of time to compress.
  (1 pass)

* 7-Zip shrink_extra: Takes a long enough time to compress that I decided not
  to use it as the default.  This does provide slightly better compression
  than shrink_normal.  (3 passes)

* 7-Zip shrink_extreme: Takes the longest time of all.  Sometimes improves
  on shrink_extra, sometimes doesn't.  (5 passes)

-----------------------------------------------------------------------------

Known Bugs, Issues, Unhappy Things
----------------------------------

* PSF-o-Cycle uses the same emulation core as Highly Experimental, and
  therefore the same emulation issues apply.

-----------------------------------------------------------------------------

Terms of Use
------------

PSF-o-Cycle is freeware and may be distributed freely as long as it is not
modified, and this documentation is included and not modified either.
"Distributed freely" means no money, goods, or services may be charged or
solicited for PSF-o-Cycle or any media or software package containing it.

Anyone using PSF-o-Cycle does so at their own risk.  No warranty is expressed
or implied.

-----------------------------------------------------------------------------

PSF-o-Cycle includes code from the following projects:

* zlib (C) 1995-2002 Jean-loup Gailly and Mark Adler (LGPL)
  http://www.gzip.org/zlib/

* 7-Zip (C) 1999-2003 Igor Pavlov (LGPL)
  http://www.7-zip.org/

* AdvanceCOMP (C) 2002 Andrea Mazzoleni (LGPL)
  http://advancemame.sourceforge.net/

-----------------------------------------------------------------------------

Thanks to
---------

* All the people who followed my progress on this without getting fed up.
* ProtoCat FOR THE MATHS.

-----------------------------------------------------------------------------

Where to find me
----------------

email: corlett@lfx.org
web:   http://lfx.org/~corlett/

-----------------------------------------------------------------------------
