// PSFoCycle.h : main header file for the PSFOCYCLE application
//

#if !defined(AFX_PSFOCYCLE_H__38FBD024_9206_42AD_B9EE_D5F65B2177AC__INCLUDED_)
#define AFX_PSFOCYCLE_H__38FBD024_9206_42AD_B9EE_D5F65B2177AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CPSFoCycleApp:
// See PSFoCycle.cpp for the implementation of this class
//

extern LPCTSTR g_lpszPSFoCycleBanner;

class CPSFoCycleApp : public CWinApp
{
public:
	CPSFoCycleApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPSFoCycleApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPSFoCycleApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CPSFoCycleApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSFOCYCLE_H__38FBD024_9206_42AD_B9EE_D5F65B2177AC__INCLUDED_)
