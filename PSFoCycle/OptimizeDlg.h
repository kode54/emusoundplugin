#if !defined(AFX_OPTIMIZEDLG_H__E247B09D_3C0D_42B0_A4E0_CED8E560A035__INCLUDED_)
#define AFX_OPTIMIZEDLG_H__E247B09D_3C0D_42B0_A4E0_CED8E560A035__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptimizeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptimizeDlg dialog

class COptimizeDlg : public CDialog
{
// Construction
public:
	COptimizeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COptimizeDlg)
	enum { IDD = IDD_OPTIMIZE };
	BOOL	m_bForceLength;
	BOOL	m_bNoSongSpecific;
	BOOL	m_bParanoid;
	int		m_nParanoidBytes;
	BOOL	m_bRunFixed;
	CString	m_strRunFixedTime;
	BOOL	m_bRunSmart;
	CString	m_strRunSmartTime;
	BOOL	m_bSetFade;
	CString	m_strFadeTime;
	BOOL	m_bSetLength;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptimizeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptimizeDlg)
	afx_msg void OnEmusettings();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIMIZEDLG_H__E247B09D_3C0D_42B0_A4E0_CED8E560A035__INCLUDED_)
