#if !defined(AFX_RECOMPRESSINGDLG_H__528CFE40_5BCE_4518_82A2_83AEB05276F5__INCLUDED_)
#define AFX_RECOMPRESSINGDLG_H__528CFE40_5BCE_4518_82A2_83AEB05276F5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RecompressingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRecompressingDlg dialog

class CRecompressingDlg : public CDialog
{
// Construction
public:
	BOOL m_bOnlyIfSmaller;
	CStringArray m_saFilesChanged;
	CStringArray m_saFiles;
  UINT m_uMethod;
	CRecompressingDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRecompressingDlg)
	enum { IDD = IDD_RECOMPRESSING };
	CProgressCtrl	m_cProgress;
	CEdit	m_cLogBox;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRecompressingDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRecompressingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bSkip;
	void UpdateInterface();
  friend unsigned long __stdcall CRecompressingDlg__Thread(LPVOID lpParam);
	BOOL PerformRecompress(LPCTSTR lpszPathName);
	void WorkLog(LPCTSTR lpszPath, LPCTSTR lpszMessage);
	void SyncLogBox();
	void QueueLogLine(LPCTSTR lpszLine);
	void StopThread();
	void StartThread();
	int m_nSuccessfulFiles;
	int m_nSkippedFiles;
	int m_nErrorFiles;
	INT64 m_n64TotalSavings;
	void ReportDone();

	BOOL m_bFinished;
	CString m_strLogQueue;
	int m_nFile;
	CEvent m_evThreadStarted;
  BOOL m_bThreadKillFlag;
	CCriticalSection m_csThreadRunning;
  CCriticalSection m_csLiveInfo;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RECOMPRESSINGDLG_H__528CFE40_5BCE_4518_82A2_83AEB05276F5__INCLUDED_)
