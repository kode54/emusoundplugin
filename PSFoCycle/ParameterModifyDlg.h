#if !defined(AFX_PARAMETERMODIFYDLG_H__26EEB06A_6604_423D_AED9_1A341DD06CF3__INCLUDED_)
#define AFX_PARAMETERMODIFYDLG_H__26EEB06A_6604_423D_AED9_1A341DD06CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ParameterModifyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CParameterModifyDlg dialog

class CParameterModifyDlg : public CDialog
{
// Construction
public:
	CParameterModifyDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CParameterModifyDlg)
	enum { IDD = IDD_PARAMETER_MODIFY };
	CString	m_strName;
	DWORD	m_dwValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParameterModifyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CParameterModifyDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARAMETERMODIFYDLG_H__26EEB06A_6604_423D_AED9_1A341DD06CF3__INCLUDED_)
