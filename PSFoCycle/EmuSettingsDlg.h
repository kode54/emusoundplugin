#if !defined(AFX_EMUSETTINGSDLG_H__045AFE38_EC74_422B_9D5F_4FFEFF1596C3__INCLUDED_)
#define AFX_EMUSETTINGSDLG_H__045AFE38_EC74_422B_9D5F_4FFEFF1596C3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EmuSettingsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEmuSettingsDlg dialog

class CEmuSettingsDlg : public CDialog
{
// Construction
public:
	CEmuSettingsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEmuSettingsDlg)
	enum { IDD = IDD_EMUSETTINGS };
	int		m_nCompat;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEmuSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEmuSettingsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void UpdateDivision();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EMUSETTINGSDLG_H__045AFE38_EC74_422B_9D5F_4FFEFF1596C3__INCLUDED_)
