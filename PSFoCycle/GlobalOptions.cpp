// GlobalOptions.cpp: implementation of the CGlobalOptions class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PSFoCycle.h"
#include "GlobalOptions.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CGlobalOptions goOptions;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGlobalOptions::CGlobalOptions()
{
  m_bEmuFriendly = FALSE;
  //m_nEmuDivider = TRUE;
}

CGlobalOptions::~CGlobalOptions()
{

}

void CGlobalOptions::Load()
{
  CString s;

  m_bEmuFriendly        = theApp.GetProfileInt   (_T("Options"), _T("emu_friendly"        ), 0);
//m_nEmuDivider         = theApp.GetProfileInt   (_T("Options"), _T("emu_divider"         ), 8);
  m_bAuditParanoid      = theApp.GetProfileInt   (_T("Options"), _T("audit_paranoid"      ), 1);
  m_nAuditParanoidBytes = theApp.GetProfileInt   (_T("Options"), _T("audit_paranoid_bytes"), 1024);

  m_bRunFixed           = theApp.GetProfileInt   (_T("Options"), _T("run_fixed"     ), 0);
  m_bRunSmart           = theApp.GetProfileInt   (_T("Options"), _T("run_smart"     ), 1);

  s = theApp.GetProfileString(_T("Options"), _T("run_fixed_time"), _T("10:00"));
  if(!Util::StringToNum(s, m_dRunFixedSeconds, TRUE)) m_dRunFixedSeconds = 600.0;
  s = theApp.GetProfileString(_T("Options"), _T("run_smart_time"), _T("5:00"));
  if(!Util::StringToNum(s, m_dRunSmartSeconds, TRUE)) m_dRunSmartSeconds = 300.0;

  m_bSetLength          = theApp.GetProfileInt   (_T("Options"), _T("tag_set_length"    ), 1);
  m_bSetFade            = theApp.GetProfileInt   (_T("Options"), _T("tag_set_fade"      ), 1);
  m_bForceLength        = theApp.GetProfileInt   (_T("Options"), _T("tag_force_length"  ), 0);
  s = theApp.GetProfileString(_T("Options"), _T("tag_fade_time"), _T("10"));
  if(!Util::StringToNum(s, m_dFadeSeconds, TRUE)) m_dFadeSeconds = 10.0;

  m_bDoNotOptimizeSongSpecific = theApp.GetProfileInt(_T("Options"), _T("do_not_optimize_song_specific"), 1);

  m_strRecompressMethod = theApp.GetProfileString(_T("Options"), _T("recompress_method"), _T(""));
  m_bRecompressOnlyIfSmaller = theApp.GetProfileInt (_T("Options"), _T("recompress_only_if_smaller"), 1);

}

void CGlobalOptions::Save()
{
  theApp.WriteProfileInt(_T("Options"), _T("emu_friendly"        ), m_bEmuFriendly);
//theApp.WriteProfileInt(_T("Options"), _T("emu_divider"         ), m_nEmuDivider);
  theApp.WriteProfileInt(_T("Options"), _T("audit_paranoid"      ), m_bAuditParanoid);
  theApp.WriteProfileInt(_T("Options"), _T("audit_paranoid_bytes"), m_nAuditParanoidBytes);

  theApp.WriteProfileInt(_T("Options"), _T("run_fixed"), m_bRunFixed);
  theApp.WriteProfileInt(_T("Options"), _T("run_smart"), m_bRunSmart);
  theApp.WriteProfileString(_T("Options"), _T("run_fixed_time"), Util::NumToString(m_dRunFixedSeconds, TRUE));
  theApp.WriteProfileString(_T("Options"), _T("run_smart_time"), Util::NumToString(m_dRunSmartSeconds, TRUE));

  theApp.WriteProfileInt(_T("Options"), _T("tag_set_length"), m_bSetLength);
  theApp.WriteProfileInt(_T("Options"), _T("tag_set_fade"), m_bSetFade);
  theApp.WriteProfileInt(_T("Options"), _T("tag_force_length"), m_bForceLength);
  theApp.WriteProfileString(_T("Options"), _T("tag_fade_time"), Util::NumToString(m_dFadeSeconds, TRUE));

  theApp.WriteProfileInt(_T("Options"), _T("do_not_optimize_song_specific"), m_bDoNotOptimizeSongSpecific);

  theApp.WriteProfileString(_T("Options"), _T("recompress_method"), m_strRecompressMethod);
  theApp.WriteProfileInt(_T("Options"), _T("recompress_only_if_smaller"), m_bRecompressOnlyIfSmaller);
}
