// UpgradingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PSFoCycle.h"
#include "UpgradingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUpgradingDlg dialog


CUpgradingDlg::CUpgradingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUpgradingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUpgradingDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
  m_pDriverEXE = NULL;
  m_pDriverInfo = NULL;
}


void CUpgradingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUpgradingDlg)
	DDX_Control(pDX, IDC_LOGBOX, m_cLogBox);
	DDX_Control(pDX, IDC_PROGRESS, m_cProgress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUpgradingDlg, CDialog)
	//{{AFX_MSG_MAP(CUpgradingDlg)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUpgradingDlg message handlers

//////////////////////////////////////////////////////////////////////////////
//
// This thread does the actual work
//
static unsigned long __stdcall CUpgradingDlg__Thread(LPVOID lpParam) {
  CUpgradingDlg *me = (CUpgradingDlg*)lpParam;
  ASSERT(me);
  /*
  ** Signal thread start
  */
  me->m_csThreadRunning.Lock();
  me->m_evThreadStarted.SetEvent();
  //
  // Loop
  //
  for(;;) {
    me->m_csLiveInfo.Lock();
    BOOL b;
    b = me->m_bThreadKillFlag;
    me->m_csLiveInfo.Unlock();
    if(b) break;
    //
    // Thread state machine
    //
    CString strPath;
    me->m_csLiveInfo.Lock();
    int n = me->m_nFile;
    if(n < me->m_saFiles.GetSize()) {
      strPath = me->m_saFiles.GetAt(n);
    } else {
      b = TRUE;
    }
    me->m_csLiveInfo.Unlock();
    //
    // If we're out of files, the thread should probably quit
    //
    if(b) break;
    //
    // Perform the upgrade on this file
    //
    b = me->PerformUpgrade(strPath);
    //
    // Update numbers accordingly
    // If the upgrade was a success, add it to the list of files changed
    //
    if(b) {
      me->m_csLiveInfo.Lock();
      me->m_nSuccessfulFiles++;
      me->m_saFilesChanged.Add(strPath);
      me->m_csLiveInfo.Unlock();
    } else {
      me->m_csLiveInfo.Lock();
      me->m_nErrorFiles++;
      me->m_csLiveInfo.Unlock();
    }
    //
    // Increment file number
    //
    me->m_csLiveInfo.Lock();
    if(me->m_nFile < me->m_saFiles.GetSize()) {
      me->m_nFile++;
    }
    me->m_csLiveInfo.Unlock();
    //
    // Signal UI update
    //
    ::PostMessage(me->m_hWnd, WM_USER, 0, 0);
  }
  //
  // Signal thread end
  //
  me->m_csThreadRunning.Unlock();
  return 0;
}

void CUpgradingDlg::StartThread()
{
  StopThread();
  m_evThreadStarted.ResetEvent();
  m_bThreadKillFlag = FALSE;
  DWORD tid;
  ::CreateThread(NULL, 0, CUpgradingDlg__Thread, (LPVOID)this, 0, &tid);
  m_evThreadStarted.Lock();
  m_evThreadStarted.ResetEvent();
}

void CUpgradingDlg::StopThread()
{
  m_csLiveInfo.Lock();
  m_bThreadKillFlag = TRUE;
  m_csLiveInfo.Unlock();
  m_csThreadRunning.Lock();
  m_csThreadRunning.Unlock();
  m_bThreadKillFlag = FALSE;
}

//////////////////////////////////////////////////////////////////////////////
//
// Called from work thread
//
// Returns FALSE on error (this is how they're counted)
//
BOOL CUpgradingDlg::PerformUpgrade(LPCTSTR lpszPathName)
{
  /*
  ** Create a new EXE and copy the driver one into it
  */
  CPSXEXE exeNew;
  exeNew.CopyFrom(m_pDriverEXE);

  BOOL b;

  /*
  ** Read the song EXE
  */
  CPSF psfSong;
  b = psfSong.ReadFromFile(lpszPathName,
    CPSF::PSF_FILE_VERSION |
    CPSF::PSF_FILE_ZEXE,
    TRUE
  );
  // TODO: better error handling
  if(!b) {
    WorkLog(lpszPathName, (LPCTSTR)psfSong.GetLastError());
    return FALSE;
  }

  CPSXEXE exeSong;
  b = exeSong.ReadFromPSF(psfSong, TRUE);
  // TODO: better error handling
  if(!b) {
    WorkLog(lpszPathName, (LPCTSTR)exeSong.GetLastError());
    return FALSE;
  }

  /*
  ** Take all the song-specific areas and copy them over to the new EXE
  */
  for(int i = 0; i < m_pDriverInfo->m_dwaSongSpecificStarts.GetSize(); i++) {
    DWORD dwStart  = m_pDriverInfo->m_dwaSongSpecificStarts.GetAt(i);
    DWORD dwLength = m_pDriverInfo->m_dwaSongSpecificLengths.GetAt(i);
    if(dwLength > 0x200000) { dwLength = 0x200000; }
    LPBYTE lpTemp = (LPBYTE)malloc(dwLength);
    exeSong.ReadAddressRange(lpTemp, dwStart, dwLength);
    exeNew.WriteAddressRange(dwStart, lpTemp, dwLength);
    free(lpTemp);
  }

  /*
  ** Write the new EXE to the existing PSF
  */
  b = exeNew.WriteToPSF(psfSong, CPSFCompressor::GetDefaultMethod());
  // TODO: better error handling
  if(!b) {
    WorkLog(lpszPathName, (LPCTSTR)exeNew.GetLastError());
    return FALSE;
  }

  b = psfSong.WriteToFile(lpszPathName, CPSF::PSF_FILE_ZEXE, TRUE);
  // TODO: better error handling
  if(!b) {
    WorkLog(lpszPathName, (LPCTSTR)psfSong.GetLastError());
    return FALSE;
  }

  //
  // Success (calling routine will add it to the changed list)
  //
  WorkLog(lpszPathName, _T("OK"));
  return TRUE;
}

BOOL CUpgradingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
  ASSERT(m_pDriverEXE);
  ASSERT(m_pDriverInfo);

  m_cProgress.SetRange(0, 10000);

  //
  // Initialize file counts
  //
  m_nFile = 0;
  m_nSuccessfulFiles = 0;
  m_nErrorFiles = 0;
  m_nSkippedFiles = 0;

  m_bFinished = FALSE;

  StartThread();

  UpdateInterface();
  SetTimer(10, 500, NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUpgradingDlg::OnDestroy() 
{
  KillTimer(10);
  StopThread();
	CDialog::OnDestroy();
}

void CUpgradingDlg::OnTimer(UINT nIDEvent) 
{
	CDialog::OnTimer(nIDEvent);
  if(nIDEvent != 10) return;
  UpdateInterface();
}

void CUpgradingDlg::UpdateInterface()
{
  if(m_bFinished) return;

  CString strCurrentFile;
  int nCurrent;
  int nMax;
  m_csLiveInfo.Lock();
  nCurrent = m_nFile;
  nMax = m_saFiles.GetSize();
  if(nCurrent >= 0 && nCurrent < nMax) {
    strCurrentFile = m_saFiles.GetAt(nCurrent);
  }
  m_csLiveInfo.Unlock();

  CString s;
  s.Format(_T("Upgrading (%d of %d)"), nCurrent + 1, nMax);
  SetWindowText(s);
  SetDlgItemText(IDC_CURRENTFILE, strCurrentFile);
  m_cProgress.SetPos((nCurrent * 10000) / nMax);

  SyncLogBox();

  if(nCurrent >= nMax) {
    m_bFinished = TRUE;
    ReportDone();
    StopThread();
    KillTimer(10);
  }
}

//////////////////////////////////////////////////////////////////////////////
//
// To be called from work thread only
//
void CUpgradingDlg::QueueLogLine(LPCTSTR lpszLine)
{
  CString s;
  CTime t = CTime::GetCurrentTime();
  s += t.Format((LPCSTR)"[%I:%M:%S %p] ");
  s += lpszLine;
  s += _T("\r\n");
  m_csLiveInfo.Lock();
  m_strLogQueue += s;
  m_csLiveInfo.Unlock();
}

//////////////////////////////////////////////////////////////////////////////
//
// To be called from UI thread only
//
void CUpgradingDlg::SyncLogBox()
{
  CString q;
  m_csLiveInfo.Lock();
  q = m_strLogQueue;
  m_strLogQueue.Empty();
  m_csLiveInfo.Unlock();
  if(q.IsEmpty()) return;

  CString s;
  m_cLogBox.GetWindowText(s);
  s += q;
  m_cLogBox.SetWindowText(s);
  m_cLogBox.LineScroll(m_cLogBox.GetLineCount());
}

//////////////////////////////////////////////////////////////////////////////
//
// Update the UI to show that the operation is done
// Should only be called from the UI thread
//
void CUpgradingDlg::ReportDone()
{
  SetWindowText(_T("Upgrading (done)"));
  SetDlgItemText(IDC_CURRENTFILE, _T("Done"));
  m_cProgress.SetPos(10000);
  QueueLogLine(_T("Done"));

  CString s;
  if(m_nSuccessfulFiles) {
    s.Format(_T("- %d file%s upgraded"), m_nSuccessfulFiles, m_nSuccessfulFiles==1?_T(""):_T("s"));
    QueueLogLine(s);
  }
  if(m_nErrorFiles) {
    s.Format(_T("- %d error%s"), m_nErrorFiles, m_nErrorFiles==1?_T(""):_T("s"));
    QueueLogLine(s);
  } else {
    QueueLogLine(_T("- No errors"));
  }
  if(m_nSkippedFiles) {
    s.Format(_T("- %d file%s skipped"), m_nSkippedFiles, m_nSkippedFiles==1?_T(""):_T("s"));
    QueueLogLine(s);
  }

  // Do nothing more fancy than make the "Cancel" button say "OK"
  SetDlgItemText(IDCANCEL, _T("OK"));

  SyncLogBox();
}

//////////////////////////////////////////////////////////////////////////////
//
// Front end for QueueLogLine
// Should only be called from the work thread
//
void CUpgradingDlg::WorkLog(LPCTSTR lpszPath, LPCTSTR lpszMessage)
{
  CString s;
  s.Format(_T("%s: %s"), Util::SeekToFileName(lpszPath), lpszMessage);
  QueueLogLine(s);
}

//////////////////////////////////////////////////////////////////////////////

BOOL CUpgradingDlg::PreTranslateMessage(MSG* pMsg) 
{
  if(pMsg->message == WM_USER) {
    UpdateInterface();
  }
	return CDialog::PreTranslateMessage(pMsg);
}

//////////////////////////////////////////////////////////////////////////////
