The EmuSoundPlugin Kingdom

7z - 7zip code from AdvanceComp (basically stock)

EmuSoundPlugin - ESP framework code
  All the fancy info boxes and plugin stuff is handled in here. Compatibility
  with different music players (independent of HE/HQ/HT) is handled here.
  Anything Winamp2- or Winamp3-specific is handled here.

HE - Highly Experimental
HQ - Highly Quixotic
HT - Highly Theoretical
  Build points for the 3 plugins.  Mostly this is just glue code since the heavy
  lifting is done by EmuSoundPlugin and the PSX/QSound/Sega core libraries.

MFCUtil - various MFC utility classes

PSF - utility code which is specific to the PSF format in general and not any
  one particular core.

PSFLab2 - PSFLab v2 code. currently "under construction" and broken.

PSFLab-old - PSFLab v1 code. May compile but is deprecated.

PSFoCycle - PSFoCycle code. Unsure whether this still compiles.

PSFPoint - PSFPoint. The contents of this directory are public domain.

PSX - PSX emulation (R3000/IOP/SPU1/SPU2). *OLD*
QSound - QSound emulation (Z80/QSound chip). *OLD*
Sega - Saturn/Dreamcast emulation (68K/ARM/SCSP/AICA). *OLD*

Note by kode54: OLD libraries archived from original source that was slightly
  modified, please use my individual xSF core libraries instead.

zlib - zlib code (stock)
