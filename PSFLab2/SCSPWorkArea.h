// SCSPWorkArea.h: interface for the CSCSPWorkArea class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCSPWORKAREA_H__65506B1B_CB85_4513_A439_AAF2E7DCD6D1__INCLUDED_)
#define AFX_SCSPWORKAREA_H__65506B1B_CB85_4513_A439_AAF2E7DCD6D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WorkArea.h"

class CSCSPWorkArea : public CWorkArea  
{
public:
	CSCSPWorkArea();
	virtual ~CSCSPWorkArea();

};

#endif // !defined(AFX_SCSPWORKAREA_H__65506B1B_CB85_4513_A439_AAF2E7DCD6D1__INCLUDED_)
