// PS1WorkArea.h: interface for the CPS1WorkArea class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PS1WORKAREA_H__47A0691D_FA4C_4826_BB45_A9A15954319B__INCLUDED_)
#define AFX_PS1WORKAREA_H__47A0691D_FA4C_4826_BB45_A9A15954319B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WorkArea.h"

class CPS1WorkArea : public CWorkArea  
{
public:
	CPS1WorkArea();
	virtual ~CPS1WorkArea();

};

#endif // !defined(AFX_PS1WORKAREA_H__47A0691D_FA4C_4826_BB45_A9A15954319B__INCLUDED_)
