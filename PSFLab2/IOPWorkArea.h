// IOPWorkArea.h: interface for the CIOPWorkArea class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IOPWORKAREA_H__13264169_5E87_4C9B_9DBA_3658D7A93792__INCLUDED_)
#define AFX_IOPWORKAREA_H__13264169_5E87_4C9B_9DBA_3658D7A93792__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WorkArea.h"

class CIOPWorkArea : public CWorkArea  
{
public:
	CIOPWorkArea();
	virtual ~CIOPWorkArea();

};

#endif // !defined(AFX_IOPWORKAREA_H__13264169_5E87_4C9B_9DBA_3658D7A93792__INCLUDED_)
