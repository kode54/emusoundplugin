// PSFLab2.h : main header file for the PSFLAB2 application
//

#if !defined(AFX_PSFLAB2_H__41307AA3_1A0C_4D23_B6C3_0B28B99337EC__INCLUDED_)
#define AFX_PSFLAB2_H__41307AA3_1A0C_4D23_B6C3_0B28B99337EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Options.h"	// Added by ClassView

/////////////////////////////////////////////////////////////////////////////
// CPSFLab2App:
// See PSFLab2.cpp for the implementation of this class
//

class CPSFLab2App : public CWinApp
{
public:
	CPSFLab2App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPSFLab2App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	//{{AFX_MSG(CPSFLab2App)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	COptions m_options;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSFLAB2_H__41307AA3_1A0C_4D23_B6C3_0B28B99337EC__INCLUDED_)
