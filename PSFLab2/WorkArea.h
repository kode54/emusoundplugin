// WorkArea.h: interface for the CWorkArea class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WORKAREA_H__127AD9B3_3260_4F12_B531_6394BBFA7E1E__INCLUDED_)
#define AFX_WORKAREA_H__127AD9B3_3260_4F12_B531_6394BBFA7E1E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CWorkArea  
{
public:
	CWorkArea();
	virtual ~CWorkArea();

};

#endif // !defined(AFX_WORKAREA_H__127AD9B3_3260_4F12_B531_6394BBFA7E1E__INCLUDED_)
