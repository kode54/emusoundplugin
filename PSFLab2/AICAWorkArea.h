// AICAWorkArea.h: interface for the CAICAWorkArea class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AICAWORKAREA_H__423CFE7A_0039_40F1_9C61_2551A65AFD7A__INCLUDED_)
#define AFX_AICAWORKAREA_H__423CFE7A_0039_40F1_9C61_2551A65AFD7A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WorkArea.h"

class CAICAWorkArea : public CWorkArea  
{
public:
	CAICAWorkArea();
	virtual ~CAICAWorkArea();

};

#endif // !defined(AFX_AICAWORKAREA_H__423CFE7A_0039_40F1_9C61_2551A65AFD7A__INCLUDED_)
