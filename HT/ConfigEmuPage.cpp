// ConfigEmuPage.cpp : implementation file
//

#include "stdafx.h"
#include "in_ssf.h"
#include "ConfigEmuPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfigEmuPage property page

IMPLEMENT_DYNCREATE(CConfigEmuPage, CPropertyPage)

CConfigEmuPage::CConfigEmuPage() : CPropertyPage(CConfigEmuPage::IDD)
{
	//{{AFX_DATA_INIT(CConfigEmuPage)
	//}}AFX_DATA_INIT
}

CConfigEmuPage::~CConfigEmuPage()
{
}

void CConfigEmuPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigEmuPage)
	DDX_Control(pDX, IDC_ENABLEDSP, m_cEnableDSP);
	DDX_Control(pDX, IDC_ENABLEDRY, m_cEnableDry);
	DDX_Control(pDX, IDC_DSPDYNAREC, m_cDSPDynaRec);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfigEmuPage, CPropertyPage)
	//{{AFX_MSG_MAP(CConfigEmuPage)
	ON_BN_CLICKED(IDC_ENABLEDRY, OnEnabledry)
	ON_BN_CLICKED(IDC_ENABLEDSP, OnEnabledsp)
	ON_BN_CLICKED(IDC_DSPDYNAREC, OnDspdynarec)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigEmuPage message handlers

void CConfigEmuPage::BindToPlugin(CHTPlugin *pPlugin)
{
  m_pPlugin = pPlugin;
}


void CConfigEmuPage::OnEnabledry() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();
    m_pPlugin->opt_bEnableDry = (m_cEnableDry.GetCheck() == BST_CHECKED);
    m_pPlugin->UnlockOptions();
  }
}

void CConfigEmuPage::OnEnabledsp() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();
    m_pPlugin->opt_bEnableDSP = (m_cEnableDSP.GetCheck() == BST_CHECKED);
    m_pPlugin->UnlockOptions();
  }
  UpdateInterface();
}

void CConfigEmuPage::OnDspdynarec() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();
    m_pPlugin->opt_bDSPDynaRec = (m_cDSPDynaRec.GetCheck() == BST_CHECKED);
    m_pPlugin->UnlockOptions();
  }
}

void CConfigEmuPage::UpdateInterface()
{
  m_cDSPDynaRec.EnableWindow(m_cEnableDSP.GetCheck() == BST_CHECKED);
}

BOOL CConfigEmuPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
  if(m_pPlugin) {
    m_pPlugin->LockOptions();

    m_bOriginalEnableDry  = m_pPlugin->opt_bEnableDry;
    m_bOriginalEnableDSP  = m_pPlugin->opt_bEnableDSP;
    m_bOriginalDSPDynaRec = m_pPlugin->opt_bDSPDynaRec;

    m_cEnableDry.SetCheck(m_pPlugin->opt_bEnableDry ? BST_CHECKED : BST_UNCHECKED);
    m_cEnableDSP.SetCheck(m_pPlugin->opt_bEnableDSP ? BST_CHECKED : BST_UNCHECKED);
    m_cDSPDynaRec.SetCheck(m_pPlugin->opt_bDSPDynaRec ? BST_CHECKED : BST_UNCHECKED);

    m_pPlugin->UnlockOptions();
  }

  UpdateInterface();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CConfigEmuPage::OnOK() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();

    m_pPlugin->opt_bEnableDry  = (m_cEnableDry.GetCheck() == BST_CHECKED);
    m_pPlugin->opt_bEnableDSP  = (m_cEnableDSP.GetCheck() == BST_CHECKED);
    m_pPlugin->opt_bDSPDynaRec = (m_cDSPDynaRec.GetCheck() == BST_CHECKED);

    m_pPlugin->UnlockOptions();
  }

	CPropertyPage::OnOK();
}

void CConfigEmuPage::OnCancel() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();

    m_pPlugin->opt_bEnableDry  = m_bOriginalEnableDry;
    m_pPlugin->opt_bEnableDSP  = m_bOriginalEnableDSP;
    m_pPlugin->opt_bDSPDynaRec = m_bOriginalDSPDynaRec;

    m_pPlugin->UnlockOptions();
  }

	CPropertyPage::OnCancel();
}
