#pragma once

#include "../PSF/Util/PSFPlayer.h"

class CSegaPlayer : public CPSFPlayer  
{
public:
	CSegaPlayer(File f, int nVersion);
	virtual ~CSegaPlayer();

    virtual CStringW GetFormat();

protected:
	virtual BOOL RawOpen();
	virtual void RawClose();
	virtual int RawRead(short *buffer, int nSamples);

private:
    BOOL BuildExecutable(File f, CByteArray &array, int nLevel);

    int m_nVersion;
    CByteArray m_aHW;

};
