// HTPlugin.h: interface for the CHTPlugin class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HTPLUGIN_H__C8E6F07A_9C52_48EE_8E30_2ED99CB10E97__INCLUDED_)
#define AFX_HTPLUGIN_H__C8E6F07A_9C52_48EE_8E30_2ED99CB10E97__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../PSF/Util/PSFPlugin.h"

class CHTPlugin : public CPSFPlugin  
{
public:
	virtual void About(HWND hWndParent);
	virtual LPCWSTR* GetSupportedExtensions();
	virtual LPCWSTR  GetOptionSection();
	virtual LPCWSTR  GetBanner();
	virtual LPCWSTR  GetShortBanner();
	virtual LPCWSTR* GetAllPSFTypes();
	virtual LPCWSTR GetPSFType(int nPSFVersion);
	virtual int GetPSFLogoBitmap(File f, CPSFTag *pPSFTag, int nVersion);
    virtual bool CheckMagic(BYTE *buf, DWORD length);

    BOOL opt_bEnableDry;
    BOOL opt_bEnableDSP;
    BOOL opt_bDSPDynaRec;

	CHTPlugin();
	virtual ~CHTPlugin();

protected:
    ESP_OPTION_DECLARE

    virtual void BuildConfigPropertySheet(CPropertySheet *pSheet);
    virtual CPlayer* CreatePlayer(File f, CStringW &strError);

};

extern CHTPlugin *g_pHTPlugin;

#endif // !defined(AFX_HTPLUGIN_H__C8E6F07A_9C52_48EE_8E30_2ED99CB10E97__INCLUDED_)
