// HTPlugin.cpp: implementation of the CHTPlugin class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "in_ssf.h"

#include "HTPlugin.h"
#include "SegaPlayer.h"

#include "AboutDlg.h"

#include "ConfigEmuPage.h"

#include "../Sega/Core/sega.h"

#include "../PSF/Util/PSFDecompressor.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////

LPCWSTR CHTPlugin::GetBanner() {
  return L"Highly Theoretical SSF/DSF Player v0.13dev";
}

LPCWSTR CHTPlugin::GetShortBanner() {
  return L"Highly Theoretical";
}

LPCWSTR* CHTPlugin::GetSupportedExtensions() {
    static LPCWSTR list[] = {
        L"ssf"     , L"Saturn Sound Format",
        L"minissf" , L"Mini-SSF",
        L"dsf"     , L"Dreamcast Sound Format",
        L"minidsf" , L"Mini-DSF",
        NULL
    };
    return list;
}

LPCWSTR CHTPlugin::GetOptionSection() {
    return L"HighlyTheoretical";
}

LPCWSTR* CHTPlugin::GetAllPSFTypes() {
    static LPCWSTR list[] = {
        L"SSF",
        L"DSF",
        NULL
    };
    return list;
}

LPCWSTR CHTPlugin::GetPSFType(int nPSFVersion) {
    switch(nPSFVersion) {
    case 0x11: return L"SSF";
    case 0x12: return L"DSF";
    }
    return L"";
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHTPlugin::CHTPlugin()
{
  srand(time(NULL));
  ::sega_init();

  opt_bEnableDry  = TRUE;
  opt_bEnableDSP  = TRUE;
  opt_bDSPDynaRec = TRUE;
}

CHTPlugin::~CHTPlugin() { }

/////////////////////////////////////////////////////////////////////////////

ESP_OPTION_START(CHTPlugin, CPSFPlugin)

ESP_OPTION(L"EnableDry"   , OPT_BOOL, opt_bEnableDry                , ESP_ALL_PLAYERS)
ESP_OPTION(L"EnableDSP"   , OPT_BOOL, opt_bEnableDSP                , ESP_ALL_PLAYERS)
ESP_OPTION(L"DSPDynaRec"  , OPT_BOOL, opt_bDSPDynaRec               , ESP_ALL_PLAYERS)

ESP_OPTION_END(CHTPlugin, CPSFPlugin)

/////////////////////////////////////////////////////////////////////////////

bool CHTPlugin::CheckMagic(BYTE *buf, DWORD length) {
    if(length < 4) return false;
    if(memcmp(buf, "PSF", 3)) return false;
    switch(buf[3]) {
    case 0x11:
    case 0x12:
        return true;
    }
    return false;
}

/////////////////////////////////////////////////////////////////////////////

CPlayer* CHTPlugin::CreatePlayer(File f, CStringW &strError) {

    int ver = CPSFPlugin::GrabPSFVersion(f, strError);
    if(ver < 0) return NULL;

    strError = L"Out of memory";
    switch(ver) {
    case 0x11: return new CSegaPlayer(f, 1);
    case 0x12: return new CSegaPlayer(f, 2);
    }
    strError = L"Incorrect version; not an SSF or DSF file";
    return NULL;
}

/////////////////////////////////////////////////////////////////////////////

int CHTPlugin::GetPSFLogoBitmap(File f, CPSFTag *pPSFTag, int nVersion)
{
    int r = rand();

    if((r & 127) == 20) return IDB_SPOCKSALSA;

    if(nVersion == 0x11) return IDB_HTSATLOGO;
    if(nVersion == 0x12) return IDB_HTDCLOGO;

    return IDB_HTDCLOGO;
}

/////////////////////////////////////////////////////////////////////////////

void CHTPlugin::About(HWND hWndParent) {
    CAboutDlg dlg(CWnd::FromHandle(hWndParent));

    // TODO: version details again

    dlg.m_strBanner = GetBanner();

    CStringW s;

    s += GetBanner();

    s += L"\n";

    s += L"Built ";
    s += __DATE__;

    s += L"\n\n";

    s += GetFrameworkVersion();
    s += L"\n";
    s += ::sega_getversion();
    s += L"\n";
    s += CPSFDecompressor::GetVersion();

    dlg.m_strVersionDetails = s;

    dlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////

void CHTPlugin::BuildConfigPropertySheet(CPropertySheet *pSheet) {

    CPSFPlugin::BuildConfigPropertySheet(pSheet);

    { CConfigEmuPage *p = new CConfigEmuPage;
        p->BindToPlugin(this);
        pSheet->AddPage(p);
    }
}

/////////////////////////////////////////////////////////////////////////////
