// in_ssf.h : main header file for the IN_SSF DLL
//

#if !defined(AFX_IN_SSF_H__2B2CE1EC_D336_4C2C_9FA3_1082425D11C9__INCLUDED_)
#define AFX_IN_SSF_H__2B2CE1EC_D336_4C2C_9FA3_1082425D11C9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CIn_ssfApp
// See in_ssf.cpp for the implementation of this class
//

class CIn_ssfApp : public CWinApp
{
public:
	CIn_ssfApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIn_ssfApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CIn_ssfApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IN_SSF_H__2B2CE1EC_D336_4C2C_9FA3_1082425D11C9__INCLUDED_)
