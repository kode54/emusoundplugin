#if !defined(AFX_CONFIGEMUPAGE_H__9A692AB6_6E51_42BA_BB98_FE72CD13DA33__INCLUDED_)
#define AFX_CONFIGEMUPAGE_H__9A692AB6_6E51_42BA_BB98_FE72CD13DA33__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConfigEmuPage.h : header file
//

#include "HTPlugin.h"

/////////////////////////////////////////////////////////////////////////////
// CConfigEmuPage dialog

class CConfigEmuPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CConfigEmuPage)

// Construction
public:
	void BindToPlugin(CHTPlugin *pPlugin);
	CConfigEmuPage();
	~CConfigEmuPage();

// Dialog Data
	//{{AFX_DATA(CConfigEmuPage)
	enum { IDD = IDD_CONFIG_EMU };
	CButton	m_cEnableDSP;
	CButton	m_cEnableDry;
	CButton	m_cDSPDynaRec;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CConfigEmuPage)
	public:
	virtual void OnOK();
	virtual void OnCancel();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CConfigEmuPage)
	afx_msg void OnEnabledry();
	afx_msg void OnEnabledsp();
	afx_msg void OnDspdynarec();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void UpdateInterface();
  CHTPlugin *m_pPlugin;

  BOOL m_bOriginalEnableDry;
  BOOL m_bOriginalEnableDSP;
  BOOL m_bOriginalDSPDynaRec;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIGEMUPAGE_H__9A692AB6_6E51_42BA_BB98_FE72CD13DA33__INCLUDED_)
