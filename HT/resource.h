//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by in_ssf.rc
//
#define IDC_ENABLEDSP                   1027
#define IDC_DSPDYNAREC                  1028
#define IDC_ENABLEDRY                   1031
#define IDC_URL                         1035
#define IDC_VERSION                     1039
#define IDC_VERSIONDETAILS              1047
#define IDD_ABOUT                       3000
#define IDB_HTDCLOGO                    3001
#define IDB_SPOCKSALSA                  3002
#define IDD_CONFIG_EMU                  3003
#define IDB_HTSATLOGO                   3004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        3005
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         3000
#define _APS_NEXT_SYMED_VALUE           3000
#endif
#endif
