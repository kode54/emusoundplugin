// SegaPlayer.cpp: implementation of the CSegaPlayer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "in_ssf.h"
#include "SegaPlayer.h"

#include "HTPlugin.h"

#include "../Sega/Core/sega.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSegaPlayer::CSegaPlayer(File f, int nVersion) : CPSFPlayer(f)
{
  if(nVersion != 2) nVersion = 1;
  m_nVersion = nVersion;
  m_aHW.SetSize(0);
}

CSegaPlayer::~CSegaPlayer()
{

}

/////////////////////////////////////////////////////////////////////////////

CStringW CSegaPlayer::GetFormat() {
    switch(m_nVersion) {
    case 1: return L"SSF";
    case 2: return L"DSF";
    }
    return L"";
}

/////////////////////////////////////////////////////////////////////////////

static void superimpose_sega_image(CByteArray &dst, CByteArray &src) {
  if(dst.GetSize() < 4) return;
  if(src.GetSize() < 4) return;

  DWORD dst_start = *(DWORD*)(dst.GetData());
  DWORD src_start = *(DWORD*)(src.GetData());
  dst_start &= 0x1FFFFF;
  src_start &= 0x1FFFFF;
  DWORD dst_len = dst.GetSize() - 4;
  DWORD src_len = src.GetSize() - 4;
  if(dst_len > 0x200000) dst_len = 0x200000;
  if(src_len > 0x200000) src_len = 0x200000;

  if(src_start < dst_start) {
    DWORD diff = dst_start - src_start;
    dst.SetSize(dst_len + 4 + diff);
    memmove(dst.GetData() + 4 + diff, dst.GetData() + 4, dst_len);
    memset(dst.GetData() + 4, 0, diff);
    dst_len += diff;
    dst_start = src_start;
  }
  if((src_start+src_len) > (dst_start+dst_len)) {
    DWORD diff = (dst_start+dst_len) - (src_start+src_len);
    dst.SetSize(dst_len + 4 + diff);
    memset(dst.GetData() + 4 + dst_len, 0, diff);
    dst_len += diff;
  }

  memcpy(dst.GetData() + 4 + (src_start - dst_start), src.GetData() + 4, src_len);

}

/////////////////////////////////////////////////////////////////////////////

BOOL CSegaPlayer::BuildExecutable(File f, CByteArray &array, int nLevel)
{
    array.SetSize(0);

    g_pHTPlugin->LockOptions();
    BOOL bStrict = g_pHTPlugin->opt_bStrictFormatChecking;
    g_pHTPlugin->UnlockOptions();

    if(nLevel > 10) {
        BuildExecutableError(f, L"Recursion limit exceeded");
        return FALSE;
    }

    CPSF psf;

    //
    // Attempt to read the file - version, exe, and tag
    //
    if(!psf.ReadFromFile(f,
        CPSF::PSF_FILE_VERSION |
        CPSF::PSF_FILE_ZEXE |
        CPSF::PSF_FILE_TAG,
        bStrict
    )) {
        BuildExecutableError(f, psf.GetLastError());
        return FALSE;
    }

    //
    // Verify version
    //
    int v = psf.GetVersion();
    if(v != 0x11 && v != 0x12) {
        BuildExecutableError(f, L"Invalid version: not a SSF or DSF file");
        return FALSE;
    }
    if(m_nVersion == 1 && v != 0x11) {
        BuildExecutableError(f, L"Invalid version: not a SSF file");
        return FALSE;
    }
    if(m_nVersion == 2 && v != 0x12) {
        BuildExecutableError(f, L"Invalid version: not a DSF file");
        return FALSE;
    }

    //
    // Attempt to decompress the exe from the psf
    //
    DWORD dwSize = 0x200004;
    array.SetSize(dwSize);
    if(!psf.GetProgramData(array.GetData(), &dwSize)) {
        BuildExecutableError(f, psf.GetLastError());
        array.SetSize(0);
        return FALSE;
    }
    array.SetSize(dwSize);

//{char s[1000];sprintf(s,"dwSize=%u",dwSize);::MessageBox(NULL,s,NULL,MB_OK);}

    //
    // Library tag stuff
    //
    CPSFTag tag;
    if(!tag.ReadFromPSF(psf, bStrict)) {
        BuildExecutableError(f, tag.GetLastError());
        array.SetSize(0);
        return FALSE;
    }

    CStringW strVar;
    CStringW strLib;
    CByteArray libarray;

    //
    // Handle _lib
    //
    strLib = tag.GetVarW(L"_lib");
    if(!(strLib.IsEmpty())) {

        libarray.SetSize(array.GetSize());
        memcpy(libarray.GetData(), array.GetData(), libarray.GetSize());

        if(!BuildExecutable(File(f, CPSF::MakeLibPath(f.GetPath(), strLib)), array, nLevel + 1)) {
            array.SetSize(0);
            return FALSE;
        }

        // superimpose libarray onto array
        superimpose_sega_image(array, libarray);
    }
    //
    // Handle _lib2, etc.
    //
    for(int i = 2;; i++) {
        strVar.Format(L"_lib%d", i);
        strLib = tag.GetVarW(strVar);
        if(strLib.IsEmpty()) break;
        if(!BuildExecutable(File(f, CPSF::MakeLibPath(f.GetPath(), strLib)), libarray, nLevel + 1)) {
            array.SetSize(0);
            return FALSE;
        }

        // superimpose libarray onto array
        superimpose_sega_image(array, libarray);
    }

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CSegaPlayer::RawOpen()
{
  CByteArray segaexe;
  if(!BuildExecutable(GetFile(), segaexe, 0)) return FALSE;

  // Create hardware state
  m_aHW.SetSize(0);
  m_aHW.SetSize(::sega_get_state_size(m_nVersion));
  ::sega_clear_state(m_aHW.GetData(), m_nVersion);

  // Upload program
  int r = ::sega_upload_program(m_aHW.GetData(), segaexe.GetData(), segaexe.GetSize());
  if(r) {
    m_strLastError = L"Invalid program; unable to start the emulator";
    return FALSE;
  }

  m_dwInternalSampleRate = 44100;
  m_dwChannels = 2;

  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void CSegaPlayer::RawClose()
{
  m_aHW.SetSize(0);
}

/////////////////////////////////////////////////////////////////////////////

int CSegaPlayer::RawRead(short *buffer, int nSamples)
{
  if(!m_aHW.GetSize()) {
    m_strLastError = L"Emulation state not created (program error)";
    return -2;
  }

  g_pHTPlugin->LockOptions();
  BOOL bEnableDry  = g_pHTPlugin->opt_bEnableDry;
  BOOL bEnableDSP  = g_pHTPlugin->opt_bEnableDSP;
  BOOL bDSPDynaRec = g_pHTPlugin->opt_bDSPDynaRec;
  g_pHTPlugin->UnlockOptions();

  ::sega_enable_dry        (m_aHW.GetData(), bEnableDry);
  ::sega_enable_dsp        (m_aHW.GetData(), bEnableDSP);
  ::sega_enable_dsp_dynarec(m_aHW.GetData(), bDSPDynaRec);

  BOOL bEmulationError = FALSE;
  int samples_left = nSamples;
  short *b = buffer;
  while(samples_left > 0) {
    int n = samples_left;
    int r = ::sega_execute(m_aHW.GetData(), 0x7FFFFFFF, b, (unsigned*)(&n));
    if(r < 0) {
      bEmulationError = TRUE;
      break;
    }
    if(b) { b += 2 * n; }
    samples_left -= n;
  }
  int r = nSamples - samples_left;

    if(!r) {
        if(bEmulationError) {
            DWORD dwPC = ::sega_get_pc(m_aHW.GetData());
            m_strLastError.Format(L"Unrecoverable emulation error, PC=0x%08X", dwPC);
            r = -1;
        }
    }

    return r;
}

/////////////////////////////////////////////////////////////////////////////
