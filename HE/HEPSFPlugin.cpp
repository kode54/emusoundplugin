/////////////////////////////////////////////////////////////////////////////
//
// lalala
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "Resource.h"

#include "HEPSFPlugin.h"

#include "HEPSF1Player.h"
#include "HEPSF2Player.h"

#include "AboutDlg.h"

#include "ConfigEmuPage.h"

#include "../PSX/Core/psx.h"

#include "../PSF/Util/PSFDecompressor.h"

/////////////////////////////////////////////////////////////////////////////

LPCWSTR CHEPSFPlugin::GetBanner() {
    return L"Highly Experimental PSF1/PSF2 Player v2.08dev";
}

LPCWSTR CHEPSFPlugin::GetShortBanner() {
    return L"Highly Experimental";
}

LPCWSTR* CHEPSFPlugin::GetSupportedExtensions() {
    static LPCWSTR list[] = {
        L"psf"     , L"Playstation Sound Format",
        L"minipsf" , L"Mini-PSF",
        L"psf2"    , L"Playstation Sound Format 2",
        L"minipsf2", L"Mini-PSF2",
        NULL
    };
    return list;
}

LPCWSTR CHEPSFPlugin::GetOptionSection() {
    return L"HighlyExperimental";
}

LPCWSTR* CHEPSFPlugin::GetAllPSFTypes() {
    static LPCWSTR list[] = {
        L"PSF",
        NULL 
    };
    return list;
}

LPCWSTR CHEPSFPlugin::GetPSFType(int nPSFVersion) {
    switch(nPSFVersion) {
    case 0x01: return L"PSF";
    case 0x02: return L"PSF";
    }
    return L"";
}


/////////////////////////////////////////////////////////////////////////////

CHEPSFPlugin::CHEPSFPlugin()
{
    srand(time(NULL));
    ::psx_init();

    opt_bEnableMain   = TRUE;
    opt_bEnableReverb = TRUE;
    opt_bSimulateFrequencyResponse = TRUE;
    opt_bEmuShowCPUUsage = TRUE;
    opt_bEnableEasterEggs = TRUE;
}

CHEPSFPlugin::~CHEPSFPlugin() { }

/////////////////////////////////////////////////////////////////////////////

ESP_OPTION_START(CHEPSFPlugin, CPSFPlugin)

ESP_OPTION(L"EnableMain"               , OPT_BOOL, opt_bEnableMain               , ESP_ALL_PLAYERS)
ESP_OPTION(L"EnableReverb"             , OPT_BOOL, opt_bEnableReverb             , ESP_ALL_PLAYERS)
ESP_OPTION(L"SimulateFrequencyResponse", OPT_BOOL, opt_bSimulateFrequencyResponse, ESP_ALL_PLAYERS)
ESP_OPTION(L"EmuShowCPUUsage"          , OPT_BOOL, opt_bEmuShowCPUUsage          , ESP_WINAMP2)
ESP_OPTION(L"JinxIsAGoodBoy"           , OPT_BOOL, opt_bEnableEasterEggs         , ESP_ALL_PLAYERS)

ESP_OPTION_END(CHEPSFPlugin, CPSFPlugin)

/////////////////////////////////////////////////////////////////////////////

bool CHEPSFPlugin::CheckMagic(BYTE *buf, DWORD length) {
    if(length < 4) return false;
    if(memcmp(buf, "PSF", 3)) return false;
    switch(buf[3]) {
    case 0x01:
    case 0x02:
        return true;
    }
    return false;
}

/////////////////////////////////////////////////////////////////////////////

CPlayer* CHEPSFPlugin::CreatePlayer(File f, CStringW &strError) {

    int ver = CPSFPlugin::GrabPSFVersion(f, strError);
    if(ver < 0) return NULL;

    strError = L"Out of memory";
    switch(ver) {
    case 0x01: return new CHEPSF1Player(f);
    case 0x02: return new CHEPSF2Player(f);
    }
    strError = L"Unknown PSF version";
    return NULL;
}

/////////////////////////////////////////////////////////////////////////////

void CHEPSFPlugin::About(HWND hWndParent) {
    CAboutDlg dlg(CWnd::FromHandle(hWndParent));

    // TODO: version details again

    dlg.m_strBanner = GetBanner();

    CStringW s;

    s += GetBanner();

    s += L"\n";

    s += L"Built ";
    s += __DATE__;

    s += L"\n\n";

    s += GetFrameworkVersion();
    s += L"\n";
    s += ::psx_getversion();
    s += L"\n";
    s += CPSFDecompressor::GetVersion();

    dlg.m_strVersionDetails = s;

    dlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////

void CHEPSFPlugin::BuildConfigPropertySheet(CPropertySheet *pSheet) {

  CPSFPlugin::BuildConfigPropertySheet(pSheet);

  { CConfigEmuPage *p = new CConfigEmuPage;
    p->BindToPlugin(this);
    pSheet->AddPage(p);
  }
}

/////////////////////////////////////////////////////////////////////////////

int CHEPSFPlugin::GetPSFLogoBitmap(File f, CPSFTag *pPSFTag, int nVersion)
{
    int r = rand();

    if(opt_bEnableEasterEggs) {

        if((r & 255) == 121) {
            return IDB_JINXCANOE;
        }

    }

    if(nVersion == 1) return IDB_HE;
    if(nVersion == 2) return IDB_HE2;

    return IDB_HE;
}

/////////////////////////////////////////////////////////////////////////////
