// HEPSF2Player.cpp: implementation of the CHEPSF2Player class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "HEPSF2Player.h"

#include "../PSF/Util/PSF2FS.h"

#include "../PSX/Core/psx.h"

#include "HEPSFPlugin.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHEPSF2Player::CHEPSF2Player(File f) : CHEPSFPlayer(f)
{
    m_psf2fs = NULL;
}

CHEPSF2Player::~CHEPSF2Player()
{
    if(m_psf2fs) delete m_psf2fs;
}

/////////////////////////////////////////////////////////////////////////////

CStringW CHEPSF2Player::GetFormat() { return L"PSF2"; }

/////////////////////////////////////////////////////////////////////////////

void CHEPSF2Player::RawClose()
{
    CHEPSFPlayer::RawClose();
    if(m_psf2fs) {
        delete m_psf2fs;
        m_psf2fs = NULL;
    }
}

/////////////////////////////////////////////////////////////////////////////
//
// Callback for PSF2 virtual file reading
//
extern "C" sint32 EMU_CALL CHEPSF2Player__readfile_cb(
  void *context,
  const char *path,
  sint32 offset,
  char *buffer,
  sint32 length
) {
    CPSF2FS *fs = (CPSF2FS*)context;
    int r = fs->VirtualRead(path, offset, buffer, length);
//  sprintf(s,"readfile(%s,o=%d,l=%d) returned %d", path,offset,length,r);
//MessageBox(NULL,s,"readfile",MB_OK);
//  if(r < 0) {
//    sprintf(s,"error was '%s'", psf2fs_getlasterror(context));
//MessageBox(NULL,s,"readfile",MB_OK);
//  }
    return r;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CHEPSF2Player::RawOpen()
{
    if(m_psf2fs) {
        delete m_psf2fs;
        m_psf2fs = NULL;
    }
    m_psf2fs = new CPSF2FS;
    if(!m_psf2fs) {
        m_strLastError = L"psf2fs creation failed";
        return FALSE;
    }
    if(!m_psf2fs->AddArchive(GetFile(), 2)) {
        m_strLastError = m_psf2fs->GetLastError();
        delete m_psf2fs;
        m_psf2fs = NULL;
        return FALSE;
    }
    CreateHW(2);
    ::psx_set_readfile(m_aHW.GetData(), CHEPSF2Player__readfile_cb, m_psf2fs);

    m_dwInternalSampleRate = 48000;
    m_dwChannels = 2;

    m_filter.Redesign(48000);

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
