// HEPSFPlayer.cpp: implementation of the CHEPSFPlayer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "HEPSFPlayer.h"

#include "HEPSFPlugin.h"

#include "../PSX/Core/psx.h"
#include "../PSX/Core/iop.h"
#include "../PSX/Core/r3000.h"
#include "../PSX/Core/spu.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHEPSFPlayer::CHEPSFPlayer(File f, bool reference) : CPSFPlayer(f)
{
    m_aHW.SetSize(0);
    m_filter.Redesign(44100);
    m_isReference = reference;
    m_aSampleBuffer.SetSize(2 * BUFFER_SAMPLES);
}

CHEPSFPlayer::~CHEPSFPlayer()
{

}

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////

int CHEPSFPlayer::GetKBPS()
{
  BOOL bEmuShowCPUUsage = TRUE;
  g_pHEPSFPlugin->LockOptions();
  bEmuShowCPUUsage = g_pHEPSFPlugin->opt_bEmuShowCPUUsage;
  g_pHEPSFPlugin->UnlockOptions();
  if(!bEmuShowCPUUsage) return 0;

  // must ensure HW data exists to prevent a crash
  // unfortunately this function doesn't have any error reporting
  if(!m_aHW.GetSize()) return 0;

  void *psx_state = m_aHW.GetData();
  void *iop_state = ::psx_get_iop_state(psx_state);
  void *r3000_state = ::iop_get_r3000_state(iop_state);
  int f = ::r3000_get_usage_fraction(r3000_state);
  return (((f * 100) + 0x8000) / 0x10000);
}

/////////////////////////////////////////////////////////////////////////////

int CHEPSFPlayer::RawRead(float *buffer, int nSamples)
{
  if(!m_aHW.GetSize()) {
    m_strLastError = L"Emulation state not created (program error)";
    return -2;
  }

  BOOL bSim          = TRUE;
  BOOL bEnableMain   = TRUE;
  BOOL bEnableReverb = TRUE;
  if(!m_isReference) {
    g_pHEPSFPlugin->LockOptions();
    bSim          = g_pHEPSFPlugin->opt_bSimulateFrequencyResponse;
    bEnableMain   = g_pHEPSFPlugin->opt_bEnableMain;
    bEnableReverb = g_pHEPSFPlugin->opt_bEnableReverb;
    g_pHEPSFPlugin->UnlockOptions();
  }

  //
  // directly set the reverb enable/disable
  //
  void *psx_state = m_aHW.GetData();
  void *iop_state = ::psx_get_iop_state(psx_state);
  void *spu_state = ::iop_get_spu_state(iop_state);
  ::spu_enable_main(spu_state, bEnableMain);
  ::spu_enable_reverb(spu_state, bEnableReverb);

  BOOL bEmulationError = FALSE;
  BOOL bQuitCleanly = FALSE;
  int samples_left = nSamples;
  float *b = buffer;
  short *bi = buffer ? ((short*)m_aSampleBuffer.GetData()) : NULL;
  while(samples_left > 0) {
    int n = samples_left;
    if(n > BUFFER_SAMPLES) n = BUFFER_SAMPLES;
    int r = ::psx_execute(m_aHW.GetData(), 0x7FFFFFFF, bi, (unsigned*)(&n), 0);
    if(r < -1) {
      bEmulationError = TRUE;
      break;
    }
    if(r < 0) {
      bQuitCleanly = TRUE;
      break;
    }
    if(b) {
      CPlayer::Convert16bitToFloat(bi, b, 2 * n);
      b += 2 * n;
    }
    samples_left -= n;
  }
  int r = nSamples - samples_left;

  if(buffer && r && bSim) {
    m_filter.Process(buffer, r);
  }

  if(!r) {
    if(bEmulationError) {
      void *psx_state = m_aHW.GetData();
      void *iop_state = ::psx_get_iop_state(psx_state);
      void *r3000_state = ::iop_get_r3000_state(iop_state);
      DWORD dwPC = ::r3000_getreg(r3000_state, R3000_REG_PC);
      m_strLastError.Format(L"Unrecoverable emulation error, PC=0x%08X", dwPC);
      r = -2;
    } else if(bQuitCleanly) {
      r = -1;
    }
  }
  return r;
}

/////////////////////////////////////////////////////////////////////////////

void CHEPSFPlayer::RawClose()
{
  m_aHW.SetSize(0);
}

/////////////////////////////////////////////////////////////////////////////

void CHEPSFPlayer::CreateHW(int nVersion)
{
  m_aHW.SetSize(0);
  m_aHW.SetSize(::psx_get_state_size(nVersion));
  ::psx_clear_state(m_aHW.GetData(), nVersion);
  void *psx_state = m_aHW.GetData();
  void *iop_state = ::psx_get_iop_state(psx_state);
  ::iop_set_compat(
    iop_state,
    IOP_COMPAT_FRIENDLY
  );
}

/////////////////////////////////////////////////////////////////////////////
