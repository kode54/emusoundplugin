#pragma once

#include "HEPSFPlugin.h"

/////////////////////////////////////////////////////////////////////////////
// CConfigEmuPage dialog

class CConfigEmuPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CConfigEmuPage)

// Construction
public:
	void BindToPlugin(CHEPSFPlugin *pPlugin);
	CConfigEmuPage();
	~CConfigEmuPage();

// Dialog Data
	//{{AFX_DATA(CConfigEmuPage)
	enum { IDD = IDD_CONFIG_EMU };
	CButton	m_cShowCPUUsage;
	CButton	m_cReverb;
	CButton	m_cMain;
	CButton	m_cFreqResponse;
	//}}AFX_DATA

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CConfigEmuPage)
	public:
	virtual void OnOK();
	virtual void OnCancel();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CConfigEmuPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnMain();
	afx_msg void OnReverb();
	afx_msg void OnFreqresponse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
  CHEPSFPlugin *m_pPlugin;

  BOOL m_bOriginalMain;
  BOOL m_bOriginalReverb;
  BOOL m_bOriginalSim;

};
