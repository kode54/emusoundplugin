// ConfigEmuPage.cpp : implementation file
//

#include "stdafx.h"

#include "Resource.h"

#include "ConfigEmuPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfigEmuPage property page

IMPLEMENT_DYNCREATE(CConfigEmuPage, CPropertyPage)

CConfigEmuPage::CConfigEmuPage() : CPropertyPage(CConfigEmuPage::IDD)
{
  m_pPlugin = NULL;

  m_bOriginalMain = TRUE;
  m_bOriginalReverb = TRUE;
  m_bOriginalSim = TRUE;

	//{{AFX_DATA_INIT(CConfigEmuPage)
	//}}AFX_DATA_INIT
}

CConfigEmuPage::~CConfigEmuPage()
{
}

void CConfigEmuPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigEmuPage)
	DDX_Control(pDX, IDC_SHOWCPUUSAGE, m_cShowCPUUsage);
	DDX_Control(pDX, IDC_REVERB, m_cReverb);
	DDX_Control(pDX, IDC_MAIN, m_cMain);
	DDX_Control(pDX, IDC_FREQRESPONSE, m_cFreqResponse);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfigEmuPage, CPropertyPage)
	//{{AFX_MSG_MAP(CConfigEmuPage)
	ON_BN_CLICKED(IDC_MAIN, OnMain)
	ON_BN_CLICKED(IDC_REVERB, OnReverb)
	ON_BN_CLICKED(IDC_FREQRESPONSE, OnFreqresponse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigEmuPage message handlers


BOOL CConfigEmuPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

  if(m_pPlugin) {
    m_pPlugin->LockOptions();

    m_bOriginalMain = m_pPlugin->opt_bEnableMain;
    m_bOriginalReverb = m_pPlugin->opt_bEnableReverb;
    m_bOriginalSim = m_pPlugin->opt_bSimulateFrequencyResponse;

    m_cMain.SetCheck(m_pPlugin->opt_bEnableMain ? BST_CHECKED : BST_UNCHECKED);
    m_cReverb.SetCheck(m_pPlugin->opt_bEnableReverb ? BST_CHECKED : BST_UNCHECKED);
    m_cFreqResponse.SetCheck(m_pPlugin->opt_bSimulateFrequencyResponse ? BST_CHECKED : BST_UNCHECKED);

    m_cShowCPUUsage.SetCheck(m_pPlugin->opt_bEmuShowCPUUsage ? BST_CHECKED : BST_UNCHECKED);
    if(!m_pPlugin->IsOptionSupported(&m_pPlugin->opt_bEmuShowCPUUsage)) {
        m_cShowCPUUsage.ShowWindow(SW_HIDE);
        m_cShowCPUUsage.EnableWindow(FALSE);
    }

    m_pPlugin->UnlockOptions();
  }
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CConfigEmuPage::OnOK() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();

    m_pPlugin->opt_bEnableMain = (m_cMain.GetCheck() == BST_CHECKED);
    m_pPlugin->opt_bEnableReverb = (m_cReverb.GetCheck() == BST_CHECKED);
    m_pPlugin->opt_bSimulateFrequencyResponse = (m_cFreqResponse.GetCheck() == BST_CHECKED);

    m_pPlugin->opt_bEmuShowCPUUsage = (m_cShowCPUUsage.GetCheck() == BST_CHECKED);

    m_pPlugin->UnlockOptions();
  }

	CPropertyPage::OnOK();
}

void CConfigEmuPage::OnCancel() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();

    m_pPlugin->opt_bEnableMain = m_bOriginalMain;
    m_pPlugin->opt_bEnableReverb = m_bOriginalReverb;
    m_pPlugin->opt_bSimulateFrequencyResponse = m_bOriginalSim;

    m_pPlugin->UnlockOptions();
  }

	CPropertyPage::OnCancel();
}

void CConfigEmuPage::BindToPlugin(CHEPSFPlugin *pPlugin)
{
  m_pPlugin = pPlugin;
}

void CConfigEmuPage::OnMain() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();
    m_pPlugin->opt_bEnableMain = (m_cMain.GetCheck() == BST_CHECKED);
    m_pPlugin->UnlockOptions();
  }
}

void CConfigEmuPage::OnReverb() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();
    m_pPlugin->opt_bEnableReverb = (m_cReverb.GetCheck() == BST_CHECKED);
    m_pPlugin->UnlockOptions();
  }
}

void CConfigEmuPage::OnFreqresponse() 
{
  if(m_pPlugin) {
    m_pPlugin->LockOptions();
    m_pPlugin->opt_bSimulateFrequencyResponse = (m_cFreqResponse.GetCheck() == BST_CHECKED);
    m_pPlugin->UnlockOptions();
  }
}
