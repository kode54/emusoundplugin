//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by in_psf.rc
//
#define IDC_BROWSE                      3
#define IDD_INFO                        1000
#define IDC_PATHNAME                    1000
#define IDD_ABOUT                       1001
#define IDC_TITLE                       1001
#define IDC_ARTIST                      1002
#define IDB_HE                          1003
#define IDC_GAME                        1003
#define IDC_YEAR                        1004
#define IDC_GENRE                       1005
#define IDC_COMMENT                     1006
#define IDD_CONFIG_SOUND                1006
#define IDC_COPYRIGHT                   1007
#define IDD_RAWTAG                      1007
#define IDC_PSFBY                       1008
#define IDD_CONFIG_EMU                  1009
#define IDC_RAWMODE                     1010
#define IDC_VOLUMESLIDER                1011
#define IDD_DEBUG                       1011
#define IDB_HE2                         1012
#define IDC_BYTES                       1016
#define IDB_JINXCANOE                   1016
#define IDC_FADE                        1017
#define IDC_LENGTH                      1018
#define IDC_NOW                         1021
#define IDC_REVERB                      1027
#define IDC_FREQRESPONSE                1028
#define IDC_MAIN                        1031
#define IDC_URL                         1035
#define IDC_TAG                         1036
#define IDC_VOLUME                      1037
#define IDC_VERSION                     1039
#define IDC_SHOWCPUUSAGE                1040
#define IDC_HELOGO                      1041
#define IDC_TIME                        1042
#define IDC_PSFFILE                     1043
#define IDC_RESULTS                     1046
#define IDC_VERSIONDETAILS              1047

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1017
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1051
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
