// in_psf.h : main header file for the IN_PSF DLL
//

#if !defined(AFX_IN_PSF_H__B2597B4F_0CAC_4DFB_8933_8887A67494A9__INCLUDED_)
#define AFX_IN_PSF_H__B2597B4F_0CAC_4DFB_8933_8887A67494A9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////

class CIn_psfApp : public CWinApp
{
public:
	CIn_psfApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIn_psfApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CIn_psfApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IN_PSF_H__B2597B4F_0CAC_4DFB_8933_8887A67494A9__INCLUDED_)
