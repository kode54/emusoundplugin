//////////////////////////////////////////////////////////////////////////////
//
// NoCompress
//
// Stub functions, here for the sole purpose of not having to link the
// compression libs.  They all return an error of some sort.
//
//////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////////

bool compress_rfc1950_7z(const unsigned char* in_data, unsigned in_size, unsigned char* out_data, unsigned& out_size, unsigned num_passes, unsigned num_fast_bytes) { return 0; }
extern "C" int compress2(unsigned char *dest, unsigned long *destLen, const unsigned char *source, unsigned long sourceLen, int level) { return -6; }

//////////////////////////////////////////////////////////////////////////////
