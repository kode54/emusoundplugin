// HEPSFPlugin.h: interface for the CHEPSFPlugin class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HEPSFPLUGIN_H__6F60C11C_1D57_490B_A234_619FBABFCD17__INCLUDED_)
#define AFX_HEPSFPLUGIN_H__6F60C11C_1D57_490B_A234_619FBABFCD17__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../PSF/Util/PSFPlugin.h"

class CHEPSFPlugin : public CPSFPlugin  
{
public:
	virtual void About(HWND hWndParent);
	virtual LPCWSTR* GetSupportedExtensions();
	virtual LPCWSTR  GetOptionSection();
	virtual LPCWSTR  GetBanner();
	virtual LPCWSTR  GetShortBanner();
	virtual LPCWSTR* GetAllPSFTypes();
	virtual LPCWSTR GetPSFType(int nPSFVersion);
	virtual int GetPSFLogoBitmap(File f, CPSFTag *pPSFTag, int nVersion);
    virtual bool CheckMagic(BYTE *buf, DWORD length);

    BOOL opt_bEnableMain;
    BOOL opt_bEnableReverb;
    BOOL opt_bSimulateFrequencyResponse;
    BOOL opt_bEmuShowCPUUsage;
    BOOL opt_bEnableEasterEggs;

	CHEPSFPlugin();
	virtual ~CHEPSFPlugin();

protected:
    ESP_OPTION_DECLARE

    virtual void BuildConfigPropertySheet(CPropertySheet *pSheet);
    virtual CPlayer* CreatePlayer(File f, CStringW &strError);

};

extern CHEPSFPlugin *g_pHEPSFPlugin;

#endif // !defined(AFX_HEPSFPLUGIN_H__6F60C11C_1D57_490B_A234_619FBABFCD17__INCLUDED_)
