#pragma once

#include "HEPSFPlayer.h"

#include "../PSX/Util/PSXEXE.h"

#include "../MFCUtil/File.h"

class CHEPSF1Player : public CHEPSFPlayer  
{
public:
	CHEPSF1Player(File f);
	virtual ~CHEPSF1Player();

    virtual CStringW GetFormat();

private:
    BOOL BuildExecutable(File f, CPSXEXE &exe, int nLevel, int &nRefresh);

protected:
	virtual BOOL RawOpen();
};
