#if !defined(AFX_ABOUTDLG_H__3CDD85F3_0574_4B2F_9901_21FE27F8CDBF__INCLUDED_)
#define AFX_ABOUTDLG_H__3CDD85F3_0574_4B2F_9901_21FE27F8CDBF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AboutDlg.h : header file
//

#include "resource.h"

#include "../MFCUtil/LinkText.h"

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog

class CAboutDlg : public CDialog
{
// Construction
public:
	CStringW m_strBanner;
	CStringW m_strVersionDetails;
	CAboutDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUT };
	CStatic	m_cVersion;
	CLinkText	m_cURL;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnVersiondetails();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void SecretCode();
	int m_nSecretCodeIndex;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ABOUTDLG_H__3CDD85F3_0574_4B2F_9901_21FE27F8CDBF__INCLUDED_)
