#pragma once

#include "../PSF/Util/PSFPlayer.h"
#include "PSXFilter.h"

class CHEPSFPlayer : public CPSFPlayer  
{
public:
	virtual int GetKBPS();

	CHEPSFPlayer(File f, bool reference = false);
	virtual ~CHEPSFPlayer();

    enum { BUFFER_SAMPLES = 576 };

protected:
	virtual void RawClose();
	virtual int RawRead(float *buffer, int nSamples);

	void CreateHW(int nVersion);
    CByteArray m_aHW;

	CPSXFilter m_filter;

    CWordArray m_aSampleBuffer;

private:

    bool m_isReference;

};
