// HEPSF1Player.cpp: implementation of the CHEPSF1Player class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "HEPSF1Player.h"

#include "HEPSFPlugin.h"

#include "../PSX/Core/psx.h"

#include "../MFCUtil/NumString.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHEPSF1Player::CHEPSF1Player(File f) : CHEPSFPlayer(f)
{
}

CHEPSF1Player::~CHEPSF1Player()
{
}

/////////////////////////////////////////////////////////////////////////////

CStringW CHEPSF1Player::GetFormat() { return L"PSF1"; }

/////////////////////////////////////////////////////////////////////////////

BOOL CHEPSF1Player::BuildExecutable(File f, CPSXEXE &exe, int nLevel, int &nRefresh)
{
    g_pHEPSFPlugin->LockOptions();
    BOOL bStrict = g_pHEPSFPlugin->opt_bStrictFormatChecking;
    g_pHEPSFPlugin->UnlockOptions();

    if(nLevel > 10) {
        BuildExecutableError(f, L"Recursion limit exceeded");
        exe.ReleaseData();
        return FALSE;
    }

    CPSF psf;

    //
    // Attempt to read the file - version, exe, and tag
    //
    if(!psf.ReadFromFile(f,
        CPSF::PSF_FILE_VERSION |
        CPSF::PSF_FILE_ZEXE |
        CPSF::PSF_FILE_TAG,
        bStrict
    )) {
        BuildExecutableError(f, psf.GetLastError());
        exe.ReleaseData();
        return FALSE;
    }

    //
    // Attempt to decompress the exe from the psf
    //
    // (note that the CPSXEXE class already checks to make sure the
    // version number is 0x01)
    //
    if(!exe.ReadFromPSF(psf, bStrict)) {
        BuildExecutableError(f, exe.GetLastError());
        exe.ReleaseData();
        return FALSE;
    }

    //
    // Library tag stuff
    //
    CPSFTag tag;
    if(!tag.ReadFromPSF(psf, bStrict)) {
        BuildExecutableError(f, tag.GetLastError());
        exe.ReleaseData();
        return FALSE;
    }

    //
    // If we don't have a refresh yet, try to get it from the _refresh tag
    //
    if(nRefresh != 50 && nRefresh != 60) {
        CStringW strRefresh = tag.GetVarW(L"_refresh");
        if(!(strRefresh.IsEmpty())) {
            double d = 0.0;
            if(Util::StringToNum(strRefresh, d, 0)) nRefresh = d;
        }
    }

    //
    // Handle _lib
    // Note that in this case we must preserve the original region data
    //
    CPSXEXE libexe;
    CStringW strVar;
    CStringW strLib = tag.GetVarW(L"_lib");
    if(!(strLib.IsEmpty())) {
        CByteArray regiondata;
        exe.GetRegionData(regiondata);
        libexe.CopyFrom(&exe);
        if(!BuildExecutable(File(f, CPSF::MakeLibPath(f.GetPath(), strLib)), exe, nLevel + 1, nRefresh)) {
            exe.ReleaseData();
            return FALSE;
        }
        exe.SuperimposeFrom(&libexe);
        exe.SetRegionData(regiondata);
    }
    //
    // Handle _lib2, etc.
    //
    for(int i = 2;; i++) {
        strVar.Format(L"_lib%d", i);
        strLib = tag.GetVarW(strVar);
        if(strLib.IsEmpty()) break;
        if(!BuildExecutable(File(f, CPSF::MakeLibPath(f.GetPath(), strLib)), libexe, nLevel + 1, nRefresh)) {
            exe.ReleaseData();
            return FALSE;
        }
        exe.SuperimposeFrom(&libexe);
    }

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CHEPSF1Player::RawOpen()
{
    CPSXEXE exe;
    int nRefresh = 0; // default
    if(!BuildExecutable(GetFile(), exe, 0, nRefresh)) return FALSE;

    CreateHW(1);
    ::psx_upload_psxexe(m_aHW.GetData(), exe.GetEXEBuffer(), exe.GetEXESize());
    // override refresh if necessary
    if(nRefresh == 50 || nRefresh == 60) {
        ::psx_set_refresh(m_aHW.GetData(), nRefresh);
    }

    m_dwInternalSampleRate = 44100;
    m_dwChannels = 2;

    m_filter.Redesign(44100);

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
