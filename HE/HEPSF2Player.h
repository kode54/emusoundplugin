// HEPSF2Player.h: interface for the CHEPSF2Player class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HEPSF2PLAYER_H__EA3D0799_8262_4807_B364_A09F7F904288__INCLUDED_)
#define AFX_HEPSF2PLAYER_H__EA3D0799_8262_4807_B364_A09F7F904288__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "HEPSFPlayer.h"

#include "../PSF/Util/PSF2FS.h"

class CHEPSF2Player : public CHEPSFPlayer  
{
public:
	CHEPSF2Player(File f);
	virtual ~CHEPSF2Player();

    virtual CStringW GetFormat();

private:
	CPSF2FS *m_psf2fs;
protected:
	virtual BOOL RawOpen();
	virtual void RawClose();
};

#endif // !defined(AFX_HEPSF2PLAYER_H__EA3D0799_8262_4807_B364_A09F7F904288__INCLUDED_)
