// DebugDlg.cpp : implementation file
//

#include "stdafx.h"

#include "Resource.h"

#include "DebugDlg.h"

#include "HEPSFPlugin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "../MFCUtil/MFCFileObject.h"

/////////////////////////////////////////////////////////////////////////////
// CDebugDlg dialog


CDebugDlg::CDebugDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDebugDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDebugDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDebugDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDebugDlg)
	DDX_Control(pDX, IDC_RESULTS, m_cResults);
	DDX_Control(pDX, IDC_PSFFILE, m_cPSFFile);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDebugDlg, CDialog)
	//{{AFX_MSG_MAP(CDebugDlg)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
	ON_BN_CLICKED(IDC_TIME, OnTime)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDebugDlg message handlers

//extern "C" {
//extern int spucore_frq[300];
//}

void CDebugDlg::OnBrowse() 
{
  CFileDialog dlg(
    TRUE, // open
    NULL,
    NULL,
    //OFN_ALLOWMULTISELECT|
      OFN_FILEMUSTEXIST|
      OFN_HIDEREADONLY,
    _T("All files (*.*)|*.*||"),
    this
  );
  dlg.m_ofn.lpstrTitle = _T("Browse");
  if(dlg.DoModal() != IDOK) return;
  m_cPSFFile.SetWindowText(dlg.GetPathName());
}

void CDebugDlg::OnTime() 
{
    CString strFile;
    m_cPSFFile.GetWindowText(strFile);

//MessageBox(strFile);

  CStringW strError;
  CPlayer *pPlayer = g_pPlugin->Open(MFCFileObject(CStringW(strFile)), strError);
//MessageBox("after create");
  if(!pPlayer) {
    CStringW s;
    s.Format(L"Error opening %s:\n%s", (LPCWSTR)CStringW(strFile), (LPCWSTR)CStringW(strError));
    MessageBox(CString(s));
    return;
  }

  CWaitCursor cwc;
  CDWordArray dwabuffer;
  dwabuffer.SetSize(2*4410);
  float *buffer = (float*)(dwabuffer.GetData());
  int i;

  DWORD play_tStart = ::GetTickCount();
  for(i = 0; i < 60*10; i++) {
    int r = pPlayer->Read(buffer, 4410);
    if(r < 0) {
      CStringW s;
      s.Format(L"read error %d lasterror='%s'", r, LPCWSTR(pPlayer->GetLastError()));
      MessageBox(CString(s));
      break;
    }
  }

  DWORD play_tEnd   = ::GetTickCount();

  BOOL b = pPlayer->Rewind();
  if(!b) {
    CStringW s;
    s.Format(L"rewind failed: '%s'", LPCWSTR(pPlayer->GetLastError()));
    MessageBox(CString(s));
  }

//memset(spucore_frq,0,sizeof(spucore_frq));

    DWORD seek_tStart = ::GetTickCount();

    pPlayer->IncrementalSeek(60*44100, 60*44100);

    DWORD seek_tEnd   = ::GetTickCount();

    g_pPlugin->Close(pPlayer);

//{FILE*f=fopen("C:\\corlett\\spufrq","wb");if(f){fwrite(spucore_frq,1,sizeof(spucore_frq),f);fclose(f);}}

    DWORD playticks = play_tEnd - play_tStart;
    DWORD seekticks = seek_tEnd - seek_tStart;

    CStringW s;
    s.Format(L"%s\r\n\r\n44100-sample play: %d ticks\r\n44100-sample seek: %d ticks\r\n", 
        LPCWSTR(CStringW(strFile)),
        playticks, seekticks
    );
    m_cResults.SetWindowText(CString(s));

}
