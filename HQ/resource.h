//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by in_qsf.rc
//
#define IDC_URL                         1035
#define IDC_VERSION                     1039
#define IDC_VERSIONDETAILS              1047
#define IDB_HQ                          4000
#define IDD_ABOUT                       4001

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        4002
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         4000
#define _APS_NEXT_SYMED_VALUE           4000
#endif
#endif
