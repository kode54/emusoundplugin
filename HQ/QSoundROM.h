#pragma once

class CQSoundROM  
{
public:
	BOOL SuperimposeFrom(CQSoundROM &from);
	BOOL UploadSection(LPCSTR section, DWORD start, LPBYTE data, DWORD size);
	void Clear();
	CQSoundROM();
	virtual ~CQSoundROM();

	CByteArray m_aKey;
    CDWordArray m_aKeyValid;
	CByteArray m_aZ80ROM;
    CDWordArray m_aZ80ROMValid;
	CByteArray m_aSampleROM;
    CDWordArray m_aSampleROMValid;

    CStringW GetLastError() const { return m_strLastError; }

private:
	BOOL SuperimposeSectionFrom(LPCSTR section, CByteArray &from, CDWordArray &fromvalid);
	CStringW m_strLastError;
};
