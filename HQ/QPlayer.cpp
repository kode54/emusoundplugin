// QPlayer.cpp: implementation of the CQPlayer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "in_qsf.h"
#include "QPlayer.h"

#include "HQPlugin.h"

#include "../QSound/Core/qsound.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CQPlayer::CQPlayer(File f) : CPSFPlayer(f)
{
  m_ROM.Clear();
  m_aHW.SetSize(0);
}

CQPlayer::~CQPlayer()
{
}

/////////////////////////////////////////////////////////////////////////////

CStringW CQPlayer::GetFormat() { return L"QSF"; }

/////////////////////////////////////////////////////////////////////////////

static uint32 get32lsb(uint8 *src) {
  return
    ((((uint32)(src[0])) & 0xFF) <<  0) |
    ((((uint32)(src[1])) & 0xFF) <<  8) |
    ((((uint32)(src[2])) & 0xFF) << 16) |
    ((((uint32)(src[3])) & 0xFF) << 24);
}

static uint32 get32msb(uint8 *src) {
  return
    ((((uint32)(src[3])) & 0xFF) <<  0) |
    ((((uint32)(src[2])) & 0xFF) <<  8) |
    ((((uint32)(src[1])) & 0xFF) << 16) |
    ((((uint32)(src[0])) & 0xFF) << 24);
}

static uint16 get16msb(uint8 *src) {
  return
    ((((uint16)(src[1])) & 0xFF) <<  0) |
    ((((uint16)(src[0])) & 0xFF) <<  8);
}

static uint8 get8msb(uint8 *src) { return *src; }

/////////////////////////////////////////////////////////////////////////////

BOOL CQPlayer::BuildExecutable(File f, CQSoundROM &rom, int nLevel)
{
    CByteArray program;

    rom.Clear();

    g_pHQPlugin->LockOptions();
    BOOL bStrict = g_pHQPlugin->opt_bStrictFormatChecking;
    g_pHQPlugin->UnlockOptions();

    if(nLevel > 10) {
        BuildExecutableError(f, L"Recursion limit exceeded");
        return FALSE;
    }

    CPSF psf;

    //
    // Attempt to read the file - version, exe, and tag
    //
    if(!psf.ReadFromFile(f,
        CPSF::PSF_FILE_VERSION |
        CPSF::PSF_FILE_ZEXE |
        CPSF::PSF_FILE_TAG,
        bStrict
    )) {
        BuildExecutableError(f, psf.GetLastError());
        return FALSE;
    }

    //
    // Verify version
    //
    int v = psf.GetVersion();
    if(v != 0x41) {
        BuildExecutableError(f, L"Invalid version: not a QSF file");
        return FALSE;
    }

    //
    // Attempt to decompress the exe from the psf
    //
    DWORD dwSize = 0x1000000; // max 16 megs for now
    program.SetSize(dwSize);
    if(!psf.GetProgramData(program.GetData(), &dwSize)) {
        BuildExecutableError(f, psf.GetLastError());
        rom.Clear();
        return FALSE;
    }
    program.SetSize(dwSize);

  // Copy tag data to ROM
  { BYTE *src = program.GetData();
    DWORD size = program.GetSize();
    for(;;) {
      char s[4];
      if(size < 11) break;
      memcpy(s, src, 3); src += 3; size -= 3;
      s[3] = 0;
      DWORD dataofs  = get32lsb(src); src += 4; size -= 4;
      DWORD datasize = get32lsb(src); src += 4; size -= 4;
      if(datasize > size) {
        BuildExecutableError(f, L"Unexpected end of program section");
        rom.Clear();
        return FALSE;
      }

      if(!rom.UploadSection(s, dataofs, src, datasize)) {
        BuildExecutableError(f, rom.GetLastError());
        rom.Clear();
        return FALSE;
      }
      src += datasize;
      size -= datasize;
    }
  }
  // we're done with the program area
  program.SetSize(0);

  /*
  ** Library tag stuff
  */
  CPSFTag tag;
  if(!tag.ReadFromPSF(psf, bStrict)) {
    BuildExecutableError(f, tag.GetLastError());
    rom.Clear();
    return FALSE;
  }

  CStringW strVar;
  CStringW strLib;

  CQSoundROM librom;

  /*
  ** Handle _lib
  */
  strLib = tag.GetVarW(L"_lib");
  if(!(strLib.IsEmpty())) {
    librom.Clear();
    if(!BuildExecutable(File(f, CPSF::MakeLibPath(f.GetPath(), strLib)), librom, nLevel + 1)) {
      rom.Clear();
      return FALSE;
    }

    // superimpose actual onto lib, then switch
    if(!librom.SuperimposeFrom(rom)) {
      BuildExecutableError(f, librom.GetLastError());
      rom.Clear(); return FALSE;
    }
    rom.Clear();
    if(!rom.SuperimposeFrom(librom)) {
      BuildExecutableError(f, rom.GetLastError());
      rom.Clear(); return FALSE;
    }
  }
  /*
  ** Handle _lib2, etc.
  */
  for(int i = 2;; i++) {
    strVar.Format(L"_lib%d", i);
    strLib = tag.GetVarW(strVar);
    if(strLib.IsEmpty()) break;
    if(!BuildExecutable(File(f, CPSF::MakeLibPath(f.GetPath(), strLib)), librom, nLevel + 1)) {
      rom.Clear();
      return FALSE;
    }
    // superimpose
    if(!rom.SuperimposeFrom(librom)) {
      BuildExecutableError(f, rom.GetLastError());
      rom.Clear();
      return FALSE;
    }
  }

  // success
  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CQPlayer::RawOpen()
{
    if(!BuildExecutable(GetFile(), m_ROM, 0)) return FALSE;

    // Create hardware state
    m_aHW.SetSize(0);
    m_aHW.SetSize(::qsound_get_state_size());
    ::qsound_clear_state(m_aHW.GetData());

    // Set ROM info
    if(m_ROM.m_aKey.GetSize() == 11) {
        DWORD swap_key1 = get32msb(m_ROM.m_aKey.GetData() +  0);
        DWORD swap_key2 = get32msb(m_ROM.m_aKey.GetData() +  4);
        WORD  addr_key  = get16msb(m_ROM.m_aKey.GetData() +  8);
        BYTE  xor_key   = get8msb (m_ROM.m_aKey.GetData() + 10);
        ::qsound_set_kabuki_key(m_aHW.GetData(),
            swap_key1, swap_key2, addr_key, xor_key
        );
    } else {
        ::qsound_set_kabuki_key(m_aHW.GetData(), 0, 0, 0, 0);
    }
    ::qsound_set_z80_rom(
        m_aHW.GetData(),
        m_ROM.m_aZ80ROM.GetData(),
        m_ROM.m_aZ80ROM.GetSize()
    );
    ::qsound_set_sample_rom(
        m_aHW.GetData(),
        m_ROM.m_aSampleROM.GetData(),
        m_ROM.m_aSampleROM.GetSize()
    );

    // TODO

    // Set other info
    m_dwInternalSampleRate = 44100;
    m_dwChannels = 2;

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void CQPlayer::RawClose()
{
    m_ROM.Clear();
    m_aHW.SetSize(0);
}

/////////////////////////////////////////////////////////////////////////////

int CQPlayer::RawRead(short *buffer, int nSamples)
{
  if(!m_aHW.GetSize()) {
    m_strLastError = L"Emulation state not created (program error)";
    return -2;
  }

  // this is where you'd do option syncing etc.
//  g_pHTPlugin->LockOptions();
//  BOOL bEnableDry  = g_pHTPlugin->opt_bEnableDry;
//  g_pHTPlugin->UnlockOptions();
//  ::sega_enable_dry        (m_aHW.GetData(), bEnableDry);

  BOOL bEmulationError = FALSE;
  int samples_left = nSamples;
  short *b = buffer;
  while(samples_left > 0) {
    int n = samples_left;
    int r = ::qsound_execute(m_aHW.GetData(), 0x7FFFFFFF, b, (unsigned*)(&n));
    if(r < 0) {
      bEmulationError = TRUE;
      break;
    }
    if(b) { b += 2 * n; }
    samples_left -= n;
  }
  int r = nSamples - samples_left;

  if(!r) {
    if(bEmulationError) {
      DWORD dwPC = ::qsound_getpc(m_aHW.GetData());
      m_strLastError.Format(L"Unrecoverable emulation error, PC=0x%08X", dwPC);
      r = -1;
    }
  }

  return r;
}

/////////////////////////////////////////////////////////////////////////////
