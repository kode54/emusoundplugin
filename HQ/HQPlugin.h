#pragma once

#include "../PSF/Util/PSFPlugin.h"

class CHQPlugin : public CPSFPlugin  
{
public:
	virtual void About(HWND hWndParent);
	virtual LPCWSTR* GetSupportedExtensions();
	virtual LPCWSTR  GetOptionSection();
	virtual LPCWSTR  GetBanner();
	virtual LPCWSTR  GetShortBanner();
	virtual LPCWSTR* GetAllPSFTypes();
	virtual LPCWSTR GetPSFType(int nPSFVersion);
	virtual int GetPSFLogoBitmap(File f, CPSFTag *pPSFTag, int nVersion);
    virtual bool CheckMagic(BYTE *buf, DWORD length);

// local options go here
//  BOOL opt_bEnableDry;

	CHQPlugin();
	virtual ~CHQPlugin();

protected:
    ESP_OPTION_DECLARE

    virtual void BuildConfigPropertySheet(CPropertySheet *pSheet);
    virtual CPlayer* CreatePlayer(File f, CStringW &strError);

};

extern CHQPlugin *g_pHQPlugin;
