-----------------------------------------------------------------------------
Highly Quixotic v1.01
QSF player plugin for Winamp
Copyright 2003-2004 Neill Corlett
-----------------------------------------------------------------------------

Introduction
------------

Highly Quixotic is an input plugin for Winamp that plays QSF files, which
contain music from Capcom CPS1 and CPS2 arcade games using QSound hardware.
Highly Quixotic emulates the Z80 CPU and sound hardware in order to play the
music from these games.  This is similar in concept to Icer Addis' original
QSound Player, except that QSF allows the QSound ROM data to be individually
queued in a playlist or tagged.

If you don't have any QSF files to play, you can start by checking my home
page at http://www.neillcorlett.com/, where I've linked to a few archive
sites.

-----------------------------------------------------------------------------

File Format Overview
--------------------

.qsf (QSound Format) - A self-contained file with one song.  Includes full
  Z80 and sample ROM data.

.miniqsf (Miniature QSound Format) - An abbreviated file containing only data
  specific to one song, usually just a command number.  Relies on .qsflib
  files, residing in the same directory, for the remainder of the Z80 code
  and/or samples.

.qsflib (QSound Format Library) - A file containing common Z80 and sample
  data for one or more .miniqsf files.  Not guaranteed to be playable as a
  separate file.

-----------------------------------------------------------------------------

Requirements
------------

* A Pentium or higher PC.  I'd recommend at least a Pentium Pro 200.
* A media player which supports either the Winamp 2 or 3 plugin API.
  Note: Winamp 5 will work too, since it uses the Winamp 2 API.
  Winamp can be downloaded freely from http://www.winamp.com

-----------------------------------------------------------------------------

Setup
-----

For Winamp 2 or 5:

Copy in_qsf.dll into the Plugins directory inside your Winamp directory,
i.e. "C:\Program Files\Winamp\Plugins\".  Close and re-start Winamp if
necessary so it will recognize the new plugin.

For Winamp 3:

Rename in_qsf.dll to "cnv_qsf.wac" and copy it into the Wacs directory inside
your Winamp directory.  Close and re-start Winamp if necessary so it will
recognize the new plugin.

-----------------------------------------------------------------------------

Configuration (WINAMP 2 AND 5 ONLY)
-------------

Highly Quixotic saves all its options into the plugin.ini file in the same
directory as the DLL, under the section [HighlyQuixotic].  Here's a rundown
of the options you may configure:

General:

* Force all songs to play indefinitely

  Select this if you want all songs to loop for as long as possible.  Winamp
  will still show the standard length and standard seekbar, but when it
  reaches the end of the seekbar, it will keep going.  Note that songs may
  still end if there is a prolonged period of silence, and you have the
  "Stop after ___ sec. of silence" option enabled.

* Default length / fade

  This is the default length for QSFs that don't have length information in
  their tags.  Format is minutes:seconds.

* Suppress opening silence

  This will cause the plugin to skip silence at the beginning of songs.

* Stop after ___ sec. of silence

  This will cause songs to end prematurely if a prolonged period of silence
  is detected.  This is useful for songs that don't loop, but also don't have
  a length tag.  You can define how prolonged is "prolonged" by entering a
  number of seconds into the edit box.

* Thread Priority

  Selects the priority of the emulation thread.  Higher priorities may help
  if you encounter problems with skipping.

  Note that if you select "Above Normal" or "Highest", the thread will
  temporarily demote itself to "Normal" during initial load and seeking.

Output:

* Global Volume

  This is a multiplier that affects the volume of all QSF files and is
  independant of Winamp's main volume.  Change this if you notice that all
  your QSFs are a different volume than all your MP3s, or something similar.
  1.0 is normal.

* Soft Saturate instead of clipping

  Reduces pops and clicks associated with clipping by using a soft saturation
  algorithm.  No more wincing at those loud crescendos!  You should leave this
  enabled unless you know you have a reason to disable it.

* Resample output to _____ Hz

  Normally, QSF files play at 44100 Hz.  Enable this option if you'd rather
  have them play at the sample rate of your choice.  This option increases
  CPU usage, so you probably shouldn't use it unless you have to.

Errors:

* Perform strict format checking on all files

  Enable this option to perform additional file format checks on every file
  played.  Highly Quixotic will refuse to play a file that fails the checks.

  Note that CRC checking is now no longer performed unless you have this
  option enabled.  However, zlib's uncompress function should be robust
  enough to handle most corrupt files.

* Show error messages instead of silently advancing

  With this checked, Highly Quixotic will verbosely pop up a message box on
  every error, rather than just allowing Winamp to advance to the next song.

  This is disabled by default, because I don't think plugins are supposed to
  do this.  Consider:  If you're running Winamp in the background or you're
  away from the PC, and it's shuffling through a large playlist, and a few of
  the files on that playlist no longer exist... you don't want a message box
  and a lot of dead air.  You'd want it to silently advance.  Still, enabling
  this option is useful for troubleshooting.

Title:

* Title Format

  This affects the titles shown in Winamp's main player window and the
  playlist.  Variables enclosed in % signs, such as %title% or %game%, are
  replaced with the corresponding values from the QSF file's tag.

* Fallback if no variables are defined

  If a particular file contains none of the variables listed in the title
  format, then the plugin will use this alternate format instead.

-----------------------------------------------------------------------------

File Information (WINAMP 2 AND 5 ONLY)
----------------

The layout of the File Information (Alt+3) box should be familiar to anyone
who's used Winamp to play MP3s before.  Here you may edit the title, artist,
game, year, genre, comment, and copyright associated with the song, and the
name of the QSF file's creator ("QSF by").

You can also set the relative volume, song length, and fade length of each
file.  The volume you set here is relative to the global volume set under
Configuration.  Lengths set here will override the defaults.

Finally, the Raw Mode button will allow you to edit the raw ASCII tag
associated with the QSF file.  This can be useful if you wish to add your
own custom variables to the tag.  Simply add extra lines of the format
variable=value to the end of the tag.  You can then make these show up in
Winamp's player window and playlist, by adding %variable% to the title
format under Configuration.

-----------------------------------------------------------------------------

Known Bugs, Issues, Unhappy Things
----------------------------------

* QSound emulation is based on information from MAME and isn't very accurate
  (by my perfectionist standards).  Specifically, there seems to be some sort
  of ADSR envelope action going on that isn't documented anywhere.  I also
  think there's some extra signal processing that's supposed to happen to get
  the wide-stereo effect, but that's not emulated either.  And without actual
  hardware to bang on, I'm kind of stuck.  So there you go.

* HQ uses 4-point Gaussian interpolation for now, since I think it sounds
  nice and smoothes out some of the 8-bit sample noise.  In the future, this
  may be optional.

* Speed.  There's plenty of room left to optimize, but it's not even breaking
  1% CPU time on my P4 2.4GHz anyway, so I didn't optimize it yet.  Sorry.

* Winamp 3 support is incomplete, and now that Winamp 3 itself is deprecated,
  I don't see much reason to finish it.  Upgrade to 5 if you haven't already.

* Seeking.  Seeking forwards is slow, and seeking backwards requires the
  plugin to rewind the QSF to the beginning.  This is an unfortunate tradeoff
  of the format.

-----------------------------------------------------------------------------

Terms of Use
------------

Highly Quixotic is freeware and may be distributed freely as long as it is
not modified, and this documentation is included and not modified either.
"Distributed freely" means no money, goods, or services may be charged or
solicited for Highly Quixotic or any media or software package containing it.

Anyone using Highly Quixotic does so at their own risk.  I will not be held
liable for any loss or damage arising from its use.  No warranty is expressed
or implied.

-----------------------------------------------------------------------------

Highly Quixotic includes code from the following projects:

* zlib Copyright 1995-2002 Jean-loup Gailly and Mark Adler
  http://www.gzip.org/zlib/
* UPX Copyright 1996, 1997, 1998, 1999, 2000, 2001, 2002 Markus F.X.J.
  Oberhumer & L�szl� Moln�r
  http://upx.sourceforge.net/

-----------------------------------------------------------------------------

Thanks to
---------

* Paul Leaman, Miguel Angel Horna, and anyone else involved in MAME's QSound
  implementation.
* All the Capcom musicians who made QSound worthy of its own format.
* P-P-P-ProtoCat.

-----------------------------------------------------------------------------

Where to find me
----------------

email: neill@neillcorlett.com
web:   http://www.neillcorlett.com/

-----------------------------------------------------------------------------
