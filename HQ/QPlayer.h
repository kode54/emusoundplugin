// QPlayer.h: interface for the CQPlayer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_QPLAYER_H__A7BD3916_032E_4273_A6CB_FCF2C54487D4__INCLUDED_)
#define AFX_QPLAYER_H__A7BD3916_032E_4273_A6CB_FCF2C54487D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../PSF/Util/PSFPlayer.h"

#include "../MFCUtil/File.h"

#include "QSoundROM.h"

class CQPlayer : public CPSFPlayer  
{
public:
	CQPlayer(File f);
	virtual ~CQPlayer();

    virtual CStringW GetFormat();

protected:
	virtual BOOL RawOpen();
	virtual void RawClose();
	virtual int RawRead(short *buffer, int nSamples);

private:
    BOOL BuildExecutable(File f, CQSoundROM &rom, int nLevel);

    CQSoundROM m_ROM;
    CByteArray m_aHW;
};

#endif // !defined(AFX_QPLAYER_H__A7BD3916_032E_4273_A6CB_FCF2C54487D4__INCLUDED_)
