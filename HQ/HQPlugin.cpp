// HQPlugin.cpp: implementation of the CHQPlugin class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "in_qsf.h"

#include "HQPlugin.h"
#include "QPlayer.h"

#include "AboutDlg.h"

//#include "ConfigEmuPage.h"

#include "../QSound/Core/qsound.h"

#include "../PSF/Util/PSFDecompressor.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////

LPCWSTR CHQPlugin::GetBanner() {
    return L"Highly Quixotic QSF Player v1.02dev";
}

LPCWSTR CHQPlugin::GetShortBanner() {
    return L"Highly Quixotic";
}

LPCWSTR* CHQPlugin::GetSupportedExtensions() {
    static LPCWSTR list[] = {
        L"qsf"     , L"QSound Format",
        L"miniqsf" , L"Mini-QSF",
        NULL
    };
    return list;
}

LPCWSTR CHQPlugin::GetOptionSection() {
    return L"HighlyQuixotic";
}

LPCWSTR* CHQPlugin::GetAllPSFTypes() {
    static LPCWSTR list[] = {
        L"QSF",
        NULL
    };
    return list;
}

LPCWSTR CHQPlugin::GetPSFType(int nPSFVersion) {
    switch(nPSFVersion) {
    case 0x41: return L"QSF";
    }
    return L"";
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHQPlugin::CHQPlugin()
{
  srand(time(NULL));
  ::qsound_init();

//  opt_bEnableDry  = TRUE;
}

CHQPlugin::~CHQPlugin() { }

/////////////////////////////////////////////////////////////////////////////

ESP_OPTION_START(CHQPlugin, CPSFPlugin)

ESP_OPTION_END(CHQPlugin, CPSFPlugin)

/////////////////////////////////////////////////////////////////////////////

bool CHQPlugin::CheckMagic(BYTE *buf, DWORD length) {
    if(length < 4) return false;
    if(memcmp(buf, "PSF", 3)) return false;
    switch(buf[3]) {
    case 0x41:
        return true;
    }
    return false;
}

/////////////////////////////////////////////////////////////////////////////

CPlayer* CHQPlugin::CreatePlayer(File f, CStringW &strError) {

    int ver = CPSFPlugin::GrabPSFVersion(f, strError);
    if(ver < 0) return NULL;

    strError = L"Out of memory";
    switch(ver) {
    case 0x41: return new CQPlayer(f);
    }
    strError = L"Incorrect version; not a QSF file";
    return NULL;
}

/////////////////////////////////////////////////////////////////////////////

int CHQPlugin::GetPSFLogoBitmap(File f, CPSFTag *pPSFTag, int nVersion)
{
    return IDB_HQ;
}

/////////////////////////////////////////////////////////////////////////////

void CHQPlugin::About(HWND hWndParent) {
    CAboutDlg dlg(CWnd::FromHandle(hWndParent));

    // TODO: version details again

    dlg.m_strBanner = GetBanner();

    CStringW s;

    s += GetBanner();

    s += L"\n";

    s += L"Built ";
    s += __DATE__;

    s += L"\n\n";

    s += GetFrameworkVersion();
    s += L"\n";
    s += ::qsound_getversion();
    s += L"\n";
    s += CPSFDecompressor::GetVersion();

    dlg.m_strVersionDetails = s;

    dlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////

void CHQPlugin::BuildConfigPropertySheet(CPropertySheet *pSheet) {

  CPSFPlugin::BuildConfigPropertySheet(pSheet);

//  { CConfigEmuPage *p = new CConfigEmuPage;
//    p->BindToPlugin(this);
//    pSheet->AddPage(p);
//  }
}

/////////////////////////////////////////////////////////////////////////////
