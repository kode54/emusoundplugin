// in_qsf.h : main header file for the IN_QSF DLL
//

#if !defined(AFX_IN_QSF_H__19F257EE_7574_4D0D_823E_41493D45E58C__INCLUDED_)
#define AFX_IN_QSF_H__19F257EE_7574_4D0D_823E_41493D45E58C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CIn_qsfApp
// See in_qsf.cpp for the implementation of this class
//

class CIn_qsfApp : public CWinApp
{
public:
	CIn_qsfApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIn_qsfApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CIn_qsfApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IN_QSF_H__19F257EE_7574_4D0D_823E_41493D45E58C__INCLUDED_)
