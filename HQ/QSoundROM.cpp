// QSoundROM.cpp: implementation of the CQSoundROM class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "in_qsf.h"
#include "QSoundROM.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CQSoundROM::CQSoundROM()
{
  m_aKey.SetSize(0);
  m_aKeyValid.SetSize(0);
  m_aZ80ROM.SetSize(0);
  m_aZ80ROMValid.SetSize(0);
  m_aSampleROM.SetSize(0);
  m_aSampleROMValid.SetSize(0);
}

CQSoundROM::~CQSoundROM()
{

}

void CQSoundROM::Clear()
{
  m_aKey.SetSize(0);
  m_aKeyValid.SetSize(0);
  m_aZ80ROM.SetSize(0);
  m_aZ80ROMValid.SetSize(0);
  m_aSampleROM.SetSize(0);
  m_aSampleROMValid.SetSize(0);
}

//////////////////////////////////////////////////////////////////////

BOOL CQSoundROM::UploadSection(LPCSTR section, DWORD start, LPBYTE data, DWORD size)
{
  CByteArray *pArray = NULL;
  CDWordArray *pArrayValid = NULL;
  DWORD maxsize = 0x7FFFFFFF;

  CStringW section_name = section;

       if(!strcmp(section, "KEY")) { pArray = &m_aKey; pArrayValid = &m_aKeyValid; maxsize = 11; }
  else if(!strcmp(section, "Z80")) { pArray = &m_aZ80ROM; pArrayValid = &m_aZ80ROMValid; }
  else if(!strcmp(section, "SMP")) { pArray = &m_aSampleROM; pArrayValid = &m_aSampleROMValid; }
  else {
    m_strLastError.Format(L"Unknown tag: '%s'", LPCWSTR(section_name));
    return FALSE;
  }

  if((start + size) < start) {
    m_strLastError.Format(L"Section '%s' is too large", LPCWSTR(section_name));
    return FALSE;
  }

  DWORD newsize = start + size;
  DWORD oldsize = pArray->GetSize();
  if(newsize > maxsize) {
    m_strLastError.Format(L"Section '%s' is too large (max %d bytes)", LPCWSTR(section_name), maxsize);
    return FALSE;
  }

  if(newsize > oldsize) {
    pArray->SetSize(newsize);
    memset(pArray->GetData() + oldsize, 0, newsize - oldsize);
  }

  memcpy(pArray->GetData() + start, data, size);

  oldsize = pArrayValid->GetSize();
  pArrayValid->SetSize(oldsize + 2);
  (pArrayValid->GetData())[oldsize+0] = start;
  (pArrayValid->GetData())[oldsize+1] = size;

  return TRUE;
}

//////////////////////////////////////////////////////////////////////

BOOL CQSoundROM::SuperimposeFrom(CQSoundROM &from)
{
  if(!SuperimposeSectionFrom("KEY", from.m_aKey      , from.m_aKeyValid      )) return FALSE;
  if(!SuperimposeSectionFrom("Z80", from.m_aZ80ROM   , from.m_aZ80ROMValid   )) return FALSE;
  if(!SuperimposeSectionFrom("SMP", from.m_aSampleROM, from.m_aSampleROMValid)) return FALSE;
  return TRUE;
}

//////////////////////////////////////////////////////////////////////

BOOL CQSoundROM::SuperimposeSectionFrom(LPCSTR section, CByteArray &from, CDWordArray &fromvalid)
{
  int i;

  for(i = 0; (i+1) < fromvalid.GetSize(); i += 2) {
    DWORD start = (fromvalid.GetData())[i+0];
    DWORD size  = (fromvalid.GetData())[i+1];
    if(
      (start          >= from.GetSize()) ||
      (size           >  from.GetSize()) ||
      ((start + size) >  from.GetSize())
    ) {
      m_strLastError = L"Invalid start/size in QSoundROM internal list (program error)";
      return FALSE;
    }

    if(!UploadSection(section, start, from.GetData() + start, size)) return FALSE;
  }

  // success
  return TRUE;
}

//////////////////////////////////////////////////////////////////////
